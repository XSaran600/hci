﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000C TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000D System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C ();
// 0x0000000E System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75 ();
// 0x0000000F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000012 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000015 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000016 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000017 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000019 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001A System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001D System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000026 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000027 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000029 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000002A System.Void System.Linq.Enumerable_<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228 ();
// 0x0000002B System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032 ();
// 0x0000002C System.Boolean System.Linq.Enumerable_<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10 ();
// 0x0000002D System.Int32 System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D ();
// 0x0000002E System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2 ();
// 0x0000002F System.Object System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB ();
// 0x00000030 System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4 ();
// 0x00000031 System.Collections.IEnumerator System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880 ();
// 0x00000032 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000033 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000034 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000035 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000036 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000037 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000038 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000039 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000003A System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000003B System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000003C System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000003D System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000003E System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000003F System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000040 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000041 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000042 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000043 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000044 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000045 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000046 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000047 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000048 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000004A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000004C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000004D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000004F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000050 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000051 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000052 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000053 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000057 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000058 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000059 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000005A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000005B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000005C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000005D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000005E System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000005F System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000060 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000061 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000062 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000063 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000064 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[100] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C,
	Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032,
	U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[100] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	537,
	537,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	95,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[23] = 
{
	{ 0x02000004, { 38, 4 } },
	{ 0x02000005, { 42, 9 } },
	{ 0x02000006, { 51, 7 } },
	{ 0x02000007, { 58, 10 } },
	{ 0x02000008, { 68, 1 } },
	{ 0x0200000B, { 69, 3 } },
	{ 0x0200000C, { 72, 5 } },
	{ 0x0200000D, { 77, 7 } },
	{ 0x0200000E, { 84, 3 } },
	{ 0x0200000F, { 87, 7 } },
	{ 0x02000010, { 94, 4 } },
	{ 0x02000011, { 98, 21 } },
	{ 0x02000013, { 119, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 2 } },
	{ 0x06000008, { 17, 3 } },
	{ 0x06000009, { 20, 4 } },
	{ 0x0600000A, { 24, 4 } },
	{ 0x0600000B, { 28, 3 } },
	{ 0x0600000C, { 31, 3 } },
	{ 0x0600000F, { 34, 1 } },
	{ 0x06000010, { 35, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[121] = 
{
	{ (Il2CppRGCTXDataType)2, 22123 },
	{ (Il2CppRGCTXDataType)3, 15210 },
	{ (Il2CppRGCTXDataType)2, 22124 },
	{ (Il2CppRGCTXDataType)2, 22125 },
	{ (Il2CppRGCTXDataType)3, 15211 },
	{ (Il2CppRGCTXDataType)2, 22126 },
	{ (Il2CppRGCTXDataType)2, 22127 },
	{ (Il2CppRGCTXDataType)3, 15212 },
	{ (Il2CppRGCTXDataType)2, 22128 },
	{ (Il2CppRGCTXDataType)3, 15213 },
	{ (Il2CppRGCTXDataType)2, 22129 },
	{ (Il2CppRGCTXDataType)3, 15214 },
	{ (Il2CppRGCTXDataType)3, 15215 },
	{ (Il2CppRGCTXDataType)2, 14528 },
	{ (Il2CppRGCTXDataType)3, 15216 },
	{ (Il2CppRGCTXDataType)2, 22130 },
	{ (Il2CppRGCTXDataType)3, 15217 },
	{ (Il2CppRGCTXDataType)2, 22131 },
	{ (Il2CppRGCTXDataType)3, 15218 },
	{ (Il2CppRGCTXDataType)3, 15219 },
	{ (Il2CppRGCTXDataType)2, 22132 },
	{ (Il2CppRGCTXDataType)2, 22133 },
	{ (Il2CppRGCTXDataType)2, 14538 },
	{ (Il2CppRGCTXDataType)2, 22134 },
	{ (Il2CppRGCTXDataType)2, 22135 },
	{ (Il2CppRGCTXDataType)2, 22136 },
	{ (Il2CppRGCTXDataType)2, 14540 },
	{ (Il2CppRGCTXDataType)2, 22137 },
	{ (Il2CppRGCTXDataType)2, 14542 },
	{ (Il2CppRGCTXDataType)2, 22138 },
	{ (Il2CppRGCTXDataType)3, 15220 },
	{ (Il2CppRGCTXDataType)2, 22139 },
	{ (Il2CppRGCTXDataType)2, 14545 },
	{ (Il2CppRGCTXDataType)2, 22140 },
	{ (Il2CppRGCTXDataType)2, 14547 },
	{ (Il2CppRGCTXDataType)2, 14549 },
	{ (Il2CppRGCTXDataType)2, 22141 },
	{ (Il2CppRGCTXDataType)3, 15221 },
	{ (Il2CppRGCTXDataType)3, 15222 },
	{ (Il2CppRGCTXDataType)3, 15223 },
	{ (Il2CppRGCTXDataType)2, 14554 },
	{ (Il2CppRGCTXDataType)3, 15224 },
	{ (Il2CppRGCTXDataType)3, 15225 },
	{ (Il2CppRGCTXDataType)2, 14563 },
	{ (Il2CppRGCTXDataType)2, 22142 },
	{ (Il2CppRGCTXDataType)3, 15226 },
	{ (Il2CppRGCTXDataType)3, 15227 },
	{ (Il2CppRGCTXDataType)2, 14565 },
	{ (Il2CppRGCTXDataType)2, 21998 },
	{ (Il2CppRGCTXDataType)3, 15228 },
	{ (Il2CppRGCTXDataType)3, 15229 },
	{ (Il2CppRGCTXDataType)3, 15230 },
	{ (Il2CppRGCTXDataType)2, 14572 },
	{ (Il2CppRGCTXDataType)2, 22143 },
	{ (Il2CppRGCTXDataType)3, 15231 },
	{ (Il2CppRGCTXDataType)3, 15232 },
	{ (Il2CppRGCTXDataType)3, 14376 },
	{ (Il2CppRGCTXDataType)3, 15233 },
	{ (Il2CppRGCTXDataType)3, 15234 },
	{ (Il2CppRGCTXDataType)2, 14581 },
	{ (Il2CppRGCTXDataType)2, 22144 },
	{ (Il2CppRGCTXDataType)3, 15235 },
	{ (Il2CppRGCTXDataType)3, 15236 },
	{ (Il2CppRGCTXDataType)3, 15237 },
	{ (Il2CppRGCTXDataType)3, 15238 },
	{ (Il2CppRGCTXDataType)3, 15239 },
	{ (Il2CppRGCTXDataType)3, 14382 },
	{ (Il2CppRGCTXDataType)3, 15240 },
	{ (Il2CppRGCTXDataType)3, 15241 },
	{ (Il2CppRGCTXDataType)2, 22145 },
	{ (Il2CppRGCTXDataType)3, 15242 },
	{ (Il2CppRGCTXDataType)3, 15243 },
	{ (Il2CppRGCTXDataType)2, 22146 },
	{ (Il2CppRGCTXDataType)3, 15244 },
	{ (Il2CppRGCTXDataType)3, 15245 },
	{ (Il2CppRGCTXDataType)3, 15246 },
	{ (Il2CppRGCTXDataType)2, 14608 },
	{ (Il2CppRGCTXDataType)3, 15247 },
	{ (Il2CppRGCTXDataType)2, 14617 },
	{ (Il2CppRGCTXDataType)3, 15248 },
	{ (Il2CppRGCTXDataType)2, 22147 },
	{ (Il2CppRGCTXDataType)2, 22148 },
	{ (Il2CppRGCTXDataType)3, 15249 },
	{ (Il2CppRGCTXDataType)3, 15250 },
	{ (Il2CppRGCTXDataType)3, 15251 },
	{ (Il2CppRGCTXDataType)3, 15252 },
	{ (Il2CppRGCTXDataType)3, 15253 },
	{ (Il2CppRGCTXDataType)3, 15254 },
	{ (Il2CppRGCTXDataType)2, 14633 },
	{ (Il2CppRGCTXDataType)2, 22149 },
	{ (Il2CppRGCTXDataType)3, 15255 },
	{ (Il2CppRGCTXDataType)3, 15256 },
	{ (Il2CppRGCTXDataType)2, 14637 },
	{ (Il2CppRGCTXDataType)3, 15257 },
	{ (Il2CppRGCTXDataType)2, 22150 },
	{ (Il2CppRGCTXDataType)2, 14647 },
	{ (Il2CppRGCTXDataType)2, 14645 },
	{ (Il2CppRGCTXDataType)2, 22151 },
	{ (Il2CppRGCTXDataType)3, 15258 },
	{ (Il2CppRGCTXDataType)2, 22152 },
	{ (Il2CppRGCTXDataType)3, 15259 },
	{ (Il2CppRGCTXDataType)3, 15260 },
	{ (Il2CppRGCTXDataType)3, 15261 },
	{ (Il2CppRGCTXDataType)2, 14651 },
	{ (Il2CppRGCTXDataType)3, 15262 },
	{ (Il2CppRGCTXDataType)3, 15263 },
	{ (Il2CppRGCTXDataType)2, 14654 },
	{ (Il2CppRGCTXDataType)3, 15264 },
	{ (Il2CppRGCTXDataType)1, 22153 },
	{ (Il2CppRGCTXDataType)2, 14653 },
	{ (Il2CppRGCTXDataType)3, 15265 },
	{ (Il2CppRGCTXDataType)1, 14653 },
	{ (Il2CppRGCTXDataType)1, 14651 },
	{ (Il2CppRGCTXDataType)2, 22154 },
	{ (Il2CppRGCTXDataType)2, 14653 },
	{ (Il2CppRGCTXDataType)3, 15266 },
	{ (Il2CppRGCTXDataType)3, 15267 },
	{ (Il2CppRGCTXDataType)3, 15268 },
	{ (Il2CppRGCTXDataType)2, 14652 },
	{ (Il2CppRGCTXDataType)3, 15269 },
	{ (Il2CppRGCTXDataType)2, 14665 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	100,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	23,
	s_rgctxIndices,
	121,
	s_rgctxValues,
	NULL,
};
