﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern void ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF ();
// 0x00000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_Particle)
extern void ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D ();
// 0x00000003 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem_Particle[],System.Int32,System.Int32)
extern void ParticleSystem_SetParticles_mBD5C10AC2CCDECED4FEFE83235CF2CA1257A68AC ();
// 0x00000004 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem_Particle[],System.Int32)
extern void ParticleSystem_SetParticles_mCBB22C645CD23845D88FDF981FC2F32E31AB4FB5 ();
// 0x00000005 System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern void ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2 ();
// 0x00000006 System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
extern void ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925 ();
// 0x00000007 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_EmitParams,System.Int32)
extern void ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2 ();
// 0x00000008 System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem_Particle&)
extern void ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E ();
// 0x00000009 UnityEngine.ParticleSystem_MainModule UnityEngine.ParticleSystem::get_main()
extern void ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F ();
// 0x0000000A UnityEngine.ParticleSystem_EmissionModule UnityEngine.ParticleSystem::get_emission()
extern void ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C ();
// 0x0000000B UnityEngine.ParticleSystem_ShapeModule UnityEngine.ParticleSystem::get_shape()
extern void ParticleSystem_get_shape_m2587D055BE021CAFEDEEC2442E9F1508B6BDA6A8 ();
// 0x0000000C System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem_EmitParams&,System.Int32)
extern void ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC ();
// 0x0000000D System.Void UnityEngine.ParticleSystem_MainModule::.ctor(UnityEngine.ParticleSystem)
extern void MainModule__ctor_m10DC65291ACEC243EC5302404E059717B552BA7A_AdjustorThunk ();
// 0x0000000E System.Void UnityEngine.ParticleSystem_MainModule::set_loop(System.Boolean)
extern void MainModule_set_loop_mC661EA0DB8B61948CF9059D023CF82FFBF9E26FD_AdjustorThunk ();
// 0x0000000F System.Void UnityEngine.ParticleSystem_MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
extern void MainModule_set_simulationSpace_m79F2B40B671F8F918C22A7EB1650E742C654B326_AdjustorThunk ();
// 0x00000010 System.Void UnityEngine.ParticleSystem_MainModule::set_playOnAwake(System.Boolean)
extern void MainModule_set_playOnAwake_m60D008F80A2D5F2A612541F1700DD9FD1DCE722B_AdjustorThunk ();
// 0x00000011 System.Void UnityEngine.ParticleSystem_MainModule::set_loop_Injected(UnityEngine.ParticleSystem_MainModule&,System.Boolean)
extern void MainModule_set_loop_Injected_m0BECDDD18E8CA36DD55EEA08761BDFD90132C989 ();
// 0x00000012 System.Void UnityEngine.ParticleSystem_MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem_MainModule&,UnityEngine.ParticleSystemSimulationSpace)
extern void MainModule_set_simulationSpace_Injected_mDB0119F2712836C70136FC6BA37CEF3894F65A5D ();
// 0x00000013 System.Void UnityEngine.ParticleSystem_MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem_MainModule&,System.Boolean)
extern void MainModule_set_playOnAwake_Injected_mBC06F6D1503103C3FC50694339A726B6AE561279 ();
// 0x00000014 System.Void UnityEngine.ParticleSystem_EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern void EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk ();
// 0x00000015 System.Void UnityEngine.ParticleSystem_EmissionModule::set_enabled(System.Boolean)
extern void EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36_AdjustorThunk ();
// 0x00000016 System.Void UnityEngine.ParticleSystem_EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem_EmissionModule&,System.Boolean)
extern void EmissionModule_set_enabled_Injected_m80C70B60E49D3ADE643B12579CE8CD119BD7D5F4 ();
// 0x00000017 System.Void UnityEngine.ParticleSystem_ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern void ShapeModule__ctor_mB19F3EBB92FAD933070C3F52965041DC8738CF59_AdjustorThunk ();
// 0x00000018 System.Void UnityEngine.ParticleSystem_ShapeModule::set_enabled(System.Boolean)
extern void ShapeModule_set_enabled_mC7CC4D33D22925D65AD23FB8FA02CFB8C4509F02_AdjustorThunk ();
// 0x00000019 System.Void UnityEngine.ParticleSystem_ShapeModule::set_enabled_Injected(UnityEngine.ParticleSystem_ShapeModule&,System.Boolean)
extern void ShapeModule_set_enabled_Injected_m8A3197BE0DD20A36A1E92F1A6D96EDEA84922CE0 ();
// 0x0000001A System.Void UnityEngine.ParticleSystem_Particle::set_lifetime(System.Single)
extern void Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk ();
// 0x0000001B System.Void UnityEngine.ParticleSystem_Particle::set_position(UnityEngine.Vector3)
extern void Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk ();
// 0x0000001C System.Void UnityEngine.ParticleSystem_Particle::set_velocity(UnityEngine.Vector3)
extern void Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.ParticleSystem_Particle::set_remainingLifetime(System.Single)
extern void Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk ();
// 0x0000001E System.Void UnityEngine.ParticleSystem_Particle::set_startLifetime(System.Single)
extern void Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk ();
// 0x0000001F System.Void UnityEngine.ParticleSystem_Particle::set_startColor(UnityEngine.Color32)
extern void Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk ();
// 0x00000020 System.Void UnityEngine.ParticleSystem_Particle::set_randomSeed(System.UInt32)
extern void Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk ();
// 0x00000021 System.Void UnityEngine.ParticleSystem_Particle::set_startSize(System.Single)
extern void Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk ();
// 0x00000022 System.Void UnityEngine.ParticleSystem_Particle::set_rotation3D(UnityEngine.Vector3)
extern void Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk ();
// 0x00000023 System.Void UnityEngine.ParticleSystem_Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern void Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk ();
// 0x00000024 System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
extern void ParticleSystemRenderer_set_renderMode_mD572CDD6424E8982A57340E14854761079A7B521 ();
// 0x00000025 System.Void UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)
extern void ParticleSystemRenderer_set_mesh_m3B20D8EDDDE4D261DAA7B0EF7CCDFC3D906213BA ();
// 0x00000026 System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2 ();
static Il2CppMethodPointer s_methodPointers[38] = 
{
	ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF,
	ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D,
	ParticleSystem_SetParticles_mBD5C10AC2CCDECED4FEFE83235CF2CA1257A68AC,
	ParticleSystem_SetParticles_mCBB22C645CD23845D88FDF981FC2F32E31AB4FB5,
	ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2,
	ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925,
	ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2,
	ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E,
	ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F,
	ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C,
	ParticleSystem_get_shape_m2587D055BE021CAFEDEEC2442E9F1508B6BDA6A8,
	ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC,
	MainModule__ctor_m10DC65291ACEC243EC5302404E059717B552BA7A_AdjustorThunk,
	MainModule_set_loop_mC661EA0DB8B61948CF9059D023CF82FFBF9E26FD_AdjustorThunk,
	MainModule_set_simulationSpace_m79F2B40B671F8F918C22A7EB1650E742C654B326_AdjustorThunk,
	MainModule_set_playOnAwake_m60D008F80A2D5F2A612541F1700DD9FD1DCE722B_AdjustorThunk,
	MainModule_set_loop_Injected_m0BECDDD18E8CA36DD55EEA08761BDFD90132C989,
	MainModule_set_simulationSpace_Injected_mDB0119F2712836C70136FC6BA37CEF3894F65A5D,
	MainModule_set_playOnAwake_Injected_mBC06F6D1503103C3FC50694339A726B6AE561279,
	EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk,
	EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36_AdjustorThunk,
	EmissionModule_set_enabled_Injected_m80C70B60E49D3ADE643B12579CE8CD119BD7D5F4,
	ShapeModule__ctor_mB19F3EBB92FAD933070C3F52965041DC8738CF59_AdjustorThunk,
	ShapeModule_set_enabled_mC7CC4D33D22925D65AD23FB8FA02CFB8C4509F02_AdjustorThunk,
	ShapeModule_set_enabled_Injected_m8A3197BE0DD20A36A1E92F1A6D96EDEA84922CE0,
	Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk,
	Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk,
	Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk,
	Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk,
	Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk,
	Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk,
	Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk,
	Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk,
	Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk,
	Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk,
	ParticleSystemRenderer_set_renderMode_mD572CDD6424E8982A57340E14854761079A7B521,
	ParticleSystemRenderer_set_mesh_m3B20D8EDDDE4D261DAA7B0EF7CCDFC3D906213BA,
	ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2,
};
static const int32_t s_InvokerIndices[38] = 
{
	2086,
	2087,
	35,
	143,
	32,
	32,
	2088,
	6,
	2089,
	2090,
	2091,
	64,
	26,
	31,
	32,
	31,
	22,
	308,
	22,
	26,
	31,
	22,
	26,
	31,
	22,
	296,
	1181,
	1181,
	296,
	296,
	2092,
	32,
	296,
	1181,
	1181,
	32,
	26,
	123,
};
extern const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule = 
{
	"UnityEngine.ParticleSystemModule.dll",
	38,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
