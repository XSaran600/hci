﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean OVRBoundary::GetConfigured()
extern void OVRBoundary_GetConfigured_m5093DB550C36074DD3AD8F26296090E000775DD3 ();
// 0x00000002 OVRBoundary_BoundaryTestResult OVRBoundary::TestNode(OVRBoundary_Node,OVRBoundary_BoundaryType)
extern void OVRBoundary_TestNode_mF2DBD2BF1C57CD51F18471379A32D0C64DDB360E ();
// 0x00000003 OVRBoundary_BoundaryTestResult OVRBoundary::TestPoint(UnityEngine.Vector3,OVRBoundary_BoundaryType)
extern void OVRBoundary_TestPoint_m97C69C789CB97E33187402BD9FC5BC7158A50A90 ();
// 0x00000004 UnityEngine.Vector3[] OVRBoundary::GetGeometry(OVRBoundary_BoundaryType)
extern void OVRBoundary_GetGeometry_m0970B7164721759ECACB831864D9FB25400EB322 ();
// 0x00000005 UnityEngine.Vector3 OVRBoundary::GetDimensions(OVRBoundary_BoundaryType)
extern void OVRBoundary_GetDimensions_m5475000A9A0C37FC173E00227895BD3448B70574 ();
// 0x00000006 System.Boolean OVRBoundary::GetVisible()
extern void OVRBoundary_GetVisible_mFBFD780CF9C41971DDB3603178D5F8DE0DB5A28F ();
// 0x00000007 System.Void OVRBoundary::SetVisible(System.Boolean)
extern void OVRBoundary_SetVisible_mFBB5852614DB6E8EC0BED7866812E0FF4280EA3E ();
// 0x00000008 System.Void OVRBoundary::.ctor()
extern void OVRBoundary__ctor_m52212DC720BA6B980860AB1BA8DE279B87CD7C9C ();
// 0x00000009 System.Void OVRBoundary::.cctor()
extern void OVRBoundary__cctor_m86793BCC0E922B1A776848B012CF4A85B72C37C5 ();
// 0x0000000A UnityEngine.Camera OVRCameraRig::get_leftEyeCamera()
extern void OVRCameraRig_get_leftEyeCamera_mF543376FEB5FFDEA4E14DC98A4C8FDD145216E82 ();
// 0x0000000B UnityEngine.Camera OVRCameraRig::get_rightEyeCamera()
extern void OVRCameraRig_get_rightEyeCamera_m2D5C77B7B47EF045A27AAC1830C75EFC8ED97289 ();
// 0x0000000C UnityEngine.Transform OVRCameraRig::get_trackingSpace()
extern void OVRCameraRig_get_trackingSpace_m2DCEEAA2003B4A7EEAE4E57EEDD84E38C903F4AD ();
// 0x0000000D System.Void OVRCameraRig::set_trackingSpace(UnityEngine.Transform)
extern void OVRCameraRig_set_trackingSpace_m53A921D03416919F6BC9C0995E06A09469965B62 ();
// 0x0000000E UnityEngine.Transform OVRCameraRig::get_leftEyeAnchor()
extern void OVRCameraRig_get_leftEyeAnchor_m7B92D88D81613FCC34826AC8EC4DE637AC193D35 ();
// 0x0000000F System.Void OVRCameraRig::set_leftEyeAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_leftEyeAnchor_mDD27E24508D052E02247A832257FBFB45777B9CB ();
// 0x00000010 UnityEngine.Transform OVRCameraRig::get_centerEyeAnchor()
extern void OVRCameraRig_get_centerEyeAnchor_mD0C4CBA5D31C5A2DD25CF752D1949008D731D5BD ();
// 0x00000011 System.Void OVRCameraRig::set_centerEyeAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_centerEyeAnchor_m7DB98233FC4BD3B6E0E37EE614E0481767720538 ();
// 0x00000012 UnityEngine.Transform OVRCameraRig::get_rightEyeAnchor()
extern void OVRCameraRig_get_rightEyeAnchor_m05D61CE77257E9466F8CE866C4F2EAC1ABDD3E67 ();
// 0x00000013 System.Void OVRCameraRig::set_rightEyeAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_rightEyeAnchor_mC70B21CBEF41E0C94D340FD319B6A95A5ACDC7A4 ();
// 0x00000014 UnityEngine.Transform OVRCameraRig::get_leftHandAnchor()
extern void OVRCameraRig_get_leftHandAnchor_m8D785A001836842F561158E6FB5963E3E0BD121F ();
// 0x00000015 System.Void OVRCameraRig::set_leftHandAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_leftHandAnchor_m3632DF173E4481ABAF488A0F1FC99367694E8EA4 ();
// 0x00000016 UnityEngine.Transform OVRCameraRig::get_rightHandAnchor()
extern void OVRCameraRig_get_rightHandAnchor_m83E255B33DEEB3ED0EC58B7875BA3E7FAF47D422 ();
// 0x00000017 System.Void OVRCameraRig::set_rightHandAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_rightHandAnchor_mB2527E44B61D609253C69E8A24F6CF73E045CC42 ();
// 0x00000018 UnityEngine.Transform OVRCameraRig::get_leftControllerAnchor()
extern void OVRCameraRig_get_leftControllerAnchor_mC4F9EC32A4D317F0A839EB8D3E5BB3F6DB02F963 ();
// 0x00000019 System.Void OVRCameraRig::set_leftControllerAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_leftControllerAnchor_mF12782CABE8A1316C3F24FC9EF588AB17271E082 ();
// 0x0000001A UnityEngine.Transform OVRCameraRig::get_rightControllerAnchor()
extern void OVRCameraRig_get_rightControllerAnchor_mB384CF63CCC56DD8DCDAC016E2994D2B3BFF951A ();
// 0x0000001B System.Void OVRCameraRig::set_rightControllerAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_rightControllerAnchor_m87289B7FCE6DA0D5463B9A0F49D3FB31AB9B6578 ();
// 0x0000001C UnityEngine.Transform OVRCameraRig::get_trackerAnchor()
extern void OVRCameraRig_get_trackerAnchor_mE8338A133E43414996181AD53AAEE6B43C97C3C9 ();
// 0x0000001D System.Void OVRCameraRig::set_trackerAnchor(UnityEngine.Transform)
extern void OVRCameraRig_set_trackerAnchor_m4F2626A2928469F4DFB8D440A1F0753B9D2E91E8 ();
// 0x0000001E System.Void OVRCameraRig::add_UpdatedAnchors(System.Action`1<OVRCameraRig>)
extern void OVRCameraRig_add_UpdatedAnchors_m6CB042885064E4261C80102E4F4CED69683996DA ();
// 0x0000001F System.Void OVRCameraRig::remove_UpdatedAnchors(System.Action`1<OVRCameraRig>)
extern void OVRCameraRig_remove_UpdatedAnchors_m362CDE8090BA05C20CB7EB79864AA714CCE0739A ();
// 0x00000020 System.Void OVRCameraRig::Awake()
extern void OVRCameraRig_Awake_mAEDD9D5712DA958F52C4E26E09531E867589DEEA ();
// 0x00000021 System.Void OVRCameraRig::Start()
extern void OVRCameraRig_Start_mAB4168D5FD1BF3660B617668E41FC75F656C3004 ();
// 0x00000022 System.Void OVRCameraRig::FixedUpdate()
extern void OVRCameraRig_FixedUpdate_m0F489DAA9D79819A2A75A8843DC318EE1CA16184 ();
// 0x00000023 System.Void OVRCameraRig::Update()
extern void OVRCameraRig_Update_m9443F276279709F7D19B05FA7F8DCE654EE2301D ();
// 0x00000024 System.Void OVRCameraRig::OnDestroy()
extern void OVRCameraRig_OnDestroy_m133CA579B879D836BCC4B2C571EE51B10D6C76D5 ();
// 0x00000025 System.Void OVRCameraRig::UpdateAnchors(System.Boolean,System.Boolean)
extern void OVRCameraRig_UpdateAnchors_m62AE37D23B98B8C59A3A4743880590D4B2BC8278 ();
// 0x00000026 System.Void OVRCameraRig::OnBeforeRenderCallback()
extern void OVRCameraRig_OnBeforeRenderCallback_m62EEA41B301C4132FA9C90854F771A013791BDCF ();
// 0x00000027 System.Void OVRCameraRig::RaiseUpdatedAnchorsEvent()
extern void OVRCameraRig_RaiseUpdatedAnchorsEvent_m9899C80EE11A6A860ACBF8F9109C04D1A4BF7EDC ();
// 0x00000028 System.Void OVRCameraRig::EnsureGameObjectIntegrity()
extern void OVRCameraRig_EnsureGameObjectIntegrity_m700ED838ECF8C8DDA42D7599E48E21A54BA82288 ();
// 0x00000029 UnityEngine.Transform OVRCameraRig::ConfigureAnchor(UnityEngine.Transform,System.String)
extern void OVRCameraRig_ConfigureAnchor_m9AC741FF628804302521B90B42FDD7D15710FBA4 ();
// 0x0000002A UnityEngine.Matrix4x4 OVRCameraRig::ComputeTrackReferenceMatrix()
extern void OVRCameraRig_ComputeTrackReferenceMatrix_m055270F34B938C600D41F4A89B4E4E70E5BA0052 ();
// 0x0000002B System.Void OVRCameraRig::.ctor()
extern void OVRCameraRig__ctor_m887B0D1F57D225421ADEA575196D0EB866BAC3F8 ();
// 0x0000002C OVRPose OVRExtensions::ToTrackingSpacePose(UnityEngine.Transform,UnityEngine.Camera)
extern void OVRExtensions_ToTrackingSpacePose_m153D7125F58A05667950419DA96E1350B1349B63 ();
// 0x0000002D OVRPose OVRExtensions::ToWorldSpacePose(OVRPose)
extern void OVRExtensions_ToWorldSpacePose_m04CAC14BAF0063B6ED4A0558B6CE53C2C433258E ();
// 0x0000002E OVRPose OVRExtensions::ToHeadSpacePose(UnityEngine.Transform,UnityEngine.Camera)
extern void OVRExtensions_ToHeadSpacePose_mE3FD24B68A44B9BB18755866E7E5DEC4BCAC16C4 ();
// 0x0000002F OVRPose OVRExtensions::ToOVRPose(UnityEngine.Transform,System.Boolean)
extern void OVRExtensions_ToOVRPose_mBF3FD269D2B7F2CF4B3BE23DBED55C9E86912F7F ();
// 0x00000030 System.Void OVRExtensions::FromOVRPose(UnityEngine.Transform,OVRPose,System.Boolean)
extern void OVRExtensions_FromOVRPose_m92C4445075BC31C165ABF5A68E6D424F4895A228 ();
// 0x00000031 OVRPose OVRExtensions::ToOVRPose(OVRPlugin_Posef)
extern void OVRExtensions_ToOVRPose_mA375B53F3D98F98B8A6BC051A8DBA0C56B28A73B ();
// 0x00000032 OVRTracker_Frustum OVRExtensions::ToFrustum(OVRPlugin_Frustumf)
extern void OVRExtensions_ToFrustum_m13CD7E4E87936E1FD88909B993123DA9B01D6E9A ();
// 0x00000033 UnityEngine.Color OVRExtensions::FromColorf(OVRPlugin_Colorf)
extern void OVRExtensions_FromColorf_mBB2EE1F348A8FDEFEAB0B45653EEAE20AB1F9F14 ();
// 0x00000034 OVRPlugin_Colorf OVRExtensions::ToColorf(UnityEngine.Color)
extern void OVRExtensions_ToColorf_m4A2DD316B299C097E01741D5F9FD0B578FF7E66B ();
// 0x00000035 UnityEngine.Vector3 OVRExtensions::FromVector3f(OVRPlugin_Vector3f)
extern void OVRExtensions_FromVector3f_m5EDD11309A45158CD8CCC4F02B624E0182DBD395 ();
// 0x00000036 UnityEngine.Vector3 OVRExtensions::FromFlippedZVector3f(OVRPlugin_Vector3f)
extern void OVRExtensions_FromFlippedZVector3f_m765F9B2F484044FF1E3BA46EB292B75C17F8D378 ();
// 0x00000037 OVRPlugin_Vector3f OVRExtensions::ToVector3f(UnityEngine.Vector3)
extern void OVRExtensions_ToVector3f_m6DB81D473AA9273AA18819586D361F373F09E915 ();
// 0x00000038 OVRPlugin_Vector3f OVRExtensions::ToFlippedZVector3f(UnityEngine.Vector3)
extern void OVRExtensions_ToFlippedZVector3f_m4517FD69E3E60DF9240F2B0AC0ED084EA4B4F225 ();
// 0x00000039 UnityEngine.Quaternion OVRExtensions::FromQuatf(OVRPlugin_Quatf)
extern void OVRExtensions_FromQuatf_mD94BD35BCCE893EAFD1D0CFF8C16AFF6845767D9 ();
// 0x0000003A UnityEngine.Quaternion OVRExtensions::FromFlippedZQuatf(OVRPlugin_Quatf)
extern void OVRExtensions_FromFlippedZQuatf_mAA94273849CBFD4DFCF09E1B6C5884E5157D4690 ();
// 0x0000003B OVRPlugin_Quatf OVRExtensions::ToQuatf(UnityEngine.Quaternion)
extern void OVRExtensions_ToQuatf_mA1471855462C5B5207C1BD457EED922CE59B50A7 ();
// 0x0000003C OVRPlugin_Quatf OVRExtensions::ToFlippedZQuatf(UnityEngine.Quaternion)
extern void OVRExtensions_ToFlippedZQuatf_mA6C3102F8A6A066FE1806245D8821D922C3E8B04 ();
// 0x0000003D OVR.OpenVR.HmdMatrix34_t OVRExtensions::ConvertToHMDMatrix34(UnityEngine.Matrix4x4)
extern void OVRExtensions_ConvertToHMDMatrix34_m3692800E1981C3AA6D5115B3779BBAE878EF85DA ();
// 0x0000003E System.Boolean OVRNodeStateProperties::IsHmdPresent()
extern void OVRNodeStateProperties_IsHmdPresent_mBE6D3E1BC46942BDD9DEAE42B420092316646C03 ();
// 0x0000003F System.Boolean OVRNodeStateProperties::GetNodeStatePropertyVector3(UnityEngine.XR.XRNode,NodeStatePropertyType,OVRPlugin_Node,OVRPlugin_Step,UnityEngine.Vector3&)
extern void OVRNodeStateProperties_GetNodeStatePropertyVector3_m275A1D9D56B948E309095D307409975CEEEE392B ();
// 0x00000040 System.Boolean OVRNodeStateProperties::GetNodeStatePropertyQuaternion(UnityEngine.XR.XRNode,NodeStatePropertyType,OVRPlugin_Node,OVRPlugin_Step,UnityEngine.Quaternion&)
extern void OVRNodeStateProperties_GetNodeStatePropertyQuaternion_mD41906383F8C16613AE40CDC5538F09B3FCA2BC6 ();
// 0x00000041 System.Boolean OVRNodeStateProperties::ValidateProperty(UnityEngine.XR.XRNode,UnityEngine.XR.XRNodeState&)
extern void OVRNodeStateProperties_ValidateProperty_m58915C2F14A6D7FAE6541C32690EEF2F0617BCB1 ();
// 0x00000042 System.Boolean OVRNodeStateProperties::GetUnityXRNodeStateVector3(UnityEngine.XR.XRNode,NodeStatePropertyType,UnityEngine.Vector3&)
extern void OVRNodeStateProperties_GetUnityXRNodeStateVector3_m959B7B20DC66F895427F3D8A56B241A26AD6A607 ();
// 0x00000043 System.Boolean OVRNodeStateProperties::GetUnityXRNodeStateQuaternion(UnityEngine.XR.XRNode,NodeStatePropertyType,UnityEngine.Quaternion&)
extern void OVRNodeStateProperties_GetUnityXRNodeStateQuaternion_m5F7144D2142021097027725805C0E8FC41CDD764 ();
// 0x00000044 System.Void OVRNodeStateProperties::.cctor()
extern void OVRNodeStateProperties__cctor_mF94AF3E22B9F66AD7F63E6B7E523F3CEA490B41E ();
// 0x00000045 OVRPose OVRPose::get_identity()
extern void OVRPose_get_identity_m7CFC673402CCF7E4CA303E321A468DA21900CB69 ();
// 0x00000046 System.Boolean OVRPose::Equals(System.Object)
extern void OVRPose_Equals_m4D3B3401FEAD916EB7808A0025B82D60FDE80846_AdjustorThunk ();
// 0x00000047 System.Int32 OVRPose::GetHashCode()
extern void OVRPose_GetHashCode_m0D6BA59B6F9B48B77A70C1F53D22A930CCE0562A_AdjustorThunk ();
// 0x00000048 System.Boolean OVRPose::op_Equality(OVRPose,OVRPose)
extern void OVRPose_op_Equality_mE9CB3DD2670E417FFACE22DF02D0E977A3A3607B ();
// 0x00000049 System.Boolean OVRPose::op_Inequality(OVRPose,OVRPose)
extern void OVRPose_op_Inequality_m9374E2CC9622AF10E7F46025103A5E6BB44E678D ();
// 0x0000004A OVRPose OVRPose::op_Multiply(OVRPose,OVRPose)
extern void OVRPose_op_Multiply_m5C746FDDF924C8C99EA06A507CA71CFF083A177A ();
// 0x0000004B OVRPose OVRPose::Inverse()
extern void OVRPose_Inverse_mFD74A78C2BA1FFBBDA3C197C977E2EF58A298E31_AdjustorThunk ();
// 0x0000004C OVRPose OVRPose::flipZ()
extern void OVRPose_flipZ_m0EE7D1A58B067F5F35177B5B39ECEAA6338B825D_AdjustorThunk ();
// 0x0000004D OVRPlugin_Posef OVRPose::ToPosef()
extern void OVRPose_ToPosef_m57D6351205C3F3D923704CF100D9C28F561BD803_AdjustorThunk ();
// 0x0000004E System.Void OVRNativeBuffer::.ctor(System.Int32)
extern void OVRNativeBuffer__ctor_m2F8C99712548549DFFF7EF18227396C7F1BFE777 ();
// 0x0000004F System.Void OVRNativeBuffer::Finalize()
extern void OVRNativeBuffer_Finalize_m463ABCE67C71616FFFF30469D0323183FAF9D35A ();
// 0x00000050 System.Void OVRNativeBuffer::Reset(System.Int32)
extern void OVRNativeBuffer_Reset_mE8D7F0FE2AE4168D21D5C4579EAC1502BDCA64A2 ();
// 0x00000051 System.Int32 OVRNativeBuffer::GetCapacity()
extern void OVRNativeBuffer_GetCapacity_m5C14781B358E57A91BB1266CC0DFB9E2FA8EA3DB ();
// 0x00000052 System.IntPtr OVRNativeBuffer::GetPointer(System.Int32)
extern void OVRNativeBuffer_GetPointer_mDEA24E55784389B79EAE6C47C2ECF5482F4DA2DB ();
// 0x00000053 System.Void OVRNativeBuffer::Dispose()
extern void OVRNativeBuffer_Dispose_m61B6AA3B8278103D8886D16BCAA1B2D8E811C13A ();
// 0x00000054 System.Void OVRNativeBuffer::Dispose(System.Boolean)
extern void OVRNativeBuffer_Dispose_m7FED58D05FB27B0AD86A2A7B645936C4D72A0379 ();
// 0x00000055 System.Void OVRNativeBuffer::Reallocate(System.Int32)
extern void OVRNativeBuffer_Reallocate_m3E2C71EAD35AE0CCC8075A33E034E1EA6B9829E2 ();
// 0x00000056 System.Void OVRNativeBuffer::Release()
extern void OVRNativeBuffer_Release_m3E15FC99E583AA0C0A89AAD634D5D6EB205523DF ();
// 0x00000057 System.Void OVRDebugHeadController::Awake()
extern void OVRDebugHeadController_Awake_mB897E6D3AD5288F255C06443413B96B7DA78570F ();
// 0x00000058 System.Void OVRDebugHeadController::Start()
extern void OVRDebugHeadController_Start_mF006A3C783A8F8912C8A193D192BDF67D5144717 ();
// 0x00000059 System.Void OVRDebugHeadController::Update()
extern void OVRDebugHeadController_Update_m80F74DF8127DD87CAA1C982F9D37C9DD3DEC6083 ();
// 0x0000005A System.Void OVRDebugHeadController::.ctor()
extern void OVRDebugHeadController__ctor_mA4D3C85F379DAACFCA4E163BAFDB71932A664608 ();
// 0x0000005B System.Void OVRDisplay::.ctor()
extern void OVRDisplay__ctor_mCACE1A08C3DCAFB90530A419A3567A87E4ADD9BC ();
// 0x0000005C System.Void OVRDisplay::Update()
extern void OVRDisplay_Update_mCCE845DAD0599EBA173E85340804A48EC480E088 ();
// 0x0000005D System.Void OVRDisplay::add_RecenteredPose(System.Action)
extern void OVRDisplay_add_RecenteredPose_mB03AB370D52729662265B119DC4C9E5A867E9CBC ();
// 0x0000005E System.Void OVRDisplay::remove_RecenteredPose(System.Action)
extern void OVRDisplay_remove_RecenteredPose_m210FCB23F397C4FD305DF660B6102E52D8B1AC24 ();
// 0x0000005F System.Void OVRDisplay::RecenterPose()
extern void OVRDisplay_RecenterPose_m06194250A34CEDB16CFBB04F282B9395BD17FD5C ();
// 0x00000060 UnityEngine.Vector3 OVRDisplay::get_acceleration()
extern void OVRDisplay_get_acceleration_m4F50142C9CC6538B341D9E115CD0DB155A00F34F ();
// 0x00000061 UnityEngine.Vector3 OVRDisplay::get_angularAcceleration()
extern void OVRDisplay_get_angularAcceleration_m9351B6C8A7F4CC9A0B05F868F87D7A7C821444EA ();
// 0x00000062 UnityEngine.Vector3 OVRDisplay::get_velocity()
extern void OVRDisplay_get_velocity_m6437597774100F4897A0C36933F404BDE6E46A8F ();
// 0x00000063 UnityEngine.Vector3 OVRDisplay::get_angularVelocity()
extern void OVRDisplay_get_angularVelocity_m8ACC39BE5FAF40FBAAFF48F8DFA0281A3D2578A3 ();
// 0x00000064 OVRDisplay_EyeRenderDesc OVRDisplay::GetEyeRenderDesc(UnityEngine.XR.XRNode)
extern void OVRDisplay_GetEyeRenderDesc_m9A9BF3B303AFBDEBA9FEC685CFE5724F0A023569 ();
// 0x00000065 OVRDisplay_LatencyData OVRDisplay::get_latency()
extern void OVRDisplay_get_latency_mF97233777C03673CB38F71B24A9ABE168CBB1F6D ();
// 0x00000066 System.Single OVRDisplay::get_appFramerate()
extern void OVRDisplay_get_appFramerate_m156E2F1F6C83C8064BBD24326F93CF570D745B61 ();
// 0x00000067 System.Int32 OVRDisplay::get_recommendedMSAALevel()
extern void OVRDisplay_get_recommendedMSAALevel_m13E1993FD97E59C0A8B23C5AA5EC31205507446E ();
// 0x00000068 System.Single[] OVRDisplay::get_displayFrequenciesAvailable()
extern void OVRDisplay_get_displayFrequenciesAvailable_m937677B39E10A1AD25D115ECF6BDCF9D04812CB0 ();
// 0x00000069 System.Single OVRDisplay::get_displayFrequency()
extern void OVRDisplay_get_displayFrequency_mACDD484A58B81BB7C3E96851613C89308717282B ();
// 0x0000006A System.Void OVRDisplay::set_displayFrequency(System.Single)
extern void OVRDisplay_set_displayFrequency_m036CE7305BF1DE7B8D8DC5FDC04AB172677910AD ();
// 0x0000006B System.Void OVRDisplay::UpdateTextures()
extern void OVRDisplay_UpdateTextures_mDA3E468B41D49311BD73E1BA8C9E46AB29383B28 ();
// 0x0000006C System.Void OVRDisplay::ConfigureEyeDesc(UnityEngine.XR.XRNode)
extern void OVRDisplay_ConfigureEyeDesc_mA4C0280800DFFFDA6F0283E543F69CAC5B514BC3 ();
// 0x0000006D System.Void OVRHaptics::.cctor()
extern void OVRHaptics__cctor_m9DF9E947A29AA3C612F1C869CC306A9F532FBCE1 ();
// 0x0000006E System.Void OVRHaptics::Process()
extern void OVRHaptics_Process_m8814A49BE847D61DDF750EBABF5E8E1EFE1DBF52 ();
// 0x0000006F System.Int32 OVRHaptics_Config::get_SampleRateHz()
extern void Config_get_SampleRateHz_mBF0C423BA5253D7BBD59966A4D014EAE8098CA77 ();
// 0x00000070 System.Void OVRHaptics_Config::set_SampleRateHz(System.Int32)
extern void Config_set_SampleRateHz_mB7B0370281303C33BE60EA0C25CBE246B87A0E98 ();
// 0x00000071 System.Int32 OVRHaptics_Config::get_SampleSizeInBytes()
extern void Config_get_SampleSizeInBytes_m259D30FE07F12B434FC30C4B6AE5E3D47B0C0D76 ();
// 0x00000072 System.Void OVRHaptics_Config::set_SampleSizeInBytes(System.Int32)
extern void Config_set_SampleSizeInBytes_m4F99AA7AD08E88E597A01E9F6921519F900941AB ();
// 0x00000073 System.Int32 OVRHaptics_Config::get_MinimumSafeSamplesQueued()
extern void Config_get_MinimumSafeSamplesQueued_mE066FA55BBAEEAC92868510A3C5063A29A0B79EF ();
// 0x00000074 System.Void OVRHaptics_Config::set_MinimumSafeSamplesQueued(System.Int32)
extern void Config_set_MinimumSafeSamplesQueued_m6CD4AFFB2FBECBCF1C0ABF207E1011B8715A13F5 ();
// 0x00000075 System.Int32 OVRHaptics_Config::get_MinimumBufferSamplesCount()
extern void Config_get_MinimumBufferSamplesCount_m452FBC24E773D6B2C06D1411B9DAECD64A9923BD ();
// 0x00000076 System.Void OVRHaptics_Config::set_MinimumBufferSamplesCount(System.Int32)
extern void Config_set_MinimumBufferSamplesCount_m1FA61F2F606765835140B4D66E134082FD50FA97 ();
// 0x00000077 System.Int32 OVRHaptics_Config::get_OptimalBufferSamplesCount()
extern void Config_get_OptimalBufferSamplesCount_m031A6BFCE9A0B0B5995607CD96E71DEBE7D4D945 ();
// 0x00000078 System.Void OVRHaptics_Config::set_OptimalBufferSamplesCount(System.Int32)
extern void Config_set_OptimalBufferSamplesCount_m770AC6C6834244350C4F41CC4BC502A8992AB2C4 ();
// 0x00000079 System.Int32 OVRHaptics_Config::get_MaximumBufferSamplesCount()
extern void Config_get_MaximumBufferSamplesCount_m8C5184AD7714B7B7A88A8746A76D61931E661033 ();
// 0x0000007A System.Void OVRHaptics_Config::set_MaximumBufferSamplesCount(System.Int32)
extern void Config_set_MaximumBufferSamplesCount_m6345579FC5904BEDB3D82CD294FC3D3C5B13F895 ();
// 0x0000007B System.Void OVRHaptics_Config::.cctor()
extern void Config__cctor_m6716E7312333D34F0299F1BDB711DBFC4641A575 ();
// 0x0000007C System.Void OVRHaptics_Config::Load()
extern void Config_Load_mE5387F81A3AA215CD143175C48A9A3E5ACB60492 ();
// 0x0000007D System.Void OVRHaptics_OVRHapticsChannel::.ctor(System.UInt32)
extern void OVRHapticsChannel__ctor_m3F533C69B7EB546D1708470EE9C185224FC3A70D ();
// 0x0000007E System.Void OVRHaptics_OVRHapticsChannel::Preempt(OVRHapticsClip)
extern void OVRHapticsChannel_Preempt_mF1F53F22F5E05C5E20416880E02BD2FC043B93FC ();
// 0x0000007F System.Void OVRHaptics_OVRHapticsChannel::Queue(OVRHapticsClip)
extern void OVRHapticsChannel_Queue_m89B54C06EF2FE831032BD6F15832BE945973F388 ();
// 0x00000080 System.Void OVRHaptics_OVRHapticsChannel::Mix(OVRHapticsClip)
extern void OVRHapticsChannel_Mix_m3B593B7720180C75E770E2A76220B9D0290AE812 ();
// 0x00000081 System.Void OVRHaptics_OVRHapticsChannel::Clear()
extern void OVRHapticsChannel_Clear_mD9A6B7CC4CDC7EE727E96795F3549BC87834189E ();
// 0x00000082 System.Void OVRHaptics_OVRHapticsOutput::.ctor(System.UInt32)
extern void OVRHapticsOutput__ctor_m70F2C4446301C2177BD75A0BDEDE397841D76278 ();
// 0x00000083 System.Void OVRHaptics_OVRHapticsOutput::Process()
extern void OVRHapticsOutput_Process_m06637C596160BA835A44B95E7D3FA2BECA1D6707 ();
// 0x00000084 System.Void OVRHaptics_OVRHapticsOutput::Preempt(OVRHapticsClip)
extern void OVRHapticsOutput_Preempt_mBEFCABBFA1BAC7B6033D8C02B2485782D4748529 ();
// 0x00000085 System.Void OVRHaptics_OVRHapticsOutput::Queue(OVRHapticsClip)
extern void OVRHapticsOutput_Queue_mD5289CCC1E33BFD3AD504DA878944AE07F593C04 ();
// 0x00000086 System.Void OVRHaptics_OVRHapticsOutput::Mix(OVRHapticsClip)
extern void OVRHapticsOutput_Mix_m1EC3BA93FF7B23DDD1997C18121C726C527DBCBF ();
// 0x00000087 System.Void OVRHaptics_OVRHapticsOutput::Clear()
extern void OVRHapticsOutput_Clear_m70F2303973A13B22AAEAC175A966B08374A2F86E ();
// 0x00000088 System.Int32 OVRHaptics_OVRHapticsOutput_ClipPlaybackTracker::get_ReadCount()
extern void ClipPlaybackTracker_get_ReadCount_mF6B8F438BC83BAF8953928DF54C62D559D57902B ();
// 0x00000089 System.Void OVRHaptics_OVRHapticsOutput_ClipPlaybackTracker::set_ReadCount(System.Int32)
extern void ClipPlaybackTracker_set_ReadCount_m6FFAA969B8F6ACD22FA89E9B7960A0089824B711 ();
// 0x0000008A OVRHapticsClip OVRHaptics_OVRHapticsOutput_ClipPlaybackTracker::get_Clip()
extern void ClipPlaybackTracker_get_Clip_m853021C2B86660BBCDB6B4241D7103779938AAD3 ();
// 0x0000008B System.Void OVRHaptics_OVRHapticsOutput_ClipPlaybackTracker::set_Clip(OVRHapticsClip)
extern void ClipPlaybackTracker_set_Clip_m5E3D284F25187523412EA3C584AA2A9DAC21286D ();
// 0x0000008C System.Void OVRHaptics_OVRHapticsOutput_ClipPlaybackTracker::.ctor(OVRHapticsClip)
extern void ClipPlaybackTracker__ctor_mA5F90D85B508A90E0EAF6A634EFF4575BFD8E37D ();
// 0x0000008D System.Int32 OVRHapticsClip::get_Count()
extern void OVRHapticsClip_get_Count_mFF27F17E532B73C234E0E5B479DBA050B1A28E36 ();
// 0x0000008E System.Void OVRHapticsClip::set_Count(System.Int32)
extern void OVRHapticsClip_set_Count_m0A00657439FFA2CB9F8E080054C1D071081A3D4B ();
// 0x0000008F System.Int32 OVRHapticsClip::get_Capacity()
extern void OVRHapticsClip_get_Capacity_m738B8C59B18E4D7A583BE5F68AA435846CB3B53E ();
// 0x00000090 System.Void OVRHapticsClip::set_Capacity(System.Int32)
extern void OVRHapticsClip_set_Capacity_m8C7FB4EE07E45E12A2098F5B4D50CE8602614586 ();
// 0x00000091 System.Byte[] OVRHapticsClip::get_Samples()
extern void OVRHapticsClip_get_Samples_mEE3E7A81631DBAAA1939D9FE85AC0228FE17657C ();
// 0x00000092 System.Void OVRHapticsClip::set_Samples(System.Byte[])
extern void OVRHapticsClip_set_Samples_m883BA52B9C589BE4F27DF47974A8BA4247E0ED09 ();
// 0x00000093 System.Void OVRHapticsClip::.ctor()
extern void OVRHapticsClip__ctor_m462AA3C640AC299CBF2D604FC37E1213A2C2E9A0 ();
// 0x00000094 System.Void OVRHapticsClip::.ctor(System.Int32)
extern void OVRHapticsClip__ctor_mAC3B8AC3F953F8F23B9BB1243922C2792EB831D1 ();
// 0x00000095 System.Void OVRHapticsClip::.ctor(System.Byte[],System.Int32)
extern void OVRHapticsClip__ctor_m785E6E5D522929D4626CD3F5F5CD53FABC4A8790 ();
// 0x00000096 System.Void OVRHapticsClip::.ctor(OVRHapticsClip,OVRHapticsClip)
extern void OVRHapticsClip__ctor_m445ECAF64EF748AFFA2392D1F126588ABA34D945 ();
// 0x00000097 System.Void OVRHapticsClip::.ctor(UnityEngine.AudioClip,System.Int32)
extern void OVRHapticsClip__ctor_m18B12192C79C99888A54E11DC1BD9168359EC923 ();
// 0x00000098 System.Void OVRHapticsClip::WriteSample(System.Byte)
extern void OVRHapticsClip_WriteSample_m5CC5D5DC610D8982A39AE91AA5C43FB707BA1C82 ();
// 0x00000099 System.Void OVRHapticsClip::Reset()
extern void OVRHapticsClip_Reset_mD788010F132114CE77F2B9CEAF3BB5C36BBF09B4 ();
// 0x0000009A System.Void OVRHapticsClip::InitializeFromAudioFloatTrack(System.Single[],System.Double,System.Int32,System.Int32)
extern void OVRHapticsClip_InitializeFromAudioFloatTrack_mCF4CDE21704B856DEB2C7732E077B0C2E478F3DE ();
// 0x0000009B System.Void OVRHeadsetEmulator::Start()
extern void OVRHeadsetEmulator_Start_m7636D5E2478971107B676C2343C62F4AF01C8FF0 ();
// 0x0000009C System.Void OVRHeadsetEmulator::Update()
extern void OVRHeadsetEmulator_Update_mF81AC116E9CC65B2146CB0349B6F9DE15141AB69 ();
// 0x0000009D System.Boolean OVRHeadsetEmulator::IsEmulationActivated()
extern void OVRHeadsetEmulator_IsEmulationActivated_m10420478F794D1C04409CD19070F19C7988352FB ();
// 0x0000009E System.Boolean OVRHeadsetEmulator::IsTweakingPitch()
extern void OVRHeadsetEmulator_IsTweakingPitch_mF66F169119F6E04E5FE0E36CE5AFA67F4C50FCB5 ();
// 0x0000009F System.Void OVRHeadsetEmulator::.ctor()
extern void OVRHeadsetEmulator__ctor_m101A2AF3FA27890F50AB1799392D7B0CCE3A5985 ();
// 0x000000A0 System.Boolean OVRInput::get_pluginSupportsActiveController()
extern void OVRInput_get_pluginSupportsActiveController_m2ACF44F093928F2A378164E3E907E1C807CFBB9A ();
// 0x000000A1 System.Void OVRInput::.cctor()
extern void OVRInput__cctor_m19971E781C5A234AE2025398292EE15A5D83ABE5 ();
// 0x000000A2 System.Void OVRInput::Update()
extern void OVRInput_Update_m19E90D8F3325C01FC2E65F178C7AC0696736029C ();
// 0x000000A3 System.Void OVRInput::FixedUpdate()
extern void OVRInput_FixedUpdate_mE9EF324767391BD7F2E7E64F1E585B8B6FC3AC95 ();
// 0x000000A4 System.Boolean OVRInput::GetControllerOrientationTracked(OVRInput_Controller)
extern void OVRInput_GetControllerOrientationTracked_m36A93929E07F1BF4CB2BAEEB983D5331FAAECC31 ();
// 0x000000A5 System.Boolean OVRInput::GetControllerOrientationValid(OVRInput_Controller)
extern void OVRInput_GetControllerOrientationValid_m04AE481585DB1ED9AAEB9B3F0BFC2F413AD5C1DA ();
// 0x000000A6 System.Boolean OVRInput::GetControllerPositionTracked(OVRInput_Controller)
extern void OVRInput_GetControllerPositionTracked_m2D57B21C46794F0E326C355A8DC2E9878D2D2E2B ();
// 0x000000A7 System.Boolean OVRInput::GetControllerPositionValid(OVRInput_Controller)
extern void OVRInput_GetControllerPositionValid_m5DC6A1D5FA46702CD062D030B6339B0C1B884441 ();
// 0x000000A8 UnityEngine.Vector3 OVRInput::GetLocalControllerPosition(OVRInput_Controller)
extern void OVRInput_GetLocalControllerPosition_mDCD6ACB9F6BF4FC874343A886E69A34AFC8E39F6 ();
// 0x000000A9 UnityEngine.Vector3 OVRInput::GetLocalControllerVelocity(OVRInput_Controller)
extern void OVRInput_GetLocalControllerVelocity_m04F1E6B032A84AEB635A049E15D734585E12FF73 ();
// 0x000000AA UnityEngine.Vector3 OVRInput::GetLocalControllerAcceleration(OVRInput_Controller)
extern void OVRInput_GetLocalControllerAcceleration_m2A23D042DD7FD454302B6993D006812FD59E6DE8 ();
// 0x000000AB UnityEngine.Quaternion OVRInput::GetLocalControllerRotation(OVRInput_Controller)
extern void OVRInput_GetLocalControllerRotation_m0467B2CFE1611F6AA8F8E8D8250D27579F349FD8 ();
// 0x000000AC UnityEngine.Vector3 OVRInput::GetLocalControllerAngularVelocity(OVRInput_Controller)
extern void OVRInput_GetLocalControllerAngularVelocity_m5C67927E58BC4A203574F4858F526D95F546B2B8 ();
// 0x000000AD UnityEngine.Vector3 OVRInput::GetLocalControllerAngularAcceleration(OVRInput_Controller)
extern void OVRInput_GetLocalControllerAngularAcceleration_mA8DFF34B1D387B426853A6302C42AFDE860FE5FA ();
// 0x000000AE OVRInput_Handedness OVRInput::GetDominantHand()
extern void OVRInput_GetDominantHand_mAF6886AEFE692A5FCB97D3DBA22AC9CD906FB7A4 ();
// 0x000000AF System.Boolean OVRInput::Get(OVRInput_Button,OVRInput_Controller)
extern void OVRInput_Get_mD28FEA6BB4143B1A877406F48673CBB344023FFC ();
// 0x000000B0 System.Boolean OVRInput::Get(OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_Get_mCB5EFCA078139909FD9A8B58966D87670A4F50E0 ();
// 0x000000B1 System.Boolean OVRInput::GetResolvedButton(OVRInput_Button,OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_GetResolvedButton_mC37E9C1B1B13411707B455B83004D5CAE8E84E3C ();
// 0x000000B2 System.Boolean OVRInput::GetDown(OVRInput_Button,OVRInput_Controller)
extern void OVRInput_GetDown_m805D5B9960D8101822F988DF59EEC907E05D913D ();
// 0x000000B3 System.Boolean OVRInput::GetDown(OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_GetDown_mDC1D4A7988D6F2D3A48694928EDEA563DB0F19C4 ();
// 0x000000B4 System.Boolean OVRInput::GetResolvedButtonDown(OVRInput_Button,OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_GetResolvedButtonDown_mA3A5EF314CFCA312AAD089FEFCC8829835FFDFC0 ();
// 0x000000B5 System.Boolean OVRInput::GetUp(OVRInput_Button,OVRInput_Controller)
extern void OVRInput_GetUp_mFB3BB8EB3324A21E68898ED1B7F9324E52AAB05C ();
// 0x000000B6 System.Boolean OVRInput::GetUp(OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_GetUp_mA446A2009E6DDB404DD45E8D4C3B7C8A1C2A397F ();
// 0x000000B7 System.Boolean OVRInput::GetResolvedButtonUp(OVRInput_Button,OVRInput_RawButton,OVRInput_Controller)
extern void OVRInput_GetResolvedButtonUp_m90BF79401470B006BB7E8F256FB18C78B482D4CC ();
// 0x000000B8 System.Boolean OVRInput::Get(OVRInput_Touch,OVRInput_Controller)
extern void OVRInput_Get_m43CCF5BB1A52704C658046C5D2C19EE072352E66 ();
// 0x000000B9 System.Boolean OVRInput::Get(OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_Get_mC093F91A35FA7896E4E37E1FBA2EBACAA2F894AC ();
// 0x000000BA System.Boolean OVRInput::GetResolvedTouch(OVRInput_Touch,OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedTouch_mA7BEE15AB735DC1FA8FC621F914D06B89DE8468B ();
// 0x000000BB System.Boolean OVRInput::GetDown(OVRInput_Touch,OVRInput_Controller)
extern void OVRInput_GetDown_m3D608EF75EA3CD314A39A19A22D987BFCB597519 ();
// 0x000000BC System.Boolean OVRInput::GetDown(OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_GetDown_m29719778A02F51D20DF391CA28EB8335D9BE23B1 ();
// 0x000000BD System.Boolean OVRInput::GetResolvedTouchDown(OVRInput_Touch,OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedTouchDown_m417EB539C97A7D510D3AD4E9D9EDB2B002F75905 ();
// 0x000000BE System.Boolean OVRInput::GetUp(OVRInput_Touch,OVRInput_Controller)
extern void OVRInput_GetUp_m697C489A998DD810D9EF7D39E0194E450FA5B320 ();
// 0x000000BF System.Boolean OVRInput::GetUp(OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_GetUp_m1EC06070E9878F4E9CADB310A25E41350C2747B2 ();
// 0x000000C0 System.Boolean OVRInput::GetResolvedTouchUp(OVRInput_Touch,OVRInput_RawTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedTouchUp_m897FF6CF2A3B3EE8F7451B69A028E3D41FF0A806 ();
// 0x000000C1 System.Boolean OVRInput::Get(OVRInput_NearTouch,OVRInput_Controller)
extern void OVRInput_Get_m413113B33BF8B78355A66B1727DA852CF752CD97 ();
// 0x000000C2 System.Boolean OVRInput::Get(OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_Get_mBA882DF627E4D7E25A82B7A721ECC62F259755DB ();
// 0x000000C3 System.Boolean OVRInput::GetResolvedNearTouch(OVRInput_NearTouch,OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedNearTouch_mCE99ED1E354E6A21AC4CDB07370C958B6AB55663 ();
// 0x000000C4 System.Boolean OVRInput::GetDown(OVRInput_NearTouch,OVRInput_Controller)
extern void OVRInput_GetDown_m36BC5C370D1BBD98FEA0FDD6579505DB42772BE0 ();
// 0x000000C5 System.Boolean OVRInput::GetDown(OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_GetDown_m376282F1966F78C46E15612F9250975FA7BB0509 ();
// 0x000000C6 System.Boolean OVRInput::GetResolvedNearTouchDown(OVRInput_NearTouch,OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedNearTouchDown_m9651F25FFB006FA4BEF9227A57DEDEAAC2EA55A9 ();
// 0x000000C7 System.Boolean OVRInput::GetUp(OVRInput_NearTouch,OVRInput_Controller)
extern void OVRInput_GetUp_m942CDC17B0164FD8EDF5CBB2AF644E1E8897AC1D ();
// 0x000000C8 System.Boolean OVRInput::GetUp(OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_GetUp_mD490A8A3593F63204C87FB807703FA804099B445 ();
// 0x000000C9 System.Boolean OVRInput::GetResolvedNearTouchUp(OVRInput_NearTouch,OVRInput_RawNearTouch,OVRInput_Controller)
extern void OVRInput_GetResolvedNearTouchUp_m67F1F00BF2EC34CE666BD793DC9BBB69F4B21969 ();
// 0x000000CA System.Single OVRInput::Get(OVRInput_Axis1D,OVRInput_Controller)
extern void OVRInput_Get_m639D392EA29D4E11C99642F9E76C6DF91203ECD6 ();
// 0x000000CB System.Single OVRInput::Get(OVRInput_RawAxis1D,OVRInput_Controller)
extern void OVRInput_Get_m194F10CB40430380DBA8EA7272846A8C4E91ABB7 ();
// 0x000000CC System.Single OVRInput::GetResolvedAxis1D(OVRInput_Axis1D,OVRInput_RawAxis1D,OVRInput_Controller)
extern void OVRInput_GetResolvedAxis1D_m585EDDA8BA7086FBDC28F28EE1F0466CE1EB3429 ();
// 0x000000CD UnityEngine.Vector2 OVRInput::Get(OVRInput_Axis2D,OVRInput_Controller)
extern void OVRInput_Get_m6921274099CAB813D71B3D7D6E1C7AF414A55FCC ();
// 0x000000CE UnityEngine.Vector2 OVRInput::Get(OVRInput_RawAxis2D,OVRInput_Controller)
extern void OVRInput_Get_mBCE80A6B8532C4AC77BA3FBE2C4CDB6024031C96 ();
// 0x000000CF UnityEngine.Vector2 OVRInput::GetResolvedAxis2D(OVRInput_Axis2D,OVRInput_RawAxis2D,OVRInput_Controller)
extern void OVRInput_GetResolvedAxis2D_mE6EA9C261D90F1666384BCB29C1F93E2A129ED97 ();
// 0x000000D0 OVRInput_Controller OVRInput::GetConnectedControllers()
extern void OVRInput_GetConnectedControllers_mC0DF5B3EA8741EB53BED9F0063D7A3C91864CA09 ();
// 0x000000D1 System.Boolean OVRInput::IsControllerConnected(OVRInput_Controller)
extern void OVRInput_IsControllerConnected_m5926F35AD2016332730E587700D23E81A0302456 ();
// 0x000000D2 OVRInput_Controller OVRInput::GetActiveController()
extern void OVRInput_GetActiveController_m17A3D1B2B0A46E835D951A7621D04EE4442F67F9 ();
// 0x000000D3 System.Void OVRInput::StartVibration(System.Single,System.Single,UnityEngine.XR.XRNode)
extern void OVRInput_StartVibration_m25C2642BE6B502BD173A425D1D3F5B4B0FDD61A3 ();
// 0x000000D4 System.Void OVRInput::SetOpenVRLocalPose(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void OVRInput_SetOpenVRLocalPose_mA90EAE89BE10A18F8F32C09327A6F72CF26277A0 ();
// 0x000000D5 System.String OVRInput::GetOpenVRStringProperty(OVR.OpenVR.ETrackedDeviceProperty,System.UInt32)
extern void OVRInput_GetOpenVRStringProperty_m44EA467A648A64B3418B0B11FDD634D50E4A0EEB ();
// 0x000000D6 System.Void OVRInput::UpdateXRControllerNodeIds()
extern void OVRInput_UpdateXRControllerNodeIds_m17AD21351586EAE8ED47EE28F720DD3A6C4B131B ();
// 0x000000D7 System.Void OVRInput::UpdateXRControllerHaptics()
extern void OVRInput_UpdateXRControllerHaptics_m7A9B2471150B3F16ABE193022BE2D41E522B10BF ();
// 0x000000D8 System.Void OVRInput::InitHapticInfo()
extern void OVRInput_InitHapticInfo_m10DCFBD4735EB9DEFCC262E44A5AA90CB42AE188 ();
// 0x000000D9 System.Void OVRInput::PlayHapticImpulse(System.Single,UnityEngine.XR.XRNode)
extern void OVRInput_PlayHapticImpulse_mE640A5FDEE1025CA8EEBEC53B9AFAD4651290A7D ();
// 0x000000DA System.Boolean OVRInput::IsValidOpenVRDevice(System.UInt32)
extern void OVRInput_IsValidOpenVRDevice_mF37CE426C4EFA9CE96B4A4215FD5E8BA7A62B23D ();
// 0x000000DB System.Void OVRInput::SetControllerVibration(System.Single,System.Single,OVRInput_Controller)
extern void OVRInput_SetControllerVibration_m6734A248BD2EFC5AFBA4C3A177EC9B6EC3256D0F ();
// 0x000000DC System.Void OVRInput::RecenterController(OVRInput_Controller)
extern void OVRInput_RecenterController_m4A36B6A7278DBFFFF918AC004819272E26B6019D ();
// 0x000000DD System.Boolean OVRInput::GetControllerWasRecentered(OVRInput_Controller)
extern void OVRInput_GetControllerWasRecentered_m692BB0BF3EB419831CB53D7A5453617797132C61 ();
// 0x000000DE System.Byte OVRInput::GetControllerRecenterCount(OVRInput_Controller)
extern void OVRInput_GetControllerRecenterCount_mD4E9A21DC59032AD0C8113BDBF195BACD49773C5 ();
// 0x000000DF System.Byte OVRInput::GetControllerBatteryPercentRemaining(OVRInput_Controller)
extern void OVRInput_GetControllerBatteryPercentRemaining_m6FBF249F51F349481DF9F1074B372CBF6F9B34D6 ();
// 0x000000E0 UnityEngine.Vector2 OVRInput::CalculateAbsMax(UnityEngine.Vector2,UnityEngine.Vector2)
extern void OVRInput_CalculateAbsMax_mF55A311F254D4D3889E55F3B1130407453B0E6A1 ();
// 0x000000E1 System.Single OVRInput::CalculateAbsMax(System.Single,System.Single)
extern void OVRInput_CalculateAbsMax_mDD5FA1CDB516CD0A6044BAD5B469D95FAD6267C3 ();
// 0x000000E2 UnityEngine.Vector2 OVRInput::CalculateDeadzone(UnityEngine.Vector2,System.Single)
extern void OVRInput_CalculateDeadzone_m3EC8CDDB2E92CF1F1C8CCBADD79F4B6A6A54DF6E ();
// 0x000000E3 System.Single OVRInput::CalculateDeadzone(System.Single,System.Single)
extern void OVRInput_CalculateDeadzone_m85949E1D42104EDA26E88530689F8B81CB0FA87E ();
// 0x000000E4 System.Boolean OVRInput::ShouldResolveController(OVRInput_Controller,OVRInput_Controller)
extern void OVRInput_ShouldResolveController_m2B8792E11DFE911D0ED0EF4C60BC27767364C9AA ();
// 0x000000E5 System.Void OVRInput_HapticInfo::.ctor()
extern void HapticInfo__ctor_m2AC0E4C71FE1DE5975493EF29DCE3C0A3F41EC8F ();
// 0x000000E6 System.Void OVRInput_OVRControllerBase::.ctor()
extern void OVRControllerBase__ctor_mC61EA8CE2DD4906389E3F467AF5FE4DFEE0BC48D ();
// 0x000000E7 OVRInput_Controller OVRInput_OVRControllerBase::Update()
extern void OVRControllerBase_Update_m27794216EF2A070CA5AF2C4C24AE9D0FD55FD74B ();
// 0x000000E8 OVRPlugin_ControllerState4 OVRInput_OVRControllerBase::GetOpenVRControllerState(OVRInput_Controller)
extern void OVRControllerBase_GetOpenVRControllerState_m8953A760790660F1FC3F42CF8B17B8714D42D9D6 ();
// 0x000000E9 System.Void OVRInput_OVRControllerBase::SetControllerVibration(System.Single,System.Single)
extern void OVRControllerBase_SetControllerVibration_mDCE94CF986E07423019EB4EE8D0B6C90FFCCA8F4 ();
// 0x000000EA System.Void OVRInput_OVRControllerBase::RecenterController()
extern void OVRControllerBase_RecenterController_mC32586773B91FD247CAB858014E6753589BADA68 ();
// 0x000000EB System.Boolean OVRInput_OVRControllerBase::WasRecentered()
extern void OVRControllerBase_WasRecentered_m655756A76C186374606C68B865F59667B5C1D7FB ();
// 0x000000EC System.Byte OVRInput_OVRControllerBase::GetRecenterCount()
extern void OVRControllerBase_GetRecenterCount_m75F810E920FA08F533D0CD1E1CB1A932A54C7798 ();
// 0x000000ED System.Byte OVRInput_OVRControllerBase::GetBatteryPercentRemaining()
extern void OVRControllerBase_GetBatteryPercentRemaining_mED881266E425128D8E3DB24006BC516F0363E2CF ();
// 0x000000EE System.Void OVRInput_OVRControllerBase::ConfigureButtonMap()
// 0x000000EF System.Void OVRInput_OVRControllerBase::ConfigureTouchMap()
// 0x000000F0 System.Void OVRInput_OVRControllerBase::ConfigureNearTouchMap()
// 0x000000F1 System.Void OVRInput_OVRControllerBase::ConfigureAxis1DMap()
// 0x000000F2 System.Void OVRInput_OVRControllerBase::ConfigureAxis2DMap()
// 0x000000F3 OVRInput_RawButton OVRInput_OVRControllerBase::ResolveToRawMask(OVRInput_Button)
extern void OVRControllerBase_ResolveToRawMask_mC1D679AFFD98340F053FDAA0568B49022CE49EDA ();
// 0x000000F4 OVRInput_RawTouch OVRInput_OVRControllerBase::ResolveToRawMask(OVRInput_Touch)
extern void OVRControllerBase_ResolveToRawMask_m28A50F0D28D6D52C2DABC5BE41F4E1EB062B8CF8 ();
// 0x000000F5 OVRInput_RawNearTouch OVRInput_OVRControllerBase::ResolveToRawMask(OVRInput_NearTouch)
extern void OVRControllerBase_ResolveToRawMask_m56C9D177DC239D712089F901DA3E65DAB89AF6F4 ();
// 0x000000F6 OVRInput_RawAxis1D OVRInput_OVRControllerBase::ResolveToRawMask(OVRInput_Axis1D)
extern void OVRControllerBase_ResolveToRawMask_m8F5D2916E82BE22D3234101127E8919EE1C08449 ();
// 0x000000F7 OVRInput_RawAxis2D OVRInput_OVRControllerBase::ResolveToRawMask(OVRInput_Axis2D)
extern void OVRControllerBase_ResolveToRawMask_m3466F65C406A1CDCB0E4AEEB874CA29C3D147098 ();
// 0x000000F8 OVRInput_RawButton OVRInput_OVRControllerBase_VirtualButtonMap::ToRawMask(OVRInput_Button)
extern void VirtualButtonMap_ToRawMask_m73472FEC045C735D83E7B42366A9F88C609600F8 ();
// 0x000000F9 System.Void OVRInput_OVRControllerBase_VirtualButtonMap::.ctor()
extern void VirtualButtonMap__ctor_m91D2F6BBF0841690403F0BC66444AAC132B34A09 ();
// 0x000000FA OVRInput_RawTouch OVRInput_OVRControllerBase_VirtualTouchMap::ToRawMask(OVRInput_Touch)
extern void VirtualTouchMap_ToRawMask_m094CB0CBEC27C7FD7FBC3C9E47FD719682C85F23 ();
// 0x000000FB System.Void OVRInput_OVRControllerBase_VirtualTouchMap::.ctor()
extern void VirtualTouchMap__ctor_mF23DBA22DA140E2A9BA4F9AB481F8135426061F9 ();
// 0x000000FC OVRInput_RawNearTouch OVRInput_OVRControllerBase_VirtualNearTouchMap::ToRawMask(OVRInput_NearTouch)
extern void VirtualNearTouchMap_ToRawMask_m2B92568DC4E3CB92BD18F87F02078910C2BFB8E2 ();
// 0x000000FD System.Void OVRInput_OVRControllerBase_VirtualNearTouchMap::.ctor()
extern void VirtualNearTouchMap__ctor_m5D70FF2C55B4C3773C3E6305BE7B7BEBDA800059 ();
// 0x000000FE OVRInput_RawAxis1D OVRInput_OVRControllerBase_VirtualAxis1DMap::ToRawMask(OVRInput_Axis1D)
extern void VirtualAxis1DMap_ToRawMask_mF1B4DD9DC1D6FFC2B562596B1F96069A79B58FB5 ();
// 0x000000FF System.Void OVRInput_OVRControllerBase_VirtualAxis1DMap::.ctor()
extern void VirtualAxis1DMap__ctor_mC2E71412548C06875EEC0BC2AD0431908FAF7EA7 ();
// 0x00000100 OVRInput_RawAxis2D OVRInput_OVRControllerBase_VirtualAxis2DMap::ToRawMask(OVRInput_Axis2D)
extern void VirtualAxis2DMap_ToRawMask_mB7459D93F899F93E870E4AD215EC04407002F295 ();
// 0x00000101 System.Void OVRInput_OVRControllerBase_VirtualAxis2DMap::.ctor()
extern void VirtualAxis2DMap__ctor_mECF285D0A2576F6642A78D9654ED0DBE656B1A8E ();
// 0x00000102 System.Void OVRInput_OVRControllerTouch::.ctor()
extern void OVRControllerTouch__ctor_mA43758C56797259BCE9F4D7C529CEFD8F52078ED ();
// 0x00000103 System.Void OVRInput_OVRControllerTouch::ConfigureButtonMap()
extern void OVRControllerTouch_ConfigureButtonMap_mD1B10FF42D46FE817E933D0BF4691687939937A7 ();
// 0x00000104 System.Void OVRInput_OVRControllerTouch::ConfigureTouchMap()
extern void OVRControllerTouch_ConfigureTouchMap_mFAB4DDE8D41C37EC8E7D24091833DA5FF68C3B0E ();
// 0x00000105 System.Void OVRInput_OVRControllerTouch::ConfigureNearTouchMap()
extern void OVRControllerTouch_ConfigureNearTouchMap_m5AD6C9CDA7616942F0E24493325C6E83DCCB89DE ();
// 0x00000106 System.Void OVRInput_OVRControllerTouch::ConfigureAxis1DMap()
extern void OVRControllerTouch_ConfigureAxis1DMap_m4553C500FA0236D5CF83C877B833E3C67117C9AF ();
// 0x00000107 System.Void OVRInput_OVRControllerTouch::ConfigureAxis2DMap()
extern void OVRControllerTouch_ConfigureAxis2DMap_m93FF7E484219B989494C36984EC2D39CE9EFD82B ();
// 0x00000108 System.Boolean OVRInput_OVRControllerTouch::WasRecentered()
extern void OVRControllerTouch_WasRecentered_mAC524F0A26C49E713FFC8D8E13B3EBE02370EFC3 ();
// 0x00000109 System.Byte OVRInput_OVRControllerTouch::GetRecenterCount()
extern void OVRControllerTouch_GetRecenterCount_m668DCE21A36FF447AB5D722057FCFA4F5605EC0B ();
// 0x0000010A System.Byte OVRInput_OVRControllerTouch::GetBatteryPercentRemaining()
extern void OVRControllerTouch_GetBatteryPercentRemaining_mAD022892B71FE08B2C16EF59985F67EA349B54DC ();
// 0x0000010B System.Void OVRInput_OVRControllerLTouch::.ctor()
extern void OVRControllerLTouch__ctor_m12AD4A8D7B38554CFBA3077D778F59019E7DAC74 ();
// 0x0000010C System.Void OVRInput_OVRControllerLTouch::ConfigureButtonMap()
extern void OVRControllerLTouch_ConfigureButtonMap_m6FF799D1CBBA6F1853244678CD34992919AE853B ();
// 0x0000010D System.Void OVRInput_OVRControllerLTouch::ConfigureTouchMap()
extern void OVRControllerLTouch_ConfigureTouchMap_mCC0AD41347F6B06E8DC52B60BD37404EAE3FACAF ();
// 0x0000010E System.Void OVRInput_OVRControllerLTouch::ConfigureNearTouchMap()
extern void OVRControllerLTouch_ConfigureNearTouchMap_m0E6486776C6DF677B4315BECE9B6FEC331BEA877 ();
// 0x0000010F System.Void OVRInput_OVRControllerLTouch::ConfigureAxis1DMap()
extern void OVRControllerLTouch_ConfigureAxis1DMap_mAAAC3E079E1AFC08C7663D22675422C453C74366 ();
// 0x00000110 System.Void OVRInput_OVRControllerLTouch::ConfigureAxis2DMap()
extern void OVRControllerLTouch_ConfigureAxis2DMap_m09CAFBC7E6319F219ECCE07B18CDD56F259DECCA ();
// 0x00000111 System.Boolean OVRInput_OVRControllerLTouch::WasRecentered()
extern void OVRControllerLTouch_WasRecentered_mA4C03A56F099EDBD98115FB60C006608C8C1AF42 ();
// 0x00000112 System.Byte OVRInput_OVRControllerLTouch::GetRecenterCount()
extern void OVRControllerLTouch_GetRecenterCount_m39D20BE9234BD8445017409D77B2C8DDC67A7C3E ();
// 0x00000113 System.Byte OVRInput_OVRControllerLTouch::GetBatteryPercentRemaining()
extern void OVRControllerLTouch_GetBatteryPercentRemaining_mD3D63FB2AF59D15C73D17738C389140E9253D69E ();
// 0x00000114 System.Void OVRInput_OVRControllerRTouch::.ctor()
extern void OVRControllerRTouch__ctor_mF66E59D83609DEA496BA3606D91F26202E259FC6 ();
// 0x00000115 System.Void OVRInput_OVRControllerRTouch::ConfigureButtonMap()
extern void OVRControllerRTouch_ConfigureButtonMap_m7958BB0DE880F2B2E3098F0DA168987AC931A274 ();
// 0x00000116 System.Void OVRInput_OVRControllerRTouch::ConfigureTouchMap()
extern void OVRControllerRTouch_ConfigureTouchMap_m88249F7C4C3593162BB91859B527164112FCCF71 ();
// 0x00000117 System.Void OVRInput_OVRControllerRTouch::ConfigureNearTouchMap()
extern void OVRControllerRTouch_ConfigureNearTouchMap_m9B1420FD72420D684B4C2FFB4EF15232FA667C84 ();
// 0x00000118 System.Void OVRInput_OVRControllerRTouch::ConfigureAxis1DMap()
extern void OVRControllerRTouch_ConfigureAxis1DMap_m16CEFBE7803D7B38D6BC3D2A81D6FBDD0019074B ();
// 0x00000119 System.Void OVRInput_OVRControllerRTouch::ConfigureAxis2DMap()
extern void OVRControllerRTouch_ConfigureAxis2DMap_mDBCB76A35337C03FDFB1CFD3D971C8886F33CC02 ();
// 0x0000011A System.Boolean OVRInput_OVRControllerRTouch::WasRecentered()
extern void OVRControllerRTouch_WasRecentered_m5FFD35A0180676838AB4C9CE812AC5B938DC5241 ();
// 0x0000011B System.Byte OVRInput_OVRControllerRTouch::GetRecenterCount()
extern void OVRControllerRTouch_GetRecenterCount_mC09383A499D96705E2BF233164F483A3A19B4A09 ();
// 0x0000011C System.Byte OVRInput_OVRControllerRTouch::GetBatteryPercentRemaining()
extern void OVRControllerRTouch_GetBatteryPercentRemaining_mEBA855E4F847F39EC91C6D5B1AC2D8F485DCDAF8 ();
// 0x0000011D System.Void OVRInput_OVRControllerRemote::.ctor()
extern void OVRControllerRemote__ctor_m109415A5F44DC56832825616A38EB3738FC6D182 ();
// 0x0000011E System.Void OVRInput_OVRControllerRemote::ConfigureButtonMap()
extern void OVRControllerRemote_ConfigureButtonMap_m6F89457937147957363A4E7AC739035E5436D4F9 ();
// 0x0000011F System.Void OVRInput_OVRControllerRemote::ConfigureTouchMap()
extern void OVRControllerRemote_ConfigureTouchMap_m7F2AFF45A027F262A3970CB06D41D39E0181FB87 ();
// 0x00000120 System.Void OVRInput_OVRControllerRemote::ConfigureNearTouchMap()
extern void OVRControllerRemote_ConfigureNearTouchMap_m83CA46F9271530342618A6EF171B3374DDFF4E8B ();
// 0x00000121 System.Void OVRInput_OVRControllerRemote::ConfigureAxis1DMap()
extern void OVRControllerRemote_ConfigureAxis1DMap_m8FCB212D322378698DD5D4D1DCEFCF19F64538FE ();
// 0x00000122 System.Void OVRInput_OVRControllerRemote::ConfigureAxis2DMap()
extern void OVRControllerRemote_ConfigureAxis2DMap_mF36EB1D487A60F43B94EF80A849F3E4D6F9BE82D ();
// 0x00000123 System.Void OVRInput_OVRControllerGamepadPC::.ctor()
extern void OVRControllerGamepadPC__ctor_m0692683EFA29CF07FE60D0EB444C4BBC3EF4B306 ();
// 0x00000124 System.Void OVRInput_OVRControllerGamepadPC::ConfigureButtonMap()
extern void OVRControllerGamepadPC_ConfigureButtonMap_mDD4BA7B9D6B9A1C68E45087DD300050DEB2251EE ();
// 0x00000125 System.Void OVRInput_OVRControllerGamepadPC::ConfigureTouchMap()
extern void OVRControllerGamepadPC_ConfigureTouchMap_m3A0F0D78BB82051F629330F612738A570B1C9773 ();
// 0x00000126 System.Void OVRInput_OVRControllerGamepadPC::ConfigureNearTouchMap()
extern void OVRControllerGamepadPC_ConfigureNearTouchMap_mB18F06ACD74B6D09C37B14E8F21B80A1E076EFCC ();
// 0x00000127 System.Void OVRInput_OVRControllerGamepadPC::ConfigureAxis1DMap()
extern void OVRControllerGamepadPC_ConfigureAxis1DMap_m817FFC0996AFDCFACE326BF0EA7BC20568CF373F ();
// 0x00000128 System.Void OVRInput_OVRControllerGamepadPC::ConfigureAxis2DMap()
extern void OVRControllerGamepadPC_ConfigureAxis2DMap_m427D1273FFDFED3F46147BAE1809C0E8891627A5 ();
// 0x00000129 System.Void OVRInput_OVRControllerGamepadMac::.ctor()
extern void OVRControllerGamepadMac__ctor_m54AB63055C95C03C97B12333D720ED1F84D40097 ();
// 0x0000012A System.Void OVRInput_OVRControllerGamepadMac::Finalize()
extern void OVRControllerGamepadMac_Finalize_m36D11AC862B929B5FE372A13A2A291AEA7636EDA ();
// 0x0000012B OVRInput_Controller OVRInput_OVRControllerGamepadMac::Update()
extern void OVRControllerGamepadMac_Update_mDD21EF016E4A1B57DE14173982338CCF94D929C6 ();
// 0x0000012C System.Void OVRInput_OVRControllerGamepadMac::ConfigureButtonMap()
extern void OVRControllerGamepadMac_ConfigureButtonMap_mCE03DC1A1ECCFD186868B2AEE6AADEDED3EFFC1E ();
// 0x0000012D System.Void OVRInput_OVRControllerGamepadMac::ConfigureTouchMap()
extern void OVRControllerGamepadMac_ConfigureTouchMap_m7EC7EA82109A070C03711FE8D69092A7F6293FA8 ();
// 0x0000012E System.Void OVRInput_OVRControllerGamepadMac::ConfigureNearTouchMap()
extern void OVRControllerGamepadMac_ConfigureNearTouchMap_mEC77E2F0AFA6B1679736D3E727D1C4A1A016DD8F ();
// 0x0000012F System.Void OVRInput_OVRControllerGamepadMac::ConfigureAxis1DMap()
extern void OVRControllerGamepadMac_ConfigureAxis1DMap_m21151EF89394B9DC6907CEC38321D3B7E3518D70 ();
// 0x00000130 System.Void OVRInput_OVRControllerGamepadMac::ConfigureAxis2DMap()
extern void OVRControllerGamepadMac_ConfigureAxis2DMap_mCB1452FC11D0D7E89A7D7BCCAFB834AD8731A1AC ();
// 0x00000131 System.Void OVRInput_OVRControllerGamepadMac::SetControllerVibration(System.Single,System.Single)
extern void OVRControllerGamepadMac_SetControllerVibration_mD3626EBE9E415225B1F1B4D06B07F9CA927AA7DF ();
// 0x00000132 System.Boolean OVRInput_OVRControllerGamepadMac::OVR_GamepadController_Initialize()
extern void OVRControllerGamepadMac_OVR_GamepadController_Initialize_mC3F87E73A0875F44308E1AF143EF6093C7A05718 ();
// 0x00000133 System.Boolean OVRInput_OVRControllerGamepadMac::OVR_GamepadController_Destroy()
extern void OVRControllerGamepadMac_OVR_GamepadController_Destroy_m0F9878AD54374802864F9DB7E57359CE5AB77963 ();
// 0x00000134 System.Boolean OVRInput_OVRControllerGamepadMac::OVR_GamepadController_Update()
extern void OVRControllerGamepadMac_OVR_GamepadController_Update_mC939AE53F198C060AE7B343D7D74C2A990F0C176 ();
// 0x00000135 System.Single OVRInput_OVRControllerGamepadMac::OVR_GamepadController_GetAxis(System.Int32)
extern void OVRControllerGamepadMac_OVR_GamepadController_GetAxis_mB2D78FB3966B2DDDB99831FAC76B47D23FA5D996 ();
// 0x00000136 System.Boolean OVRInput_OVRControllerGamepadMac::OVR_GamepadController_GetButton(System.Int32)
extern void OVRControllerGamepadMac_OVR_GamepadController_GetButton_m3C07A110FF9C92A693C351F9B94EA891AA007565 ();
// 0x00000137 System.Boolean OVRInput_OVRControllerGamepadMac::OVR_GamepadController_SetVibration(System.Int32,System.Single,System.Single)
extern void OVRControllerGamepadMac_OVR_GamepadController_SetVibration_m30AB24FD661F15E84E79643FD14A5F507B940590 ();
// 0x00000138 System.Void OVRInput_OVRControllerGamepadAndroid::.ctor()
extern void OVRControllerGamepadAndroid__ctor_m17602A15B0B4544FF9A2DCE363E39CD369225DC6 ();
// 0x00000139 System.Void OVRInput_OVRControllerGamepadAndroid::ConfigureButtonMap()
extern void OVRControllerGamepadAndroid_ConfigureButtonMap_mE64446D3642873D041A56425B091484B85752308 ();
// 0x0000013A System.Void OVRInput_OVRControllerGamepadAndroid::ConfigureTouchMap()
extern void OVRControllerGamepadAndroid_ConfigureTouchMap_m800F9772FF3BFA25FEBC4C5F2733F0DADBD13439 ();
// 0x0000013B System.Void OVRInput_OVRControllerGamepadAndroid::ConfigureNearTouchMap()
extern void OVRControllerGamepadAndroid_ConfigureNearTouchMap_m5F41360FF1808E055E98ABC5EF878F2113682F2B ();
// 0x0000013C System.Void OVRInput_OVRControllerGamepadAndroid::ConfigureAxis1DMap()
extern void OVRControllerGamepadAndroid_ConfigureAxis1DMap_m8ABE1969BC577DF9EA362EABB42FFBE62E6B7EAA ();
// 0x0000013D System.Void OVRInput_OVRControllerGamepadAndroid::ConfigureAxis2DMap()
extern void OVRControllerGamepadAndroid_ConfigureAxis2DMap_mBF4F5863E1DBF9637B6CAE0520B255049C6F3A3C ();
// 0x0000013E System.Void OVRInput_OVRControllerTouchpad::.ctor()
extern void OVRControllerTouchpad__ctor_m81F247874C230DDC51359A0FD54042D3332E8562 ();
// 0x0000013F OVRInput_Controller OVRInput_OVRControllerTouchpad::Update()
extern void OVRControllerTouchpad_Update_m091CC1C96998A2C17A77A7FC1BF70B9BEED14A44 ();
// 0x00000140 System.Void OVRInput_OVRControllerTouchpad::ConfigureButtonMap()
extern void OVRControllerTouchpad_ConfigureButtonMap_mA7CA49570E1A11F53449720A16FEAA229ED31F9E ();
// 0x00000141 System.Void OVRInput_OVRControllerTouchpad::ConfigureTouchMap()
extern void OVRControllerTouchpad_ConfigureTouchMap_mFF75A52F9BCBB7B23DD59A8212C1DB00CCF94101 ();
// 0x00000142 System.Void OVRInput_OVRControllerTouchpad::ConfigureNearTouchMap()
extern void OVRControllerTouchpad_ConfigureNearTouchMap_m585F637EA858CF519EDF840083BF3504A6F0A430 ();
// 0x00000143 System.Void OVRInput_OVRControllerTouchpad::ConfigureAxis1DMap()
extern void OVRControllerTouchpad_ConfigureAxis1DMap_mEC0E4E2F63B992D574DAFBC7D66FD0BD59EE5EFA ();
// 0x00000144 System.Void OVRInput_OVRControllerTouchpad::ConfigureAxis2DMap()
extern void OVRControllerTouchpad_ConfigureAxis2DMap_m15284CBA6876042FEE66FC7455556E554C4C090F ();
// 0x00000145 System.Void OVRInput_OVRControllerLTrackedRemote::.ctor()
extern void OVRControllerLTrackedRemote__ctor_m6799E35FD41D60E71FEA8139DA8047F793739B69 ();
// 0x00000146 System.Void OVRInput_OVRControllerLTrackedRemote::ConfigureButtonMap()
extern void OVRControllerLTrackedRemote_ConfigureButtonMap_m257EDE02B7F8A44487C764D91AE90E853DE62BEB ();
// 0x00000147 System.Void OVRInput_OVRControllerLTrackedRemote::ConfigureTouchMap()
extern void OVRControllerLTrackedRemote_ConfigureTouchMap_mB19AB374891403DCD56BD58E25F3FA00AF35A3F8 ();
// 0x00000148 System.Void OVRInput_OVRControllerLTrackedRemote::ConfigureNearTouchMap()
extern void OVRControllerLTrackedRemote_ConfigureNearTouchMap_mFDCE0EEF2B757E91CEC0AAC4103CCA3FD0B94D87 ();
// 0x00000149 System.Void OVRInput_OVRControllerLTrackedRemote::ConfigureAxis1DMap()
extern void OVRControllerLTrackedRemote_ConfigureAxis1DMap_m5324359AD385BEA973DA9A2B24B34D0CD0E0AA2B ();
// 0x0000014A System.Void OVRInput_OVRControllerLTrackedRemote::ConfigureAxis2DMap()
extern void OVRControllerLTrackedRemote_ConfigureAxis2DMap_m495F7DC43643D14CB753E741A8A5A8D3BB92BA49 ();
// 0x0000014B OVRInput_Controller OVRInput_OVRControllerLTrackedRemote::Update()
extern void OVRControllerLTrackedRemote_Update_m20B0A6221715252920A412F0BFEBBBFD120FA532 ();
// 0x0000014C System.Boolean OVRInput_OVRControllerLTrackedRemote::WasRecentered()
extern void OVRControllerLTrackedRemote_WasRecentered_mE4FD973E87306C52219AFA4DF28AC2114D8F8400 ();
// 0x0000014D System.Byte OVRInput_OVRControllerLTrackedRemote::GetRecenterCount()
extern void OVRControllerLTrackedRemote_GetRecenterCount_mF45BE77D31321F1AF465B22F351BB7E5F83F2571 ();
// 0x0000014E System.Byte OVRInput_OVRControllerLTrackedRemote::GetBatteryPercentRemaining()
extern void OVRControllerLTrackedRemote_GetBatteryPercentRemaining_m37CD67AE9F6A6C3F75A37577D3B5782FE56A0FBF ();
// 0x0000014F System.Void OVRInput_OVRControllerRTrackedRemote::.ctor()
extern void OVRControllerRTrackedRemote__ctor_m321A13B4807FBA5D9FCD154D65C48B6768834E74 ();
// 0x00000150 System.Void OVRInput_OVRControllerRTrackedRemote::ConfigureButtonMap()
extern void OVRControllerRTrackedRemote_ConfigureButtonMap_mACE34F2CECBFE6EE8BA066836CD886CCF5C974C0 ();
// 0x00000151 System.Void OVRInput_OVRControllerRTrackedRemote::ConfigureTouchMap()
extern void OVRControllerRTrackedRemote_ConfigureTouchMap_m1C0F5C9524045CAEDB6AEAAA2E35D335AA13FEDD ();
// 0x00000152 System.Void OVRInput_OVRControllerRTrackedRemote::ConfigureNearTouchMap()
extern void OVRControllerRTrackedRemote_ConfigureNearTouchMap_m50A0391A52A19E224DCBCE37F6F22BB5B462BDE5 ();
// 0x00000153 System.Void OVRInput_OVRControllerRTrackedRemote::ConfigureAxis1DMap()
extern void OVRControllerRTrackedRemote_ConfigureAxis1DMap_m093ED912B993FB02054A47102B320CF8406A7774 ();
// 0x00000154 System.Void OVRInput_OVRControllerRTrackedRemote::ConfigureAxis2DMap()
extern void OVRControllerRTrackedRemote_ConfigureAxis2DMap_m9FE0734E7DC5AD73AEB7E69BC84A41E9AE32E651 ();
// 0x00000155 OVRInput_Controller OVRInput_OVRControllerRTrackedRemote::Update()
extern void OVRControllerRTrackedRemote_Update_m01D3449B0C8F94198F0105A0CFC0DD70591D0A66 ();
// 0x00000156 System.Boolean OVRInput_OVRControllerRTrackedRemote::WasRecentered()
extern void OVRControllerRTrackedRemote_WasRecentered_mF27A63C7718937A0721AC5D263C4EC53BEF7FFB4 ();
// 0x00000157 System.Byte OVRInput_OVRControllerRTrackedRemote::GetRecenterCount()
extern void OVRControllerRTrackedRemote_GetRecenterCount_m9CDB47A0FE5ADD7D4198F72053E79A3A69B44D53 ();
// 0x00000158 System.Byte OVRInput_OVRControllerRTrackedRemote::GetBatteryPercentRemaining()
extern void OVRControllerRTrackedRemote_GetBatteryPercentRemaining_m3DDCA1E8FEE31DDB4CCD69976CB84AE663AF352A ();
// 0x00000159 System.Void OVRLayerAttribute::.ctor()
extern void OVRLayerAttribute__ctor_m3B0F7D901CBA30D669078355C44AB675F57A6AFE ();
// 0x0000015A OVRManager OVRManager::get_instance()
extern void OVRManager_get_instance_mDECB23FAADD39F7A1E4FFCE849ADEC314E61BA93 ();
// 0x0000015B System.Void OVRManager::set_instance(OVRManager)
extern void OVRManager_set_instance_mFE7AAB4BD2630512DEBDC82F10B78DD483284262 ();
// 0x0000015C OVRDisplay OVRManager::get_display()
extern void OVRManager_get_display_mDD43E2C369D8F7B568202CEF99F1D365EC253B8E ();
// 0x0000015D System.Void OVRManager::set_display(OVRDisplay)
extern void OVRManager_set_display_m8BD00BBBE13C06B3BBFA7219EE670AFD855D3E77 ();
// 0x0000015E OVRTracker OVRManager::get_tracker()
extern void OVRManager_get_tracker_m3896940AE3394CD4A2869B90DE851C4974F09643 ();
// 0x0000015F System.Void OVRManager::set_tracker(OVRTracker)
extern void OVRManager_set_tracker_m94ABDA7879D536396569B21071DB293B4C9187FE ();
// 0x00000160 OVRBoundary OVRManager::get_boundary()
extern void OVRManager_get_boundary_m7808AF1F1EC2FE7C2CDA325CE00555F1B98DF7FF ();
// 0x00000161 System.Void OVRManager::set_boundary(OVRBoundary)
extern void OVRManager_set_boundary_mC8834D268079F568327FF68DA9EB44D0F694DA58 ();
// 0x00000162 OVRProfile OVRManager::get_profile()
extern void OVRManager_get_profile_m92D6DF3F1A6191783CD2174FA0B568DD72F8E395 ();
// 0x00000163 System.Void OVRManager::add_HMDAcquired(System.Action)
extern void OVRManager_add_HMDAcquired_mF6769C2AFFAE59F72E690648081360B00F9A1844 ();
// 0x00000164 System.Void OVRManager::remove_HMDAcquired(System.Action)
extern void OVRManager_remove_HMDAcquired_m72D3CC00D042F4431387B015EF7D0ECCA443D61B ();
// 0x00000165 System.Void OVRManager::add_HMDLost(System.Action)
extern void OVRManager_add_HMDLost_m7869E7000110EDFEAE4D58B5D60886ED0B6599F1 ();
// 0x00000166 System.Void OVRManager::remove_HMDLost(System.Action)
extern void OVRManager_remove_HMDLost_m0D91AD36321659BF043E471F5A9D1724C38B3097 ();
// 0x00000167 System.Void OVRManager::add_HMDMounted(System.Action)
extern void OVRManager_add_HMDMounted_m4D96DBA40FD18163B232C9C28736B5B9B4305124 ();
// 0x00000168 System.Void OVRManager::remove_HMDMounted(System.Action)
extern void OVRManager_remove_HMDMounted_m22DCAE74E33CE3F7AD77229E173E31E4D5B96A0A ();
// 0x00000169 System.Void OVRManager::add_HMDUnmounted(System.Action)
extern void OVRManager_add_HMDUnmounted_m97DA139E73CDFBF98F471E9A3BB29C236539862F ();
// 0x0000016A System.Void OVRManager::remove_HMDUnmounted(System.Action)
extern void OVRManager_remove_HMDUnmounted_mD350C82D1E8AE6CFB5DDE3AB0ACBA5B9E3BEF58A ();
// 0x0000016B System.Void OVRManager::add_VrFocusAcquired(System.Action)
extern void OVRManager_add_VrFocusAcquired_m22FE409E80970BF9464FF62A6F908EF38D2D97CA ();
// 0x0000016C System.Void OVRManager::remove_VrFocusAcquired(System.Action)
extern void OVRManager_remove_VrFocusAcquired_mDDB8E1B64988FB2B9EED920CEB69776B815F868C ();
// 0x0000016D System.Void OVRManager::add_VrFocusLost(System.Action)
extern void OVRManager_add_VrFocusLost_m60B6FF0192DEBE5702EF8B80A3F104E06BB30838 ();
// 0x0000016E System.Void OVRManager::remove_VrFocusLost(System.Action)
extern void OVRManager_remove_VrFocusLost_m46470A33D7402EA0996B4A1D323D078D16762619 ();
// 0x0000016F System.Void OVRManager::add_InputFocusAcquired(System.Action)
extern void OVRManager_add_InputFocusAcquired_m28DD7BB67EF01E87D05EDFCF51E8EB447602FC37 ();
// 0x00000170 System.Void OVRManager::remove_InputFocusAcquired(System.Action)
extern void OVRManager_remove_InputFocusAcquired_m3998B532375D6056078B886D2CC6B0EC69860814 ();
// 0x00000171 System.Void OVRManager::add_InputFocusLost(System.Action)
extern void OVRManager_add_InputFocusLost_m3115553C211F283A719531C2BC01BF036AB18317 ();
// 0x00000172 System.Void OVRManager::remove_InputFocusLost(System.Action)
extern void OVRManager_remove_InputFocusLost_mE7702024ACDBEE733CD20C94AC8419A6FFAE1A62 ();
// 0x00000173 System.Void OVRManager::add_AudioOutChanged(System.Action)
extern void OVRManager_add_AudioOutChanged_mB4939854D1F78237E10D1B9F4494BE55AAEA30C2 ();
// 0x00000174 System.Void OVRManager::remove_AudioOutChanged(System.Action)
extern void OVRManager_remove_AudioOutChanged_m7E706D6C48604E15C8CA5AB9ABCD40148404E6B4 ();
// 0x00000175 System.Void OVRManager::add_AudioInChanged(System.Action)
extern void OVRManager_add_AudioInChanged_m23DECA7E41D80D7E1C59BB29B849D362861E6B10 ();
// 0x00000176 System.Void OVRManager::remove_AudioInChanged(System.Action)
extern void OVRManager_remove_AudioInChanged_mF845BF96DC2F2FE727F25941AD2480C43C1DCDB5 ();
// 0x00000177 System.Void OVRManager::add_TrackingAcquired(System.Action)
extern void OVRManager_add_TrackingAcquired_mB91D4E78240C0133B517913F3EF44D2E54AF30EF ();
// 0x00000178 System.Void OVRManager::remove_TrackingAcquired(System.Action)
extern void OVRManager_remove_TrackingAcquired_m91B3D1D1C40B1032E65EA50134594DF9EF150E78 ();
// 0x00000179 System.Void OVRManager::add_TrackingLost(System.Action)
extern void OVRManager_add_TrackingLost_m01FFB6EE74C0BCCB40D7C7E74019B5DBDA35E9DF ();
// 0x0000017A System.Void OVRManager::remove_TrackingLost(System.Action)
extern void OVRManager_remove_TrackingLost_m1FEE5926B1C2E2FA17E6966AE163A01380C7C193 ();
// 0x0000017B System.Void OVRManager::add_HSWDismissed(System.Action)
extern void OVRManager_add_HSWDismissed_mAB7AA9F3FED0868A9E16D1CA6E4844F875EA4FFD ();
// 0x0000017C System.Void OVRManager::remove_HSWDismissed(System.Action)
extern void OVRManager_remove_HSWDismissed_m2D2A9D5E557724CE47AC99DC56346D517748EAF2 ();
// 0x0000017D System.Boolean OVRManager::get_isHmdPresent()
extern void OVRManager_get_isHmdPresent_mED41DBAFDB605514FC4650871BA080B5B0193D13 ();
// 0x0000017E System.Void OVRManager::set_isHmdPresent(System.Boolean)
extern void OVRManager_set_isHmdPresent_mE516003BEBC7BCDCB19AA35781A3AE9459D05266 ();
// 0x0000017F System.String OVRManager::get_audioOutId()
extern void OVRManager_get_audioOutId_mAEA582D051D590286473DAE68A0993EC02F98729 ();
// 0x00000180 System.String OVRManager::get_audioInId()
extern void OVRManager_get_audioInId_m298B5AA600BF2510EBE8C27DE1C67A6BE0C74919 ();
// 0x00000181 System.Boolean OVRManager::get_hasVrFocus()
extern void OVRManager_get_hasVrFocus_mACE917EA641306B20F9E6CB0B95BDC932B9EBEE1 ();
// 0x00000182 System.Void OVRManager::set_hasVrFocus(System.Boolean)
extern void OVRManager_set_hasVrFocus_mD4917F853261973340D917194E37DFAA3C98A033 ();
// 0x00000183 System.Boolean OVRManager::get_hasInputFocus()
extern void OVRManager_get_hasInputFocus_mD4AF719FC6E6273E9464EA55C48AE56A55FF6028 ();
// 0x00000184 System.Boolean OVRManager::get_chromatic()
extern void OVRManager_get_chromatic_mF6523ED06FAE723D803CA00E75EBFD5AF3C20F96 ();
// 0x00000185 System.Void OVRManager::set_chromatic(System.Boolean)
extern void OVRManager_set_chromatic_mD2FB9C7CB7DD39C4D3C6C4C697090F015513A332 ();
// 0x00000186 System.Boolean OVRManager::get_monoscopic()
extern void OVRManager_get_monoscopic_mC5104AE9A70720EB46EA6017D098219FB4A5B835 ();
// 0x00000187 System.Void OVRManager::set_monoscopic(System.Boolean)
extern void OVRManager_set_monoscopic_m28EE0C6D9A9E79F331D3CE278F64D6F361D1E2C8 ();
// 0x00000188 System.Boolean OVRManager::IsAdaptiveResSupportedByEngine()
extern void OVRManager_IsAdaptiveResSupportedByEngine_m9154A00065BA5716187BCC96FE78543678BF9D18 ();
// 0x00000189 UnityEngine.Vector3 OVRManager::get_headPoseRelativeOffsetRotation()
extern void OVRManager_get_headPoseRelativeOffsetRotation_m030915C4E7C9C95B6E030DDBD876B60D232DC580 ();
// 0x0000018A System.Void OVRManager::set_headPoseRelativeOffsetRotation(UnityEngine.Vector3)
extern void OVRManager_set_headPoseRelativeOffsetRotation_m420B4FF180D75BD70514BAFBCAAD0334CD8EA248 ();
// 0x0000018B UnityEngine.Vector3 OVRManager::get_headPoseRelativeOffsetTranslation()
extern void OVRManager_get_headPoseRelativeOffsetTranslation_m7B311AD1595AF5D574B585B91FB459F49EBEBC43 ();
// 0x0000018C System.Void OVRManager::set_headPoseRelativeOffsetTranslation(UnityEngine.Vector3)
extern void OVRManager_set_headPoseRelativeOffsetTranslation_m75ECB1CAF5C5980E092EA17CA2F4F72A788E7C9D ();
// 0x0000018D System.Int32 OVRManager::get_vsyncCount()
extern void OVRManager_get_vsyncCount_m31A4814E6EAC0C15F2287D0ABF2229298230DF97 ();
// 0x0000018E System.Void OVRManager::set_vsyncCount(System.Int32)
extern void OVRManager_set_vsyncCount_m8E37770069EAAF3106FF642186FBFDB29F90BC5A ();
// 0x0000018F System.Single OVRManager::get_batteryLevel()
extern void OVRManager_get_batteryLevel_mF3989FC197C268638E22D110AC72262BA32E88D5 ();
// 0x00000190 System.Single OVRManager::get_batteryTemperature()
extern void OVRManager_get_batteryTemperature_mC2E9309E8E9E443BF0BF25D79F14710AEB5959F5 ();
// 0x00000191 System.Int32 OVRManager::get_batteryStatus()
extern void OVRManager_get_batteryStatus_mBE7DFEA8CBB4FAC2DB9D8807D872D2D8FC6EDF11 ();
// 0x00000192 System.Single OVRManager::get_volumeLevel()
extern void OVRManager_get_volumeLevel_mB58406893F6A03554F8313FDF605B807B1E533CD ();
// 0x00000193 System.Int32 OVRManager::get_cpuLevel()
extern void OVRManager_get_cpuLevel_mC343F1940F40DB94AB04B39B98CA555CE2F4646A ();
// 0x00000194 System.Void OVRManager::set_cpuLevel(System.Int32)
extern void OVRManager_set_cpuLevel_mF51AB036A5819FBA7E066D91FA7D1A8111B7B909 ();
// 0x00000195 System.Int32 OVRManager::get_gpuLevel()
extern void OVRManager_get_gpuLevel_m5E800FC829B57595312287F4A384FA34E039FFDC ();
// 0x00000196 System.Void OVRManager::set_gpuLevel(System.Int32)
extern void OVRManager_set_gpuLevel_mE60D72BA91CA5A9379C4E043C19EBEEB2A52C452 ();
// 0x00000197 System.Boolean OVRManager::get_isPowerSavingActive()
extern void OVRManager_get_isPowerSavingActive_m8910EC0EFB83E757EAB0A9650B0ECFB55ECD24CF ();
// 0x00000198 OVRManager_EyeTextureFormat OVRManager::get_eyeTextureFormat()
extern void OVRManager_get_eyeTextureFormat_m99F7FA0CDC40B67D150551AC8C0F41820EECD1FD ();
// 0x00000199 System.Void OVRManager::set_eyeTextureFormat(OVRManager_EyeTextureFormat)
extern void OVRManager_set_eyeTextureFormat_mF39830F5EF3488295017F2F4F44F1B9E41E412FF ();
// 0x0000019A System.Boolean OVRManager::get_fixedFoveatedRenderingSupported()
extern void OVRManager_get_fixedFoveatedRenderingSupported_m80D1FC14234EFE1483B05BA61F48D149AD94039E ();
// 0x0000019B OVRManager_FixedFoveatedRenderingLevel OVRManager::get_fixedFoveatedRenderingLevel()
extern void OVRManager_get_fixedFoveatedRenderingLevel_mD15DAFC6B06C7815601CC6F5F42A56BE39BAEA2E ();
// 0x0000019C System.Void OVRManager::set_fixedFoveatedRenderingLevel(OVRManager_FixedFoveatedRenderingLevel)
extern void OVRManager_set_fixedFoveatedRenderingLevel_m8868C801F750485D0C094C525E0322A89E12FFC5 ();
// 0x0000019D System.Boolean OVRManager::get_tiledMultiResSupported()
extern void OVRManager_get_tiledMultiResSupported_m7B32DD7DDCFD3CFE47B569C277DB4D543567699B ();
// 0x0000019E OVRManager_TiledMultiResLevel OVRManager::get_tiledMultiResLevel()
extern void OVRManager_get_tiledMultiResLevel_mBA4B86D4E8770FEF2910365F884320C5C66F2694 ();
// 0x0000019F System.Void OVRManager::set_tiledMultiResLevel(OVRManager_TiledMultiResLevel)
extern void OVRManager_set_tiledMultiResLevel_m0BF85177FF074F90F5141BD898D29CDA301C364A ();
// 0x000001A0 System.Boolean OVRManager::get_gpuUtilSupported()
extern void OVRManager_get_gpuUtilSupported_m2409BFA06A721D84388FFD4208817979EE9872E2 ();
// 0x000001A1 System.Single OVRManager::get_gpuUtilLevel()
extern void OVRManager_get_gpuUtilLevel_m204E5BE39DAB99A32926D404CCCB65235BE6717E ();
// 0x000001A2 System.Void OVRManager::SetColorScaleAndOffset(UnityEngine.Vector4,UnityEngine.Vector4,System.Boolean)
extern void OVRManager_SetColorScaleAndOffset_m835700F2546573A077E78EA3EDF1531E14E5D128 ();
// 0x000001A3 System.Void OVRManager::SetOpenVRLocalPose(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void OVRManager_SetOpenVRLocalPose_m306D491740A47D48E7E94826A2B3D857A3F29FDB ();
// 0x000001A4 OVRPose OVRManager::GetOpenVRControllerOffset(UnityEngine.XR.XRNode)
extern void OVRManager_GetOpenVRControllerOffset_mDAFB7CC00B3D29E8C680A00843FF381AEDB350D9 ();
// 0x000001A5 OVRManager_TrackingOrigin OVRManager::get_trackingOriginType()
extern void OVRManager_get_trackingOriginType_m736B457ADF8B4519491CDF707714B5918F6ACAFA ();
// 0x000001A6 System.Void OVRManager::set_trackingOriginType(OVRManager_TrackingOrigin)
extern void OVRManager_set_trackingOriginType_mDE10B31D46CEAB54FBA805B25AFE623C7A4FF353 ();
// 0x000001A7 System.Boolean OVRManager::get_reorientHMDOnControllerRecenter()
extern void OVRManager_get_reorientHMDOnControllerRecenter_mFEE2A3CB5817F562EAF892C6917F1F4E16026AF2 ();
// 0x000001A8 System.Void OVRManager::set_reorientHMDOnControllerRecenter(System.Boolean)
extern void OVRManager_set_reorientHMDOnControllerRecenter_mC414A44203789A3C87FACF0DAB596D546A224527 ();
// 0x000001A9 System.Boolean OVRManager::get_isSupportedPlatform()
extern void OVRManager_get_isSupportedPlatform_m903DE769917CEC68A814D7E09DCAAD5336816113 ();
// 0x000001AA System.Void OVRManager::set_isSupportedPlatform(System.Boolean)
extern void OVRManager_set_isSupportedPlatform_m3ED3252A7F969DA1B704B8AED972AF57C492C612 ();
// 0x000001AB System.Boolean OVRManager::get_isUserPresent()
extern void OVRManager_get_isUserPresent_m0468018F27C6CB32BC9F4B2FF66B7464C36D786A ();
// 0x000001AC System.Void OVRManager::set_isUserPresent(System.Boolean)
extern void OVRManager_set_isUserPresent_m47900102D89207BC4EB1C000B31EE4F4FB3304A6 ();
// 0x000001AD System.Version OVRManager::get_utilitiesVersion()
extern void OVRManager_get_utilitiesVersion_m879B9C86EF390299CE13DED505A2E6C1565BB7F8 ();
// 0x000001AE System.Version OVRManager::get_pluginVersion()
extern void OVRManager_get_pluginVersion_m460B68FC87725CF738A74B2CC69D599E939C930C ();
// 0x000001AF System.Version OVRManager::get_sdkVersion()
extern void OVRManager_get_sdkVersion_m9FE7106BB01BD214496EA8355FD4B8E58767ADC9 ();
// 0x000001B0 System.Boolean OVRManager::IsUnityAlphaOrBetaVersion()
extern void OVRManager_IsUnityAlphaOrBetaVersion_m078EC8D73BB5EA84DFD93819756C1473567C81A0 ();
// 0x000001B1 System.Void OVRManager::InitOVRManager()
extern void OVRManager_InitOVRManager_mCAE32BB448494B6D421C54BCA4FE0A64564325C5 ();
// 0x000001B2 System.Void OVRManager::Awake()
extern void OVRManager_Awake_mE2382FF8F17CABC0C0ADEBE62F6FF2594E97DF3B ();
// 0x000001B3 System.Void OVRManager::SetCurrentXRDevice()
extern void OVRManager_SetCurrentXRDevice_m22D386A05E3864FF3A2ADBE47CAA1D3804B43CD9 ();
// 0x000001B4 System.Void OVRManager::Initialize()
extern void OVRManager_Initialize_m3ED5502F37E9E16E3B76B06DACCB7F6DDB1AC180 ();
// 0x000001B5 System.Void OVRManager::Update()
extern void OVRManager_Update_m8C2017A3E668900D35E6AE980802303675CBA89A ();
// 0x000001B6 UnityEngine.Camera OVRManager::FindMainCamera()
extern void OVRManager_FindMainCamera_m3E061562D17077AC417AD2B56369D04BB2254FA1 ();
// 0x000001B7 System.Void OVRManager::OnDisable()
extern void OVRManager_OnDisable_mF9AC11370A4563BA4BA79A1117BFF1CCDD87944F ();
// 0x000001B8 System.Void OVRManager::LateUpdate()
extern void OVRManager_LateUpdate_mAFFEBA018DD17EB5D8FA37F1DAAC0A7B80AE6B03 ();
// 0x000001B9 System.Void OVRManager::FixedUpdate()
extern void OVRManager_FixedUpdate_m33A743F2F9F71C2E325BEDFF6F403DF88EBC90BB ();
// 0x000001BA System.Void OVRManager::OnDestroy()
extern void OVRManager_OnDestroy_m6739E0927C2C01CD2A5575587D23C1FB6E188360 ();
// 0x000001BB System.Void OVRManager::OnApplicationPause(System.Boolean)
extern void OVRManager_OnApplicationPause_mCCF67F674BB1B9CC2B571D9EA42CE736611B2D91 ();
// 0x000001BC System.Void OVRManager::OnApplicationFocus(System.Boolean)
extern void OVRManager_OnApplicationFocus_m2412B9844FC8C4FC2CFA836C4198CC3E4AF8EFBF ();
// 0x000001BD System.Void OVRManager::OnApplicationQuit()
extern void OVRManager_OnApplicationQuit_mCD5F15E639C4FF858AC828151BDD58A648115402 ();
// 0x000001BE System.Void OVRManager::ReturnToLauncher()
extern void OVRManager_ReturnToLauncher_m110AE6ED933100FD138161C749A00E46ECD71BD4 ();
// 0x000001BF System.Void OVRManager::PlatformUIConfirmQuit()
extern void OVRManager_PlatformUIConfirmQuit_mBAD548D77F667E55639F7EF3F644A5D2565A9A0C ();
// 0x000001C0 System.Void OVRManager::.ctor()
extern void OVRManager__ctor_mF1D76275CB1C427FA837B8C0D00B14E57CD4607F ();
// 0x000001C1 System.Void OVRManager::.cctor()
extern void OVRManager__cctor_m5696974B5227590002A4A2574EC3F61F050BFB10 ();
// 0x000001C2 System.Void OVRManager_<>c::.cctor()
extern void U3CU3Ec__cctor_mCF283ED21B6D69D0AB24851CA57E687C12113D20 ();
// 0x000001C3 System.Void OVRManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mED4A9A4E238E59F512EA6542C2C74D8E908CA1D2 ();
// 0x000001C4 System.Int32 OVRManager_<>c::<FindMainCamera>b__196_0(UnityEngine.Camera,UnityEngine.Camera)
extern void U3CU3Ec_U3CFindMainCameraU3Eb__196_0_mB2EF6FFC72FE542CC87CCBB649B4BB784E94BEC3 ();
// 0x000001C5 System.Void OVROverlay::OverrideOverlayTextureInfo(UnityEngine.Texture,System.IntPtr,UnityEngine.XR.XRNode)
extern void OVROverlay_OverrideOverlayTextureInfo_m3DCF93E34B01580970D60D0215724B37E2BF083E ();
// 0x000001C6 OVRPlugin_LayerLayout OVROverlay::get_layout()
extern void OVROverlay_get_layout_m2D64B814A51EA065C2B5010AF9BC9EB2C600FB66 ();
// 0x000001C7 System.Int32 OVROverlay::get_texturesPerStage()
extern void OVROverlay_get_texturesPerStage_mB53D8F108151B58175A256D8BA28CCEA2FC1E082 ();
// 0x000001C8 System.Boolean OVROverlay::CreateLayer(System.Int32,System.Int32,OVRPlugin_EyeTextureFormat,System.Int32,OVRPlugin_Sizei,OVRPlugin_OverlayShape)
extern void OVROverlay_CreateLayer_m85C186A08F0C02DD66235583D02E0C2F94AEBAB6 ();
// 0x000001C9 System.Boolean OVROverlay::CreateLayerTextures(System.Boolean,OVRPlugin_Sizei,System.Boolean)
extern void OVROverlay_CreateLayerTextures_mFB2C5EE0617A15333ECC8BB66EE59F6BE4B79775 ();
// 0x000001CA System.Void OVROverlay::DestroyLayerTextures()
extern void OVROverlay_DestroyLayerTextures_m9BE57EC22590300B983D4CBF161288C0FF4703E1 ();
// 0x000001CB System.Void OVROverlay::DestroyLayer()
extern void OVROverlay_DestroyLayer_m4B3111ADEF940631EFB19CAB62F26D6E4DF705C4 ();
// 0x000001CC System.Void OVROverlay::SetSrcDestRects(UnityEngine.Rect,UnityEngine.Rect,UnityEngine.Rect,UnityEngine.Rect)
extern void OVROverlay_SetSrcDestRects_m7DCB79AD3F0080514C800FB5D24665F195525AD8 ();
// 0x000001CD System.Void OVROverlay::UpdateTextureRectMatrix()
extern void OVROverlay_UpdateTextureRectMatrix_m3CEF00BFA7470A08BD569157FD26A4825F117C5C ();
// 0x000001CE System.Void OVROverlay::SetPerLayerColorScaleAndOffset(UnityEngine.Vector4,UnityEngine.Vector4)
extern void OVROverlay_SetPerLayerColorScaleAndOffset_mB71987F85E032A94EF98BCB0736A503C2F54F0C9 ();
// 0x000001CF System.Boolean OVROverlay::LatchLayerTextures()
extern void OVROverlay_LatchLayerTextures_m1C037B2F6FC56A2B9F0825CB7FFCCF5FF6B9B8C4 ();
// 0x000001D0 OVRPlugin_LayerDesc OVROverlay::GetCurrentLayerDesc()
extern void OVROverlay_GetCurrentLayerDesc_m92721260E9C150FF61986DBDD9E23E05CFEAEDD2 ();
// 0x000001D1 System.Boolean OVROverlay::PopulateLayer(System.Int32,System.Boolean,OVRPlugin_Sizei,System.Int32,System.Int32)
extern void OVROverlay_PopulateLayer_mA26F77660DA6C25B6D8A53C7708DD3EB0C69A71D ();
// 0x000001D2 System.Boolean OVROverlay::SubmitLayer(System.Boolean,System.Boolean,System.Boolean,OVRPose,UnityEngine.Vector3,System.Int32)
extern void OVROverlay_SubmitLayer_mD3995EDE8574CE4FC3329BED6531FEFCEB38DA3A ();
// 0x000001D3 System.Void OVROverlay::Awake()
extern void OVROverlay_Awake_m74C6851805B5431E3D34D73EFBF7B8E572C5FF7F ();
// 0x000001D4 System.String OVROverlay::get_OpenVROverlayKey()
extern void OVROverlay_get_OpenVROverlayKey_mEF6F84F8FF68FC201F99156214FFF4031A5534F8 ();
// 0x000001D5 System.Void OVROverlay::OnEnable()
extern void OVROverlay_OnEnable_mF0C5A9092AA36B8D8CCA109F0C2610370C3A8992 ();
// 0x000001D6 System.Void OVROverlay::InitOVROverlay()
extern void OVROverlay_InitOVROverlay_m474A4ECF6D19AF79FDDB3AD63F180441D4A1E35E ();
// 0x000001D7 System.Void OVROverlay::OnDisable()
extern void OVROverlay_OnDisable_m4C19945F851A3A8B3098905B10E0C5A0ACCD0498 ();
// 0x000001D8 System.Void OVROverlay::OnDestroy()
extern void OVROverlay_OnDestroy_m1021238A607C57B26F2FF79B2BE47FB0AB54988F ();
// 0x000001D9 System.Boolean OVROverlay::ComputeSubmit(OVRPose&,UnityEngine.Vector3&,System.Boolean&,System.Boolean&)
extern void OVROverlay_ComputeSubmit_m89437B0B5D5BB10B02DAED0A6B98491F29F393B1 ();
// 0x000001DA System.Void OVROverlay::OpenVROverlayUpdate(UnityEngine.Vector3,OVRPose)
extern void OVROverlay_OpenVROverlayUpdate_m75733B3F0849C33F1280D2894A3F32663EF28C0F ();
// 0x000001DB System.Void OVROverlay::LateUpdate()
extern void OVROverlay_LateUpdate_m325138D8F2685430DA39F84CDECEF68103FFBEFC ();
// 0x000001DC System.Void OVROverlay::.ctor()
extern void OVROverlay__ctor_mB87D9EABB9338C64620007DF5DEB4BDCC570716D ();
// 0x000001DD System.Void OVROverlay::.cctor()
extern void OVROverlay__cctor_m3DECE8D45B87A5D7F693C568CC1DEF99532FB753 ();
// 0x000001DE System.Void OVROverlay_ExternalSurfaceObjectCreated::.ctor(System.Object,System.IntPtr)
extern void ExternalSurfaceObjectCreated__ctor_m1B546172F7C6A09C9E050CCCBD2F7B67D613B3AA ();
// 0x000001DF System.Void OVROverlay_ExternalSurfaceObjectCreated::Invoke()
extern void ExternalSurfaceObjectCreated_Invoke_mD94AC5D924842F20FA91CFECF2AD57241A0ADE4D ();
// 0x000001E0 System.IAsyncResult OVROverlay_ExternalSurfaceObjectCreated::BeginInvoke(System.AsyncCallback,System.Object)
extern void ExternalSurfaceObjectCreated_BeginInvoke_mABE1CF22A8A1E6739C89A09719D51381137FA4A2 ();
// 0x000001E1 System.Void OVROverlay_ExternalSurfaceObjectCreated::EndInvoke(System.IAsyncResult)
extern void ExternalSurfaceObjectCreated_EndInvoke_m9F6BA59C290034328796300277F215FC5F61E691 ();
// 0x000001E2 OVRPlatformMenu_eBackButtonAction OVRPlatformMenu::HandleBackButtonState()
extern void OVRPlatformMenu_HandleBackButtonState_mBF8F637999C3992EAC1DA019EAFF716CFB3F31EF ();
// 0x000001E3 System.Void OVRPlatformMenu::Awake()
extern void OVRPlatformMenu_Awake_m2EFD1169C2616E81D9FA9144835B4EFD4A7178FB ();
// 0x000001E4 System.Void OVRPlatformMenu::ShowConfirmQuitMenu()
extern void OVRPlatformMenu_ShowConfirmQuitMenu_m53589E50AD8D461C277AD2D0D61EBAFA774E55D9 ();
// 0x000001E5 System.Boolean OVRPlatformMenu::RetreatOneLevel()
extern void OVRPlatformMenu_RetreatOneLevel_m42EB7CA2FD8BD9A8610C03DBFE143386FA066605 ();
// 0x000001E6 System.Void OVRPlatformMenu::Update()
extern void OVRPlatformMenu_Update_m3CCA1AB33BC82AD44965ACC63D020F9892208A15 ();
// 0x000001E7 System.Void OVRPlatformMenu::.ctor()
extern void OVRPlatformMenu__ctor_m130FA2C8BE776EAB92DCBADE2E5049E5FD8949A7 ();
// 0x000001E8 System.Void OVRPlatformMenu::.cctor()
extern void OVRPlatformMenu__cctor_m6D55DB7807BBDDF8BC38565066C671C5BF087592 ();
// 0x000001E9 System.Version OVRPlugin::get_version()
extern void OVRPlugin_get_version_mEB9C2FA4B55E714D70A55366E9EEBDAD8E361286 ();
// 0x000001EA System.Version OVRPlugin::get_nativeSDKVersion()
extern void OVRPlugin_get_nativeSDKVersion_mEB77B7C8EE154D25D5DCE159931CF97B5D1D5834 ();
// 0x000001EB System.Boolean OVRPlugin::get_initialized()
extern void OVRPlugin_get_initialized_mC88032BD2BA67E12DFF85A5A92F52AE183820B27 ();
// 0x000001EC System.Boolean OVRPlugin::get_chromatic()
extern void OVRPlugin_get_chromatic_mC410957203243A05964CC341641C15E53D3B1627 ();
// 0x000001ED System.Void OVRPlugin::set_chromatic(System.Boolean)
extern void OVRPlugin_set_chromatic_m0E2DD4C12960B8F975CBBA498C5A0A122821512D ();
// 0x000001EE System.Boolean OVRPlugin::get_monoscopic()
extern void OVRPlugin_get_monoscopic_m3BB5AB1C7A00E1BC1AE6ADE9C7EEB7EB873E0967 ();
// 0x000001EF System.Void OVRPlugin::set_monoscopic(System.Boolean)
extern void OVRPlugin_set_monoscopic_mAD538530F8F2A9C9CC09D9CC00237D7473B96827 ();
// 0x000001F0 System.Boolean OVRPlugin::get_rotation()
extern void OVRPlugin_get_rotation_mD1B34D3419E6312F3EA3B950751A1754D41BE31F ();
// 0x000001F1 System.Void OVRPlugin::set_rotation(System.Boolean)
extern void OVRPlugin_set_rotation_m17BC23C1B1AF0E429D5671CE39B42C1191991603 ();
// 0x000001F2 System.Boolean OVRPlugin::get_position()
extern void OVRPlugin_get_position_mEF1F6068F44AFF008848634DAF658FF83148B0A8 ();
// 0x000001F3 System.Void OVRPlugin::set_position(System.Boolean)
extern void OVRPlugin_set_position_mD5D62A9A4CE78B2136AB19EC9EB2A45FBCACD4E4 ();
// 0x000001F4 System.Boolean OVRPlugin::get_useIPDInPositionTracking()
extern void OVRPlugin_get_useIPDInPositionTracking_mD9A79B1A05173B12CCE3E9A0D0243BD6847FF865 ();
// 0x000001F5 System.Void OVRPlugin::set_useIPDInPositionTracking(System.Boolean)
extern void OVRPlugin_set_useIPDInPositionTracking_mD64C10440A7078A2F56C5DB9AC25D2460786DCF9 ();
// 0x000001F6 System.Boolean OVRPlugin::get_positionSupported()
extern void OVRPlugin_get_positionSupported_m62C34824B86815DF220CA417DC64559C6CEC1FA4 ();
// 0x000001F7 System.Boolean OVRPlugin::get_positionTracked()
extern void OVRPlugin_get_positionTracked_mBA38B359913BA23E306E66132638CE54FDC5AC94 ();
// 0x000001F8 System.Boolean OVRPlugin::get_powerSaving()
extern void OVRPlugin_get_powerSaving_m963E0A888068F336B01FE28ADDBE5924401EDBDF ();
// 0x000001F9 System.Boolean OVRPlugin::get_hmdPresent()
extern void OVRPlugin_get_hmdPresent_mB282E1E5AFF6D331EC3F833FE8D79F7CB008BA5E ();
// 0x000001FA System.Boolean OVRPlugin::get_userPresent()
extern void OVRPlugin_get_userPresent_m17458D95D75EA4952A56B088321EC7F646E9921D ();
// 0x000001FB System.Boolean OVRPlugin::get_headphonesPresent()
extern void OVRPlugin_get_headphonesPresent_m55EEF0A8127C7797E81D8A54B93B9C4170EE2D74 ();
// 0x000001FC System.Int32 OVRPlugin::get_recommendedMSAALevel()
extern void OVRPlugin_get_recommendedMSAALevel_mC728B00F70079A9A7F25FE3EB48C7E70554BB3C7 ();
// 0x000001FD OVRPlugin_SystemRegion OVRPlugin::get_systemRegion()
extern void OVRPlugin_get_systemRegion_m300AD5156AE28135935F9FF0156F7D30664B02A9 ();
// 0x000001FE System.String OVRPlugin::get_audioOutId()
extern void OVRPlugin_get_audioOutId_mCE070B81A00878965DC3924AA8E14F643CAF2E61 ();
// 0x000001FF System.String OVRPlugin::get_audioInId()
extern void OVRPlugin_get_audioInId_m7B3A33CD32F88C09B173CD2BCFB62220378A8F9B ();
// 0x00000200 System.Boolean OVRPlugin::get_hasVrFocus()
extern void OVRPlugin_get_hasVrFocus_mBE848A231E8BDD2901465D70185AA98B1FB870C2 ();
// 0x00000201 System.Boolean OVRPlugin::get_hasInputFocus()
extern void OVRPlugin_get_hasInputFocus_m5E75D7DC02A567DF9042C52367038164B8E8AD1A ();
// 0x00000202 System.Boolean OVRPlugin::get_shouldQuit()
extern void OVRPlugin_get_shouldQuit_mA15A11C8D356B73C821329F36E696F2A751EF8C7 ();
// 0x00000203 System.Boolean OVRPlugin::get_shouldRecenter()
extern void OVRPlugin_get_shouldRecenter_mB2965AD2ED7D4FAEFBFFE8F43A9D21F5A34A78EA ();
// 0x00000204 System.String OVRPlugin::get_productName()
extern void OVRPlugin_get_productName_mE0DEA1F1C2AF365A49BCAC235484E33C04C68DF7 ();
// 0x00000205 System.String OVRPlugin::get_latency()
extern void OVRPlugin_get_latency_m7CA0FED05E9F585A4D2DC5215003535B65E3D69E ();
// 0x00000206 System.Single OVRPlugin::get_eyeDepth()
extern void OVRPlugin_get_eyeDepth_mDCD97322897B0634D98A759D9153F2F9B313F322 ();
// 0x00000207 System.Void OVRPlugin::set_eyeDepth(System.Single)
extern void OVRPlugin_set_eyeDepth_m02A9053C4CDFC055826632CA5CD74DEE0343DDE1 ();
// 0x00000208 System.Single OVRPlugin::get_eyeHeight()
extern void OVRPlugin_get_eyeHeight_m648BE7F5B7635B24B04DF556F6AC9333D7240FE4 ();
// 0x00000209 System.Void OVRPlugin::set_eyeHeight(System.Single)
extern void OVRPlugin_set_eyeHeight_m06836A3D2F267544770C1B5E36FFA3EB57BE3442 ();
// 0x0000020A System.Single OVRPlugin::get_batteryLevel()
extern void OVRPlugin_get_batteryLevel_mF930C2B7B6900B7F846F1B0889AD99285B99E19E ();
// 0x0000020B System.Single OVRPlugin::get_batteryTemperature()
extern void OVRPlugin_get_batteryTemperature_m61C24E9A580D96FB270D8EC8BA93CF3FDF48ECEA ();
// 0x0000020C System.Int32 OVRPlugin::get_cpuLevel()
extern void OVRPlugin_get_cpuLevel_m5106E98BB05173937584C507B9382BEB5F1D52FF ();
// 0x0000020D System.Void OVRPlugin::set_cpuLevel(System.Int32)
extern void OVRPlugin_set_cpuLevel_m0B70D791641728233611E69DB7099728F0773AF6 ();
// 0x0000020E System.Int32 OVRPlugin::get_gpuLevel()
extern void OVRPlugin_get_gpuLevel_mC3969B9E0EBD07A599EBFB0328A0C65392808DAA ();
// 0x0000020F System.Void OVRPlugin::set_gpuLevel(System.Int32)
extern void OVRPlugin_set_gpuLevel_mE4F4282D1B36C06F2A01F4680B01BE8EFAD0DFAA ();
// 0x00000210 System.Int32 OVRPlugin::get_vsyncCount()
extern void OVRPlugin_get_vsyncCount_mC93B88641934956164FC563C46F12DD3068EB5EC ();
// 0x00000211 System.Void OVRPlugin::set_vsyncCount(System.Int32)
extern void OVRPlugin_set_vsyncCount_mE5C06B5D38D7B4B6E3C5D62FDB6BCFA25E216108 ();
// 0x00000212 System.Single OVRPlugin::get_systemVolume()
extern void OVRPlugin_get_systemVolume_m15038ACCE04B10E03644776B7558B258E8C23F78 ();
// 0x00000213 System.Single OVRPlugin::get_ipd()
extern void OVRPlugin_get_ipd_mE2928EC4FC9DC56BA02DD8935281933FFC6C73E2 ();
// 0x00000214 System.Void OVRPlugin::set_ipd(System.Single)
extern void OVRPlugin_set_ipd_mBC4501DE11F7C728206CA1358CB8AF0CBD64AD17 ();
// 0x00000215 System.Boolean OVRPlugin::get_occlusionMesh()
extern void OVRPlugin_get_occlusionMesh_mDDFB6DA888A74D12E4F29E6AC6CCEEFDF2B4C7E5 ();
// 0x00000216 System.Void OVRPlugin::set_occlusionMesh(System.Boolean)
extern void OVRPlugin_set_occlusionMesh_m59E8C3D809B25ED95AD6DD0CCF18ABB4081EC3D9 ();
// 0x00000217 OVRPlugin_BatteryStatus OVRPlugin::get_batteryStatus()
extern void OVRPlugin_get_batteryStatus_m63050AA1BE0C9D86916F7396A3FB5DFEDCE515BF ();
// 0x00000218 OVRPlugin_Frustumf OVRPlugin::GetEyeFrustum(OVRPlugin_Eye)
extern void OVRPlugin_GetEyeFrustum_mB282B6470968CD4C5840015AB8780D6F2E4BBDB0 ();
// 0x00000219 OVRPlugin_Sizei OVRPlugin::GetEyeTextureSize(OVRPlugin_Eye)
extern void OVRPlugin_GetEyeTextureSize_mD9EF059EAD1B688013C541A8DEEC7DEAE408D67C ();
// 0x0000021A OVRPlugin_Posef OVRPlugin::GetTrackerPose(OVRPlugin_Tracker)
extern void OVRPlugin_GetTrackerPose_mFF540B201B40C231DDE95D15027C50AD21CC1EE1 ();
// 0x0000021B OVRPlugin_Frustumf OVRPlugin::GetTrackerFrustum(OVRPlugin_Tracker)
extern void OVRPlugin_GetTrackerFrustum_m82C30E1A089CDFF7C1DAFDBABCC208663596B18F ();
// 0x0000021C System.Boolean OVRPlugin::ShowUI(OVRPlugin_PlatformUI)
extern void OVRPlugin_ShowUI_m570EB1C014D1A16278F5281FD6E988485CC0E1B0 ();
// 0x0000021D System.Boolean OVRPlugin::EnqueueSubmitLayer(System.Boolean,System.Boolean,System.Boolean,System.IntPtr,System.IntPtr,System.Int32,System.Int32,OVRPlugin_Posef,OVRPlugin_Vector3f,System.Int32,OVRPlugin_OverlayShape,System.Boolean,OVRPlugin_TextureRectMatrixf,System.Boolean,UnityEngine.Vector4,UnityEngine.Vector4,System.Boolean)
extern void OVRPlugin_EnqueueSubmitLayer_m85A14E16646E2CCE3F92C57E3ECBCC3830997A97 ();
// 0x0000021E OVRPlugin_LayerDesc OVRPlugin::CalculateLayerDesc(OVRPlugin_OverlayShape,OVRPlugin_LayerLayout,OVRPlugin_Sizei,System.Int32,System.Int32,OVRPlugin_EyeTextureFormat,System.Int32)
extern void OVRPlugin_CalculateLayerDesc_mF9A8E3175E159D17E9A1BD7938296D32B04CEFCB ();
// 0x0000021F System.Boolean OVRPlugin::EnqueueSetupLayer(OVRPlugin_LayerDesc,System.Int32,System.IntPtr)
extern void OVRPlugin_EnqueueSetupLayer_mB198537F855D269CA0354C3C8773DAF8A990EB47 ();
// 0x00000220 System.Boolean OVRPlugin::EnqueueDestroyLayer(System.IntPtr)
extern void OVRPlugin_EnqueueDestroyLayer_mF0570FDA752A8BA4884D2C39BCEFFBDC9F46E2FB ();
// 0x00000221 System.IntPtr OVRPlugin::GetLayerTexture(System.Int32,System.Int32,OVRPlugin_Eye)
extern void OVRPlugin_GetLayerTexture_mC0542B78B4D54BFDA03471E7313FF7FC08D748D6 ();
// 0x00000222 System.Int32 OVRPlugin::GetLayerTextureStageCount(System.Int32)
extern void OVRPlugin_GetLayerTextureStageCount_m49FB1477D25FB41EACFD8B3B803EA298F4BC1140 ();
// 0x00000223 System.IntPtr OVRPlugin::GetLayerAndroidSurfaceObject(System.Int32)
extern void OVRPlugin_GetLayerAndroidSurfaceObject_mB9749C2A415FF74E34635537EA42DC7B12A6E145 ();
// 0x00000224 System.Boolean OVRPlugin::UpdateNodePhysicsPoses(System.Int32,System.Double)
extern void OVRPlugin_UpdateNodePhysicsPoses_m1FF7E1ABE7DF31F01496FCD11F2202CC71798553 ();
// 0x00000225 OVRPlugin_Posef OVRPlugin::GetNodePose(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodePose_m8E17A9C355D1EA3C8F4D61711D05F11A8C07D7B4 ();
// 0x00000226 OVRPlugin_Vector3f OVRPlugin::GetNodeVelocity(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodeVelocity_m0E4E9D3C7BDAD86EAB8A44B4725253749E796C74 ();
// 0x00000227 OVRPlugin_Vector3f OVRPlugin::GetNodeAngularVelocity(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodeAngularVelocity_mEBD792C57D22CE8A3D207795F94F081C9881D019 ();
// 0x00000228 OVRPlugin_Vector3f OVRPlugin::GetNodeAcceleration(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodeAcceleration_m4E95166E277B33B4F9DDEF62F491109B8AE31560 ();
// 0x00000229 OVRPlugin_Vector3f OVRPlugin::GetNodeAngularAcceleration(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodeAngularAcceleration_m9A023A1EF55501B45CC90C07F9C7853D0C923127 ();
// 0x0000022A System.Boolean OVRPlugin::GetNodePresent(OVRPlugin_Node)
extern void OVRPlugin_GetNodePresent_m2BC4E2F09494A80769D135A9E5D06CE8FEC22874 ();
// 0x0000022B System.Boolean OVRPlugin::GetNodeOrientationTracked(OVRPlugin_Node)
extern void OVRPlugin_GetNodeOrientationTracked_m3E5E013A3D663F0CF0E7C035D7146A49CF068A2F ();
// 0x0000022C System.Boolean OVRPlugin::GetNodeOrientationValid(OVRPlugin_Node)
extern void OVRPlugin_GetNodeOrientationValid_m366551E25AAB8064D95D3E5718AD3BF5E8957CD5 ();
// 0x0000022D System.Boolean OVRPlugin::GetNodePositionTracked(OVRPlugin_Node)
extern void OVRPlugin_GetNodePositionTracked_m436EE2C5F627527458249795360F351113396546 ();
// 0x0000022E System.Boolean OVRPlugin::GetNodePositionValid(OVRPlugin_Node)
extern void OVRPlugin_GetNodePositionValid_m072E3F2BAD71D069475AB85C2621119186C5E0C3 ();
// 0x0000022F OVRPlugin_PoseStatef OVRPlugin::GetNodePoseStateRaw(OVRPlugin_Node,OVRPlugin_Step)
extern void OVRPlugin_GetNodePoseStateRaw_m000A5EDBD619458DFE69065ABE0C5E2AAC0AD6BD ();
// 0x00000230 OVRPlugin_Posef OVRPlugin::GetCurrentTrackingTransformPose()
extern void OVRPlugin_GetCurrentTrackingTransformPose_m6C9F84840F7B578E28D57BB2417B0B012FB78083 ();
// 0x00000231 OVRPlugin_Posef OVRPlugin::GetTrackingTransformRawPose()
extern void OVRPlugin_GetTrackingTransformRawPose_m704B52FDEBA37F9497B07C11E6EBC5192BA87E57 ();
// 0x00000232 OVRPlugin_Posef OVRPlugin::GetTrackingTransformRelativePose(OVRPlugin_TrackingOrigin)
extern void OVRPlugin_GetTrackingTransformRelativePose_m1655AC2D28911E8A8F13A6453573E2A0B723B1A3 ();
// 0x00000233 OVRPlugin_ControllerState OVRPlugin::GetControllerState(System.UInt32)
extern void OVRPlugin_GetControllerState_m33319AD1556B5F03FC935F464684CF7862E4CE5E ();
// 0x00000234 OVRPlugin_ControllerState2 OVRPlugin::GetControllerState2(System.UInt32)
extern void OVRPlugin_GetControllerState2_m86EB19B91A75C94EB0E440460AFF13EE2C698BF3 ();
// 0x00000235 OVRPlugin_ControllerState4 OVRPlugin::GetControllerState4(System.UInt32)
extern void OVRPlugin_GetControllerState4_m0532C6F230A218CEB810EF3349DACCDF2A55497F ();
// 0x00000236 System.Boolean OVRPlugin::SetControllerVibration(System.UInt32,System.Single,System.Single)
extern void OVRPlugin_SetControllerVibration_m5AD5160D0F123599B291905A686345A916084E22 ();
// 0x00000237 OVRPlugin_HapticsDesc OVRPlugin::GetControllerHapticsDesc(System.UInt32)
extern void OVRPlugin_GetControllerHapticsDesc_mC2A686A189AB58F1B505D8DE985DC1B99D11577C ();
// 0x00000238 OVRPlugin_HapticsState OVRPlugin::GetControllerHapticsState(System.UInt32)
extern void OVRPlugin_GetControllerHapticsState_m328F5D227817DDED13645D47BE19BCF59525305C ();
// 0x00000239 System.Boolean OVRPlugin::SetControllerHaptics(System.UInt32,OVRPlugin_HapticsBuffer)
extern void OVRPlugin_SetControllerHaptics_m3CAEE3E26EA82254EEAB05ACE29A2A0B5872F5FA ();
// 0x0000023A System.Single OVRPlugin::GetEyeRecommendedResolutionScale()
extern void OVRPlugin_GetEyeRecommendedResolutionScale_m296EAAFF9057A7570C007E1C7323E48F298256E2 ();
// 0x0000023B System.Single OVRPlugin::GetAppCpuStartToGpuEndTime()
extern void OVRPlugin_GetAppCpuStartToGpuEndTime_m6ECBEDFEA85313DFD842693DA138101F2AE536C2 ();
// 0x0000023C System.Boolean OVRPlugin::GetBoundaryConfigured()
extern void OVRPlugin_GetBoundaryConfigured_mD2841B3BBCD935D91E6DAEC6FC1F264908C2BACC ();
// 0x0000023D OVRPlugin_BoundaryTestResult OVRPlugin::TestBoundaryNode(OVRPlugin_Node,OVRPlugin_BoundaryType)
extern void OVRPlugin_TestBoundaryNode_m34EBBC52B5387BFC8A22B1A919D79A6617656639 ();
// 0x0000023E OVRPlugin_BoundaryTestResult OVRPlugin::TestBoundaryPoint(OVRPlugin_Vector3f,OVRPlugin_BoundaryType)
extern void OVRPlugin_TestBoundaryPoint_mF663F28191FE448A4290C8A1134FC2FA65696054 ();
// 0x0000023F OVRPlugin_BoundaryGeometry OVRPlugin::GetBoundaryGeometry(OVRPlugin_BoundaryType)
extern void OVRPlugin_GetBoundaryGeometry_m82BEE773606B284084CB6B12CB8F3385D1165376 ();
// 0x00000240 System.Boolean OVRPlugin::GetBoundaryGeometry2(OVRPlugin_BoundaryType,System.IntPtr,System.Int32&)
extern void OVRPlugin_GetBoundaryGeometry2_mB81B5788856F3E0ECB819CCA5EFDDBC03FF804BA ();
// 0x00000241 OVRPlugin_AppPerfStats OVRPlugin::GetAppPerfStats()
extern void OVRPlugin_GetAppPerfStats_m88FB26784D4E5818A92F0EE7664A7C44A1D26798 ();
// 0x00000242 System.Boolean OVRPlugin::ResetAppPerfStats()
extern void OVRPlugin_ResetAppPerfStats_m09307AEF6BBD8BC41F8B6EC6B1693E02AEAE11C3 ();
// 0x00000243 System.Single OVRPlugin::GetAppFramerate()
extern void OVRPlugin_GetAppFramerate_mABAD5A920BD49CA405CE59C081964B4254006E20 ();
// 0x00000244 System.Boolean OVRPlugin::SetHandNodePoseStateLatency(System.Double)
extern void OVRPlugin_SetHandNodePoseStateLatency_mA9BB8C47B48AFDFF4D91CB008CD1B54FA8E657A6 ();
// 0x00000245 System.Double OVRPlugin::GetHandNodePoseStateLatency()
extern void OVRPlugin_GetHandNodePoseStateLatency_mF531430D816D66C877049DE9F20A1A573290F99A ();
// 0x00000246 OVRPlugin_EyeTextureFormat OVRPlugin::GetDesiredEyeTextureFormat()
extern void OVRPlugin_GetDesiredEyeTextureFormat_mC689F281E14E8284CF5CC90D13D619289D3B6F80 ();
// 0x00000247 System.Boolean OVRPlugin::SetDesiredEyeTextureFormat(OVRPlugin_EyeTextureFormat)
extern void OVRPlugin_SetDesiredEyeTextureFormat_mBC33132FC6DF8CC438A0589E3D0B273C00D7BEF2 ();
// 0x00000248 System.Boolean OVRPlugin::InitializeMixedReality()
extern void OVRPlugin_InitializeMixedReality_mED45B2B983DA9885D9E2B6502BC88E3B3C4D3B20 ();
// 0x00000249 System.Boolean OVRPlugin::ShutdownMixedReality()
extern void OVRPlugin_ShutdownMixedReality_m317FF312A0F19C8CDA496F07418A97785FA8BCB1 ();
// 0x0000024A System.Boolean OVRPlugin::IsMixedRealityInitialized()
extern void OVRPlugin_IsMixedRealityInitialized_mD500325BE1DD0860CC86C0C20E360AF70F933DE6 ();
// 0x0000024B System.Int32 OVRPlugin::GetExternalCameraCount()
extern void OVRPlugin_GetExternalCameraCount_m052D2022ECCC13FC6C0FCAD86D4AB8BAF3C7A665 ();
// 0x0000024C System.Boolean OVRPlugin::UpdateExternalCamera()
extern void OVRPlugin_UpdateExternalCamera_m20AE9EFC6A4B4E7C964933B83F1F3714791DA963 ();
// 0x0000024D System.Boolean OVRPlugin::GetMixedRealityCameraInfo(System.Int32,OVRPlugin_CameraExtrinsics&,OVRPlugin_CameraIntrinsics&,OVRPlugin_Posef&)
extern void OVRPlugin_GetMixedRealityCameraInfo_m612910B4EE99526DEBC27C1AF3134C1208ACEE15 ();
// 0x0000024E OVRPlugin_Vector3f OVRPlugin::GetBoundaryDimensions(OVRPlugin_BoundaryType)
extern void OVRPlugin_GetBoundaryDimensions_m77D2E3C3020151115C11B6AD841166EBF80416B9 ();
// 0x0000024F System.Boolean OVRPlugin::GetBoundaryVisible()
extern void OVRPlugin_GetBoundaryVisible_m5F0C29D06ECA6B229ABA6287888B73EBCEBA0409 ();
// 0x00000250 System.Boolean OVRPlugin::SetBoundaryVisible(System.Boolean)
extern void OVRPlugin_SetBoundaryVisible_m85F8141A83704C7BE67DD4DD92A7B6B6BAB00C9D ();
// 0x00000251 OVRPlugin_SystemHeadset OVRPlugin::GetSystemHeadsetType()
extern void OVRPlugin_GetSystemHeadsetType_mF98D9E09E27729E88645A1644B66840E20A5A0EB ();
// 0x00000252 OVRPlugin_Controller OVRPlugin::GetActiveController()
extern void OVRPlugin_GetActiveController_m9745C9E3F9C5CFC7F7B308F2D3C0DA463055577D ();
// 0x00000253 OVRPlugin_Controller OVRPlugin::GetConnectedControllers()
extern void OVRPlugin_GetConnectedControllers_m8E27DDA4FBAE4058BDDE6A308DD20B44A09F1739 ();
// 0x00000254 OVRPlugin_Bool OVRPlugin::ToBool(System.Boolean)
extern void OVRPlugin_ToBool_m8EDCF3BDDED6AC1FC142C1FE6B536127B70BF2D7 ();
// 0x00000255 OVRPlugin_TrackingOrigin OVRPlugin::GetTrackingOriginType()
extern void OVRPlugin_GetTrackingOriginType_m46D172CDB9E440588A9616665F62F263F7A292C1 ();
// 0x00000256 System.Boolean OVRPlugin::SetTrackingOriginType(OVRPlugin_TrackingOrigin)
extern void OVRPlugin_SetTrackingOriginType_m083C10AA6533612EA760CFAEB0EBD5118E76BBD3 ();
// 0x00000257 OVRPlugin_Posef OVRPlugin::GetTrackingCalibratedOrigin()
extern void OVRPlugin_GetTrackingCalibratedOrigin_mAACD6689ED95DDCBEA89362F3B190454B7B925B1 ();
// 0x00000258 System.Boolean OVRPlugin::SetTrackingCalibratedOrigin()
extern void OVRPlugin_SetTrackingCalibratedOrigin_m4AFB8183DF1BFC4F6CC229A29045A69291666AF4 ();
// 0x00000259 System.Boolean OVRPlugin::RecenterTrackingOrigin(OVRPlugin_RecenterFlags)
extern void OVRPlugin_RecenterTrackingOrigin_mF85344F36B3B67AB6EC6255BD447A71C42C55238 ();
// 0x0000025A System.Boolean OVRPlugin::get_fixedFoveatedRenderingSupported()
extern void OVRPlugin_get_fixedFoveatedRenderingSupported_m4AD027704502D11B747374ABB8744A7FBB713F5C ();
// 0x0000025B OVRPlugin_FixedFoveatedRenderingLevel OVRPlugin::get_fixedFoveatedRenderingLevel()
extern void OVRPlugin_get_fixedFoveatedRenderingLevel_mBBFB81B5306788FDF943123B77B8E9947F9FD3C2 ();
// 0x0000025C System.Void OVRPlugin::set_fixedFoveatedRenderingLevel(OVRPlugin_FixedFoveatedRenderingLevel)
extern void OVRPlugin_set_fixedFoveatedRenderingLevel_m662564421557C9089301412892D600898931A556 ();
// 0x0000025D System.Boolean OVRPlugin::get_tiledMultiResSupported()
extern void OVRPlugin_get_tiledMultiResSupported_m35D05CDE175B0151F58D3FB06E147F763BE3AE34 ();
// 0x0000025E OVRPlugin_TiledMultiResLevel OVRPlugin::get_tiledMultiResLevel()
extern void OVRPlugin_get_tiledMultiResLevel_m0A3E0F393A7288E2E9DC1596D1BE7CC890AC83CB ();
// 0x0000025F System.Void OVRPlugin::set_tiledMultiResLevel(OVRPlugin_TiledMultiResLevel)
extern void OVRPlugin_set_tiledMultiResLevel_m3D938445562C8073E2D8B9AC533D6FE5995D7CB2 ();
// 0x00000260 System.Boolean OVRPlugin::get_gpuUtilSupported()
extern void OVRPlugin_get_gpuUtilSupported_mD1E48D8B2A982DC5E6CAC0097CE9806A50278017 ();
// 0x00000261 System.Single OVRPlugin::get_gpuUtilLevel()
extern void OVRPlugin_get_gpuUtilLevel_mB2CC6C6BA200A0C5B4ED2030AD295ED8C4F3398B ();
// 0x00000262 System.Single[] OVRPlugin::get_systemDisplayFrequenciesAvailable()
extern void OVRPlugin_get_systemDisplayFrequenciesAvailable_m6AE8A36131A28AD77332CF64AC57A1A20233BB71 ();
// 0x00000263 System.Single OVRPlugin::get_systemDisplayFrequency()
extern void OVRPlugin_get_systemDisplayFrequency_m208A930A385234253D0C42E7976FCDA49670E5C6 ();
// 0x00000264 System.Void OVRPlugin::set_systemDisplayFrequency(System.Single)
extern void OVRPlugin_set_systemDisplayFrequency_m4D6D72581C79E6EE0278A3E5E42A6596D86A3569 ();
// 0x00000265 System.Boolean OVRPlugin::GetNodeFrustum2(OVRPlugin_Node,OVRPlugin_Frustumf2&)
extern void OVRPlugin_GetNodeFrustum2_mE5D3787585754BC1D6F89F4123DA6001EFF8FC0A ();
// 0x00000266 System.Boolean OVRPlugin::get_AsymmetricFovEnabled()
extern void OVRPlugin_get_AsymmetricFovEnabled_mC7863F3E087243063D719F03401156D78E8F6399 ();
// 0x00000267 System.Boolean OVRPlugin::get_EyeTextureArrayEnabled()
extern void OVRPlugin_get_EyeTextureArrayEnabled_m0CF09B463DCD9EAD79D7CFB35C88358E3DD3DFE2 ();
// 0x00000268 OVRPlugin_Handedness OVRPlugin::GetDominantHand()
extern void OVRPlugin_GetDominantHand_m2EEFE95D8510986C2C08E3231794014B74ECA93B ();
// 0x00000269 System.Boolean OVRPlugin::GetReorientHMDOnControllerRecenter()
extern void OVRPlugin_GetReorientHMDOnControllerRecenter_mA2C7EA9DC5CCC5056670E5B9EB3BCFCFA13F7DC6 ();
// 0x0000026A System.Boolean OVRPlugin::SetReorientHMDOnControllerRecenter(System.Boolean)
extern void OVRPlugin_SetReorientHMDOnControllerRecenter_m31F2721F1EFBBA38958A5A225D3DF0A351EDED76 ();
// 0x0000026B System.Boolean OVRPlugin::SendEvent(System.String,System.String,System.String)
extern void OVRPlugin_SendEvent_mC013F62F65AF33B6421E1D8A4A0BDEE5AC537778 ();
// 0x0000026C System.Boolean OVRPlugin::SetHeadPoseModifier(OVRPlugin_Quatf&,OVRPlugin_Vector3f&)
extern void OVRPlugin_SetHeadPoseModifier_m1AE42678B92BE1FEC7CCD8EC96478890878BF566 ();
// 0x0000026D System.Boolean OVRPlugin::GetHeadPoseModifier(OVRPlugin_Quatf&,OVRPlugin_Vector3f&)
extern void OVRPlugin_GetHeadPoseModifier_m297D517CB172AD2CF3A037C4084DA4238161F329 ();
// 0x0000026E System.Boolean OVRPlugin::IsPerfMetricsSupported(OVRPlugin_PerfMetrics)
extern void OVRPlugin_IsPerfMetricsSupported_m410EB38D0A8922C14FACE593B2B7DEAED73E4426 ();
// 0x0000026F System.Nullable`1<System.Single> OVRPlugin::GetPerfMetricsFloat(OVRPlugin_PerfMetrics)
extern void OVRPlugin_GetPerfMetricsFloat_m9581366E9BA559C4B1ED2CA1B78245DACADC0AF1 ();
// 0x00000270 System.Nullable`1<System.Int32> OVRPlugin::GetPerfMetricsInt(OVRPlugin_PerfMetrics)
extern void OVRPlugin_GetPerfMetricsInt_m7C48BA5972D09980A9D17AF66C79DC248698E6CC ();
// 0x00000271 System.Double OVRPlugin::GetTimeInSeconds()
extern void OVRPlugin_GetTimeInSeconds_m7D0A99C37EB7C5AC0C3F2D7EBB6B28B34BCF03DF ();
// 0x00000272 System.Boolean OVRPlugin::SetColorScaleAndOffset(UnityEngine.Vector4,UnityEngine.Vector4,System.Boolean)
extern void OVRPlugin_SetColorScaleAndOffset_m4E5067FBA1D8413EDB9EEE54A0E67151F1669DEF ();
// 0x00000273 System.Boolean OVRPlugin::AddCustomMetadata(System.String,System.String)
extern void OVRPlugin_AddCustomMetadata_mB2EF38F10EC62F3B63CB93A364BC342B461BBB53 ();
// 0x00000274 System.Boolean OVRPlugin::SetDeveloperMode(OVRPlugin_Bool)
extern void OVRPlugin_SetDeveloperMode_m8FB3CB2325C2D916DF720967271A6348951E454E ();
// 0x00000275 System.Void OVRPlugin::.cctor()
extern void OVRPlugin__cctor_mB76764AC9D1F9EAC44550E0645E4E610336E3C69 ();
// 0x00000276 System.Void OVRPlugin_GUID::.ctor()
extern void GUID__ctor_mB41A593938E3F6E09CD8DD70E19A065AAF72216C ();
// 0x00000277 System.String OVRPlugin_Vector3f::ToString()
extern void Vector3f_ToString_m52F5DF2083B0F4B8CE7AE59E92E8947971D48240_AdjustorThunk ();
// 0x00000278 System.Void OVRPlugin_Vector3f::.cctor()
extern void Vector3f__cctor_mB31252A637DD589E8F052DE55AA1C4B3B8A6A7ED ();
// 0x00000279 System.String OVRPlugin_Quatf::ToString()
extern void Quatf_ToString_m8A49887F29F66BF5CAC155958941771F0AEA3E7E_AdjustorThunk ();
// 0x0000027A System.Void OVRPlugin_Quatf::.cctor()
extern void Quatf__cctor_m66765639C6821E433F25FF1208FDA86880FEE2DA ();
// 0x0000027B System.String OVRPlugin_Posef::ToString()
extern void Posef_ToString_m53165D004C9025E97DDBAF497A5E5097615CA0E9_AdjustorThunk ();
// 0x0000027C System.Void OVRPlugin_Posef::.cctor()
extern void Posef__cctor_m5416229378AC93676B54FCCFD32F9064AD11A503 ();
// 0x0000027D System.String OVRPlugin_TextureRectMatrixf::ToString()
extern void TextureRectMatrixf_ToString_m86B06425AF311C178BA27F4B362FD77B95A7B8C5_AdjustorThunk ();
// 0x0000027E System.Void OVRPlugin_TextureRectMatrixf::.cctor()
extern void TextureRectMatrixf__cctor_m1208362CB610A1AEB2B4B7E50BAFC001773169BF ();
// 0x0000027F System.Void OVRPlugin_PoseStatef::.cctor()
extern void PoseStatef__cctor_mB723CAE0F82C4634B13687F392BD7DCD8B9E7A32 ();
// 0x00000280 System.Void OVRPlugin_ControllerState4::.ctor(OVRPlugin_ControllerState2)
extern void ControllerState4__ctor_m6EBB9E6A88945E4FCAB3BCF70D6B0A3C719B2218_AdjustorThunk ();
// 0x00000281 System.Void OVRPlugin_ControllerState2::.ctor(OVRPlugin_ControllerState)
extern void ControllerState2__ctor_mE197436281EA28931E3EA0ABC787FA163ED49545_AdjustorThunk ();
// 0x00000282 System.Void OVRPlugin_Sizei::.cctor()
extern void Sizei__cctor_mBE8606E15C1CD0DE04FADA3467A3C64C9D74FCEC ();
// 0x00000283 System.Void OVRPlugin_Sizef::.cctor()
extern void Sizef__cctor_m6569C2BBB4B2551AED754AC807A3B10DFF881008 ();
// 0x00000284 System.String OVRPlugin_LayerDesc::ToString()
extern void LayerDesc_ToString_m0D64A316B93226028E7E28A447FACEED02C7C234_AdjustorThunk ();
// 0x00000285 System.Boolean OVRPlugin_Media::Initialize()
extern void Media_Initialize_m110EE9A132C73E287DF471DD98FE1CDF28980D50 ();
// 0x00000286 System.Boolean OVRPlugin_Media::Shutdown()
extern void Media_Shutdown_mA1E27A8F792246C916C50FD199C31F2D781EEBC8 ();
// 0x00000287 System.Boolean OVRPlugin_Media::GetInitialized()
extern void Media_GetInitialized_mFF74ED9A9E1F70355A08E1EB1070727B1E7CF5AC ();
// 0x00000288 System.Boolean OVRPlugin_Media::Update()
extern void Media_Update_m79D255C18B4423799B11AA648F71C86E44D8AE03 ();
// 0x00000289 OVRPlugin_Media_MrcActivationMode OVRPlugin_Media::GetMrcActivationMode()
extern void Media_GetMrcActivationMode_m82704E4755C576CBFAEB056448DECC2CC7341E30 ();
// 0x0000028A System.Boolean OVRPlugin_Media::SetMrcActivationMode(OVRPlugin_Media_MrcActivationMode)
extern void Media_SetMrcActivationMode_m2478FD333BE8C1DC01D6637C5173D16C6EB8D49D ();
// 0x0000028B System.Boolean OVRPlugin_Media::IsMrcEnabled()
extern void Media_IsMrcEnabled_m3D1FBBD6A08ADB94B52D703DE471AD81CBFD660A ();
// 0x0000028C System.Boolean OVRPlugin_Media::IsMrcActivated()
extern void Media_IsMrcActivated_m11C79E99CA3F824D08A2529C743444566BA2EEE9 ();
// 0x0000028D System.Boolean OVRPlugin_Media::UseMrcDebugCamera()
extern void Media_UseMrcDebugCamera_m83308CB20311C0FA1949EF40A9185C1750401664 ();
// 0x0000028E System.Boolean OVRPlugin_Media::SetMrcInputVideoBufferType(OVRPlugin_Media_InputVideoBufferType)
extern void Media_SetMrcInputVideoBufferType_m7A4451CB98F212E4A0275428AF89B3F471624184 ();
// 0x0000028F OVRPlugin_Media_InputVideoBufferType OVRPlugin_Media::GetMrcInputVideoBufferType()
extern void Media_GetMrcInputVideoBufferType_mB075060863D22D392623C25D0D56BC463C3D676A ();
// 0x00000290 System.Boolean OVRPlugin_Media::SetMrcFrameSize(System.Int32,System.Int32)
extern void Media_SetMrcFrameSize_mA63E3989BAE333E979048F080D216CCCE9FBB885 ();
// 0x00000291 System.Void OVRPlugin_Media::GetMrcFrameSize(System.Int32&,System.Int32&)
extern void Media_GetMrcFrameSize_m2D251EDBF48690D30A8C8F9CB1243B4B573F1C61 ();
// 0x00000292 System.Boolean OVRPlugin_Media::SetMrcAudioSampleRate(System.Int32)
extern void Media_SetMrcAudioSampleRate_mE474E40D625718FD0E1B02ABC1C4CE8750D0CB87 ();
// 0x00000293 System.Int32 OVRPlugin_Media::GetMrcAudioSampleRate()
extern void Media_GetMrcAudioSampleRate_m0FDB114C9CA0E57E093D4801213894FEA2A71603 ();
// 0x00000294 System.Boolean OVRPlugin_Media::SetMrcFrameImageFlipped(System.Boolean)
extern void Media_SetMrcFrameImageFlipped_m8B0CFED72A146031A7F390DA10694FB99E77E2CE ();
// 0x00000295 System.Boolean OVRPlugin_Media::GetMrcFrameImageFlipped()
extern void Media_GetMrcFrameImageFlipped_mEF19E8EDCD9376CC2516BE69B59FA6FE0663417D ();
// 0x00000296 System.Boolean OVRPlugin_Media::EncodeMrcFrame(System.IntPtr,System.Single[],System.Int32,System.Double,System.Int32&)
extern void Media_EncodeMrcFrame_mF6E6D3997B1983375B04DC88AD2C8AC11B51B080 ();
// 0x00000297 System.Boolean OVRPlugin_Media::EncodeMrcFrame(UnityEngine.RenderTexture,System.Single[],System.Int32,System.Double,System.Int32&)
extern void Media_EncodeMrcFrame_m08AF783BD90950237D48F85598BE2B0EC1E1ADA4 ();
// 0x00000298 System.Boolean OVRPlugin_Media::SyncMrcFrame(System.Int32)
extern void Media_SyncMrcFrame_m4699985766B31D8CD69D1C2A7D5BA8557B1B559E ();
// 0x00000299 System.Void OVRPlugin_Media::.ctor()
extern void Media__ctor_mA4CE69A55B97638F9F96996AA555E9D950F572F8 ();
// 0x0000029A System.String OVRProfile::get_id()
extern void OVRProfile_get_id_mF6B9F2B1657033A53D7FCACB3BCA4A5D463E1142 ();
// 0x0000029B System.String OVRProfile::get_userName()
extern void OVRProfile_get_userName_m22EA9ACF72E83B5E1AD453CFB856ABEDCA387DEB ();
// 0x0000029C System.String OVRProfile::get_locale()
extern void OVRProfile_get_locale_m952EEADD042F1176AFEB9137656D2E56DB84EC3A ();
// 0x0000029D System.Single OVRProfile::get_ipd()
extern void OVRProfile_get_ipd_mBAD219728CDB9641BCB4621F9E58A7C723D0E485 ();
// 0x0000029E System.Single OVRProfile::get_eyeHeight()
extern void OVRProfile_get_eyeHeight_m700ABDDD88905E629CD73DBDB3BC9D8A05248C17 ();
// 0x0000029F System.Single OVRProfile::get_eyeDepth()
extern void OVRProfile_get_eyeDepth_m6A7F6837846068E10E2E9A976386B693B7246A04 ();
// 0x000002A0 System.Single OVRProfile::get_neckHeight()
extern void OVRProfile_get_neckHeight_m0EF4F8D9ABF863A68BEF010A98BD1C9DF2F31DC8 ();
// 0x000002A1 OVRProfile_State OVRProfile::get_state()
extern void OVRProfile_get_state_mF3E526492ACC893F81D30EE98AB656C7AD3A4DE4 ();
// 0x000002A2 System.Void OVRProfile::.ctor()
extern void OVRProfile__ctor_mE5BBAB7A4F3C262DAF1D2E0A353DA28F84CC03BB ();
// 0x000002A3 System.Boolean OVRTracker::get_isPresent()
extern void OVRTracker_get_isPresent_m22FF23801E88857AAEB8F79B60C30D189ACCF83D ();
// 0x000002A4 System.Boolean OVRTracker::get_isPositionTracked()
extern void OVRTracker_get_isPositionTracked_mDF1A07176116F8108D84687EAEB0014E1032E6EB ();
// 0x000002A5 System.Boolean OVRTracker::get_isEnabled()
extern void OVRTracker_get_isEnabled_m5FCDBFEADD8BDD0E6EA49C2EF8863CBFC884AD1C ();
// 0x000002A6 System.Void OVRTracker::set_isEnabled(System.Boolean)
extern void OVRTracker_set_isEnabled_m9A79824010DCF1A79715085FCF4E3410B28583B2 ();
// 0x000002A7 System.Int32 OVRTracker::get_count()
extern void OVRTracker_get_count_mA452A77BEA0B84A36CC4595BE8CFD2AA2A9912A6 ();
// 0x000002A8 OVRTracker_Frustum OVRTracker::GetFrustum(System.Int32)
extern void OVRTracker_GetFrustum_m22ACF82F788AFFA64BC98726CC59D0FE67E53B58 ();
// 0x000002A9 OVRPose OVRTracker::GetPose(System.Int32)
extern void OVRTracker_GetPose_m18A4C0458E4AE6AC3D12B2A52DD8C957D6D1DDD7 ();
// 0x000002AA System.Boolean OVRTracker::GetPoseValid(System.Int32)
extern void OVRTracker_GetPoseValid_mF540E6BD8912B335237F3491B3FC4BBFBE990E35 ();
// 0x000002AB System.Boolean OVRTracker::GetPresent(System.Int32)
extern void OVRTracker_GetPresent_mE13F140DF9F5F558E57F882F05E53514151F43A6 ();
// 0x000002AC System.Void OVRTracker::.ctor()
extern void OVRTracker__ctor_m9A28691E308F96818A71BBA542B2E23FB701BEB4 ();
// 0x000002AD System.Void OVRAudioSourceTest::Start()
extern void OVRAudioSourceTest_Start_mB1A8057A0BE8267BF8C72C5F8C7F27A42D9DBACB ();
// 0x000002AE System.Void OVRAudioSourceTest::Update()
extern void OVRAudioSourceTest_Update_mA9FD2DDAC817707E7E3AA9ACD83C3FDD853B719B ();
// 0x000002AF System.Void OVRAudioSourceTest::.ctor()
extern void OVRAudioSourceTest__ctor_m1F7B9F8963600D1EFBC12EBD0066674A3147A552 ();
// 0x000002B0 System.Void OVRChromaticAberration::Start()
extern void OVRChromaticAberration_Start_m2D11EC425F5D5B92C4AEC7F5217C7F1D0054A0EE ();
// 0x000002B1 System.Void OVRChromaticAberration::Update()
extern void OVRChromaticAberration_Update_m990F9764B1A447A4F95E5766EF70B2CABF23661D ();
// 0x000002B2 System.Void OVRChromaticAberration::.ctor()
extern void OVRChromaticAberration__ctor_m886C9F6567B2EDD3BA36DDCCCE301E3C89A6C9A5 ();
// 0x000002B3 System.Void OVRControllerHelper::Start()
extern void OVRControllerHelper_Start_m8174B79E134F3E2BCC2AF841BE81FA345BDBD1D8 ();
// 0x000002B4 System.Void OVRControllerHelper::Update()
extern void OVRControllerHelper_Update_m4FD9DE5966FEE9776FBC399F8D6D4E4FF1BA842C ();
// 0x000002B5 System.Void OVRControllerHelper::.ctor()
extern void OVRControllerHelper__ctor_m89D06B34BEB6CFF68A6C124E91148B9E44D01EE0 ();
// 0x000002B6 System.Void OVRCubemapCapture::Update()
extern void OVRCubemapCapture_Update_m519D9FBF324882EB42CA3354FF73DC08B1D52DC4 ();
// 0x000002B7 System.Void OVRCubemapCapture::TriggerCubemapCapture(UnityEngine.Vector3,System.Int32,System.String)
extern void OVRCubemapCapture_TriggerCubemapCapture_m249C89D7387A8F50635512025B99348CA2882171 ();
// 0x000002B8 System.Void OVRCubemapCapture::RenderIntoCubemap(UnityEngine.Camera,UnityEngine.Cubemap)
extern void OVRCubemapCapture_RenderIntoCubemap_mFE06AAACA3FC4F53B524844333D4A37C3DF9D139 ();
// 0x000002B9 System.Boolean OVRCubemapCapture::SaveCubemapCapture(UnityEngine.Cubemap,System.String)
extern void OVRCubemapCapture_SaveCubemapCapture_mC269B7EBFC062AD561D7DE34F2CB7F0368D71B28 ();
// 0x000002BA System.Void OVRCubemapCapture::.ctor()
extern void OVRCubemapCapture__ctor_m3B4CF453D98FF1B6BD9D7F708934FC9BAD1C9D93 ();
// 0x000002BB System.Void OVRCursor::SetCursorRay(UnityEngine.Transform)
// 0x000002BC System.Void OVRCursor::SetCursorStartDest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
// 0x000002BD System.Void OVRCursor::.ctor()
extern void OVRCursor__ctor_m6F6920D21D4918B96658ACA251E6B34D636CA237 ();
// 0x000002BE System.Void OVRDebugInfo::Awake()
extern void OVRDebugInfo_Awake_m0959C6BF0B8C661548C49FF452916500AFACE05C ();
// 0x000002BF System.Void OVRDebugInfo::Update()
extern void OVRDebugInfo_Update_mA8F99324C92A1232AB35C3DE71BBE819F42EAF2C ();
// 0x000002C0 System.Void OVRDebugInfo::OnDestroy()
extern void OVRDebugInfo_OnDestroy_m0B2CA7FF62E4EBD0CAE37F47FE85E5A41B264183 ();
// 0x000002C1 System.Void OVRDebugInfo::InitUIComponents()
extern void OVRDebugInfo_InitUIComponents_m4C2D569AC7195FB227803C1410F0126CD18DE972 ();
// 0x000002C2 System.Void OVRDebugInfo::UpdateVariable()
extern void OVRDebugInfo_UpdateVariable_m1B782E9B84463FC84F8E5F04589B15367CC1321C ();
// 0x000002C3 System.Void OVRDebugInfo::UpdateStrings()
extern void OVRDebugInfo_UpdateStrings_m1495AAD95AF6C2CAD4FA5A68CB82799C4F121138 ();
// 0x000002C4 System.Void OVRDebugInfo::RiftPresentGUI(UnityEngine.GameObject)
extern void OVRDebugInfo_RiftPresentGUI_m1D46CBC279389D9217B7782F54B9C36654694B91 ();
// 0x000002C5 System.Void OVRDebugInfo::UpdateDeviceDetection()
extern void OVRDebugInfo_UpdateDeviceDetection_mBE2D48F966B4436C428D0AB5CB840FD040687B08 ();
// 0x000002C6 UnityEngine.GameObject OVRDebugInfo::VariableObjectManager(UnityEngine.GameObject,System.String,System.Single,System.String,System.Int32)
extern void OVRDebugInfo_VariableObjectManager_m62A43FB195A7519A5803CB989E8DBB33AFCA0322 ();
// 0x000002C7 UnityEngine.GameObject OVRDebugInfo::ComponentComposition(UnityEngine.GameObject)
extern void OVRDebugInfo_ComponentComposition_m5B22234BABFA46E7312990BC3F382DAC0FC904E6 ();
// 0x000002C8 System.Void OVRDebugInfo::UpdateIPD()
extern void OVRDebugInfo_UpdateIPD_m3D65C1427A56C9A86AD89764BC4520C8927E8266 ();
// 0x000002C9 System.Void OVRDebugInfo::UpdateEyeHeightOffset()
extern void OVRDebugInfo_UpdateEyeHeightOffset_mB05494A3F1418A7C2D4DD750DC06AFBC5391C4C8 ();
// 0x000002CA System.Void OVRDebugInfo::UpdateEyeDepthOffset()
extern void OVRDebugInfo_UpdateEyeDepthOffset_m900B121BF0EBA2343FA03ED8E93A9005DF921E24 ();
// 0x000002CB System.Void OVRDebugInfo::UpdateFOV()
extern void OVRDebugInfo_UpdateFOV_m395AEB87AF5D9A16165C70D07221F5501B653533 ();
// 0x000002CC System.Void OVRDebugInfo::UpdateResolutionEyeTexture()
extern void OVRDebugInfo_UpdateResolutionEyeTexture_mA7EAD7AAEEE088195DF317563046A69D5C3589ED ();
// 0x000002CD System.Void OVRDebugInfo::UpdateLatencyValues()
extern void OVRDebugInfo_UpdateLatencyValues_m525106BA0478918283BCEC28B08EF67DD4C4796F ();
// 0x000002CE System.Void OVRDebugInfo::UpdateFPS()
extern void OVRDebugInfo_UpdateFPS_m9C3E0E797BA2939F4A7C8092719FE5F2AFF4EC24 ();
// 0x000002CF System.Void OVRDebugInfo::.ctor()
extern void OVRDebugInfo__ctor_mF380334B95800EB1A3E085EA670A8B052EF3245B ();
// 0x000002D0 System.Boolean OVRGazePointer::get_hidden()
extern void OVRGazePointer_get_hidden_m1CC04CD9F7142893A8EBCB7036714747B37D0F6E ();
// 0x000002D1 System.Void OVRGazePointer::set_hidden(System.Boolean)
extern void OVRGazePointer_set_hidden_m91028FD28F5623B27B97401727AF7F363B785AF6 ();
// 0x000002D2 System.Single OVRGazePointer::get_currentScale()
extern void OVRGazePointer_get_currentScale_m5051A8B8D60AAA1AFBE4D0DEDCA126834479092C ();
// 0x000002D3 System.Void OVRGazePointer::set_currentScale(System.Single)
extern void OVRGazePointer_set_currentScale_m0BB5E81BF2951D8CE84331E5773DAB7ADC13B506 ();
// 0x000002D4 OVRGazePointer OVRGazePointer::get_instance()
extern void OVRGazePointer_get_instance_mB814E0429576CD0768FE8E6BC4F1DEF39BA1F696 ();
// 0x000002D5 System.Single OVRGazePointer::get_visibilityStrength()
extern void OVRGazePointer_get_visibilityStrength_mDB9C9C1AA471FD4879108A6619F3081789D83AA8 ();
// 0x000002D6 System.Single OVRGazePointer::get_SelectionProgress()
extern void OVRGazePointer_get_SelectionProgress_m23301CAC39B254665615AB2043ACA9515DAAEB81 ();
// 0x000002D7 System.Void OVRGazePointer::set_SelectionProgress(System.Single)
extern void OVRGazePointer_set_SelectionProgress_m681026E6FFEEAC87247F5CD70E2D257D68C8AA35 ();
// 0x000002D8 System.Void OVRGazePointer::Awake()
extern void OVRGazePointer_Awake_mB16AD7EBDBEF175B83BBF294EAF75E54B2494E32 ();
// 0x000002D9 System.Void OVRGazePointer::Update()
extern void OVRGazePointer_Update_m47B6CA68B973ABD7DD50A089DD0828E5D51A66F8 ();
// 0x000002DA System.Void OVRGazePointer::SetCursorStartDest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void OVRGazePointer_SetCursorStartDest_m66BE5DAB68F34F03CE012B29BA1100DDA7C74378 ();
// 0x000002DB System.Void OVRGazePointer::SetCursorRay(UnityEngine.Transform)
extern void OVRGazePointer_SetCursorRay_m6BFB06845441CDA570D4FD6CB255D2488DBE8923 ();
// 0x000002DC System.Void OVRGazePointer::LateUpdate()
extern void OVRGazePointer_LateUpdate_mAB1945271CFB531CA5ED4BC960038F1869DA197A ();
// 0x000002DD System.Void OVRGazePointer::RequestHide()
extern void OVRGazePointer_RequestHide_m5BFAC46F71DB65CED6B99974401F602B03F9EC8D ();
// 0x000002DE System.Void OVRGazePointer::RequestShow()
extern void OVRGazePointer_RequestShow_m283EF20BD04309581CD8FDADFEB48E06BEB17F75 ();
// 0x000002DF System.Void OVRGazePointer::Hide()
extern void OVRGazePointer_Hide_m5DD0A17F041B0ED3CAA74056E06BC41F56D7F092 ();
// 0x000002E0 System.Void OVRGazePointer::Show()
extern void OVRGazePointer_Show_m37CF988CF6C07C90823B78715D7BD439C4A025AC ();
// 0x000002E1 System.Void OVRGazePointer::.ctor()
extern void OVRGazePointer__ctor_mA7772B90D2A107BDAD261B37EF7A548FE6EAB358 ();
// 0x000002E2 System.Void OVRGearVrControllerTest::Start()
extern void OVRGearVrControllerTest_Start_mF4C8D0A5BD562179C62EC446ACF3C7E9941BE475 ();
// 0x000002E3 System.Void OVRGearVrControllerTest::Update()
extern void OVRGearVrControllerTest_Update_m0F28590EAE690EDEBAC33B5A99201FFE0C541E60 ();
// 0x000002E4 System.Void OVRGearVrControllerTest::.ctor()
extern void OVRGearVrControllerTest__ctor_mB7172FA925271BEF6344AE7099DEDA8492A993D4 ();
// 0x000002E5 System.Void OVRGearVrControllerTest::.cctor()
extern void OVRGearVrControllerTest__cctor_m6E5846C09FF582606C6E3B5D278F91521597935B ();
// 0x000002E6 System.Void OVRGearVrControllerTest_BoolMonitor::.ctor(System.String,OVRGearVrControllerTest_BoolMonitor_BoolGenerator,System.Single)
extern void BoolMonitor__ctor_m88D8A874667D1A4A5EE7CFE065C90CD22E0767C7 ();
// 0x000002E7 System.Void OVRGearVrControllerTest_BoolMonitor::Update()
extern void BoolMonitor_Update_m643614BB078842DF7BDF42A119D07CA4FC12F4A7 ();
// 0x000002E8 System.Void OVRGearVrControllerTest_BoolMonitor::AppendToStringBuilder(System.Text.StringBuilder&)
extern void BoolMonitor_AppendToStringBuilder_mAC6A2F1A0B036BE248590A14114DABBC27A4F678 ();
// 0x000002E9 System.Void OVRGearVrControllerTest_BoolMonitor_BoolGenerator::.ctor(System.Object,System.IntPtr)
extern void BoolGenerator__ctor_m7CD43EC2090CF104E05C9EE030044974BA3078C0 ();
// 0x000002EA System.Boolean OVRGearVrControllerTest_BoolMonitor_BoolGenerator::Invoke()
extern void BoolGenerator_Invoke_m723ACF6DFD86801B825F3845D51A5A6BBAB0C4AB ();
// 0x000002EB System.IAsyncResult OVRGearVrControllerTest_BoolMonitor_BoolGenerator::BeginInvoke(System.AsyncCallback,System.Object)
extern void BoolGenerator_BeginInvoke_m2A1F2E4A764442E894BF04D95A1D1C22991AAFE5 ();
// 0x000002EC System.Boolean OVRGearVrControllerTest_BoolMonitor_BoolGenerator::EndInvoke(System.IAsyncResult)
extern void BoolGenerator_EndInvoke_m33008BC23FBA9CF4255DD28B85BD6896DD42BC60 ();
// 0x000002ED System.Void OVRGearVrControllerTest_<>c::.cctor()
extern void U3CU3Ec__cctor_m8F500FC932E47035E957AA7659D2E4A96B495549 ();
// 0x000002EE System.Void OVRGearVrControllerTest_<>c::.ctor()
extern void U3CU3Ec__ctor_mA42DB8363017E94A70841C0A467957EE80E3A741 ();
// 0x000002EF System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_0()
extern void U3CU3Ec_U3CStartU3Eb__4_0_m44A63E093D6B6C8CE47D49665FA141257CAA4D35 ();
// 0x000002F0 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_1()
extern void U3CU3Ec_U3CStartU3Eb__4_1_mD9AD48120837747AED4E5AE7E187248CB0C4578E ();
// 0x000002F1 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_2()
extern void U3CU3Ec_U3CStartU3Eb__4_2_mEABA34409121BF66E0773DDC007D991AAED8A3EF ();
// 0x000002F2 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_3()
extern void U3CU3Ec_U3CStartU3Eb__4_3_m2FADF2C9A38E6E471848615A922C1929EAEFCB29 ();
// 0x000002F3 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_4()
extern void U3CU3Ec_U3CStartU3Eb__4_4_m2E9B7E7541ED75D63C743B5014B96726D31AC919 ();
// 0x000002F4 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_5()
extern void U3CU3Ec_U3CStartU3Eb__4_5_m761D28AAC6AA1CA293E918BD58E770F3B8AEDA46 ();
// 0x000002F5 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_6()
extern void U3CU3Ec_U3CStartU3Eb__4_6_m686F164708715A3044C934DA7311691D99F27CB9 ();
// 0x000002F6 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_7()
extern void U3CU3Ec_U3CStartU3Eb__4_7_m3A354FABF7B734EA6217B5E8D4F5FF25AFA7072C ();
// 0x000002F7 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_8()
extern void U3CU3Ec_U3CStartU3Eb__4_8_m644067018CCB5162F36C4476D50318FCAED411B8 ();
// 0x000002F8 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_9()
extern void U3CU3Ec_U3CStartU3Eb__4_9_m80BCA1DF15CEEE0EE3C373190019EDB9DB19BE23 ();
// 0x000002F9 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_10()
extern void U3CU3Ec_U3CStartU3Eb__4_10_mE2942CF0DE26D8519D7BEA08432AE0D61653A5C1 ();
// 0x000002FA System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_11()
extern void U3CU3Ec_U3CStartU3Eb__4_11_m1E86165B012C5250D341759EC98BE7AD0A90E4AD ();
// 0x000002FB System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_12()
extern void U3CU3Ec_U3CStartU3Eb__4_12_m9439754D56B96C3148C93CAA4680AAF93AA4178E ();
// 0x000002FC System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_13()
extern void U3CU3Ec_U3CStartU3Eb__4_13_mD60BB4F89156092829DFA371977A60F1EDE495E0 ();
// 0x000002FD System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_14()
extern void U3CU3Ec_U3CStartU3Eb__4_14_mDA1ED84100D40C7E60AEE22BEA0F33355987795E ();
// 0x000002FE System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_15()
extern void U3CU3Ec_U3CStartU3Eb__4_15_mF5C2985F3910D9424813D0B674B3F4D747F59290 ();
// 0x000002FF System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_16()
extern void U3CU3Ec_U3CStartU3Eb__4_16_m72755202C4FD3BE42F20A4E32E71B6603DAD7CEA ();
// 0x00000300 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_17()
extern void U3CU3Ec_U3CStartU3Eb__4_17_m8206839BE943F50E4133AC7FC2D24EB09C788AE3 ();
// 0x00000301 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_18()
extern void U3CU3Ec_U3CStartU3Eb__4_18_m6240EF55CA073017897FD60BCAF0B147110D4AA0 ();
// 0x00000302 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_19()
extern void U3CU3Ec_U3CStartU3Eb__4_19_m8B1F1891FE6BBB7193DE04B4A8986C936C28DB9F ();
// 0x00000303 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_20()
extern void U3CU3Ec_U3CStartU3Eb__4_20_mB9CBD36D63F5D9D0F0E1B37C8B1CB1922CA0A842 ();
// 0x00000304 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_21()
extern void U3CU3Ec_U3CStartU3Eb__4_21_m3EFDE0E591DB94DCC68A449CBF009261F0134B6E ();
// 0x00000305 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_22()
extern void U3CU3Ec_U3CStartU3Eb__4_22_mE820AEC1242AC5716B37DA0EE08783E9593F5996 ();
// 0x00000306 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_23()
extern void U3CU3Ec_U3CStartU3Eb__4_23_mE868AEBC402C62AA6BAD13D8DBE8A3EF667419CB ();
// 0x00000307 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_24()
extern void U3CU3Ec_U3CStartU3Eb__4_24_m4D43E168623A700523B91BAB9C2C7D12CB45972F ();
// 0x00000308 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_25()
extern void U3CU3Ec_U3CStartU3Eb__4_25_mF7D5178C19BA1438BD8D4C52919121A2BFA63C3C ();
// 0x00000309 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_26()
extern void U3CU3Ec_U3CStartU3Eb__4_26_mF3F63586ECA5570986360B522DB3830A6D1F008E ();
// 0x0000030A System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_27()
extern void U3CU3Ec_U3CStartU3Eb__4_27_m39746C908249E7B79937F51628CF748A976B95AD ();
// 0x0000030B System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_28()
extern void U3CU3Ec_U3CStartU3Eb__4_28_m1D6C556535C5B7B8D8399C7D55AC80E29B8F11AD ();
// 0x0000030C System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_29()
extern void U3CU3Ec_U3CStartU3Eb__4_29_m11151152D9CA3A57F078BBEB18786018926492B5 ();
// 0x0000030D System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_30()
extern void U3CU3Ec_U3CStartU3Eb__4_30_mEDE76950B0475200DF3AB47793722EB7D05BD4DB ();
// 0x0000030E System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_31()
extern void U3CU3Ec_U3CStartU3Eb__4_31_mC0C198FC8373B9B9098FE957DA1F8DE8E3976821 ();
// 0x0000030F System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_32()
extern void U3CU3Ec_U3CStartU3Eb__4_32_mFD25E4E97F15A04940BDB3C446A4E97A0C5BE5D0 ();
// 0x00000310 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_33()
extern void U3CU3Ec_U3CStartU3Eb__4_33_m19E2C1ED3DED1579C9218CEB57C37D2529CE823F ();
// 0x00000311 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_34()
extern void U3CU3Ec_U3CStartU3Eb__4_34_mD88F96C2B5B0940A28659D2B1E1676D21B2AE759 ();
// 0x00000312 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_35()
extern void U3CU3Ec_U3CStartU3Eb__4_35_m1B3EFDC03F7AC2D0E0E9C9BB3B71081429218AD1 ();
// 0x00000313 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_36()
extern void U3CU3Ec_U3CStartU3Eb__4_36_m564CA6A3D7A64C5280E9B6104D2AF37E87973B58 ();
// 0x00000314 System.Boolean OVRGearVrControllerTest_<>c::<Start>b__4_37()
extern void U3CU3Ec_U3CStartU3Eb__4_37_m560267CE648219A8AECEE043C3F9FC3E079C5265 ();
// 0x00000315 System.Boolean OVRGearVrControllerTest_<>c::<.cctor>b__9_0()
extern void U3CU3Ec_U3C_cctorU3Eb__9_0_m2D76F808C9D9F56C3D5D171CC398D391991F1B1A ();
// 0x00000316 System.Boolean OVRGrabbable::get_allowOffhandGrab()
extern void OVRGrabbable_get_allowOffhandGrab_mC2FAF00A0F5A00E7C2C7DFF97E9907B808ADC5CC ();
// 0x00000317 System.Boolean OVRGrabbable::get_isGrabbed()
extern void OVRGrabbable_get_isGrabbed_m3BF5FF2FB70AA28D0E65769BFFF41959BC6C6C68 ();
// 0x00000318 System.Boolean OVRGrabbable::get_snapPosition()
extern void OVRGrabbable_get_snapPosition_m2A6031A4E49B5D116ED097A20B71D3F854819297 ();
// 0x00000319 System.Boolean OVRGrabbable::get_snapOrientation()
extern void OVRGrabbable_get_snapOrientation_m365EDF846A5BA54DB9DF01A8B5567D2384FA46DD ();
// 0x0000031A UnityEngine.Transform OVRGrabbable::get_snapOffset()
extern void OVRGrabbable_get_snapOffset_m56FBF4E8C0CE1E7E8EC6377779E38E9F7A2234A2 ();
// 0x0000031B OVRGrabber OVRGrabbable::get_grabbedBy()
extern void OVRGrabbable_get_grabbedBy_m6C2AEA29FE82B1865CB18A013F1579812DEF4AB9 ();
// 0x0000031C UnityEngine.Transform OVRGrabbable::get_grabbedTransform()
extern void OVRGrabbable_get_grabbedTransform_m94686C63DE14C02F9AC3559DB9B92A744FB33B4A ();
// 0x0000031D UnityEngine.Rigidbody OVRGrabbable::get_grabbedRigidbody()
extern void OVRGrabbable_get_grabbedRigidbody_m631EE15156D1DB1FF1D5E554260B8A6BB8E03728 ();
// 0x0000031E UnityEngine.Collider[] OVRGrabbable::get_grabPoints()
extern void OVRGrabbable_get_grabPoints_mC82FAC26EA82EB722BE4E5F5CFEE77717FB0176E ();
// 0x0000031F System.Void OVRGrabbable::GrabBegin(OVRGrabber,UnityEngine.Collider)
extern void OVRGrabbable_GrabBegin_m7AFD3090290332E539082D33666E168EAD3054AD ();
// 0x00000320 System.Void OVRGrabbable::GrabEnd(UnityEngine.Vector3,UnityEngine.Vector3)
extern void OVRGrabbable_GrabEnd_mAD0DC368D97ADD4C5802CF6BFE451C8B59753FDC ();
// 0x00000321 System.Void OVRGrabbable::Awake()
extern void OVRGrabbable_Awake_m4891E7203A33F0E2E94D601B9F3631A7AA7260FE ();
// 0x00000322 System.Void OVRGrabbable::Start()
extern void OVRGrabbable_Start_mC86B606A4900FDEBCF9FEBF1B28CB72B7AA9F837 ();
// 0x00000323 System.Void OVRGrabbable::OnDestroy()
extern void OVRGrabbable_OnDestroy_mB96B3FE259306CA255D7402DB4DFCCD8927D44D9 ();
// 0x00000324 System.Void OVRGrabbable::.ctor()
extern void OVRGrabbable__ctor_m6347A9620A021FE6BAD9DB69BCF759423B32362A ();
// 0x00000325 OVRGrabbable OVRGrabber::get_grabbedObject()
extern void OVRGrabber_get_grabbedObject_m4219D0E00D5E1C409303E1B40464A26C6330E394 ();
// 0x00000326 System.Void OVRGrabber::ForceRelease(OVRGrabbable)
extern void OVRGrabber_ForceRelease_m13887812BE7C69B9D79C4508FB23E62570C90FE0 ();
// 0x00000327 System.Void OVRGrabber::Awake()
extern void OVRGrabber_Awake_m59BB1D586F051A396A914EB68576BAD06700B7C8 ();
// 0x00000328 System.Void OVRGrabber::Start()
extern void OVRGrabber_Start_mF5630EC0CF0E4E658724AF86DA7DC837EBDCBE09 ();
// 0x00000329 System.Void OVRGrabber::FixedUpdate()
extern void OVRGrabber_FixedUpdate_m76EFC8B20BF0AFE48FBC10928C4B55FE06D540F9 ();
// 0x0000032A System.Void OVRGrabber::OnUpdatedAnchors()
extern void OVRGrabber_OnUpdatedAnchors_mD348059D23379A2F1C4F59FA1E7B0DC01B526FDA ();
// 0x0000032B System.Void OVRGrabber::OnDestroy()
extern void OVRGrabber_OnDestroy_m0733C0D4C4141B911ADBDFEE368F862A414FEA6D ();
// 0x0000032C System.Void OVRGrabber::OnTriggerEnter(UnityEngine.Collider)
extern void OVRGrabber_OnTriggerEnter_m72BE0FEB8152CE41253E7B9135E0DB3C6833B4A9 ();
// 0x0000032D System.Void OVRGrabber::OnTriggerExit(UnityEngine.Collider)
extern void OVRGrabber_OnTriggerExit_m4B2F1D525E42F7B4E094EA52D5A040A76F06E736 ();
// 0x0000032E System.Void OVRGrabber::CheckForGrabOrRelease(System.Single)
extern void OVRGrabber_CheckForGrabOrRelease_m92381B223CA0D3180A874B475B5A54B937F84256 ();
// 0x0000032F System.Void OVRGrabber::GrabBegin()
extern void OVRGrabber_GrabBegin_m2E85C86F50E7D50CCBE85BACFF7D9D117282D782 ();
// 0x00000330 System.Void OVRGrabber::MoveGrabbedObject(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void OVRGrabber_MoveGrabbedObject_m553A0633F56DBBB05A82306C5645AB084ADE5F02 ();
// 0x00000331 System.Void OVRGrabber::GrabEnd()
extern void OVRGrabber_GrabEnd_mE19867FEA6C93593536CCD73884CAA68116542D3 ();
// 0x00000332 System.Void OVRGrabber::GrabbableRelease(UnityEngine.Vector3,UnityEngine.Vector3)
extern void OVRGrabber_GrabbableRelease_m3A62FBADABAE37F8806980E4C41AEDC885CC2157 ();
// 0x00000333 System.Void OVRGrabber::GrabVolumeEnable(System.Boolean)
extern void OVRGrabber_GrabVolumeEnable_m623BB5AA6E5DCFB34235B5015A3A43AEE36120B7 ();
// 0x00000334 System.Void OVRGrabber::OffhandGrabbed(OVRGrabbable)
extern void OVRGrabber_OffhandGrabbed_m4408F0D6B24B6AC8CBE2C818A69D8B941B760146 ();
// 0x00000335 System.Void OVRGrabber::.ctor()
extern void OVRGrabber__ctor_mC8F506802658928681DC80ADD3529E7639417F6E ();
// 0x00000336 System.Void OVRGrabber::<Awake>b__21_0(OVRCameraRig)
extern void OVRGrabber_U3CAwakeU3Eb__21_0_m6A86F16E01E7EA802BB65AF0F6EE76F81977BA0B ();
// 0x00000337 System.Void OVRGridCube::Update()
extern void OVRGridCube_Update_m19D35E72AB4F4A30EF9599DF39A5BA96DEB64EE6 ();
// 0x00000338 System.Void OVRGridCube::SetOVRCameraController(OVRCameraRig&)
extern void OVRGridCube_SetOVRCameraController_m0A001667BBBD176CB26CEB8C649406BDCB9FB9E7 ();
// 0x00000339 System.Void OVRGridCube::UpdateCubeGrid()
extern void OVRGridCube_UpdateCubeGrid_mCD5FB9C2C4C4DAB34F6C2D3E6B343ED95C953CDD ();
// 0x0000033A System.Void OVRGridCube::CreateCubeGrid()
extern void OVRGridCube_CreateCubeGrid_m6AD79AB85264F3CD0E1AAE0EFCD951E1010AE197 ();
// 0x0000033B System.Void OVRGridCube::CubeGridSwitchColor(System.Boolean)
extern void OVRGridCube_CubeGridSwitchColor_m1E901DA50645413952C41D02CECEC1A222438482 ();
// 0x0000033C System.Void OVRGridCube::.ctor()
extern void OVRGridCube__ctor_m9BE320D7B11F21A4D7C58D57AE2DF81CD201D7E2 ();
// 0x0000033D System.Void OVRModeParms::Start()
extern void OVRModeParms_Start_m4B54385D9DBBA6347A51EAAC9733BA642907D7AE ();
// 0x0000033E System.Void OVRModeParms::Update()
extern void OVRModeParms_Update_mC642AE2C179F57F4B2BDE50C8CF076CA2E58E268 ();
// 0x0000033F System.Void OVRModeParms::TestPowerStateMode()
extern void OVRModeParms_TestPowerStateMode_mF0CA16420A77546B65B08FD49765D0FD7633AE6D ();
// 0x00000340 System.Void OVRModeParms::.ctor()
extern void OVRModeParms__ctor_m0EB897AFFA827582D4DB25CA7ED2EB48B2FC6A5B ();
// 0x00000341 System.Void OVRMonoscopic::Update()
extern void OVRMonoscopic_Update_mBC2839D9A5847CADF0B57212F0D4947CFCE4B1A4 ();
// 0x00000342 System.Void OVRMonoscopic::.ctor()
extern void OVRMonoscopic__ctor_m4F98F7F01126079533D6B651982E5C67DBF39D30 ();
// 0x00000343 System.Void OVRNetwork::.ctor()
extern void OVRNetwork__ctor_m2AA9DA154F5E99439220D84A7389B61448CF097A ();
// 0x00000344 System.Byte[] OVRNetwork_FrameHeader::ToBytes()
extern void FrameHeader_ToBytes_m713EC3931B082BE4997B3EA49576AEE3064A049D_AdjustorThunk ();
// 0x00000345 OVRNetwork_FrameHeader OVRNetwork_FrameHeader::FromBytes(System.Byte[])
extern void FrameHeader_FromBytes_mA9EF9C7F09AE317D7677A332E137875807D1D107 ();
// 0x00000346 System.Void OVRNetwork_OVRNetworkTcpServer::StartListening(System.Int32)
extern void OVRNetworkTcpServer_StartListening_mD8982C918E324E9F14A8AC2C9BC971086FB2B04A ();
// 0x00000347 System.Void OVRNetwork_OVRNetworkTcpServer::StopListening()
extern void OVRNetworkTcpServer_StopListening_m2611132C9E521EF2EFA486A4F5C6108DC64DFB64 ();
// 0x00000348 System.Void OVRNetwork_OVRNetworkTcpServer::DoAcceptTcpClientCallback(System.IAsyncResult)
extern void OVRNetworkTcpServer_DoAcceptTcpClientCallback_m8A9A32947442526EE384AD00F9768F8D542B125F ();
// 0x00000349 System.Boolean OVRNetwork_OVRNetworkTcpServer::HasConnectedClient()
extern void OVRNetworkTcpServer_HasConnectedClient_mAD81EBB4C83858DE407EE6F83E06F66BD6D11EF3 ();
// 0x0000034A System.Void OVRNetwork_OVRNetworkTcpServer::Broadcast(System.Int32,System.Byte[])
extern void OVRNetworkTcpServer_Broadcast_mA9D5DE85EAF080A289113B9914B7D8C26CDE5270 ();
// 0x0000034B System.Void OVRNetwork_OVRNetworkTcpServer::DoWriteDataCallback(System.IAsyncResult)
extern void OVRNetworkTcpServer_DoWriteDataCallback_m945ABCF09440ADCE992D461FB26865C6EBE5E8FA ();
// 0x0000034C System.Void OVRNetwork_OVRNetworkTcpServer::.ctor()
extern void OVRNetworkTcpServer__ctor_m7924C6AA73697D8099352CCCD3AACCBFB084ADDA ();
// 0x0000034D OVRNetwork_OVRNetworkTcpClient_ConnectionState OVRNetwork_OVRNetworkTcpClient::get_connectionState()
extern void OVRNetworkTcpClient_get_connectionState_m52768313BFEBD74F5DF77075FF9A4B058C2C86E7 ();
// 0x0000034E System.Boolean OVRNetwork_OVRNetworkTcpClient::get_Connected()
extern void OVRNetworkTcpClient_get_Connected_m42E1340F16CFB7F890006F48A451563158091DB8 ();
// 0x0000034F System.Void OVRNetwork_OVRNetworkTcpClient::Connect(System.Int32)
extern void OVRNetworkTcpClient_Connect_m0FCFE6B0BE2DEAB605B62FB691F9D51749AB194D ();
// 0x00000350 System.Void OVRNetwork_OVRNetworkTcpClient::ConnectCallback(System.IAsyncResult)
extern void OVRNetworkTcpClient_ConnectCallback_mE00816DACEB5EB9EFB6DAF6E9ED3D8104F5BE125 ();
// 0x00000351 System.Void OVRNetwork_OVRNetworkTcpClient::Disconnect()
extern void OVRNetworkTcpClient_Disconnect_m8C6479AFCD432B12EFD1FACA455E130CEF9721B3 ();
// 0x00000352 System.Void OVRNetwork_OVRNetworkTcpClient::Tick()
extern void OVRNetworkTcpClient_Tick_m52CE1A261F9114161331199D3BB2C6705800EFAF ();
// 0x00000353 System.Void OVRNetwork_OVRNetworkTcpClient::OnReadDataCallback(System.IAsyncResult)
extern void OVRNetworkTcpClient_OnReadDataCallback_m9D78F8D5F325CEAFF34A00BE8E77FCD982119D4B ();
// 0x00000354 System.Void OVRNetwork_OVRNetworkTcpClient::.ctor()
extern void OVRNetworkTcpClient__ctor_mEBAEB62733442322931EA30FD14AAA3761BAA2F5 ();
// 0x00000355 System.Void OVRPlayerController::add_TransformUpdated(System.Action`1<UnityEngine.Transform>)
extern void OVRPlayerController_add_TransformUpdated_m7FB492C719A139F5DCAA54F73C5D9C55E7860CC7 ();
// 0x00000356 System.Void OVRPlayerController::remove_TransformUpdated(System.Action`1<UnityEngine.Transform>)
extern void OVRPlayerController_remove_TransformUpdated_mDE57809723250264926FCC11FCEAAFC4A1650670 ();
// 0x00000357 System.Void OVRPlayerController::add_CameraUpdated(System.Action)
extern void OVRPlayerController_add_CameraUpdated_mBB7BC1C27F1179AA2465FA294E4A8996E89D62B4 ();
// 0x00000358 System.Void OVRPlayerController::remove_CameraUpdated(System.Action)
extern void OVRPlayerController_remove_CameraUpdated_m3BA83C077B908AA60012292151A883136DB5729F ();
// 0x00000359 System.Void OVRPlayerController::add_PreCharacterMove(System.Action)
extern void OVRPlayerController_add_PreCharacterMove_m30B863614DA426239E3D7318E3679D5220B15BE7 ();
// 0x0000035A System.Void OVRPlayerController::remove_PreCharacterMove(System.Action)
extern void OVRPlayerController_remove_PreCharacterMove_m0A090CCDAAA57169D85E3500B4730C5F0D45DDC8 ();
// 0x0000035B System.Single OVRPlayerController::get_InitialYRotation()
extern void OVRPlayerController_get_InitialYRotation_mE0C203F34B717F860815227B2A74CDCA91DE035F ();
// 0x0000035C System.Void OVRPlayerController::set_InitialYRotation(System.Single)
extern void OVRPlayerController_set_InitialYRotation_mC5C6D37D3BB533E1B1692D05402795A17A33317F ();
// 0x0000035D System.Void OVRPlayerController::Start()
extern void OVRPlayerController_Start_m7380017B83EE9DB4E387DCE98AF1474F923D5CB1 ();
// 0x0000035E System.Void OVRPlayerController::Awake()
extern void OVRPlayerController_Awake_m3A07A95C186DC9B855921C6CB4C67376D7F88326 ();
// 0x0000035F System.Void OVRPlayerController::OnEnable()
extern void OVRPlayerController_OnEnable_mF4D652F936AE2CBE20CAABDF3F5501799C317A89 ();
// 0x00000360 System.Void OVRPlayerController::OnDisable()
extern void OVRPlayerController_OnDisable_mC68609D83565FB4A974607B48D8472BBB191255E ();
// 0x00000361 System.Void OVRPlayerController::Update()
extern void OVRPlayerController_Update_m308D263AF0287713D7B2F7D0126F747B610811DE ();
// 0x00000362 System.Void OVRPlayerController::UpdateController()
extern void OVRPlayerController_UpdateController_m3CA68419620144497EF2AA0883640508C882D3E7 ();
// 0x00000363 System.Void OVRPlayerController::UpdateMovement()
extern void OVRPlayerController_UpdateMovement_mD61A904F36D31B1275DA8FB8C52D4085AEB7A772 ();
// 0x00000364 System.Void OVRPlayerController::UpdateTransform(OVRCameraRig)
extern void OVRPlayerController_UpdateTransform_m61345C2D7687B35705D23A6A5038A60884DED4A5 ();
// 0x00000365 System.Boolean OVRPlayerController::Jump()
extern void OVRPlayerController_Jump_m41E169973AB02CDA441E802816D4783A242BD26B ();
// 0x00000366 System.Void OVRPlayerController::Stop()
extern void OVRPlayerController_Stop_m572095671886993EC9C138AF9A11876A48D2E601 ();
// 0x00000367 System.Void OVRPlayerController::GetMoveScaleMultiplier(System.Single&)
extern void OVRPlayerController_GetMoveScaleMultiplier_m6553184B97FA79A209B4624689BC2E3CFEBEFBF6 ();
// 0x00000368 System.Void OVRPlayerController::SetMoveScaleMultiplier(System.Single)
extern void OVRPlayerController_SetMoveScaleMultiplier_mF61F60033BEA615E3037F5FEBDA940D0D5510289 ();
// 0x00000369 System.Void OVRPlayerController::GetRotationScaleMultiplier(System.Single&)
extern void OVRPlayerController_GetRotationScaleMultiplier_m5433E3B5DA9CC5C9821A3D3E898417F63BD654B6 ();
// 0x0000036A System.Void OVRPlayerController::SetRotationScaleMultiplier(System.Single)
extern void OVRPlayerController_SetRotationScaleMultiplier_m094085BCC6908B73D9DDD843CEA94A8FA5ACE4AE ();
// 0x0000036B System.Void OVRPlayerController::GetSkipMouseRotation(System.Boolean&)
extern void OVRPlayerController_GetSkipMouseRotation_m08D044C2BF4C26B2B0748B5108D404F864BE9302 ();
// 0x0000036C System.Void OVRPlayerController::SetSkipMouseRotation(System.Boolean)
extern void OVRPlayerController_SetSkipMouseRotation_mF644D2CF348FE8C9BDCC0A7C347D9D280E1DF01C ();
// 0x0000036D System.Void OVRPlayerController::GetHaltUpdateMovement(System.Boolean&)
extern void OVRPlayerController_GetHaltUpdateMovement_mFF11F1BF3B54FE5DA5495BB29A7561EB486F67F2 ();
// 0x0000036E System.Void OVRPlayerController::SetHaltUpdateMovement(System.Boolean)
extern void OVRPlayerController_SetHaltUpdateMovement_mFF5235DA3AEA17C76F89ED5FFE6299129170E1FB ();
// 0x0000036F System.Void OVRPlayerController::ResetOrientation()
extern void OVRPlayerController_ResetOrientation_m05C6C2BB5447BBC1D1C090DE51D2CA4E511FE26B ();
// 0x00000370 System.Void OVRPlayerController::.ctor()
extern void OVRPlayerController__ctor_m4018AF8A4C8BD3E12AE2E6EA664BAE8FC56B670A ();
// 0x00000371 System.Void OVRProgressIndicator::Awake()
extern void OVRProgressIndicator_Awake_mDE3A794E2C1959404652188736B2621935DE7268 ();
// 0x00000372 System.Void OVRProgressIndicator::Update()
extern void OVRProgressIndicator_Update_m1CC97404BA2292A8B4235D9EC834B7B0A54E94F7 ();
// 0x00000373 System.Void OVRProgressIndicator::.ctor()
extern void OVRProgressIndicator__ctor_m510C2E7532E8035D2F1F2B057712007C2FB18ED8 ();
// 0x00000374 System.Void OVRRaycaster::.ctor()
extern void OVRRaycaster__ctor_mD850E7353D9DEE7C841ABC66E921DD6216B4BC0D ();
// 0x00000375 UnityEngine.Canvas OVRRaycaster::get_canvas()
extern void OVRRaycaster_get_canvas_m464156C7DBF8339454FF639456BAAA837FE87F91 ();
// 0x00000376 UnityEngine.Camera OVRRaycaster::get_eventCamera()
extern void OVRRaycaster_get_eventCamera_m97088446BED642AF8DDFE9E6C734B2342DDDFF40 ();
// 0x00000377 System.Int32 OVRRaycaster::get_sortOrderPriority()
extern void OVRRaycaster_get_sortOrderPriority_mEEE6EFD0458D36C7978A0E5B25D44D81AED38F02 ();
// 0x00000378 System.Void OVRRaycaster::Start()
extern void OVRRaycaster_Start_mABF81E3203CAEBA6B96B16B8285B57C393EA7923 ();
// 0x00000379 System.Void OVRRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>,UnityEngine.Ray,System.Boolean)
extern void OVRRaycaster_Raycast_m5D74C042C864EECEFAFD73B0E2128AE1004216C9 ();
// 0x0000037A System.Void OVRRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void OVRRaycaster_Raycast_m536FEA11DD00032FEC17360E98F0C1A08753D4BB ();
// 0x0000037B System.Void OVRRaycaster::RaycastPointer(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void OVRRaycaster_RaycastPointer_m199AD4DB1E3196EEFE40E6131784F07A653054B6 ();
// 0x0000037C System.Void OVRRaycaster::GraphicRaycast(UnityEngine.Canvas,UnityEngine.Ray,System.Collections.Generic.List`1<OVRRaycaster_RaycastHit>)
extern void OVRRaycaster_GraphicRaycast_mC5BDDA993E180ED35C99D2DC84808C0263842A4A ();
// 0x0000037D UnityEngine.Vector2 OVRRaycaster::GetScreenPosition(UnityEngine.EventSystems.RaycastResult)
extern void OVRRaycaster_GetScreenPosition_mF7869ECB0131A1CBEBFFDFF3B097BB893ABD9E7F ();
// 0x0000037E System.Boolean OVRRaycaster::RayIntersectsRectTransform(UnityEngine.RectTransform,UnityEngine.Ray,UnityEngine.Vector3&)
extern void OVRRaycaster_RayIntersectsRectTransform_m24F57D2FE1D24CB6822ABE334CC51200831A8FBA ();
// 0x0000037F System.Boolean OVRRaycaster::IsFocussed()
extern void OVRRaycaster_IsFocussed_mCAB4168CBFCBCE9B2FC8FD90F9E90A4E65138FC0 ();
// 0x00000380 System.Void OVRRaycaster::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void OVRRaycaster_OnPointerEnter_mEE6C19AF42E31D3B0FDD0A19538AAB428A91AE7D ();
// 0x00000381 System.Void OVRRaycaster::.cctor()
extern void OVRRaycaster__cctor_m43D7831ED5837C41626416EA61C979938B12014E ();
// 0x00000382 System.Void OVRRaycaster_<>c::.cctor()
extern void U3CU3Ec__cctor_m46230A0043C6D1E26268CF37369EC62ED883CC4A ();
// 0x00000383 System.Void OVRRaycaster_<>c::.ctor()
extern void U3CU3Ec__ctor_m3D623AAF6A08ADB17F619D78E78045052622C453 ();
// 0x00000384 System.Int32 OVRRaycaster_<>c::<GraphicRaycast>b__16_0(OVRRaycaster_RaycastHit,OVRRaycaster_RaycastHit)
extern void U3CU3Ec_U3CGraphicRaycastU3Eb__16_0_m7D9750E42FEDF2909AA69A17E1A5B4F46B1F51A6 ();
// 0x00000385 System.Void OVRResetOrientation::Update()
extern void OVRResetOrientation_Update_mA4EE6BA6C640BE6CE860A64E62F3AB261B872CCE ();
// 0x00000386 System.Void OVRResetOrientation::.ctor()
extern void OVRResetOrientation__ctor_m82F132C8C20E18E9713975246DA3062A67A68C67 ();
// 0x00000387 System.Void OVRSceneSampleController::Awake()
extern void OVRSceneSampleController_Awake_mFCF063EC14D56C6B74C76B1DDEFF4A2BBCEDC47C ();
// 0x00000388 System.Void OVRSceneSampleController::Start()
extern void OVRSceneSampleController_Start_m53CE4C890802F57B7195A8BD1520CAC70A3545FB ();
// 0x00000389 System.Void OVRSceneSampleController::Update()
extern void OVRSceneSampleController_Update_mDF4E1489655FDBB35E95D43B7FAE022ED03F20DD ();
// 0x0000038A System.Void OVRSceneSampleController::UpdateVisionMode()
extern void OVRSceneSampleController_UpdateVisionMode_mAF8C5FB6A16121C3541477FCAA5ADFE1C0337C5C ();
// 0x0000038B System.Void OVRSceneSampleController::UpdateSpeedAndRotationScaleMultiplier()
extern void OVRSceneSampleController_UpdateSpeedAndRotationScaleMultiplier_m6469A69A07A8C85F97B85FB8B3E14F86E3542C6A ();
// 0x0000038C System.Void OVRSceneSampleController::UpdateRecenterPose()
extern void OVRSceneSampleController_UpdateRecenterPose_m261286E0913021DF752B89DABD2E3754A17D6658 ();
// 0x0000038D System.Void OVRSceneSampleController::.ctor()
extern void OVRSceneSampleController__ctor_mD894A5832A674830DF052BF8F1BF85BEC0F77882 ();
// 0x0000038E System.Single OVRScreenFade::get_currentAlpha()
extern void OVRScreenFade_get_currentAlpha_m0F6CC62AAC351079AECB9193BE101F981369CE66 ();
// 0x0000038F System.Void OVRScreenFade::set_currentAlpha(System.Single)
extern void OVRScreenFade_set_currentAlpha_m3D36BAFC6225A4BB4A63A527F9D0BD9E6E354544 ();
// 0x00000390 System.Void OVRScreenFade::Awake()
extern void OVRScreenFade_Awake_m80ACAAB51B0549932312EBC8EB51AA736792E2D3 ();
// 0x00000391 System.Void OVRScreenFade::FadeOut()
extern void OVRScreenFade_FadeOut_m05D8D9CC82D1927E6F6B7D144F03DE7BFCA25E2A ();
// 0x00000392 System.Void OVRScreenFade::OnLevelFinishedLoading(System.Int32)
extern void OVRScreenFade_OnLevelFinishedLoading_m05F0196AF2A19E7F50DF8458F22131977B4ACEAA ();
// 0x00000393 System.Void OVRScreenFade::Start()
extern void OVRScreenFade_Start_m86A416E0A43ED2A3094BF9FF636F761D1E6E843E ();
// 0x00000394 System.Void OVRScreenFade::OnEnable()
extern void OVRScreenFade_OnEnable_mF86405A64481B49570C900AAF567D954B43B7320 ();
// 0x00000395 System.Void OVRScreenFade::OnDestroy()
extern void OVRScreenFade_OnDestroy_m75D4EB9F8F27E175CD11EB00D92F5985681302DD ();
// 0x00000396 System.Void OVRScreenFade::SetUIFade(System.Single)
extern void OVRScreenFade_SetUIFade_m482FAAB0B460580CE1A2CFAB41C64DD0A333C904 ();
// 0x00000397 System.Void OVRScreenFade::SetFadeLevel(System.Single)
extern void OVRScreenFade_SetFadeLevel_m8106A6BBE990FB28506FB239F625CFCB8860FCBC ();
// 0x00000398 System.Collections.IEnumerator OVRScreenFade::Fade(System.Single,System.Single)
extern void OVRScreenFade_Fade_m909418F8E06618AC04CA0B89662957D52598B61C ();
// 0x00000399 System.Void OVRScreenFade::SetMaterialAlpha()
extern void OVRScreenFade_SetMaterialAlpha_mE60E60A758D2D7383B111800E9E19BECA8245333 ();
// 0x0000039A System.Void OVRScreenFade::.ctor()
extern void OVRScreenFade__ctor_m856A3A9B6367CD22BB12BBDC3327DDF38A629953 ();
// 0x0000039B System.Void OVRScreenFade_<Fade>d__21::.ctor(System.Int32)
extern void U3CFadeU3Ed__21__ctor_m59EF26E769BE48B29A99B7D55B0208020F8BD6A7 ();
// 0x0000039C System.Void OVRScreenFade_<Fade>d__21::System.IDisposable.Dispose()
extern void U3CFadeU3Ed__21_System_IDisposable_Dispose_mC68EB95C937056AB51EB95E0D640E1B737FA1A55 ();
// 0x0000039D System.Boolean OVRScreenFade_<Fade>d__21::MoveNext()
extern void U3CFadeU3Ed__21_MoveNext_m805303CBD5E0A4C370122EE0DA0E3D0DFE0B0C72 ();
// 0x0000039E System.Object OVRScreenFade_<Fade>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B48721ECD0FED54D7FF40FE201DC80FF44E7059 ();
// 0x0000039F System.Void OVRScreenFade_<Fade>d__21::System.Collections.IEnumerator.Reset()
extern void U3CFadeU3Ed__21_System_Collections_IEnumerator_Reset_m43BBE93639312543D26E1AC1E1BF16697824EFEE ();
// 0x000003A0 System.Object OVRScreenFade_<Fade>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m849A5E317F2D48B85A30D0E49C5B48950BFC871F ();
// 0x000003A1 System.Void OVRSystemPerfMetrics::.ctor()
extern void OVRSystemPerfMetrics__ctor_m3EFE4B85D0CA975DB52208DCF12C5C630773DD95 ();
// 0x000003A2 System.String OVRSystemPerfMetrics_PerfMetrics::ToJSON()
extern void PerfMetrics_ToJSON_mB97EC503CBD1F3BAD45E1638F8AECA64CD6995A0 ();
// 0x000003A3 System.Boolean OVRSystemPerfMetrics_PerfMetrics::LoadFromJSON(System.String)
extern void PerfMetrics_LoadFromJSON_mFC15BBD60F0AD7551481D2C17E969993A3144F01 ();
// 0x000003A4 System.Void OVRSystemPerfMetrics_PerfMetrics::.ctor()
extern void PerfMetrics__ctor_mDEB091146BA8F9740FC19ABFBB17B69653FA3443 ();
// 0x000003A5 System.Void OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::OnEnable()
extern void OVRSystemPerfMetricsTcpServer_OnEnable_m06489B3E3133525FE402D1B8B7A30EB563F2115B ();
// 0x000003A6 System.Void OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::OnDisable()
extern void OVRSystemPerfMetricsTcpServer_OnDisable_mDAF5DAB99832B63EB9F409935443DDE1BB375FCA ();
// 0x000003A7 System.Void OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::Update()
extern void OVRSystemPerfMetricsTcpServer_Update_m745F0F6CCD7F5B1A7E0D2C04CFF71C5609A2F2F0 ();
// 0x000003A8 OVRSystemPerfMetrics_PerfMetrics OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::GatherPerfMetrics()
extern void OVRSystemPerfMetricsTcpServer_GatherPerfMetrics_m11D87026A8E82BFCAE30C3F57A6DB47DC5557F82 ();
// 0x000003A9 System.Void OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::.ctor()
extern void OVRSystemPerfMetricsTcpServer__ctor_mDEAD1FF3D67B14B6361006CED0D36F0D6C1E3583 ();
// 0x000003AA System.Void OVRSystemPerfMetrics_OVRSystemPerfMetricsTcpServer::.cctor()
extern void OVRSystemPerfMetricsTcpServer__cctor_mF0F4B490B8DC68D51B39ED6B0A237792BA1F29C3 ();
// 0x000003AB System.Void OVRWaitCursor::Update()
extern void OVRWaitCursor_Update_mA16FEAA810EA8C5B3CC255CF756F3E5A3D874CE2 ();
// 0x000003AC System.Void OVRWaitCursor::.ctor()
extern void OVRWaitCursor__ctor_m3CA58B0C8E39F3156E2F3FC64731CD1C255BDD32 ();
// 0x000003AD OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONNode::get_Tag()
// 0x000003AE OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m44C8808E9BE8D31204D5F604C5EAD0B32A0D7416 ();
// 0x000003AF System.Void OVRSimpleJSON.JSONNode::set_Item(System.Int32,OVRSimpleJSON.JSONNode)
extern void JSONNode_set_Item_m38D0C67E569B9C859F7DBD471B62D568C1091DAF ();
// 0x000003B0 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mAD2331FE3B68F3F01DDAC26DBF02DE74415A11F4 ();
// 0x000003B1 System.Void OVRSimpleJSON.JSONNode::set_Item(System.String,OVRSimpleJSON.JSONNode)
extern void JSONNode_set_Item_m6E293178FEDEED2298A1AD353C28FB1D75391D2A ();
// 0x000003B2 System.String OVRSimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_mFA2CDE6481441C57E7F040BC0DDEAD7B41D5B62C ();
// 0x000003B3 System.Void OVRSimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_m61C34C8810D5ABA5DF0E4EE4BEFF709470CAF5B7 ();
// 0x000003B4 System.Int32 OVRSimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m4AD71226990E97C58120702FD921265AABFF7A54 ();
// 0x000003B5 System.Boolean OVRSimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m73C89526289211C322AD679E074D783CE5389402 ();
// 0x000003B6 System.Boolean OVRSimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mFF63F2442E9474F6B71C5604F847977186B7EEF7 ();
// 0x000003B7 System.Boolean OVRSimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_m303DB76AF4F5D9BDBA0C0B979592319BF0B20840 ();
// 0x000003B8 System.Boolean OVRSimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_mC146FE0FE815586BB8A7830C5A89202C630EF2D9 ();
// 0x000003B9 System.Boolean OVRSimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m7585226F59D4FB43F06BC2BD5F1955DA2C050986 ();
// 0x000003BA System.Boolean OVRSimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_m8121A5AFAEFC48AAEF02B1CBF109BB8F62FD71B1 ();
// 0x000003BB System.Boolean OVRSimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m0924BB18C8CE1118B230CEF4331BCD7E2891B2F0 ();
// 0x000003BC System.Void OVRSimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m58C1B28FBEC6D9D7C6F4919E104F11C6D87C8C6F ();
// 0x000003BD System.Void OVRSimpleJSON.JSONNode::Add(System.String,OVRSimpleJSON.JSONNode)
extern void JSONNode_Add_m223852E9B0DE582539418C5857E15D3FCC1801C4 ();
// 0x000003BE System.Void OVRSimpleJSON.JSONNode::Add(OVRSimpleJSON.JSONNode)
extern void JSONNode_Add_m7D015A7E87F35ACC958EAD874D373071E14D7807 ();
// 0x000003BF OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mD2BAF3B09D47B59D123AA03A44AF702D8CD91965 ();
// 0x000003C0 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m93DEB66BEE51BD6A79E214314721D8214F24B126 ();
// 0x000003C1 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::Remove(OVRSimpleJSON.JSONNode)
extern void JSONNode_Remove_m698972B1717A2E3FB15CCDE19F427000AA36A70E ();
// 0x000003C2 System.Collections.Generic.IEnumerable`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_mB3EF6363C77258ACC557D4EA04AB0BEC42A6E72A ();
// 0x000003C3 System.Collections.Generic.IEnumerable`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_mD820CD9B7D702F0FAA5B8839E89E1A71CBFB7036 ();
// 0x000003C4 System.String OVRSimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_mB5C1001E9836E9AA77B0D4B504CED2B76BFA46D8 ();
// 0x000003C5 System.String OVRSimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m65DA914D4D6A248E14533FE8ADDB2DC0A9816F65 ();
// 0x000003C6 System.Void OVRSimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
// 0x000003C7 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONNode::GetEnumerator()
// 0x000003C8 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode>> OVRSimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_mD9AD46F8C7892412122BD599A47754297258F8DF ();
// 0x000003C9 OVRSimpleJSON.JSONNode_KeyEnumerator OVRSimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m695FA7DE7A02CD1E3CB7A461D182438FCFAF048D ();
// 0x000003CA OVRSimpleJSON.JSONNode_ValueEnumerator OVRSimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_mA739B317C804DDDC1275AD88959D88184C0AB5DF ();
// 0x000003CB System.Double OVRSimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m45487E41476CB62F80C39CB6EFEFCADF4CD5F7A6 ();
// 0x000003CC System.Void OVRSimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_m40254B27734C59675426F7B8693DF5E719861843 ();
// 0x000003CD System.Int32 OVRSimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_mB0064CA8B5E302159DC64DD51B6C11CC6882C488 ();
// 0x000003CE System.Void OVRSimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m76C13AD5860B690F143F06812519F800F0172D81 ();
// 0x000003CF System.Single OVRSimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m5A705824CB38952BAA15E8DDAF36C0E6D70BCDDE ();
// 0x000003D0 System.Void OVRSimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m7B74551DC193C99F74E01F550CBCF5A9765BACEA ();
// 0x000003D1 System.Boolean OVRSimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_mF0D2A3EE7D37FEE24B2B171785C1D5B6DC979F38 ();
// 0x000003D2 System.Void OVRSimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m8A89F588DDBBF1232886BB8CC2FA8CB896428E56 ();
// 0x000003D3 System.Int64 OVRSimpleJSON.JSONNode::get_AsLong()
extern void JSONNode_get_AsLong_mE4E3D9F6A545573CF809942F7CB4F3A6210B46F6 ();
// 0x000003D4 System.Void OVRSimpleJSON.JSONNode::set_AsLong(System.Int64)
extern void JSONNode_set_AsLong_mDA7BF126ED30F8A2480999F871179AB1700F51C1 ();
// 0x000003D5 OVRSimpleJSON.JSONArray OVRSimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_mEE9ECAC1968135AE8CB9A6DE5879364090A0A573 ();
// 0x000003D6 OVRSimpleJSON.JSONObject OVRSimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_mEEE08E5EA28BE8ABAE70C841981CB501B8A2C7F1 ();
// 0x000003D7 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m7234DD15D996E49393ADE3DA0D0EB913FF345A7D ();
// 0x000003D8 System.String OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA5A307A11DC975BB4A11010A9D1E815CE5EDDA4E ();
// 0x000003D9 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m4AF6254D9A917920B721AD1EA469D10802621435 ();
// 0x000003DA System.Double OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m42A71F942B2725F3770F04B64210D63BC6549C58 ();
// 0x000003DB OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m63308FD45AF08A98958D1B6E1C87A0DF87C500B6 ();
// 0x000003DC System.Single OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mF6DF08DC23BAC07226A4075DBA21AEF0E425CAC7 ();
// 0x000003DD OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m0B96B2BF6B54BD74897AC5A2D44215334D81DB8D ();
// 0x000003DE System.Int32 OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mFDA6F6453965BAB49DD9A47D6947E3F0A8F27A96 ();
// 0x000003DF OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Int64)
extern void JSONNode_op_Implicit_mF45ECEFCD896422C97B72A64C1EA5D7A5B16E6E1 ();
// 0x000003E0 System.Int64 OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mF72089C945AFB2AB7D915412D372A08339C130E0 ();
// 0x000003E1 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mDF68964291FAAACA9F4D7E91AD320A07EF10C5B3 ();
// 0x000003E2 System.Boolean OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m2CE1859CB8FA978C9FC4C8A1C588DF966FA34ED8 ();
// 0x000003E3 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m66441829AC8F8A08132800B68C9AC9E29008B741 ();
// 0x000003E4 System.Boolean OVRSimpleJSON.JSONNode::op_Equality(OVRSimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mC6FB160F574A7A7E8FA30225DC5FE4F3D4E18A82 ();
// 0x000003E5 System.Boolean OVRSimpleJSON.JSONNode::op_Inequality(OVRSimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_mE1CC88332375669AD199A23DF192E294044C1FFC ();
// 0x000003E6 System.Boolean OVRSimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m5F33EB5E56535C5273AE8EB2D0D6C07A37426536 ();
// 0x000003E7 System.Int32 OVRSimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mBB90468BDBE75634838A69867F1275D16742FFC4 ();
// 0x000003E8 System.Text.StringBuilder OVRSimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m63F032467D6F8FFA5DB70BB52953D271BEEAD786 ();
// 0x000003E9 System.String OVRSimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mA4E3E754D5A032C46A7C89C53F31BE51F4B2A11C ();
// 0x000003EA OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::ParseElement(System.String,System.Boolean)
extern void JSONNode_ParseElement_mDCCE725DBE03152408074991DA9C39E9354531DC ();
// 0x000003EB OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m832D509168C00484CA22C19B6D8B86DF59CFD6A7 ();
// 0x000003EC OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::GetContainer(OVRSimpleJSON.JSONContainerType)
extern void JSONNode_GetContainer_mD04109B86D61D53638A881BB3AAA6FA2809C213E ();
// 0x000003ED OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector2)
extern void JSONNode_op_Implicit_m1AA0471E2312C655367AF20A2E123C608C0A1200 ();
// 0x000003EE OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector3)
extern void JSONNode_op_Implicit_mD6A80B1CA2B86DDCF9158ECD3490B7CF05D494E8 ();
// 0x000003EF OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector4)
extern void JSONNode_op_Implicit_m1A5115F2F2E5D8DFED0CF4DF95E06FFA34377A35 ();
// 0x000003F0 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.Quaternion)
extern void JSONNode_op_Implicit_mA666C49A2B2440A4EC3DF6108082C4D4D4F28759 ();
// 0x000003F1 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.Rect)
extern void JSONNode_op_Implicit_m0EA8CC32933C1F2FDE15CD654FBD107B9C8DAEEA ();
// 0x000003F2 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::op_Implicit(UnityEngine.RectOffset)
extern void JSONNode_op_Implicit_mF44569AA7DD35AEBFEB31A23B0BA008F295259DC ();
// 0x000003F3 UnityEngine.Vector2 OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1FAA8E5278CFE1DF35E9B7F273425A8347615390 ();
// 0x000003F4 UnityEngine.Vector3 OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1D2177CB401073D9B7B0E36B6DDE222C9183ECA9 ();
// 0x000003F5 UnityEngine.Vector4 OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mBB184E4D762C26E377EFFE2554A966C7CC19250C ();
// 0x000003F6 UnityEngine.Quaternion OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m2068B5975A2907FC1990D61348A9069DB3FD1BF8 ();
// 0x000003F7 UnityEngine.Rect OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mFDC236577CA6C4152ED194750340CE46EC41B16A ();
// 0x000003F8 UnityEngine.RectOffset OVRSimpleJSON.JSONNode::op_Implicit(OVRSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m824F14AEFFB463D28E8593A8E2B6F7C6AF9A668D ();
// 0x000003F9 UnityEngine.Vector2 OVRSimpleJSON.JSONNode::ReadVector2(UnityEngine.Vector2)
extern void JSONNode_ReadVector2_m89EC5FA944988E40960CBDF49E082394C4B5FF48 ();
// 0x000003FA UnityEngine.Vector2 OVRSimpleJSON.JSONNode::ReadVector2(System.String,System.String)
extern void JSONNode_ReadVector2_mD3137E919A1419CDF5F6D3A3CFFE05096CC145B9 ();
// 0x000003FB UnityEngine.Vector2 OVRSimpleJSON.JSONNode::ReadVector2()
extern void JSONNode_ReadVector2_m6C7B881A666095A3BE3EB3260524C94CA9604290 ();
// 0x000003FC OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteVector2(UnityEngine.Vector2,System.String,System.String)
extern void JSONNode_WriteVector2_mCD0BCFE4A6C5E0BCDCAA9F8E700B1BFEB7DFC9F4 ();
// 0x000003FD UnityEngine.Vector3 OVRSimpleJSON.JSONNode::ReadVector3(UnityEngine.Vector3)
extern void JSONNode_ReadVector3_mB07D8F0603DCB9AC4709F9BAC84731E156C3882D ();
// 0x000003FE UnityEngine.Vector3 OVRSimpleJSON.JSONNode::ReadVector3(System.String,System.String,System.String)
extern void JSONNode_ReadVector3_mC7D09C06D738A7B823EA348747CFCB12675F8B1E ();
// 0x000003FF UnityEngine.Vector3 OVRSimpleJSON.JSONNode::ReadVector3()
extern void JSONNode_ReadVector3_m13C11E7B63B63E9C4A31CB257AD5703E3502B578 ();
// 0x00000400 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteVector3(UnityEngine.Vector3,System.String,System.String,System.String)
extern void JSONNode_WriteVector3_mE84448338B6ED90F950539883F1586465C841818 ();
// 0x00000401 UnityEngine.Vector4 OVRSimpleJSON.JSONNode::ReadVector4(UnityEngine.Vector4)
extern void JSONNode_ReadVector4_m414C7FDC4BA7A25C61AD296DA2EEAB004BE05417 ();
// 0x00000402 UnityEngine.Vector4 OVRSimpleJSON.JSONNode::ReadVector4()
extern void JSONNode_ReadVector4_mEE487157F980579A92E8F98019F6F17EF28DCE2C ();
// 0x00000403 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteVector4(UnityEngine.Vector4)
extern void JSONNode_WriteVector4_m8C09A205FEF9F131DF0066E2B56E44E684041EB0 ();
// 0x00000404 UnityEngine.Quaternion OVRSimpleJSON.JSONNode::ReadQuaternion(UnityEngine.Quaternion)
extern void JSONNode_ReadQuaternion_m79D5410344207A4DF536D4EF784D5C68DF7808E5 ();
// 0x00000405 UnityEngine.Quaternion OVRSimpleJSON.JSONNode::ReadQuaternion()
extern void JSONNode_ReadQuaternion_mA44E0C5E2C3D1E5AC0CB75A85766D5CD6407304A ();
// 0x00000406 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteQuaternion(UnityEngine.Quaternion)
extern void JSONNode_WriteQuaternion_mED82ED486FEDA1468D8D8E1D65504B3CE6D75973 ();
// 0x00000407 UnityEngine.Rect OVRSimpleJSON.JSONNode::ReadRect(UnityEngine.Rect)
extern void JSONNode_ReadRect_m37FF16C0B6C4D8C05FF5D4E59ADD52E17011279B ();
// 0x00000408 UnityEngine.Rect OVRSimpleJSON.JSONNode::ReadRect()
extern void JSONNode_ReadRect_mE2059C2DBA39876F7A978998BE08A532F260218C ();
// 0x00000409 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteRect(UnityEngine.Rect)
extern void JSONNode_WriteRect_m28194F89DF767195D296DFF318799314068FE847 ();
// 0x0000040A UnityEngine.RectOffset OVRSimpleJSON.JSONNode::ReadRectOffset(UnityEngine.RectOffset)
extern void JSONNode_ReadRectOffset_m58BF23AC3C70F99D67393D6A5961CE06533E4F0F ();
// 0x0000040B UnityEngine.RectOffset OVRSimpleJSON.JSONNode::ReadRectOffset()
extern void JSONNode_ReadRectOffset_m72AD6FD0D2ECD30DBDA6449D71354E248E28284B ();
// 0x0000040C OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteRectOffset(UnityEngine.RectOffset)
extern void JSONNode_WriteRectOffset_mD5084D28F9708BC670F474CF92BDFE99656D50AC ();
// 0x0000040D UnityEngine.Matrix4x4 OVRSimpleJSON.JSONNode::ReadMatrix()
extern void JSONNode_ReadMatrix_m81B1AA274B2B5315AC023F60A1802492B84EDA29 ();
// 0x0000040E OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode::WriteMatrix(UnityEngine.Matrix4x4)
extern void JSONNode_WriteMatrix_m98E8E5997B1230719BE0E1C16754803AE23FF91A ();
// 0x0000040F System.Void OVRSimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mCE7840687B8368CF97DAF82FF10AE6161C0C0812 ();
// 0x00000410 System.Void OVRSimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mF74ED433D9180175F7CFAF62778F8010D5D7573B ();
// 0x00000411 System.Boolean OVRSimpleJSON.JSONNode_Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mE123BBA83C796F69D735D0B2FCC7B181A02A0EA1_AdjustorThunk ();
// 0x00000412 System.Void OVRSimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.List`1_Enumerator<OVRSimpleJSON.JSONNode>)
extern void Enumerator__ctor_mD261FA63B0FD69D40B9DD906CB9A229937A725E5_AdjustorThunk ();
// 0x00000413 System.Void OVRSimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,OVRSimpleJSON.JSONNode>)
extern void Enumerator__ctor_m387D645DBA722C95B907D4A3A34FFA51F952D9AF_AdjustorThunk ();
// 0x00000414 System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode_Enumerator::get_Current()
extern void Enumerator_get_Current_mD3F62AE5476416BD4011A26AFDE7C2296532862B_AdjustorThunk ();
// 0x00000415 System.Boolean OVRSimpleJSON.JSONNode_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m811DA47435A443DD4C7046FAE60CF3395FDDA853_AdjustorThunk ();
// 0x00000416 System.Void OVRSimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<OVRSimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m28DA7821F52F4026F144758562461E34F274E849_AdjustorThunk ();
// 0x00000417 System.Void OVRSimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,OVRSimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m44BE054B01BC96EED1BFEDDB743C370A1C25CAA9_AdjustorThunk ();
// 0x00000418 System.Void OVRSimpleJSON.JSONNode_ValueEnumerator::.ctor(OVRSimpleJSON.JSONNode_Enumerator)
extern void ValueEnumerator__ctor_mB8BD50FEAC6DB9F897FA852573D5F0C64716EA8F_AdjustorThunk ();
// 0x00000419 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode_ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mA038DA91BDADAC416EEC79DC94310383CA9F6E5C_AdjustorThunk ();
// 0x0000041A System.Boolean OVRSimpleJSON.JSONNode_ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_m703E2108777E10816D6A634F16A130B6DE198804_AdjustorThunk ();
// 0x0000041B OVRSimpleJSON.JSONNode_ValueEnumerator OVRSimpleJSON.JSONNode_ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m27EF6AEEEC34F5ED08DDB69429DF6037DA52C24E_AdjustorThunk ();
// 0x0000041C System.Void OVRSimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<OVRSimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m6D2B45677B312982618C71DF0A308F4574D73360_AdjustorThunk ();
// 0x0000041D System.Void OVRSimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,OVRSimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mB341FFA8F3E36A2663FF0E0E23D2AE5012905FDB_AdjustorThunk ();
// 0x0000041E System.Void OVRSimpleJSON.JSONNode_KeyEnumerator::.ctor(OVRSimpleJSON.JSONNode_Enumerator)
extern void KeyEnumerator__ctor_m23A9E209ED858A69A4E38119CED5A1B1132A4222_AdjustorThunk ();
// 0x0000041F System.String OVRSimpleJSON.JSONNode_KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m0070CFDA79C67097DC93DBFA160B11E2A0218178_AdjustorThunk ();
// 0x00000420 System.Boolean OVRSimpleJSON.JSONNode_KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m1030560B49414A9CDB6FF4693EE1B2CD43616C64_AdjustorThunk ();
// 0x00000421 OVRSimpleJSON.JSONNode_KeyEnumerator OVRSimpleJSON.JSONNode_KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mB1C04C3864CEACBD8B98097A8FFD06B9507F0D15_AdjustorThunk ();
// 0x00000422 System.Void OVRSimpleJSON.JSONNode_LinqEnumerator::.ctor(OVRSimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_mB053B605AE3DE68104D687E22FEA9503BD72B2CA ();
// 0x00000423 System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode_LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m8377DC764296AECC3FCF182C65BEF3FA27879CD1 ();
// 0x00000424 System.Object OVRSimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m71E85F6C0D1DDB0A80958BB88FFFD3BAE93B03C8 ();
// 0x00000425 System.Boolean OVRSimpleJSON.JSONNode_LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_m7B47174A4F7BDD3DD65E4D967F156E43C885A90D ();
// 0x00000426 System.Void OVRSimpleJSON.JSONNode_LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mBB69FA50D51E55484112B55DDDE34EB33B4E4D38 ();
// 0x00000427 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode>> OVRSimpleJSON.JSONNode_LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_mD5E1A0AA4CBAF7A6CE62024EEC64F98BC65EE28C ();
// 0x00000428 System.Void OVRSimpleJSON.JSONNode_LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m7D4F66C18FB1D604F5961298C7B8E6452EA8DF18 ();
// 0x00000429 System.Collections.IEnumerator OVRSimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m1668D6337CA4B5CB616FE1B996578C4D2B7D6F6A ();
// 0x0000042A System.Void OVRSimpleJSON.JSONNode_<get_Children>d__40::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__40__ctor_mAD6CD6872580F578224D94CBB299BCF123BB93C1 ();
// 0x0000042B System.Void OVRSimpleJSON.JSONNode_<get_Children>d__40::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m92248ADCB1ED01C78F70E82DF1BB9CD732369AC5 ();
// 0x0000042C System.Boolean OVRSimpleJSON.JSONNode_<get_Children>d__40::MoveNext()
extern void U3Cget_ChildrenU3Ed__40_MoveNext_m70BB8162EE701BEE9F924CE91547CA3E4FA9E2C4 ();
// 0x0000042D OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode_<get_Children>d__40::System.Collections.Generic.IEnumerator<OVRSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_mF82B0132E2F5BA04FF1C2D6FB1FCE4A2095E7ABE ();
// 0x0000042E System.Void OVRSimpleJSON.JSONNode_<get_Children>d__40::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_mAA068B65006A01891986B8C586A25A3B56D95797 ();
// 0x0000042F System.Object OVRSimpleJSON.JSONNode_<get_Children>d__40::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_mA4CC3694A3AC45D6125C3D3BFFA1C32C31F15E4E ();
// 0x00000430 System.Collections.Generic.IEnumerator`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode_<get_Children>d__40::System.Collections.Generic.IEnumerable<OVRSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mAC929C6443AF52C4AA5003AFC9F9C23DA76D05DD ();
// 0x00000431 System.Collections.IEnumerator OVRSimpleJSON.JSONNode_<get_Children>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m235FA3F4F024D79664561DFBB053DC9F3FB3CE16 ();
// 0x00000432 System.Void OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__42__ctor_m1E734A5A3B49DB0835C452FD8B80B4804A73C763 ();
// 0x00000433 System.Void OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_mB86CF5FA23A85902E1079B4CE4F0A28D8AC35506 ();
// 0x00000434 System.Boolean OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__42_MoveNext_m471E7C3CC88185A5F91EEC8C3739D586479B1DC0 ();
// 0x00000435 System.Void OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_m9A14EEF2617277D0CC69BB883E9DFED91325D082 ();
// 0x00000436 System.Void OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_m27C9FF86FA820950A232E20C1B78215AA26AA4D1 ();
// 0x00000437 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.Collections.Generic.IEnumerator<OVRSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_mF35F302F8A362C821BCCF27512EA356E932B24C3 ();
// 0x00000438 System.Void OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_mDB0C93551DDE6AA31442E328E07565C13DDE31AB ();
// 0x00000439 System.Object OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_m8344EC56018958D1876E7C639C5914A3A4EC985D ();
// 0x0000043A System.Collections.Generic.IEnumerator`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.Collections.Generic.IEnumerable<OVRSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mB6B41E8663270F9CE14B58BDB1F04C4E0DE07EEC ();
// 0x0000043B System.Collections.IEnumerator OVRSimpleJSON.JSONNode_<get_DeepChildren>d__42::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB4B8437C9D0AFC6E89E0CA69D3D4D4799F644073 ();
// 0x0000043C System.Boolean OVRSimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mAF078AEF285D10A110C1F0A39AE86448B97CA723 ();
// 0x0000043D System.Void OVRSimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m40C0873AF1FE5B71A192AF177D72593D4DF826A3 ();
// 0x0000043E OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m42FB5B0477396B81582E9559EFCCBDF9C35951D5 ();
// 0x0000043F System.Boolean OVRSimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_mC889F3B86EC345120EF71DEED6E1C444D543967C ();
// 0x00000440 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m9CD59C8DE819F59571B02F90904C0A6A84A9F9F7 ();
// 0x00000441 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m96F1F0DC6003D15C1BDC6E725E13228F079B94E8 ();
// 0x00000442 System.Void OVRSimpleJSON.JSONArray::set_Item(System.Int32,OVRSimpleJSON.JSONNode)
extern void JSONArray_set_Item_m9CCAD03B83783F5F7BB6617AC0E691FF375EDA67 ();
// 0x00000443 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_mE00DCE6138678487E5F7C0A9C766CF41D83C4B56 ();
// 0x00000444 System.Void OVRSimpleJSON.JSONArray::set_Item(System.String,OVRSimpleJSON.JSONNode)
extern void JSONArray_set_Item_mF58D55AC31388B1ACA7FF55B1C918D0B3D4C1C52 ();
// 0x00000445 System.Int32 OVRSimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m9AD3FD0D8CC0ACA09D5BD9B7DA5876FF5361307E ();
// 0x00000446 System.Void OVRSimpleJSON.JSONArray::Add(System.String,OVRSimpleJSON.JSONNode)
extern void JSONArray_Add_m4D865F50D473E8D19C42F97721A5EC7E073B6380 ();
// 0x00000447 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mBA01E42BB17B3B27933F5E8278B3135C92AEF3AE ();
// 0x00000448 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONArray::Remove(OVRSimpleJSON.JSONNode)
extern void JSONArray_Remove_mDE68BB43ABDC9CB352EA39D0890853CF33453999 ();
// 0x00000449 System.Collections.Generic.IEnumerable`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m8496DAA6D259D5C7E9CE43BF5895A4C5B7CF853C ();
// 0x0000044A System.Void OVRSimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m83F2719DF4541A8EA401AA31FA3DE1D82B1D1404 ();
// 0x0000044B System.Void OVRSimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m84F1E1EC0F85EFF095C178CF992D94DAD7A20259 ();
// 0x0000044C System.Void OVRSimpleJSON.JSONArray_<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m9AE3E99927C10B7209F13A69BC18CEAA37D33AF2 ();
// 0x0000044D System.Void OVRSimpleJSON.JSONArray_<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mE2D7EA5B1A304C606FA1E15BBFD971F0D77F4012 ();
// 0x0000044E System.Boolean OVRSimpleJSON.JSONArray_<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m57F7D3D844E4C307D8F15771BB308AAE33154FAF ();
// 0x0000044F System.Void OVRSimpleJSON.JSONArray_<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m45275F57BBE360D9746E494CD833F38DE18406F9 ();
// 0x00000450 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerator<OVRSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_m77CC70426E1BE7CAD3FA9D65D1DA4F318DE36D28 ();
// 0x00000451 System.Void OVRSimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m0B28E39C83A7028AADB0629626919F907D98E96D ();
// 0x00000452 System.Object OVRSimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m8F4D15331C631EF8595F0CA20DF905A882BAFA75 ();
// 0x00000453 System.Collections.Generic.IEnumerator`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerable<OVRSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mAF84935A98A444C0727D416D561C318A3BD7D81E ();
// 0x00000454 System.Collections.IEnumerator OVRSimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m5067564BE513F6A53E861581819EC1369E14947C ();
// 0x00000455 System.Boolean OVRSimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mC1760B920AB4B95ABA2436237103AE18450EE822 ();
// 0x00000456 System.Void OVRSimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_m6A232439E4C175753F9D6B04D1D81688B778EDAD ();
// 0x00000457 OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_mDF7FEBED36F50B0EEB51D6A5641DC9C8FDFBCE4A ();
// 0x00000458 System.Boolean OVRSimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_mD510E36DA5091AFA8347FAD189FCDF6575B5C555 ();
// 0x00000459 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_mFC5B33B3DE809479C0D8CD6721D685DECB60680F ();
// 0x0000045A OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_m64F0403FFD193DE2EA5A7BE3D7E6E2EACC738BBB ();
// 0x0000045B System.Void OVRSimpleJSON.JSONObject::set_Item(System.String,OVRSimpleJSON.JSONNode)
extern void JSONObject_set_Item_mDF3D2A2515CC5AB82CC2AC19EAF767AD103BFE99 ();
// 0x0000045C OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_mB68C5D2CDEAC5B9D145468611C3221971BB9636D ();
// 0x0000045D System.Void OVRSimpleJSON.JSONObject::set_Item(System.Int32,OVRSimpleJSON.JSONNode)
extern void JSONObject_set_Item_m9241F889D7A773B184165A85305CE4B458A86047 ();
// 0x0000045E System.Int32 OVRSimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m99D647A694A126773BAE03CFB23D7D4643BDDF93 ();
// 0x0000045F System.Void OVRSimpleJSON.JSONObject::Add(System.String,OVRSimpleJSON.JSONNode)
extern void JSONObject_Add_m2379E4B57F29A6A7FFC7B470CAA8930E8AA4D31E ();
// 0x00000460 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_mCD9E5C9DDBAE2EBBE034697152A184233ADDC80E ();
// 0x00000461 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m908875BEC1ACFA58EE910C91DC2150224E782931 ();
// 0x00000462 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject::Remove(OVRSimpleJSON.JSONNode)
extern void JSONObject_Remove_mC80963F65BB4803E89B79CC7DC07474EA99B0F16 ();
// 0x00000463 System.Collections.Generic.IEnumerable`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m83F4DA97C496533E579B9CC02C9E11A3BF30D9A4 ();
// 0x00000464 System.Void OVRSimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m5112F6C48FFA03E323F5C5B5AF9D577816E1FBDF ();
// 0x00000465 System.Void OVRSimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m150F04D7B1139BC04F600A8D1C8572310DA27EB4 ();
// 0x00000466 System.Void OVRSimpleJSON.JSONObject_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mF2D75E02D7F687BF83E0AE5D8C21CD24DE8C24FE ();
// 0x00000467 System.Boolean OVRSimpleJSON.JSONObject_<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,OVRSimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_mAA6E310D01B9D1F2702034D23FFC286ED4DA49D8 ();
// 0x00000468 System.Void OVRSimpleJSON.JSONObject_<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m761C1CC855D1A5DD65F0B3F5A6AAA59BCBC20BF7 ();
// 0x00000469 System.Void OVRSimpleJSON.JSONObject_<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m5E329BCB7A2590EE790AFB8D060CCF6255499C1A ();
// 0x0000046A System.Boolean OVRSimpleJSON.JSONObject_<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m0557A7FE061E161FD92B34159500C00FD7115488 ();
// 0x0000046B System.Void OVRSimpleJSON.JSONObject_<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m71659AE80BF7C55F9CB0F2522F226609AB9B15F5 ();
// 0x0000046C OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerator<OVRSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_m87165354C7AF46925B9975D1E782E91D57FF4C66 ();
// 0x0000046D System.Void OVRSimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m379784AB70A4695E82987816F0C7CF87209D2CF2 ();
// 0x0000046E System.Object OVRSimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m8581BF31291E50CE32B31469AB156A58E15BEF04 ();
// 0x0000046F System.Collections.Generic.IEnumerator`1<OVRSimpleJSON.JSONNode> OVRSimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerable<OVRSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mF763C9B0C7129679056EF542060364099D90868F ();
// 0x00000470 System.Collections.IEnumerator OVRSimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m767871B41389012F2889D67BFF4EFCF08BEEB8B1 ();
// 0x00000471 OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_mE5BA7132E89AE13F605134B5A4F3AE92B5F17CC1 ();
// 0x00000472 System.Boolean OVRSimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mBBBFA269B86A1ECECA9BAB6FAB5264F4C13C6E97 ();
// 0x00000473 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m0771C20646D2DFC9BEA6F3DA22E60C4199A2DBD4 ();
// 0x00000474 System.String OVRSimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m355370C080DFEB63A09AC9B0FA1A2F851D5CC324 ();
// 0x00000475 System.Void OVRSimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_mFF6D420839FAB0B413C9AC1C278BC03F649E7AD3 ();
// 0x00000476 System.Void OVRSimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m4C57021CA53FF07A1ADFCDE7817F0A5EB1C645BE ();
// 0x00000477 System.Void OVRSimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_m2026B3AC2462337284DF0F25EB3956A48E0B885D ();
// 0x00000478 System.Boolean OVRSimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m38158581EB8283D9B9A55FFC4FB7AF8F1C660D1D ();
// 0x00000479 System.Int32 OVRSimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_mFDA60DD6060654176B088E1DE944B329F12C79B8 ();
// 0x0000047A OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_mE13CEBF2F06154FD2EB85BFEA5A4D2DB242FCEB0 ();
// 0x0000047B System.Boolean OVRSimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mA0D9672501245C9AE9B4AD6E6089B69BFCB34961 ();
// 0x0000047C OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_mC15BACA3C6B8F656325EF4EF45F2C0268BECC8BE ();
// 0x0000047D System.String OVRSimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_mA44013E2EA65E059CDFCE5CEE52C4983F4A087D1 ();
// 0x0000047E System.Void OVRSimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m7582A93899259AE9EA236C5CA4B25013F6271950 ();
// 0x0000047F System.Double OVRSimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_mE62C503EB0C807A1368C2B334A4FDCE81CD812FA ();
// 0x00000480 System.Void OVRSimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_mF4963A2469A701264931075ADBCD14281E30131F ();
// 0x00000481 System.Int64 OVRSimpleJSON.JSONNumber::get_AsLong()
extern void JSONNumber_get_AsLong_m5555868108391C3660DD945373FA779BF601949E ();
// 0x00000482 System.Void OVRSimpleJSON.JSONNumber::set_AsLong(System.Int64)
extern void JSONNumber_set_AsLong_mC44836E8747ECC0344F56B1B921D4C959D5407D7 ();
// 0x00000483 System.Void OVRSimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_m27B4556EFB6F0009935C440276FD70CF9EBCE891 ();
// 0x00000484 System.Void OVRSimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m86B496855043132F187D5A8B279E18F8557D8A15 ();
// 0x00000485 System.Void OVRSimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m555012920C544B0B93893AB5E9562F99693B6981 ();
// 0x00000486 System.Boolean OVRSimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_m33E35EE7B944C73BBC320334970FFE811E0F29B9 ();
// 0x00000487 System.Boolean OVRSimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_m4B21D4C3F2086AA46C7EF9B1963D265B54E54F18 ();
// 0x00000488 System.Int32 OVRSimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mFCFAC73A33977CBB384F434AC7B744EFBDE4C177 ();
// 0x00000489 OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mC563B972CCE147E1DEE64D4EB21122CBE6B965DF ();
// 0x0000048A System.Boolean OVRSimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m9BEF919F560D6419A294C948AB5F6D41AEF362D8 ();
// 0x0000048B OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_m79C66C8F6B84C1469291E79D58932FBDA4ECC93C ();
// 0x0000048C System.String OVRSimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mA17B179F2AFA1BD846652BE66D839CF8BFB72AA9 ();
// 0x0000048D System.Void OVRSimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mD8702E0F9DA86E92B764D522BAB151C4266E7560 ();
// 0x0000048E System.Boolean OVRSimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_m916D19CBB78F88DAA67F23940107ACECD136E8BF ();
// 0x0000048F System.Void OVRSimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m98D346609202C2795A23213C89492E3F08312B05 ();
// 0x00000490 System.Void OVRSimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_m118638B3D19A93430034FF43B4ECE6ABF223DB76 ();
// 0x00000491 System.Void OVRSimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_mC14E6B634C72FD2D35647D24C21848C768F20270 ();
// 0x00000492 System.Void OVRSimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_mA5F5B224213511F9FAB9DC710670D1C4BA507F60 ();
// 0x00000493 System.Boolean OVRSimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m0845A60B76EF045468E02A7199D8421FE05B4C69 ();
// 0x00000494 System.Int32 OVRSimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_mB7E0C3E291BFE38A03865CA5643972739E94F7D4 ();
// 0x00000495 OVRSimpleJSON.JSONNull OVRSimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mDE760A7DCD5513E6DC4241FC534FA6C2AAB1E642 ();
// 0x00000496 System.Void OVRSimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m3502BF126A8926B8784D591742425EB95C6563C9 ();
// 0x00000497 OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_mDB88127BA74CF00677AC4A644B91EDF11D3604C8 ();
// 0x00000498 System.Boolean OVRSimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_mFBB993FBF500F81A082AF0468CE898100ECAA2B3 ();
// 0x00000499 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_mBC3ACFCCB37FA9636CD7455447D0772684945D0B ();
// 0x0000049A System.String OVRSimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m99D3B008678BBCE9F4B141CD4237EC078B01CE0B ();
// 0x0000049B System.Void OVRSimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mE30CC6719CCBB798A15284DC324AF3040667F824 ();
// 0x0000049C System.Boolean OVRSimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m2249E1308A275CE4D2CEC1118D21EA73E32B4D51 ();
// 0x0000049D System.Void OVRSimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m24BFBAE4FBB988822E8F062DA58A43476512D261 ();
// 0x0000049E System.Boolean OVRSimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_mC88F3FCEFF6E18A8F02566BAB438A89938DC7960 ();
// 0x0000049F System.Int32 OVRSimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m91672C1BF77B94CB79D361CD11348521CDF05183 ();
// 0x000004A0 System.Void OVRSimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_mF75F4C5C6DE6B124046564C36B4ABD5782ADE737 ();
// 0x000004A1 System.Void OVRSimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_m2776B02040819D16A8329C3F74B6F3C8806C7DAD ();
// 0x000004A2 OVRSimpleJSON.JSONNodeType OVRSimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_mAADC5C7865835338A5461BC698D743F07D5F5D2B ();
// 0x000004A3 OVRSimpleJSON.JSONNode_Enumerator OVRSimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_mAC948A27B43CB02227B6880578C48F8D1BE2F929 ();
// 0x000004A4 System.Void OVRSimpleJSON.JSONLazyCreator::.ctor(OVRSimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_mCFC9265D05453A136EF8814636DEC8E1B723347C ();
// 0x000004A5 System.Void OVRSimpleJSON.JSONLazyCreator::.ctor(OVRSimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mD5E1018E27201444A9C37D79B96BC267FEDE765E ();
// 0x000004A6 T OVRSimpleJSON.JSONLazyCreator::Set(T)
// 0x000004A7 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_mDF098D0C4D073C5057FC5883F05D2956322F2939 ();
// 0x000004A8 System.Void OVRSimpleJSON.JSONLazyCreator::set_Item(System.Int32,OVRSimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mC254391476B81EE16DE3CD73AF85167B6A9A856B ();
// 0x000004A9 OVRSimpleJSON.JSONNode OVRSimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_mFB6397062B4F12C2A3E271336784FE652CE4D132 ();
// 0x000004AA System.Void OVRSimpleJSON.JSONLazyCreator::set_Item(System.String,OVRSimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m6ADAEB6321383D581067AB35B6CCB6C266C0C760 ();
// 0x000004AB System.Void OVRSimpleJSON.JSONLazyCreator::Add(OVRSimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m474AB0D4A2C2527E1B92C215C92F6AC60C89C1DD ();
// 0x000004AC System.Void OVRSimpleJSON.JSONLazyCreator::Add(System.String,OVRSimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m92289FA80C186C4908BE3209FC86056836603039 ();
// 0x000004AD System.Boolean OVRSimpleJSON.JSONLazyCreator::op_Equality(OVRSimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m1FE662C081F2C0B639E274A9313D6A8ED8A4B70B ();
// 0x000004AE System.Boolean OVRSimpleJSON.JSONLazyCreator::op_Inequality(OVRSimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m619DBCA769060479E9944B4B2BBD73D2B538D45B ();
// 0x000004AF System.Boolean OVRSimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m1C147E2F6E2103726BAB70B71DC5C23B59B7CE72 ();
// 0x000004B0 System.Int32 OVRSimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_mF56F3BA22470B6B97E42BD58C3EF47E07EEA567D ();
// 0x000004B1 System.Int32 OVRSimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_mF0338533F4A721F62FE286852320636F76CF75FD ();
// 0x000004B2 System.Void OVRSimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_mF386C6C522FF4A5F68B96926898161C0F6F3D18D ();
// 0x000004B3 System.Single OVRSimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_mB785CDB3A3BC7EF9EC686AE95E885F22EE86D361 ();
// 0x000004B4 System.Void OVRSimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m82B4D141131E8EF56DCB5A4FEA13EFA01C8B23A8 ();
// 0x000004B5 System.Double OVRSimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_mAD4401A132188578D1E91363BF35035AA4C7C4E3 ();
// 0x000004B6 System.Void OVRSimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m2ED433CB35199F0B73E76332BC995C8907CF1C14 ();
// 0x000004B7 System.Int64 OVRSimpleJSON.JSONLazyCreator::get_AsLong()
extern void JSONLazyCreator_get_AsLong_mDD08CF4E447F6F2773263AC40A7984B30FB5EDA6 ();
// 0x000004B8 System.Void OVRSimpleJSON.JSONLazyCreator::set_AsLong(System.Int64)
extern void JSONLazyCreator_set_AsLong_mA80BFA6C245B7FA84718A4A674230F328F58524B ();
// 0x000004B9 System.Boolean OVRSimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m69E6BFE69A8326EEA059BD0484B824772D5CD345 ();
// 0x000004BA System.Void OVRSimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_mBD035BEDF122508AABC4ED493A7F09E2B2C5409E ();
// 0x000004BB OVRSimpleJSON.JSONArray OVRSimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mB80673EEF5B90AF4EA632078CC54B94E5FF62BD7 ();
// 0x000004BC OVRSimpleJSON.JSONObject OVRSimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m8584284E55FDA11DB95C85A43ACA8E1BDCA5B8EB ();
// 0x000004BD System.Void OVRSimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,OVRSimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m3063F0DD56479E7CB9234F2A90A02189CD745FA8 ();
// 0x000004BE OVRSimpleJSON.JSONNode OVRSimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mD85F2D33E54B9617F6CDB4720033DED2FC30F28E ();
// 0x000004BF System.Void OVR.OpenVR.IVRSystem__GetRecommendedRenderTargetSize::.ctor(System.Object,System.IntPtr)
extern void _GetRecommendedRenderTargetSize__ctor_mC79DD5ECC62BF30F75BA4A11FC75C3AACDAA9603 ();
// 0x000004C0 System.Void OVR.OpenVR.IVRSystem__GetRecommendedRenderTargetSize::Invoke(System.UInt32&,System.UInt32&)
extern void _GetRecommendedRenderTargetSize_Invoke_m0A0C52AC07E9A885DCD3948D84F4E7743061D681 ();
// 0x000004C1 System.IAsyncResult OVR.OpenVR.IVRSystem__GetRecommendedRenderTargetSize::BeginInvoke(System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetRecommendedRenderTargetSize_BeginInvoke_mDD67D6B93AFCE852B0A76A64DD3C4B803C91B215 ();
// 0x000004C2 System.Void OVR.OpenVR.IVRSystem__GetRecommendedRenderTargetSize::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetRecommendedRenderTargetSize_EndInvoke_mEE6F2E5E6570EE5460180B465B8360E1DE35AD8E ();
// 0x000004C3 System.Void OVR.OpenVR.IVRSystem__GetProjectionMatrix::.ctor(System.Object,System.IntPtr)
extern void _GetProjectionMatrix__ctor_mD719074A436075639F384534A71D52B719250025 ();
// 0x000004C4 OVR.OpenVR.HmdMatrix44_t OVR.OpenVR.IVRSystem__GetProjectionMatrix::Invoke(OVR.OpenVR.EVREye,System.Single,System.Single)
extern void _GetProjectionMatrix_Invoke_m8FC606E7AB49ACCCCD201CF692F8D38D367EB7AC ();
// 0x000004C5 System.IAsyncResult OVR.OpenVR.IVRSystem__GetProjectionMatrix::BeginInvoke(OVR.OpenVR.EVREye,System.Single,System.Single,System.AsyncCallback,System.Object)
extern void _GetProjectionMatrix_BeginInvoke_m8473D762C98EDB38C80E0A1B8A763C7AC87A671C ();
// 0x000004C6 OVR.OpenVR.HmdMatrix44_t OVR.OpenVR.IVRSystem__GetProjectionMatrix::EndInvoke(System.IAsyncResult)
extern void _GetProjectionMatrix_EndInvoke_m782AFFE1F530B3CDD13CEB74A3BFEB9BD87059E9 ();
// 0x000004C7 System.Void OVR.OpenVR.IVRSystem__GetProjectionRaw::.ctor(System.Object,System.IntPtr)
extern void _GetProjectionRaw__ctor_m12AD08852797DF0F160C618D4778028F3B4B1646 ();
// 0x000004C8 System.Void OVR.OpenVR.IVRSystem__GetProjectionRaw::Invoke(OVR.OpenVR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&)
extern void _GetProjectionRaw_Invoke_mA18B089BD9ABFF6996658085D6E8A98E721EA065 ();
// 0x000004C9 System.IAsyncResult OVR.OpenVR.IVRSystem__GetProjectionRaw::BeginInvoke(OVR.OpenVR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetProjectionRaw_BeginInvoke_m831D8F4C9332B2178A6207FFC51330AA5F0FA4E9 ();
// 0x000004CA System.Void OVR.OpenVR.IVRSystem__GetProjectionRaw::EndInvoke(System.Single&,System.Single&,System.Single&,System.Single&,System.IAsyncResult)
extern void _GetProjectionRaw_EndInvoke_m6D2B0D861B7C00604D762096ED05518219AF7A45 ();
// 0x000004CB System.Void OVR.OpenVR.IVRSystem__ComputeDistortion::.ctor(System.Object,System.IntPtr)
extern void _ComputeDistortion__ctor_m7980FBA2A9BB3A6B148329F3ED63172FF9CE54E5 ();
// 0x000004CC System.Boolean OVR.OpenVR.IVRSystem__ComputeDistortion::Invoke(OVR.OpenVR.EVREye,System.Single,System.Single,OVR.OpenVR.DistortionCoordinates_t&)
extern void _ComputeDistortion_Invoke_mF3E334140059CCD109EFA8D04AFBE69378CE1278 ();
// 0x000004CD System.IAsyncResult OVR.OpenVR.IVRSystem__ComputeDistortion::BeginInvoke(OVR.OpenVR.EVREye,System.Single,System.Single,OVR.OpenVR.DistortionCoordinates_t&,System.AsyncCallback,System.Object)
extern void _ComputeDistortion_BeginInvoke_mF476A019BB9CF8A39D7C3E65FCBE5110F22C7C64 ();
// 0x000004CE System.Boolean OVR.OpenVR.IVRSystem__ComputeDistortion::EndInvoke(OVR.OpenVR.DistortionCoordinates_t&,System.IAsyncResult)
extern void _ComputeDistortion_EndInvoke_mF326A4397057E3817D01F7A143E9765C3F5A69E4 ();
// 0x000004CF System.Void OVR.OpenVR.IVRSystem__GetEyeToHeadTransform::.ctor(System.Object,System.IntPtr)
extern void _GetEyeToHeadTransform__ctor_m2A367E6806C230446299281346B3734F88018443 ();
// 0x000004D0 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetEyeToHeadTransform::Invoke(OVR.OpenVR.EVREye)
extern void _GetEyeToHeadTransform_Invoke_mD3E7F92F47833ADB36F0335541BDF22B7EF8F1EF ();
// 0x000004D1 System.IAsyncResult OVR.OpenVR.IVRSystem__GetEyeToHeadTransform::BeginInvoke(OVR.OpenVR.EVREye,System.AsyncCallback,System.Object)
extern void _GetEyeToHeadTransform_BeginInvoke_mEBD8C24DBAC303EC59189BD27BB75939E79C1B48 ();
// 0x000004D2 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetEyeToHeadTransform::EndInvoke(System.IAsyncResult)
extern void _GetEyeToHeadTransform_EndInvoke_mC968ED6B6CC6EC3BB1E777C96871BE9159B0E280 ();
// 0x000004D3 System.Void OVR.OpenVR.IVRSystem__GetTimeSinceLastVsync::.ctor(System.Object,System.IntPtr)
extern void _GetTimeSinceLastVsync__ctor_m79236D04C7DB98317B1A48A232DA68E924F77453 ();
// 0x000004D4 System.Boolean OVR.OpenVR.IVRSystem__GetTimeSinceLastVsync::Invoke(System.Single&,System.UInt64&)
extern void _GetTimeSinceLastVsync_Invoke_m6898F486DC086B0BEE2CB74FB974D6D33F1DF08B ();
// 0x000004D5 System.IAsyncResult OVR.OpenVR.IVRSystem__GetTimeSinceLastVsync::BeginInvoke(System.Single&,System.UInt64&,System.AsyncCallback,System.Object)
extern void _GetTimeSinceLastVsync_BeginInvoke_mD3C591DCDDAFD8BA5DDEC42D16CEADB8DFA621D0 ();
// 0x000004D6 System.Boolean OVR.OpenVR.IVRSystem__GetTimeSinceLastVsync::EndInvoke(System.Single&,System.UInt64&,System.IAsyncResult)
extern void _GetTimeSinceLastVsync_EndInvoke_m04D3E90F25C3DCAA762F4712289F69136A08E384 ();
// 0x000004D7 System.Void OVR.OpenVR.IVRSystem__GetD3D9AdapterIndex::.ctor(System.Object,System.IntPtr)
extern void _GetD3D9AdapterIndex__ctor_mC50841190AE0832DF71D197F503E5DD9E672F3B1 ();
// 0x000004D8 System.Int32 OVR.OpenVR.IVRSystem__GetD3D9AdapterIndex::Invoke()
extern void _GetD3D9AdapterIndex_Invoke_mE2733FBFA4DEE2E225B310F7A8CED7B9238A4B31 ();
// 0x000004D9 System.IAsyncResult OVR.OpenVR.IVRSystem__GetD3D9AdapterIndex::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetD3D9AdapterIndex_BeginInvoke_m6B396470B2BC3B2D70BA53F0B73D9703DC3085B4 ();
// 0x000004DA System.Int32 OVR.OpenVR.IVRSystem__GetD3D9AdapterIndex::EndInvoke(System.IAsyncResult)
extern void _GetD3D9AdapterIndex_EndInvoke_m35A8B48836E0F028DD65F475071A9AB45361F574 ();
// 0x000004DB System.Void OVR.OpenVR.IVRSystem__GetDXGIOutputInfo::.ctor(System.Object,System.IntPtr)
extern void _GetDXGIOutputInfo__ctor_m257F225CF99F8939E98DECF542A7ADE5D06AF662 ();
// 0x000004DC System.Void OVR.OpenVR.IVRSystem__GetDXGIOutputInfo::Invoke(System.Int32&)
extern void _GetDXGIOutputInfo_Invoke_m05F0F871FB1FC093DA80FA138F065E30DE7EE2B3 ();
// 0x000004DD System.IAsyncResult OVR.OpenVR.IVRSystem__GetDXGIOutputInfo::BeginInvoke(System.Int32&,System.AsyncCallback,System.Object)
extern void _GetDXGIOutputInfo_BeginInvoke_mFE865294BC64CA64D543954481C0E8964415939A ();
// 0x000004DE System.Void OVR.OpenVR.IVRSystem__GetDXGIOutputInfo::EndInvoke(System.Int32&,System.IAsyncResult)
extern void _GetDXGIOutputInfo_EndInvoke_m178601C3B3336C9CA133E13061A15921DB286CDE ();
// 0x000004DF System.Void OVR.OpenVR.IVRSystem__GetOutputDevice::.ctor(System.Object,System.IntPtr)
extern void _GetOutputDevice__ctor_mEA350DB5FAF7E9785833182E4EF688832583E296 ();
// 0x000004E0 System.Void OVR.OpenVR.IVRSystem__GetOutputDevice::Invoke(System.UInt64&,OVR.OpenVR.ETextureType,System.IntPtr)
extern void _GetOutputDevice_Invoke_mBE050AB1EF4B2092E94A0D266C49C4AAB659C27A ();
// 0x000004E1 System.IAsyncResult OVR.OpenVR.IVRSystem__GetOutputDevice::BeginInvoke(System.UInt64&,OVR.OpenVR.ETextureType,System.IntPtr,System.AsyncCallback,System.Object)
extern void _GetOutputDevice_BeginInvoke_m6E37010E549612AA42D4CF7CA8243D5FF07B8DB3 ();
// 0x000004E2 System.Void OVR.OpenVR.IVRSystem__GetOutputDevice::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _GetOutputDevice_EndInvoke_m4D6A82385ED303BF97243A80FD35301F434E5DDB ();
// 0x000004E3 System.Void OVR.OpenVR.IVRSystem__IsDisplayOnDesktop::.ctor(System.Object,System.IntPtr)
extern void _IsDisplayOnDesktop__ctor_mBF352ABE73668899A65A035D6D8D62E8E3679762 ();
// 0x000004E4 System.Boolean OVR.OpenVR.IVRSystem__IsDisplayOnDesktop::Invoke()
extern void _IsDisplayOnDesktop_Invoke_m7EFAFE10D02463D938C16485C00371F9FBFB8F69 ();
// 0x000004E5 System.IAsyncResult OVR.OpenVR.IVRSystem__IsDisplayOnDesktop::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsDisplayOnDesktop_BeginInvoke_m92E2E186C5F9D75702D28C40E063FB11CAA979F6 ();
// 0x000004E6 System.Boolean OVR.OpenVR.IVRSystem__IsDisplayOnDesktop::EndInvoke(System.IAsyncResult)
extern void _IsDisplayOnDesktop_EndInvoke_m000F74B2CDE8A15720BC63A140DBFF285458EF6F ();
// 0x000004E7 System.Void OVR.OpenVR.IVRSystem__SetDisplayVisibility::.ctor(System.Object,System.IntPtr)
extern void _SetDisplayVisibility__ctor_m1277F99F880C333895795615C41FFB5757A61A43 ();
// 0x000004E8 System.Boolean OVR.OpenVR.IVRSystem__SetDisplayVisibility::Invoke(System.Boolean)
extern void _SetDisplayVisibility_Invoke_m59E3B6EBC29FC6312A2188B100089243E5A39C86 ();
// 0x000004E9 System.IAsyncResult OVR.OpenVR.IVRSystem__SetDisplayVisibility::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void _SetDisplayVisibility_BeginInvoke_m9E7728A03C9B70E72F0BE5CD2E061506726C5C84 ();
// 0x000004EA System.Boolean OVR.OpenVR.IVRSystem__SetDisplayVisibility::EndInvoke(System.IAsyncResult)
extern void _SetDisplayVisibility_EndInvoke_m9F45EF027B15E502171726158DF21C5130E01792 ();
// 0x000004EB System.Void OVR.OpenVR.IVRSystem__GetDeviceToAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetDeviceToAbsoluteTrackingPose__ctor_mF86295906E0AC7BCC5F642F5610EC09E79980E06 ();
// 0x000004EC System.Void OVR.OpenVR.IVRSystem__GetDeviceToAbsoluteTrackingPose::Invoke(OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32)
extern void _GetDeviceToAbsoluteTrackingPose_Invoke_mB4A1B7F9E33FC2FE03E9668830EBC3262EA5E9C4 ();
// 0x000004ED System.IAsyncResult OVR.OpenVR.IVRSystem__GetDeviceToAbsoluteTrackingPose::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _GetDeviceToAbsoluteTrackingPose_BeginInvoke_m8159059337705510440E54E4173759C74BCFEE50 ();
// 0x000004EE System.Void OVR.OpenVR.IVRSystem__GetDeviceToAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern void _GetDeviceToAbsoluteTrackingPose_EndInvoke_mAE0F24BA614C4751A27395D2D58228B7EAD45A8B ();
// 0x000004EF System.Void OVR.OpenVR.IVRSystem__ResetSeatedZeroPose::.ctor(System.Object,System.IntPtr)
extern void _ResetSeatedZeroPose__ctor_mE7A271DEE8238F0A90A393EA528B5956C4389271 ();
// 0x000004F0 System.Void OVR.OpenVR.IVRSystem__ResetSeatedZeroPose::Invoke()
extern void _ResetSeatedZeroPose_Invoke_mE5748E241095720F5720CB0EED246BB94AE888DB ();
// 0x000004F1 System.IAsyncResult OVR.OpenVR.IVRSystem__ResetSeatedZeroPose::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ResetSeatedZeroPose_BeginInvoke_m53B19E2E0731B5A926C3D97B34A10E0F7B1B199B ();
// 0x000004F2 System.Void OVR.OpenVR.IVRSystem__ResetSeatedZeroPose::EndInvoke(System.IAsyncResult)
extern void _ResetSeatedZeroPose_EndInvoke_m2AB0D1ACB4553BA2AA2A09891887FD84EF849E3B ();
// 0x000004F3 System.Void OVR.OpenVR.IVRSystem__GetSeatedZeroPoseToStandingAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose__ctor_m25B0ACEC3E77AD0BE07BCA0A8DE2283C01C53B09 ();
// 0x000004F4 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetSeatedZeroPoseToStandingAbsoluteTrackingPose::Invoke()
extern void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_Invoke_m18421B43CF741AA4307CCE4A10500334B25A32F3 ();
// 0x000004F5 System.IAsyncResult OVR.OpenVR.IVRSystem__GetSeatedZeroPoseToStandingAbsoluteTrackingPose::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_mCC8F45CE9CC4AFFDE190E2082C5BDA5C9E07C6B0 ();
// 0x000004F6 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetSeatedZeroPoseToStandingAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m98D33B37944E544BCDB8E3A257940041CBCFA665 ();
// 0x000004F7 System.Void OVR.OpenVR.IVRSystem__GetRawZeroPoseToStandingAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetRawZeroPoseToStandingAbsoluteTrackingPose__ctor_mB44B73014E9C6FFA704196E755518177FB7CF930 ();
// 0x000004F8 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetRawZeroPoseToStandingAbsoluteTrackingPose::Invoke()
extern void _GetRawZeroPoseToStandingAbsoluteTrackingPose_Invoke_m00DF45579B54DB666667FB909297AA42545D2300 ();
// 0x000004F9 System.IAsyncResult OVR.OpenVR.IVRSystem__GetRawZeroPoseToStandingAbsoluteTrackingPose::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetRawZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_m76268B9590B3B8B4EB09A4D57952596BF932F77A ();
// 0x000004FA OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetRawZeroPoseToStandingAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern void _GetRawZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m5CDAD7675FFD4DA4AF3D1979A9A8FD46960BF650 ();
// 0x000004FB System.Void OVR.OpenVR.IVRSystem__GetSortedTrackedDeviceIndicesOfClass::.ctor(System.Object,System.IntPtr)
extern void _GetSortedTrackedDeviceIndicesOfClass__ctor_mEA22C4FA2929883F0A3DD4E172A48C81F9648529 ();
// 0x000004FC System.UInt32 OVR.OpenVR.IVRSystem__GetSortedTrackedDeviceIndicesOfClass::Invoke(OVR.OpenVR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32)
extern void _GetSortedTrackedDeviceIndicesOfClass_Invoke_m282452AF8C3ACAF7D857D9D70C87B539A9981B5D ();
// 0x000004FD System.IAsyncResult OVR.OpenVR.IVRSystem__GetSortedTrackedDeviceIndicesOfClass::BeginInvoke(OVR.OpenVR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m9B3815B69D955880891C1419F3DD52F609D9213B ();
// 0x000004FE System.UInt32 OVR.OpenVR.IVRSystem__GetSortedTrackedDeviceIndicesOfClass::EndInvoke(System.IAsyncResult)
extern void _GetSortedTrackedDeviceIndicesOfClass_EndInvoke_m39A4B28E578976E4CCB54E85C86E1C6E4F16E6D6 ();
// 0x000004FF System.Void OVR.OpenVR.IVRSystem__GetTrackedDeviceActivityLevel::.ctor(System.Object,System.IntPtr)
extern void _GetTrackedDeviceActivityLevel__ctor_mC3FE74E37BDF4576214131F0E97A597EB6CB5109 ();
// 0x00000500 OVR.OpenVR.EDeviceActivityLevel OVR.OpenVR.IVRSystem__GetTrackedDeviceActivityLevel::Invoke(System.UInt32)
extern void _GetTrackedDeviceActivityLevel_Invoke_mCFDC6C3339CC35B2C3C3E1EBB752B201C2390514 ();
// 0x00000501 System.IAsyncResult OVR.OpenVR.IVRSystem__GetTrackedDeviceActivityLevel::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _GetTrackedDeviceActivityLevel_BeginInvoke_mA805C95669C25BE6095EB0B55133938F5AFDE003 ();
// 0x00000502 OVR.OpenVR.EDeviceActivityLevel OVR.OpenVR.IVRSystem__GetTrackedDeviceActivityLevel::EndInvoke(System.IAsyncResult)
extern void _GetTrackedDeviceActivityLevel_EndInvoke_m12966AD011716BDF42AE02E612D8DC821B40938D ();
// 0x00000503 System.Void OVR.OpenVR.IVRSystem__ApplyTransform::.ctor(System.Object,System.IntPtr)
extern void _ApplyTransform__ctor_mADD92A81FC6283815A26C67DF847D2F24BBDB3B5 ();
// 0x00000504 System.Void OVR.OpenVR.IVRSystem__ApplyTransform::Invoke(OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.HmdMatrix34_t&)
extern void _ApplyTransform_Invoke_mF4F6CBFEDA0C2500DEA11F26DC7F6D0E96B3839A ();
// 0x00000505 System.IAsyncResult OVR.OpenVR.IVRSystem__ApplyTransform::BeginInvoke(OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _ApplyTransform_BeginInvoke_mFCE0158FD823EC509B2AC051634840CD7C022306 ();
// 0x00000506 System.Void OVR.OpenVR.IVRSystem__ApplyTransform::EndInvoke(OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _ApplyTransform_EndInvoke_m6D32F0BAF61C048DCFA6415A76AD767A3105E969 ();
// 0x00000507 System.Void OVR.OpenVR.IVRSystem__GetTrackedDeviceIndexForControllerRole::.ctor(System.Object,System.IntPtr)
extern void _GetTrackedDeviceIndexForControllerRole__ctor_m6F24270B375A336D496074DA4E2AA6F417F48CE8 ();
// 0x00000508 System.UInt32 OVR.OpenVR.IVRSystem__GetTrackedDeviceIndexForControllerRole::Invoke(OVR.OpenVR.ETrackedControllerRole)
extern void _GetTrackedDeviceIndexForControllerRole_Invoke_m156B8501ACB889DAD21A456D8E17DAC2D1F4893B ();
// 0x00000509 System.IAsyncResult OVR.OpenVR.IVRSystem__GetTrackedDeviceIndexForControllerRole::BeginInvoke(OVR.OpenVR.ETrackedControllerRole,System.AsyncCallback,System.Object)
extern void _GetTrackedDeviceIndexForControllerRole_BeginInvoke_m464EA42521FFD89D565E3E2B57C8F2E3DBB0F6A4 ();
// 0x0000050A System.UInt32 OVR.OpenVR.IVRSystem__GetTrackedDeviceIndexForControllerRole::EndInvoke(System.IAsyncResult)
extern void _GetTrackedDeviceIndexForControllerRole_EndInvoke_m2BE9038F4B7BE937DD358AACF63857856F861DEE ();
// 0x0000050B System.Void OVR.OpenVR.IVRSystem__GetControllerRoleForTrackedDeviceIndex::.ctor(System.Object,System.IntPtr)
extern void _GetControllerRoleForTrackedDeviceIndex__ctor_m967C016162CEA0E832C10A84CC985A74288A86CC ();
// 0x0000050C OVR.OpenVR.ETrackedControllerRole OVR.OpenVR.IVRSystem__GetControllerRoleForTrackedDeviceIndex::Invoke(System.UInt32)
extern void _GetControllerRoleForTrackedDeviceIndex_Invoke_m5D599FBFE92D9F97EDDE43077AB817697B041531 ();
// 0x0000050D System.IAsyncResult OVR.OpenVR.IVRSystem__GetControllerRoleForTrackedDeviceIndex::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _GetControllerRoleForTrackedDeviceIndex_BeginInvoke_mB87ABFB753D585BA6CDF3FF250EF571C597DC55D ();
// 0x0000050E OVR.OpenVR.ETrackedControllerRole OVR.OpenVR.IVRSystem__GetControllerRoleForTrackedDeviceIndex::EndInvoke(System.IAsyncResult)
extern void _GetControllerRoleForTrackedDeviceIndex_EndInvoke_m8521DF0BE7608845F3EADEC57B7F787FE787ECE0 ();
// 0x0000050F System.Void OVR.OpenVR.IVRSystem__GetTrackedDeviceClass::.ctor(System.Object,System.IntPtr)
extern void _GetTrackedDeviceClass__ctor_m8888331F1905AA6177F41775E369CD0161A4C8E3 ();
// 0x00000510 OVR.OpenVR.ETrackedDeviceClass OVR.OpenVR.IVRSystem__GetTrackedDeviceClass::Invoke(System.UInt32)
extern void _GetTrackedDeviceClass_Invoke_m28184E2A015BC02A6E24F0F3B3AABAD1A94A3D70 ();
// 0x00000511 System.IAsyncResult OVR.OpenVR.IVRSystem__GetTrackedDeviceClass::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _GetTrackedDeviceClass_BeginInvoke_mF342303B892529AD3E64A439696095892EA1EBD1 ();
// 0x00000512 OVR.OpenVR.ETrackedDeviceClass OVR.OpenVR.IVRSystem__GetTrackedDeviceClass::EndInvoke(System.IAsyncResult)
extern void _GetTrackedDeviceClass_EndInvoke_m4B4F45619BE77E4341C27265BE5F381D00980C16 ();
// 0x00000513 System.Void OVR.OpenVR.IVRSystem__IsTrackedDeviceConnected::.ctor(System.Object,System.IntPtr)
extern void _IsTrackedDeviceConnected__ctor_mD6F21BBBF860DB9ACEC53CEC1BAFA426070E61F1 ();
// 0x00000514 System.Boolean OVR.OpenVR.IVRSystem__IsTrackedDeviceConnected::Invoke(System.UInt32)
extern void _IsTrackedDeviceConnected_Invoke_m26D54165570B4B4EFB279313FE1DA187C523963D ();
// 0x00000515 System.IAsyncResult OVR.OpenVR.IVRSystem__IsTrackedDeviceConnected::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _IsTrackedDeviceConnected_BeginInvoke_m8CA8FA532BA54E813B1521AE69D8F8E908F43BDD ();
// 0x00000516 System.Boolean OVR.OpenVR.IVRSystem__IsTrackedDeviceConnected::EndInvoke(System.IAsyncResult)
extern void _IsTrackedDeviceConnected_EndInvoke_m30E8C70EE5AE7138CB86FE9C3BEE3581F561DA76 ();
// 0x00000517 System.Void OVR.OpenVR.IVRSystem__GetBoolTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetBoolTrackedDeviceProperty__ctor_mDC038C5A88EF2E0FA08EB3CA609188498F830B16 ();
// 0x00000518 System.Boolean OVR.OpenVR.IVRSystem__GetBoolTrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetBoolTrackedDeviceProperty_Invoke_m6A25E5ED8FC472633773D0686B3F12810432D56B ();
// 0x00000519 System.IAsyncResult OVR.OpenVR.IVRSystem__GetBoolTrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetBoolTrackedDeviceProperty_BeginInvoke_m73156F528974BF65CC43E6165497F045434CF16C ();
// 0x0000051A System.Boolean OVR.OpenVR.IVRSystem__GetBoolTrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetBoolTrackedDeviceProperty_EndInvoke_mDAF4E241FF202D8DD45E8ED7703DEE55F602DA9D ();
// 0x0000051B System.Void OVR.OpenVR.IVRSystem__GetFloatTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetFloatTrackedDeviceProperty__ctor_m41F0EB795EAE879862DF990AD14C4F4D5947E923 ();
// 0x0000051C System.Single OVR.OpenVR.IVRSystem__GetFloatTrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetFloatTrackedDeviceProperty_Invoke_m1038FC7B116E84AF493904A329EFD9195FB67E41 ();
// 0x0000051D System.IAsyncResult OVR.OpenVR.IVRSystem__GetFloatTrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetFloatTrackedDeviceProperty_BeginInvoke_m27B3C4A798E65045B0DE3EB275C24CF79E70D3F4 ();
// 0x0000051E System.Single OVR.OpenVR.IVRSystem__GetFloatTrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetFloatTrackedDeviceProperty_EndInvoke_mE4CB96ACF10A758985403BC9C78DFECE066D8CA3 ();
// 0x0000051F System.Void OVR.OpenVR.IVRSystem__GetInt32TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetInt32TrackedDeviceProperty__ctor_mB0AB1C88F72C999DE8A2928E450BE3197D75289F ();
// 0x00000520 System.Int32 OVR.OpenVR.IVRSystem__GetInt32TrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetInt32TrackedDeviceProperty_Invoke_m93E8C5E07459B0CAEE172BCEC73C80FC6145B527 ();
// 0x00000521 System.IAsyncResult OVR.OpenVR.IVRSystem__GetInt32TrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetInt32TrackedDeviceProperty_BeginInvoke_mDE2E82C433DA5641C5BE4E7241F52FD0847F8687 ();
// 0x00000522 System.Int32 OVR.OpenVR.IVRSystem__GetInt32TrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetInt32TrackedDeviceProperty_EndInvoke_m66052BF414C68497E58B1CF774FA9ABBC19F1B13 ();
// 0x00000523 System.Void OVR.OpenVR.IVRSystem__GetUint64TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetUint64TrackedDeviceProperty__ctor_mB51E27B099A0D202A0C0D9A8648A7738E75B6050 ();
// 0x00000524 System.UInt64 OVR.OpenVR.IVRSystem__GetUint64TrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetUint64TrackedDeviceProperty_Invoke_mD5D179224386F9C61D6A6F81843721854B0F188E ();
// 0x00000525 System.IAsyncResult OVR.OpenVR.IVRSystem__GetUint64TrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetUint64TrackedDeviceProperty_BeginInvoke_mF0ADCAEB5E6E1E555F05E4B9DF50788A2D146C46 ();
// 0x00000526 System.UInt64 OVR.OpenVR.IVRSystem__GetUint64TrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetUint64TrackedDeviceProperty_EndInvoke_m0638824FB82FC209EFB9D7EFBEDE8A82C288C0BD ();
// 0x00000527 System.Void OVR.OpenVR.IVRSystem__GetMatrix34TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetMatrix34TrackedDeviceProperty__ctor_mF24336475547C6A6977FEE63C8EFAF3810F57ADB ();
// 0x00000528 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetMatrix34TrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetMatrix34TrackedDeviceProperty_Invoke_m974853DC6B4CC1B4B459DCF54EF546681B82F71B ();
// 0x00000529 System.IAsyncResult OVR.OpenVR.IVRSystem__GetMatrix34TrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetMatrix34TrackedDeviceProperty_BeginInvoke_mF607A5FF0588005F0C732DA28573701C5075F188 ();
// 0x0000052A OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.IVRSystem__GetMatrix34TrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetMatrix34TrackedDeviceProperty_EndInvoke_m6ED9416567FD45ED13A6254B39A8CDB4088864F4 ();
// 0x0000052B System.Void OVR.OpenVR.IVRSystem__GetArrayTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetArrayTrackedDeviceProperty__ctor_m81DCCC37679C5CE0DCF135FBD5E0702790682D73 ();
// 0x0000052C System.UInt32 OVR.OpenVR.IVRSystem__GetArrayTrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.UInt32,System.IntPtr,System.UInt32,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetArrayTrackedDeviceProperty_Invoke_m75458BE4C33DE808389B2F5BBA3A33D36F1FDE17 ();
// 0x0000052D System.IAsyncResult OVR.OpenVR.IVRSystem__GetArrayTrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.UInt32,System.IntPtr,System.UInt32,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetArrayTrackedDeviceProperty_BeginInvoke_m4B3001A67E69DEF5674FCB93E1CD4F3697A397E0 ();
// 0x0000052E System.UInt32 OVR.OpenVR.IVRSystem__GetArrayTrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetArrayTrackedDeviceProperty_EndInvoke_mE42DA97A9EE3D9D7CC6256DF45B3B381F9C9C4CF ();
// 0x0000052F System.Void OVR.OpenVR.IVRSystem__GetStringTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern void _GetStringTrackedDeviceProperty__ctor_mA2FB4BECEE44739CF06D2D2FDD03B37C8FCAB057 ();
// 0x00000530 System.UInt32 OVR.OpenVR.IVRSystem__GetStringTrackedDeviceProperty::Invoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.ETrackedPropertyError&)
extern void _GetStringTrackedDeviceProperty_Invoke_mBFD477014E37F447C879A96A93DC4B5168737E25 ();
// 0x00000531 System.IAsyncResult OVR.OpenVR.IVRSystem__GetStringTrackedDeviceProperty::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern void _GetStringTrackedDeviceProperty_BeginInvoke_m858EAE0F9C2283CA471B94403957964467456C2B ();
// 0x00000532 System.UInt32 OVR.OpenVR.IVRSystem__GetStringTrackedDeviceProperty::EndInvoke(OVR.OpenVR.ETrackedPropertyError&,System.IAsyncResult)
extern void _GetStringTrackedDeviceProperty_EndInvoke_mC62B255F1DB010FDF7FDA566ED1E876152A50646 ();
// 0x00000533 System.Void OVR.OpenVR.IVRSystem__GetPropErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetPropErrorNameFromEnum__ctor_mA00109017FC8BA6CD2F72FD11A48B77F3B8B3606 ();
// 0x00000534 System.IntPtr OVR.OpenVR.IVRSystem__GetPropErrorNameFromEnum::Invoke(OVR.OpenVR.ETrackedPropertyError)
extern void _GetPropErrorNameFromEnum_Invoke_mC86C6745D35A20FF333C23A0BCFE5A79210D4000 ();
// 0x00000535 System.IAsyncResult OVR.OpenVR.IVRSystem__GetPropErrorNameFromEnum::BeginInvoke(OVR.OpenVR.ETrackedPropertyError,System.AsyncCallback,System.Object)
extern void _GetPropErrorNameFromEnum_BeginInvoke_mFB7C21E22CE8994A3B6576DF7F1BB6FA0ACE3E21 ();
// 0x00000536 System.IntPtr OVR.OpenVR.IVRSystem__GetPropErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetPropErrorNameFromEnum_EndInvoke_m398479E875F2A7DD2AA42C547D796ED7B4FF3A59 ();
// 0x00000537 System.Void OVR.OpenVR.IVRSystem__PollNextEvent::.ctor(System.Object,System.IntPtr)
extern void _PollNextEvent__ctor_m2397D373B9FBF5846446944A63787CC065F6B12A ();
// 0x00000538 System.Boolean OVR.OpenVR.IVRSystem__PollNextEvent::Invoke(OVR.OpenVR.VREvent_t&,System.UInt32)
extern void _PollNextEvent_Invoke_m1B25A00D5753A22B0C8284927E7D3A2B501EDCFC ();
// 0x00000539 System.IAsyncResult OVR.OpenVR.IVRSystem__PollNextEvent::BeginInvoke(OVR.OpenVR.VREvent_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _PollNextEvent_BeginInvoke_m997A35DE835E30343C39903EBFB7C40D6516EB6F ();
// 0x0000053A System.Boolean OVR.OpenVR.IVRSystem__PollNextEvent::EndInvoke(OVR.OpenVR.VREvent_t&,System.IAsyncResult)
extern void _PollNextEvent_EndInvoke_m9DB790D935A250E3C2124F15182724213C8743B0 ();
// 0x0000053B System.Void OVR.OpenVR.IVRSystem__PollNextEventWithPose::.ctor(System.Object,System.IntPtr)
extern void _PollNextEventWithPose__ctor_m6A8A769E84535D3FD80AC5B1A7B28A12E15C5B80 ();
// 0x0000053C System.Boolean OVR.OpenVR.IVRSystem__PollNextEventWithPose::Invoke(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.VREvent_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&)
extern void _PollNextEventWithPose_Invoke_m94B1066A0CD58CFBB216B145A8BB89802274A318 ();
// 0x0000053D System.IAsyncResult OVR.OpenVR.IVRSystem__PollNextEventWithPose::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.VREvent_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern void _PollNextEventWithPose_BeginInvoke_m4DF01BE0F475E01268AC5628735C686C51E95CA3 ();
// 0x0000053E System.Boolean OVR.OpenVR.IVRSystem__PollNextEventWithPose::EndInvoke(OVR.OpenVR.VREvent_t&,OVR.OpenVR.TrackedDevicePose_t&,System.IAsyncResult)
extern void _PollNextEventWithPose_EndInvoke_m356B54C51FDAEDAF2E7D31895D5B5CAAEB23EF49 ();
// 0x0000053F System.Void OVR.OpenVR.IVRSystem__GetEventTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetEventTypeNameFromEnum__ctor_m745FC5F1645FEEBC5B17A3FF13A98B7839D13990 ();
// 0x00000540 System.IntPtr OVR.OpenVR.IVRSystem__GetEventTypeNameFromEnum::Invoke(OVR.OpenVR.EVREventType)
extern void _GetEventTypeNameFromEnum_Invoke_m207B8A126CD48D518776050BB5BC7B1C60B1CAA7 ();
// 0x00000541 System.IAsyncResult OVR.OpenVR.IVRSystem__GetEventTypeNameFromEnum::BeginInvoke(OVR.OpenVR.EVREventType,System.AsyncCallback,System.Object)
extern void _GetEventTypeNameFromEnum_BeginInvoke_m42523BC4EC84F003DAAF90DE45E8011DF664201D ();
// 0x00000542 System.IntPtr OVR.OpenVR.IVRSystem__GetEventTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetEventTypeNameFromEnum_EndInvoke_mFD973317D0FA6E72A2647915CF2AC0B510F6BBDE ();
// 0x00000543 System.Void OVR.OpenVR.IVRSystem__GetHiddenAreaMesh::.ctor(System.Object,System.IntPtr)
extern void _GetHiddenAreaMesh__ctor_m0AE39A09956343234122F4F78F16A81BD31845EE ();
// 0x00000544 OVR.OpenVR.HiddenAreaMesh_t OVR.OpenVR.IVRSystem__GetHiddenAreaMesh::Invoke(OVR.OpenVR.EVREye,OVR.OpenVR.EHiddenAreaMeshType)
extern void _GetHiddenAreaMesh_Invoke_m4E9412F506DA0B66DE720B93652107CD1E8E4895 ();
// 0x00000545 System.IAsyncResult OVR.OpenVR.IVRSystem__GetHiddenAreaMesh::BeginInvoke(OVR.OpenVR.EVREye,OVR.OpenVR.EHiddenAreaMeshType,System.AsyncCallback,System.Object)
extern void _GetHiddenAreaMesh_BeginInvoke_m5E1ACE2060DDA7D210BF3A3FB941A8DD708BA5D3 ();
// 0x00000546 OVR.OpenVR.HiddenAreaMesh_t OVR.OpenVR.IVRSystem__GetHiddenAreaMesh::EndInvoke(System.IAsyncResult)
extern void _GetHiddenAreaMesh_EndInvoke_m92790FB7E3C4970D1604A9BC9E196B9AC00483C5 ();
// 0x00000547 System.Void OVR.OpenVR.IVRSystem__GetControllerState::.ctor(System.Object,System.IntPtr)
extern void _GetControllerState__ctor_m731F594692CFC09E788221719B8BD91766BD410A ();
// 0x00000548 System.Boolean OVR.OpenVR.IVRSystem__GetControllerState::Invoke(System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32)
extern void _GetControllerState_Invoke_m3FD96B76D65ACEC1AE4A942FB150B8CC6938251B ();
// 0x00000549 System.IAsyncResult OVR.OpenVR.IVRSystem__GetControllerState::BeginInvoke(System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetControllerState_BeginInvoke_m04ED3F050AAD4F896A80337E4E4A59C87C071AA0 ();
// 0x0000054A System.Boolean OVR.OpenVR.IVRSystem__GetControllerState::EndInvoke(OVR.OpenVR.VRControllerState_t&,System.IAsyncResult)
extern void _GetControllerState_EndInvoke_mEEA4F9D5A75E203FA899102878F39CB189C2FD87 ();
// 0x0000054B System.Void OVR.OpenVR.IVRSystem__GetControllerStateWithPose::.ctor(System.Object,System.IntPtr)
extern void _GetControllerStateWithPose__ctor_mEA49D50F28906B0D5D3433BACDF8D267C8514B9D ();
// 0x0000054C System.Boolean OVR.OpenVR.IVRSystem__GetControllerStateWithPose::Invoke(OVR.OpenVR.ETrackingUniverseOrigin,System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&)
extern void _GetControllerStateWithPose_Invoke_m758E5C7BC3D849CEA2630AD503D1B206B649C867 ();
// 0x0000054D System.IAsyncResult OVR.OpenVR.IVRSystem__GetControllerStateWithPose::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern void _GetControllerStateWithPose_BeginInvoke_m73FA675C7FC334D848D9128E01917E5396C07234 ();
// 0x0000054E System.Boolean OVR.OpenVR.IVRSystem__GetControllerStateWithPose::EndInvoke(OVR.OpenVR.VRControllerState_t&,OVR.OpenVR.TrackedDevicePose_t&,System.IAsyncResult)
extern void _GetControllerStateWithPose_EndInvoke_m88A6DB66944DF63DA708611E2DF6CD90A21E9974 ();
// 0x0000054F System.Void OVR.OpenVR.IVRSystem__TriggerHapticPulse::.ctor(System.Object,System.IntPtr)
extern void _TriggerHapticPulse__ctor_mDB39757A7BE980375D317AB5DF4F8B1F8D4C082E ();
// 0x00000550 System.Void OVR.OpenVR.IVRSystem__TriggerHapticPulse::Invoke(System.UInt32,System.UInt32,System.UInt16)
extern void _TriggerHapticPulse_Invoke_mA1211B3664A3E9D0199437EF083A541E331B80B0 ();
// 0x00000551 System.IAsyncResult OVR.OpenVR.IVRSystem__TriggerHapticPulse::BeginInvoke(System.UInt32,System.UInt32,System.UInt16,System.AsyncCallback,System.Object)
extern void _TriggerHapticPulse_BeginInvoke_m71D85A1795335B014450AD9F07719E6A16581A8B ();
// 0x00000552 System.Void OVR.OpenVR.IVRSystem__TriggerHapticPulse::EndInvoke(System.IAsyncResult)
extern void _TriggerHapticPulse_EndInvoke_m031C07A5848D4AF56A5D9FBC3694EC5801CBBB97 ();
// 0x00000553 System.Void OVR.OpenVR.IVRSystem__GetButtonIdNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetButtonIdNameFromEnum__ctor_m1D66A06D90C09A14CAC5749B6D1EB609712FC1B3 ();
// 0x00000554 System.IntPtr OVR.OpenVR.IVRSystem__GetButtonIdNameFromEnum::Invoke(OVR.OpenVR.EVRButtonId)
extern void _GetButtonIdNameFromEnum_Invoke_m6FE5FBC65BAB5A13F60563D23E08835E58D51F89 ();
// 0x00000555 System.IAsyncResult OVR.OpenVR.IVRSystem__GetButtonIdNameFromEnum::BeginInvoke(OVR.OpenVR.EVRButtonId,System.AsyncCallback,System.Object)
extern void _GetButtonIdNameFromEnum_BeginInvoke_m192A3422C5F4D0F07A16E9420083B33431AD8C5C ();
// 0x00000556 System.IntPtr OVR.OpenVR.IVRSystem__GetButtonIdNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetButtonIdNameFromEnum_EndInvoke_mCD0E939B94D68F4E5BE2A6AEF9777DD5B9150A7E ();
// 0x00000557 System.Void OVR.OpenVR.IVRSystem__GetControllerAxisTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetControllerAxisTypeNameFromEnum__ctor_m72814256E68D1BB3114B68344DEC81ECF1CCB345 ();
// 0x00000558 System.IntPtr OVR.OpenVR.IVRSystem__GetControllerAxisTypeNameFromEnum::Invoke(OVR.OpenVR.EVRControllerAxisType)
extern void _GetControllerAxisTypeNameFromEnum_Invoke_mF3FD75ADB3D72667BFFB09F1EAC69D28FFA24E91 ();
// 0x00000559 System.IAsyncResult OVR.OpenVR.IVRSystem__GetControllerAxisTypeNameFromEnum::BeginInvoke(OVR.OpenVR.EVRControllerAxisType,System.AsyncCallback,System.Object)
extern void _GetControllerAxisTypeNameFromEnum_BeginInvoke_m8383269668B959504E9653A62F078232B5BBFA61 ();
// 0x0000055A System.IntPtr OVR.OpenVR.IVRSystem__GetControllerAxisTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetControllerAxisTypeNameFromEnum_EndInvoke_m7EBF94DAC73D9184C95D6063176138AE330E4333 ();
// 0x0000055B System.Void OVR.OpenVR.IVRSystem__IsInputAvailable::.ctor(System.Object,System.IntPtr)
extern void _IsInputAvailable__ctor_m64137A01B1D7DA02538432D7C49BAB9DC7D47DEC ();
// 0x0000055C System.Boolean OVR.OpenVR.IVRSystem__IsInputAvailable::Invoke()
extern void _IsInputAvailable_Invoke_m990134A13B897E67D1081143E8A5918675104465 ();
// 0x0000055D System.IAsyncResult OVR.OpenVR.IVRSystem__IsInputAvailable::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsInputAvailable_BeginInvoke_mCDA1D9E22BE33B68303AAC499C437A0980D19328 ();
// 0x0000055E System.Boolean OVR.OpenVR.IVRSystem__IsInputAvailable::EndInvoke(System.IAsyncResult)
extern void _IsInputAvailable_EndInvoke_m9884990B525012A7265C45052C7BB5031DB13FC9 ();
// 0x0000055F System.Void OVR.OpenVR.IVRSystem__IsSteamVRDrawingControllers::.ctor(System.Object,System.IntPtr)
extern void _IsSteamVRDrawingControllers__ctor_mAC9881A1E200A4810DD7E228F656C9EB64C410A0 ();
// 0x00000560 System.Boolean OVR.OpenVR.IVRSystem__IsSteamVRDrawingControllers::Invoke()
extern void _IsSteamVRDrawingControllers_Invoke_mC5EB3F5CBCC68CE56E53C38929BD3AB03F69A516 ();
// 0x00000561 System.IAsyncResult OVR.OpenVR.IVRSystem__IsSteamVRDrawingControllers::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsSteamVRDrawingControllers_BeginInvoke_mE1DA65D68B9B29EF5BC00A426E1D9DB151F82D71 ();
// 0x00000562 System.Boolean OVR.OpenVR.IVRSystem__IsSteamVRDrawingControllers::EndInvoke(System.IAsyncResult)
extern void _IsSteamVRDrawingControllers_EndInvoke_mB573416ED037D325FF9C85A83BBF28372D47ECB6 ();
// 0x00000563 System.Void OVR.OpenVR.IVRSystem__ShouldApplicationPause::.ctor(System.Object,System.IntPtr)
extern void _ShouldApplicationPause__ctor_mC9B1F6653A94973AC35ADF42D1929B0140BD1DC8 ();
// 0x00000564 System.Boolean OVR.OpenVR.IVRSystem__ShouldApplicationPause::Invoke()
extern void _ShouldApplicationPause_Invoke_mE58AB9BD5DC7151B695CF0FEE404BA45FAA05C18 ();
// 0x00000565 System.IAsyncResult OVR.OpenVR.IVRSystem__ShouldApplicationPause::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ShouldApplicationPause_BeginInvoke_m267EC96EEBB5FB8B03D17B1C6CAE77B70A31E5BA ();
// 0x00000566 System.Boolean OVR.OpenVR.IVRSystem__ShouldApplicationPause::EndInvoke(System.IAsyncResult)
extern void _ShouldApplicationPause_EndInvoke_m78615A7F670D1A4320ADF101C766A77CA1D1161F ();
// 0x00000567 System.Void OVR.OpenVR.IVRSystem__ShouldApplicationReduceRenderingWork::.ctor(System.Object,System.IntPtr)
extern void _ShouldApplicationReduceRenderingWork__ctor_mEF79CAF1EA3DEA34B8719CF36B3FF6C0812BD990 ();
// 0x00000568 System.Boolean OVR.OpenVR.IVRSystem__ShouldApplicationReduceRenderingWork::Invoke()
extern void _ShouldApplicationReduceRenderingWork_Invoke_mE636FF9AB4E27825A21A65B766E0885662CB49B2 ();
// 0x00000569 System.IAsyncResult OVR.OpenVR.IVRSystem__ShouldApplicationReduceRenderingWork::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ShouldApplicationReduceRenderingWork_BeginInvoke_mBECE74EE8016DCA13D881D6D9774FEC91F49A23D ();
// 0x0000056A System.Boolean OVR.OpenVR.IVRSystem__ShouldApplicationReduceRenderingWork::EndInvoke(System.IAsyncResult)
extern void _ShouldApplicationReduceRenderingWork_EndInvoke_m737F6C0FF7AF403414A0C2895E623B095DFE7166 ();
// 0x0000056B System.Void OVR.OpenVR.IVRSystem__DriverDebugRequest::.ctor(System.Object,System.IntPtr)
extern void _DriverDebugRequest__ctor_m03B3ECBF560986A0B8940EF04D877108D0F36AED ();
// 0x0000056C System.UInt32 OVR.OpenVR.IVRSystem__DriverDebugRequest::Invoke(System.UInt32,System.String,System.Text.StringBuilder,System.UInt32)
extern void _DriverDebugRequest_Invoke_m2BDD6EE30E72BDB2F5B775AE96C3806C69D2869C ();
// 0x0000056D System.IAsyncResult OVR.OpenVR.IVRSystem__DriverDebugRequest::BeginInvoke(System.UInt32,System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _DriverDebugRequest_BeginInvoke_m2269CACD17652F6BC476154643F59A8389AE5875 ();
// 0x0000056E System.UInt32 OVR.OpenVR.IVRSystem__DriverDebugRequest::EndInvoke(System.IAsyncResult)
extern void _DriverDebugRequest_EndInvoke_mBE3B71E210B0BF99679BF38CC535BB5E2AD12C67 ();
// 0x0000056F System.Void OVR.OpenVR.IVRSystem__PerformFirmwareUpdate::.ctor(System.Object,System.IntPtr)
extern void _PerformFirmwareUpdate__ctor_m1EEC2AC94CFB01B693FDBB5C5F173F1341FA0C94 ();
// 0x00000570 OVR.OpenVR.EVRFirmwareError OVR.OpenVR.IVRSystem__PerformFirmwareUpdate::Invoke(System.UInt32)
extern void _PerformFirmwareUpdate_Invoke_m35993BB13E5F0BCA1B5FC833E8BCAC0B7D9F3860 ();
// 0x00000571 System.IAsyncResult OVR.OpenVR.IVRSystem__PerformFirmwareUpdate::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _PerformFirmwareUpdate_BeginInvoke_m8D11CAFC6F31ADC3A329171D5A67514AE6F18827 ();
// 0x00000572 OVR.OpenVR.EVRFirmwareError OVR.OpenVR.IVRSystem__PerformFirmwareUpdate::EndInvoke(System.IAsyncResult)
extern void _PerformFirmwareUpdate_EndInvoke_m5165FE224F57CC6CC28B502FB5A2E76FAF662FA5 ();
// 0x00000573 System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_Exiting::.ctor(System.Object,System.IntPtr)
extern void _AcknowledgeQuit_Exiting__ctor_mC3B9AA28269BB817DD668521941FBA90CAAEAAC9 ();
// 0x00000574 System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_Exiting::Invoke()
extern void _AcknowledgeQuit_Exiting_Invoke_m199F3555B34ECBA57C72482D4CDDBF7CE9307B64 ();
// 0x00000575 System.IAsyncResult OVR.OpenVR.IVRSystem__AcknowledgeQuit_Exiting::BeginInvoke(System.AsyncCallback,System.Object)
extern void _AcknowledgeQuit_Exiting_BeginInvoke_mC6C6A1D7B3FB266FF9EE88F65292C3B54EC4ADD0 ();
// 0x00000576 System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_Exiting::EndInvoke(System.IAsyncResult)
extern void _AcknowledgeQuit_Exiting_EndInvoke_m52919D072EEF28D5D1D4012B3C812AFFB2D6801A ();
// 0x00000577 System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_UserPrompt::.ctor(System.Object,System.IntPtr)
extern void _AcknowledgeQuit_UserPrompt__ctor_mE72287842207321FEF9961B0836BDFB704E93D40 ();
// 0x00000578 System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_UserPrompt::Invoke()
extern void _AcknowledgeQuit_UserPrompt_Invoke_m0D3B39C448AF24FE6929884E04DE7D52680A8AA0 ();
// 0x00000579 System.IAsyncResult OVR.OpenVR.IVRSystem__AcknowledgeQuit_UserPrompt::BeginInvoke(System.AsyncCallback,System.Object)
extern void _AcknowledgeQuit_UserPrompt_BeginInvoke_m5C4E545573A355662423FEB9CB157BF806677495 ();
// 0x0000057A System.Void OVR.OpenVR.IVRSystem__AcknowledgeQuit_UserPrompt::EndInvoke(System.IAsyncResult)
extern void _AcknowledgeQuit_UserPrompt_EndInvoke_mDBD6F5077DEC760A46863565D081BC76AACDCC14 ();
// 0x0000057B System.Void OVR.OpenVR.IVRExtendedDisplay__GetWindowBounds::.ctor(System.Object,System.IntPtr)
extern void _GetWindowBounds__ctor_m84A84C118688E0FADF259A49D71F1520AAB21B9C ();
// 0x0000057C System.Void OVR.OpenVR.IVRExtendedDisplay__GetWindowBounds::Invoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&)
extern void _GetWindowBounds_Invoke_m4FDF7A8F5333FAB04653EE8BC7A124DA3464D4A4 ();
// 0x0000057D System.IAsyncResult OVR.OpenVR.IVRExtendedDisplay__GetWindowBounds::BeginInvoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetWindowBounds_BeginInvoke_m27D9DE28EDD58A780BB7DEE761F83CF564ABC2C8 ();
// 0x0000057E System.Void OVR.OpenVR.IVRExtendedDisplay__GetWindowBounds::EndInvoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetWindowBounds_EndInvoke_mEAE7D1BF066943FB2CA6F3DDE36310FDA051C343 ();
// 0x0000057F System.Void OVR.OpenVR.IVRExtendedDisplay__GetEyeOutputViewport::.ctor(System.Object,System.IntPtr)
extern void _GetEyeOutputViewport__ctor_m51C41E49E7AC9075054B23CE43D4C67900A43A36 ();
// 0x00000580 System.Void OVR.OpenVR.IVRExtendedDisplay__GetEyeOutputViewport::Invoke(OVR.OpenVR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&)
extern void _GetEyeOutputViewport_Invoke_mA6EE89C086079408374F4131DCCBA9760C079D46 ();
// 0x00000581 System.IAsyncResult OVR.OpenVR.IVRExtendedDisplay__GetEyeOutputViewport::BeginInvoke(OVR.OpenVR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetEyeOutputViewport_BeginInvoke_m5B40E60B41DB4EDAF27A9FEA4E44AE945536F40E ();
// 0x00000582 System.Void OVR.OpenVR.IVRExtendedDisplay__GetEyeOutputViewport::EndInvoke(System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetEyeOutputViewport_EndInvoke_mA34BD3E1959CACF24293E5B0C7BE5C4F52B7DB5F ();
// 0x00000583 System.Void OVR.OpenVR.IVRExtendedDisplay__GetDXGIOutputInfo::.ctor(System.Object,System.IntPtr)
extern void _GetDXGIOutputInfo__ctor_m2E636E44B9E5B4BCD7E84C79A4EA5FFD579A42A5 ();
// 0x00000584 System.Void OVR.OpenVR.IVRExtendedDisplay__GetDXGIOutputInfo::Invoke(System.Int32&,System.Int32&)
extern void _GetDXGIOutputInfo_Invoke_m3F01FEE3323612D4B99A29C56EA4C0750ADBAEDF ();
// 0x00000585 System.IAsyncResult OVR.OpenVR.IVRExtendedDisplay__GetDXGIOutputInfo::BeginInvoke(System.Int32&,System.Int32&,System.AsyncCallback,System.Object)
extern void _GetDXGIOutputInfo_BeginInvoke_m39D191F97E3736D8B80C8158B58E2144D4C2FBA8 ();
// 0x00000586 System.Void OVR.OpenVR.IVRExtendedDisplay__GetDXGIOutputInfo::EndInvoke(System.Int32&,System.Int32&,System.IAsyncResult)
extern void _GetDXGIOutputInfo_EndInvoke_m266CC3CB84AB5CA9380ADDE29AD8014B8E167CD2 ();
// 0x00000587 System.Void OVR.OpenVR.IVRTrackedCamera__GetCameraErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetCameraErrorNameFromEnum__ctor_m11AC1A5FC182150DFD4B993DCA2D64DA0A7193DE ();
// 0x00000588 System.IntPtr OVR.OpenVR.IVRTrackedCamera__GetCameraErrorNameFromEnum::Invoke(OVR.OpenVR.EVRTrackedCameraError)
extern void _GetCameraErrorNameFromEnum_Invoke_m8DE93F93C81562B232D92B48510B3637A6A24F00 ();
// 0x00000589 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetCameraErrorNameFromEnum::BeginInvoke(OVR.OpenVR.EVRTrackedCameraError,System.AsyncCallback,System.Object)
extern void _GetCameraErrorNameFromEnum_BeginInvoke_m5248723D07AAADAC4873A901FE6FF34B9093EFDD ();
// 0x0000058A System.IntPtr OVR.OpenVR.IVRTrackedCamera__GetCameraErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetCameraErrorNameFromEnum_EndInvoke_mF2B4273B3E88FC7C05DB183ED1C0709CED3CA24A ();
// 0x0000058B System.Void OVR.OpenVR.IVRTrackedCamera__HasCamera::.ctor(System.Object,System.IntPtr)
extern void _HasCamera__ctor_mAA9FC08FA538E9C15599723C8BE751065EC778F5 ();
// 0x0000058C OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__HasCamera::Invoke(System.UInt32,System.Boolean&)
extern void _HasCamera_Invoke_m23E7FD9A3F3DA5042BFB3053BBC12FC374A4E223 ();
// 0x0000058D System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__HasCamera::BeginInvoke(System.UInt32,System.Boolean&,System.AsyncCallback,System.Object)
extern void _HasCamera_BeginInvoke_m088514BCA1F2F9D3AF8F71FCF37776DCF3C79FF5 ();
// 0x0000058E OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__HasCamera::EndInvoke(System.Boolean&,System.IAsyncResult)
extern void _HasCamera_EndInvoke_m8D802B1F4F41CBC74173B978FA1DC9B39D1A939F ();
// 0x0000058F System.Void OVR.OpenVR.IVRTrackedCamera__GetCameraFrameSize::.ctor(System.Object,System.IntPtr)
extern void _GetCameraFrameSize__ctor_m98BE332A8C036D724D40FC0D4809A86EF1905E35 ();
// 0x00000590 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraFrameSize::Invoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&)
extern void _GetCameraFrameSize_Invoke_m4217775D0404A2061940FB7BEE66571D20A6AAC4 ();
// 0x00000591 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetCameraFrameSize::BeginInvoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetCameraFrameSize_BeginInvoke_m1284B0CAC9ADC6D08E0A0711163DD7550199CAC4 ();
// 0x00000592 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraFrameSize::EndInvoke(System.UInt32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetCameraFrameSize_EndInvoke_mE6F0DC41FDD6668B966B23303BAF2D4141717BF8 ();
// 0x00000593 System.Void OVR.OpenVR.IVRTrackedCamera__GetCameraIntrinsics::.ctor(System.Object,System.IntPtr)
extern void _GetCameraIntrinsics__ctor_mAA5C457CF295CB6266D4F2A41525C16CF512869A ();
// 0x00000594 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraIntrinsics::Invoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.HmdVector2_t&,OVR.OpenVR.HmdVector2_t&)
extern void _GetCameraIntrinsics_Invoke_m3212819D3D482C91B14D78C42CDC530CAB622D16 ();
// 0x00000595 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetCameraIntrinsics::BeginInvoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.HmdVector2_t&,OVR.OpenVR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern void _GetCameraIntrinsics_BeginInvoke_mE93A022B28E943ED7353F2BA728D6B196097E218 ();
// 0x00000596 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraIntrinsics::EndInvoke(OVR.OpenVR.HmdVector2_t&,OVR.OpenVR.HmdVector2_t&,System.IAsyncResult)
extern void _GetCameraIntrinsics_EndInvoke_m1E43DC49983266739251B998051164F53986E914 ();
// 0x00000597 System.Void OVR.OpenVR.IVRTrackedCamera__GetCameraProjection::.ctor(System.Object,System.IntPtr)
extern void _GetCameraProjection__ctor_m8B30FA056DF6013EFF82E1408305F0D8954151BF ();
// 0x00000598 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraProjection::Invoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.Single,System.Single,OVR.OpenVR.HmdMatrix44_t&)
extern void _GetCameraProjection_Invoke_m987371F8352115CEBA3AE89431A92AA83791EFBD ();
// 0x00000599 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetCameraProjection::BeginInvoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.Single,System.Single,OVR.OpenVR.HmdMatrix44_t&,System.AsyncCallback,System.Object)
extern void _GetCameraProjection_BeginInvoke_m13664CB49E96AA3F3524C9FB9E2B9745C9BD68F5 ();
// 0x0000059A OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetCameraProjection::EndInvoke(OVR.OpenVR.HmdMatrix44_t&,System.IAsyncResult)
extern void _GetCameraProjection_EndInvoke_mE6773A04853266233DC5A6BDDFBA061F955BE739 ();
// 0x0000059B System.Void OVR.OpenVR.IVRTrackedCamera__AcquireVideoStreamingService::.ctor(System.Object,System.IntPtr)
extern void _AcquireVideoStreamingService__ctor_m0D16A197EF31A60FA765FB59F4B787E7E455D34E ();
// 0x0000059C OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__AcquireVideoStreamingService::Invoke(System.UInt32,System.UInt64&)
extern void _AcquireVideoStreamingService_Invoke_mA4459230482C70D4A1B7D2618780A9921C59CD32 ();
// 0x0000059D System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__AcquireVideoStreamingService::BeginInvoke(System.UInt32,System.UInt64&,System.AsyncCallback,System.Object)
extern void _AcquireVideoStreamingService_BeginInvoke_mCBB966D69C809982AB10422578740CE8E2A3825B ();
// 0x0000059E OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__AcquireVideoStreamingService::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _AcquireVideoStreamingService_EndInvoke_mF20738ED5754B1F464041E82391D9FA976778E22 ();
// 0x0000059F System.Void OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamingService::.ctor(System.Object,System.IntPtr)
extern void _ReleaseVideoStreamingService__ctor_mBE089FEC1D42C0F533B22BD2D6107E4DD7FFC35F ();
// 0x000005A0 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamingService::Invoke(System.UInt64)
extern void _ReleaseVideoStreamingService_Invoke_mD13D2E8E617D0CCC50215D05B181D6D37AB6A8CD ();
// 0x000005A1 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamingService::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _ReleaseVideoStreamingService_BeginInvoke_mE3574B9CC7EC9D66C5FB9C2CB3347005350EA2B5 ();
// 0x000005A2 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamingService::EndInvoke(System.IAsyncResult)
extern void _ReleaseVideoStreamingService_EndInvoke_m72FA88C655CE731302200B84522716A9D84D3D91 ();
// 0x000005A3 System.Void OVR.OpenVR.IVRTrackedCamera__GetVideoStreamFrameBuffer::.ctor(System.Object,System.IntPtr)
extern void _GetVideoStreamFrameBuffer__ctor_m552209C64C72360044B58B37BC3F85198255AA5F ();
// 0x000005A4 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamFrameBuffer::Invoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void _GetVideoStreamFrameBuffer_Invoke_m42A9E3CD5D53998CBEC5675336ECBD1D7FD09BF6 ();
// 0x000005A5 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetVideoStreamFrameBuffer::BeginInvoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetVideoStreamFrameBuffer_BeginInvoke_mFCC631BC5BC71107F54736CF18A068849C321E53 ();
// 0x000005A6 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamFrameBuffer::EndInvoke(OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern void _GetVideoStreamFrameBuffer_EndInvoke_mCC1FD14C6CCC6F3B1A98A7E9E710B562732B21EF ();
// 0x000005A7 System.Void OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureSize::.ctor(System.Object,System.IntPtr)
extern void _GetVideoStreamTextureSize__ctor_m7820265ED74A5914FED52F49B0E6C61C95D4F2E3 ();
// 0x000005A8 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureSize::Invoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.VRTextureBounds_t&,System.UInt32&,System.UInt32&)
extern void _GetVideoStreamTextureSize_Invoke_mFC62C310DDCDE780FA54F04ED77C6C0BB4AE10CC ();
// 0x000005A9 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureSize::BeginInvoke(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.VRTextureBounds_t&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetVideoStreamTextureSize_BeginInvoke_m2280FCC5D5381BBA423BCB5F5F92908EAF026F27 ();
// 0x000005AA OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureSize::EndInvoke(OVR.OpenVR.VRTextureBounds_t&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetVideoStreamTextureSize_EndInvoke_m9B18F3525D9F5D311934C8991A9A8792E748C260 ();
// 0x000005AB System.Void OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureD3D11::.ctor(System.Object,System.IntPtr)
extern void _GetVideoStreamTextureD3D11__ctor_mD57BF604FEBB1760568B0C3E389462D1723E220B ();
// 0x000005AC OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureD3D11::Invoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void _GetVideoStreamTextureD3D11_Invoke_m917B49E8920A2A2BDE080CE5416478BBADF95A17 ();
// 0x000005AD System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureD3D11::BeginInvoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetVideoStreamTextureD3D11_BeginInvoke_mB275B85726824F8A9FE70A203ADB2CEC27CB9DB6 ();
// 0x000005AE OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureD3D11::EndInvoke(System.IntPtr&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern void _GetVideoStreamTextureD3D11_EndInvoke_mBF501FA2B9448ACFE4D32C2DD0379824E4605CA0 ();
// 0x000005AF System.Void OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureGL::.ctor(System.Object,System.IntPtr)
extern void _GetVideoStreamTextureGL__ctor_m4AB7BEED12932CA5AC510B59091BB5561BE3958A ();
// 0x000005B0 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureGL::Invoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void _GetVideoStreamTextureGL_Invoke_m2D30DA93AC78DDC2BFAD44E4B10204A3604141A9 ();
// 0x000005B1 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureGL::BeginInvoke(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetVideoStreamTextureGL_BeginInvoke_m15DC6ADCCD04B88E0FAEAA9234AE56B3326AE39F ();
// 0x000005B2 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__GetVideoStreamTextureGL::EndInvoke(System.UInt32&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern void _GetVideoStreamTextureGL_EndInvoke_m83998DC041B56D2AF94A77FACFC55AC526BBB508 ();
// 0x000005B3 System.Void OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamTextureGL::.ctor(System.Object,System.IntPtr)
extern void _ReleaseVideoStreamTextureGL__ctor_m3FC2F2B66DBB65A8990892BD1DB2E3DA21AF58D9 ();
// 0x000005B4 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamTextureGL::Invoke(System.UInt64,System.UInt32)
extern void _ReleaseVideoStreamTextureGL_Invoke_m758C0778DA8841113203E0828A7A7B8121057EA6 ();
// 0x000005B5 System.IAsyncResult OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamTextureGL::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern void _ReleaseVideoStreamTextureGL_BeginInvoke_m3C6C27B1F58C80F64166FB7CAE874DDE5B373B00 ();
// 0x000005B6 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.IVRTrackedCamera__ReleaseVideoStreamTextureGL::EndInvoke(System.IAsyncResult)
extern void _ReleaseVideoStreamTextureGL_EndInvoke_m8EB0A5DCA65B55FF6ED0FBF645D2090802E1F92F ();
// 0x000005B7 System.Void OVR.OpenVR.IVRApplications__AddApplicationManifest::.ctor(System.Object,System.IntPtr)
extern void _AddApplicationManifest__ctor_m27FF628AEA2942208F59C98ECA1811F501858388 ();
// 0x000005B8 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__AddApplicationManifest::Invoke(System.String,System.Boolean)
extern void _AddApplicationManifest_Invoke_m7C1939B2FADE571F8E0FBD20ACE3CEF70C8D965F ();
// 0x000005B9 System.IAsyncResult OVR.OpenVR.IVRApplications__AddApplicationManifest::BeginInvoke(System.String,System.Boolean,System.AsyncCallback,System.Object)
extern void _AddApplicationManifest_BeginInvoke_m87A69C590E96CCBC5EB51657421FB2B8EA5A48F9 ();
// 0x000005BA OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__AddApplicationManifest::EndInvoke(System.IAsyncResult)
extern void _AddApplicationManifest_EndInvoke_mC720F64DCD3924DC869294858F6473CF0FECA33A ();
// 0x000005BB System.Void OVR.OpenVR.IVRApplications__RemoveApplicationManifest::.ctor(System.Object,System.IntPtr)
extern void _RemoveApplicationManifest__ctor_m0D7E15DD9CAE8C2BD005AE77DE47D69FD497BAEF ();
// 0x000005BC OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__RemoveApplicationManifest::Invoke(System.String)
extern void _RemoveApplicationManifest_Invoke_m853A9EDED90D74C95D995E07EAC7FCD33EF4ADA3 ();
// 0x000005BD System.IAsyncResult OVR.OpenVR.IVRApplications__RemoveApplicationManifest::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _RemoveApplicationManifest_BeginInvoke_mD4578124ECB56E74A2F7285FFC7B792763AAA50C ();
// 0x000005BE OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__RemoveApplicationManifest::EndInvoke(System.IAsyncResult)
extern void _RemoveApplicationManifest_EndInvoke_mD338A6F5B4E5F880F266A7338E63A2648C4D1AA4 ();
// 0x000005BF System.Void OVR.OpenVR.IVRApplications__IsApplicationInstalled::.ctor(System.Object,System.IntPtr)
extern void _IsApplicationInstalled__ctor_m268B59BDD2C1D158F263FB2A19951AF2ACB11F99 ();
// 0x000005C0 System.Boolean OVR.OpenVR.IVRApplications__IsApplicationInstalled::Invoke(System.String)
extern void _IsApplicationInstalled_Invoke_mFF45BBFC8F42378FA577597F8A45283D637C283D ();
// 0x000005C1 System.IAsyncResult OVR.OpenVR.IVRApplications__IsApplicationInstalled::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _IsApplicationInstalled_BeginInvoke_m1E0E72BB0EC53EAF24FB420FA23ED49075729316 ();
// 0x000005C2 System.Boolean OVR.OpenVR.IVRApplications__IsApplicationInstalled::EndInvoke(System.IAsyncResult)
extern void _IsApplicationInstalled_EndInvoke_mB2E436D53A9222604198B0F71856B986946183FA ();
// 0x000005C3 System.Void OVR.OpenVR.IVRApplications__GetApplicationCount::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationCount__ctor_m6084F985C214A72FD48EA87FDEC627E03DBCF769 ();
// 0x000005C4 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationCount::Invoke()
extern void _GetApplicationCount_Invoke_m6A9A35DDC9D52F0515F07DACFE812AE13559EF26 ();
// 0x000005C5 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationCount::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetApplicationCount_BeginInvoke_m8FE37BD58A454BFEEE0B01BAD50553746C132999 ();
// 0x000005C6 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationCount::EndInvoke(System.IAsyncResult)
extern void _GetApplicationCount_EndInvoke_m12F7DE8D11FD983D21D8E3EEBFBB28FE18F4BE7A ();
// 0x000005C7 System.Void OVR.OpenVR.IVRApplications__GetApplicationKeyByIndex::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationKeyByIndex__ctor_m43D4EB76A280A3206BF5E9DFA5EE7478CE754397 ();
// 0x000005C8 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetApplicationKeyByIndex::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetApplicationKeyByIndex_Invoke_m7154C9183404739D542D58112FAA7F88F1687645 ();
// 0x000005C9 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationKeyByIndex::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetApplicationKeyByIndex_BeginInvoke_m29E447545F4731A16287AD58F48061AB223263B8 ();
// 0x000005CA OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetApplicationKeyByIndex::EndInvoke(System.IAsyncResult)
extern void _GetApplicationKeyByIndex_EndInvoke_m962D174A64B203B7531D2F96841C7FD038FB70CE ();
// 0x000005CB System.Void OVR.OpenVR.IVRApplications__GetApplicationKeyByProcessId::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationKeyByProcessId__ctor_mF8CA2A55E3A41C2EBA2AD14A2004CD8466FAE995 ();
// 0x000005CC OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetApplicationKeyByProcessId::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetApplicationKeyByProcessId_Invoke_m58700077CDD1AD4BEFDFD9EE84D90F26C6714349 ();
// 0x000005CD System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationKeyByProcessId::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetApplicationKeyByProcessId_BeginInvoke_mAD164DB8849446DE0A070CF735A3DC68B4F98BF6 ();
// 0x000005CE OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetApplicationKeyByProcessId::EndInvoke(System.IAsyncResult)
extern void _GetApplicationKeyByProcessId_EndInvoke_mCC47F8D5E33C1BC3310574C4032F482D8C8146F7 ();
// 0x000005CF System.Void OVR.OpenVR.IVRApplications__LaunchApplication::.ctor(System.Object,System.IntPtr)
extern void _LaunchApplication__ctor_m66331F78E78718C73B6EAECE2554EEEACE695C3B ();
// 0x000005D0 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchApplication::Invoke(System.String)
extern void _LaunchApplication_Invoke_mB18025113B887F6E234131F0B0053BCA905ABC7B ();
// 0x000005D1 System.IAsyncResult OVR.OpenVR.IVRApplications__LaunchApplication::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _LaunchApplication_BeginInvoke_m5952281F0AB13E9A7E9DB8FD4282D422BAA70707 ();
// 0x000005D2 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchApplication::EndInvoke(System.IAsyncResult)
extern void _LaunchApplication_EndInvoke_m2064B65C2C37A46027CD6BCBFAC7206E3C9FE3D2 ();
// 0x000005D3 System.Void OVR.OpenVR.IVRApplications__LaunchTemplateApplication::.ctor(System.Object,System.IntPtr)
extern void _LaunchTemplateApplication__ctor_m2F2467044BF449071B05C321FB8A025D62BD9482 ();
// 0x000005D4 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchTemplateApplication::Invoke(System.String,System.String,OVR.OpenVR.AppOverrideKeys_t[],System.UInt32)
extern void _LaunchTemplateApplication_Invoke_mA2667FC537C296F54871C2CDEE6A06ADA2261828 ();
// 0x000005D5 System.IAsyncResult OVR.OpenVR.IVRApplications__LaunchTemplateApplication::BeginInvoke(System.String,System.String,OVR.OpenVR.AppOverrideKeys_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _LaunchTemplateApplication_BeginInvoke_m986D7DDFE1CE379AC90D6096E433DF13714092A1 ();
// 0x000005D6 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchTemplateApplication::EndInvoke(System.IAsyncResult)
extern void _LaunchTemplateApplication_EndInvoke_mA4682D411519965CE7EE432ADBF74A2B1BDCED3B ();
// 0x000005D7 System.Void OVR.OpenVR.IVRApplications__LaunchApplicationFromMimeType::.ctor(System.Object,System.IntPtr)
extern void _LaunchApplicationFromMimeType__ctor_mEE5DAC21CE60D38EA4435E89E9EC3D10A00D8B5B ();
// 0x000005D8 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchApplicationFromMimeType::Invoke(System.String,System.String)
extern void _LaunchApplicationFromMimeType_Invoke_m767617AA6C75CF1D09462E3E55CFD858B47BD35A ();
// 0x000005D9 System.IAsyncResult OVR.OpenVR.IVRApplications__LaunchApplicationFromMimeType::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void _LaunchApplicationFromMimeType_BeginInvoke_m3870931FF064305CE3FA23B2DB76350F3635086D ();
// 0x000005DA OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchApplicationFromMimeType::EndInvoke(System.IAsyncResult)
extern void _LaunchApplicationFromMimeType_EndInvoke_mD6DBD6A00DC336F9E4155CD3E8C4766915FEF588 ();
// 0x000005DB System.Void OVR.OpenVR.IVRApplications__LaunchDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern void _LaunchDashboardOverlay__ctor_mE5BF9F8873259414BECD1CCC62BB31BC72AE4340 ();
// 0x000005DC OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchDashboardOverlay::Invoke(System.String)
extern void _LaunchDashboardOverlay_Invoke_mBF4FAFC66BC768BCB500B7E28C1F0985AF27E2B7 ();
// 0x000005DD System.IAsyncResult OVR.OpenVR.IVRApplications__LaunchDashboardOverlay::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _LaunchDashboardOverlay_BeginInvoke_mBBEB79ED5A38AE68B1C23ED90DDB784ACD6527BB ();
// 0x000005DE OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchDashboardOverlay::EndInvoke(System.IAsyncResult)
extern void _LaunchDashboardOverlay_EndInvoke_m88FC98EB7C0507FEABDBC6231FC82FF894E5AAB1 ();
// 0x000005DF System.Void OVR.OpenVR.IVRApplications__CancelApplicationLaunch::.ctor(System.Object,System.IntPtr)
extern void _CancelApplicationLaunch__ctor_m1D909079D2D01FC7EFB1090BB4F4DE28417CDC49 ();
// 0x000005E0 System.Boolean OVR.OpenVR.IVRApplications__CancelApplicationLaunch::Invoke(System.String)
extern void _CancelApplicationLaunch_Invoke_mA6F09E650B05395A3F5F0AA00E15C01F1E44779E ();
// 0x000005E1 System.IAsyncResult OVR.OpenVR.IVRApplications__CancelApplicationLaunch::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _CancelApplicationLaunch_BeginInvoke_m31BAA8F2698199C55CA78CA6FD5F20CA5533700B ();
// 0x000005E2 System.Boolean OVR.OpenVR.IVRApplications__CancelApplicationLaunch::EndInvoke(System.IAsyncResult)
extern void _CancelApplicationLaunch_EndInvoke_m19B15D6691AD84E073CAE8829635B4D0F2E60AC4 ();
// 0x000005E3 System.Void OVR.OpenVR.IVRApplications__IdentifyApplication::.ctor(System.Object,System.IntPtr)
extern void _IdentifyApplication__ctor_mB43FE26A4FB50AD9FAC81BA8CBC6DAD2AAF859DC ();
// 0x000005E4 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__IdentifyApplication::Invoke(System.UInt32,System.String)
extern void _IdentifyApplication_Invoke_mBDF0FB2C8FF929343689E48E5A82449A78DF3BAD ();
// 0x000005E5 System.IAsyncResult OVR.OpenVR.IVRApplications__IdentifyApplication::BeginInvoke(System.UInt32,System.String,System.AsyncCallback,System.Object)
extern void _IdentifyApplication_BeginInvoke_mE4D7C6C6940E1DFCB32C0FDE27BD57095FDDFEB3 ();
// 0x000005E6 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__IdentifyApplication::EndInvoke(System.IAsyncResult)
extern void _IdentifyApplication_EndInvoke_mAB5BB3A3866E784FEF2615172963F15E66D7DD7B ();
// 0x000005E7 System.Void OVR.OpenVR.IVRApplications__GetApplicationProcessId::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationProcessId__ctor_mF0E729537282EC197FD196895EBE92E64DDCE365 ();
// 0x000005E8 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationProcessId::Invoke(System.String)
extern void _GetApplicationProcessId_Invoke_mE974998B62A8F801F6C0075E2764CB0AE1FAF1C6 ();
// 0x000005E9 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationProcessId::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _GetApplicationProcessId_BeginInvoke_mA400864A78758CC8D8EFF91DD2E33D56F1BF6B51 ();
// 0x000005EA System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationProcessId::EndInvoke(System.IAsyncResult)
extern void _GetApplicationProcessId_EndInvoke_m23A7AEBB7FFDCF1AC9F193D3C53A74053DBBAD6D ();
// 0x000005EB System.Void OVR.OpenVR.IVRApplications__GetApplicationsErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationsErrorNameFromEnum__ctor_m6778968488CD0EEF44F5E6782AC6C559F0A9F42A ();
// 0x000005EC System.IntPtr OVR.OpenVR.IVRApplications__GetApplicationsErrorNameFromEnum::Invoke(OVR.OpenVR.EVRApplicationError)
extern void _GetApplicationsErrorNameFromEnum_Invoke_m1C69420F08EB2A8391EA8E71AB20F3DA0B08FA16 ();
// 0x000005ED System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationsErrorNameFromEnum::BeginInvoke(OVR.OpenVR.EVRApplicationError,System.AsyncCallback,System.Object)
extern void _GetApplicationsErrorNameFromEnum_BeginInvoke_m0432C3310897CE4B2158E3DDCF956B4BE8BFD159 ();
// 0x000005EE System.IntPtr OVR.OpenVR.IVRApplications__GetApplicationsErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetApplicationsErrorNameFromEnum_EndInvoke_mCFC5BD58D3B2612EB79D15FD572F042E94900C7C ();
// 0x000005EF System.Void OVR.OpenVR.IVRApplications__GetApplicationPropertyString::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationPropertyString__ctor_mAAA16E43DB585F615D02A70EEFA0858D0D0675C4 ();
// 0x000005F0 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationPropertyString::Invoke(System.String,OVR.OpenVR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRApplicationError&)
extern void _GetApplicationPropertyString_Invoke_m970FF900F7167283C44BEB1628733D3BD112FAA7 ();
// 0x000005F1 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationPropertyString::BeginInvoke(System.String,OVR.OpenVR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRApplicationError&,System.AsyncCallback,System.Object)
extern void _GetApplicationPropertyString_BeginInvoke_m82A6ACFEDEF9AB4EC72A8299FBE85FAE113F6AAA ();
// 0x000005F2 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationPropertyString::EndInvoke(OVR.OpenVR.EVRApplicationError&,System.IAsyncResult)
extern void _GetApplicationPropertyString_EndInvoke_m12323BD85FE5F3E1E04C2354F2F1F8F341C6DFBE ();
// 0x000005F3 System.Void OVR.OpenVR.IVRApplications__GetApplicationPropertyBool::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationPropertyBool__ctor_m6EDA8B745CFAF8E39E2A62D7FBC66695C8FB921F ();
// 0x000005F4 System.Boolean OVR.OpenVR.IVRApplications__GetApplicationPropertyBool::Invoke(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&)
extern void _GetApplicationPropertyBool_Invoke_m9B14396C19D63B85D68F41346825980116C6E52F ();
// 0x000005F5 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationPropertyBool::BeginInvoke(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&,System.AsyncCallback,System.Object)
extern void _GetApplicationPropertyBool_BeginInvoke_m80896E1380E49A5E3207A4D1248C0857877887A4 ();
// 0x000005F6 System.Boolean OVR.OpenVR.IVRApplications__GetApplicationPropertyBool::EndInvoke(OVR.OpenVR.EVRApplicationError&,System.IAsyncResult)
extern void _GetApplicationPropertyBool_EndInvoke_m32A94EAC935E4D4FEBF97EE225A209D1C78C12FC ();
// 0x000005F7 System.Void OVR.OpenVR.IVRApplications__GetApplicationPropertyUint64::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationPropertyUint64__ctor_m63DCFF0968C0E6F000B124346C9748EEDA2BDF4B ();
// 0x000005F8 System.UInt64 OVR.OpenVR.IVRApplications__GetApplicationPropertyUint64::Invoke(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&)
extern void _GetApplicationPropertyUint64_Invoke_m9237BBCE4BC86750C9B301DF5397F1753810D014 ();
// 0x000005F9 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationPropertyUint64::BeginInvoke(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&,System.AsyncCallback,System.Object)
extern void _GetApplicationPropertyUint64_BeginInvoke_mDBE2B8E777DCA4C12C9BD4D4122BC37BB209027C ();
// 0x000005FA System.UInt64 OVR.OpenVR.IVRApplications__GetApplicationPropertyUint64::EndInvoke(OVR.OpenVR.EVRApplicationError&,System.IAsyncResult)
extern void _GetApplicationPropertyUint64_EndInvoke_mE6FBC4918A4D39551923EA69E9032DD634DD66B4 ();
// 0x000005FB System.Void OVR.OpenVR.IVRApplications__SetApplicationAutoLaunch::.ctor(System.Object,System.IntPtr)
extern void _SetApplicationAutoLaunch__ctor_mE75647ED640E02909498CE2244DF2CEC42A62E0A ();
// 0x000005FC OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__SetApplicationAutoLaunch::Invoke(System.String,System.Boolean)
extern void _SetApplicationAutoLaunch_Invoke_mA2A82E8A4B7BF03ECBF469F2929D647D21DD36C2 ();
// 0x000005FD System.IAsyncResult OVR.OpenVR.IVRApplications__SetApplicationAutoLaunch::BeginInvoke(System.String,System.Boolean,System.AsyncCallback,System.Object)
extern void _SetApplicationAutoLaunch_BeginInvoke_mB1B7ABF72FC57A2D8E808635EF659500F3042065 ();
// 0x000005FE OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__SetApplicationAutoLaunch::EndInvoke(System.IAsyncResult)
extern void _SetApplicationAutoLaunch_EndInvoke_m5E3A9C35F2B42E79D328A369ECC0B4E954679C63 ();
// 0x000005FF System.Void OVR.OpenVR.IVRApplications__GetApplicationAutoLaunch::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationAutoLaunch__ctor_m6FFC6541FE7F3DBBC38984CE02389F726BDC26D2 ();
// 0x00000600 System.Boolean OVR.OpenVR.IVRApplications__GetApplicationAutoLaunch::Invoke(System.String)
extern void _GetApplicationAutoLaunch_Invoke_mDE6044AC0B43A889DFD9EF6DA6CED1E4573AD32C ();
// 0x00000601 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationAutoLaunch::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _GetApplicationAutoLaunch_BeginInvoke_m18B604769FD16A2C6BCE188C1E734294261993AF ();
// 0x00000602 System.Boolean OVR.OpenVR.IVRApplications__GetApplicationAutoLaunch::EndInvoke(System.IAsyncResult)
extern void _GetApplicationAutoLaunch_EndInvoke_mA90905B69623B654B9D50D7033CB276B44877408 ();
// 0x00000603 System.Void OVR.OpenVR.IVRApplications__SetDefaultApplicationForMimeType::.ctor(System.Object,System.IntPtr)
extern void _SetDefaultApplicationForMimeType__ctor_mDD962D61F4F0709AD9032D5BE7F403D7D3B85E56 ();
// 0x00000604 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__SetDefaultApplicationForMimeType::Invoke(System.String,System.String)
extern void _SetDefaultApplicationForMimeType_Invoke_m26CA165503571550BA208ECD213A9F7B5B50594A ();
// 0x00000605 System.IAsyncResult OVR.OpenVR.IVRApplications__SetDefaultApplicationForMimeType::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void _SetDefaultApplicationForMimeType_BeginInvoke_m7FCBAAD9DFE11BEA8B8A5EF5C951F82EF80598CA ();
// 0x00000606 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__SetDefaultApplicationForMimeType::EndInvoke(System.IAsyncResult)
extern void _SetDefaultApplicationForMimeType_EndInvoke_m3D141DC7F0724C9BBC031565A9C6CAB9570DD6F8 ();
// 0x00000607 System.Void OVR.OpenVR.IVRApplications__GetDefaultApplicationForMimeType::.ctor(System.Object,System.IntPtr)
extern void _GetDefaultApplicationForMimeType__ctor_m6C626E6215A10A860DC3AC765A6FCAC856A777FA ();
// 0x00000608 System.Boolean OVR.OpenVR.IVRApplications__GetDefaultApplicationForMimeType::Invoke(System.String,System.Text.StringBuilder,System.UInt32)
extern void _GetDefaultApplicationForMimeType_Invoke_mE9E74EA1EDC480939AD485AAD8535042560C3A2E ();
// 0x00000609 System.IAsyncResult OVR.OpenVR.IVRApplications__GetDefaultApplicationForMimeType::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetDefaultApplicationForMimeType_BeginInvoke_m9383E8341B91EAE9F448E5B44A62974C73D4A782 ();
// 0x0000060A System.Boolean OVR.OpenVR.IVRApplications__GetDefaultApplicationForMimeType::EndInvoke(System.IAsyncResult)
extern void _GetDefaultApplicationForMimeType_EndInvoke_m1F4CF2D9DF4B82E28CD78AA1CD583ADE77245224 ();
// 0x0000060B System.Void OVR.OpenVR.IVRApplications__GetApplicationSupportedMimeTypes::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationSupportedMimeTypes__ctor_m48E5FD8195605891F789B2AA2A36356D896631DC ();
// 0x0000060C System.Boolean OVR.OpenVR.IVRApplications__GetApplicationSupportedMimeTypes::Invoke(System.String,System.Text.StringBuilder,System.UInt32)
extern void _GetApplicationSupportedMimeTypes_Invoke_m0DCCF72E59C727962FF920BE26645342A327C5E9 ();
// 0x0000060D System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationSupportedMimeTypes::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetApplicationSupportedMimeTypes_BeginInvoke_m702E375832498F9C968A821A04241CEEFC9B412C ();
// 0x0000060E System.Boolean OVR.OpenVR.IVRApplications__GetApplicationSupportedMimeTypes::EndInvoke(System.IAsyncResult)
extern void _GetApplicationSupportedMimeTypes_EndInvoke_m3809E01ED389B61FE11DED48064D50B63C3AC619 ();
// 0x0000060F System.Void OVR.OpenVR.IVRApplications__GetApplicationsThatSupportMimeType::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationsThatSupportMimeType__ctor_m21107403C77281A366A0A081ED80ACE77D6953C1 ();
// 0x00000610 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationsThatSupportMimeType::Invoke(System.String,System.Text.StringBuilder,System.UInt32)
extern void _GetApplicationsThatSupportMimeType_Invoke_mEDE676EEEAD468EADF5941600B5C3F78AFCB549F ();
// 0x00000611 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationsThatSupportMimeType::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetApplicationsThatSupportMimeType_BeginInvoke_m212918EC011D235D130B881DC9A4F05E78F8E857 ();
// 0x00000612 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationsThatSupportMimeType::EndInvoke(System.IAsyncResult)
extern void _GetApplicationsThatSupportMimeType_EndInvoke_m7A87BA58183A842777AA6040D944CC4177A42762 ();
// 0x00000613 System.Void OVR.OpenVR.IVRApplications__GetApplicationLaunchArguments::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationLaunchArguments__ctor_mA37130E68A8B0124CB50CBBF3D1E941CC6A2156D ();
// 0x00000614 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationLaunchArguments::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetApplicationLaunchArguments_Invoke_m6018D754C57BC75F6D278E5046C03BC70FEE0742 ();
// 0x00000615 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationLaunchArguments::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetApplicationLaunchArguments_BeginInvoke_m62C64DB5D1CCC6F7BC09EEAE679754F95B558029 ();
// 0x00000616 System.UInt32 OVR.OpenVR.IVRApplications__GetApplicationLaunchArguments::EndInvoke(System.IAsyncResult)
extern void _GetApplicationLaunchArguments_EndInvoke_mB6DCDD968395E922425E37C5A4435DC455AEFB21 ();
// 0x00000617 System.Void OVR.OpenVR.IVRApplications__GetStartingApplication::.ctor(System.Object,System.IntPtr)
extern void _GetStartingApplication__ctor_mD18D8D540CB468E3F52B03079F006C9F81D6E8F0 ();
// 0x00000618 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetStartingApplication::Invoke(System.Text.StringBuilder,System.UInt32)
extern void _GetStartingApplication_Invoke_mC622EDD6C695EF0C7372D14AFEB9ED2009B0E954 ();
// 0x00000619 System.IAsyncResult OVR.OpenVR.IVRApplications__GetStartingApplication::BeginInvoke(System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetStartingApplication_BeginInvoke_m4CA51449BBB86A87639CC8E273E506C7699809C7 ();
// 0x0000061A OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__GetStartingApplication::EndInvoke(System.IAsyncResult)
extern void _GetStartingApplication_EndInvoke_m1C50AEC21C8EE285499CD72D131815173C8614A6 ();
// 0x0000061B System.Void OVR.OpenVR.IVRApplications__GetTransitionState::.ctor(System.Object,System.IntPtr)
extern void _GetTransitionState__ctor_m34CA516C187CAE43F45609FD9817BEFE5C7F7601 ();
// 0x0000061C OVR.OpenVR.EVRApplicationTransitionState OVR.OpenVR.IVRApplications__GetTransitionState::Invoke()
extern void _GetTransitionState_Invoke_m0CB91D3C0364B314797FF74D2558B28FBB260D71 ();
// 0x0000061D System.IAsyncResult OVR.OpenVR.IVRApplications__GetTransitionState::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetTransitionState_BeginInvoke_mA0A4C91F9458A23435C3A7A3A63E704F025333E9 ();
// 0x0000061E OVR.OpenVR.EVRApplicationTransitionState OVR.OpenVR.IVRApplications__GetTransitionState::EndInvoke(System.IAsyncResult)
extern void _GetTransitionState_EndInvoke_mCF5B937D418064A32AAAFB475D190AE7FDA39B3B ();
// 0x0000061F System.Void OVR.OpenVR.IVRApplications__PerformApplicationPrelaunchCheck::.ctor(System.Object,System.IntPtr)
extern void _PerformApplicationPrelaunchCheck__ctor_mA2FCDBD2E16623E41FE61C25DF182236480F18C8 ();
// 0x00000620 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__PerformApplicationPrelaunchCheck::Invoke(System.String)
extern void _PerformApplicationPrelaunchCheck_Invoke_m68FD0C4A63420B152128B880BDE83CF06F55992E ();
// 0x00000621 System.IAsyncResult OVR.OpenVR.IVRApplications__PerformApplicationPrelaunchCheck::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _PerformApplicationPrelaunchCheck_BeginInvoke_mDF0053A2604FA2A46E36407E62EBC8C6FA14EA6A ();
// 0x00000622 OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__PerformApplicationPrelaunchCheck::EndInvoke(System.IAsyncResult)
extern void _PerformApplicationPrelaunchCheck_EndInvoke_mC204CB45236CD9A3AA8BC8BB5037DC4E8BF48493 ();
// 0x00000623 System.Void OVR.OpenVR.IVRApplications__GetApplicationsTransitionStateNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetApplicationsTransitionStateNameFromEnum__ctor_m77AF71EB9940C51EF994662AAC8A72363D8E5D51 ();
// 0x00000624 System.IntPtr OVR.OpenVR.IVRApplications__GetApplicationsTransitionStateNameFromEnum::Invoke(OVR.OpenVR.EVRApplicationTransitionState)
extern void _GetApplicationsTransitionStateNameFromEnum_Invoke_mA1B2075ECA5DC1CA078540262FDBDE895123E749 ();
// 0x00000625 System.IAsyncResult OVR.OpenVR.IVRApplications__GetApplicationsTransitionStateNameFromEnum::BeginInvoke(OVR.OpenVR.EVRApplicationTransitionState,System.AsyncCallback,System.Object)
extern void _GetApplicationsTransitionStateNameFromEnum_BeginInvoke_mB08D1217755FBD97FB16FA45FC7CD8A7F0AC7683 ();
// 0x00000626 System.IntPtr OVR.OpenVR.IVRApplications__GetApplicationsTransitionStateNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetApplicationsTransitionStateNameFromEnum_EndInvoke_m9456A33694B4193C6A7496352D269F498BCB0893 ();
// 0x00000627 System.Void OVR.OpenVR.IVRApplications__IsQuitUserPromptRequested::.ctor(System.Object,System.IntPtr)
extern void _IsQuitUserPromptRequested__ctor_mB85CB4A44C871059B70FCE8C1FEA9446308384CB ();
// 0x00000628 System.Boolean OVR.OpenVR.IVRApplications__IsQuitUserPromptRequested::Invoke()
extern void _IsQuitUserPromptRequested_Invoke_m8EF5A8577A945109A3AAD7F6686CA95FA5D3CEFC ();
// 0x00000629 System.IAsyncResult OVR.OpenVR.IVRApplications__IsQuitUserPromptRequested::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsQuitUserPromptRequested_BeginInvoke_mAA46F1A18CFB6DCFF10519A11A118EDDE6F71691 ();
// 0x0000062A System.Boolean OVR.OpenVR.IVRApplications__IsQuitUserPromptRequested::EndInvoke(System.IAsyncResult)
extern void _IsQuitUserPromptRequested_EndInvoke_m13F32DC5109F338E243737C0DB2F422A2962B11C ();
// 0x0000062B System.Void OVR.OpenVR.IVRApplications__LaunchInternalProcess::.ctor(System.Object,System.IntPtr)
extern void _LaunchInternalProcess__ctor_m2BA44B7526B8BFEB9B842EDAF63E90B8318A2415 ();
// 0x0000062C OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchInternalProcess::Invoke(System.String,System.String,System.String)
extern void _LaunchInternalProcess_Invoke_m21FE6F08ED76EBBDC5599EDEF6FDC055A2EC49EA ();
// 0x0000062D System.IAsyncResult OVR.OpenVR.IVRApplications__LaunchInternalProcess::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern void _LaunchInternalProcess_BeginInvoke_m84FFB33B208036038FF7906F1F49EFF6DB71348D ();
// 0x0000062E OVR.OpenVR.EVRApplicationError OVR.OpenVR.IVRApplications__LaunchInternalProcess::EndInvoke(System.IAsyncResult)
extern void _LaunchInternalProcess_EndInvoke_m827F7E8C587AD55C4FEF272B7AE95481392E393A ();
// 0x0000062F System.Void OVR.OpenVR.IVRApplications__GetCurrentSceneProcessId::.ctor(System.Object,System.IntPtr)
extern void _GetCurrentSceneProcessId__ctor_m4CE139BC60DBA5796EE5DE1B8FEF8899885AEAF2 ();
// 0x00000630 System.UInt32 OVR.OpenVR.IVRApplications__GetCurrentSceneProcessId::Invoke()
extern void _GetCurrentSceneProcessId_Invoke_m3355E48640ED7D60AD10B406D1B43066CF7BDC28 ();
// 0x00000631 System.IAsyncResult OVR.OpenVR.IVRApplications__GetCurrentSceneProcessId::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetCurrentSceneProcessId_BeginInvoke_m7684E5A83B9FDA85E15AC803E77DED693D647682 ();
// 0x00000632 System.UInt32 OVR.OpenVR.IVRApplications__GetCurrentSceneProcessId::EndInvoke(System.IAsyncResult)
extern void _GetCurrentSceneProcessId_EndInvoke_mE66A0251E95DF34F2A452D22F8402FC652B176A5 ();
// 0x00000633 System.Void OVR.OpenVR.IVRChaperone__GetCalibrationState::.ctor(System.Object,System.IntPtr)
extern void _GetCalibrationState__ctor_m391848D07A54F016118F9F10065A4AC945693B81 ();
// 0x00000634 OVR.OpenVR.ChaperoneCalibrationState OVR.OpenVR.IVRChaperone__GetCalibrationState::Invoke()
extern void _GetCalibrationState_Invoke_mA921B066BD69721095036CCC556345075CF2CB94 ();
// 0x00000635 System.IAsyncResult OVR.OpenVR.IVRChaperone__GetCalibrationState::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetCalibrationState_BeginInvoke_mA71D5E371AECFF6076DED8B2ADCDEFADB70107F2 ();
// 0x00000636 OVR.OpenVR.ChaperoneCalibrationState OVR.OpenVR.IVRChaperone__GetCalibrationState::EndInvoke(System.IAsyncResult)
extern void _GetCalibrationState_EndInvoke_m5504BEF3E5229DECC2AF37AB41E4153CF6A82A91 ();
// 0x00000637 System.Void OVR.OpenVR.IVRChaperone__GetPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern void _GetPlayAreaSize__ctor_mDC1F208CC58A78D9BA994A46C3DCA49482C7BA90 ();
// 0x00000638 System.Boolean OVR.OpenVR.IVRChaperone__GetPlayAreaSize::Invoke(System.Single&,System.Single&)
extern void _GetPlayAreaSize_Invoke_m7FBE613CF4666ADB8BABC158EFF2424EBD56469F ();
// 0x00000639 System.IAsyncResult OVR.OpenVR.IVRChaperone__GetPlayAreaSize::BeginInvoke(System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetPlayAreaSize_BeginInvoke_mB6898EC15A6DCA7AB9971A799306AAC661132E94 ();
// 0x0000063A System.Boolean OVR.OpenVR.IVRChaperone__GetPlayAreaSize::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern void _GetPlayAreaSize_EndInvoke_m27D17612D5349C627CA24086791EE3FBC502ABA5 ();
// 0x0000063B System.Void OVR.OpenVR.IVRChaperone__GetPlayAreaRect::.ctor(System.Object,System.IntPtr)
extern void _GetPlayAreaRect__ctor_mE5F444F007E5286F616ED7DF2E2E6A06CBAFD377 ();
// 0x0000063C System.Boolean OVR.OpenVR.IVRChaperone__GetPlayAreaRect::Invoke(OVR.OpenVR.HmdQuad_t&)
extern void _GetPlayAreaRect_Invoke_m2BB4ABA587BB81BA960D2C347E2EB61A56DE3AD4 ();
// 0x0000063D System.IAsyncResult OVR.OpenVR.IVRChaperone__GetPlayAreaRect::BeginInvoke(OVR.OpenVR.HmdQuad_t&,System.AsyncCallback,System.Object)
extern void _GetPlayAreaRect_BeginInvoke_m79F4752FC2B22C13755AF6B575310D2C93CE3ED0 ();
// 0x0000063E System.Boolean OVR.OpenVR.IVRChaperone__GetPlayAreaRect::EndInvoke(OVR.OpenVR.HmdQuad_t&,System.IAsyncResult)
extern void _GetPlayAreaRect_EndInvoke_m8C8A8B621AAE856ED190C1FEA0BD7A76FE040A5C ();
// 0x0000063F System.Void OVR.OpenVR.IVRChaperone__ReloadInfo::.ctor(System.Object,System.IntPtr)
extern void _ReloadInfo__ctor_m6E041B00A305FB4A1D0E7615AAF5EF85971160C6 ();
// 0x00000640 System.Void OVR.OpenVR.IVRChaperone__ReloadInfo::Invoke()
extern void _ReloadInfo_Invoke_mF6724AD5E3105D61C67A63E94E6763694C2130E0 ();
// 0x00000641 System.IAsyncResult OVR.OpenVR.IVRChaperone__ReloadInfo::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ReloadInfo_BeginInvoke_mF5059DE57263450FE859CE07111DE534F971FA8C ();
// 0x00000642 System.Void OVR.OpenVR.IVRChaperone__ReloadInfo::EndInvoke(System.IAsyncResult)
extern void _ReloadInfo_EndInvoke_m1BEAABAA552B0EEC53280F926833FD2AA4824ECF ();
// 0x00000643 System.Void OVR.OpenVR.IVRChaperone__SetSceneColor::.ctor(System.Object,System.IntPtr)
extern void _SetSceneColor__ctor_m651CAF28AA2F1DC4275584B5B2F3BCD321AA904B ();
// 0x00000644 System.Void OVR.OpenVR.IVRChaperone__SetSceneColor::Invoke(OVR.OpenVR.HmdColor_t)
extern void _SetSceneColor_Invoke_m767F8E1A65F152E838796C73CE752A717FC3B915 ();
// 0x00000645 System.IAsyncResult OVR.OpenVR.IVRChaperone__SetSceneColor::BeginInvoke(OVR.OpenVR.HmdColor_t,System.AsyncCallback,System.Object)
extern void _SetSceneColor_BeginInvoke_m6BDC5CBE5F923B88BC0229C6AD98BE7C4DAB570A ();
// 0x00000646 System.Void OVR.OpenVR.IVRChaperone__SetSceneColor::EndInvoke(System.IAsyncResult)
extern void _SetSceneColor_EndInvoke_mD84023A1500A3443A49C1383319A9DDD1EF7EEBE ();
// 0x00000647 System.Void OVR.OpenVR.IVRChaperone__GetBoundsColor::.ctor(System.Object,System.IntPtr)
extern void _GetBoundsColor__ctor_mB7EE4D74A6BFBC9C18947EF7CCE6D9542D5569B0 ();
// 0x00000648 System.Void OVR.OpenVR.IVRChaperone__GetBoundsColor::Invoke(OVR.OpenVR.HmdColor_t&,System.Int32,System.Single,OVR.OpenVR.HmdColor_t&)
extern void _GetBoundsColor_Invoke_m2104C029CAA863503F423DB8A89BD833A835EC3D ();
// 0x00000649 System.IAsyncResult OVR.OpenVR.IVRChaperone__GetBoundsColor::BeginInvoke(OVR.OpenVR.HmdColor_t&,System.Int32,System.Single,OVR.OpenVR.HmdColor_t&,System.AsyncCallback,System.Object)
extern void _GetBoundsColor_BeginInvoke_mD2F4032455A6E4EB6C972A729B9E7A73F604824D ();
// 0x0000064A System.Void OVR.OpenVR.IVRChaperone__GetBoundsColor::EndInvoke(OVR.OpenVR.HmdColor_t&,OVR.OpenVR.HmdColor_t&,System.IAsyncResult)
extern void _GetBoundsColor_EndInvoke_m1D2691A4DA2B51D462E3FD79850767C6CEDB02DB ();
// 0x0000064B System.Void OVR.OpenVR.IVRChaperone__AreBoundsVisible::.ctor(System.Object,System.IntPtr)
extern void _AreBoundsVisible__ctor_mB388D77A6F394EE0B8BB31755F3170840F7C7FAD ();
// 0x0000064C System.Boolean OVR.OpenVR.IVRChaperone__AreBoundsVisible::Invoke()
extern void _AreBoundsVisible_Invoke_m687BAA828F5B2DAF7CCA68A8B176B887C15912D5 ();
// 0x0000064D System.IAsyncResult OVR.OpenVR.IVRChaperone__AreBoundsVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern void _AreBoundsVisible_BeginInvoke_mA3B7B557C6633E3B4AD28C7D5F9C815A7CA298DB ();
// 0x0000064E System.Boolean OVR.OpenVR.IVRChaperone__AreBoundsVisible::EndInvoke(System.IAsyncResult)
extern void _AreBoundsVisible_EndInvoke_m4CE761533527D41CA4CF5FCD2FB9186106384EF2 ();
// 0x0000064F System.Void OVR.OpenVR.IVRChaperone__ForceBoundsVisible::.ctor(System.Object,System.IntPtr)
extern void _ForceBoundsVisible__ctor_mFE47D965C33F4185CE5941FA420EACA839EB453E ();
// 0x00000650 System.Void OVR.OpenVR.IVRChaperone__ForceBoundsVisible::Invoke(System.Boolean)
extern void _ForceBoundsVisible_Invoke_m5FCA0FC2F659289190E2F8DCE8AC29C2190D5A08 ();
// 0x00000651 System.IAsyncResult OVR.OpenVR.IVRChaperone__ForceBoundsVisible::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void _ForceBoundsVisible_BeginInvoke_m9D096F94D71FF802B040290FEB848D0D7312765E ();
// 0x00000652 System.Void OVR.OpenVR.IVRChaperone__ForceBoundsVisible::EndInvoke(System.IAsyncResult)
extern void _ForceBoundsVisible_EndInvoke_mC7CD101DBFC6F7D74259203A4A650F36FF24FAB1 ();
// 0x00000653 System.Void OVR.OpenVR.IVRChaperoneSetup__CommitWorkingCopy::.ctor(System.Object,System.IntPtr)
extern void _CommitWorkingCopy__ctor_m6119CDC225C28922651BED66A4FBA1255D9EBB82 ();
// 0x00000654 System.Boolean OVR.OpenVR.IVRChaperoneSetup__CommitWorkingCopy::Invoke(OVR.OpenVR.EChaperoneConfigFile)
extern void _CommitWorkingCopy_Invoke_m3502C7464CD4C2A1A3273E9DB192A8A36CC77B03 ();
// 0x00000655 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__CommitWorkingCopy::BeginInvoke(OVR.OpenVR.EChaperoneConfigFile,System.AsyncCallback,System.Object)
extern void _CommitWorkingCopy_BeginInvoke_mF5A4B1DF92DEE63110A5982F10C1D0257058B2FB ();
// 0x00000656 System.Boolean OVR.OpenVR.IVRChaperoneSetup__CommitWorkingCopy::EndInvoke(System.IAsyncResult)
extern void _CommitWorkingCopy_EndInvoke_mB2983D8AB4AEC592105A74744B56E405D29093DA ();
// 0x00000657 System.Void OVR.OpenVR.IVRChaperoneSetup__RevertWorkingCopy::.ctor(System.Object,System.IntPtr)
extern void _RevertWorkingCopy__ctor_m88A941F4416A1811003667971BBA3A5E2A5D606E ();
// 0x00000658 System.Void OVR.OpenVR.IVRChaperoneSetup__RevertWorkingCopy::Invoke()
extern void _RevertWorkingCopy_Invoke_m818368F57F2265AFD60DCD5E2205523FDFA1CD47 ();
// 0x00000659 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__RevertWorkingCopy::BeginInvoke(System.AsyncCallback,System.Object)
extern void _RevertWorkingCopy_BeginInvoke_m96AFBB0A937C51FC244D7246106FF763EA23B0A4 ();
// 0x0000065A System.Void OVR.OpenVR.IVRChaperoneSetup__RevertWorkingCopy::EndInvoke(System.IAsyncResult)
extern void _RevertWorkingCopy_EndInvoke_mB8118CBA20BB1A158588D32B09FC5F2FDAD5E1D4 ();
// 0x0000065B System.Void OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern void _GetWorkingPlayAreaSize__ctor_m6AD6498B731DE09984BCA35135C9E84451BA01BB ();
// 0x0000065C System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaSize::Invoke(System.Single&,System.Single&)
extern void _GetWorkingPlayAreaSize_Invoke_mFC109852D4E17861BC6B1296F22886F0D4799EE9 ();
// 0x0000065D System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaSize::BeginInvoke(System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetWorkingPlayAreaSize_BeginInvoke_mB2C019121212D9548E9577D3C56FE4493AC69820 ();
// 0x0000065E System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaSize::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern void _GetWorkingPlayAreaSize_EndInvoke_mB522D19BFB33370DEA86D485E8A024A3FECB8342 ();
// 0x0000065F System.Void OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaRect::.ctor(System.Object,System.IntPtr)
extern void _GetWorkingPlayAreaRect__ctor_m6E7C9256FBD19CA6392C6D8D23B833A88FE82DC0 ();
// 0x00000660 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaRect::Invoke(OVR.OpenVR.HmdQuad_t&)
extern void _GetWorkingPlayAreaRect_Invoke_mE375BD26CB235A0F7AEB43DEE700B9E8203E1A6A ();
// 0x00000661 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaRect::BeginInvoke(OVR.OpenVR.HmdQuad_t&,System.AsyncCallback,System.Object)
extern void _GetWorkingPlayAreaRect_BeginInvoke_mE2032707115B419C29B7ABA324DA043F4837DB27 ();
// 0x00000662 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingPlayAreaRect::EndInvoke(OVR.OpenVR.HmdQuad_t&,System.IAsyncResult)
extern void _GetWorkingPlayAreaRect_EndInvoke_m01DACBB85CF69BCF042922B6175A7A7FB4A5AD8D ();
// 0x00000663 System.Void OVR.OpenVR.IVRChaperoneSetup__GetWorkingCollisionBoundsInfo::.ctor(System.Object,System.IntPtr)
extern void _GetWorkingCollisionBoundsInfo__ctor_m0EFAF11EB7706C2AB48816AD5F621D533C8CA862 ();
// 0x00000664 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingCollisionBoundsInfo::Invoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&)
extern void _GetWorkingCollisionBoundsInfo_Invoke_mDAFC3BEDA35AC0D776F4B5B81B2A00FCD3195BAC ();
// 0x00000665 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetWorkingCollisionBoundsInfo::BeginInvoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetWorkingCollisionBoundsInfo_BeginInvoke_m9DD9D61CDB380AAB1C2E0108C35281297C70D899 ();
// 0x00000666 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingCollisionBoundsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetWorkingCollisionBoundsInfo_EndInvoke_m95B65FFA19DEF3A12AC7F662AE9241A51CEE66BD ();
// 0x00000667 System.Void OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsInfo::.ctor(System.Object,System.IntPtr)
extern void _GetLiveCollisionBoundsInfo__ctor_m16566693A5EAAED1B9B643B4FE7DBA70E974CE53 ();
// 0x00000668 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsInfo::Invoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&)
extern void _GetLiveCollisionBoundsInfo_Invoke_m4C86C52BD805A3DAAFE3D3082DE615206A850D07 ();
// 0x00000669 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsInfo::BeginInvoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetLiveCollisionBoundsInfo_BeginInvoke_m3893CA50B1355313AF2C640AAEBE5AAAAA67EAF1 ();
// 0x0000066A System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetLiveCollisionBoundsInfo_EndInvoke_m24146FFEC15FCF3D2E2B27F00A6839D15BB5A763 ();
// 0x0000066B System.Void OVR.OpenVR.IVRChaperoneSetup__GetWorkingSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetWorkingSeatedZeroPoseToRawTrackingPose__ctor_m2C9474C6D82C77DC9952C167CE156D51C40DCC48 ();
// 0x0000066C System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingSeatedZeroPoseToRawTrackingPose::Invoke(OVR.OpenVR.HmdMatrix34_t&)
extern void _GetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m4181F47E5782F10DC94BE3EA35FD70C330BD9E7E ();
// 0x0000066D System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetWorkingSeatedZeroPoseToRawTrackingPose::BeginInvoke(OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_mD7C8440BA0025089C0E80577EE7DA503F6C2DA62 ();
// 0x0000066E System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingSeatedZeroPoseToRawTrackingPose::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m716CCA424829F8BA1FFA317C718FFF3C34556BCA ();
// 0x0000066F System.Void OVR.OpenVR.IVRChaperoneSetup__GetWorkingStandingZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetWorkingStandingZeroPoseToRawTrackingPose__ctor_m2E599FA31ACE06D69A274E6D3C1684B0884A4C59 ();
// 0x00000670 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingStandingZeroPoseToRawTrackingPose::Invoke(OVR.OpenVR.HmdMatrix34_t&)
extern void _GetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m5E9D5D0BE45410AAB0E4759DBB9B56F0F34CC079 ();
// 0x00000671 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetWorkingStandingZeroPoseToRawTrackingPose::BeginInvoke(OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_mE6B2ACC68DF1FB78171E4E8550403A31DD7F5F91 ();
// 0x00000672 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetWorkingStandingZeroPoseToRawTrackingPose::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_mD512BF22E4305C6A0792C71DDE4F7D8DF9BD9413 ();
// 0x00000673 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingPlayAreaSize__ctor_m7F1D398B800BCC63270BBC1D828B8184BFFC57EE ();
// 0x00000674 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingPlayAreaSize::Invoke(System.Single,System.Single)
extern void _SetWorkingPlayAreaSize_Invoke_m985D593E5ECAD7B4C8E5BBA3BFAB73F91AED67B8 ();
// 0x00000675 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingPlayAreaSize::BeginInvoke(System.Single,System.Single,System.AsyncCallback,System.Object)
extern void _SetWorkingPlayAreaSize_BeginInvoke_mB3A4EF0E0630869B78B92A8EC215F7E7A61E284F ();
// 0x00000676 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingPlayAreaSize::EndInvoke(System.IAsyncResult)
extern void _SetWorkingPlayAreaSize_EndInvoke_mDEB4F4C5D99A6B94B904193AC13D45EC306121FB ();
// 0x00000677 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsInfo::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingCollisionBoundsInfo__ctor_m9006D32C05AB70055D59B7F9EF6E66E5E6757458 ();
// 0x00000678 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsInfo::Invoke(OVR.OpenVR.HmdQuad_t[],System.UInt32)
extern void _SetWorkingCollisionBoundsInfo_Invoke_mDABF0506B5788D4DCF344DBFB83069923014DE70 ();
// 0x00000679 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsInfo::BeginInvoke(OVR.OpenVR.HmdQuad_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _SetWorkingCollisionBoundsInfo_BeginInvoke_m660BF3E708B69AA35317AC1B09ED6710C5546F86 ();
// 0x0000067A System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsInfo::EndInvoke(System.IAsyncResult)
extern void _SetWorkingCollisionBoundsInfo_EndInvoke_mE8B6D3E37AB375FD5C977D7C2B149DF8BE905D50 ();
// 0x0000067B System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingSeatedZeroPoseToRawTrackingPose__ctor_mD92F0109415118BCBD4F808E19CF0EDF9927063B ();
// 0x0000067C System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingSeatedZeroPoseToRawTrackingPose::Invoke(OVR.OpenVR.HmdMatrix34_t&)
extern void _SetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m05B7705AAB895E2994981E0620BF36D6F99258DD ();
// 0x0000067D System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingSeatedZeroPoseToRawTrackingPose::BeginInvoke(OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_m9276BD47C609CEC80C5B852D0E7BEBC7C843B1F1 ();
// 0x0000067E System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingSeatedZeroPoseToRawTrackingPose::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m10A4B22F85F7A92B10FB6248312B5A7D1CDB5046 ();
// 0x0000067F System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingStandingZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingStandingZeroPoseToRawTrackingPose__ctor_mB9B1918FC1FF594A5DEA0011FF55E71E1EE76ED6 ();
// 0x00000680 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingStandingZeroPoseToRawTrackingPose::Invoke(OVR.OpenVR.HmdMatrix34_t&)
extern void _SetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m3FCBCD7946C6049E67486AA57B920233920274D5 ();
// 0x00000681 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingStandingZeroPoseToRawTrackingPose::BeginInvoke(OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_mC685DB39368BE82B7CD703E1298C410D63294B67 ();
// 0x00000682 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingStandingZeroPoseToRawTrackingPose::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_m9B08B8F96839FAECB840B9BEA34E96B1A67DF194 ();
// 0x00000683 System.Void OVR.OpenVR.IVRChaperoneSetup__ReloadFromDisk::.ctor(System.Object,System.IntPtr)
extern void _ReloadFromDisk__ctor_mD43352301228F86956DB3FEDE862D4081B82D48A ();
// 0x00000684 System.Void OVR.OpenVR.IVRChaperoneSetup__ReloadFromDisk::Invoke(OVR.OpenVR.EChaperoneConfigFile)
extern void _ReloadFromDisk_Invoke_mF254F7572030E0FE3E98F84D006333F79077D052 ();
// 0x00000685 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__ReloadFromDisk::BeginInvoke(OVR.OpenVR.EChaperoneConfigFile,System.AsyncCallback,System.Object)
extern void _ReloadFromDisk_BeginInvoke_m48AC8CDB95EFEDA56409EA6D1A046E37D94497F6 ();
// 0x00000686 System.Void OVR.OpenVR.IVRChaperoneSetup__ReloadFromDisk::EndInvoke(System.IAsyncResult)
extern void _ReloadFromDisk_EndInvoke_mBFB915070E147A1F5EB30D7D69ABFEA1356B0682 ();
// 0x00000687 System.Void OVR.OpenVR.IVRChaperoneSetup__GetLiveSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern void _GetLiveSeatedZeroPoseToRawTrackingPose__ctor_m3B041EE430FF36F5E267FB46D6D0B7A5D1107719 ();
// 0x00000688 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveSeatedZeroPoseToRawTrackingPose::Invoke(OVR.OpenVR.HmdMatrix34_t&)
extern void _GetLiveSeatedZeroPoseToRawTrackingPose_Invoke_m964F1199B72A75A39D13DC5176E5482224C88645 ();
// 0x00000689 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetLiveSeatedZeroPoseToRawTrackingPose::BeginInvoke(OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetLiveSeatedZeroPoseToRawTrackingPose_BeginInvoke_mEA06461D2528711FC42AD6D82AC0C2B67292169E ();
// 0x0000068A System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveSeatedZeroPoseToRawTrackingPose::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetLiveSeatedZeroPoseToRawTrackingPose_EndInvoke_m1328DCA3524202E420BBFAB11C360BEF0AF5BD5B ();
// 0x0000068B System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsTagsInfo::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingCollisionBoundsTagsInfo__ctor_m8E193FB17CF21FDCCABB65B51620072F18FE1FD9 ();
// 0x0000068C System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsTagsInfo::Invoke(System.Byte[],System.UInt32)
extern void _SetWorkingCollisionBoundsTagsInfo_Invoke_mA97A813115268E5722A1720EF1030E7573BDA3F3 ();
// 0x0000068D System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsTagsInfo::BeginInvoke(System.Byte[],System.UInt32,System.AsyncCallback,System.Object)
extern void _SetWorkingCollisionBoundsTagsInfo_BeginInvoke_mDA74BCD77EFC2787C906AC4BBD415F5B41C5B5E8 ();
// 0x0000068E System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingCollisionBoundsTagsInfo::EndInvoke(System.IAsyncResult)
extern void _SetWorkingCollisionBoundsTagsInfo_EndInvoke_m8DAAB6F68131FA162D770C08214659458DB34FDE ();
// 0x0000068F System.Void OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsTagsInfo::.ctor(System.Object,System.IntPtr)
extern void _GetLiveCollisionBoundsTagsInfo__ctor_m17ED8D572F497B98458C0685B205305EDB9C106C ();
// 0x00000690 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsTagsInfo::Invoke(System.Byte[],System.UInt32&)
extern void _GetLiveCollisionBoundsTagsInfo_Invoke_m5844DAAE9920FE81A0D5BFE2611E990A1BA4B43D ();
// 0x00000691 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsTagsInfo::BeginInvoke(System.Byte[],System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetLiveCollisionBoundsTagsInfo_BeginInvoke_mEA561325E98A766C3CF6E99F84524B3B83CE5C07 ();
// 0x00000692 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLiveCollisionBoundsTagsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetLiveCollisionBoundsTagsInfo_EndInvoke_m65A8321AB14CDFB5DE1AEA47FE91ADAC73D2FF38 ();
// 0x00000693 System.Void OVR.OpenVR.IVRChaperoneSetup__SetWorkingPhysicalBoundsInfo::.ctor(System.Object,System.IntPtr)
extern void _SetWorkingPhysicalBoundsInfo__ctor_m7119EA837FBEA054308D26880DCBE18C48F75AB7 ();
// 0x00000694 System.Boolean OVR.OpenVR.IVRChaperoneSetup__SetWorkingPhysicalBoundsInfo::Invoke(OVR.OpenVR.HmdQuad_t[],System.UInt32)
extern void _SetWorkingPhysicalBoundsInfo_Invoke_m3754699A166C67BCA0BE1CB6A7FDE52D18A15FCC ();
// 0x00000695 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__SetWorkingPhysicalBoundsInfo::BeginInvoke(OVR.OpenVR.HmdQuad_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _SetWorkingPhysicalBoundsInfo_BeginInvoke_m444974D47BFD76603E7D7357C72BC0ABA41CC80A ();
// 0x00000696 System.Boolean OVR.OpenVR.IVRChaperoneSetup__SetWorkingPhysicalBoundsInfo::EndInvoke(System.IAsyncResult)
extern void _SetWorkingPhysicalBoundsInfo_EndInvoke_mC9996DDAE5F55B1B37C6C7638CF5E5E780145568 ();
// 0x00000697 System.Void OVR.OpenVR.IVRChaperoneSetup__GetLivePhysicalBoundsInfo::.ctor(System.Object,System.IntPtr)
extern void _GetLivePhysicalBoundsInfo__ctor_mAE013DC2DDBE877CD4D86502C8D34E48A7C6D529 ();
// 0x00000698 System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLivePhysicalBoundsInfo::Invoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&)
extern void _GetLivePhysicalBoundsInfo_Invoke_m527DD73741B5A1BDE53B771B69DEFCB8603B14F8 ();
// 0x00000699 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__GetLivePhysicalBoundsInfo::BeginInvoke(OVR.OpenVR.HmdQuad_t[],System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetLivePhysicalBoundsInfo_BeginInvoke_m686549FADB1B1A3BAF914FF5D0E0D28180ECFD06 ();
// 0x0000069A System.Boolean OVR.OpenVR.IVRChaperoneSetup__GetLivePhysicalBoundsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetLivePhysicalBoundsInfo_EndInvoke_mC78987AF9B84E84DB67AEFE7837B58A3F31AA179 ();
// 0x0000069B System.Void OVR.OpenVR.IVRChaperoneSetup__ExportLiveToBuffer::.ctor(System.Object,System.IntPtr)
extern void _ExportLiveToBuffer__ctor_m69E7498138427FE2255B4CA99631877A86258D53 ();
// 0x0000069C System.Boolean OVR.OpenVR.IVRChaperoneSetup__ExportLiveToBuffer::Invoke(System.Text.StringBuilder,System.UInt32&)
extern void _ExportLiveToBuffer_Invoke_mBAA1DCD7AE665F4232117D20DF1C21FD421C0F9B ();
// 0x0000069D System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__ExportLiveToBuffer::BeginInvoke(System.Text.StringBuilder,System.UInt32&,System.AsyncCallback,System.Object)
extern void _ExportLiveToBuffer_BeginInvoke_m3FFFC8EDE8115C0B7E69242AEF7FE8D9C075102E ();
// 0x0000069E System.Boolean OVR.OpenVR.IVRChaperoneSetup__ExportLiveToBuffer::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _ExportLiveToBuffer_EndInvoke_mFA8017AEF3EA227B7F0CFDD6841337E5346DF576 ();
// 0x0000069F System.Void OVR.OpenVR.IVRChaperoneSetup__ImportFromBufferToWorking::.ctor(System.Object,System.IntPtr)
extern void _ImportFromBufferToWorking__ctor_mFB14979FBEE413CFE81532441E7D115B1922228A ();
// 0x000006A0 System.Boolean OVR.OpenVR.IVRChaperoneSetup__ImportFromBufferToWorking::Invoke(System.String,System.UInt32)
extern void _ImportFromBufferToWorking_Invoke_mAFFC138C737039EE4A8A14C7B5992A2191B9D3CF ();
// 0x000006A1 System.IAsyncResult OVR.OpenVR.IVRChaperoneSetup__ImportFromBufferToWorking::BeginInvoke(System.String,System.UInt32,System.AsyncCallback,System.Object)
extern void _ImportFromBufferToWorking_BeginInvoke_mA707892F243062BB05592D22919A3B23AA2EFA45 ();
// 0x000006A2 System.Boolean OVR.OpenVR.IVRChaperoneSetup__ImportFromBufferToWorking::EndInvoke(System.IAsyncResult)
extern void _ImportFromBufferToWorking_EndInvoke_m7A45E3B63AF719CD8D0833B0ECD4C7C6CB8281D0 ();
// 0x000006A3 System.Void OVR.OpenVR.IVRCompositor__SetTrackingSpace::.ctor(System.Object,System.IntPtr)
extern void _SetTrackingSpace__ctor_mD9295855E4372A49B21253BB142A2BBD92F9AC2B ();
// 0x000006A4 System.Void OVR.OpenVR.IVRCompositor__SetTrackingSpace::Invoke(OVR.OpenVR.ETrackingUniverseOrigin)
extern void _SetTrackingSpace_Invoke_mB46006C0CE6DC74488CEA28D2F3D5A7D0092B145 ();
// 0x000006A5 System.IAsyncResult OVR.OpenVR.IVRCompositor__SetTrackingSpace::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,System.AsyncCallback,System.Object)
extern void _SetTrackingSpace_BeginInvoke_m613C03CBF38BBE2BDD4CB82215F403B754FC0FAB ();
// 0x000006A6 System.Void OVR.OpenVR.IVRCompositor__SetTrackingSpace::EndInvoke(System.IAsyncResult)
extern void _SetTrackingSpace_EndInvoke_mBA9FA025CF79836F31E8E9D509AD0B74CC91EDF4 ();
// 0x000006A7 System.Void OVR.OpenVR.IVRCompositor__GetTrackingSpace::.ctor(System.Object,System.IntPtr)
extern void _GetTrackingSpace__ctor_m305EB2312ABECD5A69E57870DB832A66DCDD53C3 ();
// 0x000006A8 OVR.OpenVR.ETrackingUniverseOrigin OVR.OpenVR.IVRCompositor__GetTrackingSpace::Invoke()
extern void _GetTrackingSpace_Invoke_m116040E366EBCBFE98C0BF1C9A6166E86C00752F ();
// 0x000006A9 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetTrackingSpace::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetTrackingSpace_BeginInvoke_m3890C767BC3862604CDCCE3307E8B63A6F074FAB ();
// 0x000006AA OVR.OpenVR.ETrackingUniverseOrigin OVR.OpenVR.IVRCompositor__GetTrackingSpace::EndInvoke(System.IAsyncResult)
extern void _GetTrackingSpace_EndInvoke_m82E8102A6FA2F7526DB9AA67B3DA2737674CA9DC ();
// 0x000006AB System.Void OVR.OpenVR.IVRCompositor__WaitGetPoses::.ctor(System.Object,System.IntPtr)
extern void _WaitGetPoses__ctor_m628F698AAB34013939EA732661FE6CCCADF2419E ();
// 0x000006AC OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__WaitGetPoses::Invoke(OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32)
extern void _WaitGetPoses_Invoke_mEEC365C6C10E198F8E0EF8C416352235469BE5D4 ();
// 0x000006AD System.IAsyncResult OVR.OpenVR.IVRCompositor__WaitGetPoses::BeginInvoke(OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _WaitGetPoses_BeginInvoke_m7E3637C7ED4098F8AF0355C0C7DBAB59BF21C1F7 ();
// 0x000006AE OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__WaitGetPoses::EndInvoke(System.IAsyncResult)
extern void _WaitGetPoses_EndInvoke_m71D4C8D9FE1B7956FEE10D0A2FA8D830101FB015 ();
// 0x000006AF System.Void OVR.OpenVR.IVRCompositor__GetLastPoses::.ctor(System.Object,System.IntPtr)
extern void _GetLastPoses__ctor_m1EC747AB1CB75D963E49589110AF9F477DF82441 ();
// 0x000006B0 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetLastPoses::Invoke(OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32)
extern void _GetLastPoses_Invoke_m68E5F9F88CBD04D05ABD3A1045505CDDAC6F896C ();
// 0x000006B1 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetLastPoses::BeginInvoke(OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,OVR.OpenVR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _GetLastPoses_BeginInvoke_m04A26B4AEEAD6FF97CB3A93DB57AF68368D04A85 ();
// 0x000006B2 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetLastPoses::EndInvoke(System.IAsyncResult)
extern void _GetLastPoses_EndInvoke_m3B41DD9E37BEFAF5442FBF33F003A01494D447E1 ();
// 0x000006B3 System.Void OVR.OpenVR.IVRCompositor__GetLastPoseForTrackedDeviceIndex::.ctor(System.Object,System.IntPtr)
extern void _GetLastPoseForTrackedDeviceIndex__ctor_mB2E5E606C4EAFDBBF2E0307FB3CA5DDB76FCE4DB ();
// 0x000006B4 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetLastPoseForTrackedDeviceIndex::Invoke(System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&)
extern void _GetLastPoseForTrackedDeviceIndex_Invoke_m1914B7355AF459549280B0FE0795CE440AD60203 ();
// 0x000006B5 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetLastPoseForTrackedDeviceIndex::BeginInvoke(System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern void _GetLastPoseForTrackedDeviceIndex_BeginInvoke_m0EACD371B8160F7965C7B5ABD43ACB3B99C2E647 ();
// 0x000006B6 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetLastPoseForTrackedDeviceIndex::EndInvoke(OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,System.IAsyncResult)
extern void _GetLastPoseForTrackedDeviceIndex_EndInvoke_m7941B56A0D744D96A35FD7D5ACE03AF834241D2E ();
// 0x000006B7 System.Void OVR.OpenVR.IVRCompositor__Submit::.ctor(System.Object,System.IntPtr)
extern void _Submit__ctor_mCA60E0C978503ED8FC190C769D42F0AC6FD14376 ();
// 0x000006B8 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__Submit::Invoke(OVR.OpenVR.EVREye,OVR.OpenVR.Texture_t&,OVR.OpenVR.VRTextureBounds_t&,OVR.OpenVR.EVRSubmitFlags)
extern void _Submit_Invoke_mA6B89CD1BA58B4436C440869A7E4112E3FA149D2 ();
// 0x000006B9 System.IAsyncResult OVR.OpenVR.IVRCompositor__Submit::BeginInvoke(OVR.OpenVR.EVREye,OVR.OpenVR.Texture_t&,OVR.OpenVR.VRTextureBounds_t&,OVR.OpenVR.EVRSubmitFlags,System.AsyncCallback,System.Object)
extern void _Submit_BeginInvoke_m4A576D51E80D620EF3AC77463D9AE509AFC4FD67 ();
// 0x000006BA OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__Submit::EndInvoke(OVR.OpenVR.Texture_t&,OVR.OpenVR.VRTextureBounds_t&,System.IAsyncResult)
extern void _Submit_EndInvoke_mDF3959CB1B9599C29706DFD901BD0F21EA3F08B3 ();
// 0x000006BB System.Void OVR.OpenVR.IVRCompositor__ClearLastSubmittedFrame::.ctor(System.Object,System.IntPtr)
extern void _ClearLastSubmittedFrame__ctor_mD19D96974E601DE9FC048B3773D3E5B2D2FDF751 ();
// 0x000006BC System.Void OVR.OpenVR.IVRCompositor__ClearLastSubmittedFrame::Invoke()
extern void _ClearLastSubmittedFrame_Invoke_m59DCA17075ADD078ADB4DB98047D9A507EFB5005 ();
// 0x000006BD System.IAsyncResult OVR.OpenVR.IVRCompositor__ClearLastSubmittedFrame::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ClearLastSubmittedFrame_BeginInvoke_mD2472FA342EBC994B8F51D12511923A07A03B166 ();
// 0x000006BE System.Void OVR.OpenVR.IVRCompositor__ClearLastSubmittedFrame::EndInvoke(System.IAsyncResult)
extern void _ClearLastSubmittedFrame_EndInvoke_m58C826AE116EDC222FA1B1A9ABA6EA96CDCCAC7C ();
// 0x000006BF System.Void OVR.OpenVR.IVRCompositor__PostPresentHandoff::.ctor(System.Object,System.IntPtr)
extern void _PostPresentHandoff__ctor_m88750003FFBA8FC4417E2486F6ACEFB762F584AF ();
// 0x000006C0 System.Void OVR.OpenVR.IVRCompositor__PostPresentHandoff::Invoke()
extern void _PostPresentHandoff_Invoke_m662967E346613A6DF55F19DD8796E8E3B3A9398E ();
// 0x000006C1 System.IAsyncResult OVR.OpenVR.IVRCompositor__PostPresentHandoff::BeginInvoke(System.AsyncCallback,System.Object)
extern void _PostPresentHandoff_BeginInvoke_m5F2D690AE4EE290C3ABF29FF290C2D63523CB8F1 ();
// 0x000006C2 System.Void OVR.OpenVR.IVRCompositor__PostPresentHandoff::EndInvoke(System.IAsyncResult)
extern void _PostPresentHandoff_EndInvoke_m4C8A58D11DEB27495C489BEB2B03F3FB313790CF ();
// 0x000006C3 System.Void OVR.OpenVR.IVRCompositor__GetFrameTiming::.ctor(System.Object,System.IntPtr)
extern void _GetFrameTiming__ctor_mC69618F6CAA1BE7EFC20DEF1E5C7B3ECCB1A380C ();
// 0x000006C4 System.Boolean OVR.OpenVR.IVRCompositor__GetFrameTiming::Invoke(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32)
extern void _GetFrameTiming_Invoke_m45B9854777546134DBBDC4E8AB1AECAAC6115A0B ();
// 0x000006C5 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetFrameTiming::BeginInvoke(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetFrameTiming_BeginInvoke_m25562BA194279692B73A2457A900E796B9786BE9 ();
// 0x000006C6 System.Boolean OVR.OpenVR.IVRCompositor__GetFrameTiming::EndInvoke(OVR.OpenVR.Compositor_FrameTiming&,System.IAsyncResult)
extern void _GetFrameTiming_EndInvoke_m5B3BA73549D50C7E4183C450BA641D4DBD76D171 ();
// 0x000006C7 System.Void OVR.OpenVR.IVRCompositor__GetFrameTimings::.ctor(System.Object,System.IntPtr)
extern void _GetFrameTimings__ctor_m61AECEB55B10E85E947977FEEFCF97AE3C3B1E58 ();
// 0x000006C8 System.UInt32 OVR.OpenVR.IVRCompositor__GetFrameTimings::Invoke(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32)
extern void _GetFrameTimings_Invoke_m6531463FD5DA8799E5617AB7ED40EDD5C8007A14 ();
// 0x000006C9 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetFrameTimings::BeginInvoke(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetFrameTimings_BeginInvoke_mBB4198E4806C6D207738435FB2FF81AD29E9BDF6 ();
// 0x000006CA System.UInt32 OVR.OpenVR.IVRCompositor__GetFrameTimings::EndInvoke(OVR.OpenVR.Compositor_FrameTiming&,System.IAsyncResult)
extern void _GetFrameTimings_EndInvoke_m39140A3F1D5F6DBE346FCC893F08D0C190F2793E ();
// 0x000006CB System.Void OVR.OpenVR.IVRCompositor__GetFrameTimeRemaining::.ctor(System.Object,System.IntPtr)
extern void _GetFrameTimeRemaining__ctor_mA9AD1CE4F1DEF7A6379B7FE80754705A8E53B815 ();
// 0x000006CC System.Single OVR.OpenVR.IVRCompositor__GetFrameTimeRemaining::Invoke()
extern void _GetFrameTimeRemaining_Invoke_m26BB55AEE8C167A80DB3FDF042FFD4D499D34D7C ();
// 0x000006CD System.IAsyncResult OVR.OpenVR.IVRCompositor__GetFrameTimeRemaining::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetFrameTimeRemaining_BeginInvoke_m02C70E6E72B78E2E4761D355FDCFB9461536CCEF ();
// 0x000006CE System.Single OVR.OpenVR.IVRCompositor__GetFrameTimeRemaining::EndInvoke(System.IAsyncResult)
extern void _GetFrameTimeRemaining_EndInvoke_mD859B826DD280764755E04303177DDA13638D668 ();
// 0x000006CF System.Void OVR.OpenVR.IVRCompositor__GetCumulativeStats::.ctor(System.Object,System.IntPtr)
extern void _GetCumulativeStats__ctor_mB087D5C1CC5B1D3CA349BB08B5C0CA696E3DDDCE ();
// 0x000006D0 System.Void OVR.OpenVR.IVRCompositor__GetCumulativeStats::Invoke(OVR.OpenVR.Compositor_CumulativeStats&,System.UInt32)
extern void _GetCumulativeStats_Invoke_m9078FE356C4B0660284FB5699BB469FA0A92C231 ();
// 0x000006D1 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetCumulativeStats::BeginInvoke(OVR.OpenVR.Compositor_CumulativeStats&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetCumulativeStats_BeginInvoke_mBF87EB0D54295AD4A1256DDE3547AF2D6C59227D ();
// 0x000006D2 System.Void OVR.OpenVR.IVRCompositor__GetCumulativeStats::EndInvoke(OVR.OpenVR.Compositor_CumulativeStats&,System.IAsyncResult)
extern void _GetCumulativeStats_EndInvoke_m4D1DFADCB0E9666BA85637FA615C06F03C58EAB5 ();
// 0x000006D3 System.Void OVR.OpenVR.IVRCompositor__FadeToColor::.ctor(System.Object,System.IntPtr)
extern void _FadeToColor__ctor_m1C193387B0866E1236B8723A78CFBB4D0A98793F ();
// 0x000006D4 System.Void OVR.OpenVR.IVRCompositor__FadeToColor::Invoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void _FadeToColor_Invoke_m402AA10113BED820E140D07E166D2542110FB9B1 ();
// 0x000006D5 System.IAsyncResult OVR.OpenVR.IVRCompositor__FadeToColor::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.AsyncCallback,System.Object)
extern void _FadeToColor_BeginInvoke_m388F34E26B3BC6526705FDFB17FF5C28F2EAC22D ();
// 0x000006D6 System.Void OVR.OpenVR.IVRCompositor__FadeToColor::EndInvoke(System.IAsyncResult)
extern void _FadeToColor_EndInvoke_m146E47ADA5808957332AB5C75D501EC3D0C52EB5 ();
// 0x000006D7 System.Void OVR.OpenVR.IVRCompositor__GetCurrentFadeColor::.ctor(System.Object,System.IntPtr)
extern void _GetCurrentFadeColor__ctor_mD9B81BBA0820A043C29CB01C276A6DFFFA252119 ();
// 0x000006D8 OVR.OpenVR.HmdColor_t OVR.OpenVR.IVRCompositor__GetCurrentFadeColor::Invoke(System.Boolean)
extern void _GetCurrentFadeColor_Invoke_m639B0F46883B62499FCB99401091ACC87E231994 ();
// 0x000006D9 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetCurrentFadeColor::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void _GetCurrentFadeColor_BeginInvoke_mE6D7032715457DC41D75ED768903E25EE84BBC75 ();
// 0x000006DA OVR.OpenVR.HmdColor_t OVR.OpenVR.IVRCompositor__GetCurrentFadeColor::EndInvoke(System.IAsyncResult)
extern void _GetCurrentFadeColor_EndInvoke_m4A6E629433561AAE6714C00F99D4492893F80A31 ();
// 0x000006DB System.Void OVR.OpenVR.IVRCompositor__FadeGrid::.ctor(System.Object,System.IntPtr)
extern void _FadeGrid__ctor_m3E6F7295C0C8512566E19EEFF2AF019ADB5CF4AD ();
// 0x000006DC System.Void OVR.OpenVR.IVRCompositor__FadeGrid::Invoke(System.Single,System.Boolean)
extern void _FadeGrid_Invoke_m34A93EA43FBAD541234F377E2B1F3521C8273B62 ();
// 0x000006DD System.IAsyncResult OVR.OpenVR.IVRCompositor__FadeGrid::BeginInvoke(System.Single,System.Boolean,System.AsyncCallback,System.Object)
extern void _FadeGrid_BeginInvoke_m770E6F321E18D4B0C22732C365E5A570B633F373 ();
// 0x000006DE System.Void OVR.OpenVR.IVRCompositor__FadeGrid::EndInvoke(System.IAsyncResult)
extern void _FadeGrid_EndInvoke_m4FAFC53EECCAE9A8D34D5596C8F6FD424BAB365C ();
// 0x000006DF System.Void OVR.OpenVR.IVRCompositor__GetCurrentGridAlpha::.ctor(System.Object,System.IntPtr)
extern void _GetCurrentGridAlpha__ctor_m0026DF43714730A05A769072B07ED1F57EA7F648 ();
// 0x000006E0 System.Single OVR.OpenVR.IVRCompositor__GetCurrentGridAlpha::Invoke()
extern void _GetCurrentGridAlpha_Invoke_mD0DDC7F605512F31A0E07AC456893B7AD1C057A8 ();
// 0x000006E1 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetCurrentGridAlpha::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetCurrentGridAlpha_BeginInvoke_m58488E73B342A6DCBCD9C33F935372FE84F69871 ();
// 0x000006E2 System.Single OVR.OpenVR.IVRCompositor__GetCurrentGridAlpha::EndInvoke(System.IAsyncResult)
extern void _GetCurrentGridAlpha_EndInvoke_m7F270A6698C142AC58D325669EE981242A828AF7 ();
// 0x000006E3 System.Void OVR.OpenVR.IVRCompositor__SetSkyboxOverride::.ctor(System.Object,System.IntPtr)
extern void _SetSkyboxOverride__ctor_m4FF05012FE44B09DE36E9AB8D1C5E82C2B1EF2EA ();
// 0x000006E4 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__SetSkyboxOverride::Invoke(OVR.OpenVR.Texture_t[],System.UInt32)
extern void _SetSkyboxOverride_Invoke_m8664B7126D14B4CBC872B69E6E8BE1D310CFA6F7 ();
// 0x000006E5 System.IAsyncResult OVR.OpenVR.IVRCompositor__SetSkyboxOverride::BeginInvoke(OVR.OpenVR.Texture_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _SetSkyboxOverride_BeginInvoke_mC0858AB01BA17CAD82A560A77A85AF05970AAA83 ();
// 0x000006E6 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__SetSkyboxOverride::EndInvoke(System.IAsyncResult)
extern void _SetSkyboxOverride_EndInvoke_mE21BF845BC175807F52A11FE321436C903EFCC6A ();
// 0x000006E7 System.Void OVR.OpenVR.IVRCompositor__ClearSkyboxOverride::.ctor(System.Object,System.IntPtr)
extern void _ClearSkyboxOverride__ctor_mD71E9EAE366B306828E866DB40F5F279D184B618 ();
// 0x000006E8 System.Void OVR.OpenVR.IVRCompositor__ClearSkyboxOverride::Invoke()
extern void _ClearSkyboxOverride_Invoke_m43CC909EFEAD19FDBE8EAEB5B23065FD9C019C9C ();
// 0x000006E9 System.IAsyncResult OVR.OpenVR.IVRCompositor__ClearSkyboxOverride::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ClearSkyboxOverride_BeginInvoke_m11851B9C04195F504A193FBFA289542D6DA603A7 ();
// 0x000006EA System.Void OVR.OpenVR.IVRCompositor__ClearSkyboxOverride::EndInvoke(System.IAsyncResult)
extern void _ClearSkyboxOverride_EndInvoke_m64095CA801F652CBD6BC544A16905B4E3E90F426 ();
// 0x000006EB System.Void OVR.OpenVR.IVRCompositor__CompositorBringToFront::.ctor(System.Object,System.IntPtr)
extern void _CompositorBringToFront__ctor_m5DA327FD2073A4B5D74CBBF69038D9E0C164BF51 ();
// 0x000006EC System.Void OVR.OpenVR.IVRCompositor__CompositorBringToFront::Invoke()
extern void _CompositorBringToFront_Invoke_mB7C20AC4FCF8DE6BF5845E24A57D9C307D94BE3B ();
// 0x000006ED System.IAsyncResult OVR.OpenVR.IVRCompositor__CompositorBringToFront::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CompositorBringToFront_BeginInvoke_mAE7B4FE34146FA5D1710B5B4CCC7052B0DF4A598 ();
// 0x000006EE System.Void OVR.OpenVR.IVRCompositor__CompositorBringToFront::EndInvoke(System.IAsyncResult)
extern void _CompositorBringToFront_EndInvoke_m4FC83BA9BC4ECA442AF5A6F7FF5D288BBD2A0CC2 ();
// 0x000006EF System.Void OVR.OpenVR.IVRCompositor__CompositorGoToBack::.ctor(System.Object,System.IntPtr)
extern void _CompositorGoToBack__ctor_mCBAF8CE874533BC44EBA13AF0337C2911BC32BB3 ();
// 0x000006F0 System.Void OVR.OpenVR.IVRCompositor__CompositorGoToBack::Invoke()
extern void _CompositorGoToBack_Invoke_m44EB49F1B806E5AAF58ED8EF0E592A3E197188F7 ();
// 0x000006F1 System.IAsyncResult OVR.OpenVR.IVRCompositor__CompositorGoToBack::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CompositorGoToBack_BeginInvoke_m8AC3BCDE791287BDD1A897DD3AA4AAA3A6AFF6BF ();
// 0x000006F2 System.Void OVR.OpenVR.IVRCompositor__CompositorGoToBack::EndInvoke(System.IAsyncResult)
extern void _CompositorGoToBack_EndInvoke_m31C479FA7F3241940BACB67D47BAF1C116FADF34 ();
// 0x000006F3 System.Void OVR.OpenVR.IVRCompositor__CompositorQuit::.ctor(System.Object,System.IntPtr)
extern void _CompositorQuit__ctor_mDC155BC3B31EDD27B089D61DEC54A3AC9990AF5F ();
// 0x000006F4 System.Void OVR.OpenVR.IVRCompositor__CompositorQuit::Invoke()
extern void _CompositorQuit_Invoke_m5BB9B5D55A2EF1D0C43C11776892FAD190B41792 ();
// 0x000006F5 System.IAsyncResult OVR.OpenVR.IVRCompositor__CompositorQuit::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CompositorQuit_BeginInvoke_m5E87B45A9CF3D705F0791121BEC9ED3E79079D38 ();
// 0x000006F6 System.Void OVR.OpenVR.IVRCompositor__CompositorQuit::EndInvoke(System.IAsyncResult)
extern void _CompositorQuit_EndInvoke_mB485BE7041B4C1C196B53C83B03E5A7212CE2149 ();
// 0x000006F7 System.Void OVR.OpenVR.IVRCompositor__IsFullscreen::.ctor(System.Object,System.IntPtr)
extern void _IsFullscreen__ctor_m91A10D5582734A6DFA39822E44C3AC84C18A2F25 ();
// 0x000006F8 System.Boolean OVR.OpenVR.IVRCompositor__IsFullscreen::Invoke()
extern void _IsFullscreen_Invoke_m29F1EBB3CBAF8757FF3F97E7E2F68D81778D1793 ();
// 0x000006F9 System.IAsyncResult OVR.OpenVR.IVRCompositor__IsFullscreen::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsFullscreen_BeginInvoke_m53A48A340493620C3978DC1563BE05ADF29325E6 ();
// 0x000006FA System.Boolean OVR.OpenVR.IVRCompositor__IsFullscreen::EndInvoke(System.IAsyncResult)
extern void _IsFullscreen_EndInvoke_m7A117E462828DB550E2378FAB5D125624293C9BA ();
// 0x000006FB System.Void OVR.OpenVR.IVRCompositor__GetCurrentSceneFocusProcess::.ctor(System.Object,System.IntPtr)
extern void _GetCurrentSceneFocusProcess__ctor_mB297364E7F8191E52F3E070F218E1411B763AA04 ();
// 0x000006FC System.UInt32 OVR.OpenVR.IVRCompositor__GetCurrentSceneFocusProcess::Invoke()
extern void _GetCurrentSceneFocusProcess_Invoke_mFE2A7BF5E0FB35BAA590C2162140018A1119C398 ();
// 0x000006FD System.IAsyncResult OVR.OpenVR.IVRCompositor__GetCurrentSceneFocusProcess::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetCurrentSceneFocusProcess_BeginInvoke_m402C1B9836B36F82532BC649CD456A8CA7B7367C ();
// 0x000006FE System.UInt32 OVR.OpenVR.IVRCompositor__GetCurrentSceneFocusProcess::EndInvoke(System.IAsyncResult)
extern void _GetCurrentSceneFocusProcess_EndInvoke_mCC19F1ABE907DA5FC495080F17E5FA868235E6F0 ();
// 0x000006FF System.Void OVR.OpenVR.IVRCompositor__GetLastFrameRenderer::.ctor(System.Object,System.IntPtr)
extern void _GetLastFrameRenderer__ctor_m95483D8E41B238FF5BA62506096861775009225A ();
// 0x00000700 System.UInt32 OVR.OpenVR.IVRCompositor__GetLastFrameRenderer::Invoke()
extern void _GetLastFrameRenderer_Invoke_m948EC952F4ECD6EC0BD50631EE8D61248F2E3AE1 ();
// 0x00000701 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetLastFrameRenderer::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetLastFrameRenderer_BeginInvoke_mF6FB76A9288944EF13FD651F93B71B480846CD58 ();
// 0x00000702 System.UInt32 OVR.OpenVR.IVRCompositor__GetLastFrameRenderer::EndInvoke(System.IAsyncResult)
extern void _GetLastFrameRenderer_EndInvoke_mE12CDD106CC729971FC8F92287D6299C4AAE3217 ();
// 0x00000703 System.Void OVR.OpenVR.IVRCompositor__CanRenderScene::.ctor(System.Object,System.IntPtr)
extern void _CanRenderScene__ctor_mB5BDF8C3BDF87C57826B1AA6CAFB2AE493A0C9E2 ();
// 0x00000704 System.Boolean OVR.OpenVR.IVRCompositor__CanRenderScene::Invoke()
extern void _CanRenderScene_Invoke_m3C88985A0D3A30EC0A8871A9653F56DC541B5B99 ();
// 0x00000705 System.IAsyncResult OVR.OpenVR.IVRCompositor__CanRenderScene::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CanRenderScene_BeginInvoke_m62B364E1877FE5D3083121B80886D31A611F27A5 ();
// 0x00000706 System.Boolean OVR.OpenVR.IVRCompositor__CanRenderScene::EndInvoke(System.IAsyncResult)
extern void _CanRenderScene_EndInvoke_m92815C2F1FA762CC41E17A480933A15D15D7C25E ();
// 0x00000707 System.Void OVR.OpenVR.IVRCompositor__ShowMirrorWindow::.ctor(System.Object,System.IntPtr)
extern void _ShowMirrorWindow__ctor_m6E99A67798012A94CE85B19127783DB36FF2C359 ();
// 0x00000708 System.Void OVR.OpenVR.IVRCompositor__ShowMirrorWindow::Invoke()
extern void _ShowMirrorWindow_Invoke_m9EE70326549AB66AEA5DEEDE9957CFD2F7F22438 ();
// 0x00000709 System.IAsyncResult OVR.OpenVR.IVRCompositor__ShowMirrorWindow::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ShowMirrorWindow_BeginInvoke_mFE703C6BCF3EAAFFFACA314CAAEB7128199D6FDD ();
// 0x0000070A System.Void OVR.OpenVR.IVRCompositor__ShowMirrorWindow::EndInvoke(System.IAsyncResult)
extern void _ShowMirrorWindow_EndInvoke_m0840940FA70E39F75A68AC6021ED4366BCC4DB89 ();
// 0x0000070B System.Void OVR.OpenVR.IVRCompositor__HideMirrorWindow::.ctor(System.Object,System.IntPtr)
extern void _HideMirrorWindow__ctor_m111F8676161E9AABCD20EFEA7B472A4ED8A3A339 ();
// 0x0000070C System.Void OVR.OpenVR.IVRCompositor__HideMirrorWindow::Invoke()
extern void _HideMirrorWindow_Invoke_mB608734921E20DDDAF3422644C0FCADA96AF1BE6 ();
// 0x0000070D System.IAsyncResult OVR.OpenVR.IVRCompositor__HideMirrorWindow::BeginInvoke(System.AsyncCallback,System.Object)
extern void _HideMirrorWindow_BeginInvoke_m864852A53D023CB02B95EB3CAA298E1DCDF9D935 ();
// 0x0000070E System.Void OVR.OpenVR.IVRCompositor__HideMirrorWindow::EndInvoke(System.IAsyncResult)
extern void _HideMirrorWindow_EndInvoke_mDF5C2C7986BECF62EAFBE21B815575DBE74D5BED ();
// 0x0000070F System.Void OVR.OpenVR.IVRCompositor__IsMirrorWindowVisible::.ctor(System.Object,System.IntPtr)
extern void _IsMirrorWindowVisible__ctor_mDE2B653D06FD58E8A674F09800CA20751781C08B ();
// 0x00000710 System.Boolean OVR.OpenVR.IVRCompositor__IsMirrorWindowVisible::Invoke()
extern void _IsMirrorWindowVisible_Invoke_mF024D7CB0E96FE49BB680454804501E83166FD7D ();
// 0x00000711 System.IAsyncResult OVR.OpenVR.IVRCompositor__IsMirrorWindowVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsMirrorWindowVisible_BeginInvoke_m45F6BC59676284C52E5697BEF7E0F18440A3270F ();
// 0x00000712 System.Boolean OVR.OpenVR.IVRCompositor__IsMirrorWindowVisible::EndInvoke(System.IAsyncResult)
extern void _IsMirrorWindowVisible_EndInvoke_m7C4A2CFFF14E12D56553524D3ACA0FFD49F6A6C5 ();
// 0x00000713 System.Void OVR.OpenVR.IVRCompositor__CompositorDumpImages::.ctor(System.Object,System.IntPtr)
extern void _CompositorDumpImages__ctor_mD752528C2B252F03CB3F96F5F2B1BE66DC3BC642 ();
// 0x00000714 System.Void OVR.OpenVR.IVRCompositor__CompositorDumpImages::Invoke()
extern void _CompositorDumpImages_Invoke_m57D81410074AA451FC037F438F66E9ACF9A59D77 ();
// 0x00000715 System.IAsyncResult OVR.OpenVR.IVRCompositor__CompositorDumpImages::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CompositorDumpImages_BeginInvoke_m28663B8C722425B3D864E512D559F556EAF7AF05 ();
// 0x00000716 System.Void OVR.OpenVR.IVRCompositor__CompositorDumpImages::EndInvoke(System.IAsyncResult)
extern void _CompositorDumpImages_EndInvoke_m4544FF4A5C4E806556A255FC0E0776472368EDE2 ();
// 0x00000717 System.Void OVR.OpenVR.IVRCompositor__ShouldAppRenderWithLowResources::.ctor(System.Object,System.IntPtr)
extern void _ShouldAppRenderWithLowResources__ctor_m3DE2405B381509238F57149A2D2FA2003B8275D2 ();
// 0x00000718 System.Boolean OVR.OpenVR.IVRCompositor__ShouldAppRenderWithLowResources::Invoke()
extern void _ShouldAppRenderWithLowResources_Invoke_m963EAAA9AC894F9675664B6BCE07CA16EED2AF5A ();
// 0x00000719 System.IAsyncResult OVR.OpenVR.IVRCompositor__ShouldAppRenderWithLowResources::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ShouldAppRenderWithLowResources_BeginInvoke_m37B50EDA952B974BB2F4D3992653933EF71F27A1 ();
// 0x0000071A System.Boolean OVR.OpenVR.IVRCompositor__ShouldAppRenderWithLowResources::EndInvoke(System.IAsyncResult)
extern void _ShouldAppRenderWithLowResources_EndInvoke_mA167707E8EA8D1575BB6BD0E71A8154C859880E3 ();
// 0x0000071B System.Void OVR.OpenVR.IVRCompositor__ForceInterleavedReprojectionOn::.ctor(System.Object,System.IntPtr)
extern void _ForceInterleavedReprojectionOn__ctor_mC5B36680C4893743D0BFEE8359A0566725980827 ();
// 0x0000071C System.Void OVR.OpenVR.IVRCompositor__ForceInterleavedReprojectionOn::Invoke(System.Boolean)
extern void _ForceInterleavedReprojectionOn_Invoke_m16B335C44D42C0B3E33A6C15B6FE9CAB790924DD ();
// 0x0000071D System.IAsyncResult OVR.OpenVR.IVRCompositor__ForceInterleavedReprojectionOn::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void _ForceInterleavedReprojectionOn_BeginInvoke_mCD73D97F59B524D590BC7DB21645CA84AF4A0DB7 ();
// 0x0000071E System.Void OVR.OpenVR.IVRCompositor__ForceInterleavedReprojectionOn::EndInvoke(System.IAsyncResult)
extern void _ForceInterleavedReprojectionOn_EndInvoke_mDA87D992E44053B84622D7CC41010C816F39AF8F ();
// 0x0000071F System.Void OVR.OpenVR.IVRCompositor__ForceReconnectProcess::.ctor(System.Object,System.IntPtr)
extern void _ForceReconnectProcess__ctor_m279653E525B2C4F42DB108A3B40DD80ECC5DD77A ();
// 0x00000720 System.Void OVR.OpenVR.IVRCompositor__ForceReconnectProcess::Invoke()
extern void _ForceReconnectProcess_Invoke_m0DCD4574C2C081C093F7AE37AF76D79A8ECA1C26 ();
// 0x00000721 System.IAsyncResult OVR.OpenVR.IVRCompositor__ForceReconnectProcess::BeginInvoke(System.AsyncCallback,System.Object)
extern void _ForceReconnectProcess_BeginInvoke_mCBE137FDD0CE9D59D884632918DC2F6C18E98295 ();
// 0x00000722 System.Void OVR.OpenVR.IVRCompositor__ForceReconnectProcess::EndInvoke(System.IAsyncResult)
extern void _ForceReconnectProcess_EndInvoke_mD8DBE4D2A0213C7B394D813DE908FB08CCBD0ED8 ();
// 0x00000723 System.Void OVR.OpenVR.IVRCompositor__SuspendRendering::.ctor(System.Object,System.IntPtr)
extern void _SuspendRendering__ctor_mF239A966EC5BDD81A77FA7EEDA324F3EB08426B2 ();
// 0x00000724 System.Void OVR.OpenVR.IVRCompositor__SuspendRendering::Invoke(System.Boolean)
extern void _SuspendRendering_Invoke_m1662633A3707BEA5E8DA85E32A9528E2BA4AD048 ();
// 0x00000725 System.IAsyncResult OVR.OpenVR.IVRCompositor__SuspendRendering::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void _SuspendRendering_BeginInvoke_m6692B9ED057AC079C0B4B5C264CE3CDBD2347EF2 ();
// 0x00000726 System.Void OVR.OpenVR.IVRCompositor__SuspendRendering::EndInvoke(System.IAsyncResult)
extern void _SuspendRendering_EndInvoke_m57C3F54D5CBBF88F17BEC414E0B911E1E5476583 ();
// 0x00000727 System.Void OVR.OpenVR.IVRCompositor__GetMirrorTextureD3D11::.ctor(System.Object,System.IntPtr)
extern void _GetMirrorTextureD3D11__ctor_m456D35BF675AF50B90049D310E02385BCEE54C7C ();
// 0x00000728 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetMirrorTextureD3D11::Invoke(OVR.OpenVR.EVREye,System.IntPtr,System.IntPtr&)
extern void _GetMirrorTextureD3D11_Invoke_m4C9675D66FADFD1BD79A2F3530BB47CC72F1637A ();
// 0x00000729 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetMirrorTextureD3D11::BeginInvoke(OVR.OpenVR.EVREye,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void _GetMirrorTextureD3D11_BeginInvoke_m97785A53DDB4CE10B54529DB1C2F8BB628586A09 ();
// 0x0000072A OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetMirrorTextureD3D11::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void _GetMirrorTextureD3D11_EndInvoke_m2AE5BB82682EA9276E104F5B908A9594B071228F ();
// 0x0000072B System.Void OVR.OpenVR.IVRCompositor__ReleaseMirrorTextureD3D11::.ctor(System.Object,System.IntPtr)
extern void _ReleaseMirrorTextureD3D11__ctor_m822DEC7AE6F088CDFE9888BD82A76C1A65EFE9D5 ();
// 0x0000072C System.Void OVR.OpenVR.IVRCompositor__ReleaseMirrorTextureD3D11::Invoke(System.IntPtr)
extern void _ReleaseMirrorTextureD3D11_Invoke_m5153EBA85339FD3430C1072A6C09C8805EED4967 ();
// 0x0000072D System.IAsyncResult OVR.OpenVR.IVRCompositor__ReleaseMirrorTextureD3D11::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _ReleaseMirrorTextureD3D11_BeginInvoke_m528B859522562B2327FFC5F73C3D1021B0EECBE6 ();
// 0x0000072E System.Void OVR.OpenVR.IVRCompositor__ReleaseMirrorTextureD3D11::EndInvoke(System.IAsyncResult)
extern void _ReleaseMirrorTextureD3D11_EndInvoke_m9370C6FD9C6FA81C607AE15A6CABD8E7F8D5D578 ();
// 0x0000072F System.Void OVR.OpenVR.IVRCompositor__GetMirrorTextureGL::.ctor(System.Object,System.IntPtr)
extern void _GetMirrorTextureGL__ctor_m534390FBFE638AB956263C217373FE3C14964EAF ();
// 0x00000730 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetMirrorTextureGL::Invoke(OVR.OpenVR.EVREye,System.UInt32&,System.IntPtr)
extern void _GetMirrorTextureGL_Invoke_m44DE609C5211156C80881CD2F0931C55958AF17C ();
// 0x00000731 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetMirrorTextureGL::BeginInvoke(OVR.OpenVR.EVREye,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void _GetMirrorTextureGL_BeginInvoke_mF1D3A6626BDC7411D406B6B8F8285CB1CA4DAD9D ();
// 0x00000732 OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__GetMirrorTextureGL::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetMirrorTextureGL_EndInvoke_m789E72A0B444426B8C0DE935A44379471734859B ();
// 0x00000733 System.Void OVR.OpenVR.IVRCompositor__ReleaseSharedGLTexture::.ctor(System.Object,System.IntPtr)
extern void _ReleaseSharedGLTexture__ctor_mFA5B7A341DB471B0F0A750A0D8E39734023A2F60 ();
// 0x00000734 System.Boolean OVR.OpenVR.IVRCompositor__ReleaseSharedGLTexture::Invoke(System.UInt32,System.IntPtr)
extern void _ReleaseSharedGLTexture_Invoke_m70578617E6FDA78906286EDAF41409A25B6A44A9 ();
// 0x00000735 System.IAsyncResult OVR.OpenVR.IVRCompositor__ReleaseSharedGLTexture::BeginInvoke(System.UInt32,System.IntPtr,System.AsyncCallback,System.Object)
extern void _ReleaseSharedGLTexture_BeginInvoke_m1915D3A6B08AB4DA4CC1D41A2D67FD83668C701E ();
// 0x00000736 System.Boolean OVR.OpenVR.IVRCompositor__ReleaseSharedGLTexture::EndInvoke(System.IAsyncResult)
extern void _ReleaseSharedGLTexture_EndInvoke_mBFEF244142158D3FC0361DEFF76895D8520B17C8 ();
// 0x00000737 System.Void OVR.OpenVR.IVRCompositor__LockGLSharedTextureForAccess::.ctor(System.Object,System.IntPtr)
extern void _LockGLSharedTextureForAccess__ctor_mDBB08C95F8EE3B64EF63BA3F899A227BC68CA2ED ();
// 0x00000738 System.Void OVR.OpenVR.IVRCompositor__LockGLSharedTextureForAccess::Invoke(System.IntPtr)
extern void _LockGLSharedTextureForAccess_Invoke_mFC8458381FDB15B8A3891040D9A208045D77B221 ();
// 0x00000739 System.IAsyncResult OVR.OpenVR.IVRCompositor__LockGLSharedTextureForAccess::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _LockGLSharedTextureForAccess_BeginInvoke_m25EE9DD2F8AF689B33D1BD1BD045515594217529 ();
// 0x0000073A System.Void OVR.OpenVR.IVRCompositor__LockGLSharedTextureForAccess::EndInvoke(System.IAsyncResult)
extern void _LockGLSharedTextureForAccess_EndInvoke_mB4C1035BA747E7F478082BB8AA7536575C38EBE3 ();
// 0x0000073B System.Void OVR.OpenVR.IVRCompositor__UnlockGLSharedTextureForAccess::.ctor(System.Object,System.IntPtr)
extern void _UnlockGLSharedTextureForAccess__ctor_mAA33A92A8A6618DA2E5DE7588B176ED4FD80DFAC ();
// 0x0000073C System.Void OVR.OpenVR.IVRCompositor__UnlockGLSharedTextureForAccess::Invoke(System.IntPtr)
extern void _UnlockGLSharedTextureForAccess_Invoke_mE7B8FAF8C95B1AD683A2D40A797F15D6B6802A8B ();
// 0x0000073D System.IAsyncResult OVR.OpenVR.IVRCompositor__UnlockGLSharedTextureForAccess::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _UnlockGLSharedTextureForAccess_BeginInvoke_mFFB8E2AE4A92F06D8D7127C09A456D1B428FCBFF ();
// 0x0000073E System.Void OVR.OpenVR.IVRCompositor__UnlockGLSharedTextureForAccess::EndInvoke(System.IAsyncResult)
extern void _UnlockGLSharedTextureForAccess_EndInvoke_m1232681DA829AF27795C16CBCCE1897663211613 ();
// 0x0000073F System.Void OVR.OpenVR.IVRCompositor__GetVulkanInstanceExtensionsRequired::.ctor(System.Object,System.IntPtr)
extern void _GetVulkanInstanceExtensionsRequired__ctor_m534F82C3D5C2C20B4CF277980C23AAB5DBB5DD94 ();
// 0x00000740 System.UInt32 OVR.OpenVR.IVRCompositor__GetVulkanInstanceExtensionsRequired::Invoke(System.Text.StringBuilder,System.UInt32)
extern void _GetVulkanInstanceExtensionsRequired_Invoke_mDEC7117F416B709BDA43B0068E268EA1D31AA88C ();
// 0x00000741 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetVulkanInstanceExtensionsRequired::BeginInvoke(System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetVulkanInstanceExtensionsRequired_BeginInvoke_m2DE231574362C58F7F1127F13C5B915A161C94AE ();
// 0x00000742 System.UInt32 OVR.OpenVR.IVRCompositor__GetVulkanInstanceExtensionsRequired::EndInvoke(System.IAsyncResult)
extern void _GetVulkanInstanceExtensionsRequired_EndInvoke_m9AB3ACF3D3679F7A64619AF1384A79C8C5ADC303 ();
// 0x00000743 System.Void OVR.OpenVR.IVRCompositor__GetVulkanDeviceExtensionsRequired::.ctor(System.Object,System.IntPtr)
extern void _GetVulkanDeviceExtensionsRequired__ctor_mC4F0112DFA6ACFE3CC58D7FFFE36096CBFC57A96 ();
// 0x00000744 System.UInt32 OVR.OpenVR.IVRCompositor__GetVulkanDeviceExtensionsRequired::Invoke(System.IntPtr,System.Text.StringBuilder,System.UInt32)
extern void _GetVulkanDeviceExtensionsRequired_Invoke_mCA72D0934D95CBC1E4CB4185C575BB71670B75A0 ();
// 0x00000745 System.IAsyncResult OVR.OpenVR.IVRCompositor__GetVulkanDeviceExtensionsRequired::BeginInvoke(System.IntPtr,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetVulkanDeviceExtensionsRequired_BeginInvoke_mAB1C9CB38613EDA087D1D70A18A53BBCDB917EAB ();
// 0x00000746 System.UInt32 OVR.OpenVR.IVRCompositor__GetVulkanDeviceExtensionsRequired::EndInvoke(System.IAsyncResult)
extern void _GetVulkanDeviceExtensionsRequired_EndInvoke_m78A426F7867665920CFA81B715C1D4EC6C9BDF46 ();
// 0x00000747 System.Void OVR.OpenVR.IVRCompositor__SetExplicitTimingMode::.ctor(System.Object,System.IntPtr)
extern void _SetExplicitTimingMode__ctor_m47DF6468A33564ACBB62E7B7445254359799D7AC ();
// 0x00000748 System.Void OVR.OpenVR.IVRCompositor__SetExplicitTimingMode::Invoke(OVR.OpenVR.EVRCompositorTimingMode)
extern void _SetExplicitTimingMode_Invoke_m0F0B001BE2CA7A0572048F032BD77ED14A3B6CEA ();
// 0x00000749 System.IAsyncResult OVR.OpenVR.IVRCompositor__SetExplicitTimingMode::BeginInvoke(OVR.OpenVR.EVRCompositorTimingMode,System.AsyncCallback,System.Object)
extern void _SetExplicitTimingMode_BeginInvoke_m5CBCF63EA94F2CB59E6B8CD5743DEEC65CC09EE5 ();
// 0x0000074A System.Void OVR.OpenVR.IVRCompositor__SetExplicitTimingMode::EndInvoke(System.IAsyncResult)
extern void _SetExplicitTimingMode_EndInvoke_m616CEA81448E3AD0FD89832029CFC2175B297907 ();
// 0x0000074B System.Void OVR.OpenVR.IVRCompositor__SubmitExplicitTimingData::.ctor(System.Object,System.IntPtr)
extern void _SubmitExplicitTimingData__ctor_mBA14AD183D223E6BF14427F0654E41D846A569AC ();
// 0x0000074C OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__SubmitExplicitTimingData::Invoke()
extern void _SubmitExplicitTimingData_Invoke_mF86452DB54E6CA1C74B1F86E2516CF17134270AE ();
// 0x0000074D System.IAsyncResult OVR.OpenVR.IVRCompositor__SubmitExplicitTimingData::BeginInvoke(System.AsyncCallback,System.Object)
extern void _SubmitExplicitTimingData_BeginInvoke_mDB22BA92D66EC16D3C7698D3713C5C6CD8B6726B ();
// 0x0000074E OVR.OpenVR.EVRCompositorError OVR.OpenVR.IVRCompositor__SubmitExplicitTimingData::EndInvoke(System.IAsyncResult)
extern void _SubmitExplicitTimingData_EndInvoke_mA1A6095D31F795ACA17015AD4B8E4A2C793227EA ();
// 0x0000074F System.Void OVR.OpenVR.IVROverlay__FindOverlay::.ctor(System.Object,System.IntPtr)
extern void _FindOverlay__ctor_mB53AC0CB6B5FEAFDEA8106429163FC31EE0CBFE5 ();
// 0x00000750 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__FindOverlay::Invoke(System.String,System.UInt64&)
extern void _FindOverlay_Invoke_mE02774B83817848BFF986FEE7806194518C852DC ();
// 0x00000751 System.IAsyncResult OVR.OpenVR.IVROverlay__FindOverlay::BeginInvoke(System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern void _FindOverlay_BeginInvoke_m7105D85E69912160AD3876F198472C99D52A4486 ();
// 0x00000752 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__FindOverlay::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _FindOverlay_EndInvoke_mAA1DE7E95787AD01BF14A3A9E79E00D78ED4ED82 ();
// 0x00000753 System.Void OVR.OpenVR.IVROverlay__CreateOverlay::.ctor(System.Object,System.IntPtr)
extern void _CreateOverlay__ctor_m55DED3B47577CB127AD9F1C74D5CEB367A5C08F3 ();
// 0x00000754 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__CreateOverlay::Invoke(System.String,System.String,System.UInt64&)
extern void _CreateOverlay_Invoke_m190AF567CD1F78257B7A8F7C3F0C2F9506743131 ();
// 0x00000755 System.IAsyncResult OVR.OpenVR.IVROverlay__CreateOverlay::BeginInvoke(System.String,System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern void _CreateOverlay_BeginInvoke_mBB732923B7B0D6F8C2300C85E87BA1E7D4349130 ();
// 0x00000756 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__CreateOverlay::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _CreateOverlay_EndInvoke_mD4FE9F1413CC47C3DF0A908670094B9589A91C32 ();
// 0x00000757 System.Void OVR.OpenVR.IVROverlay__DestroyOverlay::.ctor(System.Object,System.IntPtr)
extern void _DestroyOverlay__ctor_m5638288DA040DAFE339A982463F40F48A2D145A3 ();
// 0x00000758 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__DestroyOverlay::Invoke(System.UInt64)
extern void _DestroyOverlay_Invoke_m1102137881BD6E84E74862F5CA002167F5792791 ();
// 0x00000759 System.IAsyncResult OVR.OpenVR.IVROverlay__DestroyOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _DestroyOverlay_BeginInvoke_m19AF85DFDE34B8430921A021C3DAD8175D4DCB42 ();
// 0x0000075A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__DestroyOverlay::EndInvoke(System.IAsyncResult)
extern void _DestroyOverlay_EndInvoke_m33644EDD02F600E4A1490CF8AD1B13135A000839 ();
// 0x0000075B System.Void OVR.OpenVR.IVROverlay__SetHighQualityOverlay::.ctor(System.Object,System.IntPtr)
extern void _SetHighQualityOverlay__ctor_m6B9CE6AF1B3F58F923D6B196132A082FA1BE333A ();
// 0x0000075C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetHighQualityOverlay::Invoke(System.UInt64)
extern void _SetHighQualityOverlay_Invoke_m24A186C0F31607CB036DE005C7D376BA3F42868D ();
// 0x0000075D System.IAsyncResult OVR.OpenVR.IVROverlay__SetHighQualityOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _SetHighQualityOverlay_BeginInvoke_mC509A0150324A1DFA4A313680DB414AD7B50A152 ();
// 0x0000075E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetHighQualityOverlay::EndInvoke(System.IAsyncResult)
extern void _SetHighQualityOverlay_EndInvoke_m785E4B9CE912C3C9FD7F6B541C0F71237312A0DC ();
// 0x0000075F System.Void OVR.OpenVR.IVROverlay__GetHighQualityOverlay::.ctor(System.Object,System.IntPtr)
extern void _GetHighQualityOverlay__ctor_m6D9E7DB16F8E65216D377C0AF7CFA12276426259 ();
// 0x00000760 System.UInt64 OVR.OpenVR.IVROverlay__GetHighQualityOverlay::Invoke()
extern void _GetHighQualityOverlay_Invoke_mF6097DE6B87B817504C60D9F12324B0B70DD6995 ();
// 0x00000761 System.IAsyncResult OVR.OpenVR.IVROverlay__GetHighQualityOverlay::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetHighQualityOverlay_BeginInvoke_m2F565230112779924CC98DABACE59CB129B70C72 ();
// 0x00000762 System.UInt64 OVR.OpenVR.IVROverlay__GetHighQualityOverlay::EndInvoke(System.IAsyncResult)
extern void _GetHighQualityOverlay_EndInvoke_m7D0DD2B83B08FC6A1C941F7D1843B554F4ADC7CA ();
// 0x00000763 System.Void OVR.OpenVR.IVROverlay__GetOverlayKey::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayKey__ctor_m93D07D420C817352847050FBD964B37F19B9D9F9 ();
// 0x00000764 System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayKey::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&)
extern void _GetOverlayKey_Invoke_m7FD52BEF6517F318DFA5F600943F58436BF8B491 ();
// 0x00000765 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayKey::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&,System.AsyncCallback,System.Object)
extern void _GetOverlayKey_BeginInvoke_m111E8354F1C2052EA648C3B0426FDDEF0FE437B2 ();
// 0x00000766 System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayKey::EndInvoke(OVR.OpenVR.EVROverlayError&,System.IAsyncResult)
extern void _GetOverlayKey_EndInvoke_mD28CC8D802074AB64BFBB8655FD12B8B00C1393F ();
// 0x00000767 System.Void OVR.OpenVR.IVROverlay__GetOverlayName::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayName__ctor_m87427FF8069D2A72D00A2B0CCBBF965DB6B99EF0 ();
// 0x00000768 System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayName::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&)
extern void _GetOverlayName_Invoke_m14110D6E9D89EA544FC677D09AE961FD040D8A0B ();
// 0x00000769 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayName::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&,System.AsyncCallback,System.Object)
extern void _GetOverlayName_BeginInvoke_mB774EA58B905EC77E5B41DD103CB426A87FB68AF ();
// 0x0000076A System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayName::EndInvoke(OVR.OpenVR.EVROverlayError&,System.IAsyncResult)
extern void _GetOverlayName_EndInvoke_mE541CD24FBC1D3B74EAC5D58E72FAE1D7F213657 ();
// 0x0000076B System.Void OVR.OpenVR.IVROverlay__SetOverlayName::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayName__ctor_m143F36A3FDDB61C5F54B967B580662BE4BB828EB ();
// 0x0000076C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayName::Invoke(System.UInt64,System.String)
extern void _SetOverlayName_Invoke_m2EE8E7AA60286F9A39C7836138C1AD745ECB7562 ();
// 0x0000076D System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayName::BeginInvoke(System.UInt64,System.String,System.AsyncCallback,System.Object)
extern void _SetOverlayName_BeginInvoke_m1260536336090DF9D407549F5EE92919880FDBC9 ();
// 0x0000076E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayName::EndInvoke(System.IAsyncResult)
extern void _SetOverlayName_EndInvoke_m46B997EF6F52F41E4FC713A4024FC68F9E619E09 ();
// 0x0000076F System.Void OVR.OpenVR.IVROverlay__GetOverlayImageData::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayImageData__ctor_m2DEE82A35BF3F2F39157C68C5ADC6A43F3229F9E ();
// 0x00000770 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayImageData::Invoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&,System.UInt32&)
extern void _GetOverlayImageData_Invoke_m1F824648C1AF729524CB0307CDB98E31F31CE0E7 ();
// 0x00000771 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayImageData::BeginInvoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetOverlayImageData_BeginInvoke_m29DC63EC936E2A652602D2301A314C015E9A894E ();
// 0x00000772 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayImageData::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetOverlayImageData_EndInvoke_m807D67067F516A322875BEABEEC83C2930B0A79E ();
// 0x00000773 System.Void OVR.OpenVR.IVROverlay__GetOverlayErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayErrorNameFromEnum__ctor_mFA76C7BCE6A4D14103E1155918F4FC36DDCF48C0 ();
// 0x00000774 System.IntPtr OVR.OpenVR.IVROverlay__GetOverlayErrorNameFromEnum::Invoke(OVR.OpenVR.EVROverlayError)
extern void _GetOverlayErrorNameFromEnum_Invoke_mC40A1C0DFAA0BA584CB8E6DE106A6DC046C6EB25 ();
// 0x00000775 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayErrorNameFromEnum::BeginInvoke(OVR.OpenVR.EVROverlayError,System.AsyncCallback,System.Object)
extern void _GetOverlayErrorNameFromEnum_BeginInvoke_m6404AA07CA6FD6A7B4ED5A818FE93ED90C9BE6BC ();
// 0x00000776 System.IntPtr OVR.OpenVR.IVROverlay__GetOverlayErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetOverlayErrorNameFromEnum_EndInvoke_mEA2082221719225700DA4780E50FF66C822E7728 ();
// 0x00000777 System.Void OVR.OpenVR.IVROverlay__SetOverlayRenderingPid::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayRenderingPid__ctor_m208990E6AF1BF1FFC72128047977316D5E935BA4 ();
// 0x00000778 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRenderingPid::Invoke(System.UInt64,System.UInt32)
extern void _SetOverlayRenderingPid_Invoke_m36D436056BD4E64760A24D3ADBC5C7A65A60C2C1 ();
// 0x00000779 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayRenderingPid::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern void _SetOverlayRenderingPid_BeginInvoke_m2D0A08F0235E9EAEA3BDC284D11ACBC334F31292 ();
// 0x0000077A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRenderingPid::EndInvoke(System.IAsyncResult)
extern void _SetOverlayRenderingPid_EndInvoke_m5D955F1C3B55BC8004B2947C8D1D89B299442BF1 ();
// 0x0000077B System.Void OVR.OpenVR.IVROverlay__GetOverlayRenderingPid::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayRenderingPid__ctor_m31C78B526A901F97F4BDBA1A8CCBBBCB2EF48FA7 ();
// 0x0000077C System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayRenderingPid::Invoke(System.UInt64)
extern void _GetOverlayRenderingPid_Invoke_m454EE872D9328F2D32597590EC1C659C08FCA301 ();
// 0x0000077D System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayRenderingPid::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _GetOverlayRenderingPid_BeginInvoke_m21AC9B0096E5EB18094D949778241807D6166A29 ();
// 0x0000077E System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayRenderingPid::EndInvoke(System.IAsyncResult)
extern void _GetOverlayRenderingPid_EndInvoke_mE0D11B4B11AD9332DD82F8B8656289AC1F91973E ();
// 0x0000077F System.Void OVR.OpenVR.IVROverlay__SetOverlayFlag::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayFlag__ctor_mCF62C541A6B13ABF5E435E07BB4445EC984053A6 ();
// 0x00000780 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayFlag::Invoke(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean)
extern void _SetOverlayFlag_Invoke_mC100C9B9B32D7C9DEDAB5A84FFCA4C8C25AA9416 ();
// 0x00000781 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayFlag::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean,System.AsyncCallback,System.Object)
extern void _SetOverlayFlag_BeginInvoke_m16A47643BE8CDB191C623AF955CFAF8F3AC762B6 ();
// 0x00000782 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayFlag::EndInvoke(System.IAsyncResult)
extern void _SetOverlayFlag_EndInvoke_mAF336419AC263E7064CB5846ACAE82BBFEB16508 ();
// 0x00000783 System.Void OVR.OpenVR.IVROverlay__GetOverlayFlag::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayFlag__ctor_m9B1531D53A1F6A8914EFF072FED19674DF90DDD5 ();
// 0x00000784 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayFlag::Invoke(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean&)
extern void _GetOverlayFlag_Invoke_mD4E3EC3BE1DBB598009F0B14AD2ADEBCF8EFF2BF ();
// 0x00000785 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayFlag::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean&,System.AsyncCallback,System.Object)
extern void _GetOverlayFlag_BeginInvoke_m7AD84D73F22583B9E0B61B30CBE79586998547F3 ();
// 0x00000786 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayFlag::EndInvoke(System.Boolean&,System.IAsyncResult)
extern void _GetOverlayFlag_EndInvoke_mA378EE01E2B0E8021007A463EF3F930C9B98A346 ();
// 0x00000787 System.Void OVR.OpenVR.IVROverlay__SetOverlayColor::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayColor__ctor_m00E65CDB79C3C2513A499615323F8AB6F9EF4381 ();
// 0x00000788 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayColor::Invoke(System.UInt64,System.Single,System.Single,System.Single)
extern void _SetOverlayColor_Invoke_mD3675E33B94A0441232372EC2DF73BB6C568D9A3 ();
// 0x00000789 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayColor::BeginInvoke(System.UInt64,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayColor_BeginInvoke_mE6696BDB4923C5E381812EB443368348099EBDD3 ();
// 0x0000078A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayColor::EndInvoke(System.IAsyncResult)
extern void _SetOverlayColor_EndInvoke_m0342B32DD37F1084DC801DBAD2BF0C4CCAE80AAF ();
// 0x0000078B System.Void OVR.OpenVR.IVROverlay__GetOverlayColor::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayColor__ctor_m7811850A5C9AC2A74043BFFED93C5E6F390A4220 ();
// 0x0000078C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayColor::Invoke(System.UInt64,System.Single&,System.Single&,System.Single&)
extern void _GetOverlayColor_Invoke_mF91B2A82A3F8FF074CD191894666E1776C6A9A1C ();
// 0x0000078D System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayColor::BeginInvoke(System.UInt64,System.Single&,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayColor_BeginInvoke_m11EE063DE1B961D14FD90FBFE2B96A9B7EE3C0F2 ();
// 0x0000078E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayColor::EndInvoke(System.Single&,System.Single&,System.Single&,System.IAsyncResult)
extern void _GetOverlayColor_EndInvoke_m4225F82B86EE08A909823724703CF986168F1752 ();
// 0x0000078F System.Void OVR.OpenVR.IVROverlay__SetOverlayAlpha::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayAlpha__ctor_m57A387C1140334B74206FFA24554DF5CDD6C8A63 ();
// 0x00000790 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayAlpha::Invoke(System.UInt64,System.Single)
extern void _SetOverlayAlpha_Invoke_mF7AADABD66172E31981E95EE9BCFFB87CED8792C ();
// 0x00000791 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayAlpha::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayAlpha_BeginInvoke_m7AD82DC7A96CB50E8A57D2FF278ADE177D478CE0 ();
// 0x00000792 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayAlpha::EndInvoke(System.IAsyncResult)
extern void _SetOverlayAlpha_EndInvoke_mE916B96080955CE40F3D57B108F4CF77055236D8 ();
// 0x00000793 System.Void OVR.OpenVR.IVROverlay__GetOverlayAlpha::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayAlpha__ctor_m67F033B2CE1D78CFEC938C9547826220238F73EA ();
// 0x00000794 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayAlpha::Invoke(System.UInt64,System.Single&)
extern void _GetOverlayAlpha_Invoke_m4421016686E7C302EA3799071D883599D89E7C98 ();
// 0x00000795 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayAlpha::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayAlpha_BeginInvoke_m9060E7DE9829C939A010BA2EB5D82138BABF46D9 ();
// 0x00000796 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayAlpha::EndInvoke(System.Single&,System.IAsyncResult)
extern void _GetOverlayAlpha_EndInvoke_m741E781AA25E9ADC691A901120965891D2DEEB0D ();
// 0x00000797 System.Void OVR.OpenVR.IVROverlay__SetOverlayTexelAspect::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTexelAspect__ctor_m2379A78B80CD16B1C7066975ED6F1048DEF39B30 ();
// 0x00000798 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTexelAspect::Invoke(System.UInt64,System.Single)
extern void _SetOverlayTexelAspect_Invoke_m59F06EA5BEF1FB42E3293EAB1FDD687B58D81E50 ();
// 0x00000799 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTexelAspect::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayTexelAspect_BeginInvoke_mAB6C9EC756B20E1987BBFF88DDF5FE5F26C88DFE ();
// 0x0000079A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTexelAspect::EndInvoke(System.IAsyncResult)
extern void _SetOverlayTexelAspect_EndInvoke_m512C112E0658EE4322B864CC3144A8A80B040F4E ();
// 0x0000079B System.Void OVR.OpenVR.IVROverlay__GetOverlayTexelAspect::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTexelAspect__ctor_m78B60F35894AF8B3A0F328FA37C5F52A2545F52A ();
// 0x0000079C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTexelAspect::Invoke(System.UInt64,System.Single&)
extern void _GetOverlayTexelAspect_Invoke_m5E269C5D0716B33AE6501B3882F66AA3151C46FF ();
// 0x0000079D System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTexelAspect::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayTexelAspect_BeginInvoke_m7B76B611C367D2C7AC8682887B7AD2618253A966 ();
// 0x0000079E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTexelAspect::EndInvoke(System.Single&,System.IAsyncResult)
extern void _GetOverlayTexelAspect_EndInvoke_m452457E1B7E6290ED4A2886AA86700EF6558A5E9 ();
// 0x0000079F System.Void OVR.OpenVR.IVROverlay__SetOverlaySortOrder::.ctor(System.Object,System.IntPtr)
extern void _SetOverlaySortOrder__ctor_mBF3D3EA67BF13832356593D79C66C2F2B3C460AB ();
// 0x000007A0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlaySortOrder::Invoke(System.UInt64,System.UInt32)
extern void _SetOverlaySortOrder_Invoke_m0B56E887B2EDC048E6B2FAC9541E15AB702A30B6 ();
// 0x000007A1 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlaySortOrder::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern void _SetOverlaySortOrder_BeginInvoke_m7DF28B55358B7D5FEBE0251725443BED355B2A51 ();
// 0x000007A2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlaySortOrder::EndInvoke(System.IAsyncResult)
extern void _SetOverlaySortOrder_EndInvoke_m44F47E1563F018EE55F8148A6C4FE6DFCEC9251D ();
// 0x000007A3 System.Void OVR.OpenVR.IVROverlay__GetOverlaySortOrder::.ctor(System.Object,System.IntPtr)
extern void _GetOverlaySortOrder__ctor_mEBD01FD558315659B29751A951D667FBDEA9199A ();
// 0x000007A4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlaySortOrder::Invoke(System.UInt64,System.UInt32&)
extern void _GetOverlaySortOrder_Invoke_mB5C50D760D60425B779EC9E52DABD9C65DCE29E5 ();
// 0x000007A5 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlaySortOrder::BeginInvoke(System.UInt64,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetOverlaySortOrder_BeginInvoke_mC015C907326CB97D7B7B94748D3E68C7C7BE3A2F ();
// 0x000007A6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlaySortOrder::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetOverlaySortOrder_EndInvoke_mFCD9FBB42DECD1F53F43F3232E1C92025F77C656 ();
// 0x000007A7 System.Void OVR.OpenVR.IVROverlay__SetOverlayWidthInMeters::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayWidthInMeters__ctor_mD85DF2CE00A92C7D31C81925469EADFB33E502D8 ();
// 0x000007A8 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayWidthInMeters::Invoke(System.UInt64,System.Single)
extern void _SetOverlayWidthInMeters_Invoke_mA94F390AEFDA255452F3DAA15BFF897946D863F9 ();
// 0x000007A9 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayWidthInMeters::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayWidthInMeters_BeginInvoke_m49EF54A069EAD34F80C138B7F6521260A3BB767D ();
// 0x000007AA OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayWidthInMeters::EndInvoke(System.IAsyncResult)
extern void _SetOverlayWidthInMeters_EndInvoke_m7E9B0F8344F4AEB10CCDFF30A9ECEB7673A0335F ();
// 0x000007AB System.Void OVR.OpenVR.IVROverlay__GetOverlayWidthInMeters::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayWidthInMeters__ctor_m304495438EE54CF31BF2C07B7371B5AC7D4A40D5 ();
// 0x000007AC OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayWidthInMeters::Invoke(System.UInt64,System.Single&)
extern void _GetOverlayWidthInMeters_Invoke_m06AB42114E62F94041035113CEC126169B9F742C ();
// 0x000007AD System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayWidthInMeters::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayWidthInMeters_BeginInvoke_mBF281FBE5BEBE75DFCF67E1066BE0FBC12AE357F ();
// 0x000007AE OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayWidthInMeters::EndInvoke(System.Single&,System.IAsyncResult)
extern void _GetOverlayWidthInMeters_EndInvoke_m762D47E505F52570996F4141BA8CD3015FA3AD1F ();
// 0x000007AF System.Void OVR.OpenVR.IVROverlay__SetOverlayAutoCurveDistanceRangeInMeters::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayAutoCurveDistanceRangeInMeters__ctor_mE640AE7C3D6EF81C7E6FBD0195E85C8DF8ABF830 ();
// 0x000007B0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayAutoCurveDistanceRangeInMeters::Invoke(System.UInt64,System.Single,System.Single)
extern void _SetOverlayAutoCurveDistanceRangeInMeters_Invoke_m5FEE202093D38BDE33A2D83B4C44ABBFFD09EBD0 ();
// 0x000007B1 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayAutoCurveDistanceRangeInMeters::BeginInvoke(System.UInt64,System.Single,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m6785CF72D5C1A63A2B8E1678C03A7102B4E81479 ();
// 0x000007B2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayAutoCurveDistanceRangeInMeters::EndInvoke(System.IAsyncResult)
extern void _SetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m103B9A14D20374D8BF7E94548EE28CEEF1F8CE49 ();
// 0x000007B3 System.Void OVR.OpenVR.IVROverlay__GetOverlayAutoCurveDistanceRangeInMeters::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayAutoCurveDistanceRangeInMeters__ctor_m608EC745E125CF9E386071464A69CF3A113DEAAA ();
// 0x000007B4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayAutoCurveDistanceRangeInMeters::Invoke(System.UInt64,System.Single&,System.Single&)
extern void _GetOverlayAutoCurveDistanceRangeInMeters_Invoke_mAD22FCF45D35041EAF32CD68F6B6DAB89436BB84 ();
// 0x000007B5 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayAutoCurveDistanceRangeInMeters::BeginInvoke(System.UInt64,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m042F49CB2E487AD7E51DFC27B87B3DB1841BD3FC ();
// 0x000007B6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayAutoCurveDistanceRangeInMeters::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern void _GetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m674A719317C6A7FEFBB47D7D6CA56128148D6EB7 ();
// 0x000007B7 System.Void OVR.OpenVR.IVROverlay__SetOverlayTextureColorSpace::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTextureColorSpace__ctor_mFBBEAC70DACB5F3612F7DC3968C7D9575344AB90 ();
// 0x000007B8 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTextureColorSpace::Invoke(System.UInt64,OVR.OpenVR.EColorSpace)
extern void _SetOverlayTextureColorSpace_Invoke_m17ABCAF3ABA7D99D51C3BAF2D871C565C1B1629E ();
// 0x000007B9 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTextureColorSpace::BeginInvoke(System.UInt64,OVR.OpenVR.EColorSpace,System.AsyncCallback,System.Object)
extern void _SetOverlayTextureColorSpace_BeginInvoke_mEF30ACAF99983513800FB0C92A27F61A637DED40 ();
// 0x000007BA OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTextureColorSpace::EndInvoke(System.IAsyncResult)
extern void _SetOverlayTextureColorSpace_EndInvoke_m2A37B20782575226F4EC233B58CB3BC63806C557 ();
// 0x000007BB System.Void OVR.OpenVR.IVROverlay__GetOverlayTextureColorSpace::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTextureColorSpace__ctor_m556A9A1ADE2C4E7D3C80DBAC30EEB5B043C9A1C0 ();
// 0x000007BC OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureColorSpace::Invoke(System.UInt64,OVR.OpenVR.EColorSpace&)
extern void _GetOverlayTextureColorSpace_Invoke_m78D47252BA7F47B06949ACC12E9F4642438D986D ();
// 0x000007BD System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTextureColorSpace::BeginInvoke(System.UInt64,OVR.OpenVR.EColorSpace&,System.AsyncCallback,System.Object)
extern void _GetOverlayTextureColorSpace_BeginInvoke_m2AB2A0C45D6E250E67C889E3CB1053C79F8A42B6 ();
// 0x000007BE OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureColorSpace::EndInvoke(OVR.OpenVR.EColorSpace&,System.IAsyncResult)
extern void _GetOverlayTextureColorSpace_EndInvoke_m5DEA8534B6ADD81F14A2708EDBE3279BDD5552F1 ();
// 0x000007BF System.Void OVR.OpenVR.IVROverlay__SetOverlayTextureBounds::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTextureBounds__ctor_m656E0AC14F06717B615E465FE09659D3568E6AAD ();
// 0x000007C0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTextureBounds::Invoke(System.UInt64,OVR.OpenVR.VRTextureBounds_t&)
extern void _SetOverlayTextureBounds_Invoke_m8CEE8640189D0324713291AA599525078EE9FC4D ();
// 0x000007C1 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTextureBounds::BeginInvoke(System.UInt64,OVR.OpenVR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayTextureBounds_BeginInvoke_m7886C5E627301A1E5620F247EDF526456A9904A5 ();
// 0x000007C2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTextureBounds::EndInvoke(OVR.OpenVR.VRTextureBounds_t&,System.IAsyncResult)
extern void _SetOverlayTextureBounds_EndInvoke_mBD6546B72C19819EE4DFCDA737D26D37C5EF00D5 ();
// 0x000007C3 System.Void OVR.OpenVR.IVROverlay__GetOverlayTextureBounds::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTextureBounds__ctor_m117952B8F6622A6C6DBB509FB6C41231A71ECF77 ();
// 0x000007C4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureBounds::Invoke(System.UInt64,OVR.OpenVR.VRTextureBounds_t&)
extern void _GetOverlayTextureBounds_Invoke_m99CAB365F69A7595BABDD055B0FDD45CFB1EC6FF ();
// 0x000007C5 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTextureBounds::BeginInvoke(System.UInt64,OVR.OpenVR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayTextureBounds_BeginInvoke_m706C998272C772C00DCDEC1B4223EB49ED3FFDC5 ();
// 0x000007C6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureBounds::EndInvoke(OVR.OpenVR.VRTextureBounds_t&,System.IAsyncResult)
extern void _GetOverlayTextureBounds_EndInvoke_m67923B96DEEF8C7316F1C716253E4AC322B0DAE5 ();
// 0x000007C7 System.Void OVR.OpenVR.IVROverlay__GetOverlayRenderModel::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayRenderModel__ctor_m131B0D9CB462DFDFC180BC268E4F6D8904A68D83 ();
// 0x000007C8 System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayRenderModel::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.HmdColor_t&,OVR.OpenVR.EVROverlayError&)
extern void _GetOverlayRenderModel_Invoke_m59CA770EA890F9DEF7E8C25921CF0B1083C81E33 ();
// 0x000007C9 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayRenderModel::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.HmdColor_t&,OVR.OpenVR.EVROverlayError&,System.AsyncCallback,System.Object)
extern void _GetOverlayRenderModel_BeginInvoke_mBA2568EB1D02726E2C84E1C367D746DCAEC9C7F1 ();
// 0x000007CA System.UInt32 OVR.OpenVR.IVROverlay__GetOverlayRenderModel::EndInvoke(OVR.OpenVR.HmdColor_t&,OVR.OpenVR.EVROverlayError&,System.IAsyncResult)
extern void _GetOverlayRenderModel_EndInvoke_m43D87B75E847594AEDED894A75C698D63E9C0D01 ();
// 0x000007CB System.Void OVR.OpenVR.IVROverlay__SetOverlayRenderModel::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayRenderModel__ctor_mCCDEACEB1607786BE86A2EC09F675C4EEBEE1074 ();
// 0x000007CC OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRenderModel::Invoke(System.UInt64,System.String,OVR.OpenVR.HmdColor_t&)
extern void _SetOverlayRenderModel_Invoke_m90D73ACA2B2AC6C71DD4BCD545233BAEFE625EF9 ();
// 0x000007CD System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayRenderModel::BeginInvoke(System.UInt64,System.String,OVR.OpenVR.HmdColor_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayRenderModel_BeginInvoke_mE5730E829B2FDB0A6FDC1D162C44085B578AE520 ();
// 0x000007CE OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRenderModel::EndInvoke(OVR.OpenVR.HmdColor_t&,System.IAsyncResult)
extern void _SetOverlayRenderModel_EndInvoke_m64C447CF77DD5F3860B7236F2F2E08F005B419C1 ();
// 0x000007CF System.Void OVR.OpenVR.IVROverlay__GetOverlayTransformType::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTransformType__ctor_mA49A7C0686989CDD06796DD10A75235E8001D5EC ();
// 0x000007D0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformType::Invoke(System.UInt64,OVR.OpenVR.VROverlayTransformType&)
extern void _GetOverlayTransformType_Invoke_mE531908BBA6C7E0623BB2623123A3D95E0A616DE ();
// 0x000007D1 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTransformType::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayTransformType&,System.AsyncCallback,System.Object)
extern void _GetOverlayTransformType_BeginInvoke_mAC95BF02580E2AF9C5018C6CE9FA62453FE2134C ();
// 0x000007D2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformType::EndInvoke(OVR.OpenVR.VROverlayTransformType&,System.IAsyncResult)
extern void _GetOverlayTransformType_EndInvoke_mE941F699D8230F43A258E440FF00843DE082DE1B ();
// 0x000007D3 System.Void OVR.OpenVR.IVROverlay__SetOverlayTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTransformAbsolute__ctor_m108099F42D094EE71AA867B3EA5A0693AB0995E8 ();
// 0x000007D4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformAbsolute::Invoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&)
extern void _SetOverlayTransformAbsolute_Invoke_mB2CBA24D053535E67CF91F39097430511AD2E375 ();
// 0x000007D5 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTransformAbsolute::BeginInvoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayTransformAbsolute_BeginInvoke_m33DAB6F0B166AB245C224BBCC95AB66698D9408D ();
// 0x000007D6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformAbsolute::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetOverlayTransformAbsolute_EndInvoke_mA64E91C99395DF20F12160850B63F00DEBB20BB8 ();
// 0x000007D7 System.Void OVR.OpenVR.IVROverlay__GetOverlayTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTransformAbsolute__ctor_mD3F26F98138830E6BAE68A46377F7AE0182B483F ();
// 0x000007D8 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformAbsolute::Invoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin&,OVR.OpenVR.HmdMatrix34_t&)
extern void _GetOverlayTransformAbsolute_Invoke_m0CC2FC99AEA5CD7BE60BAC823E02E12821098194 ();
// 0x000007D9 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTransformAbsolute::BeginInvoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin&,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayTransformAbsolute_BeginInvoke_m70CF17AA58C689C64514DED6938D1D7A30397562 ();
// 0x000007DA OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformAbsolute::EndInvoke(OVR.OpenVR.ETrackingUniverseOrigin&,OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetOverlayTransformAbsolute_EndInvoke_mCCFEF3795A57E904881A821DA4AB25B457935352 ();
// 0x000007DB System.Void OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceRelative::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTransformTrackedDeviceRelative__ctor_mCDC75AC04C0977EF7D7D48B91DB5AA9108FD6E12 ();
// 0x000007DC OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceRelative::Invoke(System.UInt64,System.UInt32,OVR.OpenVR.HmdMatrix34_t&)
extern void _SetOverlayTransformTrackedDeviceRelative_Invoke_m19748CA3F1E41869B8293F86CA9C8F9CAACEE75D ();
// 0x000007DD System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceRelative::BeginInvoke(System.UInt64,System.UInt32,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m8C8752CC0FF282CED2599FEDD7DD4E3DD0FFBE8D ();
// 0x000007DE OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceRelative::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetOverlayTransformTrackedDeviceRelative_EndInvoke_m10E570B364B0C470039866CEBAAC4C826E3DAD03 ();
// 0x000007DF System.Void OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceRelative::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTransformTrackedDeviceRelative__ctor_mAFC6632B40E8A2A969B29AC94E0335A5D97CCE05 ();
// 0x000007E0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceRelative::Invoke(System.UInt64,System.UInt32&,OVR.OpenVR.HmdMatrix34_t&)
extern void _GetOverlayTransformTrackedDeviceRelative_Invoke_mE3C28FD930746EF522C32B180EDC88CE18364B8B ();
// 0x000007E1 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceRelative::BeginInvoke(System.UInt64,System.UInt32&,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayTransformTrackedDeviceRelative_BeginInvoke_m966057E62C74C4AE424B9AE73E83AEAF89A4DD37 ();
// 0x000007E2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceRelative::EndInvoke(System.UInt32&,OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetOverlayTransformTrackedDeviceRelative_EndInvoke_m9DE4A0EE407F5BB3A6C0B64DA1890060844375C4 ();
// 0x000007E3 System.Void OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceComponent::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTransformTrackedDeviceComponent__ctor_m2FBE03B08561FB0CBB388E5F752B4CD40449DE49 ();
// 0x000007E4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceComponent::Invoke(System.UInt64,System.UInt32,System.String)
extern void _SetOverlayTransformTrackedDeviceComponent_Invoke_mB4338E5F742B290D552F5899DF6A4FFB6FF817BB ();
// 0x000007E5 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceComponent::BeginInvoke(System.UInt64,System.UInt32,System.String,System.AsyncCallback,System.Object)
extern void _SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m42B48EB65F835D3CBC3A82FAC934D982174F2551 ();
// 0x000007E6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformTrackedDeviceComponent::EndInvoke(System.IAsyncResult)
extern void _SetOverlayTransformTrackedDeviceComponent_EndInvoke_mEF105683A1B1AF8848D410122786727F69963AFA ();
// 0x000007E7 System.Void OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceComponent::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTransformTrackedDeviceComponent__ctor_mB0204C812D0E606DB1747DF54CD3C353B8C13857 ();
// 0x000007E8 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceComponent::Invoke(System.UInt64,System.UInt32&,System.Text.StringBuilder,System.UInt32)
extern void _GetOverlayTransformTrackedDeviceComponent_Invoke_m7119F93CCA3687C39EEC7551EA83DF303B4A16D2 ();
// 0x000007E9 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceComponent::BeginInvoke(System.UInt64,System.UInt32&,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetOverlayTransformTrackedDeviceComponent_BeginInvoke_mE6F14C4202572FFCE2E3AB9A60E524A0383B403F ();
// 0x000007EA OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformTrackedDeviceComponent::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetOverlayTransformTrackedDeviceComponent_EndInvoke_m75C510F88E290C84CDEEACB91CA528A50F2C64E2 ();
// 0x000007EB System.Void OVR.OpenVR.IVROverlay__GetOverlayTransformOverlayRelative::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTransformOverlayRelative__ctor_mCD6481083C6C55DA3466C52205C37C9734D333D8 ();
// 0x000007EC OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformOverlayRelative::Invoke(System.UInt64,System.UInt64&,OVR.OpenVR.HmdMatrix34_t&)
extern void _GetOverlayTransformOverlayRelative_Invoke_m5F0A5E0F53137B6B99BD9058EB213C928F79E39D ();
// 0x000007ED System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTransformOverlayRelative::BeginInvoke(System.UInt64,System.UInt64&,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayTransformOverlayRelative_BeginInvoke_m9A26A6482056E88945A1475C65D5DED62225E1AC ();
// 0x000007EE OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTransformOverlayRelative::EndInvoke(System.UInt64&,OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetOverlayTransformOverlayRelative_EndInvoke_mD1C773620C83FF03B7CDC4986A5C9E0839B8CD8E ();
// 0x000007EF System.Void OVR.OpenVR.IVROverlay__SetOverlayTransformOverlayRelative::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTransformOverlayRelative__ctor_mE934C6C3692DBF614BB31F730A0463037C06269D ();
// 0x000007F0 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformOverlayRelative::Invoke(System.UInt64,System.UInt64,OVR.OpenVR.HmdMatrix34_t&)
extern void _SetOverlayTransformOverlayRelative_Invoke_m0745182BCA88A074CBFDA9C2B60240370A644642 ();
// 0x000007F1 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTransformOverlayRelative::BeginInvoke(System.UInt64,System.UInt64,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayTransformOverlayRelative_BeginInvoke_m2B19B22BC086BC0D59216D45D9325E5F192EF46D ();
// 0x000007F2 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTransformOverlayRelative::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetOverlayTransformOverlayRelative_EndInvoke_m5483F0BF24D6CA462C5867D610C1AAFE096A57A8 ();
// 0x000007F3 System.Void OVR.OpenVR.IVROverlay__ShowOverlay::.ctor(System.Object,System.IntPtr)
extern void _ShowOverlay__ctor_m65932229E3A0B1C7EA09FBE166864D8C78680779 ();
// 0x000007F4 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowOverlay::Invoke(System.UInt64)
extern void _ShowOverlay_Invoke_mEE7AC9B31BB7A4DA3251E3070BB035AB9FD2263A ();
// 0x000007F5 System.IAsyncResult OVR.OpenVR.IVROverlay__ShowOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _ShowOverlay_BeginInvoke_m65DD94C585912C912AF0F5EE55D122DA940BA5BC ();
// 0x000007F6 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowOverlay::EndInvoke(System.IAsyncResult)
extern void _ShowOverlay_EndInvoke_mCE3C7B903D897A5F40DFDB0C1524612018D9E68D ();
// 0x000007F7 System.Void OVR.OpenVR.IVROverlay__HideOverlay::.ctor(System.Object,System.IntPtr)
extern void _HideOverlay__ctor_m33AD63B27F1EC1512F86E52977C4EA76CC24AA5C ();
// 0x000007F8 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__HideOverlay::Invoke(System.UInt64)
extern void _HideOverlay_Invoke_mAFB36F4D11769B68EFBEC3FB1BAFC7145D8BB4C0 ();
// 0x000007F9 System.IAsyncResult OVR.OpenVR.IVROverlay__HideOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _HideOverlay_BeginInvoke_m8FACCFF0B42527ACDDC6C17CA542022020969C62 ();
// 0x000007FA OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__HideOverlay::EndInvoke(System.IAsyncResult)
extern void _HideOverlay_EndInvoke_m76F5256432CCEF7B41B65A5A115816B7F66FD5DF ();
// 0x000007FB System.Void OVR.OpenVR.IVROverlay__IsOverlayVisible::.ctor(System.Object,System.IntPtr)
extern void _IsOverlayVisible__ctor_m868B4CD1403ED5E63F429D9CD501AF8087C83F32 ();
// 0x000007FC System.Boolean OVR.OpenVR.IVROverlay__IsOverlayVisible::Invoke(System.UInt64)
extern void _IsOverlayVisible_Invoke_mDE56B3643B1046A720A712398D748A91D9864495 ();
// 0x000007FD System.IAsyncResult OVR.OpenVR.IVROverlay__IsOverlayVisible::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _IsOverlayVisible_BeginInvoke_m0F923F02B1F78E2A3E4C14DAAE91EC0ADC92E6E5 ();
// 0x000007FE System.Boolean OVR.OpenVR.IVROverlay__IsOverlayVisible::EndInvoke(System.IAsyncResult)
extern void _IsOverlayVisible_EndInvoke_mF7B60596EC77CB39A48495C70F1B402EA037A562 ();
// 0x000007FF System.Void OVR.OpenVR.IVROverlay__GetTransformForOverlayCoordinates::.ctor(System.Object,System.IntPtr)
extern void _GetTransformForOverlayCoordinates__ctor_mB149F50E976A0FA70127CD5E794F2AE052AC1493 ();
// 0x00000800 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetTransformForOverlayCoordinates::Invoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdVector2_t,OVR.OpenVR.HmdMatrix34_t&)
extern void _GetTransformForOverlayCoordinates_Invoke_m0B6245BB25F5E78921E65D10F52291A8CC215F55 ();
// 0x00000801 System.IAsyncResult OVR.OpenVR.IVROverlay__GetTransformForOverlayCoordinates::BeginInvoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdVector2_t,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _GetTransformForOverlayCoordinates_BeginInvoke_mEF5A8586A0F476DBA501F466EEBA7D5B472D36E0 ();
// 0x00000802 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetTransformForOverlayCoordinates::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _GetTransformForOverlayCoordinates_EndInvoke_m2FE325BD2BF540B60526B4A0F751A647B004FBBF ();
// 0x00000803 System.Void OVR.OpenVR.IVROverlay__PollNextOverlayEvent::.ctor(System.Object,System.IntPtr)
extern void _PollNextOverlayEvent__ctor_m5C5B40FC15355C9BD6AB60822E1327A519835F5C ();
// 0x00000804 System.Boolean OVR.OpenVR.IVROverlay__PollNextOverlayEvent::Invoke(System.UInt64,OVR.OpenVR.VREvent_t&,System.UInt32)
extern void _PollNextOverlayEvent_Invoke_m258E6B7E1A9605B058388625A0BE1652B545D53D ();
// 0x00000805 System.IAsyncResult OVR.OpenVR.IVROverlay__PollNextOverlayEvent::BeginInvoke(System.UInt64,OVR.OpenVR.VREvent_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _PollNextOverlayEvent_BeginInvoke_m962C202B2D90117C79FD40A64526E0A834D73C61 ();
// 0x00000806 System.Boolean OVR.OpenVR.IVROverlay__PollNextOverlayEvent::EndInvoke(OVR.OpenVR.VREvent_t&,System.IAsyncResult)
extern void _PollNextOverlayEvent_EndInvoke_m1EEC2D50E3FA43CFD90EC4FAEAE52CDC61C90049 ();
// 0x00000807 System.Void OVR.OpenVR.IVROverlay__GetOverlayInputMethod::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayInputMethod__ctor_m00E45764FF2FE8A397F25EE687FEC30570BEF6A4 ();
// 0x00000808 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayInputMethod::Invoke(System.UInt64,OVR.OpenVR.VROverlayInputMethod&)
extern void _GetOverlayInputMethod_Invoke_m79BF508A695D24F469AE2356F310CBC8B765DD81 ();
// 0x00000809 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayInputMethod::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayInputMethod&,System.AsyncCallback,System.Object)
extern void _GetOverlayInputMethod_BeginInvoke_mFFFD1595572CA2D18EAC9A5EB3C9300E602F935E ();
// 0x0000080A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayInputMethod::EndInvoke(OVR.OpenVR.VROverlayInputMethod&,System.IAsyncResult)
extern void _GetOverlayInputMethod_EndInvoke_m7F2B15BBC13CCABB44341145A88BFC3FAB55C975 ();
// 0x0000080B System.Void OVR.OpenVR.IVROverlay__SetOverlayInputMethod::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayInputMethod__ctor_mF14787A5DCECD6C30FD1AF1DDC8B2F486E84CFBC ();
// 0x0000080C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayInputMethod::Invoke(System.UInt64,OVR.OpenVR.VROverlayInputMethod)
extern void _SetOverlayInputMethod_Invoke_mBEE07180E398766C4DE4A58D52797541822FAAB3 ();
// 0x0000080D System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayInputMethod::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayInputMethod,System.AsyncCallback,System.Object)
extern void _SetOverlayInputMethod_BeginInvoke_m58C3C56AA999374EB5CEE6F8CCAFD0DD6C3C6963 ();
// 0x0000080E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayInputMethod::EndInvoke(System.IAsyncResult)
extern void _SetOverlayInputMethod_EndInvoke_m17C628F0A581D4CCB689C1C89CA1C6B9439E518F ();
// 0x0000080F System.Void OVR.OpenVR.IVROverlay__GetOverlayMouseScale::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayMouseScale__ctor_mD3569F4810107525CAA51DBA8C08A038862C5A51 ();
// 0x00000810 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayMouseScale::Invoke(System.UInt64,OVR.OpenVR.HmdVector2_t&)
extern void _GetOverlayMouseScale_Invoke_mB0992A2F3BCF9123705CBC3F2CC88DEF4E2C5697 ();
// 0x00000811 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayMouseScale::BeginInvoke(System.UInt64,OVR.OpenVR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayMouseScale_BeginInvoke_m83F457A98BE9A74A3A0C0C5E282A65F3A6C8AA45 ();
// 0x00000812 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayMouseScale::EndInvoke(OVR.OpenVR.HmdVector2_t&,System.IAsyncResult)
extern void _GetOverlayMouseScale_EndInvoke_m66552A1118FE81DB871B3468FD79FAE7B7EAA62B ();
// 0x00000813 System.Void OVR.OpenVR.IVROverlay__SetOverlayMouseScale::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayMouseScale__ctor_m33C24E4BFD14A9727BAB1FA4F2837A511032EA81 ();
// 0x00000814 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayMouseScale::Invoke(System.UInt64,OVR.OpenVR.HmdVector2_t&)
extern void _SetOverlayMouseScale_Invoke_m0CAB759AE9777FDB6A503E2B686D52FF05E96C75 ();
// 0x00000815 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayMouseScale::BeginInvoke(System.UInt64,OVR.OpenVR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayMouseScale_BeginInvoke_mB7C2EC861BAE7DBE09984F22908786FCB067C023 ();
// 0x00000816 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayMouseScale::EndInvoke(OVR.OpenVR.HmdVector2_t&,System.IAsyncResult)
extern void _SetOverlayMouseScale_EndInvoke_m7441941EE2DA1737ED9DCB7E9B862BF4F7AE68A4 ();
// 0x00000817 System.Void OVR.OpenVR.IVROverlay__ComputeOverlayIntersection::.ctor(System.Object,System.IntPtr)
extern void _ComputeOverlayIntersection__ctor_mD7B9A7BEBF6B8D3CE552444765D38868D6462149 ();
// 0x00000818 System.Boolean OVR.OpenVR.IVROverlay__ComputeOverlayIntersection::Invoke(System.UInt64,OVR.OpenVR.VROverlayIntersectionParams_t&,OVR.OpenVR.VROverlayIntersectionResults_t&)
extern void _ComputeOverlayIntersection_Invoke_m020402DFF6C74A866E68824BB1D3260C1CF9DC89 ();
// 0x00000819 System.IAsyncResult OVR.OpenVR.IVROverlay__ComputeOverlayIntersection::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayIntersectionParams_t&,OVR.OpenVR.VROverlayIntersectionResults_t&,System.AsyncCallback,System.Object)
extern void _ComputeOverlayIntersection_BeginInvoke_m0C8F1F1BFD1FC88713B3AEF7080C1AE7AA9CA423 ();
// 0x0000081A System.Boolean OVR.OpenVR.IVROverlay__ComputeOverlayIntersection::EndInvoke(OVR.OpenVR.VROverlayIntersectionParams_t&,OVR.OpenVR.VROverlayIntersectionResults_t&,System.IAsyncResult)
extern void _ComputeOverlayIntersection_EndInvoke_mBDE0D0FA4CC2B9AFB57454F2A79C48815B366018 ();
// 0x0000081B System.Void OVR.OpenVR.IVROverlay__IsHoverTargetOverlay::.ctor(System.Object,System.IntPtr)
extern void _IsHoverTargetOverlay__ctor_mE2F367611CCCAB5187B8EBF579AC8C7CF309174E ();
// 0x0000081C System.Boolean OVR.OpenVR.IVROverlay__IsHoverTargetOverlay::Invoke(System.UInt64)
extern void _IsHoverTargetOverlay_Invoke_m6E134F5EE52983C22C266C9431830CAF413EE368 ();
// 0x0000081D System.IAsyncResult OVR.OpenVR.IVROverlay__IsHoverTargetOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _IsHoverTargetOverlay_BeginInvoke_m41F5FA3C45995CB5FBA6ED27EDAEA575E6C0ABBF ();
// 0x0000081E System.Boolean OVR.OpenVR.IVROverlay__IsHoverTargetOverlay::EndInvoke(System.IAsyncResult)
extern void _IsHoverTargetOverlay_EndInvoke_m531F374BD111C26A8A708DD83BD405C3BA231175 ();
// 0x0000081F System.Void OVR.OpenVR.IVROverlay__GetGamepadFocusOverlay::.ctor(System.Object,System.IntPtr)
extern void _GetGamepadFocusOverlay__ctor_m804858C4D13E3C42C0E5C39D9FC6285597474005 ();
// 0x00000820 System.UInt64 OVR.OpenVR.IVROverlay__GetGamepadFocusOverlay::Invoke()
extern void _GetGamepadFocusOverlay_Invoke_mAFEF1124A60ED71C13259E898C615E1CC5D19EF7 ();
// 0x00000821 System.IAsyncResult OVR.OpenVR.IVROverlay__GetGamepadFocusOverlay::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetGamepadFocusOverlay_BeginInvoke_mC7243CDC558D92126C5F1C24E6FEBC54D595A8F4 ();
// 0x00000822 System.UInt64 OVR.OpenVR.IVROverlay__GetGamepadFocusOverlay::EndInvoke(System.IAsyncResult)
extern void _GetGamepadFocusOverlay_EndInvoke_mA639B753C7EE2185D02231B793F0C32765E00C90 ();
// 0x00000823 System.Void OVR.OpenVR.IVROverlay__SetGamepadFocusOverlay::.ctor(System.Object,System.IntPtr)
extern void _SetGamepadFocusOverlay__ctor_mD7D6A116398F944D9C938ECB4EF3EA95B0AA28B1 ();
// 0x00000824 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetGamepadFocusOverlay::Invoke(System.UInt64)
extern void _SetGamepadFocusOverlay_Invoke_m1284F1D3EB9E8CD53FDC77E4AB24A13372FC2BCD ();
// 0x00000825 System.IAsyncResult OVR.OpenVR.IVROverlay__SetGamepadFocusOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _SetGamepadFocusOverlay_BeginInvoke_m29F88B9F1AD20ABDF2A8F8F6BF9366537E687828 ();
// 0x00000826 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetGamepadFocusOverlay::EndInvoke(System.IAsyncResult)
extern void _SetGamepadFocusOverlay_EndInvoke_m096205AB9D650AA147BD8F36040BFEE45197B2DD ();
// 0x00000827 System.Void OVR.OpenVR.IVROverlay__SetOverlayNeighbor::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayNeighbor__ctor_mE48218584965B6FB3E1ACC730E4AE119FA3157C5 ();
// 0x00000828 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayNeighbor::Invoke(OVR.OpenVR.EOverlayDirection,System.UInt64,System.UInt64)
extern void _SetOverlayNeighbor_Invoke_mE8725421EBB4BD1EC047164890DE791401BC098C ();
// 0x00000829 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayNeighbor::BeginInvoke(OVR.OpenVR.EOverlayDirection,System.UInt64,System.UInt64,System.AsyncCallback,System.Object)
extern void _SetOverlayNeighbor_BeginInvoke_m499F6ACBE9BAF7DA627B63F10EE8CB15C622CC7E ();
// 0x0000082A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayNeighbor::EndInvoke(System.IAsyncResult)
extern void _SetOverlayNeighbor_EndInvoke_m27DCDF035E0C4D1A80B875CBE25227FB49A8F0BE ();
// 0x0000082B System.Void OVR.OpenVR.IVROverlay__MoveGamepadFocusToNeighbor::.ctor(System.Object,System.IntPtr)
extern void _MoveGamepadFocusToNeighbor__ctor_m84FDE5671A1DFAF1CD2215395AA1E7DF1884340E ();
// 0x0000082C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__MoveGamepadFocusToNeighbor::Invoke(OVR.OpenVR.EOverlayDirection,System.UInt64)
extern void _MoveGamepadFocusToNeighbor_Invoke_m1E1B2B5E8AAAAD242E68C8CAEDFAA5D08DEF1959 ();
// 0x0000082D System.IAsyncResult OVR.OpenVR.IVROverlay__MoveGamepadFocusToNeighbor::BeginInvoke(OVR.OpenVR.EOverlayDirection,System.UInt64,System.AsyncCallback,System.Object)
extern void _MoveGamepadFocusToNeighbor_BeginInvoke_m8ED596DAAF2217682AED9B834E18D1627A77D753 ();
// 0x0000082E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__MoveGamepadFocusToNeighbor::EndInvoke(System.IAsyncResult)
extern void _MoveGamepadFocusToNeighbor_EndInvoke_mB4B3C93FE0D3B79D11C73F54E927E1191E42F65A ();
// 0x0000082F System.Void OVR.OpenVR.IVROverlay__SetOverlayDualAnalogTransform::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayDualAnalogTransform__ctor_mDE3D586F934702109960590633EFC1BC8A5F41BB ();
// 0x00000830 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayDualAnalogTransform::Invoke(System.UInt64,OVR.OpenVR.EDualAnalogWhich,System.IntPtr,System.Single)
extern void _SetOverlayDualAnalogTransform_Invoke_mFC3566123CC6DD975ECA193B46C47C8EFF1C8CCF ();
// 0x00000831 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayDualAnalogTransform::BeginInvoke(System.UInt64,OVR.OpenVR.EDualAnalogWhich,System.IntPtr,System.Single,System.AsyncCallback,System.Object)
extern void _SetOverlayDualAnalogTransform_BeginInvoke_m25FBCA75B4FA082F40A498FCEE888311FD408B3F ();
// 0x00000832 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayDualAnalogTransform::EndInvoke(System.IAsyncResult)
extern void _SetOverlayDualAnalogTransform_EndInvoke_mED1E48A4E1AB933B86D26BA2E9A51F831303DB89 ();
// 0x00000833 System.Void OVR.OpenVR.IVROverlay__GetOverlayDualAnalogTransform::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayDualAnalogTransform__ctor_m91E823FC5E9038E2F72079F5921A075668B96C0E ();
// 0x00000834 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayDualAnalogTransform::Invoke(System.UInt64,OVR.OpenVR.EDualAnalogWhich,OVR.OpenVR.HmdVector2_t&,System.Single&)
extern void _GetOverlayDualAnalogTransform_Invoke_mDC11A950A4BC9DE37FA7B861193B99E90E109C49 ();
// 0x00000835 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayDualAnalogTransform::BeginInvoke(System.UInt64,OVR.OpenVR.EDualAnalogWhich,OVR.OpenVR.HmdVector2_t&,System.Single&,System.AsyncCallback,System.Object)
extern void _GetOverlayDualAnalogTransform_BeginInvoke_mA2E45E80F29409335882897E9F6AE7FB4B5E419F ();
// 0x00000836 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayDualAnalogTransform::EndInvoke(OVR.OpenVR.HmdVector2_t&,System.Single&,System.IAsyncResult)
extern void _GetOverlayDualAnalogTransform_EndInvoke_m13EE948A13B34854888EFE18AF34261396C988B9 ();
// 0x00000837 System.Void OVR.OpenVR.IVROverlay__SetOverlayTexture::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayTexture__ctor_mEC1B9FC33EEE71D011C0139D5D1AA808B8084566 ();
// 0x00000838 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTexture::Invoke(System.UInt64,OVR.OpenVR.Texture_t&)
extern void _SetOverlayTexture_Invoke_mA50FEF68FA65A13D731ECCBCC82326AD8D04AE05 ();
// 0x00000839 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayTexture::BeginInvoke(System.UInt64,OVR.OpenVR.Texture_t&,System.AsyncCallback,System.Object)
extern void _SetOverlayTexture_BeginInvoke_m9BCAFF4A475034FFE16EC5C97F484E98C7788102 ();
// 0x0000083A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayTexture::EndInvoke(OVR.OpenVR.Texture_t&,System.IAsyncResult)
extern void _SetOverlayTexture_EndInvoke_m5687CC54AEC0FEDA7CD599C92916EACAD58B25B2 ();
// 0x0000083B System.Void OVR.OpenVR.IVROverlay__ClearOverlayTexture::.ctor(System.Object,System.IntPtr)
extern void _ClearOverlayTexture__ctor_m6D30AF655D426912C40F9D4EB0ED9BF7E11FD98F ();
// 0x0000083C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ClearOverlayTexture::Invoke(System.UInt64)
extern void _ClearOverlayTexture_Invoke_mD9919CEF34B393DD27667C9E8B899B0CD1AE092A ();
// 0x0000083D System.IAsyncResult OVR.OpenVR.IVROverlay__ClearOverlayTexture::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _ClearOverlayTexture_BeginInvoke_m5FDE02CAC8FD45D0502C31B969E5A0799DD73C01 ();
// 0x0000083E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ClearOverlayTexture::EndInvoke(System.IAsyncResult)
extern void _ClearOverlayTexture_EndInvoke_m4A1F5A28CBAA82C4492ED129D2CC7A3BD16167F1 ();
// 0x0000083F System.Void OVR.OpenVR.IVROverlay__SetOverlayRaw::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayRaw__ctor_m7A6825B4349D4E0100383E6A478B1441847A892B ();
// 0x00000840 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRaw::Invoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32)
extern void _SetOverlayRaw_Invoke_m6AB24C29977942DE2CE95F16190F0A939524ABAC ();
// 0x00000841 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayRaw::BeginInvoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern void _SetOverlayRaw_BeginInvoke_m5E8C915B00BA3330C45B10EC6245409ECEB233BE ();
// 0x00000842 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayRaw::EndInvoke(System.IAsyncResult)
extern void _SetOverlayRaw_EndInvoke_m8A450225FDFD8EFD692A8401B424F5A71E339F1C ();
// 0x00000843 System.Void OVR.OpenVR.IVROverlay__SetOverlayFromFile::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayFromFile__ctor_mDE8DF12D3229FFB7FEB9B9A683A6D617F4FE53ED ();
// 0x00000844 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayFromFile::Invoke(System.UInt64,System.String)
extern void _SetOverlayFromFile_Invoke_m168FEC0D50310BEDA5D0E1353312C5318096B1AE ();
// 0x00000845 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayFromFile::BeginInvoke(System.UInt64,System.String,System.AsyncCallback,System.Object)
extern void _SetOverlayFromFile_BeginInvoke_mCA86F0A7AB0D72445E1A590BC954C3FE3D9826AF ();
// 0x00000846 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayFromFile::EndInvoke(System.IAsyncResult)
extern void _SetOverlayFromFile_EndInvoke_m95905767CF857D9A9919F1D76D6C7FEBA4BBB16F ();
// 0x00000847 System.Void OVR.OpenVR.IVROverlay__GetOverlayTexture::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTexture__ctor_m78A99EC7CDB046B08D5704C9B380DDFFB84EDCC0 ();
// 0x00000848 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTexture::Invoke(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,OVR.OpenVR.ETextureType&,OVR.OpenVR.EColorSpace&,OVR.OpenVR.VRTextureBounds_t&)
extern void _GetOverlayTexture_Invoke_m29C919D5BED915E123EBC5E1107857BC8D4DC64D ();
// 0x00000849 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTexture::BeginInvoke(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,OVR.OpenVR.ETextureType&,OVR.OpenVR.EColorSpace&,OVR.OpenVR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern void _GetOverlayTexture_BeginInvoke_mD837C513DC0E222ABB22C66120D2E7DC1F75D5C3 ();
// 0x0000084A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTexture::EndInvoke(System.IntPtr&,System.UInt32&,System.UInt32&,System.UInt32&,OVR.OpenVR.ETextureType&,OVR.OpenVR.EColorSpace&,OVR.OpenVR.VRTextureBounds_t&,System.IAsyncResult)
extern void _GetOverlayTexture_EndInvoke_mEAC566B02C07BD0836E31CCBC21FFECCD5552459 ();
// 0x0000084B System.Void OVR.OpenVR.IVROverlay__ReleaseNativeOverlayHandle::.ctor(System.Object,System.IntPtr)
extern void _ReleaseNativeOverlayHandle__ctor_mAE5034561AD5D464B07900FE7B1112524C226404 ();
// 0x0000084C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ReleaseNativeOverlayHandle::Invoke(System.UInt64,System.IntPtr)
extern void _ReleaseNativeOverlayHandle_Invoke_m96827DB4EB74569B4F1EACD33B9BC8EA86A232BC ();
// 0x0000084D System.IAsyncResult OVR.OpenVR.IVROverlay__ReleaseNativeOverlayHandle::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void _ReleaseNativeOverlayHandle_BeginInvoke_m6FFDD17C09DF1728DDF3BA2A6604631EBF46B4C9 ();
// 0x0000084E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ReleaseNativeOverlayHandle::EndInvoke(System.IAsyncResult)
extern void _ReleaseNativeOverlayHandle_EndInvoke_mC21D0B31128943F3C95C9464A5DFEAAFB8379903 ();
// 0x0000084F System.Void OVR.OpenVR.IVROverlay__GetOverlayTextureSize::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayTextureSize__ctor_mA25CFCEA5B994CDD9816F94D9134E3C5B8222152 ();
// 0x00000850 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureSize::Invoke(System.UInt64,System.UInt32&,System.UInt32&)
extern void _GetOverlayTextureSize_Invoke_m1E15F714CF6FB32D9B9F1D0CEC16783B92F5B379 ();
// 0x00000851 System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayTextureSize::BeginInvoke(System.UInt64,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetOverlayTextureSize_BeginInvoke_m6AEDF2802264CB5D6309AA5BD03DC3A8F5E35C7C ();
// 0x00000852 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayTextureSize::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void _GetOverlayTextureSize_EndInvoke_m1BC297DA4B35E8BFF70C924643E0E19A455881F6 ();
// 0x00000853 System.Void OVR.OpenVR.IVROverlay__CreateDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern void _CreateDashboardOverlay__ctor_mE5B536AF2AD714C67E16D4E932052FEBAEAA7299 ();
// 0x00000854 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__CreateDashboardOverlay::Invoke(System.String,System.String,System.UInt64&,System.UInt64&)
extern void _CreateDashboardOverlay_Invoke_mEE840694F56BC17BF8D182C5932E1EE8265283A4 ();
// 0x00000855 System.IAsyncResult OVR.OpenVR.IVROverlay__CreateDashboardOverlay::BeginInvoke(System.String,System.String,System.UInt64&,System.UInt64&,System.AsyncCallback,System.Object)
extern void _CreateDashboardOverlay_BeginInvoke_m7BF1299C5407826D7DF2AACDA1A2E26AF5B89B32 ();
// 0x00000856 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__CreateDashboardOverlay::EndInvoke(System.UInt64&,System.UInt64&,System.IAsyncResult)
extern void _CreateDashboardOverlay_EndInvoke_m82B62AF86AF802F9B09DC4B5490DF5DC462D1F1D ();
// 0x00000857 System.Void OVR.OpenVR.IVROverlay__IsDashboardVisible::.ctor(System.Object,System.IntPtr)
extern void _IsDashboardVisible__ctor_m5FE4C70E27FEA962509C4F14086C2B1F02AE9D08 ();
// 0x00000858 System.Boolean OVR.OpenVR.IVROverlay__IsDashboardVisible::Invoke()
extern void _IsDashboardVisible_Invoke_m70D7D352A81AAEAB15FAB82CD381E68F3F2E49F0 ();
// 0x00000859 System.IAsyncResult OVR.OpenVR.IVROverlay__IsDashboardVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern void _IsDashboardVisible_BeginInvoke_m152E7BB5DB123280A2D21F984D07DFC95BABC9B4 ();
// 0x0000085A System.Boolean OVR.OpenVR.IVROverlay__IsDashboardVisible::EndInvoke(System.IAsyncResult)
extern void _IsDashboardVisible_EndInvoke_m808B9A6277FB129ED5DA8097EFAFEFBEFF354549 ();
// 0x0000085B System.Void OVR.OpenVR.IVROverlay__IsActiveDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern void _IsActiveDashboardOverlay__ctor_mD05ACEBB25499584B9EF899358FB782FC3B1A16B ();
// 0x0000085C System.Boolean OVR.OpenVR.IVROverlay__IsActiveDashboardOverlay::Invoke(System.UInt64)
extern void _IsActiveDashboardOverlay_Invoke_mC2E44057CA797B347B1C6D576C73EE50F9FF5A9C ();
// 0x0000085D System.IAsyncResult OVR.OpenVR.IVROverlay__IsActiveDashboardOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _IsActiveDashboardOverlay_BeginInvoke_mC6E62C4573720E43B41E95CB27B62F6F95760810 ();
// 0x0000085E System.Boolean OVR.OpenVR.IVROverlay__IsActiveDashboardOverlay::EndInvoke(System.IAsyncResult)
extern void _IsActiveDashboardOverlay_EndInvoke_m8D260446EAD13DB6A173D94DECAAA1F3ECB57A31 ();
// 0x0000085F System.Void OVR.OpenVR.IVROverlay__SetDashboardOverlaySceneProcess::.ctor(System.Object,System.IntPtr)
extern void _SetDashboardOverlaySceneProcess__ctor_mCADDE92B4E4E048F7A75925906C4EC496294529B ();
// 0x00000860 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetDashboardOverlaySceneProcess::Invoke(System.UInt64,System.UInt32)
extern void _SetDashboardOverlaySceneProcess_Invoke_m444E56C36EA8EC89B2B2FBDE24646D998C633226 ();
// 0x00000861 System.IAsyncResult OVR.OpenVR.IVROverlay__SetDashboardOverlaySceneProcess::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern void _SetDashboardOverlaySceneProcess_BeginInvoke_m658ED5AB526B15DC8D50A2AEA4F935C9F3D30D38 ();
// 0x00000862 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetDashboardOverlaySceneProcess::EndInvoke(System.IAsyncResult)
extern void _SetDashboardOverlaySceneProcess_EndInvoke_mB3A70856CC8773B36D7BB6F5435BB8F9571E4433 ();
// 0x00000863 System.Void OVR.OpenVR.IVROverlay__GetDashboardOverlaySceneProcess::.ctor(System.Object,System.IntPtr)
extern void _GetDashboardOverlaySceneProcess__ctor_mCF07FD53BEFC15804465229666473CB2C254A555 ();
// 0x00000864 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetDashboardOverlaySceneProcess::Invoke(System.UInt64,System.UInt32&)
extern void _GetDashboardOverlaySceneProcess_Invoke_m99BCA5FB0B473DED77DDE6687B39C58635AD0FF4 ();
// 0x00000865 System.IAsyncResult OVR.OpenVR.IVROverlay__GetDashboardOverlaySceneProcess::BeginInvoke(System.UInt64,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetDashboardOverlaySceneProcess_BeginInvoke_mF36287BF8B6307B99BD9F21A3479E4F644103754 ();
// 0x00000866 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetDashboardOverlaySceneProcess::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetDashboardOverlaySceneProcess_EndInvoke_mF4E8E1325E238C29355553856E27300614D6B6D7 ();
// 0x00000867 System.Void OVR.OpenVR.IVROverlay__ShowDashboard::.ctor(System.Object,System.IntPtr)
extern void _ShowDashboard__ctor_m27B7D2B2820FA747CC1F5CE9C8F2A31790C9EBBE ();
// 0x00000868 System.Void OVR.OpenVR.IVROverlay__ShowDashboard::Invoke(System.String)
extern void _ShowDashboard_Invoke_m17DE084F137A9A8972748A9EEA5C632272318D69 ();
// 0x00000869 System.IAsyncResult OVR.OpenVR.IVROverlay__ShowDashboard::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _ShowDashboard_BeginInvoke_mC62432E97271B62853C9DEB3FB25C93A19C88656 ();
// 0x0000086A System.Void OVR.OpenVR.IVROverlay__ShowDashboard::EndInvoke(System.IAsyncResult)
extern void _ShowDashboard_EndInvoke_m24153FEEFC8F9F1E4396E1290EB088BF7AC24579 ();
// 0x0000086B System.Void OVR.OpenVR.IVROverlay__GetPrimaryDashboardDevice::.ctor(System.Object,System.IntPtr)
extern void _GetPrimaryDashboardDevice__ctor_m6CA654DE11A210ACFA0E89CF8E82945D58B081BD ();
// 0x0000086C System.UInt32 OVR.OpenVR.IVROverlay__GetPrimaryDashboardDevice::Invoke()
extern void _GetPrimaryDashboardDevice_Invoke_m9A352147A0FCDAC2E10F661BD9BDFF6266B15BB3 ();
// 0x0000086D System.IAsyncResult OVR.OpenVR.IVROverlay__GetPrimaryDashboardDevice::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetPrimaryDashboardDevice_BeginInvoke_mC20D8AE5CE562E57260920051643906FD26D9FD8 ();
// 0x0000086E System.UInt32 OVR.OpenVR.IVROverlay__GetPrimaryDashboardDevice::EndInvoke(System.IAsyncResult)
extern void _GetPrimaryDashboardDevice_EndInvoke_m36EDBD37FFCC39781BA7C9EA7286EBD92F03B58D ();
// 0x0000086F System.Void OVR.OpenVR.IVROverlay__ShowKeyboard::.ctor(System.Object,System.IntPtr)
extern void _ShowKeyboard__ctor_mB304AD2E52232F23DB7545E5B00400005189A19D ();
// 0x00000870 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowKeyboard::Invoke(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern void _ShowKeyboard_Invoke_m2F66075375F2D39BFCF710582E99A039A5BA9A2F ();
// 0x00000871 System.IAsyncResult OVR.OpenVR.IVROverlay__ShowKeyboard::BeginInvoke(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64,System.AsyncCallback,System.Object)
extern void _ShowKeyboard_BeginInvoke_m98A7353153A78E971F2F0FFD73E6E662601635B5 ();
// 0x00000872 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowKeyboard::EndInvoke(System.IAsyncResult)
extern void _ShowKeyboard_EndInvoke_m420EC30EBDB769875AD32CEB7F8A212D496E9E18 ();
// 0x00000873 System.Void OVR.OpenVR.IVROverlay__ShowKeyboardForOverlay::.ctor(System.Object,System.IntPtr)
extern void _ShowKeyboardForOverlay__ctor_m6956B48C497263EAD795997912B1DCDB8E4C9884 ();
// 0x00000874 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowKeyboardForOverlay::Invoke(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern void _ShowKeyboardForOverlay_Invoke_m297159DAA856CD5870C20BE611A8C42FF1A09C73 ();
// 0x00000875 System.IAsyncResult OVR.OpenVR.IVROverlay__ShowKeyboardForOverlay::BeginInvoke(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64,System.AsyncCallback,System.Object)
extern void _ShowKeyboardForOverlay_BeginInvoke_mFCD7B8409415D0339B6E482658D50C1B540069CE ();
// 0x00000876 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__ShowKeyboardForOverlay::EndInvoke(System.IAsyncResult)
extern void _ShowKeyboardForOverlay_EndInvoke_mF32CB159E146ECCA2F8CBF21AD0271C697CA7E33 ();
// 0x00000877 System.Void OVR.OpenVR.IVROverlay__GetKeyboardText::.ctor(System.Object,System.IntPtr)
extern void _GetKeyboardText__ctor_m3305FF2E8A9C2F3473B71BB5C1B9B05B9595CE13 ();
// 0x00000878 System.UInt32 OVR.OpenVR.IVROverlay__GetKeyboardText::Invoke(System.Text.StringBuilder,System.UInt32)
extern void _GetKeyboardText_Invoke_m998A27D96FB3E1CC80639AD7939221A72D72BEFB ();
// 0x00000879 System.IAsyncResult OVR.OpenVR.IVROverlay__GetKeyboardText::BeginInvoke(System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetKeyboardText_BeginInvoke_mA15848BDB54C6241C9CF6FE6B966041ABEB38EA7 ();
// 0x0000087A System.UInt32 OVR.OpenVR.IVROverlay__GetKeyboardText::EndInvoke(System.IAsyncResult)
extern void _GetKeyboardText_EndInvoke_m7BB4219F9C198CE120000957984B7BC6E1618A4B ();
// 0x0000087B System.Void OVR.OpenVR.IVROverlay__HideKeyboard::.ctor(System.Object,System.IntPtr)
extern void _HideKeyboard__ctor_m80C67510481424E09AA83FECF8705AD15085AA54 ();
// 0x0000087C System.Void OVR.OpenVR.IVROverlay__HideKeyboard::Invoke()
extern void _HideKeyboard_Invoke_m961B3BF62A13E17FEC72CD20FA6F7882BFA4A4A4 ();
// 0x0000087D System.IAsyncResult OVR.OpenVR.IVROverlay__HideKeyboard::BeginInvoke(System.AsyncCallback,System.Object)
extern void _HideKeyboard_BeginInvoke_m9DE5B81D45D3D9826AC43A3E731A22B678D332E7 ();
// 0x0000087E System.Void OVR.OpenVR.IVROverlay__HideKeyboard::EndInvoke(System.IAsyncResult)
extern void _HideKeyboard_EndInvoke_mADA9F3195BD43F0ABCB26F38D85F67B4E1B3925F ();
// 0x0000087F System.Void OVR.OpenVR.IVROverlay__SetKeyboardTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern void _SetKeyboardTransformAbsolute__ctor_mB0E8008A0FCBDE575AAA7C2A5495DB00BE938E5E ();
// 0x00000880 System.Void OVR.OpenVR.IVROverlay__SetKeyboardTransformAbsolute::Invoke(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&)
extern void _SetKeyboardTransformAbsolute_Invoke_m931CBE3336D58F5519D8C4A0A8BADA5A55F8F63C ();
// 0x00000881 System.IAsyncResult OVR.OpenVR.IVROverlay__SetKeyboardTransformAbsolute::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern void _SetKeyboardTransformAbsolute_BeginInvoke_m41FE32852BC7FA923E9AB4F03B8D93E11D629D4A ();
// 0x00000882 System.Void OVR.OpenVR.IVROverlay__SetKeyboardTransformAbsolute::EndInvoke(OVR.OpenVR.HmdMatrix34_t&,System.IAsyncResult)
extern void _SetKeyboardTransformAbsolute_EndInvoke_mECAE8E0ADBD79322527AF75C79D36E9266CF8F49 ();
// 0x00000883 System.Void OVR.OpenVR.IVROverlay__SetKeyboardPositionForOverlay::.ctor(System.Object,System.IntPtr)
extern void _SetKeyboardPositionForOverlay__ctor_m702F89F4A7F51CFE12C1AA2821119CACD68C5AF8 ();
// 0x00000884 System.Void OVR.OpenVR.IVROverlay__SetKeyboardPositionForOverlay::Invoke(System.UInt64,OVR.OpenVR.HmdRect2_t)
extern void _SetKeyboardPositionForOverlay_Invoke_m23C4586034C947A391F327E7620D47D8C30DB44E ();
// 0x00000885 System.IAsyncResult OVR.OpenVR.IVROverlay__SetKeyboardPositionForOverlay::BeginInvoke(System.UInt64,OVR.OpenVR.HmdRect2_t,System.AsyncCallback,System.Object)
extern void _SetKeyboardPositionForOverlay_BeginInvoke_m7FE40B16324A70305722F00377A2EAB557D5B967 ();
// 0x00000886 System.Void OVR.OpenVR.IVROverlay__SetKeyboardPositionForOverlay::EndInvoke(System.IAsyncResult)
extern void _SetKeyboardPositionForOverlay_EndInvoke_m1E577ECF3C09005994FF6946F81CF688D96AE729 ();
// 0x00000887 System.Void OVR.OpenVR.IVROverlay__SetOverlayIntersectionMask::.ctor(System.Object,System.IntPtr)
extern void _SetOverlayIntersectionMask__ctor_m3781056147C33C9C3E011B794D1DEFB8EBCD44B1 ();
// 0x00000888 OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayIntersectionMask::Invoke(System.UInt64,OVR.OpenVR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32)
extern void _SetOverlayIntersectionMask_Invoke_mB59570EA77220F499AE0E11F6BD141493E7A37DD ();
// 0x00000889 System.IAsyncResult OVR.OpenVR.IVROverlay__SetOverlayIntersectionMask::BeginInvoke(System.UInt64,OVR.OpenVR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern void _SetOverlayIntersectionMask_BeginInvoke_mBCDC96B661435BCD094BCFCF74BDDB3167E3C215 ();
// 0x0000088A OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__SetOverlayIntersectionMask::EndInvoke(OVR.OpenVR.VROverlayIntersectionMaskPrimitive_t&,System.IAsyncResult)
extern void _SetOverlayIntersectionMask_EndInvoke_m92C5B4D9AFEC9908D1C8C1650E5DFBE58AC6F0BF ();
// 0x0000088B System.Void OVR.OpenVR.IVROverlay__GetOverlayFlags::.ctor(System.Object,System.IntPtr)
extern void _GetOverlayFlags__ctor_mF339BBF4A1D85CBB3471E46A1938DE62B6F79F15 ();
// 0x0000088C OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayFlags::Invoke(System.UInt64,System.UInt32&)
extern void _GetOverlayFlags_Invoke_m45A223151BE499DA897F0413275A9903D04F4BC8 ();
// 0x0000088D System.IAsyncResult OVR.OpenVR.IVROverlay__GetOverlayFlags::BeginInvoke(System.UInt64,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetOverlayFlags_BeginInvoke_m7391829BA6B6A630B88C24684998FB37883182C9 ();
// 0x0000088E OVR.OpenVR.EVROverlayError OVR.OpenVR.IVROverlay__GetOverlayFlags::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetOverlayFlags_EndInvoke_m34B5F63E614FFE23A080E446EC02A7E43D49E4E1 ();
// 0x0000088F System.Void OVR.OpenVR.IVROverlay__ShowMessageOverlay::.ctor(System.Object,System.IntPtr)
extern void _ShowMessageOverlay__ctor_mEC3AC6FF29492B6D5F64F18EA0F1E763D2A00790 ();
// 0x00000890 OVR.OpenVR.VRMessageOverlayResponse OVR.OpenVR.IVROverlay__ShowMessageOverlay::Invoke(System.String,System.String,System.String,System.String,System.String,System.String)
extern void _ShowMessageOverlay_Invoke_m8B38191AB9B94D9F6FDDA8736C10326B0FC2C39D ();
// 0x00000891 System.IAsyncResult OVR.OpenVR.IVROverlay__ShowMessageOverlay::BeginInvoke(System.String,System.String,System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern void _ShowMessageOverlay_BeginInvoke_mE078229215AB4ED0DF135AB2AB39FDB000134F9F ();
// 0x00000892 OVR.OpenVR.VRMessageOverlayResponse OVR.OpenVR.IVROverlay__ShowMessageOverlay::EndInvoke(System.IAsyncResult)
extern void _ShowMessageOverlay_EndInvoke_mB8712A1BD34AA1CF421CDC47379DA5759C5B6A20 ();
// 0x00000893 System.Void OVR.OpenVR.IVROverlay__CloseMessageOverlay::.ctor(System.Object,System.IntPtr)
extern void _CloseMessageOverlay__ctor_m983D3F131FF10FD19628013938DB6990105FB2CC ();
// 0x00000894 System.Void OVR.OpenVR.IVROverlay__CloseMessageOverlay::Invoke()
extern void _CloseMessageOverlay_Invoke_mB4897E143F9BC2878DA5746847F1FE37406EFEE8 ();
// 0x00000895 System.IAsyncResult OVR.OpenVR.IVROverlay__CloseMessageOverlay::BeginInvoke(System.AsyncCallback,System.Object)
extern void _CloseMessageOverlay_BeginInvoke_m81ACAA21D31A99722842F73557A2C143A0EB1330 ();
// 0x00000896 System.Void OVR.OpenVR.IVROverlay__CloseMessageOverlay::EndInvoke(System.IAsyncResult)
extern void _CloseMessageOverlay_EndInvoke_mABEDF45DC8908EE371876C09143DD25F8C7DE0B4 ();
// 0x00000897 System.Void OVR.OpenVR.IVRRenderModels__LoadRenderModel_Async::.ctor(System.Object,System.IntPtr)
extern void _LoadRenderModel_Async__ctor_m766F2E0C86D4E1ECC3B6E8FF6B0D95DF99F05AD9 ();
// 0x00000898 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadRenderModel_Async::Invoke(System.String,System.IntPtr&)
extern void _LoadRenderModel_Async_Invoke_m224C6AEACFC8DF5AB833A9C7D86626467118ED29 ();
// 0x00000899 System.IAsyncResult OVR.OpenVR.IVRRenderModels__LoadRenderModel_Async::BeginInvoke(System.String,System.IntPtr&,System.AsyncCallback,System.Object)
extern void _LoadRenderModel_Async_BeginInvoke_mF23EF977F1524A130279105441F3C59E078C55D7 ();
// 0x0000089A OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadRenderModel_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void _LoadRenderModel_Async_EndInvoke_mEF7012A92DD7BE2C8E5DCF23E09642FAAEB697DA ();
// 0x0000089B System.Void OVR.OpenVR.IVRRenderModels__FreeRenderModel::.ctor(System.Object,System.IntPtr)
extern void _FreeRenderModel__ctor_m9A2CABF9DFE33066FC0935BADAABC71E289E304D ();
// 0x0000089C System.Void OVR.OpenVR.IVRRenderModels__FreeRenderModel::Invoke(System.IntPtr)
extern void _FreeRenderModel_Invoke_m494C68CCA08B36E4EFB91962473524FBC19D9374 ();
// 0x0000089D System.IAsyncResult OVR.OpenVR.IVRRenderModels__FreeRenderModel::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _FreeRenderModel_BeginInvoke_mB5C318FAFB42FFE8782C8778DC1905CDEF6D188D ();
// 0x0000089E System.Void OVR.OpenVR.IVRRenderModels__FreeRenderModel::EndInvoke(System.IAsyncResult)
extern void _FreeRenderModel_EndInvoke_m1B2FB635F8B3915D8AB9631F8E7BD396B4DA475F ();
// 0x0000089F System.Void OVR.OpenVR.IVRRenderModels__LoadTexture_Async::.ctor(System.Object,System.IntPtr)
extern void _LoadTexture_Async__ctor_m8059F56F0AB81ECF44FBFFEC65F27B4CF8D058AE ();
// 0x000008A0 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadTexture_Async::Invoke(System.Int32,System.IntPtr&)
extern void _LoadTexture_Async_Invoke_mDD760C8DEAD59A6B3EBBC64B3CB100074B517753 ();
// 0x000008A1 System.IAsyncResult OVR.OpenVR.IVRRenderModels__LoadTexture_Async::BeginInvoke(System.Int32,System.IntPtr&,System.AsyncCallback,System.Object)
extern void _LoadTexture_Async_BeginInvoke_m6BE2A7F66D4D90C703314A9CBA23B83C2914792B ();
// 0x000008A2 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadTexture_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void _LoadTexture_Async_EndInvoke_m0E0AF8C6FFD53B63070C62AD6937CE9E2B5D8F29 ();
// 0x000008A3 System.Void OVR.OpenVR.IVRRenderModels__FreeTexture::.ctor(System.Object,System.IntPtr)
extern void _FreeTexture__ctor_mE333AB15387DDBBFD083358A7A69D50B84F769BF ();
// 0x000008A4 System.Void OVR.OpenVR.IVRRenderModels__FreeTexture::Invoke(System.IntPtr)
extern void _FreeTexture_Invoke_m2358611FF7B266112954B6233C15CA8952E8D5EC ();
// 0x000008A5 System.IAsyncResult OVR.OpenVR.IVRRenderModels__FreeTexture::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _FreeTexture_BeginInvoke_mFFE6ACF5CB5240A5038F3A5E35271A1EF809E0B0 ();
// 0x000008A6 System.Void OVR.OpenVR.IVRRenderModels__FreeTexture::EndInvoke(System.IAsyncResult)
extern void _FreeTexture_EndInvoke_mCE61D6A0EACD9F5C873AF15E3D265E357C9343C3 ();
// 0x000008A7 System.Void OVR.OpenVR.IVRRenderModels__LoadTextureD3D11_Async::.ctor(System.Object,System.IntPtr)
extern void _LoadTextureD3D11_Async__ctor_m5DE6DACDE30E93B7E940D69876B50DC762F78C57 ();
// 0x000008A8 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadTextureD3D11_Async::Invoke(System.Int32,System.IntPtr,System.IntPtr&)
extern void _LoadTextureD3D11_Async_Invoke_m676C1193D08D45F8A2526DC268BFD8E8725F3612 ();
// 0x000008A9 System.IAsyncResult OVR.OpenVR.IVRRenderModels__LoadTextureD3D11_Async::BeginInvoke(System.Int32,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void _LoadTextureD3D11_Async_BeginInvoke_m34B98E91FF75C69740667814A7B6C9F36619AACB ();
// 0x000008AA OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadTextureD3D11_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void _LoadTextureD3D11_Async_EndInvoke_m3FF6CDC3813F295BAE1A6092EF7146324C8C3893 ();
// 0x000008AB System.Void OVR.OpenVR.IVRRenderModels__LoadIntoTextureD3D11_Async::.ctor(System.Object,System.IntPtr)
extern void _LoadIntoTextureD3D11_Async__ctor_m9879AC70207713E135607AF5CC3981705171E87C ();
// 0x000008AC OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadIntoTextureD3D11_Async::Invoke(System.Int32,System.IntPtr)
extern void _LoadIntoTextureD3D11_Async_Invoke_mCFF1C259714CA960B49F05D0D3410B2938D2CF45 ();
// 0x000008AD System.IAsyncResult OVR.OpenVR.IVRRenderModels__LoadIntoTextureD3D11_Async::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void _LoadIntoTextureD3D11_Async_BeginInvoke_mDC1AE964B69D97BFAE67469BFD42AAC724E6270F ();
// 0x000008AE OVR.OpenVR.EVRRenderModelError OVR.OpenVR.IVRRenderModels__LoadIntoTextureD3D11_Async::EndInvoke(System.IAsyncResult)
extern void _LoadIntoTextureD3D11_Async_EndInvoke_mFB95D207572E83A8BE38368B7A463305077E9D72 ();
// 0x000008AF System.Void OVR.OpenVR.IVRRenderModels__FreeTextureD3D11::.ctor(System.Object,System.IntPtr)
extern void _FreeTextureD3D11__ctor_mF9B7C1F8F6A72CAAF454F9AD88BC98FE017A7486 ();
// 0x000008B0 System.Void OVR.OpenVR.IVRRenderModels__FreeTextureD3D11::Invoke(System.IntPtr)
extern void _FreeTextureD3D11_Invoke_m61D0D07D06EB3D92C5FD10BF7D29794389A5E4F0 ();
// 0x000008B1 System.IAsyncResult OVR.OpenVR.IVRRenderModels__FreeTextureD3D11::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _FreeTextureD3D11_BeginInvoke_mD2021D56EACEF98F6F14756726CC52580D5B9C4F ();
// 0x000008B2 System.Void OVR.OpenVR.IVRRenderModels__FreeTextureD3D11::EndInvoke(System.IAsyncResult)
extern void _FreeTextureD3D11_EndInvoke_mD547BC3069F24D8B3CDE3F4F9F6CC060F5760CF8 ();
// 0x000008B3 System.Void OVR.OpenVR.IVRRenderModels__GetRenderModelName::.ctor(System.Object,System.IntPtr)
extern void _GetRenderModelName__ctor_m3E589232DBF6A29FC087629E64C0B60713707EA9 ();
// 0x000008B4 System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelName::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetRenderModelName_Invoke_m200A8632C5AD2F59CD8E5C48F65F405288CA78D3 ();
// 0x000008B5 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetRenderModelName::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetRenderModelName_BeginInvoke_mD5267EE33088AD15D8ABB8D419EAB6436F5A7F02 ();
// 0x000008B6 System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelName::EndInvoke(System.IAsyncResult)
extern void _GetRenderModelName_EndInvoke_mD41C490461C1C4D7D0D2F39779086B73D8A48F9A ();
// 0x000008B7 System.Void OVR.OpenVR.IVRRenderModels__GetRenderModelCount::.ctor(System.Object,System.IntPtr)
extern void _GetRenderModelCount__ctor_m4AE7B041D0C3684416651E9A64BFBFB0B4F5CDD0 ();
// 0x000008B8 System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelCount::Invoke()
extern void _GetRenderModelCount_Invoke_m89346EF43B893C6773E7AC0F58E4558AAFF8DAEE ();
// 0x000008B9 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetRenderModelCount::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetRenderModelCount_BeginInvoke_mAAEDC75B05F6B101DD7F5BCBE61954A7471A62A5 ();
// 0x000008BA System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelCount::EndInvoke(System.IAsyncResult)
extern void _GetRenderModelCount_EndInvoke_m63DAC5D3914E603B23EE72F6B7964A4F953B2BC3 ();
// 0x000008BB System.Void OVR.OpenVR.IVRRenderModels__GetComponentCount::.ctor(System.Object,System.IntPtr)
extern void _GetComponentCount__ctor_m2EFCB406D9F6F92806002EC60C099533007329A4 ();
// 0x000008BC System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentCount::Invoke(System.String)
extern void _GetComponentCount_Invoke_m378D0FFE040A80DA1EF99EA832DBF7D0329EC000 ();
// 0x000008BD System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentCount::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _GetComponentCount_BeginInvoke_mAF9AB925CF110A00F9ACAA53940D335A7A08DC2B ();
// 0x000008BE System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentCount::EndInvoke(System.IAsyncResult)
extern void _GetComponentCount_EndInvoke_mE5C3B820CF3E438AB4657DD730D1FA50BEE0577B ();
// 0x000008BF System.Void OVR.OpenVR.IVRRenderModels__GetComponentName::.ctor(System.Object,System.IntPtr)
extern void _GetComponentName__ctor_m78951178BC4C67452DB5A2424A0B675AC31A6DA6 ();
// 0x000008C0 System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentName::Invoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetComponentName_Invoke_m9796F8C20F8F78857B8F6D09DBBAB8D64B92DF6E ();
// 0x000008C1 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentName::BeginInvoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetComponentName_BeginInvoke_mF2BDBEA65F34713BD1E4DC586A9EAA5DED700F2E ();
// 0x000008C2 System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentName::EndInvoke(System.IAsyncResult)
extern void _GetComponentName_EndInvoke_m08FCEF6F9C027D6FC0B0500E0CA6B63063026122 ();
// 0x000008C3 System.Void OVR.OpenVR.IVRRenderModels__GetComponentButtonMask::.ctor(System.Object,System.IntPtr)
extern void _GetComponentButtonMask__ctor_m1313FB209FB7F2F6F29D2C689CE04BF4663139B3 ();
// 0x000008C4 System.UInt64 OVR.OpenVR.IVRRenderModels__GetComponentButtonMask::Invoke(System.String,System.String)
extern void _GetComponentButtonMask_Invoke_m8E74CFBDE03B27FC1118C51289BF81BB6540912C ();
// 0x000008C5 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentButtonMask::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void _GetComponentButtonMask_BeginInvoke_mFAADB11C728DB9D07D67C0796B11C7F89D4D6B25 ();
// 0x000008C6 System.UInt64 OVR.OpenVR.IVRRenderModels__GetComponentButtonMask::EndInvoke(System.IAsyncResult)
extern void _GetComponentButtonMask_EndInvoke_mAC3CE87E617A5A3458468A5F782B3CCFBB736B6B ();
// 0x000008C7 System.Void OVR.OpenVR.IVRRenderModels__GetComponentRenderModelName::.ctor(System.Object,System.IntPtr)
extern void _GetComponentRenderModelName__ctor_m5445FBF63542B869016DCA0E9CE7FFB26BF2DFB7 ();
// 0x000008C8 System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentRenderModelName::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern void _GetComponentRenderModelName_Invoke_mE63FDF7517DE1318A07ADEB62F4DDC65269FA355 ();
// 0x000008C9 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentRenderModelName::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetComponentRenderModelName_BeginInvoke_m0A9A330B011C2059F96C934D1C5E9A767A7D88D6 ();
// 0x000008CA System.UInt32 OVR.OpenVR.IVRRenderModels__GetComponentRenderModelName::EndInvoke(System.IAsyncResult)
extern void _GetComponentRenderModelName_EndInvoke_mD37A36D2DFEDF7D500EFCF7CBCEFB247ABB3D8F0 ();
// 0x000008CB System.Void OVR.OpenVR.IVRRenderModels__GetComponentStateForDevicePath::.ctor(System.Object,System.IntPtr)
extern void _GetComponentStateForDevicePath__ctor_m50E29C5BBDD3BB33C2CC7F2F0BFE0571FD8DF1C8 ();
// 0x000008CC System.Boolean OVR.OpenVR.IVRRenderModels__GetComponentStateForDevicePath::Invoke(System.String,System.String,System.UInt64,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&)
extern void _GetComponentStateForDevicePath_Invoke_m924D938A8A14FC3D0E3A400FCDE8912B7715B15A ();
// 0x000008CD System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentStateForDevicePath::BeginInvoke(System.String,System.String,System.UInt64,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.AsyncCallback,System.Object)
extern void _GetComponentStateForDevicePath_BeginInvoke_m9F3C90D41C2338FD1C84265BFDBF92538FC22ED9 ();
// 0x000008CE System.Boolean OVR.OpenVR.IVRRenderModels__GetComponentStateForDevicePath::EndInvoke(OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.IAsyncResult)
extern void _GetComponentStateForDevicePath_EndInvoke_m752456100009E6E1AAFB9C7F1042E6D47C5BE140 ();
// 0x000008CF System.Void OVR.OpenVR.IVRRenderModels__GetComponentState::.ctor(System.Object,System.IntPtr)
extern void _GetComponentState__ctor_mD3078D2AE7E8EB2513875F6CA4CC0AC4510A6165 ();
// 0x000008D0 System.Boolean OVR.OpenVR.IVRRenderModels__GetComponentState::Invoke(System.String,System.String,OVR.OpenVR.VRControllerState_t&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&)
extern void _GetComponentState_Invoke_m4061A96F0D734C87E399A35A794EB2897D0E4568 ();
// 0x000008D1 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetComponentState::BeginInvoke(System.String,System.String,OVR.OpenVR.VRControllerState_t&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.AsyncCallback,System.Object)
extern void _GetComponentState_BeginInvoke_mB6C55CE5E9F3857CFB20CD776E06E0A8F9835999 ();
// 0x000008D2 System.Boolean OVR.OpenVR.IVRRenderModels__GetComponentState::EndInvoke(OVR.OpenVR.VRControllerState_t&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.IAsyncResult)
extern void _GetComponentState_EndInvoke_m057CBD32F68FD188A4CFA892EF6B0623470C8E59 ();
// 0x000008D3 System.Void OVR.OpenVR.IVRRenderModels__RenderModelHasComponent::.ctor(System.Object,System.IntPtr)
extern void _RenderModelHasComponent__ctor_mF09119F3FA1AC05667F24A82A02980F4C92C94F7 ();
// 0x000008D4 System.Boolean OVR.OpenVR.IVRRenderModels__RenderModelHasComponent::Invoke(System.String,System.String)
extern void _RenderModelHasComponent_Invoke_mE23AB5AF6970C13020D42FE35CB19EF4F34AB263 ();
// 0x000008D5 System.IAsyncResult OVR.OpenVR.IVRRenderModels__RenderModelHasComponent::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void _RenderModelHasComponent_BeginInvoke_m6840139073EE1917FC9E1A28A213C8BE43C07798 ();
// 0x000008D6 System.Boolean OVR.OpenVR.IVRRenderModels__RenderModelHasComponent::EndInvoke(System.IAsyncResult)
extern void _RenderModelHasComponent_EndInvoke_m45295C97D75C6D9274EFC24939BACFE80D194C77 ();
// 0x000008D7 System.Void OVR.OpenVR.IVRRenderModels__GetRenderModelThumbnailURL::.ctor(System.Object,System.IntPtr)
extern void _GetRenderModelThumbnailURL__ctor_mDE0723E2BCCF58C3A386C394FBE6635C33A4800A ();
// 0x000008D8 System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelThumbnailURL::Invoke(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&)
extern void _GetRenderModelThumbnailURL_Invoke_m26D16B4DEC5936AC1EEB3FD87EEFFDD801A92331 ();
// 0x000008D9 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetRenderModelThumbnailURL::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern void _GetRenderModelThumbnailURL_BeginInvoke_m73E7B8F2E047D49C8EBAF87B5CAD2D64E917505B ();
// 0x000008DA System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelThumbnailURL::EndInvoke(OVR.OpenVR.EVRRenderModelError&,System.IAsyncResult)
extern void _GetRenderModelThumbnailURL_EndInvoke_m22759841B1B5E49CDF2CA88842DF689C5111B151 ();
// 0x000008DB System.Void OVR.OpenVR.IVRRenderModels__GetRenderModelOriginalPath::.ctor(System.Object,System.IntPtr)
extern void _GetRenderModelOriginalPath__ctor_m2F304B30F5F56EECC15E622217B6537881C01E6D ();
// 0x000008DC System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelOriginalPath::Invoke(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&)
extern void _GetRenderModelOriginalPath_Invoke_mBBFE89ECAFC7864DFAF5F6A60100527818A68250 ();
// 0x000008DD System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetRenderModelOriginalPath::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern void _GetRenderModelOriginalPath_BeginInvoke_m0C66CE5FEF0AF2362AA9C611B33AE13FAE6EA3F3 ();
// 0x000008DE System.UInt32 OVR.OpenVR.IVRRenderModels__GetRenderModelOriginalPath::EndInvoke(OVR.OpenVR.EVRRenderModelError&,System.IAsyncResult)
extern void _GetRenderModelOriginalPath_EndInvoke_mAA2EE0B8D10F34F01DBEB76A9B9025DE0BFCAEEB ();
// 0x000008DF System.Void OVR.OpenVR.IVRRenderModels__GetRenderModelErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetRenderModelErrorNameFromEnum__ctor_m52F68EABC579A4FE9B3D8878E7F927831467D119 ();
// 0x000008E0 System.IntPtr OVR.OpenVR.IVRRenderModels__GetRenderModelErrorNameFromEnum::Invoke(OVR.OpenVR.EVRRenderModelError)
extern void _GetRenderModelErrorNameFromEnum_Invoke_m86410F7C2696F1E144C7C2FEBD2180A46B7624E6 ();
// 0x000008E1 System.IAsyncResult OVR.OpenVR.IVRRenderModels__GetRenderModelErrorNameFromEnum::BeginInvoke(OVR.OpenVR.EVRRenderModelError,System.AsyncCallback,System.Object)
extern void _GetRenderModelErrorNameFromEnum_BeginInvoke_m5C80002E1E46A41B8CCB26C2C030F3AB9550F6CC ();
// 0x000008E2 System.IntPtr OVR.OpenVR.IVRRenderModels__GetRenderModelErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetRenderModelErrorNameFromEnum_EndInvoke_m00D1C42A449272DAD713C306C4A87DAA4545E895 ();
// 0x000008E3 System.Void OVR.OpenVR.IVRNotifications__CreateNotification::.ctor(System.Object,System.IntPtr)
extern void _CreateNotification__ctor_mF810D8938CE8A6E43881F1870B72A46B93096F96 ();
// 0x000008E4 OVR.OpenVR.EVRNotificationError OVR.OpenVR.IVRNotifications__CreateNotification::Invoke(System.UInt64,System.UInt64,OVR.OpenVR.EVRNotificationType,System.String,OVR.OpenVR.EVRNotificationStyle,OVR.OpenVR.NotificationBitmap_t&,System.UInt32&)
extern void _CreateNotification_Invoke_mD27F82596F3A824F2927C318AB3EC7E6EAA42E40 ();
// 0x000008E5 System.IAsyncResult OVR.OpenVR.IVRNotifications__CreateNotification::BeginInvoke(System.UInt64,System.UInt64,OVR.OpenVR.EVRNotificationType,System.String,OVR.OpenVR.EVRNotificationStyle,OVR.OpenVR.NotificationBitmap_t&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _CreateNotification_BeginInvoke_m66D5001A39C6DEDBA22449AF2F8CC0F06ED6F1CB ();
// 0x000008E6 OVR.OpenVR.EVRNotificationError OVR.OpenVR.IVRNotifications__CreateNotification::EndInvoke(OVR.OpenVR.NotificationBitmap_t&,System.UInt32&,System.IAsyncResult)
extern void _CreateNotification_EndInvoke_m2F991F841DAFBB37BEA7F760B2B96A6AE79F3BB4 ();
// 0x000008E7 System.Void OVR.OpenVR.IVRNotifications__RemoveNotification::.ctor(System.Object,System.IntPtr)
extern void _RemoveNotification__ctor_mFCE7FB1E2E136EFE9C924E4C577074A19B1EE80D ();
// 0x000008E8 OVR.OpenVR.EVRNotificationError OVR.OpenVR.IVRNotifications__RemoveNotification::Invoke(System.UInt32)
extern void _RemoveNotification_Invoke_m8AA21A01341AC63F7D52539B6DDA5DEB6D9455FB ();
// 0x000008E9 System.IAsyncResult OVR.OpenVR.IVRNotifications__RemoveNotification::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void _RemoveNotification_BeginInvoke_m546B088397ED53612B787E37919DCF438BE60F1E ();
// 0x000008EA OVR.OpenVR.EVRNotificationError OVR.OpenVR.IVRNotifications__RemoveNotification::EndInvoke(System.IAsyncResult)
extern void _RemoveNotification_EndInvoke_mA98F63AC6E38C13ABBC89C3BD0DA275AC2B94806 ();
// 0x000008EB System.Void OVR.OpenVR.IVRSettings__GetSettingsErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern void _GetSettingsErrorNameFromEnum__ctor_mE589F8A4FAE6F6873C7BC286FC19AD6AA01FA78D ();
// 0x000008EC System.IntPtr OVR.OpenVR.IVRSettings__GetSettingsErrorNameFromEnum::Invoke(OVR.OpenVR.EVRSettingsError)
extern void _GetSettingsErrorNameFromEnum_Invoke_m8ED5180BEE96D9058C9F3324E3CDB405DE3D1F34 ();
// 0x000008ED System.IAsyncResult OVR.OpenVR.IVRSettings__GetSettingsErrorNameFromEnum::BeginInvoke(OVR.OpenVR.EVRSettingsError,System.AsyncCallback,System.Object)
extern void _GetSettingsErrorNameFromEnum_BeginInvoke_m6E3A9961F8165534902C0403DA494436F15A2370 ();
// 0x000008EE System.IntPtr OVR.OpenVR.IVRSettings__GetSettingsErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern void _GetSettingsErrorNameFromEnum_EndInvoke_m804F2CF8C6A61A076F4166A07FC6E71201327E1D ();
// 0x000008EF System.Void OVR.OpenVR.IVRSettings__Sync::.ctor(System.Object,System.IntPtr)
extern void _Sync__ctor_mA76AC4C95534FCB00644A4F50218CF73D1132900 ();
// 0x000008F0 System.Boolean OVR.OpenVR.IVRSettings__Sync::Invoke(System.Boolean,OVR.OpenVR.EVRSettingsError&)
extern void _Sync_Invoke_mB9AD7BC78BEB4FC3E32AE842183224F18917A197 ();
// 0x000008F1 System.IAsyncResult OVR.OpenVR.IVRSettings__Sync::BeginInvoke(System.Boolean,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _Sync_BeginInvoke_mF852A9B348FD4FD13C8B1B55965694BF24E3E0A5 ();
// 0x000008F2 System.Boolean OVR.OpenVR.IVRSettings__Sync::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _Sync_EndInvoke_mB6681200BD2F310F39BC40666F27856D7BE39DB6 ();
// 0x000008F3 System.Void OVR.OpenVR.IVRSettings__SetBool::.ctor(System.Object,System.IntPtr)
extern void _SetBool__ctor_m499F577942F2DAC6472A6E0C5600DEF2B0C5E309 ();
// 0x000008F4 System.Void OVR.OpenVR.IVRSettings__SetBool::Invoke(System.String,System.String,System.Boolean,OVR.OpenVR.EVRSettingsError&)
extern void _SetBool_Invoke_mC6941F6A38C4BEADCAAD8C11A0E3E943755C3927 ();
// 0x000008F5 System.IAsyncResult OVR.OpenVR.IVRSettings__SetBool::BeginInvoke(System.String,System.String,System.Boolean,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _SetBool_BeginInvoke_m56CA56FD78D0C58917A93B3E5B1979F453BF6CFA ();
// 0x000008F6 System.Void OVR.OpenVR.IVRSettings__SetBool::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _SetBool_EndInvoke_m8D578A6E20FA0DBEA3E720D60DF4E498568D6A0F ();
// 0x000008F7 System.Void OVR.OpenVR.IVRSettings__SetInt32::.ctor(System.Object,System.IntPtr)
extern void _SetInt32__ctor_mABBD242F2DB1EDCABDD1783597568A6B005439FE ();
// 0x000008F8 System.Void OVR.OpenVR.IVRSettings__SetInt32::Invoke(System.String,System.String,System.Int32,OVR.OpenVR.EVRSettingsError&)
extern void _SetInt32_Invoke_m3287CC5461338C29C166D9ACDB976251DDF545D8 ();
// 0x000008F9 System.IAsyncResult OVR.OpenVR.IVRSettings__SetInt32::BeginInvoke(System.String,System.String,System.Int32,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _SetInt32_BeginInvoke_mE2277AF9D9B94E041123E7A87F872E6C91FE6E3A ();
// 0x000008FA System.Void OVR.OpenVR.IVRSettings__SetInt32::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _SetInt32_EndInvoke_m5C26F82B44033AD04501C4ADF667B64F19E06770 ();
// 0x000008FB System.Void OVR.OpenVR.IVRSettings__SetFloat::.ctor(System.Object,System.IntPtr)
extern void _SetFloat__ctor_m1FFD3A05F64135AD7CA9E744C69531FAD97D8C18 ();
// 0x000008FC System.Void OVR.OpenVR.IVRSettings__SetFloat::Invoke(System.String,System.String,System.Single,OVR.OpenVR.EVRSettingsError&)
extern void _SetFloat_Invoke_mCB87D40FB3F5AFD5F5991A1B726D1966C940E12C ();
// 0x000008FD System.IAsyncResult OVR.OpenVR.IVRSettings__SetFloat::BeginInvoke(System.String,System.String,System.Single,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _SetFloat_BeginInvoke_m02B4C2DA63351DCCDA87DEB6DE27C2321078DF6E ();
// 0x000008FE System.Void OVR.OpenVR.IVRSettings__SetFloat::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _SetFloat_EndInvoke_mF521CA84371E9D27F47FA4D1E98D7B5FB5A93B0B ();
// 0x000008FF System.Void OVR.OpenVR.IVRSettings__SetString::.ctor(System.Object,System.IntPtr)
extern void _SetString__ctor_m66A0318642C76353E28B9D8D1DA1BFDF33EAC925 ();
// 0x00000900 System.Void OVR.OpenVR.IVRSettings__SetString::Invoke(System.String,System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void _SetString_Invoke_m2FD554712739C6A1862DFD58F70640521ABB04C5 ();
// 0x00000901 System.IAsyncResult OVR.OpenVR.IVRSettings__SetString::BeginInvoke(System.String,System.String,System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _SetString_BeginInvoke_m702F665733139079F77E9780DF0ED203A63B9851 ();
// 0x00000902 System.Void OVR.OpenVR.IVRSettings__SetString::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _SetString_EndInvoke_mD25EF8A4CF325D5B3351C712059849E039E3756F ();
// 0x00000903 System.Void OVR.OpenVR.IVRSettings__GetBool::.ctor(System.Object,System.IntPtr)
extern void _GetBool__ctor_mC39E50434C6DFEBDE3DFBECB283A20F50E4DCE48 ();
// 0x00000904 System.Boolean OVR.OpenVR.IVRSettings__GetBool::Invoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void _GetBool_Invoke_m3F3045C489BE3DD01D60B381519480379FB215ED ();
// 0x00000905 System.IAsyncResult OVR.OpenVR.IVRSettings__GetBool::BeginInvoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _GetBool_BeginInvoke_m7836255592B554B72EC1083C731BC3E59C3B2292 ();
// 0x00000906 System.Boolean OVR.OpenVR.IVRSettings__GetBool::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _GetBool_EndInvoke_m3B65552E51AA7A0FF7A89C48F2E4E85A9D9EBCDC ();
// 0x00000907 System.Void OVR.OpenVR.IVRSettings__GetInt32::.ctor(System.Object,System.IntPtr)
extern void _GetInt32__ctor_mC65AD668CDCC66E1D5B84213461896A674FFE356 ();
// 0x00000908 System.Int32 OVR.OpenVR.IVRSettings__GetInt32::Invoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void _GetInt32_Invoke_mDBDF2AA2C6F54BF8966FA6E8B7F817398426C4A0 ();
// 0x00000909 System.IAsyncResult OVR.OpenVR.IVRSettings__GetInt32::BeginInvoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _GetInt32_BeginInvoke_m015E8B7A19A5854F1E5E470B54809AB2AD98104A ();
// 0x0000090A System.Int32 OVR.OpenVR.IVRSettings__GetInt32::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _GetInt32_EndInvoke_mD7086FD605AE3BBD7045578184A7068D5C130E68 ();
// 0x0000090B System.Void OVR.OpenVR.IVRSettings__GetFloat::.ctor(System.Object,System.IntPtr)
extern void _GetFloat__ctor_m77344D18FBB1AE941D3F375812442BA20F73D952 ();
// 0x0000090C System.Single OVR.OpenVR.IVRSettings__GetFloat::Invoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void _GetFloat_Invoke_m096D02C3BB75317E9CD0EE82CABC2FDD3D6C4C48 ();
// 0x0000090D System.IAsyncResult OVR.OpenVR.IVRSettings__GetFloat::BeginInvoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _GetFloat_BeginInvoke_m256F196A0C75285A6A5627323D8594F3F943B5E0 ();
// 0x0000090E System.Single OVR.OpenVR.IVRSettings__GetFloat::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _GetFloat_EndInvoke_m7834E091775D167FA7A591123640ECDF0FDEDD48 ();
// 0x0000090F System.Void OVR.OpenVR.IVRSettings__GetString::.ctor(System.Object,System.IntPtr)
extern void _GetString__ctor_m4CDCAFC647C7AFA5620533AF7D4FFC1230E44109 ();
// 0x00000910 System.Void OVR.OpenVR.IVRSettings__GetString::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRSettingsError&)
extern void _GetString_Invoke_mCAE4FDBB326CF0DA13E2028E0676A86BE7F0C722 ();
// 0x00000911 System.IAsyncResult OVR.OpenVR.IVRSettings__GetString::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _GetString_BeginInvoke_m0CC3A728AD54DE86FC32FFD5ABE939D10BDB2C45 ();
// 0x00000912 System.Void OVR.OpenVR.IVRSettings__GetString::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _GetString_EndInvoke_mC9AA7F2308647F9C310F060C489AF4902DBD31A3 ();
// 0x00000913 System.Void OVR.OpenVR.IVRSettings__RemoveSection::.ctor(System.Object,System.IntPtr)
extern void _RemoveSection__ctor_mE6EEFC85BAA1337DFA69AE7DCEB5EDDEE427CF5C ();
// 0x00000914 System.Void OVR.OpenVR.IVRSettings__RemoveSection::Invoke(System.String,OVR.OpenVR.EVRSettingsError&)
extern void _RemoveSection_Invoke_mD8217E3568C163B8B1EFC7B90B65A28DD4DC3A82 ();
// 0x00000915 System.IAsyncResult OVR.OpenVR.IVRSettings__RemoveSection::BeginInvoke(System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _RemoveSection_BeginInvoke_mF8E3C3F8721D2E03044052C067D00AE198519B7C ();
// 0x00000916 System.Void OVR.OpenVR.IVRSettings__RemoveSection::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _RemoveSection_EndInvoke_m32D0937298CD9D053361CD2AB15A9894B935373D ();
// 0x00000917 System.Void OVR.OpenVR.IVRSettings__RemoveKeyInSection::.ctor(System.Object,System.IntPtr)
extern void _RemoveKeyInSection__ctor_mDFA208188AADDEAA0BB5E6BAA5F10BAEC3933DD2 ();
// 0x00000918 System.Void OVR.OpenVR.IVRSettings__RemoveKeyInSection::Invoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void _RemoveKeyInSection_Invoke_m7BD96B79CCA3504AA6932B6893B932202EE806C8 ();
// 0x00000919 System.IAsyncResult OVR.OpenVR.IVRSettings__RemoveKeyInSection::BeginInvoke(System.String,System.String,OVR.OpenVR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern void _RemoveKeyInSection_BeginInvoke_mE29918A47D4F25444B60D61406805F366E53E1CC ();
// 0x0000091A System.Void OVR.OpenVR.IVRSettings__RemoveKeyInSection::EndInvoke(OVR.OpenVR.EVRSettingsError&,System.IAsyncResult)
extern void _RemoveKeyInSection_EndInvoke_mDBDC90FBF1E5E6E3464388CF876AE64D5F5B7FFC ();
// 0x0000091B System.Void OVR.OpenVR.IVRScreenshots__RequestScreenshot::.ctor(System.Object,System.IntPtr)
extern void _RequestScreenshot__ctor_mEEC1B17A2D87D88DFE0BB120A239D27F6CD6C7BB ();
// 0x0000091C OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__RequestScreenshot::Invoke(System.UInt32&,OVR.OpenVR.EVRScreenshotType,System.String,System.String)
extern void _RequestScreenshot_Invoke_mB33CA212B36AB4D631C675F890A3B7C3653A11E3 ();
// 0x0000091D System.IAsyncResult OVR.OpenVR.IVRScreenshots__RequestScreenshot::BeginInvoke(System.UInt32&,OVR.OpenVR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern void _RequestScreenshot_BeginInvoke_mB23ACC31C3827048270BFB5BFDE9573820F17477 ();
// 0x0000091E OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__RequestScreenshot::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _RequestScreenshot_EndInvoke_m9BF3EFBA56A2C283020956EB719DAEBC310F3CF9 ();
// 0x0000091F System.Void OVR.OpenVR.IVRScreenshots__HookScreenshot::.ctor(System.Object,System.IntPtr)
extern void _HookScreenshot__ctor_m84BE72DEAB329DEEB44548365E955685CFE9E40D ();
// 0x00000920 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__HookScreenshot::Invoke(OVR.OpenVR.EVRScreenshotType[],System.Int32)
extern void _HookScreenshot_Invoke_m2B5CE1BEF0034368CE56DF8A22FE2298F8469343 ();
// 0x00000921 System.IAsyncResult OVR.OpenVR.IVRScreenshots__HookScreenshot::BeginInvoke(OVR.OpenVR.EVRScreenshotType[],System.Int32,System.AsyncCallback,System.Object)
extern void _HookScreenshot_BeginInvoke_m67C4AA4EA1AD71BAD9AB9ADFC1D454D6FD02BAF1 ();
// 0x00000922 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__HookScreenshot::EndInvoke(System.IAsyncResult)
extern void _HookScreenshot_EndInvoke_m07B2E63D7C7289BE544FFC0624D2AC0F83344262 ();
// 0x00000923 System.Void OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyType::.ctor(System.Object,System.IntPtr)
extern void _GetScreenshotPropertyType__ctor_mE0345A0B5E350DFEB9AFB547CDD4F8DB9978EA65 ();
// 0x00000924 OVR.OpenVR.EVRScreenshotType OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyType::Invoke(System.UInt32,OVR.OpenVR.EVRScreenshotError&)
extern void _GetScreenshotPropertyType_Invoke_mF8CD46B8AB894888A5661B00B1A5ECD0C8A41235 ();
// 0x00000925 System.IAsyncResult OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyType::BeginInvoke(System.UInt32,OVR.OpenVR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern void _GetScreenshotPropertyType_BeginInvoke_mA9DE84A7D2C1BF6C7674B3C23C867E3DB6864BD0 ();
// 0x00000926 OVR.OpenVR.EVRScreenshotType OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyType::EndInvoke(OVR.OpenVR.EVRScreenshotError&,System.IAsyncResult)
extern void _GetScreenshotPropertyType_EndInvoke_m9200F27491967157EA70B5BCEFE270BD6A304478 ();
// 0x00000927 System.Void OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyFilename::.ctor(System.Object,System.IntPtr)
extern void _GetScreenshotPropertyFilename__ctor_mCAB6F0C8B246F8726690438B030F17B5B630D0EC ();
// 0x00000928 System.UInt32 OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyFilename::Invoke(System.UInt32,OVR.OpenVR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRScreenshotError&)
extern void _GetScreenshotPropertyFilename_Invoke_m90A7C6C04A2A5BEFB6343E90959AA0D492DE30A7 ();
// 0x00000929 System.IAsyncResult OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyFilename::BeginInvoke(System.UInt32,OVR.OpenVR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern void _GetScreenshotPropertyFilename_BeginInvoke_m36351DA154B773A8CF8F2786CE62369D9A96BA76 ();
// 0x0000092A System.UInt32 OVR.OpenVR.IVRScreenshots__GetScreenshotPropertyFilename::EndInvoke(OVR.OpenVR.EVRScreenshotError&,System.IAsyncResult)
extern void _GetScreenshotPropertyFilename_EndInvoke_m64945FD3610C1A1A55C5250D20F429884558F7A9 ();
// 0x0000092B System.Void OVR.OpenVR.IVRScreenshots__UpdateScreenshotProgress::.ctor(System.Object,System.IntPtr)
extern void _UpdateScreenshotProgress__ctor_mD6841C55A41AF5134B993BB8DDA313A492DB21BC ();
// 0x0000092C OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__UpdateScreenshotProgress::Invoke(System.UInt32,System.Single)
extern void _UpdateScreenshotProgress_Invoke_m362F2BC9EF2C7826529BB98A0D0417A7D2F47B0B ();
// 0x0000092D System.IAsyncResult OVR.OpenVR.IVRScreenshots__UpdateScreenshotProgress::BeginInvoke(System.UInt32,System.Single,System.AsyncCallback,System.Object)
extern void _UpdateScreenshotProgress_BeginInvoke_mDC925F01D57F66BB74670B2E0EAA213BD9030CE3 ();
// 0x0000092E OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__UpdateScreenshotProgress::EndInvoke(System.IAsyncResult)
extern void _UpdateScreenshotProgress_EndInvoke_m7B3AB41FD87A36CAAEFC1375E3AFE08E0FA306DF ();
// 0x0000092F System.Void OVR.OpenVR.IVRScreenshots__TakeStereoScreenshot::.ctor(System.Object,System.IntPtr)
extern void _TakeStereoScreenshot__ctor_m60BFCE43061797F2B9775937825714A19E2D5DB6 ();
// 0x00000930 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__TakeStereoScreenshot::Invoke(System.UInt32&,System.String,System.String)
extern void _TakeStereoScreenshot_Invoke_mE8C5D569FFC056DA1AEB7269365E2C6A3A945112 ();
// 0x00000931 System.IAsyncResult OVR.OpenVR.IVRScreenshots__TakeStereoScreenshot::BeginInvoke(System.UInt32&,System.String,System.String,System.AsyncCallback,System.Object)
extern void _TakeStereoScreenshot_BeginInvoke_mA6BE453E226C343B4762DD59EC2B5B021D1A8364 ();
// 0x00000932 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__TakeStereoScreenshot::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _TakeStereoScreenshot_EndInvoke_m7304AFB00AB27D438CDC4962053A9ABF4F8025C0 ();
// 0x00000933 System.Void OVR.OpenVR.IVRScreenshots__SubmitScreenshot::.ctor(System.Object,System.IntPtr)
extern void _SubmitScreenshot__ctor_m6DA149ECE992839B9C946705710D37F0A42950ED ();
// 0x00000934 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__SubmitScreenshot::Invoke(System.UInt32,OVR.OpenVR.EVRScreenshotType,System.String,System.String)
extern void _SubmitScreenshot_Invoke_mB1B624AFF1810BB56C7F4561D77C44FD5BC83635 ();
// 0x00000935 System.IAsyncResult OVR.OpenVR.IVRScreenshots__SubmitScreenshot::BeginInvoke(System.UInt32,OVR.OpenVR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern void _SubmitScreenshot_BeginInvoke_m41BEEA9735E202235DBB43FE6C9CE524FA8FD02D ();
// 0x00000936 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.IVRScreenshots__SubmitScreenshot::EndInvoke(System.IAsyncResult)
extern void _SubmitScreenshot_EndInvoke_m1C2F2EECAC86A7A31C62592A2C14378B14AA188F ();
// 0x00000937 System.Void OVR.OpenVR.IVRResources__LoadSharedResource::.ctor(System.Object,System.IntPtr)
extern void _LoadSharedResource__ctor_m0117420D62F761DF6A7B5DD4C1A59D4293067C4E ();
// 0x00000938 System.UInt32 OVR.OpenVR.IVRResources__LoadSharedResource::Invoke(System.String,System.String,System.UInt32)
extern void _LoadSharedResource_Invoke_m4B62B034CD75DEEF0395BA3DE4289D26C3C47AA8 ();
// 0x00000939 System.IAsyncResult OVR.OpenVR.IVRResources__LoadSharedResource::BeginInvoke(System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern void _LoadSharedResource_BeginInvoke_m8BBCD3CC77F3D56126043774F6BC4EB4B108E2EF ();
// 0x0000093A System.UInt32 OVR.OpenVR.IVRResources__LoadSharedResource::EndInvoke(System.IAsyncResult)
extern void _LoadSharedResource_EndInvoke_mEF903A1922E1C834046FB84B5012DE6E317F93B5 ();
// 0x0000093B System.Void OVR.OpenVR.IVRResources__GetResourceFullPath::.ctor(System.Object,System.IntPtr)
extern void _GetResourceFullPath__ctor_m9E9CF4DEAB2B7CC915127D881BA7868E13F01266 ();
// 0x0000093C System.UInt32 OVR.OpenVR.IVRResources__GetResourceFullPath::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern void _GetResourceFullPath_Invoke_m5C56A9DF43CDC774D521611A33BCB9847127C16D ();
// 0x0000093D System.IAsyncResult OVR.OpenVR.IVRResources__GetResourceFullPath::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetResourceFullPath_BeginInvoke_m8810720C8711AB5FF69E0900CDB1E132A5CC6D2D ();
// 0x0000093E System.UInt32 OVR.OpenVR.IVRResources__GetResourceFullPath::EndInvoke(System.IAsyncResult)
extern void _GetResourceFullPath_EndInvoke_m30D1AB6E5121FD1FE45A7B9ABC13397C7AEB96C6 ();
// 0x0000093F System.Void OVR.OpenVR.IVRDriverManager__GetDriverCount::.ctor(System.Object,System.IntPtr)
extern void _GetDriverCount__ctor_m85F277B423F26C7C8A69C91570AD692C32B1D311 ();
// 0x00000940 System.UInt32 OVR.OpenVR.IVRDriverManager__GetDriverCount::Invoke()
extern void _GetDriverCount_Invoke_m5B3DAE84A60E24EA2190832FD1EEFAB77BC04D49 ();
// 0x00000941 System.IAsyncResult OVR.OpenVR.IVRDriverManager__GetDriverCount::BeginInvoke(System.AsyncCallback,System.Object)
extern void _GetDriverCount_BeginInvoke_mD6563F0C6035469D1302AA2A3A467935EFA47538 ();
// 0x00000942 System.UInt32 OVR.OpenVR.IVRDriverManager__GetDriverCount::EndInvoke(System.IAsyncResult)
extern void _GetDriverCount_EndInvoke_m13F75F39E56C9BC6C09B16305E67889FB03256A2 ();
// 0x00000943 System.Void OVR.OpenVR.IVRDriverManager__GetDriverName::.ctor(System.Object,System.IntPtr)
extern void _GetDriverName__ctor_mAA55D266ABD845C0E67B5A17A901B6ACFF4B5EC7 ();
// 0x00000944 System.UInt32 OVR.OpenVR.IVRDriverManager__GetDriverName::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void _GetDriverName_Invoke_m6F095C926F1AEDD40BA95F786AE931957C8D62CE ();
// 0x00000945 System.IAsyncResult OVR.OpenVR.IVRDriverManager__GetDriverName::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetDriverName_BeginInvoke_m4558634514E9252F905854A0EC9C5BEF8791EFB8 ();
// 0x00000946 System.UInt32 OVR.OpenVR.IVRDriverManager__GetDriverName::EndInvoke(System.IAsyncResult)
extern void _GetDriverName_EndInvoke_mE4E33C093868709DD4A361744EDF36CED6246578 ();
// 0x00000947 System.Void OVR.OpenVR.IVRDriverManager__GetDriverHandle::.ctor(System.Object,System.IntPtr)
extern void _GetDriverHandle__ctor_mFD7AD43C2571AB7A75063E538A57EB113EE5E3EF ();
// 0x00000948 System.UInt64 OVR.OpenVR.IVRDriverManager__GetDriverHandle::Invoke(System.String)
extern void _GetDriverHandle_Invoke_mC741AFC84102AEAD344AC5074455C6B46AA8B57A ();
// 0x00000949 System.IAsyncResult OVR.OpenVR.IVRDriverManager__GetDriverHandle::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _GetDriverHandle_BeginInvoke_mD32DDAE693582FEA2597951BE6EA2DBB00859EC8 ();
// 0x0000094A System.UInt64 OVR.OpenVR.IVRDriverManager__GetDriverHandle::EndInvoke(System.IAsyncResult)
extern void _GetDriverHandle_EndInvoke_m5EA15FBB967AFA33EBA97B28090EDD99CC1D0A44 ();
// 0x0000094B System.Void OVR.OpenVR.IVRInput__SetActionManifestPath::.ctor(System.Object,System.IntPtr)
extern void _SetActionManifestPath__ctor_mBDA42F4CB6A73D116F4EF82C3E6C6A81C2E6D63D ();
// 0x0000094C OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__SetActionManifestPath::Invoke(System.String)
extern void _SetActionManifestPath_Invoke_mA927319DFC9C89D70215B33D9C9A747DFEBE74E7 ();
// 0x0000094D System.IAsyncResult OVR.OpenVR.IVRInput__SetActionManifestPath::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void _SetActionManifestPath_BeginInvoke_mF7131ACB08E0510F0666AED7120D00218147E88B ();
// 0x0000094E OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__SetActionManifestPath::EndInvoke(System.IAsyncResult)
extern void _SetActionManifestPath_EndInvoke_mE22089E97D954796469EAC147608BA0F231F73C8 ();
// 0x0000094F System.Void OVR.OpenVR.IVRInput__GetActionSetHandle::.ctor(System.Object,System.IntPtr)
extern void _GetActionSetHandle__ctor_m9788922B610A6443477872FB192A13F8645A9510 ();
// 0x00000950 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionSetHandle::Invoke(System.String,System.UInt64&)
extern void _GetActionSetHandle_Invoke_mC7F228DD87BCFDBAAC644FEEA962159088A1823A ();
// 0x00000951 System.IAsyncResult OVR.OpenVR.IVRInput__GetActionSetHandle::BeginInvoke(System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern void _GetActionSetHandle_BeginInvoke_m3E9BFA83762436DE86D929E97A2B62313C9DA740 ();
// 0x00000952 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionSetHandle::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _GetActionSetHandle_EndInvoke_m5ED9CF0A18720CB15BB6CAF76599DF9AF1113E46 ();
// 0x00000953 System.Void OVR.OpenVR.IVRInput__GetActionHandle::.ctor(System.Object,System.IntPtr)
extern void _GetActionHandle__ctor_m166AEC6488E6C34FC7E0665EB4BB6DD279FBFCB5 ();
// 0x00000954 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionHandle::Invoke(System.String,System.UInt64&)
extern void _GetActionHandle_Invoke_mE9A67BE0386CD7533CB86F7A86778B0EB668F890 ();
// 0x00000955 System.IAsyncResult OVR.OpenVR.IVRInput__GetActionHandle::BeginInvoke(System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern void _GetActionHandle_BeginInvoke_mA736AF15C431A9B3BACA13BC5B1032D6D57AD60E ();
// 0x00000956 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionHandle::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _GetActionHandle_EndInvoke_mA4C3295EACB991CAE705297594B224D34522FE43 ();
// 0x00000957 System.Void OVR.OpenVR.IVRInput__GetInputSourceHandle::.ctor(System.Object,System.IntPtr)
extern void _GetInputSourceHandle__ctor_m992F2F6166FC9B1390D630D2747AA8A1176294F0 ();
// 0x00000958 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetInputSourceHandle::Invoke(System.String,System.UInt64&)
extern void _GetInputSourceHandle_Invoke_m51C1A05C888684C0B15BD9D028BD6FB31F95C39A ();
// 0x00000959 System.IAsyncResult OVR.OpenVR.IVRInput__GetInputSourceHandle::BeginInvoke(System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern void _GetInputSourceHandle_BeginInvoke_mDEAF0DEF1E7E5CFCEE47311661E9895D92E61AE3 ();
// 0x0000095A OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetInputSourceHandle::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _GetInputSourceHandle_EndInvoke_mEC34EC7C9AEBFDAB6177CC686943A21EBF133C87 ();
// 0x0000095B System.Void OVR.OpenVR.IVRInput__UpdateActionState::.ctor(System.Object,System.IntPtr)
extern void _UpdateActionState__ctor_mCBB4CEF3E973C5AAF67AFCEFDA7DA5742D917B27 ();
// 0x0000095C OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__UpdateActionState::Invoke(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32,System.UInt32)
extern void _UpdateActionState_Invoke_m4DE270627046CE992C58CF512A3685804B229C27 ();
// 0x0000095D System.IAsyncResult OVR.OpenVR.IVRInput__UpdateActionState::BeginInvoke(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern void _UpdateActionState_BeginInvoke_mAC994A48687A9E3263D502B739F4EEEF0816BACC ();
// 0x0000095E OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__UpdateActionState::EndInvoke(System.IAsyncResult)
extern void _UpdateActionState_EndInvoke_m8A13215D972EE9016C58895DF81A7E312A04C39A ();
// 0x0000095F System.Void OVR.OpenVR.IVRInput__GetDigitalActionData::.ctor(System.Object,System.IntPtr)
extern void _GetDigitalActionData__ctor_m7C999081D19EA1D6551633C1522681CDB73E161F ();
// 0x00000960 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetDigitalActionData::Invoke(System.UInt64,OVR.OpenVR.InputDigitalActionData_t&,System.UInt32,System.UInt64)
extern void _GetDigitalActionData_Invoke_m15F10297BCD44BD5655CF6F459BC9C3050825DAA ();
// 0x00000961 System.IAsyncResult OVR.OpenVR.IVRInput__GetDigitalActionData::BeginInvoke(System.UInt64,OVR.OpenVR.InputDigitalActionData_t&,System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetDigitalActionData_BeginInvoke_m03D0855B960F95E3FEAB98B3B3A1C2A7F9F95CFB ();
// 0x00000962 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetDigitalActionData::EndInvoke(OVR.OpenVR.InputDigitalActionData_t&,System.IAsyncResult)
extern void _GetDigitalActionData_EndInvoke_m10A1B6FD3CABF9F82CD5D13D4F658BC8277CA660 ();
// 0x00000963 System.Void OVR.OpenVR.IVRInput__GetAnalogActionData::.ctor(System.Object,System.IntPtr)
extern void _GetAnalogActionData__ctor_mC7E6E7B52E7C89525805C738A19D70419A216F18 ();
// 0x00000964 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetAnalogActionData::Invoke(System.UInt64,OVR.OpenVR.InputAnalogActionData_t&,System.UInt32,System.UInt64)
extern void _GetAnalogActionData_Invoke_mF1043DB40F4E5C8BD31688E5B499611F91B303BF ();
// 0x00000965 System.IAsyncResult OVR.OpenVR.IVRInput__GetAnalogActionData::BeginInvoke(System.UInt64,OVR.OpenVR.InputAnalogActionData_t&,System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetAnalogActionData_BeginInvoke_mFD1811901FCE970CADD8A75D9BEE827F002A954D ();
// 0x00000966 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetAnalogActionData::EndInvoke(OVR.OpenVR.InputAnalogActionData_t&,System.IAsyncResult)
extern void _GetAnalogActionData_EndInvoke_mD9DCCD49A432958F65B1A9FD89B6DE8B96C13353 ();
// 0x00000967 System.Void OVR.OpenVR.IVRInput__GetPoseActionData::.ctor(System.Object,System.IntPtr)
extern void _GetPoseActionData__ctor_m92CFC2B4FDCCF7C9A8C61DF88967A1059E446DDE ();
// 0x00000968 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetPoseActionData::Invoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.InputPoseActionData_t&,System.UInt32,System.UInt64)
extern void _GetPoseActionData_Invoke_m016C739E4381E5D4F7DEE376ADDCC8B3FFD4E6C7 ();
// 0x00000969 System.IAsyncResult OVR.OpenVR.IVRInput__GetPoseActionData::BeginInvoke(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.InputPoseActionData_t&,System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetPoseActionData_BeginInvoke_mB3E0E1551DB1E48F8C6832BF5FC693E0937A8752 ();
// 0x0000096A OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetPoseActionData::EndInvoke(OVR.OpenVR.InputPoseActionData_t&,System.IAsyncResult)
extern void _GetPoseActionData_EndInvoke_mA0D4ACED408AF7E96B24445CC1D0649A487A8BC8 ();
// 0x0000096B System.Void OVR.OpenVR.IVRInput__GetSkeletalActionData::.ctor(System.Object,System.IntPtr)
extern void _GetSkeletalActionData__ctor_m6BB68B75FF58F0A5BF015A838D3F1204211400AD ();
// 0x0000096C OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalActionData::Invoke(System.UInt64,OVR.OpenVR.InputSkeletalActionData_t&,System.UInt32,System.UInt64)
extern void _GetSkeletalActionData_Invoke_mE57966796C5BE36FFE214BBC0F348A8B787B1AE6 ();
// 0x0000096D System.IAsyncResult OVR.OpenVR.IVRInput__GetSkeletalActionData::BeginInvoke(System.UInt64,OVR.OpenVR.InputSkeletalActionData_t&,System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetSkeletalActionData_BeginInvoke_mB0B12E82C8730340B6A3561D1BB575AA2C10E991 ();
// 0x0000096E OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalActionData::EndInvoke(OVR.OpenVR.InputSkeletalActionData_t&,System.IAsyncResult)
extern void _GetSkeletalActionData_EndInvoke_m4F26CB5E8F87AFA0AF0DC43AC07FF8EE81366A3D ();
// 0x0000096F System.Void OVR.OpenVR.IVRInput__GetSkeletalBoneData::.ctor(System.Object,System.IntPtr)
extern void _GetSkeletalBoneData__ctor_m6B3389C5C4665A3E0C5DACA5C9FA2B30F17535D2 ();
// 0x00000970 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalBoneData::Invoke(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,OVR.OpenVR.VRBoneTransform_t[],System.UInt32,System.UInt64)
extern void _GetSkeletalBoneData_Invoke_mC44B0E5F77A2C98FFEC93D08E505EA0985F2D35A ();
// 0x00000971 System.IAsyncResult OVR.OpenVR.IVRInput__GetSkeletalBoneData::BeginInvoke(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,OVR.OpenVR.VRBoneTransform_t[],System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetSkeletalBoneData_BeginInvoke_m539DCAB13C20E2DA46F4435589DE0C291CDDAA72 ();
// 0x00000972 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalBoneData::EndInvoke(System.IAsyncResult)
extern void _GetSkeletalBoneData_EndInvoke_mF6681A2DC873ED4DACCB5982151427EBBDBD5944 ();
// 0x00000973 System.Void OVR.OpenVR.IVRInput__GetSkeletalBoneDataCompressed::.ctor(System.Object,System.IntPtr)
extern void _GetSkeletalBoneDataCompressed__ctor_m8FB0D5F6FA6B001D7FD280FFAB724538F4BB1581 ();
// 0x00000974 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalBoneDataCompressed::Invoke(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,System.IntPtr,System.UInt32,System.UInt32&,System.UInt64)
extern void _GetSkeletalBoneDataCompressed_Invoke_m16716B0602B22A27DA77EA7C32AFBD2DA671F85A ();
// 0x00000975 System.IAsyncResult OVR.OpenVR.IVRInput__GetSkeletalBoneDataCompressed::BeginInvoke(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,System.IntPtr,System.UInt32,System.UInt32&,System.UInt64,System.AsyncCallback,System.Object)
extern void _GetSkeletalBoneDataCompressed_BeginInvoke_m5928CD244897EDFF7E436704ED37E885075AA6B6 ();
// 0x00000976 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetSkeletalBoneDataCompressed::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetSkeletalBoneDataCompressed_EndInvoke_m1AFAF87BC33992CC03BCA8F0E74E4D57A8002E06 ();
// 0x00000977 System.Void OVR.OpenVR.IVRInput__DecompressSkeletalBoneData::.ctor(System.Object,System.IntPtr)
extern void _DecompressSkeletalBoneData__ctor_mB762A7CF065CDEA3D2B97C2D4ED23DC4DD131A92 ();
// 0x00000978 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__DecompressSkeletalBoneData::Invoke(System.IntPtr,System.UInt32,OVR.OpenVR.EVRSkeletalTransformSpace&,OVR.OpenVR.VRBoneTransform_t[],System.UInt32)
extern void _DecompressSkeletalBoneData_Invoke_mCBA1157817D3B9747AAFF6BDD7AB7328DB438D02 ();
// 0x00000979 System.IAsyncResult OVR.OpenVR.IVRInput__DecompressSkeletalBoneData::BeginInvoke(System.IntPtr,System.UInt32,OVR.OpenVR.EVRSkeletalTransformSpace&,OVR.OpenVR.VRBoneTransform_t[],System.UInt32,System.AsyncCallback,System.Object)
extern void _DecompressSkeletalBoneData_BeginInvoke_m63A18E0450F3EFFD89CCDF5F610D45CCA4971556 ();
// 0x0000097A OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__DecompressSkeletalBoneData::EndInvoke(OVR.OpenVR.EVRSkeletalTransformSpace&,System.IAsyncResult)
extern void _DecompressSkeletalBoneData_EndInvoke_m8A1B464FD10193DAA9304166BE1B1C68DAF4E8CA ();
// 0x0000097B System.Void OVR.OpenVR.IVRInput__TriggerHapticVibrationAction::.ctor(System.Object,System.IntPtr)
extern void _TriggerHapticVibrationAction__ctor_m1728125E656BE179825C75BBC7A70A06DE65014A ();
// 0x0000097C OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__TriggerHapticVibrationAction::Invoke(System.UInt64,System.Single,System.Single,System.Single,System.Single,System.UInt64)
extern void _TriggerHapticVibrationAction_Invoke_mEBB4C9F2912BD3613EFFE2EC4A30E5015BD11ABD ();
// 0x0000097D System.IAsyncResult OVR.OpenVR.IVRInput__TriggerHapticVibrationAction::BeginInvoke(System.UInt64,System.Single,System.Single,System.Single,System.Single,System.UInt64,System.AsyncCallback,System.Object)
extern void _TriggerHapticVibrationAction_BeginInvoke_m8598EE2291794CDF50B8B8C61BA21A33975D1DFC ();
// 0x0000097E OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__TriggerHapticVibrationAction::EndInvoke(System.IAsyncResult)
extern void _TriggerHapticVibrationAction_EndInvoke_mCCF420140CB3D0017CDE27D71165DF0F403AC371 ();
// 0x0000097F System.Void OVR.OpenVR.IVRInput__GetActionOrigins::.ctor(System.Object,System.IntPtr)
extern void _GetActionOrigins__ctor_m4DE0F00231B229B89F36F47BB81BAFC3C5611E7C ();
// 0x00000980 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionOrigins::Invoke(System.UInt64,System.UInt64,System.UInt64[],System.UInt32)
extern void _GetActionOrigins_Invoke_mBFDEB2A34B77428DA11C9B051E4EFA3A295EC986 ();
// 0x00000981 System.IAsyncResult OVR.OpenVR.IVRInput__GetActionOrigins::BeginInvoke(System.UInt64,System.UInt64,System.UInt64[],System.UInt32,System.AsyncCallback,System.Object)
extern void _GetActionOrigins_BeginInvoke_m087CFABDF82A1A7EA7D044A9320DC960A26B0C3B ();
// 0x00000982 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetActionOrigins::EndInvoke(System.IAsyncResult)
extern void _GetActionOrigins_EndInvoke_mDCC74CB33E619A7DCAA1FAE1BE8512629A799547 ();
// 0x00000983 System.Void OVR.OpenVR.IVRInput__GetOriginLocalizedName::.ctor(System.Object,System.IntPtr)
extern void _GetOriginLocalizedName__ctor_m3A500313DD259B70F5AFABFF3213E61ED5218560 ();
// 0x00000984 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetOriginLocalizedName::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32)
extern void _GetOriginLocalizedName_Invoke_m17F7733E8117EDBC145E09A3CFBDDD2603295E54 ();
// 0x00000985 System.IAsyncResult OVR.OpenVR.IVRInput__GetOriginLocalizedName::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetOriginLocalizedName_BeginInvoke_m4F33EF3A93340230DB33FBE43671B94AC43759B8 ();
// 0x00000986 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetOriginLocalizedName::EndInvoke(System.IAsyncResult)
extern void _GetOriginLocalizedName_EndInvoke_mF38052FD677BB2C30189992ED685EA9CE7552BCA ();
// 0x00000987 System.Void OVR.OpenVR.IVRInput__GetOriginTrackedDeviceInfo::.ctor(System.Object,System.IntPtr)
extern void _GetOriginTrackedDeviceInfo__ctor_mB29F991D7D7D3A1AF6456B7C4C2B145277FAF264 ();
// 0x00000988 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetOriginTrackedDeviceInfo::Invoke(System.UInt64,OVR.OpenVR.InputOriginInfo_t&,System.UInt32)
extern void _GetOriginTrackedDeviceInfo_Invoke_m7E4B43D751C539738E8FF3EC98335B9804239857 ();
// 0x00000989 System.IAsyncResult OVR.OpenVR.IVRInput__GetOriginTrackedDeviceInfo::BeginInvoke(System.UInt64,OVR.OpenVR.InputOriginInfo_t&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetOriginTrackedDeviceInfo_BeginInvoke_m344F7B9E80F24141AB9A4803C48BCE0AFAF19900 ();
// 0x0000098A OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__GetOriginTrackedDeviceInfo::EndInvoke(OVR.OpenVR.InputOriginInfo_t&,System.IAsyncResult)
extern void _GetOriginTrackedDeviceInfo_EndInvoke_m76AC7711260D0287E6AF029995386C4CCBE7028A ();
// 0x0000098B System.Void OVR.OpenVR.IVRInput__ShowActionOrigins::.ctor(System.Object,System.IntPtr)
extern void _ShowActionOrigins__ctor_mEE24447B626CB66F5D05AAA458980252D76409A3 ();
// 0x0000098C OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__ShowActionOrigins::Invoke(System.UInt64,System.UInt64)
extern void _ShowActionOrigins_Invoke_m42F6268ED89CA85AA3D4744E1E18F5D8E79A5299 ();
// 0x0000098D System.IAsyncResult OVR.OpenVR.IVRInput__ShowActionOrigins::BeginInvoke(System.UInt64,System.UInt64,System.AsyncCallback,System.Object)
extern void _ShowActionOrigins_BeginInvoke_m6BE471D4A847EF030080040225C037105906FEEA ();
// 0x0000098E OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__ShowActionOrigins::EndInvoke(System.IAsyncResult)
extern void _ShowActionOrigins_EndInvoke_m15AA37E61C73BF44C9C4F9BA5E934DCD21E8556F ();
// 0x0000098F System.Void OVR.OpenVR.IVRInput__ShowBindingsForActionSet::.ctor(System.Object,System.IntPtr)
extern void _ShowBindingsForActionSet__ctor_mBDA0908B56E4FCE00F7D068D3B8BC623501B9855 ();
// 0x00000990 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__ShowBindingsForActionSet::Invoke(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32,System.UInt32,System.UInt64)
extern void _ShowBindingsForActionSet_Invoke_mC240D324B0D619597B8D64A848EC49B1C8D86130 ();
// 0x00000991 System.IAsyncResult OVR.OpenVR.IVRInput__ShowBindingsForActionSet::BeginInvoke(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32,System.UInt32,System.UInt64,System.AsyncCallback,System.Object)
extern void _ShowBindingsForActionSet_BeginInvoke_m944557D1ADA85B229ED30F5B2C3B1B276A838DA3 ();
// 0x00000992 OVR.OpenVR.EVRInputError OVR.OpenVR.IVRInput__ShowBindingsForActionSet::EndInvoke(System.IAsyncResult)
extern void _ShowBindingsForActionSet_EndInvoke_mE270C3A50EFF3A99F7E25C81C64A7F2CAC7A5CF3 ();
// 0x00000993 System.Void OVR.OpenVR.IVRIOBuffer__Open::.ctor(System.Object,System.IntPtr)
extern void _Open__ctor_m2FC011AB1A37AE4040C73B801644A29F6604FB00 ();
// 0x00000994 OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Open::Invoke(System.String,OVR.OpenVR.EIOBufferMode,System.UInt32,System.UInt32,System.UInt64&)
extern void _Open_Invoke_m1A2797E8B4E9C586C4942E26AF855D3D0B0E4EDB ();
// 0x00000995 System.IAsyncResult OVR.OpenVR.IVRIOBuffer__Open::BeginInvoke(System.String,OVR.OpenVR.EIOBufferMode,System.UInt32,System.UInt32,System.UInt64&,System.AsyncCallback,System.Object)
extern void _Open_BeginInvoke_mD9380C9012D886BBD3FF7E6652CE003A35853D80 ();
// 0x00000996 OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Open::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void _Open_EndInvoke_m00D1CB56895F98A938533F7445981E24D3406AD2 ();
// 0x00000997 System.Void OVR.OpenVR.IVRIOBuffer__Close::.ctor(System.Object,System.IntPtr)
extern void _Close__ctor_m7FF71BF7078445866EF41F876E1674A35D609131 ();
// 0x00000998 OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Close::Invoke(System.UInt64)
extern void _Close_Invoke_mDEC973EF48BBC719C41449F7E031067368B42AA0 ();
// 0x00000999 System.IAsyncResult OVR.OpenVR.IVRIOBuffer__Close::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _Close_BeginInvoke_m2BC162C96DBF7C5FAB5722E2986EF8FABC40FE31 ();
// 0x0000099A OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Close::EndInvoke(System.IAsyncResult)
extern void _Close_EndInvoke_m9869BE158F32D057F3CED444384BD20AC0009EEC ();
// 0x0000099B System.Void OVR.OpenVR.IVRIOBuffer__Read::.ctor(System.Object,System.IntPtr)
extern void _Read__ctor_m7F8FEA23B4894A5619594DEFE5C4EF7E3126A889 ();
// 0x0000099C OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Read::Invoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&)
extern void _Read_Invoke_m007C981BB0E664D85B9FF6357EFBAFC68CB206FD ();
// 0x0000099D System.IAsyncResult OVR.OpenVR.IVRIOBuffer__Read::BeginInvoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&,System.AsyncCallback,System.Object)
extern void _Read_BeginInvoke_mB2CC226866FA963D8DA09AF09665EB0774FD2B23 ();
// 0x0000099E OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Read::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _Read_EndInvoke_m1010FB6DC93828F8DE6A3CD9C50CC1492B6DB19F ();
// 0x0000099F System.Void OVR.OpenVR.IVRIOBuffer__Write::.ctor(System.Object,System.IntPtr)
extern void _Write__ctor_m9E1272735D1D65D06790D5619A30F98FB0F588CE ();
// 0x000009A0 OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Write::Invoke(System.UInt64,System.IntPtr,System.UInt32)
extern void _Write_Invoke_m197BCB2FE9F66CDB5878B36D5CDA3A626BE6332F ();
// 0x000009A1 System.IAsyncResult OVR.OpenVR.IVRIOBuffer__Write::BeginInvoke(System.UInt64,System.IntPtr,System.UInt32,System.AsyncCallback,System.Object)
extern void _Write_BeginInvoke_m5B05FE12F3BB8AA80B6B2E5CC5B3C33B6BA68910 ();
// 0x000009A2 OVR.OpenVR.EIOBufferError OVR.OpenVR.IVRIOBuffer__Write::EndInvoke(System.IAsyncResult)
extern void _Write_EndInvoke_mCDAF9C96AA317498872D0211F611AA9EABE9FB43 ();
// 0x000009A3 System.Void OVR.OpenVR.IVRIOBuffer__PropertyContainer::.ctor(System.Object,System.IntPtr)
extern void _PropertyContainer__ctor_mB48F699F7F6EFD4D095CD91242745F5F2912F3C6 ();
// 0x000009A4 System.UInt64 OVR.OpenVR.IVRIOBuffer__PropertyContainer::Invoke(System.UInt64)
extern void _PropertyContainer_Invoke_m5FF10619A007FB65CC7B661587BBB63E1300E035 ();
// 0x000009A5 System.IAsyncResult OVR.OpenVR.IVRIOBuffer__PropertyContainer::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void _PropertyContainer_BeginInvoke_m749D93BC2385AEB637E22D3D11386F00BFB194F0 ();
// 0x000009A6 System.UInt64 OVR.OpenVR.IVRIOBuffer__PropertyContainer::EndInvoke(System.IAsyncResult)
extern void _PropertyContainer_EndInvoke_mCDE008092C39939932EC1EBB9E99E4BA96497F82 ();
// 0x000009A7 System.Void OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromDescriptor::.ctor(System.Object,System.IntPtr)
extern void _CreateSpatialAnchorFromDescriptor__ctor_mD452AA9A9B00A381253D40E548CD11E3F4778982 ();
// 0x000009A8 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromDescriptor::Invoke(System.String,System.UInt32&)
extern void _CreateSpatialAnchorFromDescriptor_Invoke_mC858B0398D4467F7A3FA9B69DE8DC53174D386A0 ();
// 0x000009A9 System.IAsyncResult OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromDescriptor::BeginInvoke(System.String,System.UInt32&,System.AsyncCallback,System.Object)
extern void _CreateSpatialAnchorFromDescriptor_BeginInvoke_m43EC2F2FE96A07625050EA8EFFC9FBB3D9E2DABC ();
// 0x000009AA OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromDescriptor::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _CreateSpatialAnchorFromDescriptor_EndInvoke_m2917D63E416E57490B2E54791EBD0981C517E395 ();
// 0x000009AB System.Void OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromPose::.ctor(System.Object,System.IntPtr)
extern void _CreateSpatialAnchorFromPose__ctor_mDF112FE98437171D35E4DD8D2A2EC3524D209AE8 ();
// 0x000009AC OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromPose::Invoke(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&,System.UInt32&)
extern void _CreateSpatialAnchorFromPose_Invoke_m99F078E0A862F752AC5EF557048EBC706F0792F4 ();
// 0x000009AD System.IAsyncResult OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromPose::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&,System.UInt32&,System.AsyncCallback,System.Object)
extern void _CreateSpatialAnchorFromPose_BeginInvoke_m400A1168710BF0350D10016F8C10B7EAD3CBF6C6 ();
// 0x000009AE OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__CreateSpatialAnchorFromPose::EndInvoke(OVR.OpenVR.SpatialAnchorPose_t&,System.UInt32&,System.IAsyncResult)
extern void _CreateSpatialAnchorFromPose_EndInvoke_m7FF8B819FF62D4A29BD48732C1EB677519FDFE25 ();
// 0x000009AF System.Void OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorPose::.ctor(System.Object,System.IntPtr)
extern void _GetSpatialAnchorPose__ctor_m0DB7EB2F298BC3D34C89BC94A2CDDB3EC88DC2DF ();
// 0x000009B0 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorPose::Invoke(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&)
extern void _GetSpatialAnchorPose_Invoke_m8DDEACBDEA0FD8123BCAB195C75F5EC07314D5F9 ();
// 0x000009B1 System.IAsyncResult OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorPose::BeginInvoke(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&,System.AsyncCallback,System.Object)
extern void _GetSpatialAnchorPose_BeginInvoke_m25A23E61CD2443975E92B824736D5CB4FC64A635 ();
// 0x000009B2 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorPose::EndInvoke(OVR.OpenVR.SpatialAnchorPose_t&,System.IAsyncResult)
extern void _GetSpatialAnchorPose_EndInvoke_m89E274D1CCA1AE0CE881D3208183E06EA408BA8B ();
// 0x000009B3 System.Void OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorDescriptor::.ctor(System.Object,System.IntPtr)
extern void _GetSpatialAnchorDescriptor__ctor_mC917DFF43B19A710BFD315743305E9462B6273CF ();
// 0x000009B4 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorDescriptor::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32&)
extern void _GetSpatialAnchorDescriptor_Invoke_m1FCDD9D757299ECA528DE338A6B13B14BDA4C8F9 ();
// 0x000009B5 System.IAsyncResult OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorDescriptor::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32&,System.AsyncCallback,System.Object)
extern void _GetSpatialAnchorDescriptor_BeginInvoke_m37CAE8036505EEC48C8280439E18B0717A1E7D6A ();
// 0x000009B6 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.IVRSpatialAnchors__GetSpatialAnchorDescriptor::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void _GetSpatialAnchorDescriptor_EndInvoke_m8CF65A0B348D06E364765D4B3CBDFE58585B5F37 ();
// 0x000009B7 System.Void OVR.OpenVR.CVRSystem::.ctor(System.IntPtr)
extern void CVRSystem__ctor_m8FCC9BD7F9B566DA066E60BFB243102C3103018A ();
// 0x000009B8 System.Void OVR.OpenVR.CVRSystem::GetRecommendedRenderTargetSize(System.UInt32&,System.UInt32&)
extern void CVRSystem_GetRecommendedRenderTargetSize_mD441ECC64DFC5BA364A7FD8701D98F25F8BC338D ();
// 0x000009B9 OVR.OpenVR.HmdMatrix44_t OVR.OpenVR.CVRSystem::GetProjectionMatrix(OVR.OpenVR.EVREye,System.Single,System.Single)
extern void CVRSystem_GetProjectionMatrix_m1C9AC3850AA9EA8B1DA48D369CD1C8C6C607D532 ();
// 0x000009BA System.Void OVR.OpenVR.CVRSystem::GetProjectionRaw(OVR.OpenVR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&)
extern void CVRSystem_GetProjectionRaw_m5F99F1FE634FD7A638939628942DD83CCC72F602 ();
// 0x000009BB System.Boolean OVR.OpenVR.CVRSystem::ComputeDistortion(OVR.OpenVR.EVREye,System.Single,System.Single,OVR.OpenVR.DistortionCoordinates_t&)
extern void CVRSystem_ComputeDistortion_m27FC45ADA2305AD1D7DB5C61390485C1BFEE36C7 ();
// 0x000009BC OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.CVRSystem::GetEyeToHeadTransform(OVR.OpenVR.EVREye)
extern void CVRSystem_GetEyeToHeadTransform_mA1AA5A712366E0C943A872DDB9E47CD1991372F1 ();
// 0x000009BD System.Boolean OVR.OpenVR.CVRSystem::GetTimeSinceLastVsync(System.Single&,System.UInt64&)
extern void CVRSystem_GetTimeSinceLastVsync_m497AE07C1570B2961E9863331D4654B0BC0B0BCB ();
// 0x000009BE System.Int32 OVR.OpenVR.CVRSystem::GetD3D9AdapterIndex()
extern void CVRSystem_GetD3D9AdapterIndex_m866A065A21065B8523BA4E29BFFE2E2162B82F61 ();
// 0x000009BF System.Void OVR.OpenVR.CVRSystem::GetDXGIOutputInfo(System.Int32&)
extern void CVRSystem_GetDXGIOutputInfo_mD06FFD9DB231196EE4C6FB86541C065096BA76BF ();
// 0x000009C0 System.Void OVR.OpenVR.CVRSystem::GetOutputDevice(System.UInt64&,OVR.OpenVR.ETextureType,System.IntPtr)
extern void CVRSystem_GetOutputDevice_m3100238E25E01924A4D8D678E8ED5547126B8296 ();
// 0x000009C1 System.Boolean OVR.OpenVR.CVRSystem::IsDisplayOnDesktop()
extern void CVRSystem_IsDisplayOnDesktop_mBDF6386424BFC32835A72A2910D6DD9AE31C9E01 ();
// 0x000009C2 System.Boolean OVR.OpenVR.CVRSystem::SetDisplayVisibility(System.Boolean)
extern void CVRSystem_SetDisplayVisibility_m7C096A0304BD0D75817A16E9F2490FE92C7C3A42 ();
// 0x000009C3 System.Void OVR.OpenVR.CVRSystem::GetDeviceToAbsoluteTrackingPose(OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.TrackedDevicePose_t[])
extern void CVRSystem_GetDeviceToAbsoluteTrackingPose_m71877595C5BB6E4503B5F5F9BC4401B60B35339A ();
// 0x000009C4 System.Void OVR.OpenVR.CVRSystem::ResetSeatedZeroPose()
extern void CVRSystem_ResetSeatedZeroPose_m65D7655C353E0A4F04F715C19A9FBF0FD6D0C59B ();
// 0x000009C5 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.CVRSystem::GetSeatedZeroPoseToStandingAbsoluteTrackingPose()
extern void CVRSystem_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_m572E6F0BFA580A289D104AD59A1BE6070850EF66 ();
// 0x000009C6 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.CVRSystem::GetRawZeroPoseToStandingAbsoluteTrackingPose()
extern void CVRSystem_GetRawZeroPoseToStandingAbsoluteTrackingPose_m9067E136FE7C1DF3D42CBF40E14EC27FCA38BB89 ();
// 0x000009C7 System.UInt32 OVR.OpenVR.CVRSystem::GetSortedTrackedDeviceIndicesOfClass(OVR.OpenVR.ETrackedDeviceClass,System.UInt32[],System.UInt32)
extern void CVRSystem_GetSortedTrackedDeviceIndicesOfClass_m480099804814374D87CE808FF0768DC05836D8D8 ();
// 0x000009C8 OVR.OpenVR.EDeviceActivityLevel OVR.OpenVR.CVRSystem::GetTrackedDeviceActivityLevel(System.UInt32)
extern void CVRSystem_GetTrackedDeviceActivityLevel_m9254C0CEE91B09F46E6C90BCD2D9BC9B1F6400F3 ();
// 0x000009C9 System.Void OVR.OpenVR.CVRSystem::ApplyTransform(OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.HmdMatrix34_t&)
extern void CVRSystem_ApplyTransform_mC2A1A08DB6C52C78BC2EC5BB3BB74D4B35C5A74D ();
// 0x000009CA System.UInt32 OVR.OpenVR.CVRSystem::GetTrackedDeviceIndexForControllerRole(OVR.OpenVR.ETrackedControllerRole)
extern void CVRSystem_GetTrackedDeviceIndexForControllerRole_m3C88C0683D0964EF9EEE51D8324349D1E9271EF1 ();
// 0x000009CB OVR.OpenVR.ETrackedControllerRole OVR.OpenVR.CVRSystem::GetControllerRoleForTrackedDeviceIndex(System.UInt32)
extern void CVRSystem_GetControllerRoleForTrackedDeviceIndex_m8E9E1CE89ADBBF17385C2DF0918BCF655243C00E ();
// 0x000009CC OVR.OpenVR.ETrackedDeviceClass OVR.OpenVR.CVRSystem::GetTrackedDeviceClass(System.UInt32)
extern void CVRSystem_GetTrackedDeviceClass_m583F1CE9A06B487A2FE634466C1DDBC98DECB5FB ();
// 0x000009CD System.Boolean OVR.OpenVR.CVRSystem::IsTrackedDeviceConnected(System.UInt32)
extern void CVRSystem_IsTrackedDeviceConnected_m2F1232CC01B1DF0770DA5E515FBFC68EADB6A37F ();
// 0x000009CE System.Boolean OVR.OpenVR.CVRSystem::GetBoolTrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetBoolTrackedDeviceProperty_m5346AA63329D322916A63E613A305E125F256218 ();
// 0x000009CF System.Single OVR.OpenVR.CVRSystem::GetFloatTrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetFloatTrackedDeviceProperty_m5236D6A92A06CED44224F7687E208C75BA30AB1C ();
// 0x000009D0 System.Int32 OVR.OpenVR.CVRSystem::GetInt32TrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetInt32TrackedDeviceProperty_m998F0341B26784EA9B840511AE05096F5E36FB9D ();
// 0x000009D1 System.UInt64 OVR.OpenVR.CVRSystem::GetUint64TrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetUint64TrackedDeviceProperty_m676340ACAB693CE102A2DD16E0ED9E52D1DD7C0E ();
// 0x000009D2 OVR.OpenVR.HmdMatrix34_t OVR.OpenVR.CVRSystem::GetMatrix34TrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetMatrix34TrackedDeviceProperty_mA3F63DB96911166CCEA9F21387A2F850E001CAFB ();
// 0x000009D3 System.UInt32 OVR.OpenVR.CVRSystem::GetArrayTrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.UInt32,System.IntPtr,System.UInt32,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetArrayTrackedDeviceProperty_mB730820A15645F20A6BD6DABB4FAF0363C6E4658 ();
// 0x000009D4 System.UInt32 OVR.OpenVR.CVRSystem::GetStringTrackedDeviceProperty(System.UInt32,OVR.OpenVR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.ETrackedPropertyError&)
extern void CVRSystem_GetStringTrackedDeviceProperty_m1BA1B2501A925CB78FD751B3F9DE6512B3D158DF ();
// 0x000009D5 System.String OVR.OpenVR.CVRSystem::GetPropErrorNameFromEnum(OVR.OpenVR.ETrackedPropertyError)
extern void CVRSystem_GetPropErrorNameFromEnum_mF4B6B9646F248C4F94E1DDDD68391671FF51C8FD ();
// 0x000009D6 System.Boolean OVR.OpenVR.CVRSystem::PollNextEvent(OVR.OpenVR.VREvent_t&,System.UInt32)
extern void CVRSystem_PollNextEvent_m04E43888B8F87567C822418D775A0066CA615DF7 ();
// 0x000009D7 System.Boolean OVR.OpenVR.CVRSystem::PollNextEventWithPose(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.VREvent_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&)
extern void CVRSystem_PollNextEventWithPose_mA6909A97C8BA1C13E3BB1679E53337FA95DA573B ();
// 0x000009D8 System.String OVR.OpenVR.CVRSystem::GetEventTypeNameFromEnum(OVR.OpenVR.EVREventType)
extern void CVRSystem_GetEventTypeNameFromEnum_m1004ED334320B588D4359A9130977791C159FF3B ();
// 0x000009D9 OVR.OpenVR.HiddenAreaMesh_t OVR.OpenVR.CVRSystem::GetHiddenAreaMesh(OVR.OpenVR.EVREye,OVR.OpenVR.EHiddenAreaMeshType)
extern void CVRSystem_GetHiddenAreaMesh_m37139681F4F511417FEF36B1BA799FFA9531B88B ();
// 0x000009DA System.Boolean OVR.OpenVR.CVRSystem::GetControllerState(System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32)
extern void CVRSystem_GetControllerState_mD043B5DF3CB837AF884A281F6F9173F8C89355DB ();
// 0x000009DB System.Boolean OVR.OpenVR.CVRSystem::GetControllerStateWithPose(OVR.OpenVR.ETrackingUniverseOrigin,System.UInt32,OVR.OpenVR.VRControllerState_t&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&)
extern void CVRSystem_GetControllerStateWithPose_mD53B1D9C8C417A77FE9DF230AD61B55BB0C9B919 ();
// 0x000009DC System.Void OVR.OpenVR.CVRSystem::TriggerHapticPulse(System.UInt32,System.UInt32,System.UInt16)
extern void CVRSystem_TriggerHapticPulse_m152B442C19D76B73D5B98FBAD57B1F44A728EC4E ();
// 0x000009DD System.String OVR.OpenVR.CVRSystem::GetButtonIdNameFromEnum(OVR.OpenVR.EVRButtonId)
extern void CVRSystem_GetButtonIdNameFromEnum_m615274D507BD24BB53C0788AD647152163FECBBB ();
// 0x000009DE System.String OVR.OpenVR.CVRSystem::GetControllerAxisTypeNameFromEnum(OVR.OpenVR.EVRControllerAxisType)
extern void CVRSystem_GetControllerAxisTypeNameFromEnum_mBCA6EB99917B93A3B15B28D84ABAFEC2972C95A7 ();
// 0x000009DF System.Boolean OVR.OpenVR.CVRSystem::IsInputAvailable()
extern void CVRSystem_IsInputAvailable_m13ABBFE4BBE7AE7EDD1C68303C91403808B012F8 ();
// 0x000009E0 System.Boolean OVR.OpenVR.CVRSystem::IsSteamVRDrawingControllers()
extern void CVRSystem_IsSteamVRDrawingControllers_m2BB1ABE186019E54D4A8D93A43C2E9F97089E452 ();
// 0x000009E1 System.Boolean OVR.OpenVR.CVRSystem::ShouldApplicationPause()
extern void CVRSystem_ShouldApplicationPause_mFF841BA12BFDCA0B952A5B8E0EF84DAACBE3F65C ();
// 0x000009E2 System.Boolean OVR.OpenVR.CVRSystem::ShouldApplicationReduceRenderingWork()
extern void CVRSystem_ShouldApplicationReduceRenderingWork_mD66F7797A2F1A00836869C2486F3ED8D6978494C ();
// 0x000009E3 System.UInt32 OVR.OpenVR.CVRSystem::DriverDebugRequest(System.UInt32,System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRSystem_DriverDebugRequest_mB20BADD7B6B15E14CE08C08C689FBCEAA03E5CFC ();
// 0x000009E4 OVR.OpenVR.EVRFirmwareError OVR.OpenVR.CVRSystem::PerformFirmwareUpdate(System.UInt32)
extern void CVRSystem_PerformFirmwareUpdate_m12308AE683BB0D6D1D6F03BAEBC5942F8919501A ();
// 0x000009E5 System.Void OVR.OpenVR.CVRSystem::AcknowledgeQuit_Exiting()
extern void CVRSystem_AcknowledgeQuit_Exiting_m4D2B7A417F2CA761BFC46B51AF934F4836D4742F ();
// 0x000009E6 System.Void OVR.OpenVR.CVRSystem::AcknowledgeQuit_UserPrompt()
extern void CVRSystem_AcknowledgeQuit_UserPrompt_m5062A90BB06CE6EDBECF037F0049A9086F3D1F3E ();
// 0x000009E7 System.Void OVR.OpenVR.CVRSystem__PollNextEventPacked::.ctor(System.Object,System.IntPtr)
extern void _PollNextEventPacked__ctor_m11F94910D234D3F591E8D0759A1F7B4390E91CF3 ();
// 0x000009E8 System.Boolean OVR.OpenVR.CVRSystem__PollNextEventPacked::Invoke(OVR.OpenVR.VREvent_t_Packed&,System.UInt32)
extern void _PollNextEventPacked_Invoke_m78BF2E37E8021A6B92A25586A0B6193EE2B79478 ();
// 0x000009E9 System.IAsyncResult OVR.OpenVR.CVRSystem__PollNextEventPacked::BeginInvoke(OVR.OpenVR.VREvent_t_Packed&,System.UInt32,System.AsyncCallback,System.Object)
extern void _PollNextEventPacked_BeginInvoke_m96B394D4122E331CBD4FD6A6D6CC54CB48429C5E ();
// 0x000009EA System.Boolean OVR.OpenVR.CVRSystem__PollNextEventPacked::EndInvoke(OVR.OpenVR.VREvent_t_Packed&,System.IAsyncResult)
extern void _PollNextEventPacked_EndInvoke_mD4F41A710BA50446799367CF53D9169BC2326482 ();
// 0x000009EB System.Void OVR.OpenVR.CVRSystem__GetControllerStatePacked::.ctor(System.Object,System.IntPtr)
extern void _GetControllerStatePacked__ctor_mD596EC82CDEC27284AA8211ABCBD2769AAB8192D ();
// 0x000009EC System.Boolean OVR.OpenVR.CVRSystem__GetControllerStatePacked::Invoke(System.UInt32,OVR.OpenVR.VRControllerState_t_Packed&,System.UInt32)
extern void _GetControllerStatePacked_Invoke_m6461239365042650A34C363CA8F81CBDCC916EC8 ();
// 0x000009ED System.IAsyncResult OVR.OpenVR.CVRSystem__GetControllerStatePacked::BeginInvoke(System.UInt32,OVR.OpenVR.VRControllerState_t_Packed&,System.UInt32,System.AsyncCallback,System.Object)
extern void _GetControllerStatePacked_BeginInvoke_m28B346E036D2090D2D3CAEDADDB7AFA31C042ACA ();
// 0x000009EE System.Boolean OVR.OpenVR.CVRSystem__GetControllerStatePacked::EndInvoke(OVR.OpenVR.VRControllerState_t_Packed&,System.IAsyncResult)
extern void _GetControllerStatePacked_EndInvoke_m586751E78A18B38DB21062620F4235B62C8FAC48 ();
// 0x000009EF System.Void OVR.OpenVR.CVRSystem__GetControllerStateWithPosePacked::.ctor(System.Object,System.IntPtr)
extern void _GetControllerStateWithPosePacked__ctor_m11FE22C3793473344173CEFE286F40C815F157A6 ();
// 0x000009F0 System.Boolean OVR.OpenVR.CVRSystem__GetControllerStateWithPosePacked::Invoke(OVR.OpenVR.ETrackingUniverseOrigin,System.UInt32,OVR.OpenVR.VRControllerState_t_Packed&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&)
extern void _GetControllerStateWithPosePacked_Invoke_mC8454DB592377E156C19DAA8F09DC0219382F69A ();
// 0x000009F1 System.IAsyncResult OVR.OpenVR.CVRSystem__GetControllerStateWithPosePacked::BeginInvoke(OVR.OpenVR.ETrackingUniverseOrigin,System.UInt32,OVR.OpenVR.VRControllerState_t_Packed&,System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern void _GetControllerStateWithPosePacked_BeginInvoke_m21D6E5070D4E08D67DF4AD6E69D2AE535B001064 ();
// 0x000009F2 System.Boolean OVR.OpenVR.CVRSystem__GetControllerStateWithPosePacked::EndInvoke(OVR.OpenVR.VRControllerState_t_Packed&,OVR.OpenVR.TrackedDevicePose_t&,System.IAsyncResult)
extern void _GetControllerStateWithPosePacked_EndInvoke_mA73480310FB55E9443DAD95B7C60FB6F44A6F3A6 ();
// 0x000009F3 System.Void OVR.OpenVR.CVRExtendedDisplay::.ctor(System.IntPtr)
extern void CVRExtendedDisplay__ctor_m684BAED95BE1BA4E0D20800A922AC93B7EB37963 ();
// 0x000009F4 System.Void OVR.OpenVR.CVRExtendedDisplay::GetWindowBounds(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&)
extern void CVRExtendedDisplay_GetWindowBounds_m9F26FB7C6CC29B312044DBB4D5EB760B530CC6F4 ();
// 0x000009F5 System.Void OVR.OpenVR.CVRExtendedDisplay::GetEyeOutputViewport(OVR.OpenVR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&)
extern void CVRExtendedDisplay_GetEyeOutputViewport_m5A94D47B8FB2E14092F5AE07514921943BCACCC3 ();
// 0x000009F6 System.Void OVR.OpenVR.CVRExtendedDisplay::GetDXGIOutputInfo(System.Int32&,System.Int32&)
extern void CVRExtendedDisplay_GetDXGIOutputInfo_mCFD9805179F0BF4354EE94D170A52234B1EA1E1E ();
// 0x000009F7 System.Void OVR.OpenVR.CVRTrackedCamera::.ctor(System.IntPtr)
extern void CVRTrackedCamera__ctor_mBA8FCE2F5C6DF9701E852CC4FD22892C18A09790 ();
// 0x000009F8 System.String OVR.OpenVR.CVRTrackedCamera::GetCameraErrorNameFromEnum(OVR.OpenVR.EVRTrackedCameraError)
extern void CVRTrackedCamera_GetCameraErrorNameFromEnum_m5D9E13858193F28CC85133D763FE240187D8E891 ();
// 0x000009F9 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::HasCamera(System.UInt32,System.Boolean&)
extern void CVRTrackedCamera_HasCamera_mBDD23E5ED02A4D79A419B091BDA2CAEBD1A72B48 ();
// 0x000009FA OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetCameraFrameSize(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&)
extern void CVRTrackedCamera_GetCameraFrameSize_mC880097C8FCD14EB1BE823EF71023D789670DAE1 ();
// 0x000009FB OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetCameraIntrinsics(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.HmdVector2_t&,OVR.OpenVR.HmdVector2_t&)
extern void CVRTrackedCamera_GetCameraIntrinsics_m98E27C54985CACFAC478B8DE58654AE6124AFEAF ();
// 0x000009FC OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetCameraProjection(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,System.Single,System.Single,OVR.OpenVR.HmdMatrix44_t&)
extern void CVRTrackedCamera_GetCameraProjection_m5900B780FB1A05D919D3FB01A18DCAF341282D5C ();
// 0x000009FD OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::AcquireVideoStreamingService(System.UInt32,System.UInt64&)
extern void CVRTrackedCamera_AcquireVideoStreamingService_mA1A8CC84E468B064CDF47C01F1704C24CDA47B39 ();
// 0x000009FE OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::ReleaseVideoStreamingService(System.UInt64)
extern void CVRTrackedCamera_ReleaseVideoStreamingService_m89A88991316B0387F972D1202B1D8D809AB736BD ();
// 0x000009FF OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetVideoStreamFrameBuffer(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void CVRTrackedCamera_GetVideoStreamFrameBuffer_m789788B06DAF5170108E8C81320C9276A3454A43 ();
// 0x00000A00 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetVideoStreamTextureSize(System.UInt32,OVR.OpenVR.EVRTrackedCameraFrameType,OVR.OpenVR.VRTextureBounds_t&,System.UInt32&,System.UInt32&)
extern void CVRTrackedCamera_GetVideoStreamTextureSize_m06303B3E8B3D090B61CE936D1DB7E1F9531B15FB ();
// 0x00000A01 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetVideoStreamTextureD3D11(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void CVRTrackedCamera_GetVideoStreamTextureD3D11_m65B4B2F554F2FAA5601FA45FD0F0AD8F52E09D32 ();
// 0x00000A02 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::GetVideoStreamTextureGL(System.UInt64,OVR.OpenVR.EVRTrackedCameraFrameType,System.UInt32&,OVR.OpenVR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern void CVRTrackedCamera_GetVideoStreamTextureGL_m75DAE7D1F133F5DAD6D42A111DD678D8D433BCA0 ();
// 0x00000A03 OVR.OpenVR.EVRTrackedCameraError OVR.OpenVR.CVRTrackedCamera::ReleaseVideoStreamTextureGL(System.UInt64,System.UInt32)
extern void CVRTrackedCamera_ReleaseVideoStreamTextureGL_m9F791E3D4EE7E07E81AA5056D480303F0418FDFA ();
// 0x00000A04 System.Void OVR.OpenVR.CVRApplications::.ctor(System.IntPtr)
extern void CVRApplications__ctor_m10ED622413314220B002A0F1A921EA177394018F ();
// 0x00000A05 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::AddApplicationManifest(System.String,System.Boolean)
extern void CVRApplications_AddApplicationManifest_mE565C72D0A5432D334AF76F89978366FD65CDCDD ();
// 0x00000A06 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::RemoveApplicationManifest(System.String)
extern void CVRApplications_RemoveApplicationManifest_m6E8B0C3F4B64822D6C66BC31A81D2282CFDF5C2B ();
// 0x00000A07 System.Boolean OVR.OpenVR.CVRApplications::IsApplicationInstalled(System.String)
extern void CVRApplications_IsApplicationInstalled_mA3A2D0BDF4FFB8DA038972949A359F76B46726B6 ();
// 0x00000A08 System.UInt32 OVR.OpenVR.CVRApplications::GetApplicationCount()
extern void CVRApplications_GetApplicationCount_mD6D680D215E3FD12780CEA700085DDEEE047BC18 ();
// 0x00000A09 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::GetApplicationKeyByIndex(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetApplicationKeyByIndex_m5E27B8122546B45B932C3D91A000416DEAFEB64A ();
// 0x00000A0A OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::GetApplicationKeyByProcessId(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetApplicationKeyByProcessId_mD063F4B30D011E795CE3356CEE45231F9DA9AB26 ();
// 0x00000A0B OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::LaunchApplication(System.String)
extern void CVRApplications_LaunchApplication_mBA8D7878655B2695705BF6DB8AD1E56E7242FF29 ();
// 0x00000A0C OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::LaunchTemplateApplication(System.String,System.String,OVR.OpenVR.AppOverrideKeys_t[])
extern void CVRApplications_LaunchTemplateApplication_mA0272F708F81F60D68F6D1428DACBFF5BC2C9493 ();
// 0x00000A0D OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::LaunchApplicationFromMimeType(System.String,System.String)
extern void CVRApplications_LaunchApplicationFromMimeType_m283844B6929A737A39EAD06EF91B0BF5EE1F9279 ();
// 0x00000A0E OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::LaunchDashboardOverlay(System.String)
extern void CVRApplications_LaunchDashboardOverlay_m3E3F8C27C876E0113556A2CA64A0B175BD988F40 ();
// 0x00000A0F System.Boolean OVR.OpenVR.CVRApplications::CancelApplicationLaunch(System.String)
extern void CVRApplications_CancelApplicationLaunch_m65694406E0C52FE3B771429A7CE058C9793357AA ();
// 0x00000A10 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::IdentifyApplication(System.UInt32,System.String)
extern void CVRApplications_IdentifyApplication_m82295F9A85AAE04AC7447C33AC5FB337F660C671 ();
// 0x00000A11 System.UInt32 OVR.OpenVR.CVRApplications::GetApplicationProcessId(System.String)
extern void CVRApplications_GetApplicationProcessId_m3A427522792790EA4074B88FDB0E28A463EC9480 ();
// 0x00000A12 System.String OVR.OpenVR.CVRApplications::GetApplicationsErrorNameFromEnum(OVR.OpenVR.EVRApplicationError)
extern void CVRApplications_GetApplicationsErrorNameFromEnum_m82F732844B783AFEDECAB68FC3846F65FB327125 ();
// 0x00000A13 System.UInt32 OVR.OpenVR.CVRApplications::GetApplicationPropertyString(System.String,OVR.OpenVR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRApplicationError&)
extern void CVRApplications_GetApplicationPropertyString_mFFE0E32284D2F45957882B0D7BDF4933F923F0E0 ();
// 0x00000A14 System.Boolean OVR.OpenVR.CVRApplications::GetApplicationPropertyBool(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&)
extern void CVRApplications_GetApplicationPropertyBool_m856BFB73D9D048C8D9483CA1809E7E0B685F554B ();
// 0x00000A15 System.UInt64 OVR.OpenVR.CVRApplications::GetApplicationPropertyUint64(System.String,OVR.OpenVR.EVRApplicationProperty,OVR.OpenVR.EVRApplicationError&)
extern void CVRApplications_GetApplicationPropertyUint64_mF7B2C1C34758B564F5D14CCCAD2BD87E73DBFF63 ();
// 0x00000A16 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::SetApplicationAutoLaunch(System.String,System.Boolean)
extern void CVRApplications_SetApplicationAutoLaunch_m76C6C48380907CB3BC2F69689558339835ACA670 ();
// 0x00000A17 System.Boolean OVR.OpenVR.CVRApplications::GetApplicationAutoLaunch(System.String)
extern void CVRApplications_GetApplicationAutoLaunch_m9587D5E2729B6292574367A856AA5643A96EC835 ();
// 0x00000A18 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::SetDefaultApplicationForMimeType(System.String,System.String)
extern void CVRApplications_SetDefaultApplicationForMimeType_mCD17C90794ECBD97677800515354BDDF99B2F06E ();
// 0x00000A19 System.Boolean OVR.OpenVR.CVRApplications::GetDefaultApplicationForMimeType(System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetDefaultApplicationForMimeType_m07BE0B8FF6E4B402F18AF4E4FCF6F86015EE9E7D ();
// 0x00000A1A System.Boolean OVR.OpenVR.CVRApplications::GetApplicationSupportedMimeTypes(System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetApplicationSupportedMimeTypes_mE1D81848BC6F43F0B7F31E89B693AD0F1A9C6ABB ();
// 0x00000A1B System.UInt32 OVR.OpenVR.CVRApplications::GetApplicationsThatSupportMimeType(System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetApplicationsThatSupportMimeType_mC246AB5418331EAA4AB1C55F6962C3C3AAB74FAD ();
// 0x00000A1C System.UInt32 OVR.OpenVR.CVRApplications::GetApplicationLaunchArguments(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetApplicationLaunchArguments_m9736069BD9FE93C02A802A36EAF675E41FE64896 ();
// 0x00000A1D OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::GetStartingApplication(System.Text.StringBuilder,System.UInt32)
extern void CVRApplications_GetStartingApplication_mF9E844C256525FF6B60B2F2835D0DB946E4A9332 ();
// 0x00000A1E OVR.OpenVR.EVRApplicationTransitionState OVR.OpenVR.CVRApplications::GetTransitionState()
extern void CVRApplications_GetTransitionState_m049274D331090D805641F8B9DCEDA00C9B6A2EB3 ();
// 0x00000A1F OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::PerformApplicationPrelaunchCheck(System.String)
extern void CVRApplications_PerformApplicationPrelaunchCheck_mEBD16B62160A3E22875ED3A2796C3CB817DC3419 ();
// 0x00000A20 System.String OVR.OpenVR.CVRApplications::GetApplicationsTransitionStateNameFromEnum(OVR.OpenVR.EVRApplicationTransitionState)
extern void CVRApplications_GetApplicationsTransitionStateNameFromEnum_mF43F8DD46DCBFD922316B3DE7839F38EDC64C509 ();
// 0x00000A21 System.Boolean OVR.OpenVR.CVRApplications::IsQuitUserPromptRequested()
extern void CVRApplications_IsQuitUserPromptRequested_m1043E723C1915BB122F7A7110EBE63D55204D06F ();
// 0x00000A22 OVR.OpenVR.EVRApplicationError OVR.OpenVR.CVRApplications::LaunchInternalProcess(System.String,System.String,System.String)
extern void CVRApplications_LaunchInternalProcess_mAD63DECBB6E51DBF95804E0C0B8AE2F829BAC993 ();
// 0x00000A23 System.UInt32 OVR.OpenVR.CVRApplications::GetCurrentSceneProcessId()
extern void CVRApplications_GetCurrentSceneProcessId_mA0EAF28554F86AC5BF8B3B60E8BC40CB0C4B5EA3 ();
// 0x00000A24 System.Void OVR.OpenVR.CVRChaperone::.ctor(System.IntPtr)
extern void CVRChaperone__ctor_mB9037906CD47B436FF1540AED99E358899764193 ();
// 0x00000A25 OVR.OpenVR.ChaperoneCalibrationState OVR.OpenVR.CVRChaperone::GetCalibrationState()
extern void CVRChaperone_GetCalibrationState_m9DAE56F4B2B89906DFF72FD93C958C35FF7B72D1 ();
// 0x00000A26 System.Boolean OVR.OpenVR.CVRChaperone::GetPlayAreaSize(System.Single&,System.Single&)
extern void CVRChaperone_GetPlayAreaSize_mE413B355569CBC34537847357D7D096A268ADD4C ();
// 0x00000A27 System.Boolean OVR.OpenVR.CVRChaperone::GetPlayAreaRect(OVR.OpenVR.HmdQuad_t&)
extern void CVRChaperone_GetPlayAreaRect_m0385F1149EEB05783BAA04310D8AE369F3EFC326 ();
// 0x00000A28 System.Void OVR.OpenVR.CVRChaperone::ReloadInfo()
extern void CVRChaperone_ReloadInfo_m7833D2C132BA981FA30BB89FBDC218200CD03604 ();
// 0x00000A29 System.Void OVR.OpenVR.CVRChaperone::SetSceneColor(OVR.OpenVR.HmdColor_t)
extern void CVRChaperone_SetSceneColor_m43295A91E09CD7E51FFA65455C9110BE268FA970 ();
// 0x00000A2A System.Void OVR.OpenVR.CVRChaperone::GetBoundsColor(OVR.OpenVR.HmdColor_t&,System.Int32,System.Single,OVR.OpenVR.HmdColor_t&)
extern void CVRChaperone_GetBoundsColor_mFBD8FFF0426B7B2B3D116D1983C750FD7EBC6B10 ();
// 0x00000A2B System.Boolean OVR.OpenVR.CVRChaperone::AreBoundsVisible()
extern void CVRChaperone_AreBoundsVisible_m4C498A807B29A68C9B14E88A29118C6F497E58FF ();
// 0x00000A2C System.Void OVR.OpenVR.CVRChaperone::ForceBoundsVisible(System.Boolean)
extern void CVRChaperone_ForceBoundsVisible_mC75843B1495B2D7BF2108FC963FC67984134F301 ();
// 0x00000A2D System.Void OVR.OpenVR.CVRChaperoneSetup::.ctor(System.IntPtr)
extern void CVRChaperoneSetup__ctor_mE796C0271DB69900F9D6CC891613A2B546D8DBB9 ();
// 0x00000A2E System.Boolean OVR.OpenVR.CVRChaperoneSetup::CommitWorkingCopy(OVR.OpenVR.EChaperoneConfigFile)
extern void CVRChaperoneSetup_CommitWorkingCopy_m7C70F797E171B944E47C8AD10CA7C4EDCC13AD47 ();
// 0x00000A2F System.Void OVR.OpenVR.CVRChaperoneSetup::RevertWorkingCopy()
extern void CVRChaperoneSetup_RevertWorkingCopy_m6FE31FF446DF3CAEB5D14AE72155681AEDB9B599 ();
// 0x00000A30 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetWorkingPlayAreaSize(System.Single&,System.Single&)
extern void CVRChaperoneSetup_GetWorkingPlayAreaSize_m1470E241E78F43A44E562965B966E3B0ABB60C13 ();
// 0x00000A31 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetWorkingPlayAreaRect(OVR.OpenVR.HmdQuad_t&)
extern void CVRChaperoneSetup_GetWorkingPlayAreaRect_mE48616155F75ED657A9C9B3DB52487C0F3447ECF ();
// 0x00000A32 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetWorkingCollisionBoundsInfo(OVR.OpenVR.HmdQuad_t[]&)
extern void CVRChaperoneSetup_GetWorkingCollisionBoundsInfo_m9C51C85881779F68C6D9474D47F33F04CC64B0F4 ();
// 0x00000A33 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetLiveCollisionBoundsInfo(OVR.OpenVR.HmdQuad_t[]&)
extern void CVRChaperoneSetup_GetLiveCollisionBoundsInfo_m4A7D58BE41A1CE876CBED2F1C3447E63FAE42660 ();
// 0x00000A34 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetWorkingSeatedZeroPoseToRawTrackingPose(OVR.OpenVR.HmdMatrix34_t&)
extern void CVRChaperoneSetup_GetWorkingSeatedZeroPoseToRawTrackingPose_mB9F856508E014F40071CD4186F2ABA6B64CDADF9 ();
// 0x00000A35 System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetWorkingStandingZeroPoseToRawTrackingPose(OVR.OpenVR.HmdMatrix34_t&)
extern void CVRChaperoneSetup_GetWorkingStandingZeroPoseToRawTrackingPose_mD1806323555002EBF765E704EBF1DAA9036B58E4 ();
// 0x00000A36 System.Void OVR.OpenVR.CVRChaperoneSetup::SetWorkingPlayAreaSize(System.Single,System.Single)
extern void CVRChaperoneSetup_SetWorkingPlayAreaSize_m86B954C64BF764938B472B4A79349B95562EC071 ();
// 0x00000A37 System.Void OVR.OpenVR.CVRChaperoneSetup::SetWorkingCollisionBoundsInfo(OVR.OpenVR.HmdQuad_t[])
extern void CVRChaperoneSetup_SetWorkingCollisionBoundsInfo_m2BAF58BA229014D1084627E8864B525B9C34BCD9 ();
// 0x00000A38 System.Void OVR.OpenVR.CVRChaperoneSetup::SetWorkingSeatedZeroPoseToRawTrackingPose(OVR.OpenVR.HmdMatrix34_t&)
extern void CVRChaperoneSetup_SetWorkingSeatedZeroPoseToRawTrackingPose_m344F859C35E8633BFB1F64B16B9CF2E2E19E2B4A ();
// 0x00000A39 System.Void OVR.OpenVR.CVRChaperoneSetup::SetWorkingStandingZeroPoseToRawTrackingPose(OVR.OpenVR.HmdMatrix34_t&)
extern void CVRChaperoneSetup_SetWorkingStandingZeroPoseToRawTrackingPose_mFFC83B62EA70E7C254B2C09D20D06D953E68CEA1 ();
// 0x00000A3A System.Void OVR.OpenVR.CVRChaperoneSetup::ReloadFromDisk(OVR.OpenVR.EChaperoneConfigFile)
extern void CVRChaperoneSetup_ReloadFromDisk_m3D542F8B7A9D353264C29677D5A587C173119AA7 ();
// 0x00000A3B System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetLiveSeatedZeroPoseToRawTrackingPose(OVR.OpenVR.HmdMatrix34_t&)
extern void CVRChaperoneSetup_GetLiveSeatedZeroPoseToRawTrackingPose_mB7E53C369CABC34EF4918F0222F51450395E1363 ();
// 0x00000A3C System.Void OVR.OpenVR.CVRChaperoneSetup::SetWorkingCollisionBoundsTagsInfo(System.Byte[])
extern void CVRChaperoneSetup_SetWorkingCollisionBoundsTagsInfo_m5612C7DB5EFB852E2053564DCE3AB64827B59D2E ();
// 0x00000A3D System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetLiveCollisionBoundsTagsInfo(System.Byte[]&)
extern void CVRChaperoneSetup_GetLiveCollisionBoundsTagsInfo_mE8D1934B6C5EF7699DD570EBFC404A7E9B8C0E55 ();
// 0x00000A3E System.Boolean OVR.OpenVR.CVRChaperoneSetup::SetWorkingPhysicalBoundsInfo(OVR.OpenVR.HmdQuad_t[])
extern void CVRChaperoneSetup_SetWorkingPhysicalBoundsInfo_mF67F5B39A568082D9DF823B1F303C36DB8E1DCD1 ();
// 0x00000A3F System.Boolean OVR.OpenVR.CVRChaperoneSetup::GetLivePhysicalBoundsInfo(OVR.OpenVR.HmdQuad_t[]&)
extern void CVRChaperoneSetup_GetLivePhysicalBoundsInfo_m58CDA89A1C77EFF58EC3760173BDAF8ABF402035 ();
// 0x00000A40 System.Boolean OVR.OpenVR.CVRChaperoneSetup::ExportLiveToBuffer(System.Text.StringBuilder,System.UInt32&)
extern void CVRChaperoneSetup_ExportLiveToBuffer_m3385F968F527A722A5D0D3C73BBB6E94A7A975D4 ();
// 0x00000A41 System.Boolean OVR.OpenVR.CVRChaperoneSetup::ImportFromBufferToWorking(System.String,System.UInt32)
extern void CVRChaperoneSetup_ImportFromBufferToWorking_m90BA75E653E48B85F141EE67D6808CBB30ACB7B7 ();
// 0x00000A42 System.Void OVR.OpenVR.CVRCompositor::.ctor(System.IntPtr)
extern void CVRCompositor__ctor_m76687EACBD1D98DD26351FE3EB4BB34CC255DC06 ();
// 0x00000A43 System.Void OVR.OpenVR.CVRCompositor::SetTrackingSpace(OVR.OpenVR.ETrackingUniverseOrigin)
extern void CVRCompositor_SetTrackingSpace_mF5383683C59B684F37266A0052713F1A55C3D8EE ();
// 0x00000A44 OVR.OpenVR.ETrackingUniverseOrigin OVR.OpenVR.CVRCompositor::GetTrackingSpace()
extern void CVRCompositor_GetTrackingSpace_mE709352A6D0C3B4DDF20236C14C3DEE6EFE59643 ();
// 0x00000A45 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::WaitGetPoses(OVR.OpenVR.TrackedDevicePose_t[],OVR.OpenVR.TrackedDevicePose_t[])
extern void CVRCompositor_WaitGetPoses_m12CD1189BCDD080405743CA29CDD4AB561D3CD6F ();
// 0x00000A46 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::GetLastPoses(OVR.OpenVR.TrackedDevicePose_t[],OVR.OpenVR.TrackedDevicePose_t[])
extern void CVRCompositor_GetLastPoses_mEB04A324E02816925043913DE16580A5BBB937E2 ();
// 0x00000A47 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::GetLastPoseForTrackedDeviceIndex(System.UInt32,OVR.OpenVR.TrackedDevicePose_t&,OVR.OpenVR.TrackedDevicePose_t&)
extern void CVRCompositor_GetLastPoseForTrackedDeviceIndex_m1148D0735726373165658A4869B0CB8AF2371978 ();
// 0x00000A48 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::Submit(OVR.OpenVR.EVREye,OVR.OpenVR.Texture_t&,OVR.OpenVR.VRTextureBounds_t&,OVR.OpenVR.EVRSubmitFlags)
extern void CVRCompositor_Submit_m1C400A2D54A77A3DA44B8390ECC741D6A56D9B60 ();
// 0x00000A49 System.Void OVR.OpenVR.CVRCompositor::ClearLastSubmittedFrame()
extern void CVRCompositor_ClearLastSubmittedFrame_m891419E6FB506FC25EA8EFAEA394BA334DB29AB3 ();
// 0x00000A4A System.Void OVR.OpenVR.CVRCompositor::PostPresentHandoff()
extern void CVRCompositor_PostPresentHandoff_m4F08024F8A2F74808D26B085750C4D17E093B068 ();
// 0x00000A4B System.Boolean OVR.OpenVR.CVRCompositor::GetFrameTiming(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32)
extern void CVRCompositor_GetFrameTiming_m10E744A25C959F27173F381A7FD8F6BA172A9DC1 ();
// 0x00000A4C System.UInt32 OVR.OpenVR.CVRCompositor::GetFrameTimings(OVR.OpenVR.Compositor_FrameTiming&,System.UInt32)
extern void CVRCompositor_GetFrameTimings_mFEB399B6B2814708382C86090A52EDF7575F67F1 ();
// 0x00000A4D System.Single OVR.OpenVR.CVRCompositor::GetFrameTimeRemaining()
extern void CVRCompositor_GetFrameTimeRemaining_m5BCC67E11003D233BED3D608D4F4DCA96FF47AA1 ();
// 0x00000A4E System.Void OVR.OpenVR.CVRCompositor::GetCumulativeStats(OVR.OpenVR.Compositor_CumulativeStats&,System.UInt32)
extern void CVRCompositor_GetCumulativeStats_m6935B3AFA41BC935BEA5D15AD03914ECD17FF43E ();
// 0x00000A4F System.Void OVR.OpenVR.CVRCompositor::FadeToColor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void CVRCompositor_FadeToColor_m3ADD702AC4153707BFCEF5B65C82EED71434EA05 ();
// 0x00000A50 OVR.OpenVR.HmdColor_t OVR.OpenVR.CVRCompositor::GetCurrentFadeColor(System.Boolean)
extern void CVRCompositor_GetCurrentFadeColor_m354F6B2772CC4C06C9B8F558921F978784D7E40E ();
// 0x00000A51 System.Void OVR.OpenVR.CVRCompositor::FadeGrid(System.Single,System.Boolean)
extern void CVRCompositor_FadeGrid_m9C71900504C7407D673B0F43B5EC690FFDCB7EC8 ();
// 0x00000A52 System.Single OVR.OpenVR.CVRCompositor::GetCurrentGridAlpha()
extern void CVRCompositor_GetCurrentGridAlpha_m02235A744325967FD3BF8F12D84E745A9AE2A1EE ();
// 0x00000A53 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::SetSkyboxOverride(OVR.OpenVR.Texture_t[])
extern void CVRCompositor_SetSkyboxOverride_mCE45D06DC49EC3D9461E0C827ACA6D39801D3813 ();
// 0x00000A54 System.Void OVR.OpenVR.CVRCompositor::ClearSkyboxOverride()
extern void CVRCompositor_ClearSkyboxOverride_mEB0DBCADDF28981CAF2B297288F30711E2A2C3C4 ();
// 0x00000A55 System.Void OVR.OpenVR.CVRCompositor::CompositorBringToFront()
extern void CVRCompositor_CompositorBringToFront_mCD327B7B4FCD40251023A4E1991440BBCA65DE3B ();
// 0x00000A56 System.Void OVR.OpenVR.CVRCompositor::CompositorGoToBack()
extern void CVRCompositor_CompositorGoToBack_m6F0298EA2CA75AE8C4C7C838C476FB56DFDFC24F ();
// 0x00000A57 System.Void OVR.OpenVR.CVRCompositor::CompositorQuit()
extern void CVRCompositor_CompositorQuit_mE10BD3AEDA3D0B065427F5E3C2FE42D06B739456 ();
// 0x00000A58 System.Boolean OVR.OpenVR.CVRCompositor::IsFullscreen()
extern void CVRCompositor_IsFullscreen_m7C40557D6AD106B14DCB18566C123A3362DFB160 ();
// 0x00000A59 System.UInt32 OVR.OpenVR.CVRCompositor::GetCurrentSceneFocusProcess()
extern void CVRCompositor_GetCurrentSceneFocusProcess_m8D94EF9DC806EAFEC686A312376B88F0EE0F29FB ();
// 0x00000A5A System.UInt32 OVR.OpenVR.CVRCompositor::GetLastFrameRenderer()
extern void CVRCompositor_GetLastFrameRenderer_mCCB3697D08B9D5CD09544316E4B71371B1F08622 ();
// 0x00000A5B System.Boolean OVR.OpenVR.CVRCompositor::CanRenderScene()
extern void CVRCompositor_CanRenderScene_m47C40F7ADE3E6D69B1E05047F7CE3B793D8F684C ();
// 0x00000A5C System.Void OVR.OpenVR.CVRCompositor::ShowMirrorWindow()
extern void CVRCompositor_ShowMirrorWindow_mF23E165BEEC6438BA433E9EC0C47F6973698279E ();
// 0x00000A5D System.Void OVR.OpenVR.CVRCompositor::HideMirrorWindow()
extern void CVRCompositor_HideMirrorWindow_m9D8A43D48D83D67FBA9C673EBC1FE04895C4012B ();
// 0x00000A5E System.Boolean OVR.OpenVR.CVRCompositor::IsMirrorWindowVisible()
extern void CVRCompositor_IsMirrorWindowVisible_mC40E25027C513ADAC009CE5659CFC05A1DE6FDA0 ();
// 0x00000A5F System.Void OVR.OpenVR.CVRCompositor::CompositorDumpImages()
extern void CVRCompositor_CompositorDumpImages_mBFB5BB1C83E2D8CDF2B993012B87D57AD37017A8 ();
// 0x00000A60 System.Boolean OVR.OpenVR.CVRCompositor::ShouldAppRenderWithLowResources()
extern void CVRCompositor_ShouldAppRenderWithLowResources_m08F710E91753A61F48111412800CA64BBC11210A ();
// 0x00000A61 System.Void OVR.OpenVR.CVRCompositor::ForceInterleavedReprojectionOn(System.Boolean)
extern void CVRCompositor_ForceInterleavedReprojectionOn_mE3E8B62798B34B67D675D278909D75436E42FFC6 ();
// 0x00000A62 System.Void OVR.OpenVR.CVRCompositor::ForceReconnectProcess()
extern void CVRCompositor_ForceReconnectProcess_m90236FF370769B6C6C9189D1F8F06510ADEA9C8D ();
// 0x00000A63 System.Void OVR.OpenVR.CVRCompositor::SuspendRendering(System.Boolean)
extern void CVRCompositor_SuspendRendering_m2901CC7920CA5E179F846774B0EA21D3D141C264 ();
// 0x00000A64 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::GetMirrorTextureD3D11(OVR.OpenVR.EVREye,System.IntPtr,System.IntPtr&)
extern void CVRCompositor_GetMirrorTextureD3D11_mB3CD1A6524BA4C474F971E7FDD62AD081447FC4E ();
// 0x00000A65 System.Void OVR.OpenVR.CVRCompositor::ReleaseMirrorTextureD3D11(System.IntPtr)
extern void CVRCompositor_ReleaseMirrorTextureD3D11_m2DF06A35720FAA1CE385EFA58E3F0B9539E863BF ();
// 0x00000A66 OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::GetMirrorTextureGL(OVR.OpenVR.EVREye,System.UInt32&,System.IntPtr)
extern void CVRCompositor_GetMirrorTextureGL_m6040D9C87CF43932655F38ED57BA51BA03993414 ();
// 0x00000A67 System.Boolean OVR.OpenVR.CVRCompositor::ReleaseSharedGLTexture(System.UInt32,System.IntPtr)
extern void CVRCompositor_ReleaseSharedGLTexture_m7DF7EB3D690BA4ED1DD5FE0CBA453A050650B415 ();
// 0x00000A68 System.Void OVR.OpenVR.CVRCompositor::LockGLSharedTextureForAccess(System.IntPtr)
extern void CVRCompositor_LockGLSharedTextureForAccess_mC221EC8E2B836559F187FF7081F5920406B3CCD7 ();
// 0x00000A69 System.Void OVR.OpenVR.CVRCompositor::UnlockGLSharedTextureForAccess(System.IntPtr)
extern void CVRCompositor_UnlockGLSharedTextureForAccess_m1E82C66D15D3F5E0ACC6B4D767D510171C8E5B8A ();
// 0x00000A6A System.UInt32 OVR.OpenVR.CVRCompositor::GetVulkanInstanceExtensionsRequired(System.Text.StringBuilder,System.UInt32)
extern void CVRCompositor_GetVulkanInstanceExtensionsRequired_mF893F41CE43F93B5F7567BE458FCB5450C59E472 ();
// 0x00000A6B System.UInt32 OVR.OpenVR.CVRCompositor::GetVulkanDeviceExtensionsRequired(System.IntPtr,System.Text.StringBuilder,System.UInt32)
extern void CVRCompositor_GetVulkanDeviceExtensionsRequired_mC14FA51B144836253EF4F524D99F82493AB80630 ();
// 0x00000A6C System.Void OVR.OpenVR.CVRCompositor::SetExplicitTimingMode(OVR.OpenVR.EVRCompositorTimingMode)
extern void CVRCompositor_SetExplicitTimingMode_m6661F02036B927657177E3C57FC058D96F0BDF99 ();
// 0x00000A6D OVR.OpenVR.EVRCompositorError OVR.OpenVR.CVRCompositor::SubmitExplicitTimingData()
extern void CVRCompositor_SubmitExplicitTimingData_m955D4AEBF99ACE56DB05EEFF8B37FB80D7C464EB ();
// 0x00000A6E System.Void OVR.OpenVR.CVROverlay::.ctor(System.IntPtr)
extern void CVROverlay__ctor_m57486E1F6EB0252D78EF9C93C17CE118714451E8 ();
// 0x00000A6F OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::FindOverlay(System.String,System.UInt64&)
extern void CVROverlay_FindOverlay_m3090AED964C94B8FA46D3111F5E8A3A85E2A44C1 ();
// 0x00000A70 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::CreateOverlay(System.String,System.String,System.UInt64&)
extern void CVROverlay_CreateOverlay_mDA392D308E83FA250023786D223CC2443AE46884 ();
// 0x00000A71 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::DestroyOverlay(System.UInt64)
extern void CVROverlay_DestroyOverlay_mDE2E19F89DA7059B5ADB6617617404D7171B82D3 ();
// 0x00000A72 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetHighQualityOverlay(System.UInt64)
extern void CVROverlay_SetHighQualityOverlay_m49A71CA516A0A9BA6DCB455C69DF4F320D0C55AB ();
// 0x00000A73 System.UInt64 OVR.OpenVR.CVROverlay::GetHighQualityOverlay()
extern void CVROverlay_GetHighQualityOverlay_mBDA523928F2C4DA40A83F55AC34148918E857FBC ();
// 0x00000A74 System.UInt32 OVR.OpenVR.CVROverlay::GetOverlayKey(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&)
extern void CVROverlay_GetOverlayKey_m7CD6D0AB98188708CA92A980D562EF6F0DA272A0 ();
// 0x00000A75 System.UInt32 OVR.OpenVR.CVROverlay::GetOverlayName(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVROverlayError&)
extern void CVROverlay_GetOverlayName_m25BDD743DB4F6FF1F5CD63EEEC16B715154B56FF ();
// 0x00000A76 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayName(System.UInt64,System.String)
extern void CVROverlay_SetOverlayName_mFC2F02B99A9621BACD606F7C892CE7595B2DFAE1 ();
// 0x00000A77 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayImageData(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&,System.UInt32&)
extern void CVROverlay_GetOverlayImageData_mBCA5DBFB69D453511DDB6691678CFE4CCE088E5C ();
// 0x00000A78 System.String OVR.OpenVR.CVROverlay::GetOverlayErrorNameFromEnum(OVR.OpenVR.EVROverlayError)
extern void CVROverlay_GetOverlayErrorNameFromEnum_m31FE986C61CD470BC57D4D1FBB5ECB15A94998C1 ();
// 0x00000A79 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayRenderingPid(System.UInt64,System.UInt32)
extern void CVROverlay_SetOverlayRenderingPid_mFA4E4411DFB0EBACE8C629C59BCCA242125B16C2 ();
// 0x00000A7A System.UInt32 OVR.OpenVR.CVROverlay::GetOverlayRenderingPid(System.UInt64)
extern void CVROverlay_GetOverlayRenderingPid_m0AAB2549BED309AE6C60F879426E8566CAD2DF01 ();
// 0x00000A7B OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayFlag(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean)
extern void CVROverlay_SetOverlayFlag_m86A8C95C2B611A94D5AA8CD2F9A9BC6601CB112B ();
// 0x00000A7C OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayFlag(System.UInt64,OVR.OpenVR.VROverlayFlags,System.Boolean&)
extern void CVROverlay_GetOverlayFlag_m918702FCEF67D937D59C696CCC8B8C31A9043738 ();
// 0x00000A7D OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayColor(System.UInt64,System.Single,System.Single,System.Single)
extern void CVROverlay_SetOverlayColor_m960BDB0A2DC1C1E76CA81BF23E16326EFB399766 ();
// 0x00000A7E OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayColor(System.UInt64,System.Single&,System.Single&,System.Single&)
extern void CVROverlay_GetOverlayColor_m094E0E89E1FCE544A89A8AED07AE1DB5F9569444 ();
// 0x00000A7F OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayAlpha(System.UInt64,System.Single)
extern void CVROverlay_SetOverlayAlpha_m2B6529C3A131F8CA605F6B4452D69E0626123EA1 ();
// 0x00000A80 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayAlpha(System.UInt64,System.Single&)
extern void CVROverlay_GetOverlayAlpha_m74ADD661DA41B780600BB5193A360305F19ED5D2 ();
// 0x00000A81 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTexelAspect(System.UInt64,System.Single)
extern void CVROverlay_SetOverlayTexelAspect_m87A8AFC63C9595ED954006A099A94B49E74CCA72 ();
// 0x00000A82 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTexelAspect(System.UInt64,System.Single&)
extern void CVROverlay_GetOverlayTexelAspect_m3890C24E5EBE5620E6F7E5DCAE1B7BCD220D4C9F ();
// 0x00000A83 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlaySortOrder(System.UInt64,System.UInt32)
extern void CVROverlay_SetOverlaySortOrder_mD13E4F88190AC3AFD2C821F2155EC457773F1153 ();
// 0x00000A84 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlaySortOrder(System.UInt64,System.UInt32&)
extern void CVROverlay_GetOverlaySortOrder_m7937B07A448F72BE6C74FB44814BA00C5D2A27DF ();
// 0x00000A85 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayWidthInMeters(System.UInt64,System.Single)
extern void CVROverlay_SetOverlayWidthInMeters_m5CE95588684B94F5BF5E4BC5DDD71FD79CBB49F1 ();
// 0x00000A86 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayWidthInMeters(System.UInt64,System.Single&)
extern void CVROverlay_GetOverlayWidthInMeters_m75BEAB84F7E606153D4A685A00E7B37556C6F28E ();
// 0x00000A87 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayAutoCurveDistanceRangeInMeters(System.UInt64,System.Single,System.Single)
extern void CVROverlay_SetOverlayAutoCurveDistanceRangeInMeters_mEEF37EA229D1FFF1278E1EE73A390DA8BED214CB ();
// 0x00000A88 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayAutoCurveDistanceRangeInMeters(System.UInt64,System.Single&,System.Single&)
extern void CVROverlay_GetOverlayAutoCurveDistanceRangeInMeters_m77811D591EBC53CD84723DFC84FF71CD51F7FAD6 ();
// 0x00000A89 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTextureColorSpace(System.UInt64,OVR.OpenVR.EColorSpace)
extern void CVROverlay_SetOverlayTextureColorSpace_mDC50BCB2F2BDD9A9A0E17F26EC0B0B76574D0C9E ();
// 0x00000A8A OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTextureColorSpace(System.UInt64,OVR.OpenVR.EColorSpace&)
extern void CVROverlay_GetOverlayTextureColorSpace_m3D4B21CBB0BE2A1BA7D1EB650AE9FC4F9743BD0F ();
// 0x00000A8B OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTextureBounds(System.UInt64,OVR.OpenVR.VRTextureBounds_t&)
extern void CVROverlay_SetOverlayTextureBounds_m853DFB2A4A84ED99ECEA4D1DD035E4FB294FDE40 ();
// 0x00000A8C OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTextureBounds(System.UInt64,OVR.OpenVR.VRTextureBounds_t&)
extern void CVROverlay_GetOverlayTextureBounds_m692EA1D0E92BE427057393ED23FFE99009C03A02 ();
// 0x00000A8D System.UInt32 OVR.OpenVR.CVROverlay::GetOverlayRenderModel(System.UInt64,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.HmdColor_t&,OVR.OpenVR.EVROverlayError&)
extern void CVROverlay_GetOverlayRenderModel_mA17D44FC1500407CB7C6ECC05D4380DF1A3C545F ();
// 0x00000A8E OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayRenderModel(System.UInt64,System.String,OVR.OpenVR.HmdColor_t&)
extern void CVROverlay_SetOverlayRenderModel_mFC0F8545A26D4E2256C41944D313AADB48EF08F2 ();
// 0x00000A8F OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTransformType(System.UInt64,OVR.OpenVR.VROverlayTransformType&)
extern void CVROverlay_GetOverlayTransformType_mFDCC93C75DFBF73CCA4B3C72E24A76BFA461732F ();
// 0x00000A90 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTransformAbsolute(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_SetOverlayTransformAbsolute_mBB93F2126BA6739716D2C4E85A67E20D95EF570B ();
// 0x00000A91 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTransformAbsolute(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin&,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_GetOverlayTransformAbsolute_m1A9F970316D84E9D184094CF545F0E3332912BBC ();
// 0x00000A92 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTransformTrackedDeviceRelative(System.UInt64,System.UInt32,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_SetOverlayTransformTrackedDeviceRelative_m6A91238FD31FA53143B144B9A46EF36A5DA04F19 ();
// 0x00000A93 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTransformTrackedDeviceRelative(System.UInt64,System.UInt32&,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_GetOverlayTransformTrackedDeviceRelative_m241AC35F45346300443A37150F2CE1ECB166CAB3 ();
// 0x00000A94 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTransformTrackedDeviceComponent(System.UInt64,System.UInt32,System.String)
extern void CVROverlay_SetOverlayTransformTrackedDeviceComponent_mAB9A53FD83143DDE8F0AC9DC6975620B67424F2F ();
// 0x00000A95 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTransformTrackedDeviceComponent(System.UInt64,System.UInt32&,System.Text.StringBuilder,System.UInt32)
extern void CVROverlay_GetOverlayTransformTrackedDeviceComponent_mD60A1BF4C56B30888EE7CE52CF3808C546D5EAB4 ();
// 0x00000A96 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTransformOverlayRelative(System.UInt64,System.UInt64&,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_GetOverlayTransformOverlayRelative_m3E71324DD7D1D642B7413D03034908E8F678B727 ();
// 0x00000A97 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTransformOverlayRelative(System.UInt64,System.UInt64,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_SetOverlayTransformOverlayRelative_m3909F8C89FFACDA89FCE723661259B6DCD859F19 ();
// 0x00000A98 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::ShowOverlay(System.UInt64)
extern void CVROverlay_ShowOverlay_mB70E419A44942F4ED429BBC22F215E56D2BFC8E9 ();
// 0x00000A99 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::HideOverlay(System.UInt64)
extern void CVROverlay_HideOverlay_m08F6A32CA42F02D7CDD3B552CCD104EBDAC92356 ();
// 0x00000A9A System.Boolean OVR.OpenVR.CVROverlay::IsOverlayVisible(System.UInt64)
extern void CVROverlay_IsOverlayVisible_m4110FE91132C7C61661BB2CDF64A1AF35C5A6940 ();
// 0x00000A9B OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetTransformForOverlayCoordinates(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdVector2_t,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_GetTransformForOverlayCoordinates_m16927E43709669A10CCA29F3E503142882E6D5A7 ();
// 0x00000A9C System.Boolean OVR.OpenVR.CVROverlay::PollNextOverlayEvent(System.UInt64,OVR.OpenVR.VREvent_t&,System.UInt32)
extern void CVROverlay_PollNextOverlayEvent_m2F52848E5569DCCC2323B43D72E222715EF6DDFA ();
// 0x00000A9D OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayInputMethod(System.UInt64,OVR.OpenVR.VROverlayInputMethod&)
extern void CVROverlay_GetOverlayInputMethod_m8F690C1DE5AA9B3C03DDA48A7F230DBAF4852DC2 ();
// 0x00000A9E OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayInputMethod(System.UInt64,OVR.OpenVR.VROverlayInputMethod)
extern void CVROverlay_SetOverlayInputMethod_m0626936627F0925A505DFFCD41A990809E9883FE ();
// 0x00000A9F OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayMouseScale(System.UInt64,OVR.OpenVR.HmdVector2_t&)
extern void CVROverlay_GetOverlayMouseScale_m60EEBFAD8E567775D283BBCCF31892CC169024AA ();
// 0x00000AA0 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayMouseScale(System.UInt64,OVR.OpenVR.HmdVector2_t&)
extern void CVROverlay_SetOverlayMouseScale_m29101DF7B23D1B11585CD68CCBCD9425EF4744BB ();
// 0x00000AA1 System.Boolean OVR.OpenVR.CVROverlay::ComputeOverlayIntersection(System.UInt64,OVR.OpenVR.VROverlayIntersectionParams_t&,OVR.OpenVR.VROverlayIntersectionResults_t&)
extern void CVROverlay_ComputeOverlayIntersection_m059D501999A443467273982881370ECA9975C684 ();
// 0x00000AA2 System.Boolean OVR.OpenVR.CVROverlay::IsHoverTargetOverlay(System.UInt64)
extern void CVROverlay_IsHoverTargetOverlay_mCD9251D72BF5FC35A2F9E7DC0311F6F01EDB1038 ();
// 0x00000AA3 System.UInt64 OVR.OpenVR.CVROverlay::GetGamepadFocusOverlay()
extern void CVROverlay_GetGamepadFocusOverlay_m035F0BF5B3B96343B378DF9C75DF490590210650 ();
// 0x00000AA4 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetGamepadFocusOverlay(System.UInt64)
extern void CVROverlay_SetGamepadFocusOverlay_m300F4E3FF6196AC2F2B0CB264F84DF36301C5A94 ();
// 0x00000AA5 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayNeighbor(OVR.OpenVR.EOverlayDirection,System.UInt64,System.UInt64)
extern void CVROverlay_SetOverlayNeighbor_m2E78C55F999D1D6A32A67308DA6A56C3849D7F1F ();
// 0x00000AA6 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::MoveGamepadFocusToNeighbor(OVR.OpenVR.EOverlayDirection,System.UInt64)
extern void CVROverlay_MoveGamepadFocusToNeighbor_mA8C9E7302D51884C1CF9FEB6673F686D8852F640 ();
// 0x00000AA7 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayDualAnalogTransform(System.UInt64,OVR.OpenVR.EDualAnalogWhich,System.IntPtr,System.Single)
extern void CVROverlay_SetOverlayDualAnalogTransform_m935B9A31E78A05C4B18EB92614288E0656D9146A ();
// 0x00000AA8 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayDualAnalogTransform(System.UInt64,OVR.OpenVR.EDualAnalogWhich,OVR.OpenVR.HmdVector2_t&,System.Single&)
extern void CVROverlay_GetOverlayDualAnalogTransform_mFB732008F3620835D4695E0C34E77C00337A176F ();
// 0x00000AA9 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayTexture(System.UInt64,OVR.OpenVR.Texture_t&)
extern void CVROverlay_SetOverlayTexture_m0150DA767B7D254828F92A72A726C9C7F40203E1 ();
// 0x00000AAA OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::ClearOverlayTexture(System.UInt64)
extern void CVROverlay_ClearOverlayTexture_mD21BC4AD903D915D4EC9FF4140BFB12C38426579 ();
// 0x00000AAB OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayRaw(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32)
extern void CVROverlay_SetOverlayRaw_m48310EB7EC5FD48BCE135C292B68371D362693E7 ();
// 0x00000AAC OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayFromFile(System.UInt64,System.String)
extern void CVROverlay_SetOverlayFromFile_mB73FED316A840B9D867A841618A632D83539B24D ();
// 0x00000AAD OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTexture(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,OVR.OpenVR.ETextureType&,OVR.OpenVR.EColorSpace&,OVR.OpenVR.VRTextureBounds_t&)
extern void CVROverlay_GetOverlayTexture_m0F6944EFEB0FD280561532E88084F4B75A9A211E ();
// 0x00000AAE OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::ReleaseNativeOverlayHandle(System.UInt64,System.IntPtr)
extern void CVROverlay_ReleaseNativeOverlayHandle_m3AAE4476F48A4BA78749FF88A26D775632BEC834 ();
// 0x00000AAF OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayTextureSize(System.UInt64,System.UInt32&,System.UInt32&)
extern void CVROverlay_GetOverlayTextureSize_m2B0F221F60E130BB2900E25ABC03CBF4C642D680 ();
// 0x00000AB0 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::CreateDashboardOverlay(System.String,System.String,System.UInt64&,System.UInt64&)
extern void CVROverlay_CreateDashboardOverlay_mFBE4FAE4E311B54A3B363651EB9EFA02B2905B9B ();
// 0x00000AB1 System.Boolean OVR.OpenVR.CVROverlay::IsDashboardVisible()
extern void CVROverlay_IsDashboardVisible_m87301A1259F13BED5E27766ECB4539904B119B17 ();
// 0x00000AB2 System.Boolean OVR.OpenVR.CVROverlay::IsActiveDashboardOverlay(System.UInt64)
extern void CVROverlay_IsActiveDashboardOverlay_m17234871856DE9425100C08206AAD2C23193967F ();
// 0x00000AB3 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetDashboardOverlaySceneProcess(System.UInt64,System.UInt32)
extern void CVROverlay_SetDashboardOverlaySceneProcess_m48A7B80EE84D02D5FFD2A82A5116E755B219319B ();
// 0x00000AB4 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetDashboardOverlaySceneProcess(System.UInt64,System.UInt32&)
extern void CVROverlay_GetDashboardOverlaySceneProcess_m9F677221AC478C5F0FEB5B8975137E98840B4796 ();
// 0x00000AB5 System.Void OVR.OpenVR.CVROverlay::ShowDashboard(System.String)
extern void CVROverlay_ShowDashboard_m335CDDB205D5F3646863C95CD12D871811B34CA3 ();
// 0x00000AB6 System.UInt32 OVR.OpenVR.CVROverlay::GetPrimaryDashboardDevice()
extern void CVROverlay_GetPrimaryDashboardDevice_m488BBFB983F0A9217306823975D49BF557407D69 ();
// 0x00000AB7 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::ShowKeyboard(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern void CVROverlay_ShowKeyboard_m6F924085E25C6ECF080A51B78BEAD49037B228E5 ();
// 0x00000AB8 OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::ShowKeyboardForOverlay(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern void CVROverlay_ShowKeyboardForOverlay_m3B7C94095053AF1FAB3C7D27847D284718A0497D ();
// 0x00000AB9 System.UInt32 OVR.OpenVR.CVROverlay::GetKeyboardText(System.Text.StringBuilder,System.UInt32)
extern void CVROverlay_GetKeyboardText_mB4A01A6FBF2362CDEB00B783940F67745DC2F0DB ();
// 0x00000ABA System.Void OVR.OpenVR.CVROverlay::HideKeyboard()
extern void CVROverlay_HideKeyboard_m0438506D6A364DB5FE455AF6043AEE95FFE23943 ();
// 0x00000ABB System.Void OVR.OpenVR.CVROverlay::SetKeyboardTransformAbsolute(OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.HmdMatrix34_t&)
extern void CVROverlay_SetKeyboardTransformAbsolute_m4A87227BF4ADA8F1BAE6B060DB9EC11123825864 ();
// 0x00000ABC System.Void OVR.OpenVR.CVROverlay::SetKeyboardPositionForOverlay(System.UInt64,OVR.OpenVR.HmdRect2_t)
extern void CVROverlay_SetKeyboardPositionForOverlay_mC019B03AFA7ABEC64F4C33B35750DB364E83F521 ();
// 0x00000ABD OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::SetOverlayIntersectionMask(System.UInt64,OVR.OpenVR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32)
extern void CVROverlay_SetOverlayIntersectionMask_m572BFA18A61A15AFF13A3780DECF18D51EE8F4E5 ();
// 0x00000ABE OVR.OpenVR.EVROverlayError OVR.OpenVR.CVROverlay::GetOverlayFlags(System.UInt64,System.UInt32&)
extern void CVROverlay_GetOverlayFlags_m7F573F422D712A175F9850F9F1FD71758CBB8626 ();
// 0x00000ABF OVR.OpenVR.VRMessageOverlayResponse OVR.OpenVR.CVROverlay::ShowMessageOverlay(System.String,System.String,System.String,System.String,System.String,System.String)
extern void CVROverlay_ShowMessageOverlay_m6175A154980C28E7C10459BCFD44A3FCEFC59FA5 ();
// 0x00000AC0 System.Void OVR.OpenVR.CVROverlay::CloseMessageOverlay()
extern void CVROverlay_CloseMessageOverlay_m102D6FA392983034598E2583D10EEC563C4EF99F ();
// 0x00000AC1 System.Void OVR.OpenVR.CVROverlay__PollNextOverlayEventPacked::.ctor(System.Object,System.IntPtr)
extern void _PollNextOverlayEventPacked__ctor_m3AD5058F753C1845270442BA16C2912FC016B423 ();
// 0x00000AC2 System.Boolean OVR.OpenVR.CVROverlay__PollNextOverlayEventPacked::Invoke(System.UInt64,OVR.OpenVR.VREvent_t_Packed&,System.UInt32)
extern void _PollNextOverlayEventPacked_Invoke_m841FBDEEBB4925C48635A7F0DA373D0A9177A4F1 ();
// 0x00000AC3 System.IAsyncResult OVR.OpenVR.CVROverlay__PollNextOverlayEventPacked::BeginInvoke(System.UInt64,OVR.OpenVR.VREvent_t_Packed&,System.UInt32,System.AsyncCallback,System.Object)
extern void _PollNextOverlayEventPacked_BeginInvoke_m98C9028194921A8FECA008C4B92C892FF2958062 ();
// 0x00000AC4 System.Boolean OVR.OpenVR.CVROverlay__PollNextOverlayEventPacked::EndInvoke(OVR.OpenVR.VREvent_t_Packed&,System.IAsyncResult)
extern void _PollNextOverlayEventPacked_EndInvoke_mE6AA33EE841DDC005397C07C56E1255597421968 ();
// 0x00000AC5 System.Void OVR.OpenVR.CVRRenderModels::.ctor(System.IntPtr)
extern void CVRRenderModels__ctor_mB2D250428DAE2FA8D776B603A22B2CA658FF3159 ();
// 0x00000AC6 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.CVRRenderModels::LoadRenderModel_Async(System.String,System.IntPtr&)
extern void CVRRenderModels_LoadRenderModel_Async_m8A69052296687918E98F204084BF236BC25DEEE5 ();
// 0x00000AC7 System.Void OVR.OpenVR.CVRRenderModels::FreeRenderModel(System.IntPtr)
extern void CVRRenderModels_FreeRenderModel_m65C0E05E1DE663B48B5D57629EE895106863BE4C ();
// 0x00000AC8 OVR.OpenVR.EVRRenderModelError OVR.OpenVR.CVRRenderModels::LoadTexture_Async(System.Int32,System.IntPtr&)
extern void CVRRenderModels_LoadTexture_Async_m2FC69F68935CD11D23E95310908F03A9F950133E ();
// 0x00000AC9 System.Void OVR.OpenVR.CVRRenderModels::FreeTexture(System.IntPtr)
extern void CVRRenderModels_FreeTexture_m4700DB96B7B33DC99BF681B8EE9AB08E76415379 ();
// 0x00000ACA OVR.OpenVR.EVRRenderModelError OVR.OpenVR.CVRRenderModels::LoadTextureD3D11_Async(System.Int32,System.IntPtr,System.IntPtr&)
extern void CVRRenderModels_LoadTextureD3D11_Async_m61CE24875F44C501C2A8021866BAE4B58F96EC08 ();
// 0x00000ACB OVR.OpenVR.EVRRenderModelError OVR.OpenVR.CVRRenderModels::LoadIntoTextureD3D11_Async(System.Int32,System.IntPtr)
extern void CVRRenderModels_LoadIntoTextureD3D11_Async_mCB33166CEFAF71F4264644C073E9187F7F00E5EE ();
// 0x00000ACC System.Void OVR.OpenVR.CVRRenderModels::FreeTextureD3D11(System.IntPtr)
extern void CVRRenderModels_FreeTextureD3D11_m6513B8CC48317C285F335361084CDAF1EBDA2761 ();
// 0x00000ACD System.UInt32 OVR.OpenVR.CVRRenderModels::GetRenderModelName(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRRenderModels_GetRenderModelName_m6EF74803244E81A8BDF5712F9B5FC4A572D9BDCD ();
// 0x00000ACE System.UInt32 OVR.OpenVR.CVRRenderModels::GetRenderModelCount()
extern void CVRRenderModels_GetRenderModelCount_m88DC87AF530F201C13097A04DBFBC021C447EC00 ();
// 0x00000ACF System.UInt32 OVR.OpenVR.CVRRenderModels::GetComponentCount(System.String)
extern void CVRRenderModels_GetComponentCount_m3A02565E83312F1201E51FE0892ED444EF61E777 ();
// 0x00000AD0 System.UInt32 OVR.OpenVR.CVRRenderModels::GetComponentName(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRRenderModels_GetComponentName_mB9BDCF11834B3C48498CBCA6193F1FA7060C69C9 ();
// 0x00000AD1 System.UInt64 OVR.OpenVR.CVRRenderModels::GetComponentButtonMask(System.String,System.String)
extern void CVRRenderModels_GetComponentButtonMask_mF69D6EC84CA08EF3D624C44129D50F349A5BC927 ();
// 0x00000AD2 System.UInt32 OVR.OpenVR.CVRRenderModels::GetComponentRenderModelName(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRRenderModels_GetComponentRenderModelName_mBB10907509E2334B5E7639963C5FB252F0AF71D1 ();
// 0x00000AD3 System.Boolean OVR.OpenVR.CVRRenderModels::GetComponentStateForDevicePath(System.String,System.String,System.UInt64,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&)
extern void CVRRenderModels_GetComponentStateForDevicePath_m558C8E427C968108355955CF8D7E73FA4FBF75F8 ();
// 0x00000AD4 System.Boolean OVR.OpenVR.CVRRenderModels::GetComponentState(System.String,System.String,OVR.OpenVR.VRControllerState_t&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&)
extern void CVRRenderModels_GetComponentState_m1362E3CC0680982BB33B115566A217808121F413 ();
// 0x00000AD5 System.Boolean OVR.OpenVR.CVRRenderModels::RenderModelHasComponent(System.String,System.String)
extern void CVRRenderModels_RenderModelHasComponent_m4D6B041CC8E069CFDA7322424381C807079ACC62 ();
// 0x00000AD6 System.UInt32 OVR.OpenVR.CVRRenderModels::GetRenderModelThumbnailURL(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&)
extern void CVRRenderModels_GetRenderModelThumbnailURL_mD9C44C31DF2D85461370495A2CC965C832E3200F ();
// 0x00000AD7 System.UInt32 OVR.OpenVR.CVRRenderModels::GetRenderModelOriginalPath(System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRRenderModelError&)
extern void CVRRenderModels_GetRenderModelOriginalPath_mDFE81A16ED54BE7DE6B2A62A99197D46D9D8C3F6 ();
// 0x00000AD8 System.String OVR.OpenVR.CVRRenderModels::GetRenderModelErrorNameFromEnum(OVR.OpenVR.EVRRenderModelError)
extern void CVRRenderModels_GetRenderModelErrorNameFromEnum_m02F55C26FD13B577F3D1DD3B15A57F3EE97A6A99 ();
// 0x00000AD9 System.Void OVR.OpenVR.CVRRenderModels__GetComponentStatePacked::.ctor(System.Object,System.IntPtr)
extern void _GetComponentStatePacked__ctor_m2C63ACCAB5E70B36317F12E810951A8248768812 ();
// 0x00000ADA System.Boolean OVR.OpenVR.CVRRenderModels__GetComponentStatePacked::Invoke(System.String,System.String,OVR.OpenVR.VRControllerState_t_Packed&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&)
extern void _GetComponentStatePacked_Invoke_mAB619F0D73FBE44C3A60E39C14E433571CDDBB49 ();
// 0x00000ADB System.IAsyncResult OVR.OpenVR.CVRRenderModels__GetComponentStatePacked::BeginInvoke(System.String,System.String,OVR.OpenVR.VRControllerState_t_Packed&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.AsyncCallback,System.Object)
extern void _GetComponentStatePacked_BeginInvoke_m8D552EDD2FACC68636E3AF73F880503B20596365 ();
// 0x00000ADC System.Boolean OVR.OpenVR.CVRRenderModels__GetComponentStatePacked::EndInvoke(OVR.OpenVR.VRControllerState_t_Packed&,OVR.OpenVR.RenderModel_ControllerMode_State_t&,OVR.OpenVR.RenderModel_ComponentState_t&,System.IAsyncResult)
extern void _GetComponentStatePacked_EndInvoke_m002D0B2089B8F6B023AF14DE3BBFBB8001DD9B1F ();
// 0x00000ADD System.Void OVR.OpenVR.CVRNotifications::.ctor(System.IntPtr)
extern void CVRNotifications__ctor_mE46E46DDBE9580CF1110EDDE1F75AADA48E2016B ();
// 0x00000ADE OVR.OpenVR.EVRNotificationError OVR.OpenVR.CVRNotifications::CreateNotification(System.UInt64,System.UInt64,OVR.OpenVR.EVRNotificationType,System.String,OVR.OpenVR.EVRNotificationStyle,OVR.OpenVR.NotificationBitmap_t&,System.UInt32&)
extern void CVRNotifications_CreateNotification_m4F1068B8F0A27BE6B58785CCDF2C233F59B5012F ();
// 0x00000ADF OVR.OpenVR.EVRNotificationError OVR.OpenVR.CVRNotifications::RemoveNotification(System.UInt32)
extern void CVRNotifications_RemoveNotification_mECD6914FF3B22E65661B797B6A537D4E96133D2C ();
// 0x00000AE0 System.Void OVR.OpenVR.CVRSettings::.ctor(System.IntPtr)
extern void CVRSettings__ctor_m3D9B422E6F4D973F2378DC1F1D14C835F20809F9 ();
// 0x00000AE1 System.String OVR.OpenVR.CVRSettings::GetSettingsErrorNameFromEnum(OVR.OpenVR.EVRSettingsError)
extern void CVRSettings_GetSettingsErrorNameFromEnum_m552C3BDE021FF81473D55D564587F29D6E77795C ();
// 0x00000AE2 System.Boolean OVR.OpenVR.CVRSettings::Sync(System.Boolean,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_Sync_mE821B98AEC02135199CD43369E5F8CAE4E7542AC ();
// 0x00000AE3 System.Void OVR.OpenVR.CVRSettings::SetBool(System.String,System.String,System.Boolean,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_SetBool_m05E89BB13896B6FF8F4BD27AF11805A76FB3C924 ();
// 0x00000AE4 System.Void OVR.OpenVR.CVRSettings::SetInt32(System.String,System.String,System.Int32,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_SetInt32_mF17663555CE0C5AC5CC4B6AF0F2BF94DB893ECBC ();
// 0x00000AE5 System.Void OVR.OpenVR.CVRSettings::SetFloat(System.String,System.String,System.Single,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_SetFloat_m97E84C70A70B5FF097C29A4095F20F6BFDCB526C ();
// 0x00000AE6 System.Void OVR.OpenVR.CVRSettings::SetString(System.String,System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_SetString_m4A3A6AB65ED55237364138BB0BF7DD95ADA91A59 ();
// 0x00000AE7 System.Boolean OVR.OpenVR.CVRSettings::GetBool(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_GetBool_m719C5D19E931D79F24A84FD301B7EF007894272C ();
// 0x00000AE8 System.Int32 OVR.OpenVR.CVRSettings::GetInt32(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_GetInt32_mB6739278C17F5AFB388FFDF17F01DD888393A927 ();
// 0x00000AE9 System.Single OVR.OpenVR.CVRSettings::GetFloat(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_GetFloat_m842C0F02566E6E34AFE35C9DC48093BE1166F698 ();
// 0x00000AEA System.Void OVR.OpenVR.CVRSettings::GetString(System.String,System.String,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_GetString_m46D7AA23613E9B8D62A554F053CABE717C27602F ();
// 0x00000AEB System.Void OVR.OpenVR.CVRSettings::RemoveSection(System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_RemoveSection_m82910E182C38CF3893ED58D9F044C392F5D7F6D0 ();
// 0x00000AEC System.Void OVR.OpenVR.CVRSettings::RemoveKeyInSection(System.String,System.String,OVR.OpenVR.EVRSettingsError&)
extern void CVRSettings_RemoveKeyInSection_m69E4BDCAED17B79DB2DE701F58E53CD4B7FFA649 ();
// 0x00000AED System.Void OVR.OpenVR.CVRScreenshots::.ctor(System.IntPtr)
extern void CVRScreenshots__ctor_mF29218A6DE79028B39FA354C95B6F38779345772 ();
// 0x00000AEE OVR.OpenVR.EVRScreenshotError OVR.OpenVR.CVRScreenshots::RequestScreenshot(System.UInt32&,OVR.OpenVR.EVRScreenshotType,System.String,System.String)
extern void CVRScreenshots_RequestScreenshot_mC8963E9115B7DFB0D5C4660FF3577AE1861131FD ();
// 0x00000AEF OVR.OpenVR.EVRScreenshotError OVR.OpenVR.CVRScreenshots::HookScreenshot(OVR.OpenVR.EVRScreenshotType[])
extern void CVRScreenshots_HookScreenshot_mC7DEA6CE01BFD92C36FF3919DE3FA5D8AD273770 ();
// 0x00000AF0 OVR.OpenVR.EVRScreenshotType OVR.OpenVR.CVRScreenshots::GetScreenshotPropertyType(System.UInt32,OVR.OpenVR.EVRScreenshotError&)
extern void CVRScreenshots_GetScreenshotPropertyType_m12607755F749EE68E33692455FAF604BF827A350 ();
// 0x00000AF1 System.UInt32 OVR.OpenVR.CVRScreenshots::GetScreenshotPropertyFilename(System.UInt32,OVR.OpenVR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,OVR.OpenVR.EVRScreenshotError&)
extern void CVRScreenshots_GetScreenshotPropertyFilename_mEE07EA543DC64B3222D0F9EE17EB1B5B1F3CE87C ();
// 0x00000AF2 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.CVRScreenshots::UpdateScreenshotProgress(System.UInt32,System.Single)
extern void CVRScreenshots_UpdateScreenshotProgress_mF8D94A23604064179F3A27BF9C9BA28C96359D28 ();
// 0x00000AF3 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.CVRScreenshots::TakeStereoScreenshot(System.UInt32&,System.String,System.String)
extern void CVRScreenshots_TakeStereoScreenshot_m081E4D6B9F0EC7062C0B54C7C1CBB2CC45EA79DF ();
// 0x00000AF4 OVR.OpenVR.EVRScreenshotError OVR.OpenVR.CVRScreenshots::SubmitScreenshot(System.UInt32,OVR.OpenVR.EVRScreenshotType,System.String,System.String)
extern void CVRScreenshots_SubmitScreenshot_m7F724E0508B82F20BBCBB811C7E27097F43C4F3F ();
// 0x00000AF5 System.Void OVR.OpenVR.CVRResources::.ctor(System.IntPtr)
extern void CVRResources__ctor_mA422F38039A0C04B10DA15555D1ACAACB31E7E91 ();
// 0x00000AF6 System.UInt32 OVR.OpenVR.CVRResources::LoadSharedResource(System.String,System.String,System.UInt32)
extern void CVRResources_LoadSharedResource_mE6A74185A56D1F9D2BDFB033F68EA1F96AD30304 ();
// 0x00000AF7 System.UInt32 OVR.OpenVR.CVRResources::GetResourceFullPath(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern void CVRResources_GetResourceFullPath_mA7E53C24113F02EBE7097F22E30E043C2A1CA051 ();
// 0x00000AF8 System.Void OVR.OpenVR.CVRDriverManager::.ctor(System.IntPtr)
extern void CVRDriverManager__ctor_m926ABA41F220B282081645BDA0A4FC9D3925C914 ();
// 0x00000AF9 System.UInt32 OVR.OpenVR.CVRDriverManager::GetDriverCount()
extern void CVRDriverManager_GetDriverCount_mA1F81B38336ACA594AB6364C071CD87A453DDAAA ();
// 0x00000AFA System.UInt32 OVR.OpenVR.CVRDriverManager::GetDriverName(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void CVRDriverManager_GetDriverName_mACA1DFCD465B041A6453FCF6FEE37BDB7A4EB8E6 ();
// 0x00000AFB System.UInt64 OVR.OpenVR.CVRDriverManager::GetDriverHandle(System.String)
extern void CVRDriverManager_GetDriverHandle_m81D2B104C742B4246AC4398746FCD0E1FEB07396 ();
// 0x00000AFC System.Void OVR.OpenVR.CVRInput::.ctor(System.IntPtr)
extern void CVRInput__ctor_m93499950AFA147BFFCB43F458CEDF877C431A18D ();
// 0x00000AFD OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::SetActionManifestPath(System.String)
extern void CVRInput_SetActionManifestPath_mCEBFE2947D8D9ED21CC24A06D220B022D997FD17 ();
// 0x00000AFE OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetActionSetHandle(System.String,System.UInt64&)
extern void CVRInput_GetActionSetHandle_mCBDEBC1DA75F0CD9E570D221708F4A529152E996 ();
// 0x00000AFF OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetActionHandle(System.String,System.UInt64&)
extern void CVRInput_GetActionHandle_mC360EC86FC53063990C309C27C887CCA2A6FE3B3 ();
// 0x00000B00 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetInputSourceHandle(System.String,System.UInt64&)
extern void CVRInput_GetInputSourceHandle_m8A8CBE4084703B848A2CC7DB0FD02B51AA7FD215 ();
// 0x00000B01 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::UpdateActionState(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32)
extern void CVRInput_UpdateActionState_mCC82BE615585536DE8EA64951EB9B674305CDB9E ();
// 0x00000B02 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetDigitalActionData(System.UInt64,OVR.OpenVR.InputDigitalActionData_t&,System.UInt32,System.UInt64)
extern void CVRInput_GetDigitalActionData_m9E346B991DABB1D8B67727E4084C4C59C369A3FF ();
// 0x00000B03 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetAnalogActionData(System.UInt64,OVR.OpenVR.InputAnalogActionData_t&,System.UInt32,System.UInt64)
extern void CVRInput_GetAnalogActionData_m5CAAA2E5DB199F5D40366E0B881A8353B575FB8D ();
// 0x00000B04 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetPoseActionData(System.UInt64,OVR.OpenVR.ETrackingUniverseOrigin,System.Single,OVR.OpenVR.InputPoseActionData_t&,System.UInt32,System.UInt64)
extern void CVRInput_GetPoseActionData_m28B3AB9BDEA5AE66784E67C3F9C6F8D3067ED321 ();
// 0x00000B05 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetSkeletalActionData(System.UInt64,OVR.OpenVR.InputSkeletalActionData_t&,System.UInt32,System.UInt64)
extern void CVRInput_GetSkeletalActionData_m51CE11F3E4F1E51B4E39430C8F23DE007573C0A0 ();
// 0x00000B06 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetSkeletalBoneData(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,OVR.OpenVR.VRBoneTransform_t[],System.UInt64)
extern void CVRInput_GetSkeletalBoneData_m768448C790F705B814918D05A0F0F4D4DF8D3852 ();
// 0x00000B07 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetSkeletalBoneDataCompressed(System.UInt64,OVR.OpenVR.EVRSkeletalTransformSpace,OVR.OpenVR.EVRSkeletalMotionRange,System.IntPtr,System.UInt32,System.UInt32&,System.UInt64)
extern void CVRInput_GetSkeletalBoneDataCompressed_m2B3BA18AD0728BE6EF10D9BA7D2670F002EEC21E ();
// 0x00000B08 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::DecompressSkeletalBoneData(System.IntPtr,System.UInt32,OVR.OpenVR.EVRSkeletalTransformSpace&,OVR.OpenVR.VRBoneTransform_t[])
extern void CVRInput_DecompressSkeletalBoneData_m972891B09E03B1B8706496363CFE6626E1F2DCCB ();
// 0x00000B09 OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::TriggerHapticVibrationAction(System.UInt64,System.Single,System.Single,System.Single,System.Single,System.UInt64)
extern void CVRInput_TriggerHapticVibrationAction_m4567E463399C22D0462528F7AD6995D6FACE30CD ();
// 0x00000B0A OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetActionOrigins(System.UInt64,System.UInt64,System.UInt64[])
extern void CVRInput_GetActionOrigins_m15338484B4F7CBBC1E5BAC61E1906E98936B1687 ();
// 0x00000B0B OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetOriginLocalizedName(System.UInt64,System.Text.StringBuilder,System.UInt32)
extern void CVRInput_GetOriginLocalizedName_m1625FBA989C4A016301F7BF7696B4977858DC27C ();
// 0x00000B0C OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::GetOriginTrackedDeviceInfo(System.UInt64,OVR.OpenVR.InputOriginInfo_t&,System.UInt32)
extern void CVRInput_GetOriginTrackedDeviceInfo_m8C1CAE327C2F5A726F43BD585064BEB2E3759965 ();
// 0x00000B0D OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::ShowActionOrigins(System.UInt64,System.UInt64)
extern void CVRInput_ShowActionOrigins_m020ED911DD5A20A518330ED33FF173D267B6849D ();
// 0x00000B0E OVR.OpenVR.EVRInputError OVR.OpenVR.CVRInput::ShowBindingsForActionSet(OVR.OpenVR.VRActiveActionSet_t[],System.UInt32,System.UInt64)
extern void CVRInput_ShowBindingsForActionSet_m143F47398111A093E5603FD5D1F8240F67E5B123 ();
// 0x00000B0F System.Void OVR.OpenVR.CVRIOBuffer::.ctor(System.IntPtr)
extern void CVRIOBuffer__ctor_m9B1361C9CFB9F7AE47221CAA702A7EF9577F0AFA ();
// 0x00000B10 OVR.OpenVR.EIOBufferError OVR.OpenVR.CVRIOBuffer::Open(System.String,OVR.OpenVR.EIOBufferMode,System.UInt32,System.UInt32,System.UInt64&)
extern void CVRIOBuffer_Open_m2E9C20805EBC71567F148EADDCD561519161E70C ();
// 0x00000B11 OVR.OpenVR.EIOBufferError OVR.OpenVR.CVRIOBuffer::Close(System.UInt64)
extern void CVRIOBuffer_Close_m605FECE72296D6684056D0462A120926C973E4DC ();
// 0x00000B12 OVR.OpenVR.EIOBufferError OVR.OpenVR.CVRIOBuffer::Read(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&)
extern void CVRIOBuffer_Read_mAB49BEBDE806B4EF072AADEE34ABA90BF57AF6E0 ();
// 0x00000B13 OVR.OpenVR.EIOBufferError OVR.OpenVR.CVRIOBuffer::Write(System.UInt64,System.IntPtr,System.UInt32)
extern void CVRIOBuffer_Write_m646CA08DE5BE582E9D6A38C6348E2BEB75542478 ();
// 0x00000B14 System.UInt64 OVR.OpenVR.CVRIOBuffer::PropertyContainer(System.UInt64)
extern void CVRIOBuffer_PropertyContainer_mB3656041524B72D8E8D616816B5024477B0232EF ();
// 0x00000B15 System.Void OVR.OpenVR.CVRSpatialAnchors::.ctor(System.IntPtr)
extern void CVRSpatialAnchors__ctor_m96CB910D1648888981EFE609464C45387B262C97 ();
// 0x00000B16 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.CVRSpatialAnchors::CreateSpatialAnchorFromDescriptor(System.String,System.UInt32&)
extern void CVRSpatialAnchors_CreateSpatialAnchorFromDescriptor_m0181799D3386993F52632AF7650E3B72DF20A696 ();
// 0x00000B17 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.CVRSpatialAnchors::CreateSpatialAnchorFromPose(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&,System.UInt32&)
extern void CVRSpatialAnchors_CreateSpatialAnchorFromPose_m4121FB26A811100AF9116E8E7CA487DA4C888085 ();
// 0x00000B18 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.CVRSpatialAnchors::GetSpatialAnchorPose(System.UInt32,OVR.OpenVR.ETrackingUniverseOrigin,OVR.OpenVR.SpatialAnchorPose_t&)
extern void CVRSpatialAnchors_GetSpatialAnchorPose_m6FC4339E984AFD647E37560642CF46FA4BD0EC92 ();
// 0x00000B19 OVR.OpenVR.EVRSpatialAnchorError OVR.OpenVR.CVRSpatialAnchors::GetSpatialAnchorDescriptor(System.UInt32,System.Text.StringBuilder,System.UInt32&)
extern void CVRSpatialAnchors_GetSpatialAnchorDescriptor_m2A288A011AB04AE0A5BDBE4239D5E479C2906AAC ();
// 0x00000B1A System.UInt32 OVR.OpenVR.OpenVRInterop::InitInternal(OVR.OpenVR.EVRInitError&,OVR.OpenVR.EVRApplicationType)
extern void OpenVRInterop_InitInternal_m568739546B3644F5AF8CCDD0C08FF65EC6B5069F ();
// 0x00000B1B System.UInt32 OVR.OpenVR.OpenVRInterop::InitInternal2(OVR.OpenVR.EVRInitError&,OVR.OpenVR.EVRApplicationType,System.String)
extern void OpenVRInterop_InitInternal2_m8DD236ACD171504D571A22A2744E1F22C8043348 ();
// 0x00000B1C System.Void OVR.OpenVR.OpenVRInterop::ShutdownInternal()
extern void OpenVRInterop_ShutdownInternal_mB657DDA2CE332848F050592E53AC3B187DACC6DB ();
// 0x00000B1D System.Boolean OVR.OpenVR.OpenVRInterop::IsHmdPresent()
extern void OpenVRInterop_IsHmdPresent_m1E47CE90061744864243A10DA88584C19101F13F ();
// 0x00000B1E System.Boolean OVR.OpenVR.OpenVRInterop::IsRuntimeInstalled()
extern void OpenVRInterop_IsRuntimeInstalled_mCD5BB11DF2D20B589BA972459E12094F9A7E45FE ();
// 0x00000B1F System.IntPtr OVR.OpenVR.OpenVRInterop::GetStringForHmdError(OVR.OpenVR.EVRInitError)
extern void OpenVRInterop_GetStringForHmdError_m36B6A90A280F0688D4FF0BA018B7FA028E8CF498 ();
// 0x00000B20 System.IntPtr OVR.OpenVR.OpenVRInterop::GetGenericInterface(System.String,OVR.OpenVR.EVRInitError&)
extern void OpenVRInterop_GetGenericInterface_mBCFC4564E20F64ABF53F77AD2DD96A04FD99E5D5 ();
// 0x00000B21 System.Boolean OVR.OpenVR.OpenVRInterop::IsInterfaceVersionValid(System.String)
extern void OpenVRInterop_IsInterfaceVersionValid_m3BC837A21B957750304097F137FC40D410ADF5A2 ();
// 0x00000B22 System.UInt32 OVR.OpenVR.OpenVRInterop::GetInitToken()
extern void OpenVRInterop_GetInitToken_m39C4899B8394BA07D8E90E4B7F1B06389298FE52 ();
// 0x00000B23 System.Void OVR.OpenVR.OpenVRInterop::.ctor()
extern void OpenVRInterop__ctor_m749F588D74B3DB884003CC1A139E77012368ACE4 ();
// 0x00000B24 System.String OVR.OpenVR.VREvent_Keyboard_t::get_cNewInput()
extern void VREvent_Keyboard_t_get_cNewInput_m14DD64244AE9463A168869227270198484560792_AdjustorThunk ();
// 0x00000B25 System.Void OVR.OpenVR.VREvent_t_Packed::.ctor(OVR.OpenVR.VREvent_t)
extern void VREvent_t_Packed__ctor_mCAC3B6E0EFDD1A595CA699DC9182F16F408AB6D0_AdjustorThunk ();
// 0x00000B26 System.Void OVR.OpenVR.VREvent_t_Packed::Unpack(OVR.OpenVR.VREvent_t&)
extern void VREvent_t_Packed_Unpack_m534F1D0D47F5A93B930E931EA0588CBC93E84E22_AdjustorThunk ();
// 0x00000B27 System.Void OVR.OpenVR.VRControllerState_t_Packed::.ctor(OVR.OpenVR.VRControllerState_t)
extern void VRControllerState_t_Packed__ctor_m34E6CACCCC7E682AE448E5368F6ED2F425E95617_AdjustorThunk ();
// 0x00000B28 System.Void OVR.OpenVR.VRControllerState_t_Packed::Unpack(OVR.OpenVR.VRControllerState_t&)
extern void VRControllerState_t_Packed_Unpack_m509D69C9DD8602F41485A476D58EC6088AAFDB74_AdjustorThunk ();
// 0x00000B29 System.Void OVR.OpenVR.RenderModel_TextureMap_t_Packed::.ctor(OVR.OpenVR.RenderModel_TextureMap_t)
extern void RenderModel_TextureMap_t_Packed__ctor_m5911977B45A3A72A9ADC58BA9642A3BCBCFF3522_AdjustorThunk ();
// 0x00000B2A System.Void OVR.OpenVR.RenderModel_TextureMap_t_Packed::Unpack(OVR.OpenVR.RenderModel_TextureMap_t&)
extern void RenderModel_TextureMap_t_Packed_Unpack_m8C2D3ACDF93812B1A575A5CCBF1D9523B21C88F6_AdjustorThunk ();
// 0x00000B2B System.Void OVR.OpenVR.RenderModel_t_Packed::.ctor(OVR.OpenVR.RenderModel_t)
extern void RenderModel_t_Packed__ctor_m8E65089C982BB9444CCE5C657B9AF1C6A7E7EE0E_AdjustorThunk ();
// 0x00000B2C System.Void OVR.OpenVR.RenderModel_t_Packed::Unpack(OVR.OpenVR.RenderModel_t&)
extern void RenderModel_t_Packed_Unpack_m309CC4FBED4B55D7A9098A9EB418FF7E76845741_AdjustorThunk ();
// 0x00000B2D System.String OVR.OpenVR.InputOriginInfo_t::get_rchRenderModelComponentName()
extern void InputOriginInfo_t_get_rchRenderModelComponentName_m0DA945ED08BEC0FC8A53BB7958AED51396D53DFE_AdjustorThunk ();
// 0x00000B2E System.UInt32 OVR.OpenVR.OpenVR::InitInternal(OVR.OpenVR.EVRInitError&,OVR.OpenVR.EVRApplicationType)
extern void OpenVR_InitInternal_mE771350E30E10D786C5A3BD3C414D5473D61C81B ();
// 0x00000B2F System.UInt32 OVR.OpenVR.OpenVR::InitInternal2(OVR.OpenVR.EVRInitError&,OVR.OpenVR.EVRApplicationType,System.String)
extern void OpenVR_InitInternal2_mD247062D7446BB524CA51B420F7969FD2EC9865A ();
// 0x00000B30 System.Void OVR.OpenVR.OpenVR::ShutdownInternal()
extern void OpenVR_ShutdownInternal_m4109BCAEB683C85B36EFC41176AADEC7462A0C74 ();
// 0x00000B31 System.Boolean OVR.OpenVR.OpenVR::IsHmdPresent()
extern void OpenVR_IsHmdPresent_mDC91BFD66BCA5300BA304AA67BE26C0EF4086DE9 ();
// 0x00000B32 System.Boolean OVR.OpenVR.OpenVR::IsRuntimeInstalled()
extern void OpenVR_IsRuntimeInstalled_mD8D73AC1DAD74AD552C4D11F360027A1963AE17B ();
// 0x00000B33 System.String OVR.OpenVR.OpenVR::GetStringForHmdError(OVR.OpenVR.EVRInitError)
extern void OpenVR_GetStringForHmdError_mA5F4CF84E1D471A755206BFC1F50F19A1DB6C1AC ();
// 0x00000B34 System.IntPtr OVR.OpenVR.OpenVR::GetGenericInterface(System.String,OVR.OpenVR.EVRInitError&)
extern void OpenVR_GetGenericInterface_mB0623919D353B0124FC52B5B1511349DCB5CC3DB ();
// 0x00000B35 System.Boolean OVR.OpenVR.OpenVR::IsInterfaceVersionValid(System.String)
extern void OpenVR_IsInterfaceVersionValid_mCD8CEC94397A5E2135A566BBD1D5355DFA72099E ();
// 0x00000B36 System.UInt32 OVR.OpenVR.OpenVR::GetInitToken()
extern void OpenVR_GetInitToken_m4E36F591850B3D68AE413A8625FB28BA263F8295 ();
// 0x00000B37 System.UInt32 OVR.OpenVR.OpenVR::get_VRToken()
extern void OpenVR_get_VRToken_m2310C0C3AF4EBE907F9031C305C6144E33B7554F ();
// 0x00000B38 System.Void OVR.OpenVR.OpenVR::set_VRToken(System.UInt32)
extern void OpenVR_set_VRToken_m9D00D4A0135DCBFF68CDB9F3E297325F77794C05 ();
// 0x00000B39 OVR.OpenVR.OpenVR_COpenVRContext OVR.OpenVR.OpenVR::get_OpenVRInternal_ModuleContext()
extern void OpenVR_get_OpenVRInternal_ModuleContext_m9A9049EF7A830EBC29CA1C079F77C3CF9819B80F ();
// 0x00000B3A OVR.OpenVR.CVRSystem OVR.OpenVR.OpenVR::get_System()
extern void OpenVR_get_System_mF933C580A6BFFABA5407F9DC3FBD5C69572E7C80 ();
// 0x00000B3B OVR.OpenVR.CVRChaperone OVR.OpenVR.OpenVR::get_Chaperone()
extern void OpenVR_get_Chaperone_mB667AC54C23E60BCAA038137564E265CE3891389 ();
// 0x00000B3C OVR.OpenVR.CVRChaperoneSetup OVR.OpenVR.OpenVR::get_ChaperoneSetup()
extern void OpenVR_get_ChaperoneSetup_m467461EBE5CFF64CE0B6520519D5FC1988915B0E ();
// 0x00000B3D OVR.OpenVR.CVRCompositor OVR.OpenVR.OpenVR::get_Compositor()
extern void OpenVR_get_Compositor_mDF1B686D8D4DD188496054F8A5787298D0330DB3 ();
// 0x00000B3E OVR.OpenVR.CVROverlay OVR.OpenVR.OpenVR::get_Overlay()
extern void OpenVR_get_Overlay_m9AFF87BEFD829CA799278820A18AF0BD0007253B ();
// 0x00000B3F OVR.OpenVR.CVRRenderModels OVR.OpenVR.OpenVR::get_RenderModels()
extern void OpenVR_get_RenderModels_m5C205FE40330D719E6E9C6554A035254BD600B76 ();
// 0x00000B40 OVR.OpenVR.CVRExtendedDisplay OVR.OpenVR.OpenVR::get_ExtendedDisplay()
extern void OpenVR_get_ExtendedDisplay_m76BD047FE7886FC58D0BD7BA7C45978BD1A670F7 ();
// 0x00000B41 OVR.OpenVR.CVRSettings OVR.OpenVR.OpenVR::get_Settings()
extern void OpenVR_get_Settings_mBB2E7B2FDC1239880A47512AA4446B215D404F60 ();
// 0x00000B42 OVR.OpenVR.CVRApplications OVR.OpenVR.OpenVR::get_Applications()
extern void OpenVR_get_Applications_mF3FCC5009CE10471DFFFD6C3E52B538E4176C506 ();
// 0x00000B43 OVR.OpenVR.CVRScreenshots OVR.OpenVR.OpenVR::get_Screenshots()
extern void OpenVR_get_Screenshots_m47C049E83B5144D0B3EF9BCB24985A31AE169486 ();
// 0x00000B44 OVR.OpenVR.CVRTrackedCamera OVR.OpenVR.OpenVR::get_TrackedCamera()
extern void OpenVR_get_TrackedCamera_m1D6DA741DEA6FC1C83E72B5601377A7CAA1CB3AC ();
// 0x00000B45 OVR.OpenVR.CVRInput OVR.OpenVR.OpenVR::get_Input()
extern void OpenVR_get_Input_mAF2A670BBF0B1B250E542BDEB163B506F43A4C1E ();
// 0x00000B46 OVR.OpenVR.CVRSpatialAnchors OVR.OpenVR.OpenVR::get_SpatialAnchors()
extern void OpenVR_get_SpatialAnchors_mBFB9970144290BD78BA18F7E14DDD591BF8A2F82 ();
// 0x00000B47 OVR.OpenVR.CVRSystem OVR.OpenVR.OpenVR::Init(OVR.OpenVR.EVRInitError&,OVR.OpenVR.EVRApplicationType,System.String)
extern void OpenVR_Init_m72FE9104C1F368B45E659989DAA8B8F340990997 ();
// 0x00000B48 System.Void OVR.OpenVR.OpenVR::Shutdown()
extern void OpenVR_Shutdown_m4D3755D88EA1E2B53B4EEE6BC1CF9E0B19A22ADF ();
// 0x00000B49 System.Void OVR.OpenVR.OpenVR::.ctor()
extern void OpenVR__ctor_m2EBE59672164ACEB691FC9F1E87A1EDF07CE7B53 ();
// 0x00000B4A System.Void OVR.OpenVR.OpenVR::.cctor()
extern void OpenVR__cctor_m65D25BC9753EAAEAA5A0567C817C16A00F1AFF42 ();
// 0x00000B4B System.Void OVR.OpenVR.OpenVR_COpenVRContext::.ctor()
extern void COpenVRContext__ctor_m40510A343019E4FACB7110000D5E6C00E1C5AF0A ();
// 0x00000B4C System.Void OVR.OpenVR.OpenVR_COpenVRContext::Clear()
extern void COpenVRContext_Clear_m077E28DDA05F2B32AEE885B5EDF80BA376D6F7D2 ();
// 0x00000B4D System.Void OVR.OpenVR.OpenVR_COpenVRContext::CheckClear()
extern void COpenVRContext_CheckClear_m10C11DFBE0A94A81ABD15F2EAEC96A2355604061 ();
// 0x00000B4E OVR.OpenVR.CVRSystem OVR.OpenVR.OpenVR_COpenVRContext::VRSystem()
extern void COpenVRContext_VRSystem_m519F3D56E3527EE965293D0B5C815DCFE17ABF3F ();
// 0x00000B4F OVR.OpenVR.CVRChaperone OVR.OpenVR.OpenVR_COpenVRContext::VRChaperone()
extern void COpenVRContext_VRChaperone_mE22C101BF1701969221893A911151EE1E771F6BB ();
// 0x00000B50 OVR.OpenVR.CVRChaperoneSetup OVR.OpenVR.OpenVR_COpenVRContext::VRChaperoneSetup()
extern void COpenVRContext_VRChaperoneSetup_m09F8A1CA3E0661D224CDC22222D679467B816AB3 ();
// 0x00000B51 OVR.OpenVR.CVRCompositor OVR.OpenVR.OpenVR_COpenVRContext::VRCompositor()
extern void COpenVRContext_VRCompositor_mBB26E115FB134B467D5633650D8CE6D3A08C0429 ();
// 0x00000B52 OVR.OpenVR.CVROverlay OVR.OpenVR.OpenVR_COpenVRContext::VROverlay()
extern void COpenVRContext_VROverlay_mE13219C0F0597A936E2BF8A700048B1F9771AEDC ();
// 0x00000B53 OVR.OpenVR.CVRRenderModels OVR.OpenVR.OpenVR_COpenVRContext::VRRenderModels()
extern void COpenVRContext_VRRenderModels_m15D7DA92B230A1BE2DBC35FF6D927E75F3F28F0F ();
// 0x00000B54 OVR.OpenVR.CVRExtendedDisplay OVR.OpenVR.OpenVR_COpenVRContext::VRExtendedDisplay()
extern void COpenVRContext_VRExtendedDisplay_m57224F91F5C9A71FBE76B78CD15A37562BCE0D40 ();
// 0x00000B55 OVR.OpenVR.CVRSettings OVR.OpenVR.OpenVR_COpenVRContext::VRSettings()
extern void COpenVRContext_VRSettings_mA6D310CAA581EE36CC275689CE37326262FB9367 ();
// 0x00000B56 OVR.OpenVR.CVRApplications OVR.OpenVR.OpenVR_COpenVRContext::VRApplications()
extern void COpenVRContext_VRApplications_m9B653BE8CBF352990AEF61B5BDC404F2137364C6 ();
// 0x00000B57 OVR.OpenVR.CVRScreenshots OVR.OpenVR.OpenVR_COpenVRContext::VRScreenshots()
extern void COpenVRContext_VRScreenshots_mC83348547277C55143B84F8864F8A352A1EDC3E8 ();
// 0x00000B58 OVR.OpenVR.CVRTrackedCamera OVR.OpenVR.OpenVR_COpenVRContext::VRTrackedCamera()
extern void COpenVRContext_VRTrackedCamera_m07A37EF628B8788F38BCFE694F4DEF277B77A4CC ();
// 0x00000B59 OVR.OpenVR.CVRInput OVR.OpenVR.OpenVR_COpenVRContext::VRInput()
extern void COpenVRContext_VRInput_mB0073EDACD24D7ED231CCA8F32699CC6FA589684 ();
// 0x00000B5A OVR.OpenVR.CVRSpatialAnchors OVR.OpenVR.OpenVR_COpenVRContext::VRSpatialAnchors()
extern void COpenVRContext_VRSpatialAnchors_m371F2530754D0AA51949041D030251C1D23068DE ();
// 0x00000B5B System.Void Assets.OVR.Scripts.Record::.ctor(System.String,System.String)
extern void Record__ctor_m939BD87E9B88D65636F54D91592DE103C7B8F755 ();
// 0x00000B5C System.Void Assets.OVR.Scripts.RangedRecord::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
extern void RangedRecord__ctor_m7D7ED72EA04EA1974D7903848020E6A6CCE86A4B ();
// 0x00000B5D System.Void Assets.OVR.Scripts.FixMethodDelegate::.ctor(System.Object,System.IntPtr)
extern void FixMethodDelegate__ctor_mA6E4BDE779409869DD3B3F67BE03D29FE43839C3 ();
// 0x00000B5E System.Void Assets.OVR.Scripts.FixMethodDelegate::Invoke(UnityEngine.Object,System.Boolean,System.Int32)
extern void FixMethodDelegate_Invoke_mE4CD1ABFDC654C00B8924EF2AF54A630936B0139 ();
// 0x00000B5F System.IAsyncResult Assets.OVR.Scripts.FixMethodDelegate::BeginInvoke(UnityEngine.Object,System.Boolean,System.Int32,System.AsyncCallback,System.Object)
extern void FixMethodDelegate_BeginInvoke_m7EDF41DE1B6E0B8CA1300262B0065271E7AEB14E ();
// 0x00000B60 System.Void Assets.OVR.Scripts.FixMethodDelegate::EndInvoke(System.IAsyncResult)
extern void FixMethodDelegate_EndInvoke_m2D8B56DF4CE29BFD73212B011512372AC7EFC0E1 ();
// 0x00000B61 System.Void Assets.OVR.Scripts.FixRecord::.ctor(System.String,System.String,Assets.OVR.Scripts.FixMethodDelegate,UnityEngine.Object,System.Boolean,System.String[])
extern void FixRecord__ctor_mB8229134FB2C9D3CCF1890C65F3F98274C6F6979 ();
// 0x00000B62 System.Void UnityEngine.EventSystems.OVRInputModule::.ctor()
extern void OVRInputModule__ctor_m5CB834B641E6CD82A8FB2F3C1E92B6CCCD1A06B4 ();
// 0x00000B63 UnityEngine.EventSystems.OVRInputModule_InputMode UnityEngine.EventSystems.OVRInputModule::get_inputMode()
extern void OVRInputModule_get_inputMode_m481170BF22DE4F18EEAAD7C8FE15171C3B9051BB ();
// 0x00000B64 System.Boolean UnityEngine.EventSystems.OVRInputModule::get_allowActivationOnMobileDevice()
extern void OVRInputModule_get_allowActivationOnMobileDevice_m244430FC5B9C7EF5D9F8949C92A0812E8C3FD21C ();
// 0x00000B65 System.Void UnityEngine.EventSystems.OVRInputModule::set_allowActivationOnMobileDevice(System.Boolean)
extern void OVRInputModule_set_allowActivationOnMobileDevice_mEFD1992FC38C19E69E64A361AFD453B537EF7971 ();
// 0x00000B66 System.Single UnityEngine.EventSystems.OVRInputModule::get_inputActionsPerSecond()
extern void OVRInputModule_get_inputActionsPerSecond_m578E96A49DA2442EE7C3E08CF3DEBACFCB2143D0 ();
// 0x00000B67 System.Void UnityEngine.EventSystems.OVRInputModule::set_inputActionsPerSecond(System.Single)
extern void OVRInputModule_set_inputActionsPerSecond_m81B0E3A1026918FF067C1C26ABE0F1600694BA7B ();
// 0x00000B68 System.String UnityEngine.EventSystems.OVRInputModule::get_horizontalAxis()
extern void OVRInputModule_get_horizontalAxis_mBA8AF77470337EB43E34141A40082C1EF6C867EC ();
// 0x00000B69 System.Void UnityEngine.EventSystems.OVRInputModule::set_horizontalAxis(System.String)
extern void OVRInputModule_set_horizontalAxis_m2C0C18FA91EE05DEEAA8A5F29C2CAC6D61A06F4A ();
// 0x00000B6A System.String UnityEngine.EventSystems.OVRInputModule::get_verticalAxis()
extern void OVRInputModule_get_verticalAxis_mD87076DBD7FD641CD9B656CADD471F91F34FBD12 ();
// 0x00000B6B System.Void UnityEngine.EventSystems.OVRInputModule::set_verticalAxis(System.String)
extern void OVRInputModule_set_verticalAxis_mB2FCB47596FB7C0619E8E0610ADB36F7DF12C247 ();
// 0x00000B6C System.String UnityEngine.EventSystems.OVRInputModule::get_submitButton()
extern void OVRInputModule_get_submitButton_m61CBBC70F8073307BD57F5FE80AFBB5547FD5DB8 ();
// 0x00000B6D System.Void UnityEngine.EventSystems.OVRInputModule::set_submitButton(System.String)
extern void OVRInputModule_set_submitButton_m131D798CAB60658A6928A7C04F3EB49FC42C86B7 ();
// 0x00000B6E System.String UnityEngine.EventSystems.OVRInputModule::get_cancelButton()
extern void OVRInputModule_get_cancelButton_m00E433C926C1F5027BB6E419146E918E44EE69F4 ();
// 0x00000B6F System.Void UnityEngine.EventSystems.OVRInputModule::set_cancelButton(System.String)
extern void OVRInputModule_set_cancelButton_mF012F513E38C61C61A1E9C0D208DE30B0E6F553F ();
// 0x00000B70 System.Void UnityEngine.EventSystems.OVRInputModule::UpdateModule()
extern void OVRInputModule_UpdateModule_mC2E5F5B1078ECE12C0B54ADC121EE4C2A1F98CB0 ();
// 0x00000B71 System.Boolean UnityEngine.EventSystems.OVRInputModule::IsModuleSupported()
extern void OVRInputModule_IsModuleSupported_m94C3B5366F4E4A80573611AA4260E3EBBB45BD45 ();
// 0x00000B72 System.Boolean UnityEngine.EventSystems.OVRInputModule::ShouldActivateModule()
extern void OVRInputModule_ShouldActivateModule_m94C2531C19186F730C4456ADD9FFC6B2BB6E0669 ();
// 0x00000B73 System.Void UnityEngine.EventSystems.OVRInputModule::ActivateModule()
extern void OVRInputModule_ActivateModule_mBF35DED00716E753ABDB3D4C6A0C1D25749F67AC ();
// 0x00000B74 System.Void UnityEngine.EventSystems.OVRInputModule::DeactivateModule()
extern void OVRInputModule_DeactivateModule_m77677E554B8195C345D82058212C8B4204285117 ();
// 0x00000B75 System.Boolean UnityEngine.EventSystems.OVRInputModule::SendSubmitEventToSelectedObject()
extern void OVRInputModule_SendSubmitEventToSelectedObject_m07FE346295497118E84F557223CC884C87CA0A11 ();
// 0x00000B76 System.Boolean UnityEngine.EventSystems.OVRInputModule::AllowMoveEventProcessing(System.Single)
extern void OVRInputModule_AllowMoveEventProcessing_mD8FFB148B7046FD2374300468F924B13A30E5CD5 ();
// 0x00000B77 UnityEngine.Vector2 UnityEngine.EventSystems.OVRInputModule::GetRawMoveVector()
extern void OVRInputModule_GetRawMoveVector_m3B74C7A9CCCD20D63A057864B5697D70D0EBFD26 ();
// 0x00000B78 System.Boolean UnityEngine.EventSystems.OVRInputModule::SendMoveEventToSelectedObject()
extern void OVRInputModule_SendMoveEventToSelectedObject_mF97D71663C0DF70D4860B5850D6E752D75CB85B3 ();
// 0x00000B79 System.Boolean UnityEngine.EventSystems.OVRInputModule::SendUpdateEventToSelectedObject()
extern void OVRInputModule_SendUpdateEventToSelectedObject_mC7E30E8F4E8EE0D91F9706A58841230A18CE4AB9 ();
// 0x00000B7A System.Void UnityEngine.EventSystems.OVRInputModule::ProcessMousePress(UnityEngine.EventSystems.PointerInputModule_MouseButtonEventData)
extern void OVRInputModule_ProcessMousePress_mD1E6776C54B1819E50017DC5DD22B9ABA1EC8AA0 ();
// 0x00000B7B System.Void UnityEngine.EventSystems.OVRInputModule::ProcessMouseEvent(UnityEngine.EventSystems.PointerInputModule_MouseState)
extern void OVRInputModule_ProcessMouseEvent_mD4E1151D89D17366050D796AAD63F049757D2894 ();
// 0x00000B7C System.Void UnityEngine.EventSystems.OVRInputModule::Process()
extern void OVRInputModule_Process_mE7EE6263694349D6CC07E83232EFB1CB9A0F8D6C ();
// 0x00000B7D System.Boolean UnityEngine.EventSystems.OVRInputModule::UseMouse(System.Boolean,System.Boolean,UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_UseMouse_mDD1A75E6C695AF69CF8E81FD160A6BD5C0763952 ();
// 0x00000B7E System.Void UnityEngine.EventSystems.OVRInputModule::CopyFromTo(UnityEngine.EventSystems.OVRPointerEventData,UnityEngine.EventSystems.OVRPointerEventData)
extern void OVRInputModule_CopyFromTo_m02A4366C928D43C35AD9D32D1A4D3C9A0AFE5468 ();
// 0x00000B7F System.Void UnityEngine.EventSystems.OVRInputModule::CopyFromTo(UnityEngine.EventSystems.PointerEventData,UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_CopyFromTo_m066272CF94623FDBB29645155891EE55379D349B ();
// 0x00000B80 System.Boolean UnityEngine.EventSystems.OVRInputModule::GetPointerData(System.Int32,UnityEngine.EventSystems.OVRPointerEventData&,System.Boolean)
extern void OVRInputModule_GetPointerData_mEE6D70D8F790BA51F0F8DE0F3F2C864F71E40151 ();
// 0x00000B81 System.Void UnityEngine.EventSystems.OVRInputModule::ClearSelection()
extern void OVRInputModule_ClearSelection_mC8C4DE0E3312130453CE90A0F9B7D98D58CBAF69 ();
// 0x00000B82 UnityEngine.Vector3 UnityEngine.EventSystems.OVRInputModule::GetRectTransformNormal(UnityEngine.RectTransform)
extern void OVRInputModule_GetRectTransformNormal_m6BD54ABE582428F84A78D0623A05D66BE6D9FBCE ();
// 0x00000B83 UnityEngine.EventSystems.PointerInputModule_MouseState UnityEngine.EventSystems.OVRInputModule::GetGazePointerData()
extern void OVRInputModule_GetGazePointerData_m7CA4AAD1189FEB683E919275A946231C1DF43F52 ();
// 0x00000B84 UnityEngine.EventSystems.PointerInputModule_MouseState UnityEngine.EventSystems.OVRInputModule::GetCanvasPointerData()
extern void OVRInputModule_GetCanvasPointerData_mA4029B3BAF43567CB747A4F3D1C1841A33546781 ();
// 0x00000B85 System.Boolean UnityEngine.EventSystems.OVRInputModule::ShouldStartDrag(UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_ShouldStartDrag_m5001EA6CAD8A9FDE110EEA001269E807688E4F52 ();
// 0x00000B86 System.Boolean UnityEngine.EventSystems.OVRInputModule::IsPointerMoving(UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_IsPointerMoving_m68680D00AECCAE70E1AE7DEE78713964FC8EAD53 ();
// 0x00000B87 UnityEngine.Vector2 UnityEngine.EventSystems.OVRInputModule::SwipeAdjustedPosition(UnityEngine.Vector2,UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_SwipeAdjustedPosition_m11FE0B753E68C069FB760AEF86F7385EF43CD604 ();
// 0x00000B88 System.Void UnityEngine.EventSystems.OVRInputModule::ProcessDrag(UnityEngine.EventSystems.PointerEventData)
extern void OVRInputModule_ProcessDrag_m15EBC9C1D15BD508FE9F7B7CA679219B9E5AC982 ();
// 0x00000B89 UnityEngine.EventSystems.PointerEventData_FramePressState UnityEngine.EventSystems.OVRInputModule::GetGazeButtonState()
extern void OVRInputModule_GetGazeButtonState_mFACC396DC113D61EF7A52D1181139A0E594126C4 ();
// 0x00000B8A UnityEngine.Vector2 UnityEngine.EventSystems.OVRInputModule::GetExtraScrollDelta()
extern void OVRInputModule_GetExtraScrollDelta_m1B1188DCFDD6FC071A2B77343F533422828E44E8 ();
// 0x00000B8B System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster::.ctor()
extern void OVRPhysicsRaycaster__ctor_m6D24866F216A53C836AF6E89CBD4BC069DBD06DE ();
// 0x00000B8C UnityEngine.Camera UnityEngine.EventSystems.OVRPhysicsRaycaster::get_eventCamera()
extern void OVRPhysicsRaycaster_get_eventCamera_m10429F19C4CD4653A08282B5A6B95B4FE99C3A01 ();
// 0x00000B8D System.Int32 UnityEngine.EventSystems.OVRPhysicsRaycaster::get_depth()
extern void OVRPhysicsRaycaster_get_depth_m078AB8D83F45F3847CF455C366F806A478C306BA ();
// 0x00000B8E System.Int32 UnityEngine.EventSystems.OVRPhysicsRaycaster::get_sortOrderPriority()
extern void OVRPhysicsRaycaster_get_sortOrderPriority_m48859676B979E81A185003157119B4BEF756D6B5 ();
// 0x00000B8F System.Int32 UnityEngine.EventSystems.OVRPhysicsRaycaster::get_finalEventMask()
extern void OVRPhysicsRaycaster_get_finalEventMask_mA85434470FCF31893D3FD205EF565DDC5E60B322 ();
// 0x00000B90 UnityEngine.LayerMask UnityEngine.EventSystems.OVRPhysicsRaycaster::get_eventMask()
extern void OVRPhysicsRaycaster_get_eventMask_mC3E0C30E0256DB501588B20C7461FD494899677D ();
// 0x00000B91 System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster::set_eventMask(UnityEngine.LayerMask)
extern void OVRPhysicsRaycaster_set_eventMask_m20D3CC44CB011AB6B22C09219A6ED1ED4426373A ();
// 0x00000B92 System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void OVRPhysicsRaycaster_Raycast_m349B9024420DF9180F78AABA1621929683692E4A ();
// 0x00000B93 System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster::Spherecast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>,System.Single)
extern void OVRPhysicsRaycaster_Spherecast_mB57C8386983F9F54A03DA006C23BAB1C07E8B74D ();
// 0x00000B94 UnityEngine.Vector2 UnityEngine.EventSystems.OVRPhysicsRaycaster::GetScreenPos(UnityEngine.Vector3)
extern void OVRPhysicsRaycaster_GetScreenPos_mDEA5CE18B0A954A085F51805DF82A9BE8B539DF3 ();
// 0x00000B95 System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster_<>c::.cctor()
extern void U3CU3Ec__cctor_m1D7D4AC78BD05924F991603EAC64BF21EEC40C62 ();
// 0x00000B96 System.Void UnityEngine.EventSystems.OVRPhysicsRaycaster_<>c::.ctor()
extern void U3CU3Ec__ctor_m83E8EB044FB00FD30566EB933DA2EB943BABA5A4 ();
// 0x00000B97 System.Int32 UnityEngine.EventSystems.OVRPhysicsRaycaster_<>c::<Raycast>b__15_0(UnityEngine.RaycastHit,UnityEngine.RaycastHit)
extern void U3CU3Ec_U3CRaycastU3Eb__15_0_mB9CD5206BED51FE5C0D3F0006A8556D3FAF25C6C ();
// 0x00000B98 System.Int32 UnityEngine.EventSystems.OVRPhysicsRaycaster_<>c::<Spherecast>b__16_0(UnityEngine.RaycastHit,UnityEngine.RaycastHit)
extern void U3CU3Ec_U3CSpherecastU3Eb__16_0_mED9C459EAB5F11DF882EEAE2847937F35C41E3C1 ();
// 0x00000B99 System.Void UnityEngine.EventSystems.OVRPointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
extern void OVRPointerEventData__ctor_m09E6B3A742D7D082B62F8F7FBD1514B742B7987B ();
// 0x00000B9A System.String UnityEngine.EventSystems.OVRPointerEventData::ToString()
extern void OVRPointerEventData_ToString_m5B51F0FDF641AEB8F2D10887B0BC177814D7A55F ();
// 0x00000B9B System.Boolean UnityEngine.EventSystems.PointerEventDataExtension::IsVRPointer(UnityEngine.EventSystems.PointerEventData)
extern void PointerEventDataExtension_IsVRPointer_m1FCC09F50A4B5D1D17BD0A2C5760F7613FDFDAC1 ();
// 0x00000B9C UnityEngine.Ray UnityEngine.EventSystems.PointerEventDataExtension::GetRay(UnityEngine.EventSystems.PointerEventData)
extern void PointerEventDataExtension_GetRay_m154CB82C9019F0B2B0B566D45ED889D2826397EB ();
// 0x00000B9D UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventDataExtension::GetSwipeStart(UnityEngine.EventSystems.PointerEventData)
extern void PointerEventDataExtension_GetSwipeStart_m53B7B17EC0175D1F10A11D11288596BE83DB8104 ();
// 0x00000B9E System.Void UnityEngine.EventSystems.PointerEventDataExtension::SetSwipeStart(UnityEngine.EventSystems.PointerEventData,UnityEngine.Vector2)
extern void PointerEventDataExtension_SetSwipeStart_m21C63B5DA80D4F89E1392EA98A0D8ADF336E29E5 ();
static Il2CppMethodPointer s_methodPointers[2974] = 
{
	OVRBoundary_GetConfigured_m5093DB550C36074DD3AD8F26296090E000775DD3,
	OVRBoundary_TestNode_mF2DBD2BF1C57CD51F18471379A32D0C64DDB360E,
	OVRBoundary_TestPoint_m97C69C789CB97E33187402BD9FC5BC7158A50A90,
	OVRBoundary_GetGeometry_m0970B7164721759ECACB831864D9FB25400EB322,
	OVRBoundary_GetDimensions_m5475000A9A0C37FC173E00227895BD3448B70574,
	OVRBoundary_GetVisible_mFBFD780CF9C41971DDB3603178D5F8DE0DB5A28F,
	OVRBoundary_SetVisible_mFBB5852614DB6E8EC0BED7866812E0FF4280EA3E,
	OVRBoundary__ctor_m52212DC720BA6B980860AB1BA8DE279B87CD7C9C,
	OVRBoundary__cctor_m86793BCC0E922B1A776848B012CF4A85B72C37C5,
	OVRCameraRig_get_leftEyeCamera_mF543376FEB5FFDEA4E14DC98A4C8FDD145216E82,
	OVRCameraRig_get_rightEyeCamera_m2D5C77B7B47EF045A27AAC1830C75EFC8ED97289,
	OVRCameraRig_get_trackingSpace_m2DCEEAA2003B4A7EEAE4E57EEDD84E38C903F4AD,
	OVRCameraRig_set_trackingSpace_m53A921D03416919F6BC9C0995E06A09469965B62,
	OVRCameraRig_get_leftEyeAnchor_m7B92D88D81613FCC34826AC8EC4DE637AC193D35,
	OVRCameraRig_set_leftEyeAnchor_mDD27E24508D052E02247A832257FBFB45777B9CB,
	OVRCameraRig_get_centerEyeAnchor_mD0C4CBA5D31C5A2DD25CF752D1949008D731D5BD,
	OVRCameraRig_set_centerEyeAnchor_m7DB98233FC4BD3B6E0E37EE614E0481767720538,
	OVRCameraRig_get_rightEyeAnchor_m05D61CE77257E9466F8CE866C4F2EAC1ABDD3E67,
	OVRCameraRig_set_rightEyeAnchor_mC70B21CBEF41E0C94D340FD319B6A95A5ACDC7A4,
	OVRCameraRig_get_leftHandAnchor_m8D785A001836842F561158E6FB5963E3E0BD121F,
	OVRCameraRig_set_leftHandAnchor_m3632DF173E4481ABAF488A0F1FC99367694E8EA4,
	OVRCameraRig_get_rightHandAnchor_m83E255B33DEEB3ED0EC58B7875BA3E7FAF47D422,
	OVRCameraRig_set_rightHandAnchor_mB2527E44B61D609253C69E8A24F6CF73E045CC42,
	OVRCameraRig_get_leftControllerAnchor_mC4F9EC32A4D317F0A839EB8D3E5BB3F6DB02F963,
	OVRCameraRig_set_leftControllerAnchor_mF12782CABE8A1316C3F24FC9EF588AB17271E082,
	OVRCameraRig_get_rightControllerAnchor_mB384CF63CCC56DD8DCDAC016E2994D2B3BFF951A,
	OVRCameraRig_set_rightControllerAnchor_m87289B7FCE6DA0D5463B9A0F49D3FB31AB9B6578,
	OVRCameraRig_get_trackerAnchor_mE8338A133E43414996181AD53AAEE6B43C97C3C9,
	OVRCameraRig_set_trackerAnchor_m4F2626A2928469F4DFB8D440A1F0753B9D2E91E8,
	OVRCameraRig_add_UpdatedAnchors_m6CB042885064E4261C80102E4F4CED69683996DA,
	OVRCameraRig_remove_UpdatedAnchors_m362CDE8090BA05C20CB7EB79864AA714CCE0739A,
	OVRCameraRig_Awake_mAEDD9D5712DA958F52C4E26E09531E867589DEEA,
	OVRCameraRig_Start_mAB4168D5FD1BF3660B617668E41FC75F656C3004,
	OVRCameraRig_FixedUpdate_m0F489DAA9D79819A2A75A8843DC318EE1CA16184,
	OVRCameraRig_Update_m9443F276279709F7D19B05FA7F8DCE654EE2301D,
	OVRCameraRig_OnDestroy_m133CA579B879D836BCC4B2C571EE51B10D6C76D5,
	OVRCameraRig_UpdateAnchors_m62AE37D23B98B8C59A3A4743880590D4B2BC8278,
	OVRCameraRig_OnBeforeRenderCallback_m62EEA41B301C4132FA9C90854F771A013791BDCF,
	OVRCameraRig_RaiseUpdatedAnchorsEvent_m9899C80EE11A6A860ACBF8F9109C04D1A4BF7EDC,
	OVRCameraRig_EnsureGameObjectIntegrity_m700ED838ECF8C8DDA42D7599E48E21A54BA82288,
	OVRCameraRig_ConfigureAnchor_m9AC741FF628804302521B90B42FDD7D15710FBA4,
	OVRCameraRig_ComputeTrackReferenceMatrix_m055270F34B938C600D41F4A89B4E4E70E5BA0052,
	OVRCameraRig__ctor_m887B0D1F57D225421ADEA575196D0EB866BAC3F8,
	OVRExtensions_ToTrackingSpacePose_m153D7125F58A05667950419DA96E1350B1349B63,
	OVRExtensions_ToWorldSpacePose_m04CAC14BAF0063B6ED4A0558B6CE53C2C433258E,
	OVRExtensions_ToHeadSpacePose_mE3FD24B68A44B9BB18755866E7E5DEC4BCAC16C4,
	OVRExtensions_ToOVRPose_mBF3FD269D2B7F2CF4B3BE23DBED55C9E86912F7F,
	OVRExtensions_FromOVRPose_m92C4445075BC31C165ABF5A68E6D424F4895A228,
	OVRExtensions_ToOVRPose_mA375B53F3D98F98B8A6BC051A8DBA0C56B28A73B,
	OVRExtensions_ToFrustum_m13CD7E4E87936E1FD88909B993123DA9B01D6E9A,
	OVRExtensions_FromColorf_mBB2EE1F348A8FDEFEAB0B45653EEAE20AB1F9F14,
	OVRExtensions_ToColorf_m4A2DD316B299C097E01741D5F9FD0B578FF7E66B,
	OVRExtensions_FromVector3f_m5EDD11309A45158CD8CCC4F02B624E0182DBD395,
	OVRExtensions_FromFlippedZVector3f_m765F9B2F484044FF1E3BA46EB292B75C17F8D378,
	OVRExtensions_ToVector3f_m6DB81D473AA9273AA18819586D361F373F09E915,
	OVRExtensions_ToFlippedZVector3f_m4517FD69E3E60DF9240F2B0AC0ED084EA4B4F225,
	OVRExtensions_FromQuatf_mD94BD35BCCE893EAFD1D0CFF8C16AFF6845767D9,
	OVRExtensions_FromFlippedZQuatf_mAA94273849CBFD4DFCF09E1B6C5884E5157D4690,
	OVRExtensions_ToQuatf_mA1471855462C5B5207C1BD457EED922CE59B50A7,
	OVRExtensions_ToFlippedZQuatf_mA6C3102F8A6A066FE1806245D8821D922C3E8B04,
	OVRExtensions_ConvertToHMDMatrix34_m3692800E1981C3AA6D5115B3779BBAE878EF85DA,
	OVRNodeStateProperties_IsHmdPresent_mBE6D3E1BC46942BDD9DEAE42B420092316646C03,
	OVRNodeStateProperties_GetNodeStatePropertyVector3_m275A1D9D56B948E309095D307409975CEEEE392B,
	OVRNodeStateProperties_GetNodeStatePropertyQuaternion_mD41906383F8C16613AE40CDC5538F09B3FCA2BC6,
	OVRNodeStateProperties_ValidateProperty_m58915C2F14A6D7FAE6541C32690EEF2F0617BCB1,
	OVRNodeStateProperties_GetUnityXRNodeStateVector3_m959B7B20DC66F895427F3D8A56B241A26AD6A607,
	OVRNodeStateProperties_GetUnityXRNodeStateQuaternion_m5F7144D2142021097027725805C0E8FC41CDD764,
	OVRNodeStateProperties__cctor_mF94AF3E22B9F66AD7F63E6B7E523F3CEA490B41E,
	OVRPose_get_identity_m7CFC673402CCF7E4CA303E321A468DA21900CB69,
	OVRPose_Equals_m4D3B3401FEAD916EB7808A0025B82D60FDE80846_AdjustorThunk,
	OVRPose_GetHashCode_m0D6BA59B6F9B48B77A70C1F53D22A930CCE0562A_AdjustorThunk,
	OVRPose_op_Equality_mE9CB3DD2670E417FFACE22DF02D0E977A3A3607B,
	OVRPose_op_Inequality_m9374E2CC9622AF10E7F46025103A5E6BB44E678D,
	OVRPose_op_Multiply_m5C746FDDF924C8C99EA06A507CA71CFF083A177A,
	OVRPose_Inverse_mFD74A78C2BA1FFBBDA3C197C977E2EF58A298E31_AdjustorThunk,
	OVRPose_flipZ_m0EE7D1A58B067F5F35177B5B39ECEAA6338B825D_AdjustorThunk,
	OVRPose_ToPosef_m57D6351205C3F3D923704CF100D9C28F561BD803_AdjustorThunk,
	OVRNativeBuffer__ctor_m2F8C99712548549DFFF7EF18227396C7F1BFE777,
	OVRNativeBuffer_Finalize_m463ABCE67C71616FFFF30469D0323183FAF9D35A,
	OVRNativeBuffer_Reset_mE8D7F0FE2AE4168D21D5C4579EAC1502BDCA64A2,
	OVRNativeBuffer_GetCapacity_m5C14781B358E57A91BB1266CC0DFB9E2FA8EA3DB,
	OVRNativeBuffer_GetPointer_mDEA24E55784389B79EAE6C47C2ECF5482F4DA2DB,
	OVRNativeBuffer_Dispose_m61B6AA3B8278103D8886D16BCAA1B2D8E811C13A,
	OVRNativeBuffer_Dispose_m7FED58D05FB27B0AD86A2A7B645936C4D72A0379,
	OVRNativeBuffer_Reallocate_m3E2C71EAD35AE0CCC8075A33E034E1EA6B9829E2,
	OVRNativeBuffer_Release_m3E15FC99E583AA0C0A89AAD634D5D6EB205523DF,
	OVRDebugHeadController_Awake_mB897E6D3AD5288F255C06443413B96B7DA78570F,
	OVRDebugHeadController_Start_mF006A3C783A8F8912C8A193D192BDF67D5144717,
	OVRDebugHeadController_Update_m80F74DF8127DD87CAA1C982F9D37C9DD3DEC6083,
	OVRDebugHeadController__ctor_mA4D3C85F379DAACFCA4E163BAFDB71932A664608,
	OVRDisplay__ctor_mCACE1A08C3DCAFB90530A419A3567A87E4ADD9BC,
	OVRDisplay_Update_mCCE845DAD0599EBA173E85340804A48EC480E088,
	OVRDisplay_add_RecenteredPose_mB03AB370D52729662265B119DC4C9E5A867E9CBC,
	OVRDisplay_remove_RecenteredPose_m210FCB23F397C4FD305DF660B6102E52D8B1AC24,
	OVRDisplay_RecenterPose_m06194250A34CEDB16CFBB04F282B9395BD17FD5C,
	OVRDisplay_get_acceleration_m4F50142C9CC6538B341D9E115CD0DB155A00F34F,
	OVRDisplay_get_angularAcceleration_m9351B6C8A7F4CC9A0B05F868F87D7A7C821444EA,
	OVRDisplay_get_velocity_m6437597774100F4897A0C36933F404BDE6E46A8F,
	OVRDisplay_get_angularVelocity_m8ACC39BE5FAF40FBAAFF48F8DFA0281A3D2578A3,
	OVRDisplay_GetEyeRenderDesc_m9A9BF3B303AFBDEBA9FEC685CFE5724F0A023569,
	OVRDisplay_get_latency_mF97233777C03673CB38F71B24A9ABE168CBB1F6D,
	OVRDisplay_get_appFramerate_m156E2F1F6C83C8064BBD24326F93CF570D745B61,
	OVRDisplay_get_recommendedMSAALevel_m13E1993FD97E59C0A8B23C5AA5EC31205507446E,
	OVRDisplay_get_displayFrequenciesAvailable_m937677B39E10A1AD25D115ECF6BDCF9D04812CB0,
	OVRDisplay_get_displayFrequency_mACDD484A58B81BB7C3E96851613C89308717282B,
	OVRDisplay_set_displayFrequency_m036CE7305BF1DE7B8D8DC5FDC04AB172677910AD,
	OVRDisplay_UpdateTextures_mDA3E468B41D49311BD73E1BA8C9E46AB29383B28,
	OVRDisplay_ConfigureEyeDesc_mA4C0280800DFFFDA6F0283E543F69CAC5B514BC3,
	OVRHaptics__cctor_m9DF9E947A29AA3C612F1C869CC306A9F532FBCE1,
	OVRHaptics_Process_m8814A49BE847D61DDF750EBABF5E8E1EFE1DBF52,
	Config_get_SampleRateHz_mBF0C423BA5253D7BBD59966A4D014EAE8098CA77,
	Config_set_SampleRateHz_mB7B0370281303C33BE60EA0C25CBE246B87A0E98,
	Config_get_SampleSizeInBytes_m259D30FE07F12B434FC30C4B6AE5E3D47B0C0D76,
	Config_set_SampleSizeInBytes_m4F99AA7AD08E88E597A01E9F6921519F900941AB,
	Config_get_MinimumSafeSamplesQueued_mE066FA55BBAEEAC92868510A3C5063A29A0B79EF,
	Config_set_MinimumSafeSamplesQueued_m6CD4AFFB2FBECBCF1C0ABF207E1011B8715A13F5,
	Config_get_MinimumBufferSamplesCount_m452FBC24E773D6B2C06D1411B9DAECD64A9923BD,
	Config_set_MinimumBufferSamplesCount_m1FA61F2F606765835140B4D66E134082FD50FA97,
	Config_get_OptimalBufferSamplesCount_m031A6BFCE9A0B0B5995607CD96E71DEBE7D4D945,
	Config_set_OptimalBufferSamplesCount_m770AC6C6834244350C4F41CC4BC502A8992AB2C4,
	Config_get_MaximumBufferSamplesCount_m8C5184AD7714B7B7A88A8746A76D61931E661033,
	Config_set_MaximumBufferSamplesCount_m6345579FC5904BEDB3D82CD294FC3D3C5B13F895,
	Config__cctor_m6716E7312333D34F0299F1BDB711DBFC4641A575,
	Config_Load_mE5387F81A3AA215CD143175C48A9A3E5ACB60492,
	OVRHapticsChannel__ctor_m3F533C69B7EB546D1708470EE9C185224FC3A70D,
	OVRHapticsChannel_Preempt_mF1F53F22F5E05C5E20416880E02BD2FC043B93FC,
	OVRHapticsChannel_Queue_m89B54C06EF2FE831032BD6F15832BE945973F388,
	OVRHapticsChannel_Mix_m3B593B7720180C75E770E2A76220B9D0290AE812,
	OVRHapticsChannel_Clear_mD9A6B7CC4CDC7EE727E96795F3549BC87834189E,
	OVRHapticsOutput__ctor_m70F2C4446301C2177BD75A0BDEDE397841D76278,
	OVRHapticsOutput_Process_m06637C596160BA835A44B95E7D3FA2BECA1D6707,
	OVRHapticsOutput_Preempt_mBEFCABBFA1BAC7B6033D8C02B2485782D4748529,
	OVRHapticsOutput_Queue_mD5289CCC1E33BFD3AD504DA878944AE07F593C04,
	OVRHapticsOutput_Mix_m1EC3BA93FF7B23DDD1997C18121C726C527DBCBF,
	OVRHapticsOutput_Clear_m70F2303973A13B22AAEAC175A966B08374A2F86E,
	ClipPlaybackTracker_get_ReadCount_mF6B8F438BC83BAF8953928DF54C62D559D57902B,
	ClipPlaybackTracker_set_ReadCount_m6FFAA969B8F6ACD22FA89E9B7960A0089824B711,
	ClipPlaybackTracker_get_Clip_m853021C2B86660BBCDB6B4241D7103779938AAD3,
	ClipPlaybackTracker_set_Clip_m5E3D284F25187523412EA3C584AA2A9DAC21286D,
	ClipPlaybackTracker__ctor_mA5F90D85B508A90E0EAF6A634EFF4575BFD8E37D,
	OVRHapticsClip_get_Count_mFF27F17E532B73C234E0E5B479DBA050B1A28E36,
	OVRHapticsClip_set_Count_m0A00657439FFA2CB9F8E080054C1D071081A3D4B,
	OVRHapticsClip_get_Capacity_m738B8C59B18E4D7A583BE5F68AA435846CB3B53E,
	OVRHapticsClip_set_Capacity_m8C7FB4EE07E45E12A2098F5B4D50CE8602614586,
	OVRHapticsClip_get_Samples_mEE3E7A81631DBAAA1939D9FE85AC0228FE17657C,
	OVRHapticsClip_set_Samples_m883BA52B9C589BE4F27DF47974A8BA4247E0ED09,
	OVRHapticsClip__ctor_m462AA3C640AC299CBF2D604FC37E1213A2C2E9A0,
	OVRHapticsClip__ctor_mAC3B8AC3F953F8F23B9BB1243922C2792EB831D1,
	OVRHapticsClip__ctor_m785E6E5D522929D4626CD3F5F5CD53FABC4A8790,
	OVRHapticsClip__ctor_m445ECAF64EF748AFFA2392D1F126588ABA34D945,
	OVRHapticsClip__ctor_m18B12192C79C99888A54E11DC1BD9168359EC923,
	OVRHapticsClip_WriteSample_m5CC5D5DC610D8982A39AE91AA5C43FB707BA1C82,
	OVRHapticsClip_Reset_mD788010F132114CE77F2B9CEAF3BB5C36BBF09B4,
	OVRHapticsClip_InitializeFromAudioFloatTrack_mCF4CDE21704B856DEB2C7732E077B0C2E478F3DE,
	OVRHeadsetEmulator_Start_m7636D5E2478971107B676C2343C62F4AF01C8FF0,
	OVRHeadsetEmulator_Update_mF81AC116E9CC65B2146CB0349B6F9DE15141AB69,
	OVRHeadsetEmulator_IsEmulationActivated_m10420478F794D1C04409CD19070F19C7988352FB,
	OVRHeadsetEmulator_IsTweakingPitch_mF66F169119F6E04E5FE0E36CE5AFA67F4C50FCB5,
	OVRHeadsetEmulator__ctor_m101A2AF3FA27890F50AB1799392D7B0CCE3A5985,
	OVRInput_get_pluginSupportsActiveController_m2ACF44F093928F2A378164E3E907E1C807CFBB9A,
	OVRInput__cctor_m19971E781C5A234AE2025398292EE15A5D83ABE5,
	OVRInput_Update_m19E90D8F3325C01FC2E65F178C7AC0696736029C,
	OVRInput_FixedUpdate_mE9EF324767391BD7F2E7E64F1E585B8B6FC3AC95,
	OVRInput_GetControllerOrientationTracked_m36A93929E07F1BF4CB2BAEEB983D5331FAAECC31,
	OVRInput_GetControllerOrientationValid_m04AE481585DB1ED9AAEB9B3F0BFC2F413AD5C1DA,
	OVRInput_GetControllerPositionTracked_m2D57B21C46794F0E326C355A8DC2E9878D2D2E2B,
	OVRInput_GetControllerPositionValid_m5DC6A1D5FA46702CD062D030B6339B0C1B884441,
	OVRInput_GetLocalControllerPosition_mDCD6ACB9F6BF4FC874343A886E69A34AFC8E39F6,
	OVRInput_GetLocalControllerVelocity_m04F1E6B032A84AEB635A049E15D734585E12FF73,
	OVRInput_GetLocalControllerAcceleration_m2A23D042DD7FD454302B6993D006812FD59E6DE8,
	OVRInput_GetLocalControllerRotation_m0467B2CFE1611F6AA8F8E8D8250D27579F349FD8,
	OVRInput_GetLocalControllerAngularVelocity_m5C67927E58BC4A203574F4858F526D95F546B2B8,
	OVRInput_GetLocalControllerAngularAcceleration_mA8DFF34B1D387B426853A6302C42AFDE860FE5FA,
	OVRInput_GetDominantHand_mAF6886AEFE692A5FCB97D3DBA22AC9CD906FB7A4,
	OVRInput_Get_mD28FEA6BB4143B1A877406F48673CBB344023FFC,
	OVRInput_Get_mCB5EFCA078139909FD9A8B58966D87670A4F50E0,
	OVRInput_GetResolvedButton_mC37E9C1B1B13411707B455B83004D5CAE8E84E3C,
	OVRInput_GetDown_m805D5B9960D8101822F988DF59EEC907E05D913D,
	OVRInput_GetDown_mDC1D4A7988D6F2D3A48694928EDEA563DB0F19C4,
	OVRInput_GetResolvedButtonDown_mA3A5EF314CFCA312AAD089FEFCC8829835FFDFC0,
	OVRInput_GetUp_mFB3BB8EB3324A21E68898ED1B7F9324E52AAB05C,
	OVRInput_GetUp_mA446A2009E6DDB404DD45E8D4C3B7C8A1C2A397F,
	OVRInput_GetResolvedButtonUp_m90BF79401470B006BB7E8F256FB18C78B482D4CC,
	OVRInput_Get_m43CCF5BB1A52704C658046C5D2C19EE072352E66,
	OVRInput_Get_mC093F91A35FA7896E4E37E1FBA2EBACAA2F894AC,
	OVRInput_GetResolvedTouch_mA7BEE15AB735DC1FA8FC621F914D06B89DE8468B,
	OVRInput_GetDown_m3D608EF75EA3CD314A39A19A22D987BFCB597519,
	OVRInput_GetDown_m29719778A02F51D20DF391CA28EB8335D9BE23B1,
	OVRInput_GetResolvedTouchDown_m417EB539C97A7D510D3AD4E9D9EDB2B002F75905,
	OVRInput_GetUp_m697C489A998DD810D9EF7D39E0194E450FA5B320,
	OVRInput_GetUp_m1EC06070E9878F4E9CADB310A25E41350C2747B2,
	OVRInput_GetResolvedTouchUp_m897FF6CF2A3B3EE8F7451B69A028E3D41FF0A806,
	OVRInput_Get_m413113B33BF8B78355A66B1727DA852CF752CD97,
	OVRInput_Get_mBA882DF627E4D7E25A82B7A721ECC62F259755DB,
	OVRInput_GetResolvedNearTouch_mCE99ED1E354E6A21AC4CDB07370C958B6AB55663,
	OVRInput_GetDown_m36BC5C370D1BBD98FEA0FDD6579505DB42772BE0,
	OVRInput_GetDown_m376282F1966F78C46E15612F9250975FA7BB0509,
	OVRInput_GetResolvedNearTouchDown_m9651F25FFB006FA4BEF9227A57DEDEAAC2EA55A9,
	OVRInput_GetUp_m942CDC17B0164FD8EDF5CBB2AF644E1E8897AC1D,
	OVRInput_GetUp_mD490A8A3593F63204C87FB807703FA804099B445,
	OVRInput_GetResolvedNearTouchUp_m67F1F00BF2EC34CE666BD793DC9BBB69F4B21969,
	OVRInput_Get_m639D392EA29D4E11C99642F9E76C6DF91203ECD6,
	OVRInput_Get_m194F10CB40430380DBA8EA7272846A8C4E91ABB7,
	OVRInput_GetResolvedAxis1D_m585EDDA8BA7086FBDC28F28EE1F0466CE1EB3429,
	OVRInput_Get_m6921274099CAB813D71B3D7D6E1C7AF414A55FCC,
	OVRInput_Get_mBCE80A6B8532C4AC77BA3FBE2C4CDB6024031C96,
	OVRInput_GetResolvedAxis2D_mE6EA9C261D90F1666384BCB29C1F93E2A129ED97,
	OVRInput_GetConnectedControllers_mC0DF5B3EA8741EB53BED9F0063D7A3C91864CA09,
	OVRInput_IsControllerConnected_m5926F35AD2016332730E587700D23E81A0302456,
	OVRInput_GetActiveController_m17A3D1B2B0A46E835D951A7621D04EE4442F67F9,
	OVRInput_StartVibration_m25C2642BE6B502BD173A425D1D3F5B4B0FDD61A3,
	OVRInput_SetOpenVRLocalPose_mA90EAE89BE10A18F8F32C09327A6F72CF26277A0,
	OVRInput_GetOpenVRStringProperty_m44EA467A648A64B3418B0B11FDD634D50E4A0EEB,
	OVRInput_UpdateXRControllerNodeIds_m17AD21351586EAE8ED47EE28F720DD3A6C4B131B,
	OVRInput_UpdateXRControllerHaptics_m7A9B2471150B3F16ABE193022BE2D41E522B10BF,
	OVRInput_InitHapticInfo_m10DCFBD4735EB9DEFCC262E44A5AA90CB42AE188,
	OVRInput_PlayHapticImpulse_mE640A5FDEE1025CA8EEBEC53B9AFAD4651290A7D,
	OVRInput_IsValidOpenVRDevice_mF37CE426C4EFA9CE96B4A4215FD5E8BA7A62B23D,
	OVRInput_SetControllerVibration_m6734A248BD2EFC5AFBA4C3A177EC9B6EC3256D0F,
	OVRInput_RecenterController_m4A36B6A7278DBFFFF918AC004819272E26B6019D,
	OVRInput_GetControllerWasRecentered_m692BB0BF3EB419831CB53D7A5453617797132C61,
	OVRInput_GetControllerRecenterCount_mD4E9A21DC59032AD0C8113BDBF195BACD49773C5,
	OVRInput_GetControllerBatteryPercentRemaining_m6FBF249F51F349481DF9F1074B372CBF6F9B34D6,
	OVRInput_CalculateAbsMax_mF55A311F254D4D3889E55F3B1130407453B0E6A1,
	OVRInput_CalculateAbsMax_mDD5FA1CDB516CD0A6044BAD5B469D95FAD6267C3,
	OVRInput_CalculateDeadzone_m3EC8CDDB2E92CF1F1C8CCBADD79F4B6A6A54DF6E,
	OVRInput_CalculateDeadzone_m85949E1D42104EDA26E88530689F8B81CB0FA87E,
	OVRInput_ShouldResolveController_m2B8792E11DFE911D0ED0EF4C60BC27767364C9AA,
	HapticInfo__ctor_m2AC0E4C71FE1DE5975493EF29DCE3C0A3F41EC8F,
	OVRControllerBase__ctor_mC61EA8CE2DD4906389E3F467AF5FE4DFEE0BC48D,
	OVRControllerBase_Update_m27794216EF2A070CA5AF2C4C24AE9D0FD55FD74B,
	OVRControllerBase_GetOpenVRControllerState_m8953A760790660F1FC3F42CF8B17B8714D42D9D6,
	OVRControllerBase_SetControllerVibration_mDCE94CF986E07423019EB4EE8D0B6C90FFCCA8F4,
	OVRControllerBase_RecenterController_mC32586773B91FD247CAB858014E6753589BADA68,
	OVRControllerBase_WasRecentered_m655756A76C186374606C68B865F59667B5C1D7FB,
	OVRControllerBase_GetRecenterCount_m75F810E920FA08F533D0CD1E1CB1A932A54C7798,
	OVRControllerBase_GetBatteryPercentRemaining_mED881266E425128D8E3DB24006BC516F0363E2CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OVRControllerBase_ResolveToRawMask_mC1D679AFFD98340F053FDAA0568B49022CE49EDA,
	OVRControllerBase_ResolveToRawMask_m28A50F0D28D6D52C2DABC5BE41F4E1EB062B8CF8,
	OVRControllerBase_ResolveToRawMask_m56C9D177DC239D712089F901DA3E65DAB89AF6F4,
	OVRControllerBase_ResolveToRawMask_m8F5D2916E82BE22D3234101127E8919EE1C08449,
	OVRControllerBase_ResolveToRawMask_m3466F65C406A1CDCB0E4AEEB874CA29C3D147098,
	VirtualButtonMap_ToRawMask_m73472FEC045C735D83E7B42366A9F88C609600F8,
	VirtualButtonMap__ctor_m91D2F6BBF0841690403F0BC66444AAC132B34A09,
	VirtualTouchMap_ToRawMask_m094CB0CBEC27C7FD7FBC3C9E47FD719682C85F23,
	VirtualTouchMap__ctor_mF23DBA22DA140E2A9BA4F9AB481F8135426061F9,
	VirtualNearTouchMap_ToRawMask_m2B92568DC4E3CB92BD18F87F02078910C2BFB8E2,
	VirtualNearTouchMap__ctor_m5D70FF2C55B4C3773C3E6305BE7B7BEBDA800059,
	VirtualAxis1DMap_ToRawMask_mF1B4DD9DC1D6FFC2B562596B1F96069A79B58FB5,
	VirtualAxis1DMap__ctor_mC2E71412548C06875EEC0BC2AD0431908FAF7EA7,
	VirtualAxis2DMap_ToRawMask_mB7459D93F899F93E870E4AD215EC04407002F295,
	VirtualAxis2DMap__ctor_mECF285D0A2576F6642A78D9654ED0DBE656B1A8E,
	OVRControllerTouch__ctor_mA43758C56797259BCE9F4D7C529CEFD8F52078ED,
	OVRControllerTouch_ConfigureButtonMap_mD1B10FF42D46FE817E933D0BF4691687939937A7,
	OVRControllerTouch_ConfigureTouchMap_mFAB4DDE8D41C37EC8E7D24091833DA5FF68C3B0E,
	OVRControllerTouch_ConfigureNearTouchMap_m5AD6C9CDA7616942F0E24493325C6E83DCCB89DE,
	OVRControllerTouch_ConfigureAxis1DMap_m4553C500FA0236D5CF83C877B833E3C67117C9AF,
	OVRControllerTouch_ConfigureAxis2DMap_m93FF7E484219B989494C36984EC2D39CE9EFD82B,
	OVRControllerTouch_WasRecentered_mAC524F0A26C49E713FFC8D8E13B3EBE02370EFC3,
	OVRControllerTouch_GetRecenterCount_m668DCE21A36FF447AB5D722057FCFA4F5605EC0B,
	OVRControllerTouch_GetBatteryPercentRemaining_mAD022892B71FE08B2C16EF59985F67EA349B54DC,
	OVRControllerLTouch__ctor_m12AD4A8D7B38554CFBA3077D778F59019E7DAC74,
	OVRControllerLTouch_ConfigureButtonMap_m6FF799D1CBBA6F1853244678CD34992919AE853B,
	OVRControllerLTouch_ConfigureTouchMap_mCC0AD41347F6B06E8DC52B60BD37404EAE3FACAF,
	OVRControllerLTouch_ConfigureNearTouchMap_m0E6486776C6DF677B4315BECE9B6FEC331BEA877,
	OVRControllerLTouch_ConfigureAxis1DMap_mAAAC3E079E1AFC08C7663D22675422C453C74366,
	OVRControllerLTouch_ConfigureAxis2DMap_m09CAFBC7E6319F219ECCE07B18CDD56F259DECCA,
	OVRControllerLTouch_WasRecentered_mA4C03A56F099EDBD98115FB60C006608C8C1AF42,
	OVRControllerLTouch_GetRecenterCount_m39D20BE9234BD8445017409D77B2C8DDC67A7C3E,
	OVRControllerLTouch_GetBatteryPercentRemaining_mD3D63FB2AF59D15C73D17738C389140E9253D69E,
	OVRControllerRTouch__ctor_mF66E59D83609DEA496BA3606D91F26202E259FC6,
	OVRControllerRTouch_ConfigureButtonMap_m7958BB0DE880F2B2E3098F0DA168987AC931A274,
	OVRControllerRTouch_ConfigureTouchMap_m88249F7C4C3593162BB91859B527164112FCCF71,
	OVRControllerRTouch_ConfigureNearTouchMap_m9B1420FD72420D684B4C2FFB4EF15232FA667C84,
	OVRControllerRTouch_ConfigureAxis1DMap_m16CEFBE7803D7B38D6BC3D2A81D6FBDD0019074B,
	OVRControllerRTouch_ConfigureAxis2DMap_mDBCB76A35337C03FDFB1CFD3D971C8886F33CC02,
	OVRControllerRTouch_WasRecentered_m5FFD35A0180676838AB4C9CE812AC5B938DC5241,
	OVRControllerRTouch_GetRecenterCount_mC09383A499D96705E2BF233164F483A3A19B4A09,
	OVRControllerRTouch_GetBatteryPercentRemaining_mEBA855E4F847F39EC91C6D5B1AC2D8F485DCDAF8,
	OVRControllerRemote__ctor_m109415A5F44DC56832825616A38EB3738FC6D182,
	OVRControllerRemote_ConfigureButtonMap_m6F89457937147957363A4E7AC739035E5436D4F9,
	OVRControllerRemote_ConfigureTouchMap_m7F2AFF45A027F262A3970CB06D41D39E0181FB87,
	OVRControllerRemote_ConfigureNearTouchMap_m83CA46F9271530342618A6EF171B3374DDFF4E8B,
	OVRControllerRemote_ConfigureAxis1DMap_m8FCB212D322378698DD5D4D1DCEFCF19F64538FE,
	OVRControllerRemote_ConfigureAxis2DMap_mF36EB1D487A60F43B94EF80A849F3E4D6F9BE82D,
	OVRControllerGamepadPC__ctor_m0692683EFA29CF07FE60D0EB444C4BBC3EF4B306,
	OVRControllerGamepadPC_ConfigureButtonMap_mDD4BA7B9D6B9A1C68E45087DD300050DEB2251EE,
	OVRControllerGamepadPC_ConfigureTouchMap_m3A0F0D78BB82051F629330F612738A570B1C9773,
	OVRControllerGamepadPC_ConfigureNearTouchMap_mB18F06ACD74B6D09C37B14E8F21B80A1E076EFCC,
	OVRControllerGamepadPC_ConfigureAxis1DMap_m817FFC0996AFDCFACE326BF0EA7BC20568CF373F,
	OVRControllerGamepadPC_ConfigureAxis2DMap_m427D1273FFDFED3F46147BAE1809C0E8891627A5,
	OVRControllerGamepadMac__ctor_m54AB63055C95C03C97B12333D720ED1F84D40097,
	OVRControllerGamepadMac_Finalize_m36D11AC862B929B5FE372A13A2A291AEA7636EDA,
	OVRControllerGamepadMac_Update_mDD21EF016E4A1B57DE14173982338CCF94D929C6,
	OVRControllerGamepadMac_ConfigureButtonMap_mCE03DC1A1ECCFD186868B2AEE6AADEDED3EFFC1E,
	OVRControllerGamepadMac_ConfigureTouchMap_m7EC7EA82109A070C03711FE8D69092A7F6293FA8,
	OVRControllerGamepadMac_ConfigureNearTouchMap_mEC77E2F0AFA6B1679736D3E727D1C4A1A016DD8F,
	OVRControllerGamepadMac_ConfigureAxis1DMap_m21151EF89394B9DC6907CEC38321D3B7E3518D70,
	OVRControllerGamepadMac_ConfigureAxis2DMap_mCB1452FC11D0D7E89A7D7BCCAFB834AD8731A1AC,
	OVRControllerGamepadMac_SetControllerVibration_mD3626EBE9E415225B1F1B4D06B07F9CA927AA7DF,
	OVRControllerGamepadMac_OVR_GamepadController_Initialize_mC3F87E73A0875F44308E1AF143EF6093C7A05718,
	OVRControllerGamepadMac_OVR_GamepadController_Destroy_m0F9878AD54374802864F9DB7E57359CE5AB77963,
	OVRControllerGamepadMac_OVR_GamepadController_Update_mC939AE53F198C060AE7B343D7D74C2A990F0C176,
	OVRControllerGamepadMac_OVR_GamepadController_GetAxis_mB2D78FB3966B2DDDB99831FAC76B47D23FA5D996,
	OVRControllerGamepadMac_OVR_GamepadController_GetButton_m3C07A110FF9C92A693C351F9B94EA891AA007565,
	OVRControllerGamepadMac_OVR_GamepadController_SetVibration_m30AB24FD661F15E84E79643FD14A5F507B940590,
	OVRControllerGamepadAndroid__ctor_m17602A15B0B4544FF9A2DCE363E39CD369225DC6,
	OVRControllerGamepadAndroid_ConfigureButtonMap_mE64446D3642873D041A56425B091484B85752308,
	OVRControllerGamepadAndroid_ConfigureTouchMap_m800F9772FF3BFA25FEBC4C5F2733F0DADBD13439,
	OVRControllerGamepadAndroid_ConfigureNearTouchMap_m5F41360FF1808E055E98ABC5EF878F2113682F2B,
	OVRControllerGamepadAndroid_ConfigureAxis1DMap_m8ABE1969BC577DF9EA362EABB42FFBE62E6B7EAA,
	OVRControllerGamepadAndroid_ConfigureAxis2DMap_mBF4F5863E1DBF9637B6CAE0520B255049C6F3A3C,
	OVRControllerTouchpad__ctor_m81F247874C230DDC51359A0FD54042D3332E8562,
	OVRControllerTouchpad_Update_m091CC1C96998A2C17A77A7FC1BF70B9BEED14A44,
	OVRControllerTouchpad_ConfigureButtonMap_mA7CA49570E1A11F53449720A16FEAA229ED31F9E,
	OVRControllerTouchpad_ConfigureTouchMap_mFF75A52F9BCBB7B23DD59A8212C1DB00CCF94101,
	OVRControllerTouchpad_ConfigureNearTouchMap_m585F637EA858CF519EDF840083BF3504A6F0A430,
	OVRControllerTouchpad_ConfigureAxis1DMap_mEC0E4E2F63B992D574DAFBC7D66FD0BD59EE5EFA,
	OVRControllerTouchpad_ConfigureAxis2DMap_m15284CBA6876042FEE66FC7455556E554C4C090F,
	OVRControllerLTrackedRemote__ctor_m6799E35FD41D60E71FEA8139DA8047F793739B69,
	OVRControllerLTrackedRemote_ConfigureButtonMap_m257EDE02B7F8A44487C764D91AE90E853DE62BEB,
	OVRControllerLTrackedRemote_ConfigureTouchMap_mB19AB374891403DCD56BD58E25F3FA00AF35A3F8,
	OVRControllerLTrackedRemote_ConfigureNearTouchMap_mFDCE0EEF2B757E91CEC0AAC4103CCA3FD0B94D87,
	OVRControllerLTrackedRemote_ConfigureAxis1DMap_m5324359AD385BEA973DA9A2B24B34D0CD0E0AA2B,
	OVRControllerLTrackedRemote_ConfigureAxis2DMap_m495F7DC43643D14CB753E741A8A5A8D3BB92BA49,
	OVRControllerLTrackedRemote_Update_m20B0A6221715252920A412F0BFEBBBFD120FA532,
	OVRControllerLTrackedRemote_WasRecentered_mE4FD973E87306C52219AFA4DF28AC2114D8F8400,
	OVRControllerLTrackedRemote_GetRecenterCount_mF45BE77D31321F1AF465B22F351BB7E5F83F2571,
	OVRControllerLTrackedRemote_GetBatteryPercentRemaining_m37CD67AE9F6A6C3F75A37577D3B5782FE56A0FBF,
	OVRControllerRTrackedRemote__ctor_m321A13B4807FBA5D9FCD154D65C48B6768834E74,
	OVRControllerRTrackedRemote_ConfigureButtonMap_mACE34F2CECBFE6EE8BA066836CD886CCF5C974C0,
	OVRControllerRTrackedRemote_ConfigureTouchMap_m1C0F5C9524045CAEDB6AEAAA2E35D335AA13FEDD,
	OVRControllerRTrackedRemote_ConfigureNearTouchMap_m50A0391A52A19E224DCBCE37F6F22BB5B462BDE5,
	OVRControllerRTrackedRemote_ConfigureAxis1DMap_m093ED912B993FB02054A47102B320CF8406A7774,
	OVRControllerRTrackedRemote_ConfigureAxis2DMap_m9FE0734E7DC5AD73AEB7E69BC84A41E9AE32E651,
	OVRControllerRTrackedRemote_Update_m01D3449B0C8F94198F0105A0CFC0DD70591D0A66,
	OVRControllerRTrackedRemote_WasRecentered_mF27A63C7718937A0721AC5D263C4EC53BEF7FFB4,
	OVRControllerRTrackedRemote_GetRecenterCount_m9CDB47A0FE5ADD7D4198F72053E79A3A69B44D53,
	OVRControllerRTrackedRemote_GetBatteryPercentRemaining_m3DDCA1E8FEE31DDB4CCD69976CB84AE663AF352A,
	OVRLayerAttribute__ctor_m3B0F7D901CBA30D669078355C44AB675F57A6AFE,
	OVRManager_get_instance_mDECB23FAADD39F7A1E4FFCE849ADEC314E61BA93,
	OVRManager_set_instance_mFE7AAB4BD2630512DEBDC82F10B78DD483284262,
	OVRManager_get_display_mDD43E2C369D8F7B568202CEF99F1D365EC253B8E,
	OVRManager_set_display_m8BD00BBBE13C06B3BBFA7219EE670AFD855D3E77,
	OVRManager_get_tracker_m3896940AE3394CD4A2869B90DE851C4974F09643,
	OVRManager_set_tracker_m94ABDA7879D536396569B21071DB293B4C9187FE,
	OVRManager_get_boundary_m7808AF1F1EC2FE7C2CDA325CE00555F1B98DF7FF,
	OVRManager_set_boundary_mC8834D268079F568327FF68DA9EB44D0F694DA58,
	OVRManager_get_profile_m92D6DF3F1A6191783CD2174FA0B568DD72F8E395,
	OVRManager_add_HMDAcquired_mF6769C2AFFAE59F72E690648081360B00F9A1844,
	OVRManager_remove_HMDAcquired_m72D3CC00D042F4431387B015EF7D0ECCA443D61B,
	OVRManager_add_HMDLost_m7869E7000110EDFEAE4D58B5D60886ED0B6599F1,
	OVRManager_remove_HMDLost_m0D91AD36321659BF043E471F5A9D1724C38B3097,
	OVRManager_add_HMDMounted_m4D96DBA40FD18163B232C9C28736B5B9B4305124,
	OVRManager_remove_HMDMounted_m22DCAE74E33CE3F7AD77229E173E31E4D5B96A0A,
	OVRManager_add_HMDUnmounted_m97DA139E73CDFBF98F471E9A3BB29C236539862F,
	OVRManager_remove_HMDUnmounted_mD350C82D1E8AE6CFB5DDE3AB0ACBA5B9E3BEF58A,
	OVRManager_add_VrFocusAcquired_m22FE409E80970BF9464FF62A6F908EF38D2D97CA,
	OVRManager_remove_VrFocusAcquired_mDDB8E1B64988FB2B9EED920CEB69776B815F868C,
	OVRManager_add_VrFocusLost_m60B6FF0192DEBE5702EF8B80A3F104E06BB30838,
	OVRManager_remove_VrFocusLost_m46470A33D7402EA0996B4A1D323D078D16762619,
	OVRManager_add_InputFocusAcquired_m28DD7BB67EF01E87D05EDFCF51E8EB447602FC37,
	OVRManager_remove_InputFocusAcquired_m3998B532375D6056078B886D2CC6B0EC69860814,
	OVRManager_add_InputFocusLost_m3115553C211F283A719531C2BC01BF036AB18317,
	OVRManager_remove_InputFocusLost_mE7702024ACDBEE733CD20C94AC8419A6FFAE1A62,
	OVRManager_add_AudioOutChanged_mB4939854D1F78237E10D1B9F4494BE55AAEA30C2,
	OVRManager_remove_AudioOutChanged_m7E706D6C48604E15C8CA5AB9ABCD40148404E6B4,
	OVRManager_add_AudioInChanged_m23DECA7E41D80D7E1C59BB29B849D362861E6B10,
	OVRManager_remove_AudioInChanged_mF845BF96DC2F2FE727F25941AD2480C43C1DCDB5,
	OVRManager_add_TrackingAcquired_mB91D4E78240C0133B517913F3EF44D2E54AF30EF,
	OVRManager_remove_TrackingAcquired_m91B3D1D1C40B1032E65EA50134594DF9EF150E78,
	OVRManager_add_TrackingLost_m01FFB6EE74C0BCCB40D7C7E74019B5DBDA35E9DF,
	OVRManager_remove_TrackingLost_m1FEE5926B1C2E2FA17E6966AE163A01380C7C193,
	OVRManager_add_HSWDismissed_mAB7AA9F3FED0868A9E16D1CA6E4844F875EA4FFD,
	OVRManager_remove_HSWDismissed_m2D2A9D5E557724CE47AC99DC56346D517748EAF2,
	OVRManager_get_isHmdPresent_mED41DBAFDB605514FC4650871BA080B5B0193D13,
	OVRManager_set_isHmdPresent_mE516003BEBC7BCDCB19AA35781A3AE9459D05266,
	OVRManager_get_audioOutId_mAEA582D051D590286473DAE68A0993EC02F98729,
	OVRManager_get_audioInId_m298B5AA600BF2510EBE8C27DE1C67A6BE0C74919,
	OVRManager_get_hasVrFocus_mACE917EA641306B20F9E6CB0B95BDC932B9EBEE1,
	OVRManager_set_hasVrFocus_mD4917F853261973340D917194E37DFAA3C98A033,
	OVRManager_get_hasInputFocus_mD4AF719FC6E6273E9464EA55C48AE56A55FF6028,
	OVRManager_get_chromatic_mF6523ED06FAE723D803CA00E75EBFD5AF3C20F96,
	OVRManager_set_chromatic_mD2FB9C7CB7DD39C4D3C6C4C697090F015513A332,
	OVRManager_get_monoscopic_mC5104AE9A70720EB46EA6017D098219FB4A5B835,
	OVRManager_set_monoscopic_m28EE0C6D9A9E79F331D3CE278F64D6F361D1E2C8,
	OVRManager_IsAdaptiveResSupportedByEngine_m9154A00065BA5716187BCC96FE78543678BF9D18,
	OVRManager_get_headPoseRelativeOffsetRotation_m030915C4E7C9C95B6E030DDBD876B60D232DC580,
	OVRManager_set_headPoseRelativeOffsetRotation_m420B4FF180D75BD70514BAFBCAAD0334CD8EA248,
	OVRManager_get_headPoseRelativeOffsetTranslation_m7B311AD1595AF5D574B585B91FB459F49EBEBC43,
	OVRManager_set_headPoseRelativeOffsetTranslation_m75ECB1CAF5C5980E092EA17CA2F4F72A788E7C9D,
	OVRManager_get_vsyncCount_m31A4814E6EAC0C15F2287D0ABF2229298230DF97,
	OVRManager_set_vsyncCount_m8E37770069EAAF3106FF642186FBFDB29F90BC5A,
	OVRManager_get_batteryLevel_mF3989FC197C268638E22D110AC72262BA32E88D5,
	OVRManager_get_batteryTemperature_mC2E9309E8E9E443BF0BF25D79F14710AEB5959F5,
	OVRManager_get_batteryStatus_mBE7DFEA8CBB4FAC2DB9D8807D872D2D8FC6EDF11,
	OVRManager_get_volumeLevel_mB58406893F6A03554F8313FDF605B807B1E533CD,
	OVRManager_get_cpuLevel_mC343F1940F40DB94AB04B39B98CA555CE2F4646A,
	OVRManager_set_cpuLevel_mF51AB036A5819FBA7E066D91FA7D1A8111B7B909,
	OVRManager_get_gpuLevel_m5E800FC829B57595312287F4A384FA34E039FFDC,
	OVRManager_set_gpuLevel_mE60D72BA91CA5A9379C4E043C19EBEEB2A52C452,
	OVRManager_get_isPowerSavingActive_m8910EC0EFB83E757EAB0A9650B0ECFB55ECD24CF,
	OVRManager_get_eyeTextureFormat_m99F7FA0CDC40B67D150551AC8C0F41820EECD1FD,
	OVRManager_set_eyeTextureFormat_mF39830F5EF3488295017F2F4F44F1B9E41E412FF,
	OVRManager_get_fixedFoveatedRenderingSupported_m80D1FC14234EFE1483B05BA61F48D149AD94039E,
	OVRManager_get_fixedFoveatedRenderingLevel_mD15DAFC6B06C7815601CC6F5F42A56BE39BAEA2E,
	OVRManager_set_fixedFoveatedRenderingLevel_m8868C801F750485D0C094C525E0322A89E12FFC5,
	OVRManager_get_tiledMultiResSupported_m7B32DD7DDCFD3CFE47B569C277DB4D543567699B,
	OVRManager_get_tiledMultiResLevel_mBA4B86D4E8770FEF2910365F884320C5C66F2694,
	OVRManager_set_tiledMultiResLevel_m0BF85177FF074F90F5141BD898D29CDA301C364A,
	OVRManager_get_gpuUtilSupported_m2409BFA06A721D84388FFD4208817979EE9872E2,
	OVRManager_get_gpuUtilLevel_m204E5BE39DAB99A32926D404CCCB65235BE6717E,
	OVRManager_SetColorScaleAndOffset_m835700F2546573A077E78EA3EDF1531E14E5D128,
	OVRManager_SetOpenVRLocalPose_m306D491740A47D48E7E94826A2B3D857A3F29FDB,
	OVRManager_GetOpenVRControllerOffset_mDAFB7CC00B3D29E8C680A00843FF381AEDB350D9,
	OVRManager_get_trackingOriginType_m736B457ADF8B4519491CDF707714B5918F6ACAFA,
	OVRManager_set_trackingOriginType_mDE10B31D46CEAB54FBA805B25AFE623C7A4FF353,
	OVRManager_get_reorientHMDOnControllerRecenter_mFEE2A3CB5817F562EAF892C6917F1F4E16026AF2,
	OVRManager_set_reorientHMDOnControllerRecenter_mC414A44203789A3C87FACF0DAB596D546A224527,
	OVRManager_get_isSupportedPlatform_m903DE769917CEC68A814D7E09DCAAD5336816113,
	OVRManager_set_isSupportedPlatform_m3ED3252A7F969DA1B704B8AED972AF57C492C612,
	OVRManager_get_isUserPresent_m0468018F27C6CB32BC9F4B2FF66B7464C36D786A,
	OVRManager_set_isUserPresent_m47900102D89207BC4EB1C000B31EE4F4FB3304A6,
	OVRManager_get_utilitiesVersion_m879B9C86EF390299CE13DED505A2E6C1565BB7F8,
	OVRManager_get_pluginVersion_m460B68FC87725CF738A74B2CC69D599E939C930C,
	OVRManager_get_sdkVersion_m9FE7106BB01BD214496EA8355FD4B8E58767ADC9,
	OVRManager_IsUnityAlphaOrBetaVersion_m078EC8D73BB5EA84DFD93819756C1473567C81A0,
	OVRManager_InitOVRManager_mCAE32BB448494B6D421C54BCA4FE0A64564325C5,
	OVRManager_Awake_mE2382FF8F17CABC0C0ADEBE62F6FF2594E97DF3B,
	OVRManager_SetCurrentXRDevice_m22D386A05E3864FF3A2ADBE47CAA1D3804B43CD9,
	OVRManager_Initialize_m3ED5502F37E9E16E3B76B06DACCB7F6DDB1AC180,
	OVRManager_Update_m8C2017A3E668900D35E6AE980802303675CBA89A,
	OVRManager_FindMainCamera_m3E061562D17077AC417AD2B56369D04BB2254FA1,
	OVRManager_OnDisable_mF9AC11370A4563BA4BA79A1117BFF1CCDD87944F,
	OVRManager_LateUpdate_mAFFEBA018DD17EB5D8FA37F1DAAC0A7B80AE6B03,
	OVRManager_FixedUpdate_m33A743F2F9F71C2E325BEDFF6F403DF88EBC90BB,
	OVRManager_OnDestroy_m6739E0927C2C01CD2A5575587D23C1FB6E188360,
	OVRManager_OnApplicationPause_mCCF67F674BB1B9CC2B571D9EA42CE736611B2D91,
	OVRManager_OnApplicationFocus_m2412B9844FC8C4FC2CFA836C4198CC3E4AF8EFBF,
	OVRManager_OnApplicationQuit_mCD5F15E639C4FF858AC828151BDD58A648115402,
	OVRManager_ReturnToLauncher_m110AE6ED933100FD138161C749A00E46ECD71BD4,
	OVRManager_PlatformUIConfirmQuit_mBAD548D77F667E55639F7EF3F644A5D2565A9A0C,
	OVRManager__ctor_mF1D76275CB1C427FA837B8C0D00B14E57CD4607F,
	OVRManager__cctor_m5696974B5227590002A4A2574EC3F61F050BFB10,
	U3CU3Ec__cctor_mCF283ED21B6D69D0AB24851CA57E687C12113D20,
	U3CU3Ec__ctor_mED4A9A4E238E59F512EA6542C2C74D8E908CA1D2,
	U3CU3Ec_U3CFindMainCameraU3Eb__196_0_mB2EF6FFC72FE542CC87CCBB649B4BB784E94BEC3,
	OVROverlay_OverrideOverlayTextureInfo_m3DCF93E34B01580970D60D0215724B37E2BF083E,
	OVROverlay_get_layout_m2D64B814A51EA065C2B5010AF9BC9EB2C600FB66,
	OVROverlay_get_texturesPerStage_mB53D8F108151B58175A256D8BA28CCEA2FC1E082,
	OVROverlay_CreateLayer_m85C186A08F0C02DD66235583D02E0C2F94AEBAB6,
	OVROverlay_CreateLayerTextures_mFB2C5EE0617A15333ECC8BB66EE59F6BE4B79775,
	OVROverlay_DestroyLayerTextures_m9BE57EC22590300B983D4CBF161288C0FF4703E1,
	OVROverlay_DestroyLayer_m4B3111ADEF940631EFB19CAB62F26D6E4DF705C4,
	OVROverlay_SetSrcDestRects_m7DCB79AD3F0080514C800FB5D24665F195525AD8,
	OVROverlay_UpdateTextureRectMatrix_m3CEF00BFA7470A08BD569157FD26A4825F117C5C,
	OVROverlay_SetPerLayerColorScaleAndOffset_mB71987F85E032A94EF98BCB0736A503C2F54F0C9,
	OVROverlay_LatchLayerTextures_m1C037B2F6FC56A2B9F0825CB7FFCCF5FF6B9B8C4,
	OVROverlay_GetCurrentLayerDesc_m92721260E9C150FF61986DBDD9E23E05CFEAEDD2,
	OVROverlay_PopulateLayer_mA26F77660DA6C25B6D8A53C7708DD3EB0C69A71D,
	OVROverlay_SubmitLayer_mD3995EDE8574CE4FC3329BED6531FEFCEB38DA3A,
	OVROverlay_Awake_m74C6851805B5431E3D34D73EFBF7B8E572C5FF7F,
	OVROverlay_get_OpenVROverlayKey_mEF6F84F8FF68FC201F99156214FFF4031A5534F8,
	OVROverlay_OnEnable_mF0C5A9092AA36B8D8CCA109F0C2610370C3A8992,
	OVROverlay_InitOVROverlay_m474A4ECF6D19AF79FDDB3AD63F180441D4A1E35E,
	OVROverlay_OnDisable_m4C19945F851A3A8B3098905B10E0C5A0ACCD0498,
	OVROverlay_OnDestroy_m1021238A607C57B26F2FF79B2BE47FB0AB54988F,
	OVROverlay_ComputeSubmit_m89437B0B5D5BB10B02DAED0A6B98491F29F393B1,
	OVROverlay_OpenVROverlayUpdate_m75733B3F0849C33F1280D2894A3F32663EF28C0F,
	OVROverlay_LateUpdate_m325138D8F2685430DA39F84CDECEF68103FFBEFC,
	OVROverlay__ctor_mB87D9EABB9338C64620007DF5DEB4BDCC570716D,
	OVROverlay__cctor_m3DECE8D45B87A5D7F693C568CC1DEF99532FB753,
	ExternalSurfaceObjectCreated__ctor_m1B546172F7C6A09C9E050CCCBD2F7B67D613B3AA,
	ExternalSurfaceObjectCreated_Invoke_mD94AC5D924842F20FA91CFECF2AD57241A0ADE4D,
	ExternalSurfaceObjectCreated_BeginInvoke_mABE1CF22A8A1E6739C89A09719D51381137FA4A2,
	ExternalSurfaceObjectCreated_EndInvoke_m9F6BA59C290034328796300277F215FC5F61E691,
	OVRPlatformMenu_HandleBackButtonState_mBF8F637999C3992EAC1DA019EAFF716CFB3F31EF,
	OVRPlatformMenu_Awake_m2EFD1169C2616E81D9FA9144835B4EFD4A7178FB,
	OVRPlatformMenu_ShowConfirmQuitMenu_m53589E50AD8D461C277AD2D0D61EBAFA774E55D9,
	OVRPlatformMenu_RetreatOneLevel_m42EB7CA2FD8BD9A8610C03DBFE143386FA066605,
	OVRPlatformMenu_Update_m3CCA1AB33BC82AD44965ACC63D020F9892208A15,
	OVRPlatformMenu__ctor_m130FA2C8BE776EAB92DCBADE2E5049E5FD8949A7,
	OVRPlatformMenu__cctor_m6D55DB7807BBDDF8BC38565066C671C5BF087592,
	OVRPlugin_get_version_mEB9C2FA4B55E714D70A55366E9EEBDAD8E361286,
	OVRPlugin_get_nativeSDKVersion_mEB77B7C8EE154D25D5DCE159931CF97B5D1D5834,
	OVRPlugin_get_initialized_mC88032BD2BA67E12DFF85A5A92F52AE183820B27,
	OVRPlugin_get_chromatic_mC410957203243A05964CC341641C15E53D3B1627,
	OVRPlugin_set_chromatic_m0E2DD4C12960B8F975CBBA498C5A0A122821512D,
	OVRPlugin_get_monoscopic_m3BB5AB1C7A00E1BC1AE6ADE9C7EEB7EB873E0967,
	OVRPlugin_set_monoscopic_mAD538530F8F2A9C9CC09D9CC00237D7473B96827,
	OVRPlugin_get_rotation_mD1B34D3419E6312F3EA3B950751A1754D41BE31F,
	OVRPlugin_set_rotation_m17BC23C1B1AF0E429D5671CE39B42C1191991603,
	OVRPlugin_get_position_mEF1F6068F44AFF008848634DAF658FF83148B0A8,
	OVRPlugin_set_position_mD5D62A9A4CE78B2136AB19EC9EB2A45FBCACD4E4,
	OVRPlugin_get_useIPDInPositionTracking_mD9A79B1A05173B12CCE3E9A0D0243BD6847FF865,
	OVRPlugin_set_useIPDInPositionTracking_mD64C10440A7078A2F56C5DB9AC25D2460786DCF9,
	OVRPlugin_get_positionSupported_m62C34824B86815DF220CA417DC64559C6CEC1FA4,
	OVRPlugin_get_positionTracked_mBA38B359913BA23E306E66132638CE54FDC5AC94,
	OVRPlugin_get_powerSaving_m963E0A888068F336B01FE28ADDBE5924401EDBDF,
	OVRPlugin_get_hmdPresent_mB282E1E5AFF6D331EC3F833FE8D79F7CB008BA5E,
	OVRPlugin_get_userPresent_m17458D95D75EA4952A56B088321EC7F646E9921D,
	OVRPlugin_get_headphonesPresent_m55EEF0A8127C7797E81D8A54B93B9C4170EE2D74,
	OVRPlugin_get_recommendedMSAALevel_mC728B00F70079A9A7F25FE3EB48C7E70554BB3C7,
	OVRPlugin_get_systemRegion_m300AD5156AE28135935F9FF0156F7D30664B02A9,
	OVRPlugin_get_audioOutId_mCE070B81A00878965DC3924AA8E14F643CAF2E61,
	OVRPlugin_get_audioInId_m7B3A33CD32F88C09B173CD2BCFB62220378A8F9B,
	OVRPlugin_get_hasVrFocus_mBE848A231E8BDD2901465D70185AA98B1FB870C2,
	OVRPlugin_get_hasInputFocus_m5E75D7DC02A567DF9042C52367038164B8E8AD1A,
	OVRPlugin_get_shouldQuit_mA15A11C8D356B73C821329F36E696F2A751EF8C7,
	OVRPlugin_get_shouldRecenter_mB2965AD2ED7D4FAEFBFFE8F43A9D21F5A34A78EA,
	OVRPlugin_get_productName_mE0DEA1F1C2AF365A49BCAC235484E33C04C68DF7,
	OVRPlugin_get_latency_m7CA0FED05E9F585A4D2DC5215003535B65E3D69E,
	OVRPlugin_get_eyeDepth_mDCD97322897B0634D98A759D9153F2F9B313F322,
	OVRPlugin_set_eyeDepth_m02A9053C4CDFC055826632CA5CD74DEE0343DDE1,
	OVRPlugin_get_eyeHeight_m648BE7F5B7635B24B04DF556F6AC9333D7240FE4,
	OVRPlugin_set_eyeHeight_m06836A3D2F267544770C1B5E36FFA3EB57BE3442,
	OVRPlugin_get_batteryLevel_mF930C2B7B6900B7F846F1B0889AD99285B99E19E,
	OVRPlugin_get_batteryTemperature_m61C24E9A580D96FB270D8EC8BA93CF3FDF48ECEA,
	OVRPlugin_get_cpuLevel_m5106E98BB05173937584C507B9382BEB5F1D52FF,
	OVRPlugin_set_cpuLevel_m0B70D791641728233611E69DB7099728F0773AF6,
	OVRPlugin_get_gpuLevel_mC3969B9E0EBD07A599EBFB0328A0C65392808DAA,
	OVRPlugin_set_gpuLevel_mE4F4282D1B36C06F2A01F4680B01BE8EFAD0DFAA,
	OVRPlugin_get_vsyncCount_mC93B88641934956164FC563C46F12DD3068EB5EC,
	OVRPlugin_set_vsyncCount_mE5C06B5D38D7B4B6E3C5D62FDB6BCFA25E216108,
	OVRPlugin_get_systemVolume_m15038ACCE04B10E03644776B7558B258E8C23F78,
	OVRPlugin_get_ipd_mE2928EC4FC9DC56BA02DD8935281933FFC6C73E2,
	OVRPlugin_set_ipd_mBC4501DE11F7C728206CA1358CB8AF0CBD64AD17,
	OVRPlugin_get_occlusionMesh_mDDFB6DA888A74D12E4F29E6AC6CCEEFDF2B4C7E5,
	OVRPlugin_set_occlusionMesh_m59E8C3D809B25ED95AD6DD0CCF18ABB4081EC3D9,
	OVRPlugin_get_batteryStatus_m63050AA1BE0C9D86916F7396A3FB5DFEDCE515BF,
	OVRPlugin_GetEyeFrustum_mB282B6470968CD4C5840015AB8780D6F2E4BBDB0,
	OVRPlugin_GetEyeTextureSize_mD9EF059EAD1B688013C541A8DEEC7DEAE408D67C,
	OVRPlugin_GetTrackerPose_mFF540B201B40C231DDE95D15027C50AD21CC1EE1,
	OVRPlugin_GetTrackerFrustum_m82C30E1A089CDFF7C1DAFDBABCC208663596B18F,
	OVRPlugin_ShowUI_m570EB1C014D1A16278F5281FD6E988485CC0E1B0,
	OVRPlugin_EnqueueSubmitLayer_m85A14E16646E2CCE3F92C57E3ECBCC3830997A97,
	OVRPlugin_CalculateLayerDesc_mF9A8E3175E159D17E9A1BD7938296D32B04CEFCB,
	OVRPlugin_EnqueueSetupLayer_mB198537F855D269CA0354C3C8773DAF8A990EB47,
	OVRPlugin_EnqueueDestroyLayer_mF0570FDA752A8BA4884D2C39BCEFFBDC9F46E2FB,
	OVRPlugin_GetLayerTexture_mC0542B78B4D54BFDA03471E7313FF7FC08D748D6,
	OVRPlugin_GetLayerTextureStageCount_m49FB1477D25FB41EACFD8B3B803EA298F4BC1140,
	OVRPlugin_GetLayerAndroidSurfaceObject_mB9749C2A415FF74E34635537EA42DC7B12A6E145,
	OVRPlugin_UpdateNodePhysicsPoses_m1FF7E1ABE7DF31F01496FCD11F2202CC71798553,
	OVRPlugin_GetNodePose_m8E17A9C355D1EA3C8F4D61711D05F11A8C07D7B4,
	OVRPlugin_GetNodeVelocity_m0E4E9D3C7BDAD86EAB8A44B4725253749E796C74,
	OVRPlugin_GetNodeAngularVelocity_mEBD792C57D22CE8A3D207795F94F081C9881D019,
	OVRPlugin_GetNodeAcceleration_m4E95166E277B33B4F9DDEF62F491109B8AE31560,
	OVRPlugin_GetNodeAngularAcceleration_m9A023A1EF55501B45CC90C07F9C7853D0C923127,
	OVRPlugin_GetNodePresent_m2BC4E2F09494A80769D135A9E5D06CE8FEC22874,
	OVRPlugin_GetNodeOrientationTracked_m3E5E013A3D663F0CF0E7C035D7146A49CF068A2F,
	OVRPlugin_GetNodeOrientationValid_m366551E25AAB8064D95D3E5718AD3BF5E8957CD5,
	OVRPlugin_GetNodePositionTracked_m436EE2C5F627527458249795360F351113396546,
	OVRPlugin_GetNodePositionValid_m072E3F2BAD71D069475AB85C2621119186C5E0C3,
	OVRPlugin_GetNodePoseStateRaw_m000A5EDBD619458DFE69065ABE0C5E2AAC0AD6BD,
	OVRPlugin_GetCurrentTrackingTransformPose_m6C9F84840F7B578E28D57BB2417B0B012FB78083,
	OVRPlugin_GetTrackingTransformRawPose_m704B52FDEBA37F9497B07C11E6EBC5192BA87E57,
	OVRPlugin_GetTrackingTransformRelativePose_m1655AC2D28911E8A8F13A6453573E2A0B723B1A3,
	OVRPlugin_GetControllerState_m33319AD1556B5F03FC935F464684CF7862E4CE5E,
	OVRPlugin_GetControllerState2_m86EB19B91A75C94EB0E440460AFF13EE2C698BF3,
	OVRPlugin_GetControllerState4_m0532C6F230A218CEB810EF3349DACCDF2A55497F,
	OVRPlugin_SetControllerVibration_m5AD5160D0F123599B291905A686345A916084E22,
	OVRPlugin_GetControllerHapticsDesc_mC2A686A189AB58F1B505D8DE985DC1B99D11577C,
	OVRPlugin_GetControllerHapticsState_m328F5D227817DDED13645D47BE19BCF59525305C,
	OVRPlugin_SetControllerHaptics_m3CAEE3E26EA82254EEAB05ACE29A2A0B5872F5FA,
	OVRPlugin_GetEyeRecommendedResolutionScale_m296EAAFF9057A7570C007E1C7323E48F298256E2,
	OVRPlugin_GetAppCpuStartToGpuEndTime_m6ECBEDFEA85313DFD842693DA138101F2AE536C2,
	OVRPlugin_GetBoundaryConfigured_mD2841B3BBCD935D91E6DAEC6FC1F264908C2BACC,
	OVRPlugin_TestBoundaryNode_m34EBBC52B5387BFC8A22B1A919D79A6617656639,
	OVRPlugin_TestBoundaryPoint_mF663F28191FE448A4290C8A1134FC2FA65696054,
	OVRPlugin_GetBoundaryGeometry_m82BEE773606B284084CB6B12CB8F3385D1165376,
	OVRPlugin_GetBoundaryGeometry2_mB81B5788856F3E0ECB819CCA5EFDDBC03FF804BA,
	OVRPlugin_GetAppPerfStats_m88FB26784D4E5818A92F0EE7664A7C44A1D26798,
	OVRPlugin_ResetAppPerfStats_m09307AEF6BBD8BC41F8B6EC6B1693E02AEAE11C3,
	OVRPlugin_GetAppFramerate_mABAD5A920BD49CA405CE59C081964B4254006E20,
	OVRPlugin_SetHandNodePoseStateLatency_mA9BB8C47B48AFDFF4D91CB008CD1B54FA8E657A6,
	OVRPlugin_GetHandNodePoseStateLatency_mF531430D816D66C877049DE9F20A1A573290F99A,
	OVRPlugin_GetDesiredEyeTextureFormat_mC689F281E14E8284CF5CC90D13D619289D3B6F80,
	OVRPlugin_SetDesiredEyeTextureFormat_mBC33132FC6DF8CC438A0589E3D0B273C00D7BEF2,
	OVRPlugin_InitializeMixedReality_mED45B2B983DA9885D9E2B6502BC88E3B3C4D3B20,
	OVRPlugin_ShutdownMixedReality_m317FF312A0F19C8CDA496F07418A97785FA8BCB1,
	OVRPlugin_IsMixedRealityInitialized_mD500325BE1DD0860CC86C0C20E360AF70F933DE6,
	OVRPlugin_GetExternalCameraCount_m052D2022ECCC13FC6C0FCAD86D4AB8BAF3C7A665,
	OVRPlugin_UpdateExternalCamera_m20AE9EFC6A4B4E7C964933B83F1F3714791DA963,
	OVRPlugin_GetMixedRealityCameraInfo_m612910B4EE99526DEBC27C1AF3134C1208ACEE15,
	OVRPlugin_GetBoundaryDimensions_m77D2E3C3020151115C11B6AD841166EBF80416B9,
	OVRPlugin_GetBoundaryVisible_m5F0C29D06ECA6B229ABA6287888B73EBCEBA0409,
	OVRPlugin_SetBoundaryVisible_m85F8141A83704C7BE67DD4DD92A7B6B6BAB00C9D,
	OVRPlugin_GetSystemHeadsetType_mF98D9E09E27729E88645A1644B66840E20A5A0EB,
	OVRPlugin_GetActiveController_m9745C9E3F9C5CFC7F7B308F2D3C0DA463055577D,
	OVRPlugin_GetConnectedControllers_m8E27DDA4FBAE4058BDDE6A308DD20B44A09F1739,
	OVRPlugin_ToBool_m8EDCF3BDDED6AC1FC142C1FE6B536127B70BF2D7,
	OVRPlugin_GetTrackingOriginType_m46D172CDB9E440588A9616665F62F263F7A292C1,
	OVRPlugin_SetTrackingOriginType_m083C10AA6533612EA760CFAEB0EBD5118E76BBD3,
	OVRPlugin_GetTrackingCalibratedOrigin_mAACD6689ED95DDCBEA89362F3B190454B7B925B1,
	OVRPlugin_SetTrackingCalibratedOrigin_m4AFB8183DF1BFC4F6CC229A29045A69291666AF4,
	OVRPlugin_RecenterTrackingOrigin_mF85344F36B3B67AB6EC6255BD447A71C42C55238,
	OVRPlugin_get_fixedFoveatedRenderingSupported_m4AD027704502D11B747374ABB8744A7FBB713F5C,
	OVRPlugin_get_fixedFoveatedRenderingLevel_mBBFB81B5306788FDF943123B77B8E9947F9FD3C2,
	OVRPlugin_set_fixedFoveatedRenderingLevel_m662564421557C9089301412892D600898931A556,
	OVRPlugin_get_tiledMultiResSupported_m35D05CDE175B0151F58D3FB06E147F763BE3AE34,
	OVRPlugin_get_tiledMultiResLevel_m0A3E0F393A7288E2E9DC1596D1BE7CC890AC83CB,
	OVRPlugin_set_tiledMultiResLevel_m3D938445562C8073E2D8B9AC533D6FE5995D7CB2,
	OVRPlugin_get_gpuUtilSupported_mD1E48D8B2A982DC5E6CAC0097CE9806A50278017,
	OVRPlugin_get_gpuUtilLevel_mB2CC6C6BA200A0C5B4ED2030AD295ED8C4F3398B,
	OVRPlugin_get_systemDisplayFrequenciesAvailable_m6AE8A36131A28AD77332CF64AC57A1A20233BB71,
	OVRPlugin_get_systemDisplayFrequency_m208A930A385234253D0C42E7976FCDA49670E5C6,
	OVRPlugin_set_systemDisplayFrequency_m4D6D72581C79E6EE0278A3E5E42A6596D86A3569,
	OVRPlugin_GetNodeFrustum2_mE5D3787585754BC1D6F89F4123DA6001EFF8FC0A,
	OVRPlugin_get_AsymmetricFovEnabled_mC7863F3E087243063D719F03401156D78E8F6399,
	OVRPlugin_get_EyeTextureArrayEnabled_m0CF09B463DCD9EAD79D7CFB35C88358E3DD3DFE2,
	OVRPlugin_GetDominantHand_m2EEFE95D8510986C2C08E3231794014B74ECA93B,
	OVRPlugin_GetReorientHMDOnControllerRecenter_mA2C7EA9DC5CCC5056670E5B9EB3BCFCFA13F7DC6,
	OVRPlugin_SetReorientHMDOnControllerRecenter_m31F2721F1EFBBA38958A5A225D3DF0A351EDED76,
	OVRPlugin_SendEvent_mC013F62F65AF33B6421E1D8A4A0BDEE5AC537778,
	OVRPlugin_SetHeadPoseModifier_m1AE42678B92BE1FEC7CCD8EC96478890878BF566,
	OVRPlugin_GetHeadPoseModifier_m297D517CB172AD2CF3A037C4084DA4238161F329,
	OVRPlugin_IsPerfMetricsSupported_m410EB38D0A8922C14FACE593B2B7DEAED73E4426,
	OVRPlugin_GetPerfMetricsFloat_m9581366E9BA559C4B1ED2CA1B78245DACADC0AF1,
	OVRPlugin_GetPerfMetricsInt_m7C48BA5972D09980A9D17AF66C79DC248698E6CC,
	OVRPlugin_GetTimeInSeconds_m7D0A99C37EB7C5AC0C3F2D7EBB6B28B34BCF03DF,
	OVRPlugin_SetColorScaleAndOffset_m4E5067FBA1D8413EDB9EEE54A0E67151F1669DEF,
	OVRPlugin_AddCustomMetadata_mB2EF38F10EC62F3B63CB93A364BC342B461BBB53,
	OVRPlugin_SetDeveloperMode_m8FB3CB2325C2D916DF720967271A6348951E454E,
	OVRPlugin__cctor_mB76764AC9D1F9EAC44550E0645E4E610336E3C69,
	GUID__ctor_mB41A593938E3F6E09CD8DD70E19A065AAF72216C,
	Vector3f_ToString_m52F5DF2083B0F4B8CE7AE59E92E8947971D48240_AdjustorThunk,
	Vector3f__cctor_mB31252A637DD589E8F052DE55AA1C4B3B8A6A7ED,
	Quatf_ToString_m8A49887F29F66BF5CAC155958941771F0AEA3E7E_AdjustorThunk,
	Quatf__cctor_m66765639C6821E433F25FF1208FDA86880FEE2DA,
	Posef_ToString_m53165D004C9025E97DDBAF497A5E5097615CA0E9_AdjustorThunk,
	Posef__cctor_m5416229378AC93676B54FCCFD32F9064AD11A503,
	TextureRectMatrixf_ToString_m86B06425AF311C178BA27F4B362FD77B95A7B8C5_AdjustorThunk,
	TextureRectMatrixf__cctor_m1208362CB610A1AEB2B4B7E50BAFC001773169BF,
	PoseStatef__cctor_mB723CAE0F82C4634B13687F392BD7DCD8B9E7A32,
	ControllerState4__ctor_m6EBB9E6A88945E4FCAB3BCF70D6B0A3C719B2218_AdjustorThunk,
	ControllerState2__ctor_mE197436281EA28931E3EA0ABC787FA163ED49545_AdjustorThunk,
	Sizei__cctor_mBE8606E15C1CD0DE04FADA3467A3C64C9D74FCEC,
	Sizef__cctor_m6569C2BBB4B2551AED754AC807A3B10DFF881008,
	LayerDesc_ToString_m0D64A316B93226028E7E28A447FACEED02C7C234_AdjustorThunk,
	Media_Initialize_m110EE9A132C73E287DF471DD98FE1CDF28980D50,
	Media_Shutdown_mA1E27A8F792246C916C50FD199C31F2D781EEBC8,
	Media_GetInitialized_mFF74ED9A9E1F70355A08E1EB1070727B1E7CF5AC,
	Media_Update_m79D255C18B4423799B11AA648F71C86E44D8AE03,
	Media_GetMrcActivationMode_m82704E4755C576CBFAEB056448DECC2CC7341E30,
	Media_SetMrcActivationMode_m2478FD333BE8C1DC01D6637C5173D16C6EB8D49D,
	Media_IsMrcEnabled_m3D1FBBD6A08ADB94B52D703DE471AD81CBFD660A,
	Media_IsMrcActivated_m11C79E99CA3F824D08A2529C743444566BA2EEE9,
	Media_UseMrcDebugCamera_m83308CB20311C0FA1949EF40A9185C1750401664,
	Media_SetMrcInputVideoBufferType_m7A4451CB98F212E4A0275428AF89B3F471624184,
	Media_GetMrcInputVideoBufferType_mB075060863D22D392623C25D0D56BC463C3D676A,
	Media_SetMrcFrameSize_mA63E3989BAE333E979048F080D216CCCE9FBB885,
	Media_GetMrcFrameSize_m2D251EDBF48690D30A8C8F9CB1243B4B573F1C61,
	Media_SetMrcAudioSampleRate_mE474E40D625718FD0E1B02ABC1C4CE8750D0CB87,
	Media_GetMrcAudioSampleRate_m0FDB114C9CA0E57E093D4801213894FEA2A71603,
	Media_SetMrcFrameImageFlipped_m8B0CFED72A146031A7F390DA10694FB99E77E2CE,
	Media_GetMrcFrameImageFlipped_mEF19E8EDCD9376CC2516BE69B59FA6FE0663417D,
	Media_EncodeMrcFrame_mF6E6D3997B1983375B04DC88AD2C8AC11B51B080,
	Media_EncodeMrcFrame_m08AF783BD90950237D48F85598BE2B0EC1E1ADA4,
	Media_SyncMrcFrame_m4699985766B31D8CD69D1C2A7D5BA8557B1B559E,
	Media__ctor_mA4CE69A55B97638F9F96996AA555E9D950F572F8,
	OVRProfile_get_id_mF6B9F2B1657033A53D7FCACB3BCA4A5D463E1142,
	OVRProfile_get_userName_m22EA9ACF72E83B5E1AD453CFB856ABEDCA387DEB,
	OVRProfile_get_locale_m952EEADD042F1176AFEB9137656D2E56DB84EC3A,
	OVRProfile_get_ipd_mBAD219728CDB9641BCB4621F9E58A7C723D0E485,
	OVRProfile_get_eyeHeight_m700ABDDD88905E629CD73DBDB3BC9D8A05248C17,
	OVRProfile_get_eyeDepth_m6A7F6837846068E10E2E9A976386B693B7246A04,
	OVRProfile_get_neckHeight_m0EF4F8D9ABF863A68BEF010A98BD1C9DF2F31DC8,
	OVRProfile_get_state_mF3E526492ACC893F81D30EE98AB656C7AD3A4DE4,
	OVRProfile__ctor_mE5BBAB7A4F3C262DAF1D2E0A353DA28F84CC03BB,
	OVRTracker_get_isPresent_m22FF23801E88857AAEB8F79B60C30D189ACCF83D,
	OVRTracker_get_isPositionTracked_mDF1A07176116F8108D84687EAEB0014E1032E6EB,
	OVRTracker_get_isEnabled_m5FCDBFEADD8BDD0E6EA49C2EF8863CBFC884AD1C,
	OVRTracker_set_isEnabled_m9A79824010DCF1A79715085FCF4E3410B28583B2,
	OVRTracker_get_count_mA452A77BEA0B84A36CC4595BE8CFD2AA2A9912A6,
	OVRTracker_GetFrustum_m22ACF82F788AFFA64BC98726CC59D0FE67E53B58,
	OVRTracker_GetPose_m18A4C0458E4AE6AC3D12B2A52DD8C957D6D1DDD7,
	OVRTracker_GetPoseValid_mF540E6BD8912B335237F3491B3FC4BBFBE990E35,
	OVRTracker_GetPresent_mE13F140DF9F5F558E57F882F05E53514151F43A6,
	OVRTracker__ctor_m9A28691E308F96818A71BBA542B2E23FB701BEB4,
	OVRAudioSourceTest_Start_mB1A8057A0BE8267BF8C72C5F8C7F27A42D9DBACB,
	OVRAudioSourceTest_Update_mA9FD2DDAC817707E7E3AA9ACD83C3FDD853B719B,
	OVRAudioSourceTest__ctor_m1F7B9F8963600D1EFBC12EBD0066674A3147A552,
	OVRChromaticAberration_Start_m2D11EC425F5D5B92C4AEC7F5217C7F1D0054A0EE,
	OVRChromaticAberration_Update_m990F9764B1A447A4F95E5766EF70B2CABF23661D,
	OVRChromaticAberration__ctor_m886C9F6567B2EDD3BA36DDCCCE301E3C89A6C9A5,
	OVRControllerHelper_Start_m8174B79E134F3E2BCC2AF841BE81FA345BDBD1D8,
	OVRControllerHelper_Update_m4FD9DE5966FEE9776FBC399F8D6D4E4FF1BA842C,
	OVRControllerHelper__ctor_m89D06B34BEB6CFF68A6C124E91148B9E44D01EE0,
	OVRCubemapCapture_Update_m519D9FBF324882EB42CA3354FF73DC08B1D52DC4,
	OVRCubemapCapture_TriggerCubemapCapture_m249C89D7387A8F50635512025B99348CA2882171,
	OVRCubemapCapture_RenderIntoCubemap_mFE06AAACA3FC4F53B524844333D4A37C3DF9D139,
	OVRCubemapCapture_SaveCubemapCapture_mC269B7EBFC062AD561D7DE34F2CB7F0368D71B28,
	OVRCubemapCapture__ctor_m3B4CF453D98FF1B6BD9D7F708934FC9BAD1C9D93,
	NULL,
	NULL,
	OVRCursor__ctor_m6F6920D21D4918B96658ACA251E6B34D636CA237,
	OVRDebugInfo_Awake_m0959C6BF0B8C661548C49FF452916500AFACE05C,
	OVRDebugInfo_Update_mA8F99324C92A1232AB35C3DE71BBE819F42EAF2C,
	OVRDebugInfo_OnDestroy_m0B2CA7FF62E4EBD0CAE37F47FE85E5A41B264183,
	OVRDebugInfo_InitUIComponents_m4C2D569AC7195FB227803C1410F0126CD18DE972,
	OVRDebugInfo_UpdateVariable_m1B782E9B84463FC84F8E5F04589B15367CC1321C,
	OVRDebugInfo_UpdateStrings_m1495AAD95AF6C2CAD4FA5A68CB82799C4F121138,
	OVRDebugInfo_RiftPresentGUI_m1D46CBC279389D9217B7782F54B9C36654694B91,
	OVRDebugInfo_UpdateDeviceDetection_mBE2D48F966B4436C428D0AB5CB840FD040687B08,
	OVRDebugInfo_VariableObjectManager_m62A43FB195A7519A5803CB989E8DBB33AFCA0322,
	OVRDebugInfo_ComponentComposition_m5B22234BABFA46E7312990BC3F382DAC0FC904E6,
	OVRDebugInfo_UpdateIPD_m3D65C1427A56C9A86AD89764BC4520C8927E8266,
	OVRDebugInfo_UpdateEyeHeightOffset_mB05494A3F1418A7C2D4DD750DC06AFBC5391C4C8,
	OVRDebugInfo_UpdateEyeDepthOffset_m900B121BF0EBA2343FA03ED8E93A9005DF921E24,
	OVRDebugInfo_UpdateFOV_m395AEB87AF5D9A16165C70D07221F5501B653533,
	OVRDebugInfo_UpdateResolutionEyeTexture_mA7EAD7AAEEE088195DF317563046A69D5C3589ED,
	OVRDebugInfo_UpdateLatencyValues_m525106BA0478918283BCEC28B08EF67DD4C4796F,
	OVRDebugInfo_UpdateFPS_m9C3E0E797BA2939F4A7C8092719FE5F2AFF4EC24,
	OVRDebugInfo__ctor_mF380334B95800EB1A3E085EA670A8B052EF3245B,
	OVRGazePointer_get_hidden_m1CC04CD9F7142893A8EBCB7036714747B37D0F6E,
	OVRGazePointer_set_hidden_m91028FD28F5623B27B97401727AF7F363B785AF6,
	OVRGazePointer_get_currentScale_m5051A8B8D60AAA1AFBE4D0DEDCA126834479092C,
	OVRGazePointer_set_currentScale_m0BB5E81BF2951D8CE84331E5773DAB7ADC13B506,
	OVRGazePointer_get_instance_mB814E0429576CD0768FE8E6BC4F1DEF39BA1F696,
	OVRGazePointer_get_visibilityStrength_mDB9C9C1AA471FD4879108A6619F3081789D83AA8,
	OVRGazePointer_get_SelectionProgress_m23301CAC39B254665615AB2043ACA9515DAAEB81,
	OVRGazePointer_set_SelectionProgress_m681026E6FFEEAC87247F5CD70E2D257D68C8AA35,
	OVRGazePointer_Awake_mB16AD7EBDBEF175B83BBF294EAF75E54B2494E32,
	OVRGazePointer_Update_m47B6CA68B973ABD7DD50A089DD0828E5D51A66F8,
	OVRGazePointer_SetCursorStartDest_m66BE5DAB68F34F03CE012B29BA1100DDA7C74378,
	OVRGazePointer_SetCursorRay_m6BFB06845441CDA570D4FD6CB255D2488DBE8923,
	OVRGazePointer_LateUpdate_mAB1945271CFB531CA5ED4BC960038F1869DA197A,
	OVRGazePointer_RequestHide_m5BFAC46F71DB65CED6B99974401F602B03F9EC8D,
	OVRGazePointer_RequestShow_m283EF20BD04309581CD8FDADFEB48E06BEB17F75,
	OVRGazePointer_Hide_m5DD0A17F041B0ED3CAA74056E06BC41F56D7F092,
	OVRGazePointer_Show_m37CF988CF6C07C90823B78715D7BD439C4A025AC,
	OVRGazePointer__ctor_mA7772B90D2A107BDAD261B37EF7A548FE6EAB358,
	OVRGearVrControllerTest_Start_mF4C8D0A5BD562179C62EC446ACF3C7E9941BE475,
	OVRGearVrControllerTest_Update_m0F28590EAE690EDEBAC33B5A99201FFE0C541E60,
	OVRGearVrControllerTest__ctor_mB7172FA925271BEF6344AE7099DEDA8492A993D4,
	OVRGearVrControllerTest__cctor_m6E5846C09FF582606C6E3B5D278F91521597935B,
	BoolMonitor__ctor_m88D8A874667D1A4A5EE7CFE065C90CD22E0767C7,
	BoolMonitor_Update_m643614BB078842DF7BDF42A119D07CA4FC12F4A7,
	BoolMonitor_AppendToStringBuilder_mAC6A2F1A0B036BE248590A14114DABBC27A4F678,
	BoolGenerator__ctor_m7CD43EC2090CF104E05C9EE030044974BA3078C0,
	BoolGenerator_Invoke_m723ACF6DFD86801B825F3845D51A5A6BBAB0C4AB,
	BoolGenerator_BeginInvoke_m2A1F2E4A764442E894BF04D95A1D1C22991AAFE5,
	BoolGenerator_EndInvoke_m33008BC23FBA9CF4255DD28B85BD6896DD42BC60,
	U3CU3Ec__cctor_m8F500FC932E47035E957AA7659D2E4A96B495549,
	U3CU3Ec__ctor_mA42DB8363017E94A70841C0A467957EE80E3A741,
	U3CU3Ec_U3CStartU3Eb__4_0_m44A63E093D6B6C8CE47D49665FA141257CAA4D35,
	U3CU3Ec_U3CStartU3Eb__4_1_mD9AD48120837747AED4E5AE7E187248CB0C4578E,
	U3CU3Ec_U3CStartU3Eb__4_2_mEABA34409121BF66E0773DDC007D991AAED8A3EF,
	U3CU3Ec_U3CStartU3Eb__4_3_m2FADF2C9A38E6E471848615A922C1929EAEFCB29,
	U3CU3Ec_U3CStartU3Eb__4_4_m2E9B7E7541ED75D63C743B5014B96726D31AC919,
	U3CU3Ec_U3CStartU3Eb__4_5_m761D28AAC6AA1CA293E918BD58E770F3B8AEDA46,
	U3CU3Ec_U3CStartU3Eb__4_6_m686F164708715A3044C934DA7311691D99F27CB9,
	U3CU3Ec_U3CStartU3Eb__4_7_m3A354FABF7B734EA6217B5E8D4F5FF25AFA7072C,
	U3CU3Ec_U3CStartU3Eb__4_8_m644067018CCB5162F36C4476D50318FCAED411B8,
	U3CU3Ec_U3CStartU3Eb__4_9_m80BCA1DF15CEEE0EE3C373190019EDB9DB19BE23,
	U3CU3Ec_U3CStartU3Eb__4_10_mE2942CF0DE26D8519D7BEA08432AE0D61653A5C1,
	U3CU3Ec_U3CStartU3Eb__4_11_m1E86165B012C5250D341759EC98BE7AD0A90E4AD,
	U3CU3Ec_U3CStartU3Eb__4_12_m9439754D56B96C3148C93CAA4680AAF93AA4178E,
	U3CU3Ec_U3CStartU3Eb__4_13_mD60BB4F89156092829DFA371977A60F1EDE495E0,
	U3CU3Ec_U3CStartU3Eb__4_14_mDA1ED84100D40C7E60AEE22BEA0F33355987795E,
	U3CU3Ec_U3CStartU3Eb__4_15_mF5C2985F3910D9424813D0B674B3F4D747F59290,
	U3CU3Ec_U3CStartU3Eb__4_16_m72755202C4FD3BE42F20A4E32E71B6603DAD7CEA,
	U3CU3Ec_U3CStartU3Eb__4_17_m8206839BE943F50E4133AC7FC2D24EB09C788AE3,
	U3CU3Ec_U3CStartU3Eb__4_18_m6240EF55CA073017897FD60BCAF0B147110D4AA0,
	U3CU3Ec_U3CStartU3Eb__4_19_m8B1F1891FE6BBB7193DE04B4A8986C936C28DB9F,
	U3CU3Ec_U3CStartU3Eb__4_20_mB9CBD36D63F5D9D0F0E1B37C8B1CB1922CA0A842,
	U3CU3Ec_U3CStartU3Eb__4_21_m3EFDE0E591DB94DCC68A449CBF009261F0134B6E,
	U3CU3Ec_U3CStartU3Eb__4_22_mE820AEC1242AC5716B37DA0EE08783E9593F5996,
	U3CU3Ec_U3CStartU3Eb__4_23_mE868AEBC402C62AA6BAD13D8DBE8A3EF667419CB,
	U3CU3Ec_U3CStartU3Eb__4_24_m4D43E168623A700523B91BAB9C2C7D12CB45972F,
	U3CU3Ec_U3CStartU3Eb__4_25_mF7D5178C19BA1438BD8D4C52919121A2BFA63C3C,
	U3CU3Ec_U3CStartU3Eb__4_26_mF3F63586ECA5570986360B522DB3830A6D1F008E,
	U3CU3Ec_U3CStartU3Eb__4_27_m39746C908249E7B79937F51628CF748A976B95AD,
	U3CU3Ec_U3CStartU3Eb__4_28_m1D6C556535C5B7B8D8399C7D55AC80E29B8F11AD,
	U3CU3Ec_U3CStartU3Eb__4_29_m11151152D9CA3A57F078BBEB18786018926492B5,
	U3CU3Ec_U3CStartU3Eb__4_30_mEDE76950B0475200DF3AB47793722EB7D05BD4DB,
	U3CU3Ec_U3CStartU3Eb__4_31_mC0C198FC8373B9B9098FE957DA1F8DE8E3976821,
	U3CU3Ec_U3CStartU3Eb__4_32_mFD25E4E97F15A04940BDB3C446A4E97A0C5BE5D0,
	U3CU3Ec_U3CStartU3Eb__4_33_m19E2C1ED3DED1579C9218CEB57C37D2529CE823F,
	U3CU3Ec_U3CStartU3Eb__4_34_mD88F96C2B5B0940A28659D2B1E1676D21B2AE759,
	U3CU3Ec_U3CStartU3Eb__4_35_m1B3EFDC03F7AC2D0E0E9C9BB3B71081429218AD1,
	U3CU3Ec_U3CStartU3Eb__4_36_m564CA6A3D7A64C5280E9B6104D2AF37E87973B58,
	U3CU3Ec_U3CStartU3Eb__4_37_m560267CE648219A8AECEE043C3F9FC3E079C5265,
	U3CU3Ec_U3C_cctorU3Eb__9_0_m2D76F808C9D9F56C3D5D171CC398D391991F1B1A,
	OVRGrabbable_get_allowOffhandGrab_mC2FAF00A0F5A00E7C2C7DFF97E9907B808ADC5CC,
	OVRGrabbable_get_isGrabbed_m3BF5FF2FB70AA28D0E65769BFFF41959BC6C6C68,
	OVRGrabbable_get_snapPosition_m2A6031A4E49B5D116ED097A20B71D3F854819297,
	OVRGrabbable_get_snapOrientation_m365EDF846A5BA54DB9DF01A8B5567D2384FA46DD,
	OVRGrabbable_get_snapOffset_m56FBF4E8C0CE1E7E8EC6377779E38E9F7A2234A2,
	OVRGrabbable_get_grabbedBy_m6C2AEA29FE82B1865CB18A013F1579812DEF4AB9,
	OVRGrabbable_get_grabbedTransform_m94686C63DE14C02F9AC3559DB9B92A744FB33B4A,
	OVRGrabbable_get_grabbedRigidbody_m631EE15156D1DB1FF1D5E554260B8A6BB8E03728,
	OVRGrabbable_get_grabPoints_mC82FAC26EA82EB722BE4E5F5CFEE77717FB0176E,
	OVRGrabbable_GrabBegin_m7AFD3090290332E539082D33666E168EAD3054AD,
	OVRGrabbable_GrabEnd_mAD0DC368D97ADD4C5802CF6BFE451C8B59753FDC,
	OVRGrabbable_Awake_m4891E7203A33F0E2E94D601B9F3631A7AA7260FE,
	OVRGrabbable_Start_mC86B606A4900FDEBCF9FEBF1B28CB72B7AA9F837,
	OVRGrabbable_OnDestroy_mB96B3FE259306CA255D7402DB4DFCCD8927D44D9,
	OVRGrabbable__ctor_m6347A9620A021FE6BAD9DB69BCF759423B32362A,
	OVRGrabber_get_grabbedObject_m4219D0E00D5E1C409303E1B40464A26C6330E394,
	OVRGrabber_ForceRelease_m13887812BE7C69B9D79C4508FB23E62570C90FE0,
	OVRGrabber_Awake_m59BB1D586F051A396A914EB68576BAD06700B7C8,
	OVRGrabber_Start_mF5630EC0CF0E4E658724AF86DA7DC837EBDCBE09,
	OVRGrabber_FixedUpdate_m76EFC8B20BF0AFE48FBC10928C4B55FE06D540F9,
	OVRGrabber_OnUpdatedAnchors_mD348059D23379A2F1C4F59FA1E7B0DC01B526FDA,
	OVRGrabber_OnDestroy_m0733C0D4C4141B911ADBDFEE368F862A414FEA6D,
	OVRGrabber_OnTriggerEnter_m72BE0FEB8152CE41253E7B9135E0DB3C6833B4A9,
	OVRGrabber_OnTriggerExit_m4B2F1D525E42F7B4E094EA52D5A040A76F06E736,
	OVRGrabber_CheckForGrabOrRelease_m92381B223CA0D3180A874B475B5A54B937F84256,
	OVRGrabber_GrabBegin_m2E85C86F50E7D50CCBE85BACFF7D9D117282D782,
	OVRGrabber_MoveGrabbedObject_m553A0633F56DBBB05A82306C5645AB084ADE5F02,
	OVRGrabber_GrabEnd_mE19867FEA6C93593536CCD73884CAA68116542D3,
	OVRGrabber_GrabbableRelease_m3A62FBADABAE37F8806980E4C41AEDC885CC2157,
	OVRGrabber_GrabVolumeEnable_m623BB5AA6E5DCFB34235B5015A3A43AEE36120B7,
	OVRGrabber_OffhandGrabbed_m4408F0D6B24B6AC8CBE2C818A69D8B941B760146,
	OVRGrabber__ctor_mC8F506802658928681DC80ADD3529E7639417F6E,
	OVRGrabber_U3CAwakeU3Eb__21_0_m6A86F16E01E7EA802BB65AF0F6EE76F81977BA0B,
	OVRGridCube_Update_m19D35E72AB4F4A30EF9599DF39A5BA96DEB64EE6,
	OVRGridCube_SetOVRCameraController_m0A001667BBBD176CB26CEB8C649406BDCB9FB9E7,
	OVRGridCube_UpdateCubeGrid_mCD5FB9C2C4C4DAB34F6C2D3E6B343ED95C953CDD,
	OVRGridCube_CreateCubeGrid_m6AD79AB85264F3CD0E1AAE0EFCD951E1010AE197,
	OVRGridCube_CubeGridSwitchColor_m1E901DA50645413952C41D02CECEC1A222438482,
	OVRGridCube__ctor_m9BE320D7B11F21A4D7C58D57AE2DF81CD201D7E2,
	OVRModeParms_Start_m4B54385D9DBBA6347A51EAAC9733BA642907D7AE,
	OVRModeParms_Update_mC642AE2C179F57F4B2BDE50C8CF076CA2E58E268,
	OVRModeParms_TestPowerStateMode_mF0CA16420A77546B65B08FD49765D0FD7633AE6D,
	OVRModeParms__ctor_m0EB897AFFA827582D4DB25CA7ED2EB48B2FC6A5B,
	OVRMonoscopic_Update_mBC2839D9A5847CADF0B57212F0D4947CFCE4B1A4,
	OVRMonoscopic__ctor_m4F98F7F01126079533D6B651982E5C67DBF39D30,
	OVRNetwork__ctor_m2AA9DA154F5E99439220D84A7389B61448CF097A,
	FrameHeader_ToBytes_m713EC3931B082BE4997B3EA49576AEE3064A049D_AdjustorThunk,
	FrameHeader_FromBytes_mA9EF9C7F09AE317D7677A332E137875807D1D107,
	OVRNetworkTcpServer_StartListening_mD8982C918E324E9F14A8AC2C9BC971086FB2B04A,
	OVRNetworkTcpServer_StopListening_m2611132C9E521EF2EFA486A4F5C6108DC64DFB64,
	OVRNetworkTcpServer_DoAcceptTcpClientCallback_m8A9A32947442526EE384AD00F9768F8D542B125F,
	OVRNetworkTcpServer_HasConnectedClient_mAD81EBB4C83858DE407EE6F83E06F66BD6D11EF3,
	OVRNetworkTcpServer_Broadcast_mA9D5DE85EAF080A289113B9914B7D8C26CDE5270,
	OVRNetworkTcpServer_DoWriteDataCallback_m945ABCF09440ADCE992D461FB26865C6EBE5E8FA,
	OVRNetworkTcpServer__ctor_m7924C6AA73697D8099352CCCD3AACCBFB084ADDA,
	OVRNetworkTcpClient_get_connectionState_m52768313BFEBD74F5DF77075FF9A4B058C2C86E7,
	OVRNetworkTcpClient_get_Connected_m42E1340F16CFB7F890006F48A451563158091DB8,
	OVRNetworkTcpClient_Connect_m0FCFE6B0BE2DEAB605B62FB691F9D51749AB194D,
	OVRNetworkTcpClient_ConnectCallback_mE00816DACEB5EB9EFB6DAF6E9ED3D8104F5BE125,
	OVRNetworkTcpClient_Disconnect_m8C6479AFCD432B12EFD1FACA455E130CEF9721B3,
	OVRNetworkTcpClient_Tick_m52CE1A261F9114161331199D3BB2C6705800EFAF,
	OVRNetworkTcpClient_OnReadDataCallback_m9D78F8D5F325CEAFF34A00BE8E77FCD982119D4B,
	OVRNetworkTcpClient__ctor_mEBAEB62733442322931EA30FD14AAA3761BAA2F5,
	OVRPlayerController_add_TransformUpdated_m7FB492C719A139F5DCAA54F73C5D9C55E7860CC7,
	OVRPlayerController_remove_TransformUpdated_mDE57809723250264926FCC11FCEAAFC4A1650670,
	OVRPlayerController_add_CameraUpdated_mBB7BC1C27F1179AA2465FA294E4A8996E89D62B4,
	OVRPlayerController_remove_CameraUpdated_m3BA83C077B908AA60012292151A883136DB5729F,
	OVRPlayerController_add_PreCharacterMove_m30B863614DA426239E3D7318E3679D5220B15BE7,
	OVRPlayerController_remove_PreCharacterMove_m0A090CCDAAA57169D85E3500B4730C5F0D45DDC8,
	OVRPlayerController_get_InitialYRotation_mE0C203F34B717F860815227B2A74CDCA91DE035F,
	OVRPlayerController_set_InitialYRotation_mC5C6D37D3BB533E1B1692D05402795A17A33317F,
	OVRPlayerController_Start_m7380017B83EE9DB4E387DCE98AF1474F923D5CB1,
	OVRPlayerController_Awake_m3A07A95C186DC9B855921C6CB4C67376D7F88326,
	OVRPlayerController_OnEnable_mF4D652F936AE2CBE20CAABDF3F5501799C317A89,
	OVRPlayerController_OnDisable_mC68609D83565FB4A974607B48D8472BBB191255E,
	OVRPlayerController_Update_m308D263AF0287713D7B2F7D0126F747B610811DE,
	OVRPlayerController_UpdateController_m3CA68419620144497EF2AA0883640508C882D3E7,
	OVRPlayerController_UpdateMovement_mD61A904F36D31B1275DA8FB8C52D4085AEB7A772,
	OVRPlayerController_UpdateTransform_m61345C2D7687B35705D23A6A5038A60884DED4A5,
	OVRPlayerController_Jump_m41E169973AB02CDA441E802816D4783A242BD26B,
	OVRPlayerController_Stop_m572095671886993EC9C138AF9A11876A48D2E601,
	OVRPlayerController_GetMoveScaleMultiplier_m6553184B97FA79A209B4624689BC2E3CFEBEFBF6,
	OVRPlayerController_SetMoveScaleMultiplier_mF61F60033BEA615E3037F5FEBDA940D0D5510289,
	OVRPlayerController_GetRotationScaleMultiplier_m5433E3B5DA9CC5C9821A3D3E898417F63BD654B6,
	OVRPlayerController_SetRotationScaleMultiplier_m094085BCC6908B73D9DDD843CEA94A8FA5ACE4AE,
	OVRPlayerController_GetSkipMouseRotation_m08D044C2BF4C26B2B0748B5108D404F864BE9302,
	OVRPlayerController_SetSkipMouseRotation_mF644D2CF348FE8C9BDCC0A7C347D9D280E1DF01C,
	OVRPlayerController_GetHaltUpdateMovement_mFF11F1BF3B54FE5DA5495BB29A7561EB486F67F2,
	OVRPlayerController_SetHaltUpdateMovement_mFF5235DA3AEA17C76F89ED5FFE6299129170E1FB,
	OVRPlayerController_ResetOrientation_m05C6C2BB5447BBC1D1C090DE51D2CA4E511FE26B,
	OVRPlayerController__ctor_m4018AF8A4C8BD3E12AE2E6EA664BAE8FC56B670A,
	OVRProgressIndicator_Awake_mDE3A794E2C1959404652188736B2621935DE7268,
	OVRProgressIndicator_Update_m1CC97404BA2292A8B4235D9EC834B7B0A54E94F7,
	OVRProgressIndicator__ctor_m510C2E7532E8035D2F1F2B057712007C2FB18ED8,
	OVRRaycaster__ctor_mD850E7353D9DEE7C841ABC66E921DD6216B4BC0D,
	OVRRaycaster_get_canvas_m464156C7DBF8339454FF639456BAAA837FE87F91,
	OVRRaycaster_get_eventCamera_m97088446BED642AF8DDFE9E6C734B2342DDDFF40,
	OVRRaycaster_get_sortOrderPriority_mEEE6EFD0458D36C7978A0E5B25D44D81AED38F02,
	OVRRaycaster_Start_mABF81E3203CAEBA6B96B16B8285B57C393EA7923,
	OVRRaycaster_Raycast_m5D74C042C864EECEFAFD73B0E2128AE1004216C9,
	OVRRaycaster_Raycast_m536FEA11DD00032FEC17360E98F0C1A08753D4BB,
	OVRRaycaster_RaycastPointer_m199AD4DB1E3196EEFE40E6131784F07A653054B6,
	OVRRaycaster_GraphicRaycast_mC5BDDA993E180ED35C99D2DC84808C0263842A4A,
	OVRRaycaster_GetScreenPosition_mF7869ECB0131A1CBEBFFDFF3B097BB893ABD9E7F,
	OVRRaycaster_RayIntersectsRectTransform_m24F57D2FE1D24CB6822ABE334CC51200831A8FBA,
	OVRRaycaster_IsFocussed_mCAB4168CBFCBCE9B2FC8FD90F9E90A4E65138FC0,
	OVRRaycaster_OnPointerEnter_mEE6C19AF42E31D3B0FDD0A19538AAB428A91AE7D,
	OVRRaycaster__cctor_m43D7831ED5837C41626416EA61C979938B12014E,
	U3CU3Ec__cctor_m46230A0043C6D1E26268CF37369EC62ED883CC4A,
	U3CU3Ec__ctor_m3D623AAF6A08ADB17F619D78E78045052622C453,
	U3CU3Ec_U3CGraphicRaycastU3Eb__16_0_m7D9750E42FEDF2909AA69A17E1A5B4F46B1F51A6,
	OVRResetOrientation_Update_mA4EE6BA6C640BE6CE860A64E62F3AB261B872CCE,
	OVRResetOrientation__ctor_m82F132C8C20E18E9713975246DA3062A67A68C67,
	OVRSceneSampleController_Awake_mFCF063EC14D56C6B74C76B1DDEFF4A2BBCEDC47C,
	OVRSceneSampleController_Start_m53CE4C890802F57B7195A8BD1520CAC70A3545FB,
	OVRSceneSampleController_Update_mDF4E1489655FDBB35E95D43B7FAE022ED03F20DD,
	OVRSceneSampleController_UpdateVisionMode_mAF8C5FB6A16121C3541477FCAA5ADFE1C0337C5C,
	OVRSceneSampleController_UpdateSpeedAndRotationScaleMultiplier_m6469A69A07A8C85F97B85FB8B3E14F86E3542C6A,
	OVRSceneSampleController_UpdateRecenterPose_m261286E0913021DF752B89DABD2E3754A17D6658,
	OVRSceneSampleController__ctor_mD894A5832A674830DF052BF8F1BF85BEC0F77882,
	OVRScreenFade_get_currentAlpha_m0F6CC62AAC351079AECB9193BE101F981369CE66,
	OVRScreenFade_set_currentAlpha_m3D36BAFC6225A4BB4A63A527F9D0BD9E6E354544,
	OVRScreenFade_Awake_m80ACAAB51B0549932312EBC8EB51AA736792E2D3,
	OVRScreenFade_FadeOut_m05D8D9CC82D1927E6F6B7D144F03DE7BFCA25E2A,
	OVRScreenFade_OnLevelFinishedLoading_m05F0196AF2A19E7F50DF8458F22131977B4ACEAA,
	OVRScreenFade_Start_m86A416E0A43ED2A3094BF9FF636F761D1E6E843E,
	OVRScreenFade_OnEnable_mF86405A64481B49570C900AAF567D954B43B7320,
	OVRScreenFade_OnDestroy_m75D4EB9F8F27E175CD11EB00D92F5985681302DD,
	OVRScreenFade_SetUIFade_m482FAAB0B460580CE1A2CFAB41C64DD0A333C904,
	OVRScreenFade_SetFadeLevel_m8106A6BBE990FB28506FB239F625CFCB8860FCBC,
	OVRScreenFade_Fade_m909418F8E06618AC04CA0B89662957D52598B61C,
	OVRScreenFade_SetMaterialAlpha_mE60E60A758D2D7383B111800E9E19BECA8245333,
	OVRScreenFade__ctor_m856A3A9B6367CD22BB12BBDC3327DDF38A629953,
	U3CFadeU3Ed__21__ctor_m59EF26E769BE48B29A99B7D55B0208020F8BD6A7,
	U3CFadeU3Ed__21_System_IDisposable_Dispose_mC68EB95C937056AB51EB95E0D640E1B737FA1A55,
	U3CFadeU3Ed__21_MoveNext_m805303CBD5E0A4C370122EE0DA0E3D0DFE0B0C72,
	U3CFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B48721ECD0FED54D7FF40FE201DC80FF44E7059,
	U3CFadeU3Ed__21_System_Collections_IEnumerator_Reset_m43BBE93639312543D26E1AC1E1BF16697824EFEE,
	U3CFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m849A5E317F2D48B85A30D0E49C5B48950BFC871F,
	OVRSystemPerfMetrics__ctor_m3EFE4B85D0CA975DB52208DCF12C5C630773DD95,
	PerfMetrics_ToJSON_mB97EC503CBD1F3BAD45E1638F8AECA64CD6995A0,
	PerfMetrics_LoadFromJSON_mFC15BBD60F0AD7551481D2C17E969993A3144F01,
	PerfMetrics__ctor_mDEB091146BA8F9740FC19ABFBB17B69653FA3443,
	OVRSystemPerfMetricsTcpServer_OnEnable_m06489B3E3133525FE402D1B8B7A30EB563F2115B,
	OVRSystemPerfMetricsTcpServer_OnDisable_mDAF5DAB99832B63EB9F409935443DDE1BB375FCA,
	OVRSystemPerfMetricsTcpServer_Update_m745F0F6CCD7F5B1A7E0D2C04CFF71C5609A2F2F0,
	OVRSystemPerfMetricsTcpServer_GatherPerfMetrics_m11D87026A8E82BFCAE30C3F57A6DB47DC5557F82,
	OVRSystemPerfMetricsTcpServer__ctor_mDEAD1FF3D67B14B6361006CED0D36F0D6C1E3583,
	OVRSystemPerfMetricsTcpServer__cctor_mF0F4B490B8DC68D51B39ED6B0A237792BA1F29C3,
	OVRWaitCursor_Update_mA16FEAA810EA8C5B3CC255CF756F3E5A3D874CE2,
	OVRWaitCursor__ctor_m3CA58B0C8E39F3156E2F3FC64731CD1C255BDD32,
	NULL,
	JSONNode_get_Item_m44C8808E9BE8D31204D5F604C5EAD0B32A0D7416,
	JSONNode_set_Item_m38D0C67E569B9C859F7DBD471B62D568C1091DAF,
	JSONNode_get_Item_mAD2331FE3B68F3F01DDAC26DBF02DE74415A11F4,
	JSONNode_set_Item_m6E293178FEDEED2298A1AD353C28FB1D75391D2A,
	JSONNode_get_Value_mFA2CDE6481441C57E7F040BC0DDEAD7B41D5B62C,
	JSONNode_set_Value_m61C34C8810D5ABA5DF0E4EE4BEFF709470CAF5B7,
	JSONNode_get_Count_m4AD71226990E97C58120702FD921265AABFF7A54,
	JSONNode_get_IsNumber_m73C89526289211C322AD679E074D783CE5389402,
	JSONNode_get_IsString_mFF63F2442E9474F6B71C5604F847977186B7EEF7,
	JSONNode_get_IsBoolean_m303DB76AF4F5D9BDBA0C0B979592319BF0B20840,
	JSONNode_get_IsNull_mC146FE0FE815586BB8A7830C5A89202C630EF2D9,
	JSONNode_get_IsArray_m7585226F59D4FB43F06BC2BD5F1955DA2C050986,
	JSONNode_get_IsObject_m8121A5AFAEFC48AAEF02B1CBF109BB8F62FD71B1,
	JSONNode_get_Inline_m0924BB18C8CE1118B230CEF4331BCD7E2891B2F0,
	JSONNode_set_Inline_m58C1B28FBEC6D9D7C6F4919E104F11C6D87C8C6F,
	JSONNode_Add_m223852E9B0DE582539418C5857E15D3FCC1801C4,
	JSONNode_Add_m7D015A7E87F35ACC958EAD874D373071E14D7807,
	JSONNode_Remove_mD2BAF3B09D47B59D123AA03A44AF702D8CD91965,
	JSONNode_Remove_m93DEB66BEE51BD6A79E214314721D8214F24B126,
	JSONNode_Remove_m698972B1717A2E3FB15CCDE19F427000AA36A70E,
	JSONNode_get_Children_mB3EF6363C77258ACC557D4EA04AB0BEC42A6E72A,
	JSONNode_get_DeepChildren_mD820CD9B7D702F0FAA5B8839E89E1A71CBFB7036,
	JSONNode_ToString_mB5C1001E9836E9AA77B0D4B504CED2B76BFA46D8,
	JSONNode_ToString_m65DA914D4D6A248E14533FE8ADDB2DC0A9816F65,
	NULL,
	NULL,
	JSONNode_get_Linq_mD9AD46F8C7892412122BD599A47754297258F8DF,
	JSONNode_get_Keys_m695FA7DE7A02CD1E3CB7A461D182438FCFAF048D,
	JSONNode_get_Values_mA739B317C804DDDC1275AD88959D88184C0AB5DF,
	JSONNode_get_AsDouble_m45487E41476CB62F80C39CB6EFEFCADF4CD5F7A6,
	JSONNode_set_AsDouble_m40254B27734C59675426F7B8693DF5E719861843,
	JSONNode_get_AsInt_mB0064CA8B5E302159DC64DD51B6C11CC6882C488,
	JSONNode_set_AsInt_m76C13AD5860B690F143F06812519F800F0172D81,
	JSONNode_get_AsFloat_m5A705824CB38952BAA15E8DDAF36C0E6D70BCDDE,
	JSONNode_set_AsFloat_m7B74551DC193C99F74E01F550CBCF5A9765BACEA,
	JSONNode_get_AsBool_mF0D2A3EE7D37FEE24B2B171785C1D5B6DC979F38,
	JSONNode_set_AsBool_m8A89F588DDBBF1232886BB8CC2FA8CB896428E56,
	JSONNode_get_AsLong_mE4E3D9F6A545573CF809942F7CB4F3A6210B46F6,
	JSONNode_set_AsLong_mDA7BF126ED30F8A2480999F871179AB1700F51C1,
	JSONNode_get_AsArray_mEE9ECAC1968135AE8CB9A6DE5879364090A0A573,
	JSONNode_get_AsObject_mEEE08E5EA28BE8ABAE70C841981CB501B8A2C7F1,
	JSONNode_op_Implicit_m7234DD15D996E49393ADE3DA0D0EB913FF345A7D,
	JSONNode_op_Implicit_mA5A307A11DC975BB4A11010A9D1E815CE5EDDA4E,
	JSONNode_op_Implicit_m4AF6254D9A917920B721AD1EA469D10802621435,
	JSONNode_op_Implicit_m42A71F942B2725F3770F04B64210D63BC6549C58,
	JSONNode_op_Implicit_m63308FD45AF08A98958D1B6E1C87A0DF87C500B6,
	JSONNode_op_Implicit_mF6DF08DC23BAC07226A4075DBA21AEF0E425CAC7,
	JSONNode_op_Implicit_m0B96B2BF6B54BD74897AC5A2D44215334D81DB8D,
	JSONNode_op_Implicit_mFDA6F6453965BAB49DD9A47D6947E3F0A8F27A96,
	JSONNode_op_Implicit_mF45ECEFCD896422C97B72A64C1EA5D7A5B16E6E1,
	JSONNode_op_Implicit_mF72089C945AFB2AB7D915412D372A08339C130E0,
	JSONNode_op_Implicit_mDF68964291FAAACA9F4D7E91AD320A07EF10C5B3,
	JSONNode_op_Implicit_m2CE1859CB8FA978C9FC4C8A1C588DF966FA34ED8,
	JSONNode_op_Implicit_m66441829AC8F8A08132800B68C9AC9E29008B741,
	JSONNode_op_Equality_mC6FB160F574A7A7E8FA30225DC5FE4F3D4E18A82,
	JSONNode_op_Inequality_mE1CC88332375669AD199A23DF192E294044C1FFC,
	JSONNode_Equals_m5F33EB5E56535C5273AE8EB2D0D6C07A37426536,
	JSONNode_GetHashCode_mBB90468BDBE75634838A69867F1275D16742FFC4,
	JSONNode_get_EscapeBuilder_m63F032467D6F8FFA5DB70BB52953D271BEEAD786,
	JSONNode_Escape_mA4E3E754D5A032C46A7C89C53F31BE51F4B2A11C,
	JSONNode_ParseElement_mDCCE725DBE03152408074991DA9C39E9354531DC,
	JSONNode_Parse_m832D509168C00484CA22C19B6D8B86DF59CFD6A7,
	JSONNode_GetContainer_mD04109B86D61D53638A881BB3AAA6FA2809C213E,
	JSONNode_op_Implicit_m1AA0471E2312C655367AF20A2E123C608C0A1200,
	JSONNode_op_Implicit_mD6A80B1CA2B86DDCF9158ECD3490B7CF05D494E8,
	JSONNode_op_Implicit_m1A5115F2F2E5D8DFED0CF4DF95E06FFA34377A35,
	JSONNode_op_Implicit_mA666C49A2B2440A4EC3DF6108082C4D4D4F28759,
	JSONNode_op_Implicit_m0EA8CC32933C1F2FDE15CD654FBD107B9C8DAEEA,
	JSONNode_op_Implicit_mF44569AA7DD35AEBFEB31A23B0BA008F295259DC,
	JSONNode_op_Implicit_m1FAA8E5278CFE1DF35E9B7F273425A8347615390,
	JSONNode_op_Implicit_m1D2177CB401073D9B7B0E36B6DDE222C9183ECA9,
	JSONNode_op_Implicit_mBB184E4D762C26E377EFFE2554A966C7CC19250C,
	JSONNode_op_Implicit_m2068B5975A2907FC1990D61348A9069DB3FD1BF8,
	JSONNode_op_Implicit_mFDC236577CA6C4152ED194750340CE46EC41B16A,
	JSONNode_op_Implicit_m824F14AEFFB463D28E8593A8E2B6F7C6AF9A668D,
	JSONNode_ReadVector2_m89EC5FA944988E40960CBDF49E082394C4B5FF48,
	JSONNode_ReadVector2_mD3137E919A1419CDF5F6D3A3CFFE05096CC145B9,
	JSONNode_ReadVector2_m6C7B881A666095A3BE3EB3260524C94CA9604290,
	JSONNode_WriteVector2_mCD0BCFE4A6C5E0BCDCAA9F8E700B1BFEB7DFC9F4,
	JSONNode_ReadVector3_mB07D8F0603DCB9AC4709F9BAC84731E156C3882D,
	JSONNode_ReadVector3_mC7D09C06D738A7B823EA348747CFCB12675F8B1E,
	JSONNode_ReadVector3_m13C11E7B63B63E9C4A31CB257AD5703E3502B578,
	JSONNode_WriteVector3_mE84448338B6ED90F950539883F1586465C841818,
	JSONNode_ReadVector4_m414C7FDC4BA7A25C61AD296DA2EEAB004BE05417,
	JSONNode_ReadVector4_mEE487157F980579A92E8F98019F6F17EF28DCE2C,
	JSONNode_WriteVector4_m8C09A205FEF9F131DF0066E2B56E44E684041EB0,
	JSONNode_ReadQuaternion_m79D5410344207A4DF536D4EF784D5C68DF7808E5,
	JSONNode_ReadQuaternion_mA44E0C5E2C3D1E5AC0CB75A85766D5CD6407304A,
	JSONNode_WriteQuaternion_mED82ED486FEDA1468D8D8E1D65504B3CE6D75973,
	JSONNode_ReadRect_m37FF16C0B6C4D8C05FF5D4E59ADD52E17011279B,
	JSONNode_ReadRect_mE2059C2DBA39876F7A978998BE08A532F260218C,
	JSONNode_WriteRect_m28194F89DF767195D296DFF318799314068FE847,
	JSONNode_ReadRectOffset_m58BF23AC3C70F99D67393D6A5961CE06533E4F0F,
	JSONNode_ReadRectOffset_m72AD6FD0D2ECD30DBDA6449D71354E248E28284B,
	JSONNode_WriteRectOffset_mD5084D28F9708BC670F474CF92BDFE99656D50AC,
	JSONNode_ReadMatrix_m81B1AA274B2B5315AC023F60A1802492B84EDA29,
	JSONNode_WriteMatrix_m98E8E5997B1230719BE0E1C16754803AE23FF91A,
	JSONNode__ctor_mCE7840687B8368CF97DAF82FF10AE6161C0C0812,
	JSONNode__cctor_mF74ED433D9180175F7CFAF62778F8010D5D7573B,
	Enumerator_get_IsValid_mE123BBA83C796F69D735D0B2FCC7B181A02A0EA1_AdjustorThunk,
	Enumerator__ctor_mD261FA63B0FD69D40B9DD906CB9A229937A725E5_AdjustorThunk,
	Enumerator__ctor_m387D645DBA722C95B907D4A3A34FFA51F952D9AF_AdjustorThunk,
	Enumerator_get_Current_mD3F62AE5476416BD4011A26AFDE7C2296532862B_AdjustorThunk,
	Enumerator_MoveNext_m811DA47435A443DD4C7046FAE60CF3395FDDA853_AdjustorThunk,
	ValueEnumerator__ctor_m28DA7821F52F4026F144758562461E34F274E849_AdjustorThunk,
	ValueEnumerator__ctor_m44BE054B01BC96EED1BFEDDB743C370A1C25CAA9_AdjustorThunk,
	ValueEnumerator__ctor_mB8BD50FEAC6DB9F897FA852573D5F0C64716EA8F_AdjustorThunk,
	ValueEnumerator_get_Current_mA038DA91BDADAC416EEC79DC94310383CA9F6E5C_AdjustorThunk,
	ValueEnumerator_MoveNext_m703E2108777E10816D6A634F16A130B6DE198804_AdjustorThunk,
	ValueEnumerator_GetEnumerator_m27EF6AEEEC34F5ED08DDB69429DF6037DA52C24E_AdjustorThunk,
	KeyEnumerator__ctor_m6D2B45677B312982618C71DF0A308F4574D73360_AdjustorThunk,
	KeyEnumerator__ctor_mB341FFA8F3E36A2663FF0E0E23D2AE5012905FDB_AdjustorThunk,
	KeyEnumerator__ctor_m23A9E209ED858A69A4E38119CED5A1B1132A4222_AdjustorThunk,
	KeyEnumerator_get_Current_m0070CFDA79C67097DC93DBFA160B11E2A0218178_AdjustorThunk,
	KeyEnumerator_MoveNext_m1030560B49414A9CDB6FF4693EE1B2CD43616C64_AdjustorThunk,
	KeyEnumerator_GetEnumerator_mB1C04C3864CEACBD8B98097A8FFD06B9507F0D15_AdjustorThunk,
	LinqEnumerator__ctor_mB053B605AE3DE68104D687E22FEA9503BD72B2CA,
	LinqEnumerator_get_Current_m8377DC764296AECC3FCF182C65BEF3FA27879CD1,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m71E85F6C0D1DDB0A80958BB88FFFD3BAE93B03C8,
	LinqEnumerator_MoveNext_m7B47174A4F7BDD3DD65E4D967F156E43C885A90D,
	LinqEnumerator_Dispose_mBB69FA50D51E55484112B55DDDE34EB33B4E4D38,
	LinqEnumerator_GetEnumerator_mD5E1A0AA4CBAF7A6CE62024EEC64F98BC65EE28C,
	LinqEnumerator_Reset_m7D4F66C18FB1D604F5961298C7B8E6452EA8DF18,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m1668D6337CA4B5CB616FE1B996578C4D2B7D6F6A,
	U3Cget_ChildrenU3Ed__40__ctor_mAD6CD6872580F578224D94CBB299BCF123BB93C1,
	U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m92248ADCB1ED01C78F70E82DF1BB9CD732369AC5,
	U3Cget_ChildrenU3Ed__40_MoveNext_m70BB8162EE701BEE9F924CE91547CA3E4FA9E2C4,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_mF82B0132E2F5BA04FF1C2D6FB1FCE4A2095E7ABE,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_mAA068B65006A01891986B8C586A25A3B56D95797,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_mA4CC3694A3AC45D6125C3D3BFFA1C32C31F15E4E,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mAC929C6443AF52C4AA5003AFC9F9C23DA76D05DD,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m235FA3F4F024D79664561DFBB053DC9F3FB3CE16,
	U3Cget_DeepChildrenU3Ed__42__ctor_m1E734A5A3B49DB0835C452FD8B80B4804A73C763,
	U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_mB86CF5FA23A85902E1079B4CE4F0A28D8AC35506,
	U3Cget_DeepChildrenU3Ed__42_MoveNext_m471E7C3CC88185A5F91EEC8C3739D586479B1DC0,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_m9A14EEF2617277D0CC69BB883E9DFED91325D082,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_m27C9FF86FA820950A232E20C1B78215AA26AA4D1,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_mF35F302F8A362C821BCCF27512EA356E932B24C3,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_mDB0C93551DDE6AA31442E328E07565C13DDE31AB,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_m8344EC56018958D1876E7C639C5914A3A4EC985D,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mB6B41E8663270F9CE14B58BDB1F04C4E0DE07EEC,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB4B8437C9D0AFC6E89E0CA69D3D4D4799F644073,
	JSONArray_get_Inline_mAF078AEF285D10A110C1F0A39AE86448B97CA723,
	JSONArray_set_Inline_m40C0873AF1FE5B71A192AF177D72593D4DF826A3,
	JSONArray_get_Tag_m42FB5B0477396B81582E9559EFCCBDF9C35951D5,
	JSONArray_get_IsArray_mC889F3B86EC345120EF71DEED6E1C444D543967C,
	JSONArray_GetEnumerator_m9CD59C8DE819F59571B02F90904C0A6A84A9F9F7,
	JSONArray_get_Item_m96F1F0DC6003D15C1BDC6E725E13228F079B94E8,
	JSONArray_set_Item_m9CCAD03B83783F5F7BB6617AC0E691FF375EDA67,
	JSONArray_get_Item_mE00DCE6138678487E5F7C0A9C766CF41D83C4B56,
	JSONArray_set_Item_mF58D55AC31388B1ACA7FF55B1C918D0B3D4C1C52,
	JSONArray_get_Count_m9AD3FD0D8CC0ACA09D5BD9B7DA5876FF5361307E,
	JSONArray_Add_m4D865F50D473E8D19C42F97721A5EC7E073B6380,
	JSONArray_Remove_mBA01E42BB17B3B27933F5E8278B3135C92AEF3AE,
	JSONArray_Remove_mDE68BB43ABDC9CB352EA39D0890853CF33453999,
	JSONArray_get_Children_m8496DAA6D259D5C7E9CE43BF5895A4C5B7CF853C,
	JSONArray_WriteToStringBuilder_m83F2719DF4541A8EA401AA31FA3DE1D82B1D1404,
	JSONArray__ctor_m84F1E1EC0F85EFF095C178CF992D94DAD7A20259,
	U3Cget_ChildrenU3Ed__22__ctor_m9AE3E99927C10B7209F13A69BC18CEAA37D33AF2,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mE2D7EA5B1A304C606FA1E15BBFD971F0D77F4012,
	U3Cget_ChildrenU3Ed__22_MoveNext_m57F7D3D844E4C307D8F15771BB308AAE33154FAF,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m45275F57BBE360D9746E494CD833F38DE18406F9,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_m77CC70426E1BE7CAD3FA9D65D1DA4F318DE36D28,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m0B28E39C83A7028AADB0629626919F907D98E96D,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m8F4D15331C631EF8595F0CA20DF905A882BAFA75,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mAF84935A98A444C0727D416D561C318A3BD7D81E,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m5067564BE513F6A53E861581819EC1369E14947C,
	JSONObject_get_Inline_mC1760B920AB4B95ABA2436237103AE18450EE822,
	JSONObject_set_Inline_m6A232439E4C175753F9D6B04D1D81688B778EDAD,
	JSONObject_get_Tag_mDF7FEBED36F50B0EEB51D6A5641DC9C8FDFBCE4A,
	JSONObject_get_IsObject_mD510E36DA5091AFA8347FAD189FCDF6575B5C555,
	JSONObject_GetEnumerator_mFC5B33B3DE809479C0D8CD6721D685DECB60680F,
	JSONObject_get_Item_m64F0403FFD193DE2EA5A7BE3D7E6E2EACC738BBB,
	JSONObject_set_Item_mDF3D2A2515CC5AB82CC2AC19EAF767AD103BFE99,
	JSONObject_get_Item_mB68C5D2CDEAC5B9D145468611C3221971BB9636D,
	JSONObject_set_Item_m9241F889D7A773B184165A85305CE4B458A86047,
	JSONObject_get_Count_m99D647A694A126773BAE03CFB23D7D4643BDDF93,
	JSONObject_Add_m2379E4B57F29A6A7FFC7B470CAA8930E8AA4D31E,
	JSONObject_Remove_mCD9E5C9DDBAE2EBBE034697152A184233ADDC80E,
	JSONObject_Remove_m908875BEC1ACFA58EE910C91DC2150224E782931,
	JSONObject_Remove_mC80963F65BB4803E89B79CC7DC07474EA99B0F16,
	JSONObject_get_Children_m83F4DA97C496533E579B9CC02C9E11A3BF30D9A4,
	JSONObject_WriteToStringBuilder_m5112F6C48FFA03E323F5C5B5AF9D577816E1FBDF,
	JSONObject__ctor_m150F04D7B1139BC04F600A8D1C8572310DA27EB4,
	U3CU3Ec__DisplayClass21_0__ctor_mF2D75E02D7F687BF83E0AE5D8C21CD24DE8C24FE,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_mAA6E310D01B9D1F2702034D23FFC286ED4DA49D8,
	U3Cget_ChildrenU3Ed__23__ctor_m761C1CC855D1A5DD65F0B3F5A6AAA59BCBC20BF7,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m5E329BCB7A2590EE790AFB8D060CCF6255499C1A,
	U3Cget_ChildrenU3Ed__23_MoveNext_m0557A7FE061E161FD92B34159500C00FD7115488,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m71659AE80BF7C55F9CB0F2522F226609AB9B15F5,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3COVRSimpleJSON_JSONNodeU3E_get_Current_m87165354C7AF46925B9975D1E782E91D57FF4C66,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m379784AB70A4695E82987816F0C7CF87209D2CF2,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m8581BF31291E50CE32B31469AB156A58E15BEF04,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3COVRSimpleJSON_JSONNodeU3E_GetEnumerator_mF763C9B0C7129679056EF542060364099D90868F,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m767871B41389012F2889D67BFF4EFCF08BEEB8B1,
	JSONString_get_Tag_mE5BA7132E89AE13F605134B5A4F3AE92B5F17CC1,
	JSONString_get_IsString_mBBBFA269B86A1ECECA9BAB6FAB5264F4C13C6E97,
	JSONString_GetEnumerator_m0771C20646D2DFC9BEA6F3DA22E60C4199A2DBD4,
	JSONString_get_Value_m355370C080DFEB63A09AC9B0FA1A2F851D5CC324,
	JSONString_set_Value_mFF6D420839FAB0B413C9AC1C278BC03F649E7AD3,
	JSONString__ctor_m4C57021CA53FF07A1ADFCDE7817F0A5EB1C645BE,
	JSONString_WriteToStringBuilder_m2026B3AC2462337284DF0F25EB3956A48E0B885D,
	JSONString_Equals_m38158581EB8283D9B9A55FFC4FB7AF8F1C660D1D,
	JSONString_GetHashCode_mFDA60DD6060654176B088E1DE944B329F12C79B8,
	JSONNumber_get_Tag_mE13CEBF2F06154FD2EB85BFEA5A4D2DB242FCEB0,
	JSONNumber_get_IsNumber_mA0D9672501245C9AE9B4AD6E6089B69BFCB34961,
	JSONNumber_GetEnumerator_mC15BACA3C6B8F656325EF4EF45F2C0268BECC8BE,
	JSONNumber_get_Value_mA44013E2EA65E059CDFCE5CEE52C4983F4A087D1,
	JSONNumber_set_Value_m7582A93899259AE9EA236C5CA4B25013F6271950,
	JSONNumber_get_AsDouble_mE62C503EB0C807A1368C2B334A4FDCE81CD812FA,
	JSONNumber_set_AsDouble_mF4963A2469A701264931075ADBCD14281E30131F,
	JSONNumber_get_AsLong_m5555868108391C3660DD945373FA779BF601949E,
	JSONNumber_set_AsLong_mC44836E8747ECC0344F56B1B921D4C959D5407D7,
	JSONNumber__ctor_m27B4556EFB6F0009935C440276FD70CF9EBCE891,
	JSONNumber__ctor_m86B496855043132F187D5A8B279E18F8557D8A15,
	JSONNumber_WriteToStringBuilder_m555012920C544B0B93893AB5E9562F99693B6981,
	JSONNumber_IsNumeric_m33E35EE7B944C73BBC320334970FFE811E0F29B9,
	JSONNumber_Equals_m4B21D4C3F2086AA46C7EF9B1963D265B54E54F18,
	JSONNumber_GetHashCode_mFCFAC73A33977CBB384F434AC7B744EFBDE4C177,
	JSONBool_get_Tag_mC563B972CCE147E1DEE64D4EB21122CBE6B965DF,
	JSONBool_get_IsBoolean_m9BEF919F560D6419A294C948AB5F6D41AEF362D8,
	JSONBool_GetEnumerator_m79C66C8F6B84C1469291E79D58932FBDA4ECC93C,
	JSONBool_get_Value_mA17B179F2AFA1BD846652BE66D839CF8BFB72AA9,
	JSONBool_set_Value_mD8702E0F9DA86E92B764D522BAB151C4266E7560,
	JSONBool_get_AsBool_m916D19CBB78F88DAA67F23940107ACECD136E8BF,
	JSONBool_set_AsBool_m98D346609202C2795A23213C89492E3F08312B05,
	JSONBool__ctor_m118638B3D19A93430034FF43B4ECE6ABF223DB76,
	JSONBool__ctor_mC14E6B634C72FD2D35647D24C21848C768F20270,
	JSONBool_WriteToStringBuilder_mA5F5B224213511F9FAB9DC710670D1C4BA507F60,
	JSONBool_Equals_m0845A60B76EF045468E02A7199D8421FE05B4C69,
	JSONBool_GetHashCode_mB7E0C3E291BFE38A03865CA5643972739E94F7D4,
	JSONNull_CreateOrGet_mDE760A7DCD5513E6DC4241FC534FA6C2AAB1E642,
	JSONNull__ctor_m3502BF126A8926B8784D591742425EB95C6563C9,
	JSONNull_get_Tag_mDB88127BA74CF00677AC4A644B91EDF11D3604C8,
	JSONNull_get_IsNull_mFBB993FBF500F81A082AF0468CE898100ECAA2B3,
	JSONNull_GetEnumerator_mBC3ACFCCB37FA9636CD7455447D0772684945D0B,
	JSONNull_get_Value_m99D3B008678BBCE9F4B141CD4237EC078B01CE0B,
	JSONNull_set_Value_mE30CC6719CCBB798A15284DC324AF3040667F824,
	JSONNull_get_AsBool_m2249E1308A275CE4D2CEC1118D21EA73E32B4D51,
	JSONNull_set_AsBool_m24BFBAE4FBB988822E8F062DA58A43476512D261,
	JSONNull_Equals_mC88F3FCEFF6E18A8F02566BAB438A89938DC7960,
	JSONNull_GetHashCode_m91672C1BF77B94CB79D361CD11348521CDF05183,
	JSONNull_WriteToStringBuilder_mF75F4C5C6DE6B124046564C36B4ABD5782ADE737,
	JSONNull__cctor_m2776B02040819D16A8329C3F74B6F3C8806C7DAD,
	JSONLazyCreator_get_Tag_mAADC5C7865835338A5461BC698D743F07D5F5D2B,
	JSONLazyCreator_GetEnumerator_mAC948A27B43CB02227B6880578C48F8D1BE2F929,
	JSONLazyCreator__ctor_mCFC9265D05453A136EF8814636DEC8E1B723347C,
	JSONLazyCreator__ctor_mD5E1018E27201444A9C37D79B96BC267FEDE765E,
	NULL,
	JSONLazyCreator_get_Item_mDF098D0C4D073C5057FC5883F05D2956322F2939,
	JSONLazyCreator_set_Item_mC254391476B81EE16DE3CD73AF85167B6A9A856B,
	JSONLazyCreator_get_Item_mFB6397062B4F12C2A3E271336784FE652CE4D132,
	JSONLazyCreator_set_Item_m6ADAEB6321383D581067AB35B6CCB6C266C0C760,
	JSONLazyCreator_Add_m474AB0D4A2C2527E1B92C215C92F6AC60C89C1DD,
	JSONLazyCreator_Add_m92289FA80C186C4908BE3209FC86056836603039,
	JSONLazyCreator_op_Equality_m1FE662C081F2C0B639E274A9313D6A8ED8A4B70B,
	JSONLazyCreator_op_Inequality_m619DBCA769060479E9944B4B2BBD73D2B538D45B,
	JSONLazyCreator_Equals_m1C147E2F6E2103726BAB70B71DC5C23B59B7CE72,
	JSONLazyCreator_GetHashCode_mF56F3BA22470B6B97E42BD58C3EF47E07EEA567D,
	JSONLazyCreator_get_AsInt_mF0338533F4A721F62FE286852320636F76CF75FD,
	JSONLazyCreator_set_AsInt_mF386C6C522FF4A5F68B96926898161C0F6F3D18D,
	JSONLazyCreator_get_AsFloat_mB785CDB3A3BC7EF9EC686AE95E885F22EE86D361,
	JSONLazyCreator_set_AsFloat_m82B4D141131E8EF56DCB5A4FEA13EFA01C8B23A8,
	JSONLazyCreator_get_AsDouble_mAD4401A132188578D1E91363BF35035AA4C7C4E3,
	JSONLazyCreator_set_AsDouble_m2ED433CB35199F0B73E76332BC995C8907CF1C14,
	JSONLazyCreator_get_AsLong_mDD08CF4E447F6F2773263AC40A7984B30FB5EDA6,
	JSONLazyCreator_set_AsLong_mA80BFA6C245B7FA84718A4A674230F328F58524B,
	JSONLazyCreator_get_AsBool_m69E6BFE69A8326EEA059BD0484B824772D5CD345,
	JSONLazyCreator_set_AsBool_mBD035BEDF122508AABC4ED493A7F09E2B2C5409E,
	JSONLazyCreator_get_AsArray_mB80673EEF5B90AF4EA632078CC54B94E5FF62BD7,
	JSONLazyCreator_get_AsObject_m8584284E55FDA11DB95C85A43ACA8E1BDCA5B8EB,
	JSONLazyCreator_WriteToStringBuilder_m3063F0DD56479E7CB9234F2A90A02189CD745FA8,
	JSON_Parse_mD85F2D33E54B9617F6CDB4720033DED2FC30F28E,
	_GetRecommendedRenderTargetSize__ctor_mC79DD5ECC62BF30F75BA4A11FC75C3AACDAA9603,
	_GetRecommendedRenderTargetSize_Invoke_m0A0C52AC07E9A885DCD3948D84F4E7743061D681,
	_GetRecommendedRenderTargetSize_BeginInvoke_mDD67D6B93AFCE852B0A76A64DD3C4B803C91B215,
	_GetRecommendedRenderTargetSize_EndInvoke_mEE6F2E5E6570EE5460180B465B8360E1DE35AD8E,
	_GetProjectionMatrix__ctor_mD719074A436075639F384534A71D52B719250025,
	_GetProjectionMatrix_Invoke_m8FC606E7AB49ACCCCD201CF692F8D38D367EB7AC,
	_GetProjectionMatrix_BeginInvoke_m8473D762C98EDB38C80E0A1B8A763C7AC87A671C,
	_GetProjectionMatrix_EndInvoke_m782AFFE1F530B3CDD13CEB74A3BFEB9BD87059E9,
	_GetProjectionRaw__ctor_m12AD08852797DF0F160C618D4778028F3B4B1646,
	_GetProjectionRaw_Invoke_mA18B089BD9ABFF6996658085D6E8A98E721EA065,
	_GetProjectionRaw_BeginInvoke_m831D8F4C9332B2178A6207FFC51330AA5F0FA4E9,
	_GetProjectionRaw_EndInvoke_m6D2B0D861B7C00604D762096ED05518219AF7A45,
	_ComputeDistortion__ctor_m7980FBA2A9BB3A6B148329F3ED63172FF9CE54E5,
	_ComputeDistortion_Invoke_mF3E334140059CCD109EFA8D04AFBE69378CE1278,
	_ComputeDistortion_BeginInvoke_mF476A019BB9CF8A39D7C3E65FCBE5110F22C7C64,
	_ComputeDistortion_EndInvoke_mF326A4397057E3817D01F7A143E9765C3F5A69E4,
	_GetEyeToHeadTransform__ctor_m2A367E6806C230446299281346B3734F88018443,
	_GetEyeToHeadTransform_Invoke_mD3E7F92F47833ADB36F0335541BDF22B7EF8F1EF,
	_GetEyeToHeadTransform_BeginInvoke_mEBD8C24DBAC303EC59189BD27BB75939E79C1B48,
	_GetEyeToHeadTransform_EndInvoke_mC968ED6B6CC6EC3BB1E777C96871BE9159B0E280,
	_GetTimeSinceLastVsync__ctor_m79236D04C7DB98317B1A48A232DA68E924F77453,
	_GetTimeSinceLastVsync_Invoke_m6898F486DC086B0BEE2CB74FB974D6D33F1DF08B,
	_GetTimeSinceLastVsync_BeginInvoke_mD3C591DCDDAFD8BA5DDEC42D16CEADB8DFA621D0,
	_GetTimeSinceLastVsync_EndInvoke_m04D3E90F25C3DCAA762F4712289F69136A08E384,
	_GetD3D9AdapterIndex__ctor_mC50841190AE0832DF71D197F503E5DD9E672F3B1,
	_GetD3D9AdapterIndex_Invoke_mE2733FBFA4DEE2E225B310F7A8CED7B9238A4B31,
	_GetD3D9AdapterIndex_BeginInvoke_m6B396470B2BC3B2D70BA53F0B73D9703DC3085B4,
	_GetD3D9AdapterIndex_EndInvoke_m35A8B48836E0F028DD65F475071A9AB45361F574,
	_GetDXGIOutputInfo__ctor_m257F225CF99F8939E98DECF542A7ADE5D06AF662,
	_GetDXGIOutputInfo_Invoke_m05F0F871FB1FC093DA80FA138F065E30DE7EE2B3,
	_GetDXGIOutputInfo_BeginInvoke_mFE865294BC64CA64D543954481C0E8964415939A,
	_GetDXGIOutputInfo_EndInvoke_m178601C3B3336C9CA133E13061A15921DB286CDE,
	_GetOutputDevice__ctor_mEA350DB5FAF7E9785833182E4EF688832583E296,
	_GetOutputDevice_Invoke_mBE050AB1EF4B2092E94A0D266C49C4AAB659C27A,
	_GetOutputDevice_BeginInvoke_m6E37010E549612AA42D4CF7CA8243D5FF07B8DB3,
	_GetOutputDevice_EndInvoke_m4D6A82385ED303BF97243A80FD35301F434E5DDB,
	_IsDisplayOnDesktop__ctor_mBF352ABE73668899A65A035D6D8D62E8E3679762,
	_IsDisplayOnDesktop_Invoke_m7EFAFE10D02463D938C16485C00371F9FBFB8F69,
	_IsDisplayOnDesktop_BeginInvoke_m92E2E186C5F9D75702D28C40E063FB11CAA979F6,
	_IsDisplayOnDesktop_EndInvoke_m000F74B2CDE8A15720BC63A140DBFF285458EF6F,
	_SetDisplayVisibility__ctor_m1277F99F880C333895795615C41FFB5757A61A43,
	_SetDisplayVisibility_Invoke_m59E3B6EBC29FC6312A2188B100089243E5A39C86,
	_SetDisplayVisibility_BeginInvoke_m9E7728A03C9B70E72F0BE5CD2E061506726C5C84,
	_SetDisplayVisibility_EndInvoke_m9F45EF027B15E502171726158DF21C5130E01792,
	_GetDeviceToAbsoluteTrackingPose__ctor_mF86295906E0AC7BCC5F642F5610EC09E79980E06,
	_GetDeviceToAbsoluteTrackingPose_Invoke_mB4A1B7F9E33FC2FE03E9668830EBC3262EA5E9C4,
	_GetDeviceToAbsoluteTrackingPose_BeginInvoke_m8159059337705510440E54E4173759C74BCFEE50,
	_GetDeviceToAbsoluteTrackingPose_EndInvoke_mAE0F24BA614C4751A27395D2D58228B7EAD45A8B,
	_ResetSeatedZeroPose__ctor_mE7A271DEE8238F0A90A393EA528B5956C4389271,
	_ResetSeatedZeroPose_Invoke_mE5748E241095720F5720CB0EED246BB94AE888DB,
	_ResetSeatedZeroPose_BeginInvoke_m53B19E2E0731B5A926C3D97B34A10E0F7B1B199B,
	_ResetSeatedZeroPose_EndInvoke_m2AB0D1ACB4553BA2AA2A09891887FD84EF849E3B,
	_GetSeatedZeroPoseToStandingAbsoluteTrackingPose__ctor_m25B0ACEC3E77AD0BE07BCA0A8DE2283C01C53B09,
	_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_Invoke_m18421B43CF741AA4307CCE4A10500334B25A32F3,
	_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_mCC8F45CE9CC4AFFDE190E2082C5BDA5C9E07C6B0,
	_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m98D33B37944E544BCDB8E3A257940041CBCFA665,
	_GetRawZeroPoseToStandingAbsoluteTrackingPose__ctor_mB44B73014E9C6FFA704196E755518177FB7CF930,
	_GetRawZeroPoseToStandingAbsoluteTrackingPose_Invoke_m00DF45579B54DB666667FB909297AA42545D2300,
	_GetRawZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_m76268B9590B3B8B4EB09A4D57952596BF932F77A,
	_GetRawZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m5CDAD7675FFD4DA4AF3D1979A9A8FD46960BF650,
	_GetSortedTrackedDeviceIndicesOfClass__ctor_mEA22C4FA2929883F0A3DD4E172A48C81F9648529,
	_GetSortedTrackedDeviceIndicesOfClass_Invoke_m282452AF8C3ACAF7D857D9D70C87B539A9981B5D,
	_GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m9B3815B69D955880891C1419F3DD52F609D9213B,
	_GetSortedTrackedDeviceIndicesOfClass_EndInvoke_m39A4B28E578976E4CCB54E85C86E1C6E4F16E6D6,
	_GetTrackedDeviceActivityLevel__ctor_mC3FE74E37BDF4576214131F0E97A597EB6CB5109,
	_GetTrackedDeviceActivityLevel_Invoke_mCFDC6C3339CC35B2C3C3E1EBB752B201C2390514,
	_GetTrackedDeviceActivityLevel_BeginInvoke_mA805C95669C25BE6095EB0B55133938F5AFDE003,
	_GetTrackedDeviceActivityLevel_EndInvoke_m12966AD011716BDF42AE02E612D8DC821B40938D,
	_ApplyTransform__ctor_mADD92A81FC6283815A26C67DF847D2F24BBDB3B5,
	_ApplyTransform_Invoke_mF4F6CBFEDA0C2500DEA11F26DC7F6D0E96B3839A,
	_ApplyTransform_BeginInvoke_mFCE0158FD823EC509B2AC051634840CD7C022306,
	_ApplyTransform_EndInvoke_m6D32F0BAF61C048DCFA6415A76AD767A3105E969,
	_GetTrackedDeviceIndexForControllerRole__ctor_m6F24270B375A336D496074DA4E2AA6F417F48CE8,
	_GetTrackedDeviceIndexForControllerRole_Invoke_m156B8501ACB889DAD21A456D8E17DAC2D1F4893B,
	_GetTrackedDeviceIndexForControllerRole_BeginInvoke_m464EA42521FFD89D565E3E2B57C8F2E3DBB0F6A4,
	_GetTrackedDeviceIndexForControllerRole_EndInvoke_m2BE9038F4B7BE937DD358AACF63857856F861DEE,
	_GetControllerRoleForTrackedDeviceIndex__ctor_m967C016162CEA0E832C10A84CC985A74288A86CC,
	_GetControllerRoleForTrackedDeviceIndex_Invoke_m5D599FBFE92D9F97EDDE43077AB817697B041531,
	_GetControllerRoleForTrackedDeviceIndex_BeginInvoke_mB87ABFB753D585BA6CDF3FF250EF571C597DC55D,
	_GetControllerRoleForTrackedDeviceIndex_EndInvoke_m8521DF0BE7608845F3EADEC57B7F787FE787ECE0,
	_GetTrackedDeviceClass__ctor_m8888331F1905AA6177F41775E369CD0161A4C8E3,
	_GetTrackedDeviceClass_Invoke_m28184E2A015BC02A6E24F0F3B3AABAD1A94A3D70,
	_GetTrackedDeviceClass_BeginInvoke_mF342303B892529AD3E64A439696095892EA1EBD1,
	_GetTrackedDeviceClass_EndInvoke_m4B4F45619BE77E4341C27265BE5F381D00980C16,
	_IsTrackedDeviceConnected__ctor_mD6F21BBBF860DB9ACEC53CEC1BAFA426070E61F1,
	_IsTrackedDeviceConnected_Invoke_m26D54165570B4B4EFB279313FE1DA187C523963D,
	_IsTrackedDeviceConnected_BeginInvoke_m8CA8FA532BA54E813B1521AE69D8F8E908F43BDD,
	_IsTrackedDeviceConnected_EndInvoke_m30E8C70EE5AE7138CB86FE9C3BEE3581F561DA76,
	_GetBoolTrackedDeviceProperty__ctor_mDC038C5A88EF2E0FA08EB3CA609188498F830B16,
	_GetBoolTrackedDeviceProperty_Invoke_m6A25E5ED8FC472633773D0686B3F12810432D56B,
	_GetBoolTrackedDeviceProperty_BeginInvoke_m73156F528974BF65CC43E6165497F045434CF16C,
	_GetBoolTrackedDeviceProperty_EndInvoke_mDAF4E241FF202D8DD45E8ED7703DEE55F602DA9D,
	_GetFloatTrackedDeviceProperty__ctor_m41F0EB795EAE879862DF990AD14C4F4D5947E923,
	_GetFloatTrackedDeviceProperty_Invoke_m1038FC7B116E84AF493904A329EFD9195FB67E41,
	_GetFloatTrackedDeviceProperty_BeginInvoke_m27B3C4A798E65045B0DE3EB275C24CF79E70D3F4,
	_GetFloatTrackedDeviceProperty_EndInvoke_mE4CB96ACF10A758985403BC9C78DFECE066D8CA3,
	_GetInt32TrackedDeviceProperty__ctor_mB0AB1C88F72C999DE8A2928E450BE3197D75289F,
	_GetInt32TrackedDeviceProperty_Invoke_m93E8C5E07459B0CAEE172BCEC73C80FC6145B527,
	_GetInt32TrackedDeviceProperty_BeginInvoke_mDE2E82C433DA5641C5BE4E7241F52FD0847F8687,
	_GetInt32TrackedDeviceProperty_EndInvoke_m66052BF414C68497E58B1CF774FA9ABBC19F1B13,
	_GetUint64TrackedDeviceProperty__ctor_mB51E27B099A0D202A0C0D9A8648A7738E75B6050,
	_GetUint64TrackedDeviceProperty_Invoke_mD5D179224386F9C61D6A6F81843721854B0F188E,
	_GetUint64TrackedDeviceProperty_BeginInvoke_mF0ADCAEB5E6E1E555F05E4B9DF50788A2D146C46,
	_GetUint64TrackedDeviceProperty_EndInvoke_m0638824FB82FC209EFB9D7EFBEDE8A82C288C0BD,
	_GetMatrix34TrackedDeviceProperty__ctor_mF24336475547C6A6977FEE63C8EFAF3810F57ADB,
	_GetMatrix34TrackedDeviceProperty_Invoke_m974853DC6B4CC1B4B459DCF54EF546681B82F71B,
	_GetMatrix34TrackedDeviceProperty_BeginInvoke_mF607A5FF0588005F0C732DA28573701C5075F188,
	_GetMatrix34TrackedDeviceProperty_EndInvoke_m6ED9416567FD45ED13A6254B39A8CDB4088864F4,
	_GetArrayTrackedDeviceProperty__ctor_m81DCCC37679C5CE0DCF135FBD5E0702790682D73,
	_GetArrayTrackedDeviceProperty_Invoke_m75458BE4C33DE808389B2F5BBA3A33D36F1FDE17,
	_GetArrayTrackedDeviceProperty_BeginInvoke_m4B3001A67E69DEF5674FCB93E1CD4F3697A397E0,
	_GetArrayTrackedDeviceProperty_EndInvoke_mE42DA97A9EE3D9D7CC6256DF45B3B381F9C9C4CF,
	_GetStringTrackedDeviceProperty__ctor_mA2FB4BECEE44739CF06D2D2FDD03B37C8FCAB057,
	_GetStringTrackedDeviceProperty_Invoke_mBFD477014E37F447C879A96A93DC4B5168737E25,
	_GetStringTrackedDeviceProperty_BeginInvoke_m858EAE0F9C2283CA471B94403957964467456C2B,
	_GetStringTrackedDeviceProperty_EndInvoke_mC62B255F1DB010FDF7FDA566ED1E876152A50646,
	_GetPropErrorNameFromEnum__ctor_mA00109017FC8BA6CD2F72FD11A48B77F3B8B3606,
	_GetPropErrorNameFromEnum_Invoke_mC86C6745D35A20FF333C23A0BCFE5A79210D4000,
	_GetPropErrorNameFromEnum_BeginInvoke_mFB7C21E22CE8994A3B6576DF7F1BB6FA0ACE3E21,
	_GetPropErrorNameFromEnum_EndInvoke_m398479E875F2A7DD2AA42C547D796ED7B4FF3A59,
	_PollNextEvent__ctor_m2397D373B9FBF5846446944A63787CC065F6B12A,
	_PollNextEvent_Invoke_m1B25A00D5753A22B0C8284927E7D3A2B501EDCFC,
	_PollNextEvent_BeginInvoke_m997A35DE835E30343C39903EBFB7C40D6516EB6F,
	_PollNextEvent_EndInvoke_m9DB790D935A250E3C2124F15182724213C8743B0,
	_PollNextEventWithPose__ctor_m6A8A769E84535D3FD80AC5B1A7B28A12E15C5B80,
	_PollNextEventWithPose_Invoke_m94B1066A0CD58CFBB216B145A8BB89802274A318,
	_PollNextEventWithPose_BeginInvoke_m4DF01BE0F475E01268AC5628735C686C51E95CA3,
	_PollNextEventWithPose_EndInvoke_m356B54C51FDAEDAF2E7D31895D5B5CAAEB23EF49,
	_GetEventTypeNameFromEnum__ctor_m745FC5F1645FEEBC5B17A3FF13A98B7839D13990,
	_GetEventTypeNameFromEnum_Invoke_m207B8A126CD48D518776050BB5BC7B1C60B1CAA7,
	_GetEventTypeNameFromEnum_BeginInvoke_m42523BC4EC84F003DAAF90DE45E8011DF664201D,
	_GetEventTypeNameFromEnum_EndInvoke_mFD973317D0FA6E72A2647915CF2AC0B510F6BBDE,
	_GetHiddenAreaMesh__ctor_m0AE39A09956343234122F4F78F16A81BD31845EE,
	_GetHiddenAreaMesh_Invoke_m4E9412F506DA0B66DE720B93652107CD1E8E4895,
	_GetHiddenAreaMesh_BeginInvoke_m5E1ACE2060DDA7D210BF3A3FB941A8DD708BA5D3,
	_GetHiddenAreaMesh_EndInvoke_m92790FB7E3C4970D1604A9BC9E196B9AC00483C5,
	_GetControllerState__ctor_m731F594692CFC09E788221719B8BD91766BD410A,
	_GetControllerState_Invoke_m3FD96B76D65ACEC1AE4A942FB150B8CC6938251B,
	_GetControllerState_BeginInvoke_m04ED3F050AAD4F896A80337E4E4A59C87C071AA0,
	_GetControllerState_EndInvoke_mEEA4F9D5A75E203FA899102878F39CB189C2FD87,
	_GetControllerStateWithPose__ctor_mEA49D50F28906B0D5D3433BACDF8D267C8514B9D,
	_GetControllerStateWithPose_Invoke_m758E5C7BC3D849CEA2630AD503D1B206B649C867,
	_GetControllerStateWithPose_BeginInvoke_m73FA675C7FC334D848D9128E01917E5396C07234,
	_GetControllerStateWithPose_EndInvoke_m88A6DB66944DF63DA708611E2DF6CD90A21E9974,
	_TriggerHapticPulse__ctor_mDB39757A7BE980375D317AB5DF4F8B1F8D4C082E,
	_TriggerHapticPulse_Invoke_mA1211B3664A3E9D0199437EF083A541E331B80B0,
	_TriggerHapticPulse_BeginInvoke_m71D85A1795335B014450AD9F07719E6A16581A8B,
	_TriggerHapticPulse_EndInvoke_m031C07A5848D4AF56A5D9FBC3694EC5801CBBB97,
	_GetButtonIdNameFromEnum__ctor_m1D66A06D90C09A14CAC5749B6D1EB609712FC1B3,
	_GetButtonIdNameFromEnum_Invoke_m6FE5FBC65BAB5A13F60563D23E08835E58D51F89,
	_GetButtonIdNameFromEnum_BeginInvoke_m192A3422C5F4D0F07A16E9420083B33431AD8C5C,
	_GetButtonIdNameFromEnum_EndInvoke_mCD0E939B94D68F4E5BE2A6AEF9777DD5B9150A7E,
	_GetControllerAxisTypeNameFromEnum__ctor_m72814256E68D1BB3114B68344DEC81ECF1CCB345,
	_GetControllerAxisTypeNameFromEnum_Invoke_mF3FD75ADB3D72667BFFB09F1EAC69D28FFA24E91,
	_GetControllerAxisTypeNameFromEnum_BeginInvoke_m8383269668B959504E9653A62F078232B5BBFA61,
	_GetControllerAxisTypeNameFromEnum_EndInvoke_m7EBF94DAC73D9184C95D6063176138AE330E4333,
	_IsInputAvailable__ctor_m64137A01B1D7DA02538432D7C49BAB9DC7D47DEC,
	_IsInputAvailable_Invoke_m990134A13B897E67D1081143E8A5918675104465,
	_IsInputAvailable_BeginInvoke_mCDA1D9E22BE33B68303AAC499C437A0980D19328,
	_IsInputAvailable_EndInvoke_m9884990B525012A7265C45052C7BB5031DB13FC9,
	_IsSteamVRDrawingControllers__ctor_mAC9881A1E200A4810DD7E228F656C9EB64C410A0,
	_IsSteamVRDrawingControllers_Invoke_mC5EB3F5CBCC68CE56E53C38929BD3AB03F69A516,
	_IsSteamVRDrawingControllers_BeginInvoke_mE1DA65D68B9B29EF5BC00A426E1D9DB151F82D71,
	_IsSteamVRDrawingControllers_EndInvoke_mB573416ED037D325FF9C85A83BBF28372D47ECB6,
	_ShouldApplicationPause__ctor_mC9B1F6653A94973AC35ADF42D1929B0140BD1DC8,
	_ShouldApplicationPause_Invoke_mE58AB9BD5DC7151B695CF0FEE404BA45FAA05C18,
	_ShouldApplicationPause_BeginInvoke_m267EC96EEBB5FB8B03D17B1C6CAE77B70A31E5BA,
	_ShouldApplicationPause_EndInvoke_m78615A7F670D1A4320ADF101C766A77CA1D1161F,
	_ShouldApplicationReduceRenderingWork__ctor_mEF79CAF1EA3DEA34B8719CF36B3FF6C0812BD990,
	_ShouldApplicationReduceRenderingWork_Invoke_mE636FF9AB4E27825A21A65B766E0885662CB49B2,
	_ShouldApplicationReduceRenderingWork_BeginInvoke_mBECE74EE8016DCA13D881D6D9774FEC91F49A23D,
	_ShouldApplicationReduceRenderingWork_EndInvoke_m737F6C0FF7AF403414A0C2895E623B095DFE7166,
	_DriverDebugRequest__ctor_m03B3ECBF560986A0B8940EF04D877108D0F36AED,
	_DriverDebugRequest_Invoke_m2BDD6EE30E72BDB2F5B775AE96C3806C69D2869C,
	_DriverDebugRequest_BeginInvoke_m2269CACD17652F6BC476154643F59A8389AE5875,
	_DriverDebugRequest_EndInvoke_mBE3B71E210B0BF99679BF38CC535BB5E2AD12C67,
	_PerformFirmwareUpdate__ctor_m1EEC2AC94CFB01B693FDBB5C5F173F1341FA0C94,
	_PerformFirmwareUpdate_Invoke_m35993BB13E5F0BCA1B5FC833E8BCAC0B7D9F3860,
	_PerformFirmwareUpdate_BeginInvoke_m8D11CAFC6F31ADC3A329171D5A67514AE6F18827,
	_PerformFirmwareUpdate_EndInvoke_m5165FE224F57CC6CC28B502FB5A2E76FAF662FA5,
	_AcknowledgeQuit_Exiting__ctor_mC3B9AA28269BB817DD668521941FBA90CAAEAAC9,
	_AcknowledgeQuit_Exiting_Invoke_m199F3555B34ECBA57C72482D4CDDBF7CE9307B64,
	_AcknowledgeQuit_Exiting_BeginInvoke_mC6C6A1D7B3FB266FF9EE88F65292C3B54EC4ADD0,
	_AcknowledgeQuit_Exiting_EndInvoke_m52919D072EEF28D5D1D4012B3C812AFFB2D6801A,
	_AcknowledgeQuit_UserPrompt__ctor_mE72287842207321FEF9961B0836BDFB704E93D40,
	_AcknowledgeQuit_UserPrompt_Invoke_m0D3B39C448AF24FE6929884E04DE7D52680A8AA0,
	_AcknowledgeQuit_UserPrompt_BeginInvoke_m5C4E545573A355662423FEB9CB157BF806677495,
	_AcknowledgeQuit_UserPrompt_EndInvoke_mDBD6F5077DEC760A46863565D081BC76AACDCC14,
	_GetWindowBounds__ctor_m84A84C118688E0FADF259A49D71F1520AAB21B9C,
	_GetWindowBounds_Invoke_m4FDF7A8F5333FAB04653EE8BC7A124DA3464D4A4,
	_GetWindowBounds_BeginInvoke_m27D9DE28EDD58A780BB7DEE761F83CF564ABC2C8,
	_GetWindowBounds_EndInvoke_mEAE7D1BF066943FB2CA6F3DDE36310FDA051C343,
	_GetEyeOutputViewport__ctor_m51C41E49E7AC9075054B23CE43D4C67900A43A36,
	_GetEyeOutputViewport_Invoke_mA6EE89C086079408374F4131DCCBA9760C079D46,
	_GetEyeOutputViewport_BeginInvoke_m5B40E60B41DB4EDAF27A9FEA4E44AE945536F40E,
	_GetEyeOutputViewport_EndInvoke_mA34BD3E1959CACF24293E5B0C7BE5C4F52B7DB5F,
	_GetDXGIOutputInfo__ctor_m2E636E44B9E5B4BCD7E84C79A4EA5FFD579A42A5,
	_GetDXGIOutputInfo_Invoke_m3F01FEE3323612D4B99A29C56EA4C0750ADBAEDF,
	_GetDXGIOutputInfo_BeginInvoke_m39D191F97E3736D8B80C8158B58E2144D4C2FBA8,
	_GetDXGIOutputInfo_EndInvoke_m266CC3CB84AB5CA9380ADDE29AD8014B8E167CD2,
	_GetCameraErrorNameFromEnum__ctor_m11AC1A5FC182150DFD4B993DCA2D64DA0A7193DE,
	_GetCameraErrorNameFromEnum_Invoke_m8DE93F93C81562B232D92B48510B3637A6A24F00,
	_GetCameraErrorNameFromEnum_BeginInvoke_m5248723D07AAADAC4873A901FE6FF34B9093EFDD,
	_GetCameraErrorNameFromEnum_EndInvoke_mF2B4273B3E88FC7C05DB183ED1C0709CED3CA24A,
	_HasCamera__ctor_mAA9FC08FA538E9C15599723C8BE751065EC778F5,
	_HasCamera_Invoke_m23E7FD9A3F3DA5042BFB3053BBC12FC374A4E223,
	_HasCamera_BeginInvoke_m088514BCA1F2F9D3AF8F71FCF37776DCF3C79FF5,
	_HasCamera_EndInvoke_m8D802B1F4F41CBC74173B978FA1DC9B39D1A939F,
	_GetCameraFrameSize__ctor_m98BE332A8C036D724D40FC0D4809A86EF1905E35,
	_GetCameraFrameSize_Invoke_m4217775D0404A2061940FB7BEE66571D20A6AAC4,
	_GetCameraFrameSize_BeginInvoke_m1284B0CAC9ADC6D08E0A0711163DD7550199CAC4,
	_GetCameraFrameSize_EndInvoke_mE6F0DC41FDD6668B966B23303BAF2D4141717BF8,
	_GetCameraIntrinsics__ctor_mAA5C457CF295CB6266D4F2A41525C16CF512869A,
	_GetCameraIntrinsics_Invoke_m3212819D3D482C91B14D78C42CDC530CAB622D16,
	_GetCameraIntrinsics_BeginInvoke_mE93A022B28E943ED7353F2BA728D6B196097E218,
	_GetCameraIntrinsics_EndInvoke_m1E43DC49983266739251B998051164F53986E914,
	_GetCameraProjection__ctor_m8B30FA056DF6013EFF82E1408305F0D8954151BF,
	_GetCameraProjection_Invoke_m987371F8352115CEBA3AE89431A92AA83791EFBD,
	_GetCameraProjection_BeginInvoke_m13664CB49E96AA3F3524C9FB9E2B9745C9BD68F5,
	_GetCameraProjection_EndInvoke_mE6773A04853266233DC5A6BDDFBA061F955BE739,
	_AcquireVideoStreamingService__ctor_m0D16A197EF31A60FA765FB59F4B787E7E455D34E,
	_AcquireVideoStreamingService_Invoke_mA4459230482C70D4A1B7D2618780A9921C59CD32,
	_AcquireVideoStreamingService_BeginInvoke_mCBB966D69C809982AB10422578740CE8E2A3825B,
	_AcquireVideoStreamingService_EndInvoke_mF20738ED5754B1F464041E82391D9FA976778E22,
	_ReleaseVideoStreamingService__ctor_mBE089FEC1D42C0F533B22BD2D6107E4DD7FFC35F,
	_ReleaseVideoStreamingService_Invoke_mD13D2E8E617D0CCC50215D05B181D6D37AB6A8CD,
	_ReleaseVideoStreamingService_BeginInvoke_mE3574B9CC7EC9D66C5FB9C2CB3347005350EA2B5,
	_ReleaseVideoStreamingService_EndInvoke_m72FA88C655CE731302200B84522716A9D84D3D91,
	_GetVideoStreamFrameBuffer__ctor_m552209C64C72360044B58B37BC3F85198255AA5F,
	_GetVideoStreamFrameBuffer_Invoke_m42A9E3CD5D53998CBEC5675336ECBD1D7FD09BF6,
	_GetVideoStreamFrameBuffer_BeginInvoke_mFCC631BC5BC71107F54736CF18A068849C321E53,
	_GetVideoStreamFrameBuffer_EndInvoke_mCC1FD14C6CCC6F3B1A98A7E9E710B562732B21EF,
	_GetVideoStreamTextureSize__ctor_m7820265ED74A5914FED52F49B0E6C61C95D4F2E3,
	_GetVideoStreamTextureSize_Invoke_mFC62C310DDCDE780FA54F04ED77C6C0BB4AE10CC,
	_GetVideoStreamTextureSize_BeginInvoke_m2280FCC5D5381BBA423BCB5F5F92908EAF026F27,
	_GetVideoStreamTextureSize_EndInvoke_m9B18F3525D9F5D311934C8991A9A8792E748C260,
	_GetVideoStreamTextureD3D11__ctor_mD57BF604FEBB1760568B0C3E389462D1723E220B,
	_GetVideoStreamTextureD3D11_Invoke_m917B49E8920A2A2BDE080CE5416478BBADF95A17,
	_GetVideoStreamTextureD3D11_BeginInvoke_mB275B85726824F8A9FE70A203ADB2CEC27CB9DB6,
	_GetVideoStreamTextureD3D11_EndInvoke_mBF501FA2B9448ACFE4D32C2DD0379824E4605CA0,
	_GetVideoStreamTextureGL__ctor_m4AB7BEED12932CA5AC510B59091BB5561BE3958A,
	_GetVideoStreamTextureGL_Invoke_m2D30DA93AC78DDC2BFAD44E4B10204A3604141A9,
	_GetVideoStreamTextureGL_BeginInvoke_m15DC6ADCCD04B88E0FAEAA9234AE56B3326AE39F,
	_GetVideoStreamTextureGL_EndInvoke_m83998DC041B56D2AF94A77FACFC55AC526BBB508,
	_ReleaseVideoStreamTextureGL__ctor_m3FC2F2B66DBB65A8990892BD1DB2E3DA21AF58D9,
	_ReleaseVideoStreamTextureGL_Invoke_m758C0778DA8841113203E0828A7A7B8121057EA6,
	_ReleaseVideoStreamTextureGL_BeginInvoke_m3C6C27B1F58C80F64166FB7CAE874DDE5B373B00,
	_ReleaseVideoStreamTextureGL_EndInvoke_m8EB0A5DCA65B55FF6ED0FBF645D2090802E1F92F,
	_AddApplicationManifest__ctor_m27FF628AEA2942208F59C98ECA1811F501858388,
	_AddApplicationManifest_Invoke_m7C1939B2FADE571F8E0FBD20ACE3CEF70C8D965F,
	_AddApplicationManifest_BeginInvoke_m87A69C590E96CCBC5EB51657421FB2B8EA5A48F9,
	_AddApplicationManifest_EndInvoke_mC720F64DCD3924DC869294858F6473CF0FECA33A,
	_RemoveApplicationManifest__ctor_m0D7E15DD9CAE8C2BD005AE77DE47D69FD497BAEF,
	_RemoveApplicationManifest_Invoke_m853A9EDED90D74C95D995E07EAC7FCD33EF4ADA3,
	_RemoveApplicationManifest_BeginInvoke_mD4578124ECB56E74A2F7285FFC7B792763AAA50C,
	_RemoveApplicationManifest_EndInvoke_mD338A6F5B4E5F880F266A7338E63A2648C4D1AA4,
	_IsApplicationInstalled__ctor_m268B59BDD2C1D158F263FB2A19951AF2ACB11F99,
	_IsApplicationInstalled_Invoke_mFF45BBFC8F42378FA577597F8A45283D637C283D,
	_IsApplicationInstalled_BeginInvoke_m1E0E72BB0EC53EAF24FB420FA23ED49075729316,
	_IsApplicationInstalled_EndInvoke_mB2E436D53A9222604198B0F71856B986946183FA,
	_GetApplicationCount__ctor_m6084F985C214A72FD48EA87FDEC627E03DBCF769,
	_GetApplicationCount_Invoke_m6A9A35DDC9D52F0515F07DACFE812AE13559EF26,
	_GetApplicationCount_BeginInvoke_m8FE37BD58A454BFEEE0B01BAD50553746C132999,
	_GetApplicationCount_EndInvoke_m12F7DE8D11FD983D21D8E3EEBFBB28FE18F4BE7A,
	_GetApplicationKeyByIndex__ctor_m43D4EB76A280A3206BF5E9DFA5EE7478CE754397,
	_GetApplicationKeyByIndex_Invoke_m7154C9183404739D542D58112FAA7F88F1687645,
	_GetApplicationKeyByIndex_BeginInvoke_m29E447545F4731A16287AD58F48061AB223263B8,
	_GetApplicationKeyByIndex_EndInvoke_m962D174A64B203B7531D2F96841C7FD038FB70CE,
	_GetApplicationKeyByProcessId__ctor_mF8CA2A55E3A41C2EBA2AD14A2004CD8466FAE995,
	_GetApplicationKeyByProcessId_Invoke_m58700077CDD1AD4BEFDFD9EE84D90F26C6714349,
	_GetApplicationKeyByProcessId_BeginInvoke_mAD164DB8849446DE0A070CF735A3DC68B4F98BF6,
	_GetApplicationKeyByProcessId_EndInvoke_mCC47F8D5E33C1BC3310574C4032F482D8C8146F7,
	_LaunchApplication__ctor_m66331F78E78718C73B6EAECE2554EEEACE695C3B,
	_LaunchApplication_Invoke_mB18025113B887F6E234131F0B0053BCA905ABC7B,
	_LaunchApplication_BeginInvoke_m5952281F0AB13E9A7E9DB8FD4282D422BAA70707,
	_LaunchApplication_EndInvoke_m2064B65C2C37A46027CD6BCBFAC7206E3C9FE3D2,
	_LaunchTemplateApplication__ctor_m2F2467044BF449071B05C321FB8A025D62BD9482,
	_LaunchTemplateApplication_Invoke_mA2667FC537C296F54871C2CDEE6A06ADA2261828,
	_LaunchTemplateApplication_BeginInvoke_m986D7DDFE1CE379AC90D6096E433DF13714092A1,
	_LaunchTemplateApplication_EndInvoke_mA4682D411519965CE7EE432ADBF74A2B1BDCED3B,
	_LaunchApplicationFromMimeType__ctor_mEE5DAC21CE60D38EA4435E89E9EC3D10A00D8B5B,
	_LaunchApplicationFromMimeType_Invoke_m767617AA6C75CF1D09462E3E55CFD858B47BD35A,
	_LaunchApplicationFromMimeType_BeginInvoke_m3870931FF064305CE3FA23B2DB76350F3635086D,
	_LaunchApplicationFromMimeType_EndInvoke_mD6DBD6A00DC336F9E4155CD3E8C4766915FEF588,
	_LaunchDashboardOverlay__ctor_mE5BF9F8873259414BECD1CCC62BB31BC72AE4340,
	_LaunchDashboardOverlay_Invoke_mBF4FAFC66BC768BCB500B7E28C1F0985AF27E2B7,
	_LaunchDashboardOverlay_BeginInvoke_mBBEB79ED5A38AE68B1C23ED90DDB784ACD6527BB,
	_LaunchDashboardOverlay_EndInvoke_m88FC98EB7C0507FEABDBC6231FC82FF894E5AAB1,
	_CancelApplicationLaunch__ctor_m1D909079D2D01FC7EFB1090BB4F4DE28417CDC49,
	_CancelApplicationLaunch_Invoke_mA6F09E650B05395A3F5F0AA00E15C01F1E44779E,
	_CancelApplicationLaunch_BeginInvoke_m31BAA8F2698199C55CA78CA6FD5F20CA5533700B,
	_CancelApplicationLaunch_EndInvoke_m19B15D6691AD84E073CAE8829635B4D0F2E60AC4,
	_IdentifyApplication__ctor_mB43FE26A4FB50AD9FAC81BA8CBC6DAD2AAF859DC,
	_IdentifyApplication_Invoke_mBDF0FB2C8FF929343689E48E5A82449A78DF3BAD,
	_IdentifyApplication_BeginInvoke_mE4D7C6C6940E1DFCB32C0FDE27BD57095FDDFEB3,
	_IdentifyApplication_EndInvoke_mAB5BB3A3866E784FEF2615172963F15E66D7DD7B,
	_GetApplicationProcessId__ctor_mF0E729537282EC197FD196895EBE92E64DDCE365,
	_GetApplicationProcessId_Invoke_mE974998B62A8F801F6C0075E2764CB0AE1FAF1C6,
	_GetApplicationProcessId_BeginInvoke_mA400864A78758CC8D8EFF91DD2E33D56F1BF6B51,
	_GetApplicationProcessId_EndInvoke_m23A7AEBB7FFDCF1AC9F193D3C53A74053DBBAD6D,
	_GetApplicationsErrorNameFromEnum__ctor_m6778968488CD0EEF44F5E6782AC6C559F0A9F42A,
	_GetApplicationsErrorNameFromEnum_Invoke_m1C69420F08EB2A8391EA8E71AB20F3DA0B08FA16,
	_GetApplicationsErrorNameFromEnum_BeginInvoke_m0432C3310897CE4B2158E3DDCF956B4BE8BFD159,
	_GetApplicationsErrorNameFromEnum_EndInvoke_mCFC5BD58D3B2612EB79D15FD572F042E94900C7C,
	_GetApplicationPropertyString__ctor_mAAA16E43DB585F615D02A70EEFA0858D0D0675C4,
	_GetApplicationPropertyString_Invoke_m970FF900F7167283C44BEB1628733D3BD112FAA7,
	_GetApplicationPropertyString_BeginInvoke_m82A6ACFEDEF9AB4EC72A8299FBE85FAE113F6AAA,
	_GetApplicationPropertyString_EndInvoke_m12323BD85FE5F3E1E04C2354F2F1F8F341C6DFBE,
	_GetApplicationPropertyBool__ctor_m6EDA8B745CFAF8E39E2A62D7FBC66695C8FB921F,
	_GetApplicationPropertyBool_Invoke_m9B14396C19D63B85D68F41346825980116C6E52F,
	_GetApplicationPropertyBool_BeginInvoke_m80896E1380E49A5E3207A4D1248C0857877887A4,
	_GetApplicationPropertyBool_EndInvoke_m32A94EAC935E4D4FEBF97EE225A209D1C78C12FC,
	_GetApplicationPropertyUint64__ctor_m63DCFF0968C0E6F000B124346C9748EEDA2BDF4B,
	_GetApplicationPropertyUint64_Invoke_m9237BBCE4BC86750C9B301DF5397F1753810D014,
	_GetApplicationPropertyUint64_BeginInvoke_mDBE2B8E777DCA4C12C9BD4D4122BC37BB209027C,
	_GetApplicationPropertyUint64_EndInvoke_mE6FBC4918A4D39551923EA69E9032DD634DD66B4,
	_SetApplicationAutoLaunch__ctor_mE75647ED640E02909498CE2244DF2CEC42A62E0A,
	_SetApplicationAutoLaunch_Invoke_mA2A82E8A4B7BF03ECBF469F2929D647D21DD36C2,
	_SetApplicationAutoLaunch_BeginInvoke_mB1B7ABF72FC57A2D8E808635EF659500F3042065,
	_SetApplicationAutoLaunch_EndInvoke_m5E3A9C35F2B42E79D328A369ECC0B4E954679C63,
	_GetApplicationAutoLaunch__ctor_m6FFC6541FE7F3DBBC38984CE02389F726BDC26D2,
	_GetApplicationAutoLaunch_Invoke_mDE6044AC0B43A889DFD9EF6DA6CED1E4573AD32C,
	_GetApplicationAutoLaunch_BeginInvoke_m18B604769FD16A2C6BCE188C1E734294261993AF,
	_GetApplicationAutoLaunch_EndInvoke_mA90905B69623B654B9D50D7033CB276B44877408,
	_SetDefaultApplicationForMimeType__ctor_mDD962D61F4F0709AD9032D5BE7F403D7D3B85E56,
	_SetDefaultApplicationForMimeType_Invoke_m26CA165503571550BA208ECD213A9F7B5B50594A,
	_SetDefaultApplicationForMimeType_BeginInvoke_m7FCBAAD9DFE11BEA8B8A5EF5C951F82EF80598CA,
	_SetDefaultApplicationForMimeType_EndInvoke_m3D141DC7F0724C9BBC031565A9C6CAB9570DD6F8,
	_GetDefaultApplicationForMimeType__ctor_m6C626E6215A10A860DC3AC765A6FCAC856A777FA,
	_GetDefaultApplicationForMimeType_Invoke_mE9E74EA1EDC480939AD485AAD8535042560C3A2E,
	_GetDefaultApplicationForMimeType_BeginInvoke_m9383E8341B91EAE9F448E5B44A62974C73D4A782,
	_GetDefaultApplicationForMimeType_EndInvoke_m1F4CF2D9DF4B82E28CD78AA1CD583ADE77245224,
	_GetApplicationSupportedMimeTypes__ctor_m48E5FD8195605891F789B2AA2A36356D896631DC,
	_GetApplicationSupportedMimeTypes_Invoke_m0DCCF72E59C727962FF920BE26645342A327C5E9,
	_GetApplicationSupportedMimeTypes_BeginInvoke_m702E375832498F9C968A821A04241CEEFC9B412C,
	_GetApplicationSupportedMimeTypes_EndInvoke_m3809E01ED389B61FE11DED48064D50B63C3AC619,
	_GetApplicationsThatSupportMimeType__ctor_m21107403C77281A366A0A081ED80ACE77D6953C1,
	_GetApplicationsThatSupportMimeType_Invoke_mEDE676EEEAD468EADF5941600B5C3F78AFCB549F,
	_GetApplicationsThatSupportMimeType_BeginInvoke_m212918EC011D235D130B881DC9A4F05E78F8E857,
	_GetApplicationsThatSupportMimeType_EndInvoke_m7A87BA58183A842777AA6040D944CC4177A42762,
	_GetApplicationLaunchArguments__ctor_mA37130E68A8B0124CB50CBBF3D1E941CC6A2156D,
	_GetApplicationLaunchArguments_Invoke_m6018D754C57BC75F6D278E5046C03BC70FEE0742,
	_GetApplicationLaunchArguments_BeginInvoke_m62C64DB5D1CCC6F7BC09EEAE679754F95B558029,
	_GetApplicationLaunchArguments_EndInvoke_mB6DCDD968395E922425E37C5A4435DC455AEFB21,
	_GetStartingApplication__ctor_mD18D8D540CB468E3F52B03079F006C9F81D6E8F0,
	_GetStartingApplication_Invoke_mC622EDD6C695EF0C7372D14AFEB9ED2009B0E954,
	_GetStartingApplication_BeginInvoke_m4CA51449BBB86A87639CC8E273E506C7699809C7,
	_GetStartingApplication_EndInvoke_m1C50AEC21C8EE285499CD72D131815173C8614A6,
	_GetTransitionState__ctor_m34CA516C187CAE43F45609FD9817BEFE5C7F7601,
	_GetTransitionState_Invoke_m0CB91D3C0364B314797FF74D2558B28FBB260D71,
	_GetTransitionState_BeginInvoke_mA0A4C91F9458A23435C3A7A3A63E704F025333E9,
	_GetTransitionState_EndInvoke_mCF5B937D418064A32AAAFB475D190AE7FDA39B3B,
	_PerformApplicationPrelaunchCheck__ctor_mA2FCDBD2E16623E41FE61C25DF182236480F18C8,
	_PerformApplicationPrelaunchCheck_Invoke_m68FD0C4A63420B152128B880BDE83CF06F55992E,
	_PerformApplicationPrelaunchCheck_BeginInvoke_mDF0053A2604FA2A46E36407E62EBC8C6FA14EA6A,
	_PerformApplicationPrelaunchCheck_EndInvoke_mC204CB45236CD9A3AA8BC8BB5037DC4E8BF48493,
	_GetApplicationsTransitionStateNameFromEnum__ctor_m77AF71EB9940C51EF994662AAC8A72363D8E5D51,
	_GetApplicationsTransitionStateNameFromEnum_Invoke_mA1B2075ECA5DC1CA078540262FDBDE895123E749,
	_GetApplicationsTransitionStateNameFromEnum_BeginInvoke_mB08D1217755FBD97FB16FA45FC7CD8A7F0AC7683,
	_GetApplicationsTransitionStateNameFromEnum_EndInvoke_m9456A33694B4193C6A7496352D269F498BCB0893,
	_IsQuitUserPromptRequested__ctor_mB85CB4A44C871059B70FCE8C1FEA9446308384CB,
	_IsQuitUserPromptRequested_Invoke_m8EF5A8577A945109A3AAD7F6686CA95FA5D3CEFC,
	_IsQuitUserPromptRequested_BeginInvoke_mAA46F1A18CFB6DCFF10519A11A118EDDE6F71691,
	_IsQuitUserPromptRequested_EndInvoke_m13F32DC5109F338E243737C0DB2F422A2962B11C,
	_LaunchInternalProcess__ctor_m2BA44B7526B8BFEB9B842EDAF63E90B8318A2415,
	_LaunchInternalProcess_Invoke_m21FE6F08ED76EBBDC5599EDEF6FDC055A2EC49EA,
	_LaunchInternalProcess_BeginInvoke_m84FFB33B208036038FF7906F1F49EFF6DB71348D,
	_LaunchInternalProcess_EndInvoke_m827F7E8C587AD55C4FEF272B7AE95481392E393A,
	_GetCurrentSceneProcessId__ctor_m4CE139BC60DBA5796EE5DE1B8FEF8899885AEAF2,
	_GetCurrentSceneProcessId_Invoke_m3355E48640ED7D60AD10B406D1B43066CF7BDC28,
	_GetCurrentSceneProcessId_BeginInvoke_m7684E5A83B9FDA85E15AC803E77DED693D647682,
	_GetCurrentSceneProcessId_EndInvoke_mE66A0251E95DF34F2A452D22F8402FC652B176A5,
	_GetCalibrationState__ctor_m391848D07A54F016118F9F10065A4AC945693B81,
	_GetCalibrationState_Invoke_mA921B066BD69721095036CCC556345075CF2CB94,
	_GetCalibrationState_BeginInvoke_mA71D5E371AECFF6076DED8B2ADCDEFADB70107F2,
	_GetCalibrationState_EndInvoke_m5504BEF3E5229DECC2AF37AB41E4153CF6A82A91,
	_GetPlayAreaSize__ctor_mDC1F208CC58A78D9BA994A46C3DCA49482C7BA90,
	_GetPlayAreaSize_Invoke_m7FBE613CF4666ADB8BABC158EFF2424EBD56469F,
	_GetPlayAreaSize_BeginInvoke_mB6898EC15A6DCA7AB9971A799306AAC661132E94,
	_GetPlayAreaSize_EndInvoke_m27D17612D5349C627CA24086791EE3FBC502ABA5,
	_GetPlayAreaRect__ctor_mE5F444F007E5286F616ED7DF2E2E6A06CBAFD377,
	_GetPlayAreaRect_Invoke_m2BB4ABA587BB81BA960D2C347E2EB61A56DE3AD4,
	_GetPlayAreaRect_BeginInvoke_m79F4752FC2B22C13755AF6B575310D2C93CE3ED0,
	_GetPlayAreaRect_EndInvoke_m8C8A8B621AAE856ED190C1FEA0BD7A76FE040A5C,
	_ReloadInfo__ctor_m6E041B00A305FB4A1D0E7615AAF5EF85971160C6,
	_ReloadInfo_Invoke_mF6724AD5E3105D61C67A63E94E6763694C2130E0,
	_ReloadInfo_BeginInvoke_mF5059DE57263450FE859CE07111DE534F971FA8C,
	_ReloadInfo_EndInvoke_m1BEAABAA552B0EEC53280F926833FD2AA4824ECF,
	_SetSceneColor__ctor_m651CAF28AA2F1DC4275584B5B2F3BCD321AA904B,
	_SetSceneColor_Invoke_m767F8E1A65F152E838796C73CE752A717FC3B915,
	_SetSceneColor_BeginInvoke_m6BDC5CBE5F923B88BC0229C6AD98BE7C4DAB570A,
	_SetSceneColor_EndInvoke_mD84023A1500A3443A49C1383319A9DDD1EF7EEBE,
	_GetBoundsColor__ctor_mB7EE4D74A6BFBC9C18947EF7CCE6D9542D5569B0,
	_GetBoundsColor_Invoke_m2104C029CAA863503F423DB8A89BD833A835EC3D,
	_GetBoundsColor_BeginInvoke_mD2F4032455A6E4EB6C972A729B9E7A73F604824D,
	_GetBoundsColor_EndInvoke_m1D2691A4DA2B51D462E3FD79850767C6CEDB02DB,
	_AreBoundsVisible__ctor_mB388D77A6F394EE0B8BB31755F3170840F7C7FAD,
	_AreBoundsVisible_Invoke_m687BAA828F5B2DAF7CCA68A8B176B887C15912D5,
	_AreBoundsVisible_BeginInvoke_mA3B7B557C6633E3B4AD28C7D5F9C815A7CA298DB,
	_AreBoundsVisible_EndInvoke_m4CE761533527D41CA4CF5FCD2FB9186106384EF2,
	_ForceBoundsVisible__ctor_mFE47D965C33F4185CE5941FA420EACA839EB453E,
	_ForceBoundsVisible_Invoke_m5FCA0FC2F659289190E2F8DCE8AC29C2190D5A08,
	_ForceBoundsVisible_BeginInvoke_m9D096F94D71FF802B040290FEB848D0D7312765E,
	_ForceBoundsVisible_EndInvoke_mC7CD101DBFC6F7D74259203A4A650F36FF24FAB1,
	_CommitWorkingCopy__ctor_m6119CDC225C28922651BED66A4FBA1255D9EBB82,
	_CommitWorkingCopy_Invoke_m3502C7464CD4C2A1A3273E9DB192A8A36CC77B03,
	_CommitWorkingCopy_BeginInvoke_mF5A4B1DF92DEE63110A5982F10C1D0257058B2FB,
	_CommitWorkingCopy_EndInvoke_mB2983D8AB4AEC592105A74744B56E405D29093DA,
	_RevertWorkingCopy__ctor_m88A941F4416A1811003667971BBA3A5E2A5D606E,
	_RevertWorkingCopy_Invoke_m818368F57F2265AFD60DCD5E2205523FDFA1CD47,
	_RevertWorkingCopy_BeginInvoke_m96AFBB0A937C51FC244D7246106FF763EA23B0A4,
	_RevertWorkingCopy_EndInvoke_mB8118CBA20BB1A158588D32B09FC5F2FDAD5E1D4,
	_GetWorkingPlayAreaSize__ctor_m6AD6498B731DE09984BCA35135C9E84451BA01BB,
	_GetWorkingPlayAreaSize_Invoke_mFC109852D4E17861BC6B1296F22886F0D4799EE9,
	_GetWorkingPlayAreaSize_BeginInvoke_mB2C019121212D9548E9577D3C56FE4493AC69820,
	_GetWorkingPlayAreaSize_EndInvoke_mB522D19BFB33370DEA86D485E8A024A3FECB8342,
	_GetWorkingPlayAreaRect__ctor_m6E7C9256FBD19CA6392C6D8D23B833A88FE82DC0,
	_GetWorkingPlayAreaRect_Invoke_mE375BD26CB235A0F7AEB43DEE700B9E8203E1A6A,
	_GetWorkingPlayAreaRect_BeginInvoke_mE2032707115B419C29B7ABA324DA043F4837DB27,
	_GetWorkingPlayAreaRect_EndInvoke_m01DACBB85CF69BCF042922B6175A7A7FB4A5AD8D,
	_GetWorkingCollisionBoundsInfo__ctor_m0EFAF11EB7706C2AB48816AD5F621D533C8CA862,
	_GetWorkingCollisionBoundsInfo_Invoke_mDAFC3BEDA35AC0D776F4B5B81B2A00FCD3195BAC,
	_GetWorkingCollisionBoundsInfo_BeginInvoke_m9DD9D61CDB380AAB1C2E0108C35281297C70D899,
	_GetWorkingCollisionBoundsInfo_EndInvoke_m95B65FFA19DEF3A12AC7F662AE9241A51CEE66BD,
	_GetLiveCollisionBoundsInfo__ctor_m16566693A5EAAED1B9B643B4FE7DBA70E974CE53,
	_GetLiveCollisionBoundsInfo_Invoke_m4C86C52BD805A3DAAFE3D3082DE615206A850D07,
	_GetLiveCollisionBoundsInfo_BeginInvoke_m3893CA50B1355313AF2C640AAEBE5AAAAA67EAF1,
	_GetLiveCollisionBoundsInfo_EndInvoke_m24146FFEC15FCF3D2E2B27F00A6839D15BB5A763,
	_GetWorkingSeatedZeroPoseToRawTrackingPose__ctor_m2C9474C6D82C77DC9952C167CE156D51C40DCC48,
	_GetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m4181F47E5782F10DC94BE3EA35FD70C330BD9E7E,
	_GetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_mD7C8440BA0025089C0E80577EE7DA503F6C2DA62,
	_GetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m716CCA424829F8BA1FFA317C718FFF3C34556BCA,
	_GetWorkingStandingZeroPoseToRawTrackingPose__ctor_m2E599FA31ACE06D69A274E6D3C1684B0884A4C59,
	_GetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m5E9D5D0BE45410AAB0E4759DBB9B56F0F34CC079,
	_GetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_mE6B2ACC68DF1FB78171E4E8550403A31DD7F5F91,
	_GetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_mD512BF22E4305C6A0792C71DDE4F7D8DF9BD9413,
	_SetWorkingPlayAreaSize__ctor_m7F1D398B800BCC63270BBC1D828B8184BFFC57EE,
	_SetWorkingPlayAreaSize_Invoke_m985D593E5ECAD7B4C8E5BBA3BFAB73F91AED67B8,
	_SetWorkingPlayAreaSize_BeginInvoke_mB3A4EF0E0630869B78B92A8EC215F7E7A61E284F,
	_SetWorkingPlayAreaSize_EndInvoke_mDEB4F4C5D99A6B94B904193AC13D45EC306121FB,
	_SetWorkingCollisionBoundsInfo__ctor_m9006D32C05AB70055D59B7F9EF6E66E5E6757458,
	_SetWorkingCollisionBoundsInfo_Invoke_mDABF0506B5788D4DCF344DBFB83069923014DE70,
	_SetWorkingCollisionBoundsInfo_BeginInvoke_m660BF3E708B69AA35317AC1B09ED6710C5546F86,
	_SetWorkingCollisionBoundsInfo_EndInvoke_mE8B6D3E37AB375FD5C977D7C2B149DF8BE905D50,
	_SetWorkingSeatedZeroPoseToRawTrackingPose__ctor_mD92F0109415118BCBD4F808E19CF0EDF9927063B,
	_SetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m05B7705AAB895E2994981E0620BF36D6F99258DD,
	_SetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_m9276BD47C609CEC80C5B852D0E7BEBC7C843B1F1,
	_SetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m10A4B22F85F7A92B10FB6248312B5A7D1CDB5046,
	_SetWorkingStandingZeroPoseToRawTrackingPose__ctor_mB9B1918FC1FF594A5DEA0011FF55E71E1EE76ED6,
	_SetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m3FCBCD7946C6049E67486AA57B920233920274D5,
	_SetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_mC685DB39368BE82B7CD703E1298C410D63294B67,
	_SetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_m9B08B8F96839FAECB840B9BEA34E96B1A67DF194,
	_ReloadFromDisk__ctor_mD43352301228F86956DB3FEDE862D4081B82D48A,
	_ReloadFromDisk_Invoke_mF254F7572030E0FE3E98F84D006333F79077D052,
	_ReloadFromDisk_BeginInvoke_m48AC8CDB95EFEDA56409EA6D1A046E37D94497F6,
	_ReloadFromDisk_EndInvoke_mBFB915070E147A1F5EB30D7D69ABFEA1356B0682,
	_GetLiveSeatedZeroPoseToRawTrackingPose__ctor_m3B041EE430FF36F5E267FB46D6D0B7A5D1107719,
	_GetLiveSeatedZeroPoseToRawTrackingPose_Invoke_m964F1199B72A75A39D13DC5176E5482224C88645,
	_GetLiveSeatedZeroPoseToRawTrackingPose_BeginInvoke_mEA06461D2528711FC42AD6D82AC0C2B67292169E,
	_GetLiveSeatedZeroPoseToRawTrackingPose_EndInvoke_m1328DCA3524202E420BBFAB11C360BEF0AF5BD5B,
	_SetWorkingCollisionBoundsTagsInfo__ctor_m8E193FB17CF21FDCCABB65B51620072F18FE1FD9,
	_SetWorkingCollisionBoundsTagsInfo_Invoke_mA97A813115268E5722A1720EF1030E7573BDA3F3,
	_SetWorkingCollisionBoundsTagsInfo_BeginInvoke_mDA74BCD77EFC2787C906AC4BBD415F5B41C5B5E8,
	_SetWorkingCollisionBoundsTagsInfo_EndInvoke_m8DAAB6F68131FA162D770C08214659458DB34FDE,
	_GetLiveCollisionBoundsTagsInfo__ctor_m17ED8D572F497B98458C0685B205305EDB9C106C,
	_GetLiveCollisionBoundsTagsInfo_Invoke_m5844DAAE9920FE81A0D5BFE2611E990A1BA4B43D,
	_GetLiveCollisionBoundsTagsInfo_BeginInvoke_mEA561325E98A766C3CF6E99F84524B3B83CE5C07,
	_GetLiveCollisionBoundsTagsInfo_EndInvoke_m65A8321AB14CDFB5DE1AEA47FE91ADAC73D2FF38,
	_SetWorkingPhysicalBoundsInfo__ctor_m7119EA837FBEA054308D26880DCBE18C48F75AB7,
	_SetWorkingPhysicalBoundsInfo_Invoke_m3754699A166C67BCA0BE1CB6A7FDE52D18A15FCC,
	_SetWorkingPhysicalBoundsInfo_BeginInvoke_m444974D47BFD76603E7D7357C72BC0ABA41CC80A,
	_SetWorkingPhysicalBoundsInfo_EndInvoke_mC9996DDAE5F55B1B37C6C7638CF5E5E780145568,
	_GetLivePhysicalBoundsInfo__ctor_mAE013DC2DDBE877CD4D86502C8D34E48A7C6D529,
	_GetLivePhysicalBoundsInfo_Invoke_m527DD73741B5A1BDE53B771B69DEFCB8603B14F8,
	_GetLivePhysicalBoundsInfo_BeginInvoke_m686549FADB1B1A3BAF914FF5D0E0D28180ECFD06,
	_GetLivePhysicalBoundsInfo_EndInvoke_mC78987AF9B84E84DB67AEFE7837B58A3F31AA179,
	_ExportLiveToBuffer__ctor_m69E7498138427FE2255B4CA99631877A86258D53,
	_ExportLiveToBuffer_Invoke_mBAA1DCD7AE665F4232117D20DF1C21FD421C0F9B,
	_ExportLiveToBuffer_BeginInvoke_m3FFFC8EDE8115C0B7E69242AEF7FE8D9C075102E,
	_ExportLiveToBuffer_EndInvoke_mFA8017AEF3EA227B7F0CFDD6841337E5346DF576,
	_ImportFromBufferToWorking__ctor_mFB14979FBEE413CFE81532441E7D115B1922228A,
	_ImportFromBufferToWorking_Invoke_mAFFC138C737039EE4A8A14C7B5992A2191B9D3CF,
	_ImportFromBufferToWorking_BeginInvoke_mA707892F243062BB05592D22919A3B23AA2EFA45,
	_ImportFromBufferToWorking_EndInvoke_m7A45E3B63AF719CD8D0833B0ECD4C7C6CB8281D0,
	_SetTrackingSpace__ctor_mD9295855E4372A49B21253BB142A2BBD92F9AC2B,
	_SetTrackingSpace_Invoke_mB46006C0CE6DC74488CEA28D2F3D5A7D0092B145,
	_SetTrackingSpace_BeginInvoke_m613C03CBF38BBE2BDD4CB82215F403B754FC0FAB,
	_SetTrackingSpace_EndInvoke_mBA9FA025CF79836F31E8E9D509AD0B74CC91EDF4,
	_GetTrackingSpace__ctor_m305EB2312ABECD5A69E57870DB832A66DCDD53C3,
	_GetTrackingSpace_Invoke_m116040E366EBCBFE98C0BF1C9A6166E86C00752F,
	_GetTrackingSpace_BeginInvoke_m3890C767BC3862604CDCCE3307E8B63A6F074FAB,
	_GetTrackingSpace_EndInvoke_m82E8102A6FA2F7526DB9AA67B3DA2737674CA9DC,
	_WaitGetPoses__ctor_m628F698AAB34013939EA732661FE6CCCADF2419E,
	_WaitGetPoses_Invoke_mEEC365C6C10E198F8E0EF8C416352235469BE5D4,
	_WaitGetPoses_BeginInvoke_m7E3637C7ED4098F8AF0355C0C7DBAB59BF21C1F7,
	_WaitGetPoses_EndInvoke_m71D4C8D9FE1B7956FEE10D0A2FA8D830101FB015,
	_GetLastPoses__ctor_m1EC747AB1CB75D963E49589110AF9F477DF82441,
	_GetLastPoses_Invoke_m68E5F9F88CBD04D05ABD3A1045505CDDAC6F896C,
	_GetLastPoses_BeginInvoke_m04A26B4AEEAD6FF97CB3A93DB57AF68368D04A85,
	_GetLastPoses_EndInvoke_m3B41DD9E37BEFAF5442FBF33F003A01494D447E1,
	_GetLastPoseForTrackedDeviceIndex__ctor_mB2E5E606C4EAFDBBF2E0307FB3CA5DDB76FCE4DB,
	_GetLastPoseForTrackedDeviceIndex_Invoke_m1914B7355AF459549280B0FE0795CE440AD60203,
	_GetLastPoseForTrackedDeviceIndex_BeginInvoke_m0EACD371B8160F7965C7B5ABD43ACB3B99C2E647,
	_GetLastPoseForTrackedDeviceIndex_EndInvoke_m7941B56A0D744D96A35FD7D5ACE03AF834241D2E,
	_Submit__ctor_mCA60E0C978503ED8FC190C769D42F0AC6FD14376,
	_Submit_Invoke_mA6B89CD1BA58B4436C440869A7E4112E3FA149D2,
	_Submit_BeginInvoke_m4A576D51E80D620EF3AC77463D9AE509AFC4FD67,
	_Submit_EndInvoke_mDF3959CB1B9599C29706DFD901BD0F21EA3F08B3,
	_ClearLastSubmittedFrame__ctor_mD19D96974E601DE9FC048B3773D3E5B2D2FDF751,
	_ClearLastSubmittedFrame_Invoke_m59DCA17075ADD078ADB4DB98047D9A507EFB5005,
	_ClearLastSubmittedFrame_BeginInvoke_mD2472FA342EBC994B8F51D12511923A07A03B166,
	_ClearLastSubmittedFrame_EndInvoke_m58C826AE116EDC222FA1B1A9ABA6EA96CDCCAC7C,
	_PostPresentHandoff__ctor_m88750003FFBA8FC4417E2486F6ACEFB762F584AF,
	_PostPresentHandoff_Invoke_m662967E346613A6DF55F19DD8796E8E3B3A9398E,
	_PostPresentHandoff_BeginInvoke_m5F2D690AE4EE290C3ABF29FF290C2D63523CB8F1,
	_PostPresentHandoff_EndInvoke_m4C8A58D11DEB27495C489BEB2B03F3FB313790CF,
	_GetFrameTiming__ctor_mC69618F6CAA1BE7EFC20DEF1E5C7B3ECCB1A380C,
	_GetFrameTiming_Invoke_m45B9854777546134DBBDC4E8AB1AECAAC6115A0B,
	_GetFrameTiming_BeginInvoke_m25562BA194279692B73A2457A900E796B9786BE9,
	_GetFrameTiming_EndInvoke_m5B3BA73549D50C7E4183C450BA641D4DBD76D171,
	_GetFrameTimings__ctor_m61AECEB55B10E85E947977FEEFCF97AE3C3B1E58,
	_GetFrameTimings_Invoke_m6531463FD5DA8799E5617AB7ED40EDD5C8007A14,
	_GetFrameTimings_BeginInvoke_mBB4198E4806C6D207738435FB2FF81AD29E9BDF6,
	_GetFrameTimings_EndInvoke_m39140A3F1D5F6DBE346FCC893F08D0C190F2793E,
	_GetFrameTimeRemaining__ctor_mA9AD1CE4F1DEF7A6379B7FE80754705A8E53B815,
	_GetFrameTimeRemaining_Invoke_m26BB55AEE8C167A80DB3FDF042FFD4D499D34D7C,
	_GetFrameTimeRemaining_BeginInvoke_m02C70E6E72B78E2E4761D355FDCFB9461536CCEF,
	_GetFrameTimeRemaining_EndInvoke_mD859B826DD280764755E04303177DDA13638D668,
	_GetCumulativeStats__ctor_mB087D5C1CC5B1D3CA349BB08B5C0CA696E3DDDCE,
	_GetCumulativeStats_Invoke_m9078FE356C4B0660284FB5699BB469FA0A92C231,
	_GetCumulativeStats_BeginInvoke_mBF87EB0D54295AD4A1256DDE3547AF2D6C59227D,
	_GetCumulativeStats_EndInvoke_m4D1DFADCB0E9666BA85637FA615C06F03C58EAB5,
	_FadeToColor__ctor_m1C193387B0866E1236B8723A78CFBB4D0A98793F,
	_FadeToColor_Invoke_m402AA10113BED820E140D07E166D2542110FB9B1,
	_FadeToColor_BeginInvoke_m388F34E26B3BC6526705FDFB17FF5C28F2EAC22D,
	_FadeToColor_EndInvoke_m146E47ADA5808957332AB5C75D501EC3D0C52EB5,
	_GetCurrentFadeColor__ctor_mD9B81BBA0820A043C29CB01C276A6DFFFA252119,
	_GetCurrentFadeColor_Invoke_m639B0F46883B62499FCB99401091ACC87E231994,
	_GetCurrentFadeColor_BeginInvoke_mE6D7032715457DC41D75ED768903E25EE84BBC75,
	_GetCurrentFadeColor_EndInvoke_m4A6E629433561AAE6714C00F99D4492893F80A31,
	_FadeGrid__ctor_m3E6F7295C0C8512566E19EEFF2AF019ADB5CF4AD,
	_FadeGrid_Invoke_m34A93EA43FBAD541234F377E2B1F3521C8273B62,
	_FadeGrid_BeginInvoke_m770E6F321E18D4B0C22732C365E5A570B633F373,
	_FadeGrid_EndInvoke_m4FAFC53EECCAE9A8D34D5596C8F6FD424BAB365C,
	_GetCurrentGridAlpha__ctor_m0026DF43714730A05A769072B07ED1F57EA7F648,
	_GetCurrentGridAlpha_Invoke_mD0DDC7F605512F31A0E07AC456893B7AD1C057A8,
	_GetCurrentGridAlpha_BeginInvoke_m58488E73B342A6DCBCD9C33F935372FE84F69871,
	_GetCurrentGridAlpha_EndInvoke_m7F270A6698C142AC58D325669EE981242A828AF7,
	_SetSkyboxOverride__ctor_m4FF05012FE44B09DE36E9AB8D1C5E82C2B1EF2EA,
	_SetSkyboxOverride_Invoke_m8664B7126D14B4CBC872B69E6E8BE1D310CFA6F7,
	_SetSkyboxOverride_BeginInvoke_mC0858AB01BA17CAD82A560A77A85AF05970AAA83,
	_SetSkyboxOverride_EndInvoke_mE21BF845BC175807F52A11FE321436C903EFCC6A,
	_ClearSkyboxOverride__ctor_mD71E9EAE366B306828E866DB40F5F279D184B618,
	_ClearSkyboxOverride_Invoke_m43CC909EFEAD19FDBE8EAEB5B23065FD9C019C9C,
	_ClearSkyboxOverride_BeginInvoke_m11851B9C04195F504A193FBFA289542D6DA603A7,
	_ClearSkyboxOverride_EndInvoke_m64095CA801F652CBD6BC544A16905B4E3E90F426,
	_CompositorBringToFront__ctor_m5DA327FD2073A4B5D74CBBF69038D9E0C164BF51,
	_CompositorBringToFront_Invoke_mB7C20AC4FCF8DE6BF5845E24A57D9C307D94BE3B,
	_CompositorBringToFront_BeginInvoke_mAE7B4FE34146FA5D1710B5B4CCC7052B0DF4A598,
	_CompositorBringToFront_EndInvoke_m4FC83BA9BC4ECA442AF5A6F7FF5D288BBD2A0CC2,
	_CompositorGoToBack__ctor_mCBAF8CE874533BC44EBA13AF0337C2911BC32BB3,
	_CompositorGoToBack_Invoke_m44EB49F1B806E5AAF58ED8EF0E592A3E197188F7,
	_CompositorGoToBack_BeginInvoke_m8AC3BCDE791287BDD1A897DD3AA4AAA3A6AFF6BF,
	_CompositorGoToBack_EndInvoke_m31C479FA7F3241940BACB67D47BAF1C116FADF34,
	_CompositorQuit__ctor_mDC155BC3B31EDD27B089D61DEC54A3AC9990AF5F,
	_CompositorQuit_Invoke_m5BB9B5D55A2EF1D0C43C11776892FAD190B41792,
	_CompositorQuit_BeginInvoke_m5E87B45A9CF3D705F0791121BEC9ED3E79079D38,
	_CompositorQuit_EndInvoke_mB485BE7041B4C1C196B53C83B03E5A7212CE2149,
	_IsFullscreen__ctor_m91A10D5582734A6DFA39822E44C3AC84C18A2F25,
	_IsFullscreen_Invoke_m29F1EBB3CBAF8757FF3F97E7E2F68D81778D1793,
	_IsFullscreen_BeginInvoke_m53A48A340493620C3978DC1563BE05ADF29325E6,
	_IsFullscreen_EndInvoke_m7A117E462828DB550E2378FAB5D125624293C9BA,
	_GetCurrentSceneFocusProcess__ctor_mB297364E7F8191E52F3E070F218E1411B763AA04,
	_GetCurrentSceneFocusProcess_Invoke_mFE2A7BF5E0FB35BAA590C2162140018A1119C398,
	_GetCurrentSceneFocusProcess_BeginInvoke_m402C1B9836B36F82532BC649CD456A8CA7B7367C,
	_GetCurrentSceneFocusProcess_EndInvoke_mCC19F1ABE907DA5FC495080F17E5FA868235E6F0,
	_GetLastFrameRenderer__ctor_m95483D8E41B238FF5BA62506096861775009225A,
	_GetLastFrameRenderer_Invoke_m948EC952F4ECD6EC0BD50631EE8D61248F2E3AE1,
	_GetLastFrameRenderer_BeginInvoke_mF6FB76A9288944EF13FD651F93B71B480846CD58,
	_GetLastFrameRenderer_EndInvoke_mE12CDD106CC729971FC8F92287D6299C4AAE3217,
	_CanRenderScene__ctor_mB5BDF8C3BDF87C57826B1AA6CAFB2AE493A0C9E2,
	_CanRenderScene_Invoke_m3C88985A0D3A30EC0A8871A9653F56DC541B5B99,
	_CanRenderScene_BeginInvoke_m62B364E1877FE5D3083121B80886D31A611F27A5,
	_CanRenderScene_EndInvoke_m92815C2F1FA762CC41E17A480933A15D15D7C25E,
	_ShowMirrorWindow__ctor_m6E99A67798012A94CE85B19127783DB36FF2C359,
	_ShowMirrorWindow_Invoke_m9EE70326549AB66AEA5DEEDE9957CFD2F7F22438,
	_ShowMirrorWindow_BeginInvoke_mFE703C6BCF3EAAFFFACA314CAAEB7128199D6FDD,
	_ShowMirrorWindow_EndInvoke_m0840940FA70E39F75A68AC6021ED4366BCC4DB89,
	_HideMirrorWindow__ctor_m111F8676161E9AABCD20EFEA7B472A4ED8A3A339,
	_HideMirrorWindow_Invoke_mB608734921E20DDDAF3422644C0FCADA96AF1BE6,
	_HideMirrorWindow_BeginInvoke_m864852A53D023CB02B95EB3CAA298E1DCDF9D935,
	_HideMirrorWindow_EndInvoke_mDF5C2C7986BECF62EAFBE21B815575DBE74D5BED,
	_IsMirrorWindowVisible__ctor_mDE2B653D06FD58E8A674F09800CA20751781C08B,
	_IsMirrorWindowVisible_Invoke_mF024D7CB0E96FE49BB680454804501E83166FD7D,
	_IsMirrorWindowVisible_BeginInvoke_m45F6BC59676284C52E5697BEF7E0F18440A3270F,
	_IsMirrorWindowVisible_EndInvoke_m7C4A2CFFF14E12D56553524D3ACA0FFD49F6A6C5,
	_CompositorDumpImages__ctor_mD752528C2B252F03CB3F96F5F2B1BE66DC3BC642,
	_CompositorDumpImages_Invoke_m57D81410074AA451FC037F438F66E9ACF9A59D77,
	_CompositorDumpImages_BeginInvoke_m28663B8C722425B3D864E512D559F556EAF7AF05,
	_CompositorDumpImages_EndInvoke_m4544FF4A5C4E806556A255FC0E0776472368EDE2,
	_ShouldAppRenderWithLowResources__ctor_m3DE2405B381509238F57149A2D2FA2003B8275D2,
	_ShouldAppRenderWithLowResources_Invoke_m963EAAA9AC894F9675664B6BCE07CA16EED2AF5A,
	_ShouldAppRenderWithLowResources_BeginInvoke_m37B50EDA952B974BB2F4D3992653933EF71F27A1,
	_ShouldAppRenderWithLowResources_EndInvoke_mA167707E8EA8D1575BB6BD0E71A8154C859880E3,
	_ForceInterleavedReprojectionOn__ctor_mC5B36680C4893743D0BFEE8359A0566725980827,
	_ForceInterleavedReprojectionOn_Invoke_m16B335C44D42C0B3E33A6C15B6FE9CAB790924DD,
	_ForceInterleavedReprojectionOn_BeginInvoke_mCD73D97F59B524D590BC7DB21645CA84AF4A0DB7,
	_ForceInterleavedReprojectionOn_EndInvoke_mDA87D992E44053B84622D7CC41010C816F39AF8F,
	_ForceReconnectProcess__ctor_m279653E525B2C4F42DB108A3B40DD80ECC5DD77A,
	_ForceReconnectProcess_Invoke_m0DCD4574C2C081C093F7AE37AF76D79A8ECA1C26,
	_ForceReconnectProcess_BeginInvoke_mCBE137FDD0CE9D59D884632918DC2F6C18E98295,
	_ForceReconnectProcess_EndInvoke_mD8DBE4D2A0213C7B394D813DE908FB08CCBD0ED8,
	_SuspendRendering__ctor_mF239A966EC5BDD81A77FA7EEDA324F3EB08426B2,
	_SuspendRendering_Invoke_m1662633A3707BEA5E8DA85E32A9528E2BA4AD048,
	_SuspendRendering_BeginInvoke_m6692B9ED057AC079C0B4B5C264CE3CDBD2347EF2,
	_SuspendRendering_EndInvoke_m57C3F54D5CBBF88F17BEC414E0B911E1E5476583,
	_GetMirrorTextureD3D11__ctor_m456D35BF675AF50B90049D310E02385BCEE54C7C,
	_GetMirrorTextureD3D11_Invoke_m4C9675D66FADFD1BD79A2F3530BB47CC72F1637A,
	_GetMirrorTextureD3D11_BeginInvoke_m97785A53DDB4CE10B54529DB1C2F8BB628586A09,
	_GetMirrorTextureD3D11_EndInvoke_m2AE5BB82682EA9276E104F5B908A9594B071228F,
	_ReleaseMirrorTextureD3D11__ctor_m822DEC7AE6F088CDFE9888BD82A76C1A65EFE9D5,
	_ReleaseMirrorTextureD3D11_Invoke_m5153EBA85339FD3430C1072A6C09C8805EED4967,
	_ReleaseMirrorTextureD3D11_BeginInvoke_m528B859522562B2327FFC5F73C3D1021B0EECBE6,
	_ReleaseMirrorTextureD3D11_EndInvoke_m9370C6FD9C6FA81C607AE15A6CABD8E7F8D5D578,
	_GetMirrorTextureGL__ctor_m534390FBFE638AB956263C217373FE3C14964EAF,
	_GetMirrorTextureGL_Invoke_m44DE609C5211156C80881CD2F0931C55958AF17C,
	_GetMirrorTextureGL_BeginInvoke_mF1D3A6626BDC7411D406B6B8F8285CB1CA4DAD9D,
	_GetMirrorTextureGL_EndInvoke_m789E72A0B444426B8C0DE935A44379471734859B,
	_ReleaseSharedGLTexture__ctor_mFA5B7A341DB471B0F0A750A0D8E39734023A2F60,
	_ReleaseSharedGLTexture_Invoke_m70578617E6FDA78906286EDAF41409A25B6A44A9,
	_ReleaseSharedGLTexture_BeginInvoke_m1915D3A6B08AB4DA4CC1D41A2D67FD83668C701E,
	_ReleaseSharedGLTexture_EndInvoke_mBFEF244142158D3FC0361DEFF76895D8520B17C8,
	_LockGLSharedTextureForAccess__ctor_mDBB08C95F8EE3B64EF63BA3F899A227BC68CA2ED,
	_LockGLSharedTextureForAccess_Invoke_mFC8458381FDB15B8A3891040D9A208045D77B221,
	_LockGLSharedTextureForAccess_BeginInvoke_m25EE9DD2F8AF689B33D1BD1BD045515594217529,
	_LockGLSharedTextureForAccess_EndInvoke_mB4C1035BA747E7F478082BB8AA7536575C38EBE3,
	_UnlockGLSharedTextureForAccess__ctor_mAA33A92A8A6618DA2E5DE7588B176ED4FD80DFAC,
	_UnlockGLSharedTextureForAccess_Invoke_mE7B8FAF8C95B1AD683A2D40A797F15D6B6802A8B,
	_UnlockGLSharedTextureForAccess_BeginInvoke_mFFB8E2AE4A92F06D8D7127C09A456D1B428FCBFF,
	_UnlockGLSharedTextureForAccess_EndInvoke_m1232681DA829AF27795C16CBCCE1897663211613,
	_GetVulkanInstanceExtensionsRequired__ctor_m534F82C3D5C2C20B4CF277980C23AAB5DBB5DD94,
	_GetVulkanInstanceExtensionsRequired_Invoke_mDEC7117F416B709BDA43B0068E268EA1D31AA88C,
	_GetVulkanInstanceExtensionsRequired_BeginInvoke_m2DE231574362C58F7F1127F13C5B915A161C94AE,
	_GetVulkanInstanceExtensionsRequired_EndInvoke_m9AB3ACF3D3679F7A64619AF1384A79C8C5ADC303,
	_GetVulkanDeviceExtensionsRequired__ctor_mC4F0112DFA6ACFE3CC58D7FFFE36096CBFC57A96,
	_GetVulkanDeviceExtensionsRequired_Invoke_mCA72D0934D95CBC1E4CB4185C575BB71670B75A0,
	_GetVulkanDeviceExtensionsRequired_BeginInvoke_mAB1C9CB38613EDA087D1D70A18A53BBCDB917EAB,
	_GetVulkanDeviceExtensionsRequired_EndInvoke_m78A426F7867665920CFA81B715C1D4EC6C9BDF46,
	_SetExplicitTimingMode__ctor_m47DF6468A33564ACBB62E7B7445254359799D7AC,
	_SetExplicitTimingMode_Invoke_m0F0B001BE2CA7A0572048F032BD77ED14A3B6CEA,
	_SetExplicitTimingMode_BeginInvoke_m5CBCF63EA94F2CB59E6B8CD5743DEEC65CC09EE5,
	_SetExplicitTimingMode_EndInvoke_m616CEA81448E3AD0FD89832029CFC2175B297907,
	_SubmitExplicitTimingData__ctor_mBA14AD183D223E6BF14427F0654E41D846A569AC,
	_SubmitExplicitTimingData_Invoke_mF86452DB54E6CA1C74B1F86E2516CF17134270AE,
	_SubmitExplicitTimingData_BeginInvoke_mDB22BA92D66EC16D3C7698D3713C5C6CD8B6726B,
	_SubmitExplicitTimingData_EndInvoke_mA1A6095D31F795ACA17015AD4B8E4A2C793227EA,
	_FindOverlay__ctor_mB53AC0CB6B5FEAFDEA8106429163FC31EE0CBFE5,
	_FindOverlay_Invoke_mE02774B83817848BFF986FEE7806194518C852DC,
	_FindOverlay_BeginInvoke_m7105D85E69912160AD3876F198472C99D52A4486,
	_FindOverlay_EndInvoke_mAA1DE7E95787AD01BF14A3A9E79E00D78ED4ED82,
	_CreateOverlay__ctor_m55DED3B47577CB127AD9F1C74D5CEB367A5C08F3,
	_CreateOverlay_Invoke_m190AF567CD1F78257B7A8F7C3F0C2F9506743131,
	_CreateOverlay_BeginInvoke_mBB732923B7B0D6F8C2300C85E87BA1E7D4349130,
	_CreateOverlay_EndInvoke_mD4FE9F1413CC47C3DF0A908670094B9589A91C32,
	_DestroyOverlay__ctor_m5638288DA040DAFE339A982463F40F48A2D145A3,
	_DestroyOverlay_Invoke_m1102137881BD6E84E74862F5CA002167F5792791,
	_DestroyOverlay_BeginInvoke_m19AF85DFDE34B8430921A021C3DAD8175D4DCB42,
	_DestroyOverlay_EndInvoke_m33644EDD02F600E4A1490CF8AD1B13135A000839,
	_SetHighQualityOverlay__ctor_m6B9CE6AF1B3F58F923D6B196132A082FA1BE333A,
	_SetHighQualityOverlay_Invoke_m24A186C0F31607CB036DE005C7D376BA3F42868D,
	_SetHighQualityOverlay_BeginInvoke_mC509A0150324A1DFA4A313680DB414AD7B50A152,
	_SetHighQualityOverlay_EndInvoke_m785E4B9CE912C3C9FD7F6B541C0F71237312A0DC,
	_GetHighQualityOverlay__ctor_m6D9E7DB16F8E65216D377C0AF7CFA12276426259,
	_GetHighQualityOverlay_Invoke_mF6097DE6B87B817504C60D9F12324B0B70DD6995,
	_GetHighQualityOverlay_BeginInvoke_m2F565230112779924CC98DABACE59CB129B70C72,
	_GetHighQualityOverlay_EndInvoke_m7D0DD2B83B08FC6A1C941F7D1843B554F4ADC7CA,
	_GetOverlayKey__ctor_m93D07D420C817352847050FBD964B37F19B9D9F9,
	_GetOverlayKey_Invoke_m7FD52BEF6517F318DFA5F600943F58436BF8B491,
	_GetOverlayKey_BeginInvoke_m111E8354F1C2052EA648C3B0426FDDEF0FE437B2,
	_GetOverlayKey_EndInvoke_mD28CC8D802074AB64BFBB8655FD12B8B00C1393F,
	_GetOverlayName__ctor_m87427FF8069D2A72D00A2B0CCBBF965DB6B99EF0,
	_GetOverlayName_Invoke_m14110D6E9D89EA544FC677D09AE961FD040D8A0B,
	_GetOverlayName_BeginInvoke_mB774EA58B905EC77E5B41DD103CB426A87FB68AF,
	_GetOverlayName_EndInvoke_mE541CD24FBC1D3B74EAC5D58E72FAE1D7F213657,
	_SetOverlayName__ctor_m143F36A3FDDB61C5F54B967B580662BE4BB828EB,
	_SetOverlayName_Invoke_m2EE8E7AA60286F9A39C7836138C1AD745ECB7562,
	_SetOverlayName_BeginInvoke_m1260536336090DF9D407549F5EE92919880FDBC9,
	_SetOverlayName_EndInvoke_m46B997EF6F52F41E4FC713A4024FC68F9E619E09,
	_GetOverlayImageData__ctor_m2DEE82A35BF3F2F39157C68C5ADC6A43F3229F9E,
	_GetOverlayImageData_Invoke_m1F824648C1AF729524CB0307CDB98E31F31CE0E7,
	_GetOverlayImageData_BeginInvoke_m29DC63EC936E2A652602D2301A314C015E9A894E,
	_GetOverlayImageData_EndInvoke_m807D67067F516A322875BEABEEC83C2930B0A79E,
	_GetOverlayErrorNameFromEnum__ctor_mFA76C7BCE6A4D14103E1155918F4FC36DDCF48C0,
	_GetOverlayErrorNameFromEnum_Invoke_mC40A1C0DFAA0BA584CB8E6DE106A6DC046C6EB25,
	_GetOverlayErrorNameFromEnum_BeginInvoke_m6404AA07CA6FD6A7B4ED5A818FE93ED90C9BE6BC,
	_GetOverlayErrorNameFromEnum_EndInvoke_mEA2082221719225700DA4780E50FF66C822E7728,
	_SetOverlayRenderingPid__ctor_m208990E6AF1BF1FFC72128047977316D5E935BA4,
	_SetOverlayRenderingPid_Invoke_m36D436056BD4E64760A24D3ADBC5C7A65A60C2C1,
	_SetOverlayRenderingPid_BeginInvoke_m2D0A08F0235E9EAEA3BDC284D11ACBC334F31292,
	_SetOverlayRenderingPid_EndInvoke_m5D955F1C3B55BC8004B2947C8D1D89B299442BF1,
	_GetOverlayRenderingPid__ctor_m31C78B526A901F97F4BDBA1A8CCBBBCB2EF48FA7,
	_GetOverlayRenderingPid_Invoke_m454EE872D9328F2D32597590EC1C659C08FCA301,
	_GetOverlayRenderingPid_BeginInvoke_m21AC9B0096E5EB18094D949778241807D6166A29,
	_GetOverlayRenderingPid_EndInvoke_mE0D11B4B11AD9332DD82F8B8656289AC1F91973E,
	_SetOverlayFlag__ctor_mCF62C541A6B13ABF5E435E07BB4445EC984053A6,
	_SetOverlayFlag_Invoke_mC100C9B9B32D7C9DEDAB5A84FFCA4C8C25AA9416,
	_SetOverlayFlag_BeginInvoke_m16A47643BE8CDB191C623AF955CFAF8F3AC762B6,
	_SetOverlayFlag_EndInvoke_mAF336419AC263E7064CB5846ACAE82BBFEB16508,
	_GetOverlayFlag__ctor_m9B1531D53A1F6A8914EFF072FED19674DF90DDD5,
	_GetOverlayFlag_Invoke_mD4E3EC3BE1DBB598009F0B14AD2ADEBCF8EFF2BF,
	_GetOverlayFlag_BeginInvoke_m7AD84D73F22583B9E0B61B30CBE79586998547F3,
	_GetOverlayFlag_EndInvoke_mA378EE01E2B0E8021007A463EF3F930C9B98A346,
	_SetOverlayColor__ctor_m00E65CDB79C3C2513A499615323F8AB6F9EF4381,
	_SetOverlayColor_Invoke_mD3675E33B94A0441232372EC2DF73BB6C568D9A3,
	_SetOverlayColor_BeginInvoke_mE6696BDB4923C5E381812EB443368348099EBDD3,
	_SetOverlayColor_EndInvoke_m0342B32DD37F1084DC801DBAD2BF0C4CCAE80AAF,
	_GetOverlayColor__ctor_m7811850A5C9AC2A74043BFFED93C5E6F390A4220,
	_GetOverlayColor_Invoke_mF91B2A82A3F8FF074CD191894666E1776C6A9A1C,
	_GetOverlayColor_BeginInvoke_m11EE063DE1B961D14FD90FBFE2B96A9B7EE3C0F2,
	_GetOverlayColor_EndInvoke_m4225F82B86EE08A909823724703CF986168F1752,
	_SetOverlayAlpha__ctor_m57A387C1140334B74206FFA24554DF5CDD6C8A63,
	_SetOverlayAlpha_Invoke_mF7AADABD66172E31981E95EE9BCFFB87CED8792C,
	_SetOverlayAlpha_BeginInvoke_m7AD82DC7A96CB50E8A57D2FF278ADE177D478CE0,
	_SetOverlayAlpha_EndInvoke_mE916B96080955CE40F3D57B108F4CF77055236D8,
	_GetOverlayAlpha__ctor_m67F033B2CE1D78CFEC938C9547826220238F73EA,
	_GetOverlayAlpha_Invoke_m4421016686E7C302EA3799071D883599D89E7C98,
	_GetOverlayAlpha_BeginInvoke_m9060E7DE9829C939A010BA2EB5D82138BABF46D9,
	_GetOverlayAlpha_EndInvoke_m741E781AA25E9ADC691A901120965891D2DEEB0D,
	_SetOverlayTexelAspect__ctor_m2379A78B80CD16B1C7066975ED6F1048DEF39B30,
	_SetOverlayTexelAspect_Invoke_m59F06EA5BEF1FB42E3293EAB1FDD687B58D81E50,
	_SetOverlayTexelAspect_BeginInvoke_mAB6C9EC756B20E1987BBFF88DDF5FE5F26C88DFE,
	_SetOverlayTexelAspect_EndInvoke_m512C112E0658EE4322B864CC3144A8A80B040F4E,
	_GetOverlayTexelAspect__ctor_m78B60F35894AF8B3A0F328FA37C5F52A2545F52A,
	_GetOverlayTexelAspect_Invoke_m5E269C5D0716B33AE6501B3882F66AA3151C46FF,
	_GetOverlayTexelAspect_BeginInvoke_m7B76B611C367D2C7AC8682887B7AD2618253A966,
	_GetOverlayTexelAspect_EndInvoke_m452457E1B7E6290ED4A2886AA86700EF6558A5E9,
	_SetOverlaySortOrder__ctor_mBF3D3EA67BF13832356593D79C66C2F2B3C460AB,
	_SetOverlaySortOrder_Invoke_m0B56E887B2EDC048E6B2FAC9541E15AB702A30B6,
	_SetOverlaySortOrder_BeginInvoke_m7DF28B55358B7D5FEBE0251725443BED355B2A51,
	_SetOverlaySortOrder_EndInvoke_m44F47E1563F018EE55F8148A6C4FE6DFCEC9251D,
	_GetOverlaySortOrder__ctor_mEBD01FD558315659B29751A951D667FBDEA9199A,
	_GetOverlaySortOrder_Invoke_mB5C50D760D60425B779EC9E52DABD9C65DCE29E5,
	_GetOverlaySortOrder_BeginInvoke_mC015C907326CB97D7B7B94748D3E68C7C7BE3A2F,
	_GetOverlaySortOrder_EndInvoke_mFCD9FBB42DECD1F53F43F3232E1C92025F77C656,
	_SetOverlayWidthInMeters__ctor_mD85DF2CE00A92C7D31C81925469EADFB33E502D8,
	_SetOverlayWidthInMeters_Invoke_mA94F390AEFDA255452F3DAA15BFF897946D863F9,
	_SetOverlayWidthInMeters_BeginInvoke_m49EF54A069EAD34F80C138B7F6521260A3BB767D,
	_SetOverlayWidthInMeters_EndInvoke_m7E9B0F8344F4AEB10CCDFF30A9ECEB7673A0335F,
	_GetOverlayWidthInMeters__ctor_m304495438EE54CF31BF2C07B7371B5AC7D4A40D5,
	_GetOverlayWidthInMeters_Invoke_m06AB42114E62F94041035113CEC126169B9F742C,
	_GetOverlayWidthInMeters_BeginInvoke_mBF281FBE5BEBE75DFCF67E1066BE0FBC12AE357F,
	_GetOverlayWidthInMeters_EndInvoke_m762D47E505F52570996F4141BA8CD3015FA3AD1F,
	_SetOverlayAutoCurveDistanceRangeInMeters__ctor_mE640AE7C3D6EF81C7E6FBD0195E85C8DF8ABF830,
	_SetOverlayAutoCurveDistanceRangeInMeters_Invoke_m5FEE202093D38BDE33A2D83B4C44ABBFFD09EBD0,
	_SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m6785CF72D5C1A63A2B8E1678C03A7102B4E81479,
	_SetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m103B9A14D20374D8BF7E94548EE28CEEF1F8CE49,
	_GetOverlayAutoCurveDistanceRangeInMeters__ctor_m608EC745E125CF9E386071464A69CF3A113DEAAA,
	_GetOverlayAutoCurveDistanceRangeInMeters_Invoke_mAD22FCF45D35041EAF32CD68F6B6DAB89436BB84,
	_GetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m042F49CB2E487AD7E51DFC27B87B3DB1841BD3FC,
	_GetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m674A719317C6A7FEFBB47D7D6CA56128148D6EB7,
	_SetOverlayTextureColorSpace__ctor_mFBBEAC70DACB5F3612F7DC3968C7D9575344AB90,
	_SetOverlayTextureColorSpace_Invoke_m17ABCAF3ABA7D99D51C3BAF2D871C565C1B1629E,
	_SetOverlayTextureColorSpace_BeginInvoke_mEF30ACAF99983513800FB0C92A27F61A637DED40,
	_SetOverlayTextureColorSpace_EndInvoke_m2A37B20782575226F4EC233B58CB3BC63806C557,
	_GetOverlayTextureColorSpace__ctor_m556A9A1ADE2C4E7D3C80DBAC30EEB5B043C9A1C0,
	_GetOverlayTextureColorSpace_Invoke_m78D47252BA7F47B06949ACC12E9F4642438D986D,
	_GetOverlayTextureColorSpace_BeginInvoke_m2AB2A0C45D6E250E67C889E3CB1053C79F8A42B6,
	_GetOverlayTextureColorSpace_EndInvoke_m5DEA8534B6ADD81F14A2708EDBE3279BDD5552F1,
	_SetOverlayTextureBounds__ctor_m656E0AC14F06717B615E465FE09659D3568E6AAD,
	_SetOverlayTextureBounds_Invoke_m8CEE8640189D0324713291AA599525078EE9FC4D,
	_SetOverlayTextureBounds_BeginInvoke_m7886C5E627301A1E5620F247EDF526456A9904A5,
	_SetOverlayTextureBounds_EndInvoke_mBD6546B72C19819EE4DFCDA737D26D37C5EF00D5,
	_GetOverlayTextureBounds__ctor_m117952B8F6622A6C6DBB509FB6C41231A71ECF77,
	_GetOverlayTextureBounds_Invoke_m99CAB365F69A7595BABDD055B0FDD45CFB1EC6FF,
	_GetOverlayTextureBounds_BeginInvoke_m706C998272C772C00DCDEC1B4223EB49ED3FFDC5,
	_GetOverlayTextureBounds_EndInvoke_m67923B96DEEF8C7316F1C716253E4AC322B0DAE5,
	_GetOverlayRenderModel__ctor_m131B0D9CB462DFDFC180BC268E4F6D8904A68D83,
	_GetOverlayRenderModel_Invoke_m59CA770EA890F9DEF7E8C25921CF0B1083C81E33,
	_GetOverlayRenderModel_BeginInvoke_mBA2568EB1D02726E2C84E1C367D746DCAEC9C7F1,
	_GetOverlayRenderModel_EndInvoke_m43D87B75E847594AEDED894A75C698D63E9C0D01,
	_SetOverlayRenderModel__ctor_mCCDEACEB1607786BE86A2EC09F675C4EEBEE1074,
	_SetOverlayRenderModel_Invoke_m90D73ACA2B2AC6C71DD4BCD545233BAEFE625EF9,
	_SetOverlayRenderModel_BeginInvoke_mE5730E829B2FDB0A6FDC1D162C44085B578AE520,
	_SetOverlayRenderModel_EndInvoke_m64C447CF77DD5F3860B7236F2F2E08F005B419C1,
	_GetOverlayTransformType__ctor_mA49A7C0686989CDD06796DD10A75235E8001D5EC,
	_GetOverlayTransformType_Invoke_mE531908BBA6C7E0623BB2623123A3D95E0A616DE,
	_GetOverlayTransformType_BeginInvoke_mAC95BF02580E2AF9C5018C6CE9FA62453FE2134C,
	_GetOverlayTransformType_EndInvoke_mE941F699D8230F43A258E440FF00843DE082DE1B,
	_SetOverlayTransformAbsolute__ctor_m108099F42D094EE71AA867B3EA5A0693AB0995E8,
	_SetOverlayTransformAbsolute_Invoke_mB2CBA24D053535E67CF91F39097430511AD2E375,
	_SetOverlayTransformAbsolute_BeginInvoke_m33DAB6F0B166AB245C224BBCC95AB66698D9408D,
	_SetOverlayTransformAbsolute_EndInvoke_mA64E91C99395DF20F12160850B63F00DEBB20BB8,
	_GetOverlayTransformAbsolute__ctor_mD3F26F98138830E6BAE68A46377F7AE0182B483F,
	_GetOverlayTransformAbsolute_Invoke_m0CC2FC99AEA5CD7BE60BAC823E02E12821098194,
	_GetOverlayTransformAbsolute_BeginInvoke_m70CF17AA58C689C64514DED6938D1D7A30397562,
	_GetOverlayTransformAbsolute_EndInvoke_mCCFEF3795A57E904881A821DA4AB25B457935352,
	_SetOverlayTransformTrackedDeviceRelative__ctor_mCDC75AC04C0977EF7D7D48B91DB5AA9108FD6E12,
	_SetOverlayTransformTrackedDeviceRelative_Invoke_m19748CA3F1E41869B8293F86CA9C8F9CAACEE75D,
	_SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m8C8752CC0FF282CED2599FEDD7DD4E3DD0FFBE8D,
	_SetOverlayTransformTrackedDeviceRelative_EndInvoke_m10E570B364B0C470039866CEBAAC4C826E3DAD03,
	_GetOverlayTransformTrackedDeviceRelative__ctor_mAFC6632B40E8A2A969B29AC94E0335A5D97CCE05,
	_GetOverlayTransformTrackedDeviceRelative_Invoke_mE3C28FD930746EF522C32B180EDC88CE18364B8B,
	_GetOverlayTransformTrackedDeviceRelative_BeginInvoke_m966057E62C74C4AE424B9AE73E83AEAF89A4DD37,
	_GetOverlayTransformTrackedDeviceRelative_EndInvoke_m9DE4A0EE407F5BB3A6C0B64DA1890060844375C4,
	_SetOverlayTransformTrackedDeviceComponent__ctor_m2FBE03B08561FB0CBB388E5F752B4CD40449DE49,
	_SetOverlayTransformTrackedDeviceComponent_Invoke_mB4338E5F742B290D552F5899DF6A4FFB6FF817BB,
	_SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m42B48EB65F835D3CBC3A82FAC934D982174F2551,
	_SetOverlayTransformTrackedDeviceComponent_EndInvoke_mEF105683A1B1AF8848D410122786727F69963AFA,
	_GetOverlayTransformTrackedDeviceComponent__ctor_mB0204C812D0E606DB1747DF54CD3C353B8C13857,
	_GetOverlayTransformTrackedDeviceComponent_Invoke_m7119F93CCA3687C39EEC7551EA83DF303B4A16D2,
	_GetOverlayTransformTrackedDeviceComponent_BeginInvoke_mE6F14C4202572FFCE2E3AB9A60E524A0383B403F,
	_GetOverlayTransformTrackedDeviceComponent_EndInvoke_m75C510F88E290C84CDEEACB91CA528A50F2C64E2,
	_GetOverlayTransformOverlayRelative__ctor_mCD6481083C6C55DA3466C52205C37C9734D333D8,
	_GetOverlayTransformOverlayRelative_Invoke_m5F0A5E0F53137B6B99BD9058EB213C928F79E39D,
	_GetOverlayTransformOverlayRelative_BeginInvoke_m9A26A6482056E88945A1475C65D5DED62225E1AC,
	_GetOverlayTransformOverlayRelative_EndInvoke_mD1C773620C83FF03B7CDC4986A5C9E0839B8CD8E,
	_SetOverlayTransformOverlayRelative__ctor_mE934C6C3692DBF614BB31F730A0463037C06269D,
	_SetOverlayTransformOverlayRelative_Invoke_m0745182BCA88A074CBFDA9C2B60240370A644642,
	_SetOverlayTransformOverlayRelative_BeginInvoke_m2B19B22BC086BC0D59216D45D9325E5F192EF46D,
	_SetOverlayTransformOverlayRelative_EndInvoke_m5483F0BF24D6CA462C5867D610C1AAFE096A57A8,
	_ShowOverlay__ctor_m65932229E3A0B1C7EA09FBE166864D8C78680779,
	_ShowOverlay_Invoke_mEE7AC9B31BB7A4DA3251E3070BB035AB9FD2263A,
	_ShowOverlay_BeginInvoke_m65DD94C585912C912AF0F5EE55D122DA940BA5BC,
	_ShowOverlay_EndInvoke_mCE3C7B903D897A5F40DFDB0C1524612018D9E68D,
	_HideOverlay__ctor_m33AD63B27F1EC1512F86E52977C4EA76CC24AA5C,
	_HideOverlay_Invoke_mAFB36F4D11769B68EFBEC3FB1BAFC7145D8BB4C0,
	_HideOverlay_BeginInvoke_m8FACCFF0B42527ACDDC6C17CA542022020969C62,
	_HideOverlay_EndInvoke_m76F5256432CCEF7B41B65A5A115816B7F66FD5DF,
	_IsOverlayVisible__ctor_m868B4CD1403ED5E63F429D9CD501AF8087C83F32,
	_IsOverlayVisible_Invoke_mDE56B3643B1046A720A712398D748A91D9864495,
	_IsOverlayVisible_BeginInvoke_m0F923F02B1F78E2A3E4C14DAAE91EC0ADC92E6E5,
	_IsOverlayVisible_EndInvoke_mF7B60596EC77CB39A48495C70F1B402EA037A562,
	_GetTransformForOverlayCoordinates__ctor_mB149F50E976A0FA70127CD5E794F2AE052AC1493,
	_GetTransformForOverlayCoordinates_Invoke_m0B6245BB25F5E78921E65D10F52291A8CC215F55,
	_GetTransformForOverlayCoordinates_BeginInvoke_mEF5A8586A0F476DBA501F466EEBA7D5B472D36E0,
	_GetTransformForOverlayCoordinates_EndInvoke_m2FE325BD2BF540B60526B4A0F751A647B004FBBF,
	_PollNextOverlayEvent__ctor_m5C5B40FC15355C9BD6AB60822E1327A519835F5C,
	_PollNextOverlayEvent_Invoke_m258E6B7E1A9605B058388625A0BE1652B545D53D,
	_PollNextOverlayEvent_BeginInvoke_m962C202B2D90117C79FD40A64526E0A834D73C61,
	_PollNextOverlayEvent_EndInvoke_m1EEC2D50E3FA43CFD90EC4FAEAE52CDC61C90049,
	_GetOverlayInputMethod__ctor_m00E45764FF2FE8A397F25EE687FEC30570BEF6A4,
	_GetOverlayInputMethod_Invoke_m79BF508A695D24F469AE2356F310CBC8B765DD81,
	_GetOverlayInputMethod_BeginInvoke_mFFFD1595572CA2D18EAC9A5EB3C9300E602F935E,
	_GetOverlayInputMethod_EndInvoke_m7F2B15BBC13CCABB44341145A88BFC3FAB55C975,
	_SetOverlayInputMethod__ctor_mF14787A5DCECD6C30FD1AF1DDC8B2F486E84CFBC,
	_SetOverlayInputMethod_Invoke_mBEE07180E398766C4DE4A58D52797541822FAAB3,
	_SetOverlayInputMethod_BeginInvoke_m58C3C56AA999374EB5CEE6F8CCAFD0DD6C3C6963,
	_SetOverlayInputMethod_EndInvoke_m17C628F0A581D4CCB689C1C89CA1C6B9439E518F,
	_GetOverlayMouseScale__ctor_mD3569F4810107525CAA51DBA8C08A038862C5A51,
	_GetOverlayMouseScale_Invoke_mB0992A2F3BCF9123705CBC3F2CC88DEF4E2C5697,
	_GetOverlayMouseScale_BeginInvoke_m83F457A98BE9A74A3A0C0C5E282A65F3A6C8AA45,
	_GetOverlayMouseScale_EndInvoke_m66552A1118FE81DB871B3468FD79FAE7B7EAA62B,
	_SetOverlayMouseScale__ctor_m33C24E4BFD14A9727BAB1FA4F2837A511032EA81,
	_SetOverlayMouseScale_Invoke_m0CAB759AE9777FDB6A503E2B686D52FF05E96C75,
	_SetOverlayMouseScale_BeginInvoke_mB7C2EC861BAE7DBE09984F22908786FCB067C023,
	_SetOverlayMouseScale_EndInvoke_m7441941EE2DA1737ED9DCB7E9B862BF4F7AE68A4,
	_ComputeOverlayIntersection__ctor_mD7B9A7BEBF6B8D3CE552444765D38868D6462149,
	_ComputeOverlayIntersection_Invoke_m020402DFF6C74A866E68824BB1D3260C1CF9DC89,
	_ComputeOverlayIntersection_BeginInvoke_m0C8F1F1BFD1FC88713B3AEF7080C1AE7AA9CA423,
	_ComputeOverlayIntersection_EndInvoke_mBDE0D0FA4CC2B9AFB57454F2A79C48815B366018,
	_IsHoverTargetOverlay__ctor_mE2F367611CCCAB5187B8EBF579AC8C7CF309174E,
	_IsHoverTargetOverlay_Invoke_m6E134F5EE52983C22C266C9431830CAF413EE368,
	_IsHoverTargetOverlay_BeginInvoke_m41F5FA3C45995CB5FBA6ED27EDAEA575E6C0ABBF,
	_IsHoverTargetOverlay_EndInvoke_m531F374BD111C26A8A708DD83BD405C3BA231175,
	_GetGamepadFocusOverlay__ctor_m804858C4D13E3C42C0E5C39D9FC6285597474005,
	_GetGamepadFocusOverlay_Invoke_mAFEF1124A60ED71C13259E898C615E1CC5D19EF7,
	_GetGamepadFocusOverlay_BeginInvoke_mC7243CDC558D92126C5F1C24E6FEBC54D595A8F4,
	_GetGamepadFocusOverlay_EndInvoke_mA639B753C7EE2185D02231B793F0C32765E00C90,
	_SetGamepadFocusOverlay__ctor_mD7D6A116398F944D9C938ECB4EF3EA95B0AA28B1,
	_SetGamepadFocusOverlay_Invoke_m1284F1D3EB9E8CD53FDC77E4AB24A13372FC2BCD,
	_SetGamepadFocusOverlay_BeginInvoke_m29F88B9F1AD20ABDF2A8F8F6BF9366537E687828,
	_SetGamepadFocusOverlay_EndInvoke_m096205AB9D650AA147BD8F36040BFEE45197B2DD,
	_SetOverlayNeighbor__ctor_mE48218584965B6FB3E1ACC730E4AE119FA3157C5,
	_SetOverlayNeighbor_Invoke_mE8725421EBB4BD1EC047164890DE791401BC098C,
	_SetOverlayNeighbor_BeginInvoke_m499F6ACBE9BAF7DA627B63F10EE8CB15C622CC7E,
	_SetOverlayNeighbor_EndInvoke_m27DCDF035E0C4D1A80B875CBE25227FB49A8F0BE,
	_MoveGamepadFocusToNeighbor__ctor_m84FDE5671A1DFAF1CD2215395AA1E7DF1884340E,
	_MoveGamepadFocusToNeighbor_Invoke_m1E1B2B5E8AAAAD242E68C8CAEDFAA5D08DEF1959,
	_MoveGamepadFocusToNeighbor_BeginInvoke_m8ED596DAAF2217682AED9B834E18D1627A77D753,
	_MoveGamepadFocusToNeighbor_EndInvoke_mB4B3C93FE0D3B79D11C73F54E927E1191E42F65A,
	_SetOverlayDualAnalogTransform__ctor_mDE3D586F934702109960590633EFC1BC8A5F41BB,
	_SetOverlayDualAnalogTransform_Invoke_mFC3566123CC6DD975ECA193B46C47C8EFF1C8CCF,
	_SetOverlayDualAnalogTransform_BeginInvoke_m25FBCA75B4FA082F40A498FCEE888311FD408B3F,
	_SetOverlayDualAnalogTransform_EndInvoke_mED1E48A4E1AB933B86D26BA2E9A51F831303DB89,
	_GetOverlayDualAnalogTransform__ctor_m91E823FC5E9038E2F72079F5921A075668B96C0E,
	_GetOverlayDualAnalogTransform_Invoke_mDC11A950A4BC9DE37FA7B861193B99E90E109C49,
	_GetOverlayDualAnalogTransform_BeginInvoke_mA2E45E80F29409335882897E9F6AE7FB4B5E419F,
	_GetOverlayDualAnalogTransform_EndInvoke_m13EE948A13B34854888EFE18AF34261396C988B9,
	_SetOverlayTexture__ctor_mEC1B9FC33EEE71D011C0139D5D1AA808B8084566,
	_SetOverlayTexture_Invoke_mA50FEF68FA65A13D731ECCBCC82326AD8D04AE05,
	_SetOverlayTexture_BeginInvoke_m9BCAFF4A475034FFE16EC5C97F484E98C7788102,
	_SetOverlayTexture_EndInvoke_m5687CC54AEC0FEDA7CD599C92916EACAD58B25B2,
	_ClearOverlayTexture__ctor_m6D30AF655D426912C40F9D4EB0ED9BF7E11FD98F,
	_ClearOverlayTexture_Invoke_mD9919CEF34B393DD27667C9E8B899B0CD1AE092A,
	_ClearOverlayTexture_BeginInvoke_m5FDE02CAC8FD45D0502C31B969E5A0799DD73C01,
	_ClearOverlayTexture_EndInvoke_m4A1F5A28CBAA82C4492ED129D2CC7A3BD16167F1,
	_SetOverlayRaw__ctor_m7A6825B4349D4E0100383E6A478B1441847A892B,
	_SetOverlayRaw_Invoke_m6AB24C29977942DE2CE95F16190F0A939524ABAC,
	_SetOverlayRaw_BeginInvoke_m5E8C915B00BA3330C45B10EC6245409ECEB233BE,
	_SetOverlayRaw_EndInvoke_m8A450225FDFD8EFD692A8401B424F5A71E339F1C,
	_SetOverlayFromFile__ctor_mDE8DF12D3229FFB7FEB9B9A683A6D617F4FE53ED,
	_SetOverlayFromFile_Invoke_m168FEC0D50310BEDA5D0E1353312C5318096B1AE,
	_SetOverlayFromFile_BeginInvoke_mCA86F0A7AB0D72445E1A590BC954C3FE3D9826AF,
	_SetOverlayFromFile_EndInvoke_m95905767CF857D9A9919F1D76D6C7FEBA4BBB16F,
	_GetOverlayTexture__ctor_m78A99EC7CDB046B08D5704C9B380DDFFB84EDCC0,
	_GetOverlayTexture_Invoke_m29C919D5BED915E123EBC5E1107857BC8D4DC64D,
	_GetOverlayTexture_BeginInvoke_mD837C513DC0E222ABB22C66120D2E7DC1F75D5C3,
	_GetOverlayTexture_EndInvoke_mEAC566B02C07BD0836E31CCBC21FFECCD5552459,
	_ReleaseNativeOverlayHandle__ctor_mAE5034561AD5D464B07900FE7B1112524C226404,
	_ReleaseNativeOverlayHandle_Invoke_m96827DB4EB74569B4F1EACD33B9BC8EA86A232BC,
	_ReleaseNativeOverlayHandle_BeginInvoke_m6FFDD17C09DF1728DDF3BA2A6604631EBF46B4C9,
	_ReleaseNativeOverlayHandle_EndInvoke_mC21D0B31128943F3C95C9464A5DFEAAFB8379903,
	_GetOverlayTextureSize__ctor_mA25CFCEA5B994CDD9816F94D9134E3C5B8222152,
	_GetOverlayTextureSize_Invoke_m1E15F714CF6FB32D9B9F1D0CEC16783B92F5B379,
	_GetOverlayTextureSize_BeginInvoke_m6AEDF2802264CB5D6309AA5BD03DC3A8F5E35C7C,
	_GetOverlayTextureSize_EndInvoke_m1BC297DA4B35E8BFF70C924643E0E19A455881F6,
	_CreateDashboardOverlay__ctor_mE5B536AF2AD714C67E16D4E932052FEBAEAA7299,
	_CreateDashboardOverlay_Invoke_mEE840694F56BC17BF8D182C5932E1EE8265283A4,
	_CreateDashboardOverlay_BeginInvoke_m7BF1299C5407826D7DF2AACDA1A2E26AF5B89B32,
	_CreateDashboardOverlay_EndInvoke_m82B62AF86AF802F9B09DC4B5490DF5DC462D1F1D,
	_IsDashboardVisible__ctor_m5FE4C70E27FEA962509C4F14086C2B1F02AE9D08,
	_IsDashboardVisible_Invoke_m70D7D352A81AAEAB15FAB82CD381E68F3F2E49F0,
	_IsDashboardVisible_BeginInvoke_m152E7BB5DB123280A2D21F984D07DFC95BABC9B4,
	_IsDashboardVisible_EndInvoke_m808B9A6277FB129ED5DA8097EFAFEFBEFF354549,
	_IsActiveDashboardOverlay__ctor_mD05ACEBB25499584B9EF899358FB782FC3B1A16B,
	_IsActiveDashboardOverlay_Invoke_mC2E44057CA797B347B1C6D576C73EE50F9FF5A9C,
	_IsActiveDashboardOverlay_BeginInvoke_mC6E62C4573720E43B41E95CB27B62F6F95760810,
	_IsActiveDashboardOverlay_EndInvoke_m8D260446EAD13DB6A173D94DECAAA1F3ECB57A31,
	_SetDashboardOverlaySceneProcess__ctor_mCADDE92B4E4E048F7A75925906C4EC496294529B,
	_SetDashboardOverlaySceneProcess_Invoke_m444E56C36EA8EC89B2B2FBDE24646D998C633226,
	_SetDashboardOverlaySceneProcess_BeginInvoke_m658ED5AB526B15DC8D50A2AEA4F935C9F3D30D38,
	_SetDashboardOverlaySceneProcess_EndInvoke_mB3A70856CC8773B36D7BB6F5435BB8F9571E4433,
	_GetDashboardOverlaySceneProcess__ctor_mCF07FD53BEFC15804465229666473CB2C254A555,
	_GetDashboardOverlaySceneProcess_Invoke_m99BCA5FB0B473DED77DDE6687B39C58635AD0FF4,
	_GetDashboardOverlaySceneProcess_BeginInvoke_mF36287BF8B6307B99BD9F21A3479E4F644103754,
	_GetDashboardOverlaySceneProcess_EndInvoke_mF4E8E1325E238C29355553856E27300614D6B6D7,
	_ShowDashboard__ctor_m27B7D2B2820FA747CC1F5CE9C8F2A31790C9EBBE,
	_ShowDashboard_Invoke_m17DE084F137A9A8972748A9EEA5C632272318D69,
	_ShowDashboard_BeginInvoke_mC62432E97271B62853C9DEB3FB25C93A19C88656,
	_ShowDashboard_EndInvoke_m24153FEEFC8F9F1E4396E1290EB088BF7AC24579,
	_GetPrimaryDashboardDevice__ctor_m6CA654DE11A210ACFA0E89CF8E82945D58B081BD,
	_GetPrimaryDashboardDevice_Invoke_m9A352147A0FCDAC2E10F661BD9BDFF6266B15BB3,
	_GetPrimaryDashboardDevice_BeginInvoke_mC20D8AE5CE562E57260920051643906FD26D9FD8,
	_GetPrimaryDashboardDevice_EndInvoke_m36EDBD37FFCC39781BA7C9EA7286EBD92F03B58D,
	_ShowKeyboard__ctor_mB304AD2E52232F23DB7545E5B00400005189A19D,
	_ShowKeyboard_Invoke_m2F66075375F2D39BFCF710582E99A039A5BA9A2F,
	_ShowKeyboard_BeginInvoke_m98A7353153A78E971F2F0FFD73E6E662601635B5,
	_ShowKeyboard_EndInvoke_m420EC30EBDB769875AD32CEB7F8A212D496E9E18,
	_ShowKeyboardForOverlay__ctor_m6956B48C497263EAD795997912B1DCDB8E4C9884,
	_ShowKeyboardForOverlay_Invoke_m297159DAA856CD5870C20BE611A8C42FF1A09C73,
	_ShowKeyboardForOverlay_BeginInvoke_mFCD7B8409415D0339B6E482658D50C1B540069CE,
	_ShowKeyboardForOverlay_EndInvoke_mF32CB159E146ECCA2F8CBF21AD0271C697CA7E33,
	_GetKeyboardText__ctor_m3305FF2E8A9C2F3473B71BB5C1B9B05B9595CE13,
	_GetKeyboardText_Invoke_m998A27D96FB3E1CC80639AD7939221A72D72BEFB,
	_GetKeyboardText_BeginInvoke_mA15848BDB54C6241C9CF6FE6B966041ABEB38EA7,
	_GetKeyboardText_EndInvoke_m7BB4219F9C198CE120000957984B7BC6E1618A4B,
	_HideKeyboard__ctor_m80C67510481424E09AA83FECF8705AD15085AA54,
	_HideKeyboard_Invoke_m961B3BF62A13E17FEC72CD20FA6F7882BFA4A4A4,
	_HideKeyboard_BeginInvoke_m9DE5B81D45D3D9826AC43A3E731A22B678D332E7,
	_HideKeyboard_EndInvoke_mADA9F3195BD43F0ABCB26F38D85F67B4E1B3925F,
	_SetKeyboardTransformAbsolute__ctor_mB0E8008A0FCBDE575AAA7C2A5495DB00BE938E5E,
	_SetKeyboardTransformAbsolute_Invoke_m931CBE3336D58F5519D8C4A0A8BADA5A55F8F63C,
	_SetKeyboardTransformAbsolute_BeginInvoke_m41FE32852BC7FA923E9AB4F03B8D93E11D629D4A,
	_SetKeyboardTransformAbsolute_EndInvoke_mECAE8E0ADBD79322527AF75C79D36E9266CF8F49,
	_SetKeyboardPositionForOverlay__ctor_m702F89F4A7F51CFE12C1AA2821119CACD68C5AF8,
	_SetKeyboardPositionForOverlay_Invoke_m23C4586034C947A391F327E7620D47D8C30DB44E,
	_SetKeyboardPositionForOverlay_BeginInvoke_m7FE40B16324A70305722F00377A2EAB557D5B967,
	_SetKeyboardPositionForOverlay_EndInvoke_m1E577ECF3C09005994FF6946F81CF688D96AE729,
	_SetOverlayIntersectionMask__ctor_m3781056147C33C9C3E011B794D1DEFB8EBCD44B1,
	_SetOverlayIntersectionMask_Invoke_mB59570EA77220F499AE0E11F6BD141493E7A37DD,
	_SetOverlayIntersectionMask_BeginInvoke_mBCDC96B661435BCD094BCFCF74BDDB3167E3C215,
	_SetOverlayIntersectionMask_EndInvoke_m92C5B4D9AFEC9908D1C8C1650E5DFBE58AC6F0BF,
	_GetOverlayFlags__ctor_mF339BBF4A1D85CBB3471E46A1938DE62B6F79F15,
	_GetOverlayFlags_Invoke_m45A223151BE499DA897F0413275A9903D04F4BC8,
	_GetOverlayFlags_BeginInvoke_m7391829BA6B6A630B88C24684998FB37883182C9,
	_GetOverlayFlags_EndInvoke_m34B5F63E614FFE23A080E446EC02A7E43D49E4E1,
	_ShowMessageOverlay__ctor_mEC3AC6FF29492B6D5F64F18EA0F1E763D2A00790,
	_ShowMessageOverlay_Invoke_m8B38191AB9B94D9F6FDDA8736C10326B0FC2C39D,
	_ShowMessageOverlay_BeginInvoke_mE078229215AB4ED0DF135AB2AB39FDB000134F9F,
	_ShowMessageOverlay_EndInvoke_mB8712A1BD34AA1CF421CDC47379DA5759C5B6A20,
	_CloseMessageOverlay__ctor_m983D3F131FF10FD19628013938DB6990105FB2CC,
	_CloseMessageOverlay_Invoke_mB4897E143F9BC2878DA5746847F1FE37406EFEE8,
	_CloseMessageOverlay_BeginInvoke_m81ACAA21D31A99722842F73557A2C143A0EB1330,
	_CloseMessageOverlay_EndInvoke_mABEDF45DC8908EE371876C09143DD25F8C7DE0B4,
	_LoadRenderModel_Async__ctor_m766F2E0C86D4E1ECC3B6E8FF6B0D95DF99F05AD9,
	_LoadRenderModel_Async_Invoke_m224C6AEACFC8DF5AB833A9C7D86626467118ED29,
	_LoadRenderModel_Async_BeginInvoke_mF23EF977F1524A130279105441F3C59E078C55D7,
	_LoadRenderModel_Async_EndInvoke_mEF7012A92DD7BE2C8E5DCF23E09642FAAEB697DA,
	_FreeRenderModel__ctor_m9A2CABF9DFE33066FC0935BADAABC71E289E304D,
	_FreeRenderModel_Invoke_m494C68CCA08B36E4EFB91962473524FBC19D9374,
	_FreeRenderModel_BeginInvoke_mB5C318FAFB42FFE8782C8778DC1905CDEF6D188D,
	_FreeRenderModel_EndInvoke_m1B2FB635F8B3915D8AB9631F8E7BD396B4DA475F,
	_LoadTexture_Async__ctor_m8059F56F0AB81ECF44FBFFEC65F27B4CF8D058AE,
	_LoadTexture_Async_Invoke_mDD760C8DEAD59A6B3EBBC64B3CB100074B517753,
	_LoadTexture_Async_BeginInvoke_m6BE2A7F66D4D90C703314A9CBA23B83C2914792B,
	_LoadTexture_Async_EndInvoke_m0E0AF8C6FFD53B63070C62AD6937CE9E2B5D8F29,
	_FreeTexture__ctor_mE333AB15387DDBBFD083358A7A69D50B84F769BF,
	_FreeTexture_Invoke_m2358611FF7B266112954B6233C15CA8952E8D5EC,
	_FreeTexture_BeginInvoke_mFFE6ACF5CB5240A5038F3A5E35271A1EF809E0B0,
	_FreeTexture_EndInvoke_mCE61D6A0EACD9F5C873AF15E3D265E357C9343C3,
	_LoadTextureD3D11_Async__ctor_m5DE6DACDE30E93B7E940D69876B50DC762F78C57,
	_LoadTextureD3D11_Async_Invoke_m676C1193D08D45F8A2526DC268BFD8E8725F3612,
	_LoadTextureD3D11_Async_BeginInvoke_m34B98E91FF75C69740667814A7B6C9F36619AACB,
	_LoadTextureD3D11_Async_EndInvoke_m3FF6CDC3813F295BAE1A6092EF7146324C8C3893,
	_LoadIntoTextureD3D11_Async__ctor_m9879AC70207713E135607AF5CC3981705171E87C,
	_LoadIntoTextureD3D11_Async_Invoke_mCFF1C259714CA960B49F05D0D3410B2938D2CF45,
	_LoadIntoTextureD3D11_Async_BeginInvoke_mDC1AE964B69D97BFAE67469BFD42AAC724E6270F,
	_LoadIntoTextureD3D11_Async_EndInvoke_mFB95D207572E83A8BE38368B7A463305077E9D72,
	_FreeTextureD3D11__ctor_mF9B7C1F8F6A72CAAF454F9AD88BC98FE017A7486,
	_FreeTextureD3D11_Invoke_m61D0D07D06EB3D92C5FD10BF7D29794389A5E4F0,
	_FreeTextureD3D11_BeginInvoke_mD2021D56EACEF98F6F14756726CC52580D5B9C4F,
	_FreeTextureD3D11_EndInvoke_mD547BC3069F24D8B3CDE3F4F9F6CC060F5760CF8,
	_GetRenderModelName__ctor_m3E589232DBF6A29FC087629E64C0B60713707EA9,
	_GetRenderModelName_Invoke_m200A8632C5AD2F59CD8E5C48F65F405288CA78D3,
	_GetRenderModelName_BeginInvoke_mD5267EE33088AD15D8ABB8D419EAB6436F5A7F02,
	_GetRenderModelName_EndInvoke_mD41C490461C1C4D7D0D2F39779086B73D8A48F9A,
	_GetRenderModelCount__ctor_m4AE7B041D0C3684416651E9A64BFBFB0B4F5CDD0,
	_GetRenderModelCount_Invoke_m89346EF43B893C6773E7AC0F58E4558AAFF8DAEE,
	_GetRenderModelCount_BeginInvoke_mAAEDC75B05F6B101DD7F5BCBE61954A7471A62A5,
	_GetRenderModelCount_EndInvoke_m63DAC5D3914E603B23EE72F6B7964A4F953B2BC3,
	_GetComponentCount__ctor_m2EFCB406D9F6F92806002EC60C099533007329A4,
	_GetComponentCount_Invoke_m378D0FFE040A80DA1EF99EA832DBF7D0329EC000,
	_GetComponentCount_BeginInvoke_mAF9AB925CF110A00F9ACAA53940D335A7A08DC2B,
	_GetComponentCount_EndInvoke_mE5C3B820CF3E438AB4657DD730D1FA50BEE0577B,
	_GetComponentName__ctor_m78951178BC4C67452DB5A2424A0B675AC31A6DA6,
	_GetComponentName_Invoke_m9796F8C20F8F78857B8F6D09DBBAB8D64B92DF6E,
	_GetComponentName_BeginInvoke_mF2BDBEA65F34713BD1E4DC586A9EAA5DED700F2E,
	_GetComponentName_EndInvoke_m08FCEF6F9C027D6FC0B0500E0CA6B63063026122,
	_GetComponentButtonMask__ctor_m1313FB209FB7F2F6F29D2C689CE04BF4663139B3,
	_GetComponentButtonMask_Invoke_m8E74CFBDE03B27FC1118C51289BF81BB6540912C,
	_GetComponentButtonMask_BeginInvoke_mFAADB11C728DB9D07D67C0796B11C7F89D4D6B25,
	_GetComponentButtonMask_EndInvoke_mAC3CE87E617A5A3458468A5F782B3CCFBB736B6B,
	_GetComponentRenderModelName__ctor_m5445FBF63542B869016DCA0E9CE7FFB26BF2DFB7,
	_GetComponentRenderModelName_Invoke_mE63FDF7517DE1318A07ADEB62F4DDC65269FA355,
	_GetComponentRenderModelName_BeginInvoke_m0A9A330B011C2059F96C934D1C5E9A767A7D88D6,
	_GetComponentRenderModelName_EndInvoke_mD37A36D2DFEDF7D500EFCF7CBCEFB247ABB3D8F0,
	_GetComponentStateForDevicePath__ctor_m50E29C5BBDD3BB33C2CC7F2F0BFE0571FD8DF1C8,
	_GetComponentStateForDevicePath_Invoke_m924D938A8A14FC3D0E3A400FCDE8912B7715B15A,
	_GetComponentStateForDevicePath_BeginInvoke_m9F3C90D41C2338FD1C84265BFDBF92538FC22ED9,
	_GetComponentStateForDevicePath_EndInvoke_m752456100009E6E1AAFB9C7F1042E6D47C5BE140,
	_GetComponentState__ctor_mD3078D2AE7E8EB2513875F6CA4CC0AC4510A6165,
	_GetComponentState_Invoke_m4061A96F0D734C87E399A35A794EB2897D0E4568,
	_GetComponentState_BeginInvoke_mB6C55CE5E9F3857CFB20CD776E06E0A8F9835999,
	_GetComponentState_EndInvoke_m057CBD32F68FD188A4CFA892EF6B0623470C8E59,
	_RenderModelHasComponent__ctor_mF09119F3FA1AC05667F24A82A02980F4C92C94F7,
	_RenderModelHasComponent_Invoke_mE23AB5AF6970C13020D42FE35CB19EF4F34AB263,
	_RenderModelHasComponent_BeginInvoke_m6840139073EE1917FC9E1A28A213C8BE43C07798,
	_RenderModelHasComponent_EndInvoke_m45295C97D75C6D9274EFC24939BACFE80D194C77,
	_GetRenderModelThumbnailURL__ctor_mDE0723E2BCCF58C3A386C394FBE6635C33A4800A,
	_GetRenderModelThumbnailURL_Invoke_m26D16B4DEC5936AC1EEB3FD87EEFFDD801A92331,
	_GetRenderModelThumbnailURL_BeginInvoke_m73E7B8F2E047D49C8EBAF87B5CAD2D64E917505B,
	_GetRenderModelThumbnailURL_EndInvoke_m22759841B1B5E49CDF2CA88842DF689C5111B151,
	_GetRenderModelOriginalPath__ctor_m2F304B30F5F56EECC15E622217B6537881C01E6D,
	_GetRenderModelOriginalPath_Invoke_mBBFE89ECAFC7864DFAF5F6A60100527818A68250,
	_GetRenderModelOriginalPath_BeginInvoke_m0C66CE5FEF0AF2362AA9C611B33AE13FAE6EA3F3,
	_GetRenderModelOriginalPath_EndInvoke_mAA2EE0B8D10F34F01DBEB76A9B9025DE0BFCAEEB,
	_GetRenderModelErrorNameFromEnum__ctor_m52F68EABC579A4FE9B3D8878E7F927831467D119,
	_GetRenderModelErrorNameFromEnum_Invoke_m86410F7C2696F1E144C7C2FEBD2180A46B7624E6,
	_GetRenderModelErrorNameFromEnum_BeginInvoke_m5C80002E1E46A41B8CCB26C2C030F3AB9550F6CC,
	_GetRenderModelErrorNameFromEnum_EndInvoke_m00D1C42A449272DAD713C306C4A87DAA4545E895,
	_CreateNotification__ctor_mF810D8938CE8A6E43881F1870B72A46B93096F96,
	_CreateNotification_Invoke_mD27F82596F3A824F2927C318AB3EC7E6EAA42E40,
	_CreateNotification_BeginInvoke_m66D5001A39C6DEDBA22449AF2F8CC0F06ED6F1CB,
	_CreateNotification_EndInvoke_m2F991F841DAFBB37BEA7F760B2B96A6AE79F3BB4,
	_RemoveNotification__ctor_mFCE7FB1E2E136EFE9C924E4C577074A19B1EE80D,
	_RemoveNotification_Invoke_m8AA21A01341AC63F7D52539B6DDA5DEB6D9455FB,
	_RemoveNotification_BeginInvoke_m546B088397ED53612B787E37919DCF438BE60F1E,
	_RemoveNotification_EndInvoke_mA98F63AC6E38C13ABBC89C3BD0DA275AC2B94806,
	_GetSettingsErrorNameFromEnum__ctor_mE589F8A4FAE6F6873C7BC286FC19AD6AA01FA78D,
	_GetSettingsErrorNameFromEnum_Invoke_m8ED5180BEE96D9058C9F3324E3CDB405DE3D1F34,
	_GetSettingsErrorNameFromEnum_BeginInvoke_m6E3A9961F8165534902C0403DA494436F15A2370,
	_GetSettingsErrorNameFromEnum_EndInvoke_m804F2CF8C6A61A076F4166A07FC6E71201327E1D,
	_Sync__ctor_mA76AC4C95534FCB00644A4F50218CF73D1132900,
	_Sync_Invoke_mB9AD7BC78BEB4FC3E32AE842183224F18917A197,
	_Sync_BeginInvoke_mF852A9B348FD4FD13C8B1B55965694BF24E3E0A5,
	_Sync_EndInvoke_mB6681200BD2F310F39BC40666F27856D7BE39DB6,
	_SetBool__ctor_m499F577942F2DAC6472A6E0C5600DEF2B0C5E309,
	_SetBool_Invoke_mC6941F6A38C4BEADCAAD8C11A0E3E943755C3927,
	_SetBool_BeginInvoke_m56CA56FD78D0C58917A93B3E5B1979F453BF6CFA,
	_SetBool_EndInvoke_m8D578A6E20FA0DBEA3E720D60DF4E498568D6A0F,
	_SetInt32__ctor_mABBD242F2DB1EDCABDD1783597568A6B005439FE,
	_SetInt32_Invoke_m3287CC5461338C29C166D9ACDB976251DDF545D8,
	_SetInt32_BeginInvoke_mE2277AF9D9B94E041123E7A87F872E6C91FE6E3A,
	_SetInt32_EndInvoke_m5C26F82B44033AD04501C4ADF667B64F19E06770,
	_SetFloat__ctor_m1FFD3A05F64135AD7CA9E744C69531FAD97D8C18,
	_SetFloat_Invoke_mCB87D40FB3F5AFD5F5991A1B726D1966C940E12C,
	_SetFloat_BeginInvoke_m02B4C2DA63351DCCDA87DEB6DE27C2321078DF6E,
	_SetFloat_EndInvoke_mF521CA84371E9D27F47FA4D1E98D7B5FB5A93B0B,
	_SetString__ctor_m66A0318642C76353E28B9D8D1DA1BFDF33EAC925,
	_SetString_Invoke_m2FD554712739C6A1862DFD58F70640521ABB04C5,
	_SetString_BeginInvoke_m702F665733139079F77E9780DF0ED203A63B9851,
	_SetString_EndInvoke_mD25EF8A4CF325D5B3351C712059849E039E3756F,
	_GetBool__ctor_mC39E50434C6DFEBDE3DFBECB283A20F50E4DCE48,
	_GetBool_Invoke_m3F3045C489BE3DD01D60B381519480379FB215ED,
	_GetBool_BeginInvoke_m7836255592B554B72EC1083C731BC3E59C3B2292,
	_GetBool_EndInvoke_m3B65552E51AA7A0FF7A89C48F2E4E85A9D9EBCDC,
	_GetInt32__ctor_mC65AD668CDCC66E1D5B84213461896A674FFE356,
	_GetInt32_Invoke_mDBDF2AA2C6F54BF8966FA6E8B7F817398426C4A0,
	_GetInt32_BeginInvoke_m015E8B7A19A5854F1E5E470B54809AB2AD98104A,
	_GetInt32_EndInvoke_mD7086FD605AE3BBD7045578184A7068D5C130E68,
	_GetFloat__ctor_m77344D18FBB1AE941D3F375812442BA20F73D952,
	_GetFloat_Invoke_m096D02C3BB75317E9CD0EE82CABC2FDD3D6C4C48,
	_GetFloat_BeginInvoke_m256F196A0C75285A6A5627323D8594F3F943B5E0,
	_GetFloat_EndInvoke_m7834E091775D167FA7A591123640ECDF0FDEDD48,
	_GetString__ctor_m4CDCAFC647C7AFA5620533AF7D4FFC1230E44109,
	_GetString_Invoke_mCAE4FDBB326CF0DA13E2028E0676A86BE7F0C722,
	_GetString_BeginInvoke_m0CC3A728AD54DE86FC32FFD5ABE939D10BDB2C45,
	_GetString_EndInvoke_mC9AA7F2308647F9C310F060C489AF4902DBD31A3,
	_RemoveSection__ctor_mE6EEFC85BAA1337DFA69AE7DCEB5EDDEE427CF5C,
	_RemoveSection_Invoke_mD8217E3568C163B8B1EFC7B90B65A28DD4DC3A82,
	_RemoveSection_BeginInvoke_mF8E3C3F8721D2E03044052C067D00AE198519B7C,
	_RemoveSection_EndInvoke_m32D0937298CD9D053361CD2AB15A9894B935373D,
	_RemoveKeyInSection__ctor_mDFA208188AADDEAA0BB5E6BAA5F10BAEC3933DD2,
	_RemoveKeyInSection_Invoke_m7BD96B79CCA3504AA6932B6893B932202EE806C8,
	_RemoveKeyInSection_BeginInvoke_mE29918A47D4F25444B60D61406805F366E53E1CC,
	_RemoveKeyInSection_EndInvoke_mDBDC90FBF1E5E6E3464388CF876AE64D5F5B7FFC,
	_RequestScreenshot__ctor_mEEC1B17A2D87D88DFE0BB120A239D27F6CD6C7BB,
	_RequestScreenshot_Invoke_mB33CA212B36AB4D631C675F890A3B7C3653A11E3,
	_RequestScreenshot_BeginInvoke_mB23ACC31C3827048270BFB5BFDE9573820F17477,
	_RequestScreenshot_EndInvoke_m9BF3EFBA56A2C283020956EB719DAEBC310F3CF9,
	_HookScreenshot__ctor_m84BE72DEAB329DEEB44548365E955685CFE9E40D,
	_HookScreenshot_Invoke_m2B5CE1BEF0034368CE56DF8A22FE2298F8469343,
	_HookScreenshot_BeginInvoke_m67C4AA4EA1AD71BAD9AB9ADFC1D454D6FD02BAF1,
	_HookScreenshot_EndInvoke_m07B2E63D7C7289BE544FFC0624D2AC0F83344262,
	_GetScreenshotPropertyType__ctor_mE0345A0B5E350DFEB9AFB547CDD4F8DB9978EA65,
	_GetScreenshotPropertyType_Invoke_mF8CD46B8AB894888A5661B00B1A5ECD0C8A41235,
	_GetScreenshotPropertyType_BeginInvoke_mA9DE84A7D2C1BF6C7674B3C23C867E3DB6864BD0,
	_GetScreenshotPropertyType_EndInvoke_m9200F27491967157EA70B5BCEFE270BD6A304478,
	_GetScreenshotPropertyFilename__ctor_mCAB6F0C8B246F8726690438B030F17B5B630D0EC,
	_GetScreenshotPropertyFilename_Invoke_m90A7C6C04A2A5BEFB6343E90959AA0D492DE30A7,
	_GetScreenshotPropertyFilename_BeginInvoke_m36351DA154B773A8CF8F2786CE62369D9A96BA76,
	_GetScreenshotPropertyFilename_EndInvoke_m64945FD3610C1A1A55C5250D20F429884558F7A9,
	_UpdateScreenshotProgress__ctor_mD6841C55A41AF5134B993BB8DDA313A492DB21BC,
	_UpdateScreenshotProgress_Invoke_m362F2BC9EF2C7826529BB98A0D0417A7D2F47B0B,
	_UpdateScreenshotProgress_BeginInvoke_mDC925F01D57F66BB74670B2E0EAA213BD9030CE3,
	_UpdateScreenshotProgress_EndInvoke_m7B3AB41FD87A36CAAEFC1375E3AFE08E0FA306DF,
	_TakeStereoScreenshot__ctor_m60BFCE43061797F2B9775937825714A19E2D5DB6,
	_TakeStereoScreenshot_Invoke_mE8C5D569FFC056DA1AEB7269365E2C6A3A945112,
	_TakeStereoScreenshot_BeginInvoke_mA6BE453E226C343B4762DD59EC2B5B021D1A8364,
	_TakeStereoScreenshot_EndInvoke_m7304AFB00AB27D438CDC4962053A9ABF4F8025C0,
	_SubmitScreenshot__ctor_m6DA149ECE992839B9C946705710D37F0A42950ED,
	_SubmitScreenshot_Invoke_mB1B624AFF1810BB56C7F4561D77C44FD5BC83635,
	_SubmitScreenshot_BeginInvoke_m41BEEA9735E202235DBB43FE6C9CE524FA8FD02D,
	_SubmitScreenshot_EndInvoke_m1C2F2EECAC86A7A31C62592A2C14378B14AA188F,
	_LoadSharedResource__ctor_m0117420D62F761DF6A7B5DD4C1A59D4293067C4E,
	_LoadSharedResource_Invoke_m4B62B034CD75DEEF0395BA3DE4289D26C3C47AA8,
	_LoadSharedResource_BeginInvoke_m8BBCD3CC77F3D56126043774F6BC4EB4B108E2EF,
	_LoadSharedResource_EndInvoke_mEF903A1922E1C834046FB84B5012DE6E317F93B5,
	_GetResourceFullPath__ctor_m9E9CF4DEAB2B7CC915127D881BA7868E13F01266,
	_GetResourceFullPath_Invoke_m5C56A9DF43CDC774D521611A33BCB9847127C16D,
	_GetResourceFullPath_BeginInvoke_m8810720C8711AB5FF69E0900CDB1E132A5CC6D2D,
	_GetResourceFullPath_EndInvoke_m30D1AB6E5121FD1FE45A7B9ABC13397C7AEB96C6,
	_GetDriverCount__ctor_m85F277B423F26C7C8A69C91570AD692C32B1D311,
	_GetDriverCount_Invoke_m5B3DAE84A60E24EA2190832FD1EEFAB77BC04D49,
	_GetDriverCount_BeginInvoke_mD6563F0C6035469D1302AA2A3A467935EFA47538,
	_GetDriverCount_EndInvoke_m13F75F39E56C9BC6C09B16305E67889FB03256A2,
	_GetDriverName__ctor_mAA55D266ABD845C0E67B5A17A901B6ACFF4B5EC7,
	_GetDriverName_Invoke_m6F095C926F1AEDD40BA95F786AE931957C8D62CE,
	_GetDriverName_BeginInvoke_m4558634514E9252F905854A0EC9C5BEF8791EFB8,
	_GetDriverName_EndInvoke_mE4E33C093868709DD4A361744EDF36CED6246578,
	_GetDriverHandle__ctor_mFD7AD43C2571AB7A75063E538A57EB113EE5E3EF,
	_GetDriverHandle_Invoke_mC741AFC84102AEAD344AC5074455C6B46AA8B57A,
	_GetDriverHandle_BeginInvoke_mD32DDAE693582FEA2597951BE6EA2DBB00859EC8,
	_GetDriverHandle_EndInvoke_m5EA15FBB967AFA33EBA97B28090EDD99CC1D0A44,
	_SetActionManifestPath__ctor_mBDA42F4CB6A73D116F4EF82C3E6C6A81C2E6D63D,
	_SetActionManifestPath_Invoke_mA927319DFC9C89D70215B33D9C9A747DFEBE74E7,
	_SetActionManifestPath_BeginInvoke_mF7131ACB08E0510F0666AED7120D00218147E88B,
	_SetActionManifestPath_EndInvoke_mE22089E97D954796469EAC147608BA0F231F73C8,
	_GetActionSetHandle__ctor_m9788922B610A6443477872FB192A13F8645A9510,
	_GetActionSetHandle_Invoke_mC7F228DD87BCFDBAAC644FEEA962159088A1823A,
	_GetActionSetHandle_BeginInvoke_m3E9BFA83762436DE86D929E97A2B62313C9DA740,
	_GetActionSetHandle_EndInvoke_m5ED9CF0A18720CB15BB6CAF76599DF9AF1113E46,
	_GetActionHandle__ctor_m166AEC6488E6C34FC7E0665EB4BB6DD279FBFCB5,
	_GetActionHandle_Invoke_mE9A67BE0386CD7533CB86F7A86778B0EB668F890,
	_GetActionHandle_BeginInvoke_mA736AF15C431A9B3BACA13BC5B1032D6D57AD60E,
	_GetActionHandle_EndInvoke_mA4C3295EACB991CAE705297594B224D34522FE43,
	_GetInputSourceHandle__ctor_m992F2F6166FC9B1390D630D2747AA8A1176294F0,
	_GetInputSourceHandle_Invoke_m51C1A05C888684C0B15BD9D028BD6FB31F95C39A,
	_GetInputSourceHandle_BeginInvoke_mDEAF0DEF1E7E5CFCEE47311661E9895D92E61AE3,
	_GetInputSourceHandle_EndInvoke_mEC34EC7C9AEBFDAB6177CC686943A21EBF133C87,
	_UpdateActionState__ctor_mCBB4CEF3E973C5AAF67AFCEFDA7DA5742D917B27,
	_UpdateActionState_Invoke_m4DE270627046CE992C58CF512A3685804B229C27,
	_UpdateActionState_BeginInvoke_mAC994A48687A9E3263D502B739F4EEEF0816BACC,
	_UpdateActionState_EndInvoke_m8A13215D972EE9016C58895DF81A7E312A04C39A,
	_GetDigitalActionData__ctor_m7C999081D19EA1D6551633C1522681CDB73E161F,
	_GetDigitalActionData_Invoke_m15F10297BCD44BD5655CF6F459BC9C3050825DAA,
	_GetDigitalActionData_BeginInvoke_m03D0855B960F95E3FEAB98B3B3A1C2A7F9F95CFB,
	_GetDigitalActionData_EndInvoke_m10A1B6FD3CABF9F82CD5D13D4F658BC8277CA660,
	_GetAnalogActionData__ctor_mC7E6E7B52E7C89525805C738A19D70419A216F18,
	_GetAnalogActionData_Invoke_mF1043DB40F4E5C8BD31688E5B499611F91B303BF,
	_GetAnalogActionData_BeginInvoke_mFD1811901FCE970CADD8A75D9BEE827F002A954D,
	_GetAnalogActionData_EndInvoke_mD9DCCD49A432958F65B1A9FD89B6DE8B96C13353,
	_GetPoseActionData__ctor_m92CFC2B4FDCCF7C9A8C61DF88967A1059E446DDE,
	_GetPoseActionData_Invoke_m016C739E4381E5D4F7DEE376ADDCC8B3FFD4E6C7,
	_GetPoseActionData_BeginInvoke_mB3E0E1551DB1E48F8C6832BF5FC693E0937A8752,
	_GetPoseActionData_EndInvoke_mA0D4ACED408AF7E96B24445CC1D0649A487A8BC8,
	_GetSkeletalActionData__ctor_m6BB68B75FF58F0A5BF015A838D3F1204211400AD,
	_GetSkeletalActionData_Invoke_mE57966796C5BE36FFE214BBC0F348A8B787B1AE6,
	_GetSkeletalActionData_BeginInvoke_mB0B12E82C8730340B6A3561D1BB575AA2C10E991,
	_GetSkeletalActionData_EndInvoke_m4F26CB5E8F87AFA0AF0DC43AC07FF8EE81366A3D,
	_GetSkeletalBoneData__ctor_m6B3389C5C4665A3E0C5DACA5C9FA2B30F17535D2,
	_GetSkeletalBoneData_Invoke_mC44B0E5F77A2C98FFEC93D08E505EA0985F2D35A,
	_GetSkeletalBoneData_BeginInvoke_m539DCAB13C20E2DA46F4435589DE0C291CDDAA72,
	_GetSkeletalBoneData_EndInvoke_mF6681A2DC873ED4DACCB5982151427EBBDBD5944,
	_GetSkeletalBoneDataCompressed__ctor_m8FB0D5F6FA6B001D7FD280FFAB724538F4BB1581,
	_GetSkeletalBoneDataCompressed_Invoke_m16716B0602B22A27DA77EA7C32AFBD2DA671F85A,
	_GetSkeletalBoneDataCompressed_BeginInvoke_m5928CD244897EDFF7E436704ED37E885075AA6B6,
	_GetSkeletalBoneDataCompressed_EndInvoke_m1AFAF87BC33992CC03BCA8F0E74E4D57A8002E06,
	_DecompressSkeletalBoneData__ctor_mB762A7CF065CDEA3D2B97C2D4ED23DC4DD131A92,
	_DecompressSkeletalBoneData_Invoke_mCBA1157817D3B9747AAFF6BDD7AB7328DB438D02,
	_DecompressSkeletalBoneData_BeginInvoke_m63A18E0450F3EFFD89CCDF5F610D45CCA4971556,
	_DecompressSkeletalBoneData_EndInvoke_m8A1B464FD10193DAA9304166BE1B1C68DAF4E8CA,
	_TriggerHapticVibrationAction__ctor_m1728125E656BE179825C75BBC7A70A06DE65014A,
	_TriggerHapticVibrationAction_Invoke_mEBB4C9F2912BD3613EFFE2EC4A30E5015BD11ABD,
	_TriggerHapticVibrationAction_BeginInvoke_m8598EE2291794CDF50B8B8C61BA21A33975D1DFC,
	_TriggerHapticVibrationAction_EndInvoke_mCCF420140CB3D0017CDE27D71165DF0F403AC371,
	_GetActionOrigins__ctor_m4DE0F00231B229B89F36F47BB81BAFC3C5611E7C,
	_GetActionOrigins_Invoke_mBFDEB2A34B77428DA11C9B051E4EFA3A295EC986,
	_GetActionOrigins_BeginInvoke_m087CFABDF82A1A7EA7D044A9320DC960A26B0C3B,
	_GetActionOrigins_EndInvoke_mDCC74CB33E619A7DCAA1FAE1BE8512629A799547,
	_GetOriginLocalizedName__ctor_m3A500313DD259B70F5AFABFF3213E61ED5218560,
	_GetOriginLocalizedName_Invoke_m17F7733E8117EDBC145E09A3CFBDDD2603295E54,
	_GetOriginLocalizedName_BeginInvoke_m4F33EF3A93340230DB33FBE43671B94AC43759B8,
	_GetOriginLocalizedName_EndInvoke_mF38052FD677BB2C30189992ED685EA9CE7552BCA,
	_GetOriginTrackedDeviceInfo__ctor_mB29F991D7D7D3A1AF6456B7C4C2B145277FAF264,
	_GetOriginTrackedDeviceInfo_Invoke_m7E4B43D751C539738E8FF3EC98335B9804239857,
	_GetOriginTrackedDeviceInfo_BeginInvoke_m344F7B9E80F24141AB9A4803C48BCE0AFAF19900,
	_GetOriginTrackedDeviceInfo_EndInvoke_m76AC7711260D0287E6AF029995386C4CCBE7028A,
	_ShowActionOrigins__ctor_mEE24447B626CB66F5D05AAA458980252D76409A3,
	_ShowActionOrigins_Invoke_m42F6268ED89CA85AA3D4744E1E18F5D8E79A5299,
	_ShowActionOrigins_BeginInvoke_m6BE471D4A847EF030080040225C037105906FEEA,
	_ShowActionOrigins_EndInvoke_m15AA37E61C73BF44C9C4F9BA5E934DCD21E8556F,
	_ShowBindingsForActionSet__ctor_mBDA0908B56E4FCE00F7D068D3B8BC623501B9855,
	_ShowBindingsForActionSet_Invoke_mC240D324B0D619597B8D64A848EC49B1C8D86130,
	_ShowBindingsForActionSet_BeginInvoke_m944557D1ADA85B229ED30F5B2C3B1B276A838DA3,
	_ShowBindingsForActionSet_EndInvoke_mE270C3A50EFF3A99F7E25C81C64A7F2CAC7A5CF3,
	_Open__ctor_m2FC011AB1A37AE4040C73B801644A29F6604FB00,
	_Open_Invoke_m1A2797E8B4E9C586C4942E26AF855D3D0B0E4EDB,
	_Open_BeginInvoke_mD9380C9012D886BBD3FF7E6652CE003A35853D80,
	_Open_EndInvoke_m00D1CB56895F98A938533F7445981E24D3406AD2,
	_Close__ctor_m7FF71BF7078445866EF41F876E1674A35D609131,
	_Close_Invoke_mDEC973EF48BBC719C41449F7E031067368B42AA0,
	_Close_BeginInvoke_m2BC162C96DBF7C5FAB5722E2986EF8FABC40FE31,
	_Close_EndInvoke_m9869BE158F32D057F3CED444384BD20AC0009EEC,
	_Read__ctor_m7F8FEA23B4894A5619594DEFE5C4EF7E3126A889,
	_Read_Invoke_m007C981BB0E664D85B9FF6357EFBAFC68CB206FD,
	_Read_BeginInvoke_mB2CC226866FA963D8DA09AF09665EB0774FD2B23,
	_Read_EndInvoke_m1010FB6DC93828F8DE6A3CD9C50CC1492B6DB19F,
	_Write__ctor_m9E1272735D1D65D06790D5619A30F98FB0F588CE,
	_Write_Invoke_m197BCB2FE9F66CDB5878B36D5CDA3A626BE6332F,
	_Write_BeginInvoke_m5B05FE12F3BB8AA80B6B2E5CC5B3C33B6BA68910,
	_Write_EndInvoke_mCDAF9C96AA317498872D0211F611AA9EABE9FB43,
	_PropertyContainer__ctor_mB48F699F7F6EFD4D095CD91242745F5F2912F3C6,
	_PropertyContainer_Invoke_m5FF10619A007FB65CC7B661587BBB63E1300E035,
	_PropertyContainer_BeginInvoke_m749D93BC2385AEB637E22D3D11386F00BFB194F0,
	_PropertyContainer_EndInvoke_mCDE008092C39939932EC1EBB9E99E4BA96497F82,
	_CreateSpatialAnchorFromDescriptor__ctor_mD452AA9A9B00A381253D40E548CD11E3F4778982,
	_CreateSpatialAnchorFromDescriptor_Invoke_mC858B0398D4467F7A3FA9B69DE8DC53174D386A0,
	_CreateSpatialAnchorFromDescriptor_BeginInvoke_m43EC2F2FE96A07625050EA8EFFC9FBB3D9E2DABC,
	_CreateSpatialAnchorFromDescriptor_EndInvoke_m2917D63E416E57490B2E54791EBD0981C517E395,
	_CreateSpatialAnchorFromPose__ctor_mDF112FE98437171D35E4DD8D2A2EC3524D209AE8,
	_CreateSpatialAnchorFromPose_Invoke_m99F078E0A862F752AC5EF557048EBC706F0792F4,
	_CreateSpatialAnchorFromPose_BeginInvoke_m400A1168710BF0350D10016F8C10B7EAD3CBF6C6,
	_CreateSpatialAnchorFromPose_EndInvoke_m7FF8B819FF62D4A29BD48732C1EB677519FDFE25,
	_GetSpatialAnchorPose__ctor_m0DB7EB2F298BC3D34C89BC94A2CDDB3EC88DC2DF,
	_GetSpatialAnchorPose_Invoke_m8DDEACBDEA0FD8123BCAB195C75F5EC07314D5F9,
	_GetSpatialAnchorPose_BeginInvoke_m25A23E61CD2443975E92B824736D5CB4FC64A635,
	_GetSpatialAnchorPose_EndInvoke_m89E274D1CCA1AE0CE881D3208183E06EA408BA8B,
	_GetSpatialAnchorDescriptor__ctor_mC917DFF43B19A710BFD315743305E9462B6273CF,
	_GetSpatialAnchorDescriptor_Invoke_m1FCDD9D757299ECA528DE338A6B13B14BDA4C8F9,
	_GetSpatialAnchorDescriptor_BeginInvoke_m37CAE8036505EEC48C8280439E18B0717A1E7D6A,
	_GetSpatialAnchorDescriptor_EndInvoke_m8CF65A0B348D06E364765D4B3CBDFE58585B5F37,
	CVRSystem__ctor_m8FCC9BD7F9B566DA066E60BFB243102C3103018A,
	CVRSystem_GetRecommendedRenderTargetSize_mD441ECC64DFC5BA364A7FD8701D98F25F8BC338D,
	CVRSystem_GetProjectionMatrix_m1C9AC3850AA9EA8B1DA48D369CD1C8C6C607D532,
	CVRSystem_GetProjectionRaw_m5F99F1FE634FD7A638939628942DD83CCC72F602,
	CVRSystem_ComputeDistortion_m27FC45ADA2305AD1D7DB5C61390485C1BFEE36C7,
	CVRSystem_GetEyeToHeadTransform_mA1AA5A712366E0C943A872DDB9E47CD1991372F1,
	CVRSystem_GetTimeSinceLastVsync_m497AE07C1570B2961E9863331D4654B0BC0B0BCB,
	CVRSystem_GetD3D9AdapterIndex_m866A065A21065B8523BA4E29BFFE2E2162B82F61,
	CVRSystem_GetDXGIOutputInfo_mD06FFD9DB231196EE4C6FB86541C065096BA76BF,
	CVRSystem_GetOutputDevice_m3100238E25E01924A4D8D678E8ED5547126B8296,
	CVRSystem_IsDisplayOnDesktop_mBDF6386424BFC32835A72A2910D6DD9AE31C9E01,
	CVRSystem_SetDisplayVisibility_m7C096A0304BD0D75817A16E9F2490FE92C7C3A42,
	CVRSystem_GetDeviceToAbsoluteTrackingPose_m71877595C5BB6E4503B5F5F9BC4401B60B35339A,
	CVRSystem_ResetSeatedZeroPose_m65D7655C353E0A4F04F715C19A9FBF0FD6D0C59B,
	CVRSystem_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_m572E6F0BFA580A289D104AD59A1BE6070850EF66,
	CVRSystem_GetRawZeroPoseToStandingAbsoluteTrackingPose_m9067E136FE7C1DF3D42CBF40E14EC27FCA38BB89,
	CVRSystem_GetSortedTrackedDeviceIndicesOfClass_m480099804814374D87CE808FF0768DC05836D8D8,
	CVRSystem_GetTrackedDeviceActivityLevel_m9254C0CEE91B09F46E6C90BCD2D9BC9B1F6400F3,
	CVRSystem_ApplyTransform_mC2A1A08DB6C52C78BC2EC5BB3BB74D4B35C5A74D,
	CVRSystem_GetTrackedDeviceIndexForControllerRole_m3C88C0683D0964EF9EEE51D8324349D1E9271EF1,
	CVRSystem_GetControllerRoleForTrackedDeviceIndex_m8E9E1CE89ADBBF17385C2DF0918BCF655243C00E,
	CVRSystem_GetTrackedDeviceClass_m583F1CE9A06B487A2FE634466C1DDBC98DECB5FB,
	CVRSystem_IsTrackedDeviceConnected_m2F1232CC01B1DF0770DA5E515FBFC68EADB6A37F,
	CVRSystem_GetBoolTrackedDeviceProperty_m5346AA63329D322916A63E613A305E125F256218,
	CVRSystem_GetFloatTrackedDeviceProperty_m5236D6A92A06CED44224F7687E208C75BA30AB1C,
	CVRSystem_GetInt32TrackedDeviceProperty_m998F0341B26784EA9B840511AE05096F5E36FB9D,
	CVRSystem_GetUint64TrackedDeviceProperty_m676340ACAB693CE102A2DD16E0ED9E52D1DD7C0E,
	CVRSystem_GetMatrix34TrackedDeviceProperty_mA3F63DB96911166CCEA9F21387A2F850E001CAFB,
	CVRSystem_GetArrayTrackedDeviceProperty_mB730820A15645F20A6BD6DABB4FAF0363C6E4658,
	CVRSystem_GetStringTrackedDeviceProperty_m1BA1B2501A925CB78FD751B3F9DE6512B3D158DF,
	CVRSystem_GetPropErrorNameFromEnum_mF4B6B9646F248C4F94E1DDDD68391671FF51C8FD,
	CVRSystem_PollNextEvent_m04E43888B8F87567C822418D775A0066CA615DF7,
	CVRSystem_PollNextEventWithPose_mA6909A97C8BA1C13E3BB1679E53337FA95DA573B,
	CVRSystem_GetEventTypeNameFromEnum_m1004ED334320B588D4359A9130977791C159FF3B,
	CVRSystem_GetHiddenAreaMesh_m37139681F4F511417FEF36B1BA799FFA9531B88B,
	CVRSystem_GetControllerState_mD043B5DF3CB837AF884A281F6F9173F8C89355DB,
	CVRSystem_GetControllerStateWithPose_mD53B1D9C8C417A77FE9DF230AD61B55BB0C9B919,
	CVRSystem_TriggerHapticPulse_m152B442C19D76B73D5B98FBAD57B1F44A728EC4E,
	CVRSystem_GetButtonIdNameFromEnum_m615274D507BD24BB53C0788AD647152163FECBBB,
	CVRSystem_GetControllerAxisTypeNameFromEnum_mBCA6EB99917B93A3B15B28D84ABAFEC2972C95A7,
	CVRSystem_IsInputAvailable_m13ABBFE4BBE7AE7EDD1C68303C91403808B012F8,
	CVRSystem_IsSteamVRDrawingControllers_m2BB1ABE186019E54D4A8D93A43C2E9F97089E452,
	CVRSystem_ShouldApplicationPause_mFF841BA12BFDCA0B952A5B8E0EF84DAACBE3F65C,
	CVRSystem_ShouldApplicationReduceRenderingWork_mD66F7797A2F1A00836869C2486F3ED8D6978494C,
	CVRSystem_DriverDebugRequest_mB20BADD7B6B15E14CE08C08C689FBCEAA03E5CFC,
	CVRSystem_PerformFirmwareUpdate_m12308AE683BB0D6D1D6F03BAEBC5942F8919501A,
	CVRSystem_AcknowledgeQuit_Exiting_m4D2B7A417F2CA761BFC46B51AF934F4836D4742F,
	CVRSystem_AcknowledgeQuit_UserPrompt_m5062A90BB06CE6EDBECF037F0049A9086F3D1F3E,
	_PollNextEventPacked__ctor_m11F94910D234D3F591E8D0759A1F7B4390E91CF3,
	_PollNextEventPacked_Invoke_m78BF2E37E8021A6B92A25586A0B6193EE2B79478,
	_PollNextEventPacked_BeginInvoke_m96B394D4122E331CBD4FD6A6D6CC54CB48429C5E,
	_PollNextEventPacked_EndInvoke_mD4F41A710BA50446799367CF53D9169BC2326482,
	_GetControllerStatePacked__ctor_mD596EC82CDEC27284AA8211ABCBD2769AAB8192D,
	_GetControllerStatePacked_Invoke_m6461239365042650A34C363CA8F81CBDCC916EC8,
	_GetControllerStatePacked_BeginInvoke_m28B346E036D2090D2D3CAEDADDB7AFA31C042ACA,
	_GetControllerStatePacked_EndInvoke_m586751E78A18B38DB21062620F4235B62C8FAC48,
	_GetControllerStateWithPosePacked__ctor_m11FE22C3793473344173CEFE286F40C815F157A6,
	_GetControllerStateWithPosePacked_Invoke_mC8454DB592377E156C19DAA8F09DC0219382F69A,
	_GetControllerStateWithPosePacked_BeginInvoke_m21D6E5070D4E08D67DF4AD6E69D2AE535B001064,
	_GetControllerStateWithPosePacked_EndInvoke_mA73480310FB55E9443DAD95B7C60FB6F44A6F3A6,
	CVRExtendedDisplay__ctor_m684BAED95BE1BA4E0D20800A922AC93B7EB37963,
	CVRExtendedDisplay_GetWindowBounds_m9F26FB7C6CC29B312044DBB4D5EB760B530CC6F4,
	CVRExtendedDisplay_GetEyeOutputViewport_m5A94D47B8FB2E14092F5AE07514921943BCACCC3,
	CVRExtendedDisplay_GetDXGIOutputInfo_mCFD9805179F0BF4354EE94D170A52234B1EA1E1E,
	CVRTrackedCamera__ctor_mBA8FCE2F5C6DF9701E852CC4FD22892C18A09790,
	CVRTrackedCamera_GetCameraErrorNameFromEnum_m5D9E13858193F28CC85133D763FE240187D8E891,
	CVRTrackedCamera_HasCamera_mBDD23E5ED02A4D79A419B091BDA2CAEBD1A72B48,
	CVRTrackedCamera_GetCameraFrameSize_mC880097C8FCD14EB1BE823EF71023D789670DAE1,
	CVRTrackedCamera_GetCameraIntrinsics_m98E27C54985CACFAC478B8DE58654AE6124AFEAF,
	CVRTrackedCamera_GetCameraProjection_m5900B780FB1A05D919D3FB01A18DCAF341282D5C,
	CVRTrackedCamera_AcquireVideoStreamingService_mA1A8CC84E468B064CDF47C01F1704C24CDA47B39,
	CVRTrackedCamera_ReleaseVideoStreamingService_m89A88991316B0387F972D1202B1D8D809AB736BD,
	CVRTrackedCamera_GetVideoStreamFrameBuffer_m789788B06DAF5170108E8C81320C9276A3454A43,
	CVRTrackedCamera_GetVideoStreamTextureSize_m06303B3E8B3D090B61CE936D1DB7E1F9531B15FB,
	CVRTrackedCamera_GetVideoStreamTextureD3D11_m65B4B2F554F2FAA5601FA45FD0F0AD8F52E09D32,
	CVRTrackedCamera_GetVideoStreamTextureGL_m75DAE7D1F133F5DAD6D42A111DD678D8D433BCA0,
	CVRTrackedCamera_ReleaseVideoStreamTextureGL_m9F791E3D4EE7E07E81AA5056D480303F0418FDFA,
	CVRApplications__ctor_m10ED622413314220B002A0F1A921EA177394018F,
	CVRApplications_AddApplicationManifest_mE565C72D0A5432D334AF76F89978366FD65CDCDD,
	CVRApplications_RemoveApplicationManifest_m6E8B0C3F4B64822D6C66BC31A81D2282CFDF5C2B,
	CVRApplications_IsApplicationInstalled_mA3A2D0BDF4FFB8DA038972949A359F76B46726B6,
	CVRApplications_GetApplicationCount_mD6D680D215E3FD12780CEA700085DDEEE047BC18,
	CVRApplications_GetApplicationKeyByIndex_m5E27B8122546B45B932C3D91A000416DEAFEB64A,
	CVRApplications_GetApplicationKeyByProcessId_mD063F4B30D011E795CE3356CEE45231F9DA9AB26,
	CVRApplications_LaunchApplication_mBA8D7878655B2695705BF6DB8AD1E56E7242FF29,
	CVRApplications_LaunchTemplateApplication_mA0272F708F81F60D68F6D1428DACBFF5BC2C9493,
	CVRApplications_LaunchApplicationFromMimeType_m283844B6929A737A39EAD06EF91B0BF5EE1F9279,
	CVRApplications_LaunchDashboardOverlay_m3E3F8C27C876E0113556A2CA64A0B175BD988F40,
	CVRApplications_CancelApplicationLaunch_m65694406E0C52FE3B771429A7CE058C9793357AA,
	CVRApplications_IdentifyApplication_m82295F9A85AAE04AC7447C33AC5FB337F660C671,
	CVRApplications_GetApplicationProcessId_m3A427522792790EA4074B88FDB0E28A463EC9480,
	CVRApplications_GetApplicationsErrorNameFromEnum_m82F732844B783AFEDECAB68FC3846F65FB327125,
	CVRApplications_GetApplicationPropertyString_mFFE0E32284D2F45957882B0D7BDF4933F923F0E0,
	CVRApplications_GetApplicationPropertyBool_m856BFB73D9D048C8D9483CA1809E7E0B685F554B,
	CVRApplications_GetApplicationPropertyUint64_mF7B2C1C34758B564F5D14CCCAD2BD87E73DBFF63,
	CVRApplications_SetApplicationAutoLaunch_m76C6C48380907CB3BC2F69689558339835ACA670,
	CVRApplications_GetApplicationAutoLaunch_m9587D5E2729B6292574367A856AA5643A96EC835,
	CVRApplications_SetDefaultApplicationForMimeType_mCD17C90794ECBD97677800515354BDDF99B2F06E,
	CVRApplications_GetDefaultApplicationForMimeType_m07BE0B8FF6E4B402F18AF4E4FCF6F86015EE9E7D,
	CVRApplications_GetApplicationSupportedMimeTypes_mE1D81848BC6F43F0B7F31E89B693AD0F1A9C6ABB,
	CVRApplications_GetApplicationsThatSupportMimeType_mC246AB5418331EAA4AB1C55F6962C3C3AAB74FAD,
	CVRApplications_GetApplicationLaunchArguments_m9736069BD9FE93C02A802A36EAF675E41FE64896,
	CVRApplications_GetStartingApplication_mF9E844C256525FF6B60B2F2835D0DB946E4A9332,
	CVRApplications_GetTransitionState_m049274D331090D805641F8B9DCEDA00C9B6A2EB3,
	CVRApplications_PerformApplicationPrelaunchCheck_mEBD16B62160A3E22875ED3A2796C3CB817DC3419,
	CVRApplications_GetApplicationsTransitionStateNameFromEnum_mF43F8DD46DCBFD922316B3DE7839F38EDC64C509,
	CVRApplications_IsQuitUserPromptRequested_m1043E723C1915BB122F7A7110EBE63D55204D06F,
	CVRApplications_LaunchInternalProcess_mAD63DECBB6E51DBF95804E0C0B8AE2F829BAC993,
	CVRApplications_GetCurrentSceneProcessId_mA0EAF28554F86AC5BF8B3B60E8BC40CB0C4B5EA3,
	CVRChaperone__ctor_mB9037906CD47B436FF1540AED99E358899764193,
	CVRChaperone_GetCalibrationState_m9DAE56F4B2B89906DFF72FD93C958C35FF7B72D1,
	CVRChaperone_GetPlayAreaSize_mE413B355569CBC34537847357D7D096A268ADD4C,
	CVRChaperone_GetPlayAreaRect_m0385F1149EEB05783BAA04310D8AE369F3EFC326,
	CVRChaperone_ReloadInfo_m7833D2C132BA981FA30BB89FBDC218200CD03604,
	CVRChaperone_SetSceneColor_m43295A91E09CD7E51FFA65455C9110BE268FA970,
	CVRChaperone_GetBoundsColor_mFBD8FFF0426B7B2B3D116D1983C750FD7EBC6B10,
	CVRChaperone_AreBoundsVisible_m4C498A807B29A68C9B14E88A29118C6F497E58FF,
	CVRChaperone_ForceBoundsVisible_mC75843B1495B2D7BF2108FC963FC67984134F301,
	CVRChaperoneSetup__ctor_mE796C0271DB69900F9D6CC891613A2B546D8DBB9,
	CVRChaperoneSetup_CommitWorkingCopy_m7C70F797E171B944E47C8AD10CA7C4EDCC13AD47,
	CVRChaperoneSetup_RevertWorkingCopy_m6FE31FF446DF3CAEB5D14AE72155681AEDB9B599,
	CVRChaperoneSetup_GetWorkingPlayAreaSize_m1470E241E78F43A44E562965B966E3B0ABB60C13,
	CVRChaperoneSetup_GetWorkingPlayAreaRect_mE48616155F75ED657A9C9B3DB52487C0F3447ECF,
	CVRChaperoneSetup_GetWorkingCollisionBoundsInfo_m9C51C85881779F68C6D9474D47F33F04CC64B0F4,
	CVRChaperoneSetup_GetLiveCollisionBoundsInfo_m4A7D58BE41A1CE876CBED2F1C3447E63FAE42660,
	CVRChaperoneSetup_GetWorkingSeatedZeroPoseToRawTrackingPose_mB9F856508E014F40071CD4186F2ABA6B64CDADF9,
	CVRChaperoneSetup_GetWorkingStandingZeroPoseToRawTrackingPose_mD1806323555002EBF765E704EBF1DAA9036B58E4,
	CVRChaperoneSetup_SetWorkingPlayAreaSize_m86B954C64BF764938B472B4A79349B95562EC071,
	CVRChaperoneSetup_SetWorkingCollisionBoundsInfo_m2BAF58BA229014D1084627E8864B525B9C34BCD9,
	CVRChaperoneSetup_SetWorkingSeatedZeroPoseToRawTrackingPose_m344F859C35E8633BFB1F64B16B9CF2E2E19E2B4A,
	CVRChaperoneSetup_SetWorkingStandingZeroPoseToRawTrackingPose_mFFC83B62EA70E7C254B2C09D20D06D953E68CEA1,
	CVRChaperoneSetup_ReloadFromDisk_m3D542F8B7A9D353264C29677D5A587C173119AA7,
	CVRChaperoneSetup_GetLiveSeatedZeroPoseToRawTrackingPose_mB7E53C369CABC34EF4918F0222F51450395E1363,
	CVRChaperoneSetup_SetWorkingCollisionBoundsTagsInfo_m5612C7DB5EFB852E2053564DCE3AB64827B59D2E,
	CVRChaperoneSetup_GetLiveCollisionBoundsTagsInfo_mE8D1934B6C5EF7699DD570EBFC404A7E9B8C0E55,
	CVRChaperoneSetup_SetWorkingPhysicalBoundsInfo_mF67F5B39A568082D9DF823B1F303C36DB8E1DCD1,
	CVRChaperoneSetup_GetLivePhysicalBoundsInfo_m58CDA89A1C77EFF58EC3760173BDAF8ABF402035,
	CVRChaperoneSetup_ExportLiveToBuffer_m3385F968F527A722A5D0D3C73BBB6E94A7A975D4,
	CVRChaperoneSetup_ImportFromBufferToWorking_m90BA75E653E48B85F141EE67D6808CBB30ACB7B7,
	CVRCompositor__ctor_m76687EACBD1D98DD26351FE3EB4BB34CC255DC06,
	CVRCompositor_SetTrackingSpace_mF5383683C59B684F37266A0052713F1A55C3D8EE,
	CVRCompositor_GetTrackingSpace_mE709352A6D0C3B4DDF20236C14C3DEE6EFE59643,
	CVRCompositor_WaitGetPoses_m12CD1189BCDD080405743CA29CDD4AB561D3CD6F,
	CVRCompositor_GetLastPoses_mEB04A324E02816925043913DE16580A5BBB937E2,
	CVRCompositor_GetLastPoseForTrackedDeviceIndex_m1148D0735726373165658A4869B0CB8AF2371978,
	CVRCompositor_Submit_m1C400A2D54A77A3DA44B8390ECC741D6A56D9B60,
	CVRCompositor_ClearLastSubmittedFrame_m891419E6FB506FC25EA8EFAEA394BA334DB29AB3,
	CVRCompositor_PostPresentHandoff_m4F08024F8A2F74808D26B085750C4D17E093B068,
	CVRCompositor_GetFrameTiming_m10E744A25C959F27173F381A7FD8F6BA172A9DC1,
	CVRCompositor_GetFrameTimings_mFEB399B6B2814708382C86090A52EDF7575F67F1,
	CVRCompositor_GetFrameTimeRemaining_m5BCC67E11003D233BED3D608D4F4DCA96FF47AA1,
	CVRCompositor_GetCumulativeStats_m6935B3AFA41BC935BEA5D15AD03914ECD17FF43E,
	CVRCompositor_FadeToColor_m3ADD702AC4153707BFCEF5B65C82EED71434EA05,
	CVRCompositor_GetCurrentFadeColor_m354F6B2772CC4C06C9B8F558921F978784D7E40E,
	CVRCompositor_FadeGrid_m9C71900504C7407D673B0F43B5EC690FFDCB7EC8,
	CVRCompositor_GetCurrentGridAlpha_m02235A744325967FD3BF8F12D84E745A9AE2A1EE,
	CVRCompositor_SetSkyboxOverride_mCE45D06DC49EC3D9461E0C827ACA6D39801D3813,
	CVRCompositor_ClearSkyboxOverride_mEB0DBCADDF28981CAF2B297288F30711E2A2C3C4,
	CVRCompositor_CompositorBringToFront_mCD327B7B4FCD40251023A4E1991440BBCA65DE3B,
	CVRCompositor_CompositorGoToBack_m6F0298EA2CA75AE8C4C7C838C476FB56DFDFC24F,
	CVRCompositor_CompositorQuit_mE10BD3AEDA3D0B065427F5E3C2FE42D06B739456,
	CVRCompositor_IsFullscreen_m7C40557D6AD106B14DCB18566C123A3362DFB160,
	CVRCompositor_GetCurrentSceneFocusProcess_m8D94EF9DC806EAFEC686A312376B88F0EE0F29FB,
	CVRCompositor_GetLastFrameRenderer_mCCB3697D08B9D5CD09544316E4B71371B1F08622,
	CVRCompositor_CanRenderScene_m47C40F7ADE3E6D69B1E05047F7CE3B793D8F684C,
	CVRCompositor_ShowMirrorWindow_mF23E165BEEC6438BA433E9EC0C47F6973698279E,
	CVRCompositor_HideMirrorWindow_m9D8A43D48D83D67FBA9C673EBC1FE04895C4012B,
	CVRCompositor_IsMirrorWindowVisible_mC40E25027C513ADAC009CE5659CFC05A1DE6FDA0,
	CVRCompositor_CompositorDumpImages_mBFB5BB1C83E2D8CDF2B993012B87D57AD37017A8,
	CVRCompositor_ShouldAppRenderWithLowResources_m08F710E91753A61F48111412800CA64BBC11210A,
	CVRCompositor_ForceInterleavedReprojectionOn_mE3E8B62798B34B67D675D278909D75436E42FFC6,
	CVRCompositor_ForceReconnectProcess_m90236FF370769B6C6C9189D1F8F06510ADEA9C8D,
	CVRCompositor_SuspendRendering_m2901CC7920CA5E179F846774B0EA21D3D141C264,
	CVRCompositor_GetMirrorTextureD3D11_mB3CD1A6524BA4C474F971E7FDD62AD081447FC4E,
	CVRCompositor_ReleaseMirrorTextureD3D11_m2DF06A35720FAA1CE385EFA58E3F0B9539E863BF,
	CVRCompositor_GetMirrorTextureGL_m6040D9C87CF43932655F38ED57BA51BA03993414,
	CVRCompositor_ReleaseSharedGLTexture_m7DF7EB3D690BA4ED1DD5FE0CBA453A050650B415,
	CVRCompositor_LockGLSharedTextureForAccess_mC221EC8E2B836559F187FF7081F5920406B3CCD7,
	CVRCompositor_UnlockGLSharedTextureForAccess_m1E82C66D15D3F5E0ACC6B4D767D510171C8E5B8A,
	CVRCompositor_GetVulkanInstanceExtensionsRequired_mF893F41CE43F93B5F7567BE458FCB5450C59E472,
	CVRCompositor_GetVulkanDeviceExtensionsRequired_mC14FA51B144836253EF4F524D99F82493AB80630,
	CVRCompositor_SetExplicitTimingMode_m6661F02036B927657177E3C57FC058D96F0BDF99,
	CVRCompositor_SubmitExplicitTimingData_m955D4AEBF99ACE56DB05EEFF8B37FB80D7C464EB,
	CVROverlay__ctor_m57486E1F6EB0252D78EF9C93C17CE118714451E8,
	CVROverlay_FindOverlay_m3090AED964C94B8FA46D3111F5E8A3A85E2A44C1,
	CVROverlay_CreateOverlay_mDA392D308E83FA250023786D223CC2443AE46884,
	CVROverlay_DestroyOverlay_mDE2E19F89DA7059B5ADB6617617404D7171B82D3,
	CVROverlay_SetHighQualityOverlay_m49A71CA516A0A9BA6DCB455C69DF4F320D0C55AB,
	CVROverlay_GetHighQualityOverlay_mBDA523928F2C4DA40A83F55AC34148918E857FBC,
	CVROverlay_GetOverlayKey_m7CD6D0AB98188708CA92A980D562EF6F0DA272A0,
	CVROverlay_GetOverlayName_m25BDD743DB4F6FF1F5CD63EEEC16B715154B56FF,
	CVROverlay_SetOverlayName_mFC2F02B99A9621BACD606F7C892CE7595B2DFAE1,
	CVROverlay_GetOverlayImageData_mBCA5DBFB69D453511DDB6691678CFE4CCE088E5C,
	CVROverlay_GetOverlayErrorNameFromEnum_m31FE986C61CD470BC57D4D1FBB5ECB15A94998C1,
	CVROverlay_SetOverlayRenderingPid_mFA4E4411DFB0EBACE8C629C59BCCA242125B16C2,
	CVROverlay_GetOverlayRenderingPid_m0AAB2549BED309AE6C60F879426E8566CAD2DF01,
	CVROverlay_SetOverlayFlag_m86A8C95C2B611A94D5AA8CD2F9A9BC6601CB112B,
	CVROverlay_GetOverlayFlag_m918702FCEF67D937D59C696CCC8B8C31A9043738,
	CVROverlay_SetOverlayColor_m960BDB0A2DC1C1E76CA81BF23E16326EFB399766,
	CVROverlay_GetOverlayColor_m094E0E89E1FCE544A89A8AED07AE1DB5F9569444,
	CVROverlay_SetOverlayAlpha_m2B6529C3A131F8CA605F6B4452D69E0626123EA1,
	CVROverlay_GetOverlayAlpha_m74ADD661DA41B780600BB5193A360305F19ED5D2,
	CVROverlay_SetOverlayTexelAspect_m87A8AFC63C9595ED954006A099A94B49E74CCA72,
	CVROverlay_GetOverlayTexelAspect_m3890C24E5EBE5620E6F7E5DCAE1B7BCD220D4C9F,
	CVROverlay_SetOverlaySortOrder_mD13E4F88190AC3AFD2C821F2155EC457773F1153,
	CVROverlay_GetOverlaySortOrder_m7937B07A448F72BE6C74FB44814BA00C5D2A27DF,
	CVROverlay_SetOverlayWidthInMeters_m5CE95588684B94F5BF5E4BC5DDD71FD79CBB49F1,
	CVROverlay_GetOverlayWidthInMeters_m75BEAB84F7E606153D4A685A00E7B37556C6F28E,
	CVROverlay_SetOverlayAutoCurveDistanceRangeInMeters_mEEF37EA229D1FFF1278E1EE73A390DA8BED214CB,
	CVROverlay_GetOverlayAutoCurveDistanceRangeInMeters_m77811D591EBC53CD84723DFC84FF71CD51F7FAD6,
	CVROverlay_SetOverlayTextureColorSpace_mDC50BCB2F2BDD9A9A0E17F26EC0B0B76574D0C9E,
	CVROverlay_GetOverlayTextureColorSpace_m3D4B21CBB0BE2A1BA7D1EB650AE9FC4F9743BD0F,
	CVROverlay_SetOverlayTextureBounds_m853DFB2A4A84ED99ECEA4D1DD035E4FB294FDE40,
	CVROverlay_GetOverlayTextureBounds_m692EA1D0E92BE427057393ED23FFE99009C03A02,
	CVROverlay_GetOverlayRenderModel_mA17D44FC1500407CB7C6ECC05D4380DF1A3C545F,
	CVROverlay_SetOverlayRenderModel_mFC0F8545A26D4E2256C41944D313AADB48EF08F2,
	CVROverlay_GetOverlayTransformType_mFDCC93C75DFBF73CCA4B3C72E24A76BFA461732F,
	CVROverlay_SetOverlayTransformAbsolute_mBB93F2126BA6739716D2C4E85A67E20D95EF570B,
	CVROverlay_GetOverlayTransformAbsolute_m1A9F970316D84E9D184094CF545F0E3332912BBC,
	CVROverlay_SetOverlayTransformTrackedDeviceRelative_m6A91238FD31FA53143B144B9A46EF36A5DA04F19,
	CVROverlay_GetOverlayTransformTrackedDeviceRelative_m241AC35F45346300443A37150F2CE1ECB166CAB3,
	CVROverlay_SetOverlayTransformTrackedDeviceComponent_mAB9A53FD83143DDE8F0AC9DC6975620B67424F2F,
	CVROverlay_GetOverlayTransformTrackedDeviceComponent_mD60A1BF4C56B30888EE7CE52CF3808C546D5EAB4,
	CVROverlay_GetOverlayTransformOverlayRelative_m3E71324DD7D1D642B7413D03034908E8F678B727,
	CVROverlay_SetOverlayTransformOverlayRelative_m3909F8C89FFACDA89FCE723661259B6DCD859F19,
	CVROverlay_ShowOverlay_mB70E419A44942F4ED429BBC22F215E56D2BFC8E9,
	CVROverlay_HideOverlay_m08F6A32CA42F02D7CDD3B552CCD104EBDAC92356,
	CVROverlay_IsOverlayVisible_m4110FE91132C7C61661BB2CDF64A1AF35C5A6940,
	CVROverlay_GetTransformForOverlayCoordinates_m16927E43709669A10CCA29F3E503142882E6D5A7,
	CVROverlay_PollNextOverlayEvent_m2F52848E5569DCCC2323B43D72E222715EF6DDFA,
	CVROverlay_GetOverlayInputMethod_m8F690C1DE5AA9B3C03DDA48A7F230DBAF4852DC2,
	CVROverlay_SetOverlayInputMethod_m0626936627F0925A505DFFCD41A990809E9883FE,
	CVROverlay_GetOverlayMouseScale_m60EEBFAD8E567775D283BBCCF31892CC169024AA,
	CVROverlay_SetOverlayMouseScale_m29101DF7B23D1B11585CD68CCBCD9425EF4744BB,
	CVROverlay_ComputeOverlayIntersection_m059D501999A443467273982881370ECA9975C684,
	CVROverlay_IsHoverTargetOverlay_mCD9251D72BF5FC35A2F9E7DC0311F6F01EDB1038,
	CVROverlay_GetGamepadFocusOverlay_m035F0BF5B3B96343B378DF9C75DF490590210650,
	CVROverlay_SetGamepadFocusOverlay_m300F4E3FF6196AC2F2B0CB264F84DF36301C5A94,
	CVROverlay_SetOverlayNeighbor_m2E78C55F999D1D6A32A67308DA6A56C3849D7F1F,
	CVROverlay_MoveGamepadFocusToNeighbor_mA8C9E7302D51884C1CF9FEB6673F686D8852F640,
	CVROverlay_SetOverlayDualAnalogTransform_m935B9A31E78A05C4B18EB92614288E0656D9146A,
	CVROverlay_GetOverlayDualAnalogTransform_mFB732008F3620835D4695E0C34E77C00337A176F,
	CVROverlay_SetOverlayTexture_m0150DA767B7D254828F92A72A726C9C7F40203E1,
	CVROverlay_ClearOverlayTexture_mD21BC4AD903D915D4EC9FF4140BFB12C38426579,
	CVROverlay_SetOverlayRaw_m48310EB7EC5FD48BCE135C292B68371D362693E7,
	CVROverlay_SetOverlayFromFile_mB73FED316A840B9D867A841618A632D83539B24D,
	CVROverlay_GetOverlayTexture_m0F6944EFEB0FD280561532E88084F4B75A9A211E,
	CVROverlay_ReleaseNativeOverlayHandle_m3AAE4476F48A4BA78749FF88A26D775632BEC834,
	CVROverlay_GetOverlayTextureSize_m2B0F221F60E130BB2900E25ABC03CBF4C642D680,
	CVROverlay_CreateDashboardOverlay_mFBE4FAE4E311B54A3B363651EB9EFA02B2905B9B,
	CVROverlay_IsDashboardVisible_m87301A1259F13BED5E27766ECB4539904B119B17,
	CVROverlay_IsActiveDashboardOverlay_m17234871856DE9425100C08206AAD2C23193967F,
	CVROverlay_SetDashboardOverlaySceneProcess_m48A7B80EE84D02D5FFD2A82A5116E755B219319B,
	CVROverlay_GetDashboardOverlaySceneProcess_m9F677221AC478C5F0FEB5B8975137E98840B4796,
	CVROverlay_ShowDashboard_m335CDDB205D5F3646863C95CD12D871811B34CA3,
	CVROverlay_GetPrimaryDashboardDevice_m488BBFB983F0A9217306823975D49BF557407D69,
	CVROverlay_ShowKeyboard_m6F924085E25C6ECF080A51B78BEAD49037B228E5,
	CVROverlay_ShowKeyboardForOverlay_m3B7C94095053AF1FAB3C7D27847D284718A0497D,
	CVROverlay_GetKeyboardText_mB4A01A6FBF2362CDEB00B783940F67745DC2F0DB,
	CVROverlay_HideKeyboard_m0438506D6A364DB5FE455AF6043AEE95FFE23943,
	CVROverlay_SetKeyboardTransformAbsolute_m4A87227BF4ADA8F1BAE6B060DB9EC11123825864,
	CVROverlay_SetKeyboardPositionForOverlay_mC019B03AFA7ABEC64F4C33B35750DB364E83F521,
	CVROverlay_SetOverlayIntersectionMask_m572BFA18A61A15AFF13A3780DECF18D51EE8F4E5,
	CVROverlay_GetOverlayFlags_m7F573F422D712A175F9850F9F1FD71758CBB8626,
	CVROverlay_ShowMessageOverlay_m6175A154980C28E7C10459BCFD44A3FCEFC59FA5,
	CVROverlay_CloseMessageOverlay_m102D6FA392983034598E2583D10EEC563C4EF99F,
	_PollNextOverlayEventPacked__ctor_m3AD5058F753C1845270442BA16C2912FC016B423,
	_PollNextOverlayEventPacked_Invoke_m841FBDEEBB4925C48635A7F0DA373D0A9177A4F1,
	_PollNextOverlayEventPacked_BeginInvoke_m98C9028194921A8FECA008C4B92C892FF2958062,
	_PollNextOverlayEventPacked_EndInvoke_mE6AA33EE841DDC005397C07C56E1255597421968,
	CVRRenderModels__ctor_mB2D250428DAE2FA8D776B603A22B2CA658FF3159,
	CVRRenderModels_LoadRenderModel_Async_m8A69052296687918E98F204084BF236BC25DEEE5,
	CVRRenderModels_FreeRenderModel_m65C0E05E1DE663B48B5D57629EE895106863BE4C,
	CVRRenderModels_LoadTexture_Async_m2FC69F68935CD11D23E95310908F03A9F950133E,
	CVRRenderModels_FreeTexture_m4700DB96B7B33DC99BF681B8EE9AB08E76415379,
	CVRRenderModels_LoadTextureD3D11_Async_m61CE24875F44C501C2A8021866BAE4B58F96EC08,
	CVRRenderModels_LoadIntoTextureD3D11_Async_mCB33166CEFAF71F4264644C073E9187F7F00E5EE,
	CVRRenderModels_FreeTextureD3D11_m6513B8CC48317C285F335361084CDAF1EBDA2761,
	CVRRenderModels_GetRenderModelName_m6EF74803244E81A8BDF5712F9B5FC4A572D9BDCD,
	CVRRenderModels_GetRenderModelCount_m88DC87AF530F201C13097A04DBFBC021C447EC00,
	CVRRenderModels_GetComponentCount_m3A02565E83312F1201E51FE0892ED444EF61E777,
	CVRRenderModels_GetComponentName_mB9BDCF11834B3C48498CBCA6193F1FA7060C69C9,
	CVRRenderModels_GetComponentButtonMask_mF69D6EC84CA08EF3D624C44129D50F349A5BC927,
	CVRRenderModels_GetComponentRenderModelName_mBB10907509E2334B5E7639963C5FB252F0AF71D1,
	CVRRenderModels_GetComponentStateForDevicePath_m558C8E427C968108355955CF8D7E73FA4FBF75F8,
	CVRRenderModels_GetComponentState_m1362E3CC0680982BB33B115566A217808121F413,
	CVRRenderModels_RenderModelHasComponent_m4D6B041CC8E069CFDA7322424381C807079ACC62,
	CVRRenderModels_GetRenderModelThumbnailURL_mD9C44C31DF2D85461370495A2CC965C832E3200F,
	CVRRenderModels_GetRenderModelOriginalPath_mDFE81A16ED54BE7DE6B2A62A99197D46D9D8C3F6,
	CVRRenderModels_GetRenderModelErrorNameFromEnum_m02F55C26FD13B577F3D1DD3B15A57F3EE97A6A99,
	_GetComponentStatePacked__ctor_m2C63ACCAB5E70B36317F12E810951A8248768812,
	_GetComponentStatePacked_Invoke_mAB619F0D73FBE44C3A60E39C14E433571CDDBB49,
	_GetComponentStatePacked_BeginInvoke_m8D552EDD2FACC68636E3AF73F880503B20596365,
	_GetComponentStatePacked_EndInvoke_m002D0B2089B8F6B023AF14DE3BBFBB8001DD9B1F,
	CVRNotifications__ctor_mE46E46DDBE9580CF1110EDDE1F75AADA48E2016B,
	CVRNotifications_CreateNotification_m4F1068B8F0A27BE6B58785CCDF2C233F59B5012F,
	CVRNotifications_RemoveNotification_mECD6914FF3B22E65661B797B6A537D4E96133D2C,
	CVRSettings__ctor_m3D9B422E6F4D973F2378DC1F1D14C835F20809F9,
	CVRSettings_GetSettingsErrorNameFromEnum_m552C3BDE021FF81473D55D564587F29D6E77795C,
	CVRSettings_Sync_mE821B98AEC02135199CD43369E5F8CAE4E7542AC,
	CVRSettings_SetBool_m05E89BB13896B6FF8F4BD27AF11805A76FB3C924,
	CVRSettings_SetInt32_mF17663555CE0C5AC5CC4B6AF0F2BF94DB893ECBC,
	CVRSettings_SetFloat_m97E84C70A70B5FF097C29A4095F20F6BFDCB526C,
	CVRSettings_SetString_m4A3A6AB65ED55237364138BB0BF7DD95ADA91A59,
	CVRSettings_GetBool_m719C5D19E931D79F24A84FD301B7EF007894272C,
	CVRSettings_GetInt32_mB6739278C17F5AFB388FFDF17F01DD888393A927,
	CVRSettings_GetFloat_m842C0F02566E6E34AFE35C9DC48093BE1166F698,
	CVRSettings_GetString_m46D7AA23613E9B8D62A554F053CABE717C27602F,
	CVRSettings_RemoveSection_m82910E182C38CF3893ED58D9F044C392F5D7F6D0,
	CVRSettings_RemoveKeyInSection_m69E4BDCAED17B79DB2DE701F58E53CD4B7FFA649,
	CVRScreenshots__ctor_mF29218A6DE79028B39FA354C95B6F38779345772,
	CVRScreenshots_RequestScreenshot_mC8963E9115B7DFB0D5C4660FF3577AE1861131FD,
	CVRScreenshots_HookScreenshot_mC7DEA6CE01BFD92C36FF3919DE3FA5D8AD273770,
	CVRScreenshots_GetScreenshotPropertyType_m12607755F749EE68E33692455FAF604BF827A350,
	CVRScreenshots_GetScreenshotPropertyFilename_mEE07EA543DC64B3222D0F9EE17EB1B5B1F3CE87C,
	CVRScreenshots_UpdateScreenshotProgress_mF8D94A23604064179F3A27BF9C9BA28C96359D28,
	CVRScreenshots_TakeStereoScreenshot_m081E4D6B9F0EC7062C0B54C7C1CBB2CC45EA79DF,
	CVRScreenshots_SubmitScreenshot_m7F724E0508B82F20BBCBB811C7E27097F43C4F3F,
	CVRResources__ctor_mA422F38039A0C04B10DA15555D1ACAACB31E7E91,
	CVRResources_LoadSharedResource_mE6A74185A56D1F9D2BDFB033F68EA1F96AD30304,
	CVRResources_GetResourceFullPath_mA7E53C24113F02EBE7097F22E30E043C2A1CA051,
	CVRDriverManager__ctor_m926ABA41F220B282081645BDA0A4FC9D3925C914,
	CVRDriverManager_GetDriverCount_mA1F81B38336ACA594AB6364C071CD87A453DDAAA,
	CVRDriverManager_GetDriverName_mACA1DFCD465B041A6453FCF6FEE37BDB7A4EB8E6,
	CVRDriverManager_GetDriverHandle_m81D2B104C742B4246AC4398746FCD0E1FEB07396,
	CVRInput__ctor_m93499950AFA147BFFCB43F458CEDF877C431A18D,
	CVRInput_SetActionManifestPath_mCEBFE2947D8D9ED21CC24A06D220B022D997FD17,
	CVRInput_GetActionSetHandle_mCBDEBC1DA75F0CD9E570D221708F4A529152E996,
	CVRInput_GetActionHandle_mC360EC86FC53063990C309C27C887CCA2A6FE3B3,
	CVRInput_GetInputSourceHandle_m8A8CBE4084703B848A2CC7DB0FD02B51AA7FD215,
	CVRInput_UpdateActionState_mCC82BE615585536DE8EA64951EB9B674305CDB9E,
	CVRInput_GetDigitalActionData_m9E346B991DABB1D8B67727E4084C4C59C369A3FF,
	CVRInput_GetAnalogActionData_m5CAAA2E5DB199F5D40366E0B881A8353B575FB8D,
	CVRInput_GetPoseActionData_m28B3AB9BDEA5AE66784E67C3F9C6F8D3067ED321,
	CVRInput_GetSkeletalActionData_m51CE11F3E4F1E51B4E39430C8F23DE007573C0A0,
	CVRInput_GetSkeletalBoneData_m768448C790F705B814918D05A0F0F4D4DF8D3852,
	CVRInput_GetSkeletalBoneDataCompressed_m2B3BA18AD0728BE6EF10D9BA7D2670F002EEC21E,
	CVRInput_DecompressSkeletalBoneData_m972891B09E03B1B8706496363CFE6626E1F2DCCB,
	CVRInput_TriggerHapticVibrationAction_m4567E463399C22D0462528F7AD6995D6FACE30CD,
	CVRInput_GetActionOrigins_m15338484B4F7CBBC1E5BAC61E1906E98936B1687,
	CVRInput_GetOriginLocalizedName_m1625FBA989C4A016301F7BF7696B4977858DC27C,
	CVRInput_GetOriginTrackedDeviceInfo_m8C1CAE327C2F5A726F43BD585064BEB2E3759965,
	CVRInput_ShowActionOrigins_m020ED911DD5A20A518330ED33FF173D267B6849D,
	CVRInput_ShowBindingsForActionSet_m143F47398111A093E5603FD5D1F8240F67E5B123,
	CVRIOBuffer__ctor_m9B1361C9CFB9F7AE47221CAA702A7EF9577F0AFA,
	CVRIOBuffer_Open_m2E9C20805EBC71567F148EADDCD561519161E70C,
	CVRIOBuffer_Close_m605FECE72296D6684056D0462A120926C973E4DC,
	CVRIOBuffer_Read_mAB49BEBDE806B4EF072AADEE34ABA90BF57AF6E0,
	CVRIOBuffer_Write_m646CA08DE5BE582E9D6A38C6348E2BEB75542478,
	CVRIOBuffer_PropertyContainer_mB3656041524B72D8E8D616816B5024477B0232EF,
	CVRSpatialAnchors__ctor_m96CB910D1648888981EFE609464C45387B262C97,
	CVRSpatialAnchors_CreateSpatialAnchorFromDescriptor_m0181799D3386993F52632AF7650E3B72DF20A696,
	CVRSpatialAnchors_CreateSpatialAnchorFromPose_m4121FB26A811100AF9116E8E7CA487DA4C888085,
	CVRSpatialAnchors_GetSpatialAnchorPose_m6FC4339E984AFD647E37560642CF46FA4BD0EC92,
	CVRSpatialAnchors_GetSpatialAnchorDescriptor_m2A288A011AB04AE0A5BDBE4239D5E479C2906AAC,
	OpenVRInterop_InitInternal_m568739546B3644F5AF8CCDD0C08FF65EC6B5069F,
	OpenVRInterop_InitInternal2_m8DD236ACD171504D571A22A2744E1F22C8043348,
	OpenVRInterop_ShutdownInternal_mB657DDA2CE332848F050592E53AC3B187DACC6DB,
	OpenVRInterop_IsHmdPresent_m1E47CE90061744864243A10DA88584C19101F13F,
	OpenVRInterop_IsRuntimeInstalled_mCD5BB11DF2D20B589BA972459E12094F9A7E45FE,
	OpenVRInterop_GetStringForHmdError_m36B6A90A280F0688D4FF0BA018B7FA028E8CF498,
	OpenVRInterop_GetGenericInterface_mBCFC4564E20F64ABF53F77AD2DD96A04FD99E5D5,
	OpenVRInterop_IsInterfaceVersionValid_m3BC837A21B957750304097F137FC40D410ADF5A2,
	OpenVRInterop_GetInitToken_m39C4899B8394BA07D8E90E4B7F1B06389298FE52,
	OpenVRInterop__ctor_m749F588D74B3DB884003CC1A139E77012368ACE4,
	VREvent_Keyboard_t_get_cNewInput_m14DD64244AE9463A168869227270198484560792_AdjustorThunk,
	VREvent_t_Packed__ctor_mCAC3B6E0EFDD1A595CA699DC9182F16F408AB6D0_AdjustorThunk,
	VREvent_t_Packed_Unpack_m534F1D0D47F5A93B930E931EA0588CBC93E84E22_AdjustorThunk,
	VRControllerState_t_Packed__ctor_m34E6CACCCC7E682AE448E5368F6ED2F425E95617_AdjustorThunk,
	VRControllerState_t_Packed_Unpack_m509D69C9DD8602F41485A476D58EC6088AAFDB74_AdjustorThunk,
	RenderModel_TextureMap_t_Packed__ctor_m5911977B45A3A72A9ADC58BA9642A3BCBCFF3522_AdjustorThunk,
	RenderModel_TextureMap_t_Packed_Unpack_m8C2D3ACDF93812B1A575A5CCBF1D9523B21C88F6_AdjustorThunk,
	RenderModel_t_Packed__ctor_m8E65089C982BB9444CCE5C657B9AF1C6A7E7EE0E_AdjustorThunk,
	RenderModel_t_Packed_Unpack_m309CC4FBED4B55D7A9098A9EB418FF7E76845741_AdjustorThunk,
	InputOriginInfo_t_get_rchRenderModelComponentName_m0DA945ED08BEC0FC8A53BB7958AED51396D53DFE_AdjustorThunk,
	OpenVR_InitInternal_mE771350E30E10D786C5A3BD3C414D5473D61C81B,
	OpenVR_InitInternal2_mD247062D7446BB524CA51B420F7969FD2EC9865A,
	OpenVR_ShutdownInternal_m4109BCAEB683C85B36EFC41176AADEC7462A0C74,
	OpenVR_IsHmdPresent_mDC91BFD66BCA5300BA304AA67BE26C0EF4086DE9,
	OpenVR_IsRuntimeInstalled_mD8D73AC1DAD74AD552C4D11F360027A1963AE17B,
	OpenVR_GetStringForHmdError_mA5F4CF84E1D471A755206BFC1F50F19A1DB6C1AC,
	OpenVR_GetGenericInterface_mB0623919D353B0124FC52B5B1511349DCB5CC3DB,
	OpenVR_IsInterfaceVersionValid_mCD8CEC94397A5E2135A566BBD1D5355DFA72099E,
	OpenVR_GetInitToken_m4E36F591850B3D68AE413A8625FB28BA263F8295,
	OpenVR_get_VRToken_m2310C0C3AF4EBE907F9031C305C6144E33B7554F,
	OpenVR_set_VRToken_m9D00D4A0135DCBFF68CDB9F3E297325F77794C05,
	OpenVR_get_OpenVRInternal_ModuleContext_m9A9049EF7A830EBC29CA1C079F77C3CF9819B80F,
	OpenVR_get_System_mF933C580A6BFFABA5407F9DC3FBD5C69572E7C80,
	OpenVR_get_Chaperone_mB667AC54C23E60BCAA038137564E265CE3891389,
	OpenVR_get_ChaperoneSetup_m467461EBE5CFF64CE0B6520519D5FC1988915B0E,
	OpenVR_get_Compositor_mDF1B686D8D4DD188496054F8A5787298D0330DB3,
	OpenVR_get_Overlay_m9AFF87BEFD829CA799278820A18AF0BD0007253B,
	OpenVR_get_RenderModels_m5C205FE40330D719E6E9C6554A035254BD600B76,
	OpenVR_get_ExtendedDisplay_m76BD047FE7886FC58D0BD7BA7C45978BD1A670F7,
	OpenVR_get_Settings_mBB2E7B2FDC1239880A47512AA4446B215D404F60,
	OpenVR_get_Applications_mF3FCC5009CE10471DFFFD6C3E52B538E4176C506,
	OpenVR_get_Screenshots_m47C049E83B5144D0B3EF9BCB24985A31AE169486,
	OpenVR_get_TrackedCamera_m1D6DA741DEA6FC1C83E72B5601377A7CAA1CB3AC,
	OpenVR_get_Input_mAF2A670BBF0B1B250E542BDEB163B506F43A4C1E,
	OpenVR_get_SpatialAnchors_mBFB9970144290BD78BA18F7E14DDD591BF8A2F82,
	OpenVR_Init_m72FE9104C1F368B45E659989DAA8B8F340990997,
	OpenVR_Shutdown_m4D3755D88EA1E2B53B4EEE6BC1CF9E0B19A22ADF,
	OpenVR__ctor_m2EBE59672164ACEB691FC9F1E87A1EDF07CE7B53,
	OpenVR__cctor_m65D25BC9753EAAEAA5A0567C817C16A00F1AFF42,
	COpenVRContext__ctor_m40510A343019E4FACB7110000D5E6C00E1C5AF0A,
	COpenVRContext_Clear_m077E28DDA05F2B32AEE885B5EDF80BA376D6F7D2,
	COpenVRContext_CheckClear_m10C11DFBE0A94A81ABD15F2EAEC96A2355604061,
	COpenVRContext_VRSystem_m519F3D56E3527EE965293D0B5C815DCFE17ABF3F,
	COpenVRContext_VRChaperone_mE22C101BF1701969221893A911151EE1E771F6BB,
	COpenVRContext_VRChaperoneSetup_m09F8A1CA3E0661D224CDC22222D679467B816AB3,
	COpenVRContext_VRCompositor_mBB26E115FB134B467D5633650D8CE6D3A08C0429,
	COpenVRContext_VROverlay_mE13219C0F0597A936E2BF8A700048B1F9771AEDC,
	COpenVRContext_VRRenderModels_m15D7DA92B230A1BE2DBC35FF6D927E75F3F28F0F,
	COpenVRContext_VRExtendedDisplay_m57224F91F5C9A71FBE76B78CD15A37562BCE0D40,
	COpenVRContext_VRSettings_mA6D310CAA581EE36CC275689CE37326262FB9367,
	COpenVRContext_VRApplications_m9B653BE8CBF352990AEF61B5BDC404F2137364C6,
	COpenVRContext_VRScreenshots_mC83348547277C55143B84F8864F8A352A1EDC3E8,
	COpenVRContext_VRTrackedCamera_m07A37EF628B8788F38BCFE694F4DEF277B77A4CC,
	COpenVRContext_VRInput_mB0073EDACD24D7ED231CCA8F32699CC6FA589684,
	COpenVRContext_VRSpatialAnchors_m371F2530754D0AA51949041D030251C1D23068DE,
	Record__ctor_m939BD87E9B88D65636F54D91592DE103C7B8F755,
	RangedRecord__ctor_m7D7ED72EA04EA1974D7903848020E6A6CCE86A4B,
	FixMethodDelegate__ctor_mA6E4BDE779409869DD3B3F67BE03D29FE43839C3,
	FixMethodDelegate_Invoke_mE4CD1ABFDC654C00B8924EF2AF54A630936B0139,
	FixMethodDelegate_BeginInvoke_m7EDF41DE1B6E0B8CA1300262B0065271E7AEB14E,
	FixMethodDelegate_EndInvoke_m2D8B56DF4CE29BFD73212B011512372AC7EFC0E1,
	FixRecord__ctor_mB8229134FB2C9D3CCF1890C65F3F98274C6F6979,
	OVRInputModule__ctor_m5CB834B641E6CD82A8FB2F3C1E92B6CCCD1A06B4,
	OVRInputModule_get_inputMode_m481170BF22DE4F18EEAAD7C8FE15171C3B9051BB,
	OVRInputModule_get_allowActivationOnMobileDevice_m244430FC5B9C7EF5D9F8949C92A0812E8C3FD21C,
	OVRInputModule_set_allowActivationOnMobileDevice_mEFD1992FC38C19E69E64A361AFD453B537EF7971,
	OVRInputModule_get_inputActionsPerSecond_m578E96A49DA2442EE7C3E08CF3DEBACFCB2143D0,
	OVRInputModule_set_inputActionsPerSecond_m81B0E3A1026918FF067C1C26ABE0F1600694BA7B,
	OVRInputModule_get_horizontalAxis_mBA8AF77470337EB43E34141A40082C1EF6C867EC,
	OVRInputModule_set_horizontalAxis_m2C0C18FA91EE05DEEAA8A5F29C2CAC6D61A06F4A,
	OVRInputModule_get_verticalAxis_mD87076DBD7FD641CD9B656CADD471F91F34FBD12,
	OVRInputModule_set_verticalAxis_mB2FCB47596FB7C0619E8E0610ADB36F7DF12C247,
	OVRInputModule_get_submitButton_m61CBBC70F8073307BD57F5FE80AFBB5547FD5DB8,
	OVRInputModule_set_submitButton_m131D798CAB60658A6928A7C04F3EB49FC42C86B7,
	OVRInputModule_get_cancelButton_m00E433C926C1F5027BB6E419146E918E44EE69F4,
	OVRInputModule_set_cancelButton_mF012F513E38C61C61A1E9C0D208DE30B0E6F553F,
	OVRInputModule_UpdateModule_mC2E5F5B1078ECE12C0B54ADC121EE4C2A1F98CB0,
	OVRInputModule_IsModuleSupported_m94C3B5366F4E4A80573611AA4260E3EBBB45BD45,
	OVRInputModule_ShouldActivateModule_m94C2531C19186F730C4456ADD9FFC6B2BB6E0669,
	OVRInputModule_ActivateModule_mBF35DED00716E753ABDB3D4C6A0C1D25749F67AC,
	OVRInputModule_DeactivateModule_m77677E554B8195C345D82058212C8B4204285117,
	OVRInputModule_SendSubmitEventToSelectedObject_m07FE346295497118E84F557223CC884C87CA0A11,
	OVRInputModule_AllowMoveEventProcessing_mD8FFB148B7046FD2374300468F924B13A30E5CD5,
	OVRInputModule_GetRawMoveVector_m3B74C7A9CCCD20D63A057864B5697D70D0EBFD26,
	OVRInputModule_SendMoveEventToSelectedObject_mF97D71663C0DF70D4860B5850D6E752D75CB85B3,
	OVRInputModule_SendUpdateEventToSelectedObject_mC7E30E8F4E8EE0D91F9706A58841230A18CE4AB9,
	OVRInputModule_ProcessMousePress_mD1E6776C54B1819E50017DC5DD22B9ABA1EC8AA0,
	OVRInputModule_ProcessMouseEvent_mD4E1151D89D17366050D796AAD63F049757D2894,
	OVRInputModule_Process_mE7EE6263694349D6CC07E83232EFB1CB9A0F8D6C,
	OVRInputModule_UseMouse_mDD1A75E6C695AF69CF8E81FD160A6BD5C0763952,
	OVRInputModule_CopyFromTo_m02A4366C928D43C35AD9D32D1A4D3C9A0AFE5468,
	OVRInputModule_CopyFromTo_m066272CF94623FDBB29645155891EE55379D349B,
	OVRInputModule_GetPointerData_mEE6D70D8F790BA51F0F8DE0F3F2C864F71E40151,
	OVRInputModule_ClearSelection_mC8C4DE0E3312130453CE90A0F9B7D98D58CBAF69,
	OVRInputModule_GetRectTransformNormal_m6BD54ABE582428F84A78D0623A05D66BE6D9FBCE,
	OVRInputModule_GetGazePointerData_m7CA4AAD1189FEB683E919275A946231C1DF43F52,
	OVRInputModule_GetCanvasPointerData_mA4029B3BAF43567CB747A4F3D1C1841A33546781,
	OVRInputModule_ShouldStartDrag_m5001EA6CAD8A9FDE110EEA001269E807688E4F52,
	OVRInputModule_IsPointerMoving_m68680D00AECCAE70E1AE7DEE78713964FC8EAD53,
	OVRInputModule_SwipeAdjustedPosition_m11FE0B753E68C069FB760AEF86F7385EF43CD604,
	OVRInputModule_ProcessDrag_m15EBC9C1D15BD508FE9F7B7CA679219B9E5AC982,
	OVRInputModule_GetGazeButtonState_mFACC396DC113D61EF7A52D1181139A0E594126C4,
	OVRInputModule_GetExtraScrollDelta_m1B1188DCFDD6FC071A2B77343F533422828E44E8,
	OVRPhysicsRaycaster__ctor_m6D24866F216A53C836AF6E89CBD4BC069DBD06DE,
	OVRPhysicsRaycaster_get_eventCamera_m10429F19C4CD4653A08282B5A6B95B4FE99C3A01,
	OVRPhysicsRaycaster_get_depth_m078AB8D83F45F3847CF455C366F806A478C306BA,
	OVRPhysicsRaycaster_get_sortOrderPriority_m48859676B979E81A185003157119B4BEF756D6B5,
	OVRPhysicsRaycaster_get_finalEventMask_mA85434470FCF31893D3FD205EF565DDC5E60B322,
	OVRPhysicsRaycaster_get_eventMask_mC3E0C30E0256DB501588B20C7461FD494899677D,
	OVRPhysicsRaycaster_set_eventMask_m20D3CC44CB011AB6B22C09219A6ED1ED4426373A,
	OVRPhysicsRaycaster_Raycast_m349B9024420DF9180F78AABA1621929683692E4A,
	OVRPhysicsRaycaster_Spherecast_mB57C8386983F9F54A03DA006C23BAB1C07E8B74D,
	OVRPhysicsRaycaster_GetScreenPos_mDEA5CE18B0A954A085F51805DF82A9BE8B539DF3,
	U3CU3Ec__cctor_m1D7D4AC78BD05924F991603EAC64BF21EEC40C62,
	U3CU3Ec__ctor_m83E8EB044FB00FD30566EB933DA2EB943BABA5A4,
	U3CU3Ec_U3CRaycastU3Eb__15_0_mB9CD5206BED51FE5C0D3F0006A8556D3FAF25C6C,
	U3CU3Ec_U3CSpherecastU3Eb__16_0_mED9C459EAB5F11DF882EEAE2847937F35C41E3C1,
	OVRPointerEventData__ctor_m09E6B3A742D7D082B62F8F7FBD1514B742B7987B,
	OVRPointerEventData_ToString_m5B51F0FDF641AEB8F2D10887B0BC177814D7A55F,
	PointerEventDataExtension_IsVRPointer_m1FCC09F50A4B5D1D17BD0A2C5760F7613FDFDAC1,
	PointerEventDataExtension_GetRay_m154CB82C9019F0B2B0B566D45ED889D2826397EB,
	PointerEventDataExtension_GetSwipeStart_m53B7B17EC0175D1F10A11D11288596BE83DB8104,
	PointerEventDataExtension_SetSwipeStart_m21C63B5DA80D4F89E1392EA98A0D8ADF336E29E5,
};
static const int32_t s_InvokerIndices[2974] = 
{
	95,
	1704,
	1705,
	34,
	1706,
	95,
	31,
	23,
	3,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	42,
	23,
	23,
	23,
	121,
	1395,
	23,
	1707,
	1708,
	1707,
	1709,
	1710,
	1711,
	1712,
	1713,
	1714,
	1715,
	1715,
	1716,
	1716,
	1717,
	1717,
	1718,
	1718,
	1719,
	49,
	1720,
	1720,
	1721,
	1722,
	1722,
	3,
	1723,
	9,
	10,
	1724,
	1724,
	1725,
	1726,
	1726,
	1727,
	32,
	23,
	32,
	10,
	16,
	23,
	31,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	1180,
	1180,
	1180,
	1180,
	1728,
	1729,
	685,
	10,
	14,
	685,
	296,
	23,
	32,
	3,
	3,
	138,
	140,
	138,
	140,
	138,
	140,
	138,
	140,
	138,
	140,
	138,
	140,
	3,
	3,
	32,
	26,
	26,
	26,
	23,
	32,
	23,
	26,
	26,
	26,
	23,
	10,
	32,
	14,
	26,
	26,
	10,
	32,
	10,
	32,
	14,
	26,
	23,
	32,
	143,
	27,
	143,
	31,
	23,
	1730,
	23,
	23,
	95,
	95,
	23,
	49,
	3,
	3,
	3,
	46,
	46,
	46,
	46,
	1731,
	1731,
	1731,
	1557,
	1731,
	1731,
	138,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	53,
	53,
	632,
	1732,
	1732,
	1733,
	1734,
	1734,
	1735,
	138,
	46,
	138,
	1736,
	1737,
	537,
	3,
	3,
	3,
	1738,
	46,
	1736,
	140,
	46,
	46,
	46,
	1321,
	391,
	1323,
	391,
	53,
	23,
	23,
	10,
	1739,
	1131,
	23,
	95,
	95,
	95,
	23,
	23,
	23,
	23,
	23,
	37,
	37,
	37,
	37,
	37,
	37,
	23,
	37,
	23,
	37,
	23,
	37,
	23,
	37,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	95,
	95,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	95,
	95,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	95,
	95,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	23,
	1131,
	49,
	49,
	49,
	246,
	46,
	1740,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	95,
	95,
	95,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	95,
	95,
	95,
	23,
	4,
	129,
	4,
	129,
	4,
	129,
	4,
	129,
	4,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	129,
	49,
	800,
	4,
	4,
	49,
	800,
	49,
	95,
	31,
	95,
	31,
	49,
	1180,
	1181,
	1180,
	1181,
	10,
	32,
	1209,
	1209,
	138,
	1209,
	138,
	140,
	138,
	140,
	49,
	138,
	140,
	49,
	138,
	140,
	49,
	138,
	140,
	49,
	1209,
	1741,
	1737,
	1742,
	10,
	32,
	95,
	31,
	95,
	31,
	95,
	31,
	4,
	4,
	4,
	49,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	3,
	23,
	3,
	3,
	23,
	41,
	1743,
	10,
	10,
	1744,
	1745,
	23,
	23,
	1746,
	23,
	1747,
	95,
	1748,
	1749,
	1750,
	23,
	4,
	23,
	23,
	23,
	23,
	1751,
	1752,
	23,
	23,
	3,
	111,
	23,
	121,
	26,
	10,
	23,
	23,
	49,
	23,
	23,
	3,
	4,
	4,
	49,
	49,
	800,
	49,
	800,
	49,
	800,
	49,
	800,
	49,
	800,
	49,
	49,
	49,
	49,
	49,
	49,
	138,
	138,
	4,
	4,
	49,
	49,
	49,
	49,
	4,
	4,
	1209,
	1370,
	1209,
	1370,
	1209,
	1209,
	138,
	140,
	138,
	140,
	138,
	140,
	1209,
	1209,
	1370,
	49,
	800,
	138,
	1753,
	1754,
	1755,
	1753,
	46,
	1756,
	1757,
	1758,
	547,
	1759,
	21,
	553,
	1760,
	1761,
	1762,
	1762,
	1762,
	1762,
	46,
	46,
	46,
	46,
	46,
	1763,
	1764,
	1764,
	1755,
	1765,
	1766,
	1767,
	1740,
	1768,
	1769,
	1770,
	1209,
	1209,
	49,
	1771,
	1772,
	1773,
	1774,
	1775,
	49,
	1209,
	224,
	1776,
	138,
	46,
	49,
	49,
	49,
	138,
	49,
	589,
	1777,
	49,
	5,
	138,
	138,
	138,
	232,
	138,
	46,
	1764,
	49,
	46,
	49,
	138,
	140,
	49,
	138,
	140,
	49,
	1209,
	4,
	1209,
	1370,
	1721,
	49,
	49,
	138,
	49,
	5,
	315,
	341,
	341,
	46,
	1778,
	1779,
	1776,
	1780,
	120,
	46,
	3,
	23,
	14,
	3,
	14,
	3,
	14,
	3,
	14,
	3,
	3,
	1781,
	1782,
	3,
	3,
	14,
	49,
	49,
	49,
	49,
	138,
	46,
	49,
	49,
	49,
	46,
	138,
	53,
	347,
	46,
	138,
	5,
	49,
	1783,
	1784,
	46,
	23,
	14,
	14,
	14,
	685,
	685,
	685,
	685,
	10,
	23,
	95,
	95,
	95,
	31,
	10,
	1785,
	1786,
	30,
	30,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1787,
	141,
	120,
	23,
	26,
	1183,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	1788,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	31,
	685,
	296,
	4,
	685,
	685,
	296,
	23,
	23,
	1183,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	1589,
	23,
	6,
	111,
	95,
	121,
	9,
	3,
	23,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	14,
	14,
	14,
	14,
	14,
	27,
	1178,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	296,
	23,
	1789,
	23,
	1178,
	31,
	26,
	23,
	26,
	23,
	6,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	1790,
	32,
	23,
	26,
	95,
	62,
	26,
	23,
	10,
	95,
	32,
	26,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	685,
	296,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	95,
	23,
	6,
	296,
	6,
	296,
	6,
	31,
	6,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	10,
	23,
	1791,
	27,
	27,
	1792,
	1793,
	1794,
	95,
	26,
	3,
	3,
	23,
	1795,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	685,
	296,
	23,
	23,
	32,
	23,
	23,
	23,
	296,
	296,
	1796,
	23,
	23,
	32,
	23,
	95,
	14,
	23,
	14,
	23,
	14,
	9,
	23,
	23,
	23,
	23,
	14,
	23,
	3,
	23,
	23,
	10,
	34,
	62,
	28,
	27,
	14,
	26,
	10,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	31,
	27,
	26,
	28,
	34,
	28,
	14,
	14,
	14,
	34,
	169,
	1797,
	14,
	1798,
	1799,
	408,
	297,
	10,
	32,
	685,
	296,
	95,
	31,
	149,
	179,
	14,
	14,
	0,
	0,
	90,
	250,
	89,
	437,
	43,
	102,
	187,
	126,
	765,
	103,
	1800,
	120,
	120,
	9,
	10,
	4,
	0,
	128,
	0,
	43,
	1801,
	1802,
	1803,
	1804,
	1805,
	0,
	1398,
	1806,
	1397,
	1807,
	1808,
	0,
	1596,
	1809,
	1188,
	1810,
	1140,
	1811,
	1180,
	1812,
	1813,
	1399,
	1814,
	1815,
	1393,
	1816,
	1817,
	1138,
	1818,
	28,
	14,
	28,
	1395,
	1819,
	23,
	3,
	95,
	1820,
	1821,
	1822,
	95,
	1820,
	1821,
	1823,
	14,
	95,
	1799,
	1820,
	1821,
	1823,
	14,
	95,
	1798,
	26,
	1822,
	14,
	95,
	23,
	14,
	23,
	14,
	32,
	23,
	95,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	95,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	95,
	31,
	10,
	95,
	1797,
	34,
	62,
	28,
	27,
	10,
	27,
	34,
	28,
	14,
	169,
	23,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	14,
	14,
	95,
	31,
	10,
	95,
	1797,
	28,
	27,
	34,
	62,
	10,
	27,
	28,
	34,
	28,
	14,
	169,
	23,
	23,
	1824,
	32,
	23,
	95,
	23,
	14,
	23,
	14,
	14,
	14,
	10,
	95,
	1797,
	14,
	26,
	26,
	169,
	9,
	10,
	10,
	95,
	1797,
	14,
	26,
	408,
	297,
	149,
	179,
	297,
	26,
	169,
	103,
	9,
	10,
	10,
	95,
	1797,
	14,
	26,
	95,
	31,
	31,
	26,
	169,
	9,
	10,
	4,
	23,
	10,
	95,
	1797,
	14,
	26,
	95,
	31,
	9,
	10,
	169,
	3,
	10,
	1797,
	26,
	27,
	-1,
	34,
	62,
	28,
	27,
	26,
	27,
	120,
	120,
	9,
	10,
	10,
	32,
	685,
	296,
	408,
	297,
	149,
	179,
	95,
	31,
	14,
	14,
	169,
	0,
	111,
	525,
	1825,
	365,
	111,
	1826,
	1827,
	1828,
	111,
	1829,
	1830,
	1831,
	111,
	1832,
	1833,
	1648,
	111,
	1834,
	546,
	1835,
	111,
	793,
	1825,
	364,
	111,
	10,
	121,
	123,
	111,
	6,
	1836,
	114,
	111,
	1837,
	1838,
	114,
	111,
	95,
	121,
	9,
	111,
	194,
	1576,
	9,
	111,
	1839,
	1840,
	26,
	111,
	23,
	121,
	26,
	111,
	1841,
	121,
	1835,
	111,
	1841,
	121,
	1835,
	111,
	1842,
	1843,
	123,
	111,
	37,
	546,
	123,
	111,
	680,
	1844,
	1845,
	111,
	37,
	546,
	123,
	111,
	37,
	546,
	123,
	111,
	37,
	546,
	123,
	111,
	30,
	546,
	9,
	111,
	833,
	1846,
	1648,
	111,
	1847,
	1846,
	1848,
	111,
	1849,
	1846,
	1850,
	111,
	1851,
	1846,
	1852,
	111,
	1853,
	1846,
	1854,
	111,
	1855,
	1856,
	1850,
	111,
	1857,
	1858,
	1850,
	111,
	16,
	546,
	117,
	111,
	1859,
	1860,
	1648,
	111,
	1861,
	1862,
	364,
	111,
	16,
	546,
	117,
	111,
	1863,
	1390,
	1864,
	111,
	1865,
	1866,
	1648,
	111,
	1867,
	1868,
	364,
	111,
	1092,
	1869,
	26,
	111,
	16,
	546,
	117,
	111,
	16,
	546,
	117,
	111,
	95,
	121,
	9,
	111,
	95,
	121,
	9,
	111,
	95,
	121,
	9,
	111,
	95,
	121,
	9,
	111,
	1870,
	1871,
	123,
	111,
	37,
	546,
	123,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	1872,
	1873,
	1831,
	111,
	1829,
	1830,
	1831,
	111,
	525,
	1825,
	365,
	111,
	16,
	546,
	117,
	111,
	1874,
	1875,
	1850,
	111,
	1876,
	1877,
	1878,
	111,
	1879,
	1880,
	1881,
	111,
	1882,
	1883,
	1850,
	111,
	1874,
	1875,
	1850,
	111,
	385,
	1884,
	123,
	111,
	1885,
	1886,
	1850,
	111,
	1876,
	1877,
	1878,
	111,
	1887,
	1888,
	1881,
	111,
	1889,
	1890,
	1881,
	111,
	736,
	1891,
	123,
	111,
	1892,
	790,
	123,
	111,
	123,
	184,
	123,
	111,
	9,
	184,
	9,
	111,
	10,
	121,
	123,
	111,
	1893,
	423,
	123,
	111,
	1893,
	423,
	123,
	111,
	123,
	184,
	123,
	111,
	1894,
	1895,
	123,
	111,
	41,
	218,
	123,
	111,
	123,
	184,
	123,
	111,
	9,
	184,
	9,
	111,
	1020,
	311,
	123,
	111,
	123,
	184,
	123,
	111,
	16,
	546,
	117,
	111,
	1896,
	1897,
	1850,
	111,
	1898,
	1899,
	1648,
	111,
	1900,
	1899,
	1852,
	111,
	1892,
	790,
	123,
	111,
	9,
	184,
	9,
	111,
	41,
	218,
	123,
	111,
	67,
	1136,
	9,
	111,
	67,
	1136,
	9,
	111,
	726,
	1136,
	123,
	111,
	1893,
	423,
	123,
	111,
	458,
	1113,
	123,
	111,
	10,
	121,
	123,
	111,
	123,
	184,
	123,
	111,
	16,
	546,
	117,
	111,
	95,
	121,
	9,
	111,
	1901,
	1902,
	123,
	111,
	10,
	121,
	123,
	111,
	10,
	121,
	123,
	111,
	793,
	1825,
	364,
	111,
	792,
	1836,
	1648,
	111,
	23,
	121,
	26,
	111,
	1903,
	1904,
	26,
	111,
	1905,
	1906,
	365,
	111,
	95,
	121,
	9,
	111,
	31,
	1576,
	26,
	111,
	30,
	546,
	9,
	111,
	23,
	121,
	26,
	111,
	793,
	1825,
	364,
	111,
	792,
	1836,
	1648,
	111,
	953,
	1907,
	1648,
	111,
	953,
	1907,
	1648,
	111,
	792,
	1836,
	1648,
	111,
	792,
	1836,
	1648,
	111,
	1131,
	1908,
	26,
	111,
	143,
	1113,
	26,
	111,
	6,
	1836,
	114,
	111,
	6,
	1836,
	114,
	111,
	32,
	546,
	26,
	111,
	792,
	1836,
	1648,
	111,
	143,
	1113,
	26,
	111,
	953,
	1907,
	1648,
	111,
	439,
	1113,
	9,
	111,
	953,
	1907,
	1648,
	111,
	953,
	1907,
	1648,
	111,
	439,
	1113,
	9,
	111,
	32,
	546,
	26,
	111,
	10,
	121,
	123,
	111,
	1909,
	422,
	123,
	111,
	1909,
	422,
	123,
	111,
	1910,
	1911,
	1881,
	111,
	1912,
	1913,
	1881,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	1859,
	1860,
	1648,
	111,
	596,
	1860,
	1850,
	111,
	685,
	121,
	199,
	111,
	64,
	1860,
	114,
	111,
	1914,
	1915,
	26,
	111,
	1916,
	1576,
	1917,
	111,
	1626,
	1918,
	26,
	111,
	685,
	121,
	199,
	111,
	458,
	1113,
	123,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	95,
	121,
	9,
	111,
	10,
	121,
	123,
	111,
	10,
	121,
	123,
	111,
	95,
	121,
	9,
	111,
	23,
	121,
	26,
	111,
	23,
	121,
	26,
	111,
	95,
	121,
	9,
	111,
	23,
	121,
	26,
	111,
	95,
	121,
	9,
	111,
	31,
	1576,
	26,
	111,
	23,
	121,
	26,
	111,
	31,
	1576,
	26,
	111,
	1919,
	1920,
	1850,
	111,
	7,
	787,
	26,
	111,
	1921,
	1922,
	1850,
	111,
	1923,
	1924,
	9,
	111,
	7,
	787,
	26,
	111,
	7,
	787,
	26,
	111,
	458,
	1113,
	123,
	111,
	1925,
	1926,
	123,
	111,
	32,
	546,
	26,
	111,
	10,
	121,
	123,
	111,
	443,
	1907,
	1850,
	111,
	1927,
	1928,
	1850,
	111,
	385,
	1884,
	123,
	111,
	385,
	1884,
	123,
	111,
	149,
	121,
	198,
	111,
	1929,
	1930,
	1850,
	111,
	1929,
	1930,
	1850,
	111,
	1931,
	1932,
	123,
	111,
	1933,
	1934,
	1881,
	111,
	16,
	546,
	117,
	111,
	736,
	1891,
	123,
	111,
	385,
	1884,
	123,
	111,
	1935,
	1936,
	123,
	111,
	1937,
	1938,
	1850,
	111,
	1939,
	1940,
	123,
	111,
	1941,
	1942,
	1878,
	111,
	1943,
	1944,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	1943,
	1944,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	736,
	1891,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	1943,
	1944,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	1947,
	1948,
	123,
	111,
	1949,
	1950,
	1881,
	111,
	736,
	1891,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	1945,
	1946,
	1850,
	111,
	1945,
	1946,
	1850,
	111,
	1951,
	1952,
	1881,
	111,
	1953,
	1954,
	1850,
	111,
	1945,
	1946,
	1850,
	111,
	1937,
	1938,
	1850,
	111,
	1949,
	1950,
	1881,
	111,
	1937,
	1938,
	1850,
	111,
	1949,
	1950,
	1881,
	111,
	1955,
	1956,
	123,
	111,
	1957,
	1958,
	1850,
	111,
	1949,
	1950,
	1881,
	111,
	1959,
	1960,
	1850,
	111,
	385,
	1884,
	123,
	111,
	385,
	1884,
	123,
	111,
	386,
	1884,
	9,
	111,
	1961,
	1962,
	1850,
	111,
	1963,
	1964,
	1648,
	111,
	1945,
	1946,
	1850,
	111,
	736,
	1891,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	1945,
	1946,
	1850,
	111,
	1965,
	1950,
	364,
	111,
	386,
	1884,
	9,
	111,
	149,
	121,
	198,
	111,
	385,
	1884,
	123,
	111,
	1966,
	1967,
	123,
	111,
	1968,
	1969,
	123,
	111,
	1970,
	1971,
	123,
	111,
	1972,
	1973,
	1881,
	111,
	1945,
	1946,
	1850,
	111,
	385,
	1884,
	123,
	111,
	1974,
	1975,
	123,
	111,
	1931,
	1932,
	123,
	111,
	1976,
	1977,
	1978,
	111,
	1979,
	1980,
	123,
	111,
	1949,
	1950,
	1881,
	111,
	1981,
	1982,
	1881,
	111,
	95,
	121,
	9,
	111,
	386,
	1884,
	9,
	111,
	736,
	1891,
	123,
	111,
	1945,
	1946,
	1850,
	111,
	26,
	184,
	26,
	111,
	10,
	121,
	123,
	111,
	1983,
	1984,
	123,
	111,
	1985,
	1986,
	123,
	111,
	458,
	1113,
	123,
	111,
	23,
	121,
	26,
	111,
	775,
	1875,
	114,
	111,
	1987,
	1988,
	26,
	111,
	1989,
	1990,
	1850,
	111,
	1945,
	1946,
	1850,
	111,
	1991,
	1992,
	123,
	111,
	23,
	121,
	26,
	111,
	443,
	1907,
	1850,
	111,
	7,
	787,
	26,
	111,
	1874,
	1875,
	1850,
	111,
	7,
	787,
	26,
	111,
	1919,
	1920,
	1850,
	111,
	1993,
	1924,
	123,
	111,
	7,
	787,
	26,
	111,
	1893,
	423,
	123,
	111,
	10,
	121,
	123,
	111,
	123,
	184,
	123,
	111,
	1909,
	422,
	123,
	111,
	1994,
	218,
	198,
	111,
	1894,
	1895,
	123,
	111,
	1995,
	1996,
	364,
	111,
	1997,
	1998,
	1001,
	111,
	122,
	218,
	9,
	111,
	1999,
	2000,
	1850,
	111,
	1999,
	2000,
	1850,
	111,
	16,
	546,
	117,
	111,
	2001,
	2002,
	1881,
	111,
	37,
	546,
	123,
	111,
	16,
	546,
	117,
	111,
	2003,
	2004,
	1648,
	111,
	796,
	2005,
	114,
	111,
	987,
	2000,
	114,
	111,
	2006,
	2007,
	114,
	111,
	2008,
	2009,
	114,
	111,
	2010,
	1928,
	1648,
	111,
	1927,
	1928,
	1850,
	111,
	2011,
	1928,
	1848,
	111,
	2012,
	2013,
	114,
	111,
	587,
	1907,
	114,
	111,
	572,
	1928,
	114,
	111,
	2014,
	2015,
	1850,
	111,
	458,
	1113,
	123,
	111,
	1874,
	1875,
	1850,
	111,
	1857,
	1858,
	1850,
	111,
	2016,
	2017,
	123,
	111,
	2018,
	2019,
	1850,
	111,
	2020,
	2021,
	123,
	111,
	726,
	1136,
	123,
	111,
	1894,
	1895,
	123,
	111,
	10,
	121,
	123,
	111,
	1893,
	423,
	123,
	111,
	198,
	184,
	198,
	111,
	123,
	184,
	123,
	111,
	443,
	1907,
	1850,
	111,
	443,
	1907,
	1850,
	111,
	443,
	1907,
	1850,
	111,
	459,
	690,
	123,
	111,
	2022,
	2023,
	1850,
	111,
	2022,
	2023,
	1850,
	111,
	2024,
	2025,
	1850,
	111,
	2022,
	2023,
	1850,
	111,
	2026,
	2027,
	123,
	111,
	2028,
	2029,
	1850,
	111,
	2030,
	2031,
	1850,
	111,
	2032,
	2033,
	123,
	111,
	2034,
	2035,
	123,
	111,
	2036,
	2037,
	123,
	111,
	2038,
	1964,
	1850,
	111,
	2039,
	2040,
	123,
	111,
	2041,
	2042,
	123,
	111,
	1116,
	1118,
	1850,
	111,
	385,
	1884,
	123,
	111,
	2043,
	2044,
	1850,
	111,
	2045,
	2046,
	123,
	111,
	898,
	1884,
	198,
	111,
	443,
	1907,
	1850,
	111,
	1879,
	1880,
	1881,
	111,
	1849,
	1846,
	1850,
	111,
	2047,
	2048,
	1850,
	7,
	525,
	1826,
	1829,
	1832,
	1834,
	793,
	10,
	6,
	1837,
	95,
	194,
	945,
	23,
	1841,
	1841,
	1893,
	37,
	680,
	37,
	37,
	37,
	30,
	833,
	1847,
	1849,
	1851,
	1853,
	1855,
	1857,
	34,
	1859,
	1861,
	34,
	1863,
	1865,
	1867,
	1092,
	34,
	34,
	95,
	95,
	95,
	95,
	1870,
	37,
	23,
	23,
	111,
	1859,
	1860,
	1648,
	111,
	1865,
	1866,
	1648,
	111,
	1867,
	1868,
	364,
	7,
	1872,
	1829,
	525,
	7,
	34,
	1874,
	1876,
	1879,
	1882,
	1874,
	385,
	1885,
	1876,
	1887,
	1889,
	736,
	7,
	1892,
	123,
	9,
	10,
	1893,
	1893,
	123,
	1901,
	41,
	123,
	9,
	1020,
	123,
	34,
	1896,
	1898,
	1900,
	1892,
	9,
	41,
	67,
	67,
	726,
	1893,
	458,
	10,
	123,
	34,
	95,
	1901,
	10,
	7,
	10,
	793,
	792,
	23,
	1903,
	1905,
	95,
	31,
	7,
	30,
	23,
	793,
	792,
	792,
	792,
	792,
	792,
	1131,
	26,
	6,
	6,
	32,
	792,
	26,
	792,
	9,
	792,
	953,
	439,
	7,
	32,
	10,
	41,
	41,
	1910,
	1912,
	23,
	23,
	1859,
	596,
	685,
	64,
	1914,
	1916,
	1626,
	685,
	123,
	23,
	23,
	23,
	23,
	95,
	10,
	10,
	95,
	23,
	23,
	95,
	23,
	95,
	31,
	23,
	31,
	1919,
	7,
	1921,
	1923,
	7,
	7,
	458,
	1925,
	32,
	10,
	7,
	443,
	1927,
	385,
	385,
	149,
	1929,
	1929,
	1931,
	1933,
	34,
	736,
	385,
	1935,
	1937,
	1939,
	1941,
	1943,
	1945,
	1943,
	1945,
	736,
	1945,
	1943,
	1945,
	1947,
	1949,
	736,
	1945,
	1945,
	1945,
	1951,
	1953,
	1945,
	1937,
	1949,
	1937,
	1949,
	1955,
	1957,
	1949,
	1959,
	385,
	385,
	386,
	1961,
	1963,
	1945,
	736,
	1945,
	1945,
	1965,
	386,
	149,
	385,
	1966,
	1968,
	1970,
	1972,
	1945,
	385,
	1974,
	1931,
	1976,
	1979,
	1949,
	1981,
	95,
	386,
	736,
	1945,
	26,
	10,
	1983,
	1985,
	458,
	23,
	775,
	1987,
	1989,
	1945,
	1991,
	23,
	111,
	1963,
	1964,
	1648,
	7,
	443,
	7,
	1874,
	7,
	1919,
	1993,
	7,
	1893,
	10,
	123,
	1909,
	1994,
	1894,
	1995,
	1997,
	122,
	1999,
	1999,
	34,
	111,
	1997,
	1998,
	1001,
	7,
	2001,
	37,
	7,
	34,
	2003,
	796,
	987,
	2006,
	2008,
	2010,
	1927,
	2011,
	2012,
	587,
	572,
	7,
	2014,
	123,
	1874,
	1857,
	2016,
	2018,
	2020,
	7,
	726,
	1894,
	7,
	10,
	1893,
	198,
	7,
	123,
	443,
	443,
	443,
	458,
	2022,
	2022,
	2024,
	2022,
	2049,
	2028,
	2050,
	2032,
	2051,
	2036,
	2038,
	2039,
	2052,
	7,
	1116,
	385,
	2043,
	2045,
	898,
	7,
	443,
	1879,
	1849,
	2047,
	50,
	639,
	3,
	49,
	49,
	553,
	2053,
	103,
	138,
	23,
	14,
	2054,
	6,
	2055,
	6,
	2056,
	6,
	2057,
	6,
	14,
	50,
	639,
	3,
	49,
	49,
	43,
	2053,
	103,
	138,
	138,
	140,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	446,
	3,
	23,
	3,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	27,
	2058,
	111,
	1045,
	2059,
	26,
	2060,
	23,
	10,
	95,
	31,
	685,
	296,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	95,
	95,
	23,
	23,
	95,
	436,
	1188,
	95,
	95,
	26,
	26,
	23,
	2061,
	27,
	27,
	1669,
	23,
	1806,
	14,
	14,
	9,
	103,
	2062,
	26,
	10,
	1188,
	23,
	14,
	10,
	10,
	10,
	1672,
	1538,
	27,
	1589,
	2063,
	3,
	23,
	1673,
	1673,
	26,
	14,
	103,
	2064,
	1398,
	2065,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060004A6, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 18000 },
};
extern const Il2CppCodeGenModule g_Oculus_VRCodeGenModule;
const Il2CppCodeGenModule g_Oculus_VRCodeGenModule = 
{
	"Oculus.VR.dll",
	2974,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
