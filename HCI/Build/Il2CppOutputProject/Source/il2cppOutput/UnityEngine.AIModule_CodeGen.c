﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AI.NavMeshPath::.ctor()
extern void NavMeshPath__ctor_m97A90951BF188A25A9BD247CEDE6C2A8045A1B3C ();
// 0x00000002 System.Void UnityEngine.AI.NavMeshPath::Finalize()
extern void NavMeshPath_Finalize_m23F790F8E5479FBF2F8F1DF6154A882FE0D1E8C5 ();
// 0x00000003 System.IntPtr UnityEngine.AI.NavMeshPath::InitializeNavMeshPath()
extern void NavMeshPath_InitializeNavMeshPath_m813BB3AF473DA864BCACAA16EACA97FF754F5720 ();
// 0x00000004 System.Void UnityEngine.AI.NavMeshPath::DestroyNavMeshPath(System.IntPtr)
extern void NavMeshPath_DestroyNavMeshPath_mA533E9C84A96BCD0D10A3B0CB384378A2269E48F ();
// 0x00000005 System.Void UnityEngine.AI.NavMeshPath::ClearCornersInternal()
extern void NavMeshPath_ClearCornersInternal_mAA7B067EB75C4E3DC1B663053DBD6DFC87395277 ();
// 0x00000006 System.Void UnityEngine.AI.NavMeshPath::ClearCorners()
extern void NavMeshPath_ClearCorners_mB0B7FF49CE2AAD120C9C8279A9F47467C422C051 ();
// 0x00000007 UnityEngine.AI.NavMeshPathStatus UnityEngine.AI.NavMeshPath::get_status()
extern void NavMeshPath_get_status_mD9455D8007F954FD0E487D468D3A3BABAE5FA8EE ();
// 0x00000008 System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60 ();
// 0x00000009 System.Boolean UnityEngine.AI.NavMesh::CalculatePath(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePath_mCF9690B429137161B20FF8AEA81DB02A1D825D62 ();
// 0x0000000A System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_m5D4F3A7F26D2A2FA51E85D29E279CA70919C764C ();
// 0x0000000B System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_Injected_mF0ED4ECC69594E07FC412C14366F68BB29AE28F5 ();
// 0x0000000C System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42 ();
// 0x0000000D System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0 ();
// 0x0000000E System.IAsyncResult UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6 ();
// 0x0000000F System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::EndInvoke(System.IAsyncResult)
extern void OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836 ();
static Il2CppMethodPointer s_methodPointers[15] = 
{
	NavMeshPath__ctor_m97A90951BF188A25A9BD247CEDE6C2A8045A1B3C,
	NavMeshPath_Finalize_m23F790F8E5479FBF2F8F1DF6154A882FE0D1E8C5,
	NavMeshPath_InitializeNavMeshPath_m813BB3AF473DA864BCACAA16EACA97FF754F5720,
	NavMeshPath_DestroyNavMeshPath_mA533E9C84A96BCD0D10A3B0CB384378A2269E48F,
	NavMeshPath_ClearCornersInternal_mAA7B067EB75C4E3DC1B663053DBD6DFC87395277,
	NavMeshPath_ClearCorners_mB0B7FF49CE2AAD120C9C8279A9F47467C422C051,
	NavMeshPath_get_status_mD9455D8007F954FD0E487D468D3A3BABAE5FA8EE,
	NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60,
	NavMesh_CalculatePath_mCF9690B429137161B20FF8AEA81DB02A1D825D62,
	NavMesh_CalculatePathInternal_m5D4F3A7F26D2A2FA51E85D29E279CA70919C764C,
	NavMesh_CalculatePathInternal_Injected_mF0ED4ECC69594E07FC412C14366F68BB29AE28F5,
	OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42,
	OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0,
	OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6,
	OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836,
};
static const int32_t s_InvokerIndices[15] = 
{
	23,
	23,
	718,
	25,
	23,
	23,
	10,
	3,
	2066,
	2066,
	2067,
	111,
	23,
	121,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	15,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
