﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 UnityEngine.AudioSettings::GetSampleRate()
extern void AudioSettings_GetSampleRate_mB74463D0A8B5FF5BBDFE9FA4EAB295405202220E ();
// 0x00000002 System.Int32 UnityEngine.AudioSettings::get_outputSampleRate()
extern void AudioSettings_get_outputSampleRate_mA9092240D8A06109EA34644BD7FB239483F9A5F9 ();
// 0x00000003 System.Void UnityEngine.AudioSettings::GetDSPBufferSize(System.Int32&,System.Int32&)
extern void AudioSettings_GetDSPBufferSize_m1C77C6BAEFD074B0719A204C057D45C8D188C85D ();
// 0x00000004 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern void AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7 ();
// 0x00000005 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern void AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A ();
// 0x00000006 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern void AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49 ();
// 0x00000007 System.IAsyncResult UnityEngine.AudioSettings_AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827 ();
// 0x00000008 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern void AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830 ();
// 0x00000009 System.Void UnityEngine.AudioClip::.ctor()
extern void AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D ();
// 0x0000000A System.Boolean UnityEngine.AudioClip::GetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128 ();
// 0x0000000B System.Boolean UnityEngine.AudioClip::SetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_SetData_m9422A5FB335AFB42C2CBAD4EE650C5F71ECB3559 ();
// 0x0000000C UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
extern void AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0 ();
// 0x0000000D System.String UnityEngine.AudioClip::GetName()
extern void AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257 ();
// 0x0000000E System.Void UnityEngine.AudioClip::CreateUserSound(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD ();
// 0x0000000F System.Single UnityEngine.AudioClip::get_length()
extern void AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317 ();
// 0x00000010 System.Int32 UnityEngine.AudioClip::get_samples()
extern void AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0 ();
// 0x00000011 System.Int32 UnityEngine.AudioClip::get_channels()
extern void AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC ();
// 0x00000012 System.Int32 UnityEngine.AudioClip::get_frequency()
extern void AudioClip_get_frequency_m8CE3C1898AEC71B496092039B233B61EED527661 ();
// 0x00000013 System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern void AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0 ();
// 0x00000014 System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
extern void AudioClip_SetData_m7B400A0E491EDFE98D3A732D189443846E74CD6C ();
// 0x00000015 UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AudioClip_Create_m76744E6CF5A521F79D0282A6FBEAAC37FE9C0BF5 ();
// 0x00000016 UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.AudioClip_PCMReaderCallback,UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995 ();
// 0x00000017 System.Void UnityEngine.AudioClip::add_m_PCMReaderCallback(UnityEngine.AudioClip_PCMReaderCallback)
extern void AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62 ();
// 0x00000018 System.Void UnityEngine.AudioClip::remove_m_PCMReaderCallback(UnityEngine.AudioClip_PCMReaderCallback)
extern void AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7 ();
// 0x00000019 System.Void UnityEngine.AudioClip::add_m_PCMSetPositionCallback(UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5 ();
// 0x0000001A System.Void UnityEngine.AudioClip::remove_m_PCMSetPositionCallback(UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2 ();
// 0x0000001B System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern void AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9 ();
// 0x0000001C System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern void AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F ();
// 0x0000001D System.Void UnityEngine.AudioClip_PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern void PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6 ();
// 0x0000001E System.Void UnityEngine.AudioClip_PCMReaderCallback::Invoke(System.Single[])
extern void PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6 ();
// 0x0000001F System.IAsyncResult UnityEngine.AudioClip_PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F ();
// 0x00000020 System.Void UnityEngine.AudioClip_PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern void PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B ();
// 0x00000021 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern void PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F ();
// 0x00000022 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::Invoke(System.Int32)
extern void PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C ();
// 0x00000023 System.IAsyncResult UnityEngine.AudioClip_PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093 ();
// 0x00000024 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern void PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C ();
// 0x00000025 System.Boolean UnityEngine.AudioListener::get_pause()
extern void AudioListener_get_pause_m9CE06C0BA7D07A01E7F76168DDE079FB14D32B0F ();
// 0x00000026 System.Single UnityEngine.AudioSource::GetPitch(UnityEngine.AudioSource)
extern void AudioSource_GetPitch_m76B68079F309EB1FE2BFF4A0B014B89780B383CA ();
// 0x00000027 System.Void UnityEngine.AudioSource::SetPitch(UnityEngine.AudioSource,System.Single)
extern void AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B ();
// 0x00000028 System.Void UnityEngine.AudioSource::PlayHelper(UnityEngine.AudioSource,System.UInt64)
extern void AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC ();
// 0x00000029 System.Void UnityEngine.AudioSource::Play(System.Double)
extern void AudioSource_Play_m5F47EF396B3230E70CF138E9F9AEFFB90AA7C0F0 ();
// 0x0000002A System.Void UnityEngine.AudioSource::Stop(System.Boolean)
extern void AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8 ();
// 0x0000002B System.Void UnityEngine.AudioSource::SetCustomCurveHelper(UnityEngine.AudioSource,UnityEngine.AudioSourceCurveType,UnityEngine.AnimationCurve)
extern void AudioSource_SetCustomCurveHelper_mB444ACB3E7BF771A4E724310A44669D5F1236C23 ();
// 0x0000002C System.Single UnityEngine.AudioSource::get_volume()
extern void AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0 ();
// 0x0000002D System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern void AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06 ();
// 0x0000002E System.Single UnityEngine.AudioSource::get_pitch()
extern void AudioSource_get_pitch_m70F33CAA7F869F88AB78BFEFA7385CF533F9B50D ();
// 0x0000002F System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern void AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0 ();
// 0x00000030 System.Single UnityEngine.AudioSource::get_time()
extern void AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52 ();
// 0x00000031 System.Void UnityEngine.AudioSource::set_time(System.Single)
extern void AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1 ();
// 0x00000032 System.Int32 UnityEngine.AudioSource::get_timeSamples()
extern void AudioSource_get_timeSamples_m0E3467605A8D6CBFA36D6F167E4D663DA2D17EE7 ();
// 0x00000033 UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern void AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643 ();
// 0x00000034 System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B ();
// 0x00000035 System.Void UnityEngine.AudioSource::set_outputAudioMixerGroup(UnityEngine.Audio.AudioMixerGroup)
extern void AudioSource_set_outputAudioMixerGroup_mC153530274A1109635889A23646E1F5BA5C150E1 ();
// 0x00000036 System.Void UnityEngine.AudioSource::Play()
extern void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 ();
// 0x00000037 System.Void UnityEngine.AudioSource::PlayDelayed(System.Single)
extern void AudioSource_PlayDelayed_m9C5DCC1E8BC219DACBEB0907D7BF9A9AF7174CE1 ();
// 0x00000038 System.Void UnityEngine.AudioSource::Stop()
extern void AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200 ();
// 0x00000039 System.Void UnityEngine.AudioSource::Pause()
extern void AudioSource_Pause_m279B116A62699983F6B5347FD4770358D7794F3C ();
// 0x0000003A System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern void AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C ();
// 0x0000003B System.Boolean UnityEngine.AudioSource::get_loop()
extern void AudioSource_get_loop_m576259B36687CA2327106F945A3E49E3D74A7F90 ();
// 0x0000003C System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern void AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4 ();
// 0x0000003D System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern void AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128 ();
// 0x0000003E UnityEngine.AudioVelocityUpdateMode UnityEngine.AudioSource::get_velocityUpdateMode()
extern void AudioSource_get_velocityUpdateMode_m846BE29AA8C801B0631EEDE787B15456CF5C61F9 ();
// 0x0000003F System.Void UnityEngine.AudioSource::set_velocityUpdateMode(UnityEngine.AudioVelocityUpdateMode)
extern void AudioSource_set_velocityUpdateMode_m9D5B3C54EE308B05B103175E3D892F05134DF92D ();
// 0x00000040 System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern void AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5 ();
// 0x00000041 System.Boolean UnityEngine.AudioSource::get_spatialize()
extern void AudioSource_get_spatialize_m8CC430356EDE4FF4BD31A5F5509028B7DDCC4C11 ();
// 0x00000042 System.Void UnityEngine.AudioSource::set_spatialize(System.Boolean)
extern void AudioSource_set_spatialize_m67B01FA31D125E7A6E3E20A56A781D1E9200D8FD ();
// 0x00000043 System.Void UnityEngine.AudioSource::set_spatializePostEffects(System.Boolean)
extern void AudioSource_set_spatializePostEffects_m7030C76A06DEBF1A5096512CD132D697E8ACB89B ();
// 0x00000044 System.Void UnityEngine.AudioSource::SetCustomCurve(UnityEngine.AudioSourceCurveType,UnityEngine.AnimationCurve)
extern void AudioSource_SetCustomCurve_m2116803AB60CE22B9FD98B83D40F954D7A246C69 ();
// 0x00000045 System.Void UnityEngine.AudioSource::set_dopplerLevel(System.Single)
extern void AudioSource_set_dopplerLevel_mA90937913103AB595C891B72B4B5724F8F54AB86 ();
// 0x00000046 System.Void UnityEngine.AudioSource::set_spread(System.Single)
extern void AudioSource_set_spread_m511F99128902DF9745AD5337D79E3D43E2BE3DBF ();
// 0x00000047 System.Boolean UnityEngine.AudioSource::get_mute()
extern void AudioSource_get_mute_mC420E7827CA751603A85430D849E8C3099FBD123 ();
// 0x00000048 System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern void AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF ();
// 0x00000049 System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
extern void AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C ();
// 0x0000004A System.Void UnityEngine.AudioSource::set_maxDistance(System.Single)
extern void AudioSource_set_maxDistance_m28ACA41DEC146D471E3E5EC0CC4EC2013D46F5E0 ();
// 0x0000004B System.Void UnityEngine.AudioSource::set_rolloffMode(UnityEngine.AudioRolloffMode)
extern void AudioSource_set_rolloffMode_m1E420E7DC5D44A0001A22352C1CD59BEAED1794D ();
// 0x0000004C System.Boolean UnityEngine.AudioSource::SetSpatializerFloat(System.Int32,System.Single)
extern void AudioSource_SetSpatializerFloat_mA34E8E88A05FF3682B00E1D29D80581AFED8098F ();
// 0x0000004D System.Boolean UnityEngine.AudioSource::GetAmbisonicDecoderFloat(System.Int32,System.Single&)
extern void AudioSource_GetAmbisonicDecoderFloat_mC2CCC987BF33DF788AAFDA36FF24C72FE69EE33D ();
// 0x0000004E System.Boolean UnityEngine.AudioSource::SetAmbisonicDecoderFloat(System.Int32,System.Single)
extern void AudioSource_SetAmbisonicDecoderFloat_mE04D738825362140D90E7F2C8793C590CF76288A ();
// 0x0000004F System.Int32 UnityEngine.Microphone::GetMicrophoneDeviceIDFromName(System.String)
extern void Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E ();
// 0x00000050 UnityEngine.AudioClip UnityEngine.Microphone::StartRecord(System.Int32,System.Boolean,System.Single,System.Int32)
extern void Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8 ();
// 0x00000051 System.Void UnityEngine.Microphone::EndRecord(System.Int32)
extern void Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733 ();
// 0x00000052 System.Boolean UnityEngine.Microphone::IsRecording(System.Int32)
extern void Microphone_IsRecording_m70F5B087E69C0077AA9238931776EDE123FCF122 ();
// 0x00000053 System.Int32 UnityEngine.Microphone::GetRecordPosition(System.Int32)
extern void Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED ();
// 0x00000054 System.Void UnityEngine.Microphone::GetDeviceCaps(System.Int32,System.Int32&,System.Int32&)
extern void Microphone_GetDeviceCaps_m4592CD4A6371544156A042579626B9351DCD7E93 ();
// 0x00000055 UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern void Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047 ();
// 0x00000056 System.Void UnityEngine.Microphone::End(System.String)
extern void Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F ();
// 0x00000057 System.String[] UnityEngine.Microphone::get_devices()
extern void Microphone_get_devices_mA813A0457D5CEDA893343B389669AF4B5CFB7FC0 ();
// 0x00000058 System.Boolean UnityEngine.Microphone::IsRecording(System.String)
extern void Microphone_IsRecording_m2E3373AD36865B7261BBA5E9140271A08E0FA004 ();
// 0x00000059 System.Int32 UnityEngine.Microphone::GetPosition(System.String)
extern void Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF ();
// 0x0000005A System.Void UnityEngine.Microphone::GetDeviceCaps(System.String,System.Int32&,System.Int32&)
extern void Microphone_GetDeviceCaps_mF079FFC698AE94F132D4E0AD072498F6937BAF6B ();
// 0x0000005B UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern void AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk ();
// 0x0000005C System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern void AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk ();
// 0x0000005D UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern void AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk ();
// 0x0000005E System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern void AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk ();
// 0x0000005F System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::Finalize()
extern void AudioSampleProvider_Finalize_mACCDEE1F5F0F602DC35AE68875EB8DB16830544C ();
// 0x00000060 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::Dispose()
extern void AudioSampleProvider_Dispose_mA11D514354EDA94E8B6AA83B726DF85169C8E29A ();
// 0x00000061 System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider::get_id()
extern void AudioSampleProvider_get_id_m9E16910C6A2335F87E6B257D3059A8BBC8D7253A ();
// 0x00000062 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::set_id(System.UInt32)
extern void AudioSampleProvider_set_id_mB98E8435407CAD305BBEAA91B18CBC12562DFAF9 ();
// 0x00000063 UnityEngine.Object UnityEngine.Experimental.Audio.AudioSampleProvider::get_owner()
extern void AudioSampleProvider_get_owner_m822057CE02CF8664D7BF569CDB41D8EB154251FE ();
// 0x00000064 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::set_owner(UnityEngine.Object)
extern void AudioSampleProvider_set_owner_m21FC4073346BFBE33E2B1567ECEC8ECD373B3069 ();
// 0x00000065 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832 ();
// 0x00000066 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4 ();
// 0x00000067 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InternalRemove(System.UInt32)
extern void AudioSampleProvider_InternalRemove_m3CD8E2D57A6DC5522C71436A854926C346CEBF11 ();
// 0x00000068 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InternalSetScriptingPtr(System.UInt32,UnityEngine.Experimental.Audio.AudioSampleProvider)
extern void AudioSampleProvider_InternalSetScriptingPtr_m018841DA2C6D92E2322F1D041E4C88A054991AF9 ();
// 0x00000069 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::.ctor(System.Object,System.IntPtr)
extern void ConsumeSampleFramesNativeFunction__ctor_m10D2C92ADC2CC0F2731CC790036CF84E7E6E97F6 ();
// 0x0000006A System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::Invoke(System.UInt32,System.IntPtr,System.UInt32)
extern void ConsumeSampleFramesNativeFunction_Invoke_mA644FB7343047BEC754D81AC2AFABDC300DF5D74 ();
// 0x0000006B System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::BeginInvoke(System.UInt32,System.IntPtr,System.UInt32,System.AsyncCallback,System.Object)
extern void ConsumeSampleFramesNativeFunction_BeginInvoke_mCFD45D186107B1FD87B1A492904CBCF49DDA28CB ();
// 0x0000006C System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::EndInvoke(System.IAsyncResult)
extern void ConsumeSampleFramesNativeFunction_EndInvoke_m4F91AA6FC2DFEF1495E3BA8D7BF1098C9E043711 ();
// 0x0000006D System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::.ctor(System.Object,System.IntPtr)
extern void SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5 ();
// 0x0000006E System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32)
extern void SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD ();
// 0x0000006F System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object)
extern void SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92 ();
// 0x00000070 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::EndInvoke(System.IAsyncResult)
extern void SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28 ();
// 0x00000071 System.Void UnityEngine.Audio.AudioMixer::TransitionToSnapshots(UnityEngine.Audio.AudioMixerSnapshot[],System.Single[],System.Single)
extern void AudioMixer_TransitionToSnapshots_m2731903094968E983D486F743E6A1BF8F7CF0238 ();
// 0x00000072 System.Void UnityEngine.Audio.AudioMixerSnapshot::TransitionTo(System.Single)
extern void AudioMixerSnapshot_TransitionTo_mFAC78DCAF192B7365D6C9741E4F8B4A38274304A ();
static Il2CppMethodPointer s_methodPointers[114] = 
{
	AudioSettings_GetSampleRate_mB74463D0A8B5FF5BBDFE9FA4EAB295405202220E,
	AudioSettings_get_outputSampleRate_mA9092240D8A06109EA34644BD7FB239483F9A5F9,
	AudioSettings_GetDSPBufferSize_m1C77C6BAEFD074B0719A204C057D45C8D188C85D,
	AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7,
	AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A,
	AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49,
	AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827,
	AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830,
	AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D,
	AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128,
	AudioClip_SetData_m9422A5FB335AFB42C2CBAD4EE650C5F71ECB3559,
	AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0,
	AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257,
	AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD,
	AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317,
	AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0,
	AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC,
	AudioClip_get_frequency_m8CE3C1898AEC71B496092039B233B61EED527661,
	AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0,
	AudioClip_SetData_m7B400A0E491EDFE98D3A732D189443846E74CD6C,
	AudioClip_Create_m76744E6CF5A521F79D0282A6FBEAAC37FE9C0BF5,
	AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995,
	AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62,
	AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7,
	AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5,
	AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2,
	AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9,
	AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F,
	PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6,
	PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6,
	PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F,
	PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B,
	PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F,
	PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C,
	PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093,
	PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C,
	AudioListener_get_pause_m9CE06C0BA7D07A01E7F76168DDE079FB14D32B0F,
	AudioSource_GetPitch_m76B68079F309EB1FE2BFF4A0B014B89780B383CA,
	AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B,
	AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC,
	AudioSource_Play_m5F47EF396B3230E70CF138E9F9AEFFB90AA7C0F0,
	AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8,
	AudioSource_SetCustomCurveHelper_mB444ACB3E7BF771A4E724310A44669D5F1236C23,
	AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0,
	AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06,
	AudioSource_get_pitch_m70F33CAA7F869F88AB78BFEFA7385CF533F9B50D,
	AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0,
	AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52,
	AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1,
	AudioSource_get_timeSamples_m0E3467605A8D6CBFA36D6F167E4D663DA2D17EE7,
	AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643,
	AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B,
	AudioSource_set_outputAudioMixerGroup_mC153530274A1109635889A23646E1F5BA5C150E1,
	AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164,
	AudioSource_PlayDelayed_m9C5DCC1E8BC219DACBEB0907D7BF9A9AF7174CE1,
	AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200,
	AudioSource_Pause_m279B116A62699983F6B5347FD4770358D7794F3C,
	AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C,
	AudioSource_get_loop_m576259B36687CA2327106F945A3E49E3D74A7F90,
	AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4,
	AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128,
	AudioSource_get_velocityUpdateMode_m846BE29AA8C801B0631EEDE787B15456CF5C61F9,
	AudioSource_set_velocityUpdateMode_m9D5B3C54EE308B05B103175E3D892F05134DF92D,
	AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5,
	AudioSource_get_spatialize_m8CC430356EDE4FF4BD31A5F5509028B7DDCC4C11,
	AudioSource_set_spatialize_m67B01FA31D125E7A6E3E20A56A781D1E9200D8FD,
	AudioSource_set_spatializePostEffects_m7030C76A06DEBF1A5096512CD132D697E8ACB89B,
	AudioSource_SetCustomCurve_m2116803AB60CE22B9FD98B83D40F954D7A246C69,
	AudioSource_set_dopplerLevel_mA90937913103AB595C891B72B4B5724F8F54AB86,
	AudioSource_set_spread_m511F99128902DF9745AD5337D79E3D43E2BE3DBF,
	AudioSource_get_mute_mC420E7827CA751603A85430D849E8C3099FBD123,
	AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF,
	AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C,
	AudioSource_set_maxDistance_m28ACA41DEC146D471E3E5EC0CC4EC2013D46F5E0,
	AudioSource_set_rolloffMode_m1E420E7DC5D44A0001A22352C1CD59BEAED1794D,
	AudioSource_SetSpatializerFloat_mA34E8E88A05FF3682B00E1D29D80581AFED8098F,
	AudioSource_GetAmbisonicDecoderFloat_mC2CCC987BF33DF788AAFDA36FF24C72FE69EE33D,
	AudioSource_SetAmbisonicDecoderFloat_mE04D738825362140D90E7F2C8793C590CF76288A,
	Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E,
	Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8,
	Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733,
	Microphone_IsRecording_m70F5B087E69C0077AA9238931776EDE123FCF122,
	Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED,
	Microphone_GetDeviceCaps_m4592CD4A6371544156A042579626B9351DCD7E93,
	Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047,
	Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F,
	Microphone_get_devices_mA813A0457D5CEDA893343B389669AF4B5CFB7FC0,
	Microphone_IsRecording_m2E3373AD36865B7261BBA5E9140271A08E0FA004,
	Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF,
	Microphone_GetDeviceCaps_mF079FFC698AE94F132D4E0AD072498F6937BAF6B,
	AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk,
	AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk,
	AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk,
	AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk,
	AudioSampleProvider_Finalize_mACCDEE1F5F0F602DC35AE68875EB8DB16830544C,
	AudioSampleProvider_Dispose_mA11D514354EDA94E8B6AA83B726DF85169C8E29A,
	AudioSampleProvider_get_id_m9E16910C6A2335F87E6B257D3059A8BBC8D7253A,
	AudioSampleProvider_set_id_mB98E8435407CAD305BBEAA91B18CBC12562DFAF9,
	AudioSampleProvider_get_owner_m822057CE02CF8664D7BF569CDB41D8EB154251FE,
	AudioSampleProvider_set_owner_m21FC4073346BFBE33E2B1567ECEC8ECD373B3069,
	AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832,
	AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4,
	AudioSampleProvider_InternalRemove_m3CD8E2D57A6DC5522C71436A854926C346CEBF11,
	AudioSampleProvider_InternalSetScriptingPtr_m018841DA2C6D92E2322F1D041E4C88A054991AF9,
	ConsumeSampleFramesNativeFunction__ctor_m10D2C92ADC2CC0F2731CC790036CF84E7E6E97F6,
	ConsumeSampleFramesNativeFunction_Invoke_mA644FB7343047BEC754D81AC2AFABDC300DF5D74,
	ConsumeSampleFramesNativeFunction_BeginInvoke_mCFD45D186107B1FD87B1A492904CBCF49DDA28CB,
	ConsumeSampleFramesNativeFunction_EndInvoke_m4F91AA6FC2DFEF1495E3BA8D7BF1098C9E043711,
	SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5,
	SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD,
	SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92,
	SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28,
	AudioMixer_TransitionToSnapshots_m2731903094968E983D486F743E6A1BF8F7CF0238,
	AudioMixerSnapshot_TransitionTo_mFAC78DCAF192B7365D6C9741E4F8B4A38274304A,
};
static const int32_t s_InvokerIndices[114] = 
{
	138,
	138,
	347,
	800,
	111,
	31,
	1576,
	26,
	23,
	1577,
	1577,
	4,
	14,
	1578,
	685,
	10,
	10,
	10,
	439,
	439,
	1579,
	1580,
	26,
	26,
	26,
	26,
	26,
	32,
	111,
	26,
	184,
	26,
	111,
	32,
	546,
	26,
	49,
	437,
	1368,
	127,
	297,
	31,
	447,
	685,
	296,
	685,
	296,
	685,
	296,
	10,
	14,
	26,
	26,
	23,
	296,
	23,
	23,
	95,
	95,
	31,
	31,
	10,
	32,
	296,
	95,
	31,
	31,
	62,
	296,
	296,
	95,
	31,
	296,
	296,
	32,
	1581,
	1582,
	1581,
	102,
	1583,
	140,
	46,
	21,
	840,
	1584,
	129,
	4,
	103,
	102,
	409,
	1153,
	1585,
	1153,
	1586,
	23,
	23,
	10,
	32,
	14,
	26,
	32,
	32,
	140,
	533,
	111,
	1587,
	1588,
	123,
	111,
	143,
	1113,
	26,
	1589,
	296,
};
extern const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule = 
{
	"UnityEngine.AudioModule.dll",
	114,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
