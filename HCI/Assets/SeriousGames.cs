﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeriousGames : MonoBehaviour
{
    public List<GameObject> clear;
    public List<GameObject> blurry;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < clear.Count; i++)
        {
            clear[i].SetActive(true);
        }
        for (int i = 0; i < blurry.Count; i++)
        {
            blurry[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
        {
            for (int i = 0; i < clear.Count; i++)
            {
                clear[i].SetActive(true);
            }
            for (int i = 0; i < blurry.Count; i++)
            {
                blurry[i].SetActive(false);
            }

        }
        if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
        {
            for (int i = 0; i < clear.Count; i++)
            {
                clear[i].SetActive(false);
            }
            for (int i = 0; i < blurry.Count; i++)
            {
                blurry[i].SetActive(true);
            }
        }
    }
}
